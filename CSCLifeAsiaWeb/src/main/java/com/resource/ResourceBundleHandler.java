package com.resource;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.datatype.StringData;

public class ResourceBundleHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBundleHandler.class);
	public static final String LOCALE_ENG = "eng";
	public static final String LOCALE_CHI = "chi";
	private static final String INTEGRAL_TITLE_COMMONS = "com.bundle.commonBundle.Integral_Title_Commons";
	private static final String INTEGRAL_COMMONS = "com.bundle.commonBundle.Integral_Commons";
	private ResourceBundle resourceBundleCommons = null;
	private ResourceBundle resourceBundleTitleCommons = null;
	private ResourceBundle resourceBundle = null;
	private String screenName = null;
	private ResourceBundle resourceBundleTitle = null;
	private ResourceBundle resourceBundleMenu = null;
	private static ResourceBundle resourceBundle_Eng = null;
	private static ResourceBundle resourceBundle_Chi = null;
	private String language = null;
	private static String company = "2";
	


	// This block load the bundle at start up
	static {
		resourceBundle_Eng = ResourceBundle.getBundle("com.bundle.Bundle",
				new Locale(LOCALE_ENG));
		resourceBundle_Chi = ResourceBundle.getBundle("com.bundle.Bundle",
				new Locale(LOCALE_CHI));
		LOGGER.info("<<<<<<<<<Initialised Resource Bundle>>>>>");

	}

	public ResourceBundleHandler() {
	};

	/**
	 * This constructor is no longer in use in integral.
	 * @param screenName
	 * @param language
	 */
	public ResourceBundleHandler(String screenName, String language) {
		this.screenName = screenName;
		this.language = language;
		// resourceBundle = getResourceBundle(new Locale(language));
		resourceBundle = getResourceBundleOld(new Locale(language));
	}

	public ResourceBundleHandler(String screenName, Locale locale) {
		this.screenName = screenName;
		resourceBundle = getResourceBundle(locale);
		resourceBundleTitle = getResourceBundletitle(locale);
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, locale);
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, locale);
	}

	public ResourceBundleHandler(Locale locale) {
		this.screenName = screenName;
		resourceBundle = getResourceBundle(locale);
		resourceBundleTitle = getResourceBundletitle(locale);
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, locale);
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, locale);
	//	resourceBundleMenu = getResourceBundleMenu(locale);
	}
	
	private static ResourceBundle getResourceBundle(String bundleFile, Locale locale) {
		return ResourceBundle.getBundle(bundleFile, locale);
	}
	public ResourceBundleHandler(String screenName, Locale locale, String company) {
		LOGGER.info(company); //change logger to info to remove unnecessary highlight 
		this.company=company;
		this.screenName = screenName;
		//resourceBundle = getResourceBundle(locale);
		//resourceBundleTitle = getResourceBundletitle(locale);
		resourceBundle = getResourceBundleNew(locale);
		resourceBundleTitle = getResourceBundletitleNew(locale);
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, locale);
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, locale);
		//new ResourceBundleHandler(screenName,locale);
	}

	/**
	 * public static ResourceBundle getResourceBundle(Locale localeValue) {
	 * 
	 * ResourceBundle resourceBundle = null; if
	 * (localeValue.getLanguage().equalsIgnoreCase("eng")) { resourceBundle =
	 * resourceBundle_Eng; } else { resourceBundle = resourceBundle_Chi; }
	 * return resourceBundle; }
	 */
	/**
	 * This method is no longer in use in Integral.
	 */
	public static ResourceBundle getResourceBundleOld(Locale localeValue) {

		ResourceBundle resourceBundle = null;
		if (localeValue.getLanguage().equalsIgnoreCase("eng")) {
			resourceBundle = resourceBundle_Eng;
		} else {
			resourceBundle = resourceBundle_Chi;
		}
		return resourceBundle;
	}

	/**
	 * 
	 * @param locale
	 * @return
	 */
	public static ResourceBundle getResourceBundle(Locale locale) {
		LOGGER.info("Locale: {}", locale.getCountry());//IJTI-1499
		ResourceBundle res = null;
		try {
			res = ResourceBundle.getBundle("com.bundle.Integral_Life", locale);
		} catch (Exception ex) {
			res = ResourceBundle.getBundle("com.bundle.Integral_Life", Locale.US);
		}
		return res;
	}
	private ResourceBundle getResourceBundleNew(Locale locale) {
		LOGGER.info("Locale: {}Company: {}", locale.getCountry(), company);//IJTI-1499
		ResourceBundle res = null;
		try {
		if(company.equals("7"))
			res = ResourceBundle.getBundle("com.bundle.Integral_Takaful", locale);
		else
			res = ResourceBundle.getBundle("com.bundle.Integral_Life", locale);
		} 
		catch (Exception ex) {
			if(company.equals("7"))
				res = ResourceBundle.getBundle("com.bundle.Integral_Takaful", Locale.US);
			else
				res = ResourceBundle.getBundle("com.bundle.Integral_Life", Locale.US);
		}

		return res;
	}

	// public static ResourceBundle getResourceBundle(String locale) {
	// return ResourceBundle.getBundle("com.bundle.Integral_Life", new
	// Locale(locale));
	// }
	/**
	 * 
	 * @param locale
	 * @return
	 */
	public static ResourceBundle getResourceBundletitle(Locale locale) {
		LOGGER.info("Locale: {}", locale.getCountry());//IJTI-1499
		ResourceBundle res = null;
		try {
			res = ResourceBundle.getBundle("com.bundle.Integral_Title_Life", locale);
		} catch (Exception ex) {
			res = ResourceBundle.getBundle("com.bundle.Integral_Title_Life", Locale.US);
		}
		return res;
	}

	public static ResourceBundle getResourceBundletitleNew(Locale locale) {
		LOGGER.info("Locale: {}Company: {}", locale.getCountry(), company);//IJTI-1499
		ResourceBundle res = null;
		try {
			if(company.equals("7"))
				res = ResourceBundle.getBundle("com.bundle.Integral_Title_Takaful", locale);
			else
				res = ResourceBundle.getBundle("com.bundle.Integral_Title_Life", locale);
			} 
			catch (Exception ex) {
				if(company.equals("7"))
					res = ResourceBundle.getBundle("com.bundle.Integral_Title_Takaful", Locale.US);
				else
					res = ResourceBundle.getBundle("com.bundle.Integral_Title_Life", Locale.US);
			}		
		return res;
	}
	/**
	 * 
	 * @param locale
	 * @return
	 */
	public static ResourceBundle getResourceBundleMenu(Locale locale) {
		LOGGER.info("Locale: {}", locale.getCountry());//IJTI-1499
		ResourceBundle res = null;
		try {
			res = ResourceBundle.getBundle("com.bundle.Integral_Menu_Life", locale);
		} catch (Exception ex) {
			res = ResourceBundle.getBundle("com.bundle.Integral_Menu_Life", Locale.US);
		}
		return res;
	}

	/**
	 * Added to get html labels from resource bundle.
	 * @param key
	 * @return
	 */

	private String gettingValueFromBundleHelper(ResourceBundle rb, String key) {
		String returnValue = "";
		String bundleKey = "";
		try {
			bundleKey = (this.screenName + "." + key.trim().replaceAll(" ", "_").trim());
			returnValue = rb.getString(bundleKey);
		} catch (MissingResourceException mre) {
			LOGGER.debug("Label not found for Screen:", mre); // IJTI-1723
			try {
				returnValue = ((rb.getString(key.trim().replaceAll(" ", "_"))).trim());
			} catch (MissingResourceException mre1) {
				LOGGER.debug("Label not found:", mre1); // IJTI-1723
				returnValue = null;
			}
		}
		return returnValue;
	}
	

	public String gettingValueFromBundle(String key) {
		String value = null;
		/*
		 * For Commons Screens (FSU, Smart & Diary)
		 */
		value = gettingValueFromBundleHelper(resourceBundleCommons, key);

		/*
		 * For life Screens
		 */
		if (value == null) {
			value = gettingValueFromBundleHelper(resourceBundle, key);
		}

		/*
		 * If not found, the key will be used
		 */
		if (value == null) {
			value = key;
		}

		return value;
	}
	

	
	/**
	 * Added to getting html labels from resource bundle.
	 * @param clazz
	 * @param key
	 * @return
	 */
	public StringData gettingValueFromBundle(Class clazz, String key) {
		try {
			return new StringData(resourceBundle.getString(this.screenName
					+ "." + key.trim().replaceAll(" ", "_")));
		} catch (MissingResourceException mre) {
			try {
				return new StringData(resourceBundle.getString(key.trim().replaceAll(" ", "_")));
			} catch (MissingResourceException mre1) {
				return new StringData(key);
			}
		}
	}

	/**
	 * Added to get title labels from resource bundle.
	 * @param key
	 * @return
	 */
	public String gettingValueFromBundleforTitle(String key) {
		String value = "";
		try {

			return resourceBundleTitle.getString(this.screenName + "."
					+ key.trim().replaceAll(" ", "_"));
		} catch (MissingResourceException mre) {
			try {
				return resourceBundleTitle.getString(key.trim().replaceAll(" ",
						"_"));
			} catch (MissingResourceException mre1) {
				try {
					String[] strkey = key.split(" ");
					for (int i = 0; i < strkey.length; i++) {
						value = value
								+ resourceBundleTitle.getString(strkey[i]
										.trim()) + " ";

					}
					return value;
				} catch (Exception e) {
					return key;
				}
			}
		}
	}

	
	public String gettingValueFromBundleforCommonTitle(String key) {
		String value = null;

		/*
		 * For Commons Screens (FSU, Smart & Diary)
		 */

			value = gettingValueFromBundleforTitleHelper(resourceBundleTitleCommons, key);

		/*
		 * If not found, the key will be used
		 */

		if (value == null) {
			value = key;
		}

		return value;
	}
	
	private String gettingValueFromBundleforTitleHelper(ResourceBundle rb, String key) {
		String value = "";
		String returnValue = "";
		
		try {
			returnValue= rb.getString(this.screenName + "." + key.trim());

		} catch (MissingResourceException mre) {
			try {
				returnValue= rb.getString(key.trim());

			} catch (MissingResourceException mre1) {

				try {
					String[] strkey = key.split(" ");

					for (int i = 0; i < strkey.length; i++) {
						value = value + rb.getString(strkey[i].trim()) + " ";
					}

					returnValue= value.trim();

				} catch (Exception e) {
					LOGGER.info("Exception:",e);
					returnValue= null;
				}
			}
		}
		return returnValue;
	}
	/**
	 * Added to get menu labels from resource bundle.
	 * @param key
	 * @return
	 */
	public String gettingValueFromBundleforMenu(String key) {
		try {
			return resourceBundleMenu
					.getString(key.trim().replaceAll(" ", "_"));
		} catch (MissingResourceException mre) {
			return key;
		}
	}

	/**
	 * Added to get labels from resource bundle.
	 * @param key
	 * @return
	 */
	public String gettingValueFromBundleForLabel(String key) {
		try {
			String s = key.trim();
		
			String k = resourceBundle.getString(s.replaceAll(" ", "_"));
			return k;
		} catch (MissingResourceException mre1) {
			LOGGER.info("MissingResourceException:",mre1);
			return key;
		}
	}

}
