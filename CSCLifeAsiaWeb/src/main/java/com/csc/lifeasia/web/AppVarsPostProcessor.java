package com.csc.lifeasia.web;

import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;

public interface AppVarsPostProcessor {
	
	public void process(LifeAsiaAppVars av);

}
