package com.csc.lifeasia.web;

import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;

public final class AppVarsBuilder {
	
	private AppVarsBuilder() {
		super();
	}

	/**
	 * A convenient method to create new instance of LifeAsiaAppVars, with possibility of post processing
	 * 
	 * @param appName
	 * @param postProcessor
	 * @return
	 */
	public static LifeAsiaAppVars getLifeAsiaAppVars(String appName, AppVarsPostProcessor postProcessor) {
		LifeAsiaAppVars av = new LifeAsiaAppVars(appName);
		
		if (postProcessor != null) {
			postProcessor.process(av);
		}
		
		return av;
	}
}
