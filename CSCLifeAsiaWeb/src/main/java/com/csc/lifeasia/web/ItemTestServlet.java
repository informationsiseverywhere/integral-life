package com.csc.lifeasia.web;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.quipoz.framework.util.AppConfig;

public class ItemTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static VpmsTcpSessionFactory sessionFactory = new VpmsTcpSessionFactory();
	//private static IVpmsBaseSessionFactory sessionFactory = new VpmsJniSessionFactory();
	static IVpmsBaseSession session;
	String file =  AppConfig.getVpmsModelDirectory()+"LANGUAGE/INTLIFE.VPM";
	VpmsComputeResult result;
      
	public ItemTestServlet() {
		this.sessionFactory.setIp_addr(AppConfig.getVpmsXeServerIp());
		this.sessionFactory.setIp_port(AppConfig.getVpmsXeServerPort());
	}
	
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			session = sessionFactory.create(file);
			
			String language  = (String) request.getParameter("language");
			String company = (String) request.getParameter("company");
			String lookUp = (String) request.getParameter("sel");
			String tableName  = (String) request.getParameter("tableName");
			String order = (String) request.getParameter("order");
			String item = (String) request.getParameter("item");
			String colName = (String) request.getParameter("colName");
			
			session.setAttribute("Input TransEffDate","20180928");
			session.setAttribute("Input Calling System","Base");
			session.setAttribute("Input Contract Type","");
			session.setAttribute("Input Region","");
			session.setAttribute("Input Locale","");
			session.setAttribute("Input Language",language);
			session.setAttribute("Input Company",company);
			session.setAttribute("Input LookupType",lookUp);
			session.setAttribute("Input Table",tableName);
			session.setAttribute("Input Sort",order);
			session.setAttribute("Input Item",item);
			session.setAttribute("Input Column",colName);
			
			System.out.println("Test AV =" + session.compute("Input AV").getResult());
			result = session.computeUsingDefaults("Input AV");
			System.out.println("MESSAGE="+result.getMessage());
	        System.out.println("RESULT="+result.getResult());
	        System.out.println("RETCODE="+result.getReturnCode());
	        System.out.println("STRING="+result.toString());
	        System.out.println("Fatal Error="+result.isFatalError());

	        request.setAttribute("company", company);
	        request.setAttribute("language", language);
	        request.setAttribute("tableName", tableName);
	        request.setAttribute("item",item);
	        request.setAttribute("order", order);
		    request.setAttribute("colName", colName);
		    
		    if(lookUp.equals("D")){
		    	String desc =  result.getResult();
		    	if(colName.equals("")){
		    		request.setAttribute("allData", desc);
		    	}
		    	else {
		    		int exclamation = desc.indexOf("!");
			    	String data = desc.substring(exclamation+1, desc.length()-2);
			    	request.setAttribute("columnValue", data);
		    	}
		    	request.setAttribute("sel", lookUp);
		    }
		    else if(lookUp.equals("B")){
		    	String desc =  result.getResult();
		    	if(item.equals("")){
		    		request.setAttribute("allDescB", desc);
		    	}
		    	else {
		    		int exclamation = desc.indexOf("!");
			    	int pipe = desc.indexOf("||");
			    	String shortdesc=desc.substring(exclamation+1, pipe);
			    	String longdesc=desc.substring(pipe+2,desc.length()-1);
			    	request.setAttribute("shortDesc", shortdesc);
			    	request.setAttribute("longDesc", longdesc);
		    	}
		    	request.setAttribute("sel", lookUp);
		    } 
		    else if(lookUp.equals("S")){
		    	String desc =  result.getResult();
		    	if(item.equals("")){
		    		request.setAttribute("allDescS", desc);
		    	}
		    	else {
			    	int exclamation = desc.indexOf("!");
			    	String shortdesc=desc.substring(1, exclamation);
			    	request.setAttribute("shortDesc", shortdesc);
		    	}
		    	request.setAttribute("sel", lookUp);
		    } 
		    else if(lookUp.equals("L")){
		    	String desc =  result.getResult();
		    	if(item.equals("")){
		    		request.setAttribute("allDescL", desc);
		    	}
		    	else {
		    		int exclamation = desc.indexOf("!");
			    	String longdesc=desc.substring(exclamation+1,desc.length()-1);
			    	request.setAttribute("longDesc", longdesc);
		    	}
		    	request.setAttribute("sel", lookUp);
		    }
		    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/tableData.jsp");
		    requestDispatcher.forward(request, response);
	        
	       
		} catch (VpmsLoadFailedException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
