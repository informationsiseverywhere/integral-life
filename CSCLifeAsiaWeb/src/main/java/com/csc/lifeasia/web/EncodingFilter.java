package com.csc.lifeasia.web;

import javax.servlet.*;
import java.io.*;

public class EncodingFilter implements Filter{
	private String encoding = "";
	public EncodingFilter(){
		
	}
	public void init(FilterConfig config){
		encoding = config.getInitParameter("encoding");
		if(encoding==null || encoding.equals(""))
			encoding="UTF-8";
	}
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	throws ServletException,IOException{
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
	}
	public void destroy(){
		
	}
}
