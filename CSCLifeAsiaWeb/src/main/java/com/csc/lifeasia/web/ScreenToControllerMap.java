package com.csc.lifeasia.web;

import java.util.HashMap;
import java.util.Map;

import com.csc.smart400framework.SmartHttpScreenController;
import com.quipoz.framework.screencontrol.HTTPScreenController;

public class ScreenToControllerMap {

	public  static final Map screenToObjectMap = new HashMap();

	/**
	 * Obtains a new screenController instance.
	 * 
	 * @param screenName String
	 * @return controller instance
	 * @exception java.lang.ClassNotFoundException
	 * @exception java.lang.InstantiationException
	 * @exception java.lang.Exception
	 * @exception java.lang.IllegalAccessException
	 */
	public static HTTPScreenController getScreenController(String screenName) throws Exception {
		HTTPScreenController o = com.quipoz.framework.webcontrol.ScreenToControllerMap.getScreenController(screenName);

		if (o == null) {
			o = new SmartHttpScreenController(screenName);
		}

		return o;
	}
}
