package com.csc.lifeasia.web;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;
import com.properties.PropertyLoader;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.job.JobUtils;
import com.quipoz.framework.exception.WebServerException;
import com.quipoz.framework.screencontrol.ErrorScreenController;
import com.quipoz.framework.screencontrol.HTTPScreenController;
import com.quipoz.framework.screencontrol.TimeoutScreenController;
import com.quipoz.framework.sessionfacade.GeneralSessionDelegate;
import com.quipoz.framework.util.Action;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.RequestParms;
import com.quipoz.framework.util.WebUtil;
import com.quipoz.framework.util.log.QPLogger;
import com.quipoz.framework.webcontrol.ControllerServlet;

/**
 * Main application Servlet.
 * <p>
 * Controls
 * <ul>
 * <li>error handling</li>
 * <li>jsp re-direction</li>
 * <li>screen chaining</li>
 * </ul>
 * processes all request from the jsp pages<br>
 * Copied from PolisyAsiaWeb
 *
 * @author Max Wang(Quipoz)
 * @version 1.0
 */
public class LifeAsiaControllerServlet extends ControllerServlet {
    /**
     * Logger for this class
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LifeAsiaControllerServlet.class);

	/** Generated ID */
	private static final long serialVersionUID = 5091720752685133758L;
	private static final String ROUTINE = QPUtilities.getThisClass();
	private static QPLogger logger = QPLogger.getQPLogger(LifeAsiaControllerServlet.class);
	private static long lastChecked = 0;
	private static long timetowait = 30000;
	private static CheckForExpiredSessions cfe = null;
	private static Thread runcfe = null;

	/**
	 *
	 */
	@Override
	public void finalize() {
		LOGGER.info("finalize() - Finalize on {} called.", ROUTINE);//IJTI-1499
		lastChecked = 0;
		try {
			super.finalize();
		} catch (Throwable e) {
			LOGGER.info("finalize() - Finalize on "  + " called.",e);
			
		}
	}

	/**
	 * Get screen controller by given screenName
	 *
	 * @see ControllerServlet#getScreenController(String)
	 */
	@Override
	protected HTTPScreenController getScreenController(String screenName) throws ClassNotFoundException, InstantiationException, Exception {
		return ScreenToControllerMap.getScreenController(screenName);
	}

	/**
	 * Overridden doGet. We don't need functionality provided in base class.
	 * <p>
	 * Update 8 April. possibly, but it is still a good idea to clean out the session. Otherwise, if the app has failed,
	 * we can't get back in.
	 *
	 * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse);
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		BaseModel bm = (BaseModel) req.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
		if (bm != null) {
			/* Check for mostly but not fully cleaned up situation */
			// Fix concurrent users
			//AppVars av = bm.getApplicationVariables();
			AppVars av = AppVars.getInstance();
			if (av != null && av.getAppConfig() != null) {
				HTTPScreenController hsc = null;
				ErrorScreenController sc = new ErrorScreenController();
				sc.setRequest(req);
				sc.setSession(req.getSession());
				sc.process(Action.SCREEN_INIT, WebServerException.SESSION_OUT_OF_SYNC);
				hsc = sc;
				javax.servlet.RequestDispatcher rd = getServletConfig().getServletContext().getRequestDispatcher(hsc.getJsp());
				if (rd != null) {
					rd.forward(req, resp);
				}
				return;
			}
			else {
				Enumeration en = req.getSession().getAttributeNames();
				while (en.hasMoreElements()) {
					String s = (String) en.nextElement();
					req.getSession().removeAttribute(s);
				}
			}
		}
		doPost(req, resp);
	}

	/**
	 * @param req
	 */
	private static synchronized void implementCleaner(HttpServletRequest req) {
		try {
			//timetowait = ((BaseModel) req.getSession().getAttribute(BaseModel.SESSION_VARIABLE)).getApplicationVariables().getAppConfig().userHeartbeat;
			timetowait = AppVars.getInstance().getAppConfig().userHeartbeat;
			lastChecked = System.currentTimeMillis();
		}
		catch (Exception e) {
			LOGGER.debug("Couldn't implement the daemon, ", e);//IJTI-1499
			return;
		}
		cfe = new CheckForExpiredSessions();
		runcfe = new Thread(cfe);
		runcfe.start();
	}

	/**
	 * Over ridden do process to capture special urls to logoff or to clear session and start again.
	 *
	 * @see ControllerServlet#doProcess(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public void doProcess(HttpServletRequest req, HttpServletResponse resp) throws WebServerException {

		/* The first user who logs on will start the daemon that checks
		 * to see if there are any expired saessions. We need a user to do
		 * it, otherwise we don't have access to the timeout value from
		 * config.
		 */
		if (lastChecked == 0) {
			implementCleaner(req);
		}
		Map< String,String> excelHeadVarList = new LinkedHashMap<String,String>();
		String headerName = req.getParameter("sflheaderList");
		String sflVarName = req.getParameter("sflVarList");
		excelHeadVarList.put("SFLHEADERLIST",headerName);
		excelHeadVarList.put("SFLVARLIST",sflVarName);
		AppVars.getInstance().setSflHeadVarList(excelHeadVarList);
		//if we're doing a request to the scheduling or logging off intercept it here otherwise do super.doProcess
		String url = req.getRequestURI();
		String otherurl = (String) req.getSession().getAttribute("LoginFilterAction");
		String action = req.getParameter(RequestParms.ACTION);
		if (otherurl != null && otherurl.equals("home")) {
			if (action != null && !action.equals(Action.SCREEN_INIT) ) {
				LOGGER.info("doProcess(HttpServletRequest, HttpServletResponse) - Everything reset");
				url = "home";
			}
			req.getSession().setAttribute("LoginFilterAction", null);
		}

		if (url.endsWith("logoff")) {

			try {
				AppVars.addStaticDiagnostic("Opening page");
					WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher("/ibm_security_logout?logoutExitPage=/redirect.jsp"), req, resp);
				return;
			}
			catch (Exception ioe) {
				WebServerException.process(ioe);
			}
		}
		else if (url.endsWith("home")) {
			LOGGER.info("doProcess(HttpServletRequest, HttpServletResponse) - Going home!");
			BaseModel bm = (BaseModel) req.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
			GeneralSessionDelegate gd = (GeneralSessionDelegate) req.getSession().getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
			try {
				// YG: Defect 134c(Home icon). Refresh session bean, instead of strip it off
				//CCH - force to timeout if no BaseModel. Case identified is when
				//the HTTP session times out.
				if (bm == null) {
					AppVars.addStaticDiagnostic("Opening page");
					WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher(TimeoutScreenController.WEBAPP_Timeout_PAGE), req, resp);
					return;
				}
				if (gd == null) {
					AppVars.addStaticDiagnostic("Opening page");
					WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher(TimeoutScreenController.WEBAPP_Timeout_PAGE), req, resp);
				}
				gd.processAction(bm, GeneralSessionDelegate.ACTION_TEST);
				if (bm != null && bm.getScreenModel() != null) {
					bm.getScreenModel().setScreenName("       ");
				}
				AppVars.addStaticDiagnostic("Opening page");
				WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher("/splash.jsp"), req, resp);
				return;
			}
			catch (Exception ioe) {
				String errorMessage = ioe.getMessage();
				if (errorMessage != null && errorMessage.indexOf("CORBA OBJECT_NOT_EXIST") != -1) {
					try {
						AppVars.addStaticDiagnostic("Opening page");
						WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher(TimeoutScreenController.WEBAPP_Timeout_PAGE), req, resp);
						return;
					}
					catch (Exception e) {
						WebServerException.process(e);
					}
				}
				WebServerException.process(ioe);
			}
		}
		else {
			super.doProcess(req, resp);
		}
	}

	/**
	 * @see com.quipoz.framework.webcontrol.ControllerServlet#checkLogon(String, HttpServletRequest, boolean)
	 * This version does nothing; we're using Websphere/Windows logon.
	 * However, this would be the point for Quipoz based logon.
	 */
	public String checkLogon(String nextScreen, HttpServletRequest req, boolean logonReq) throws WebServerException {
		return nextScreen;
	}

	/**
	 * @see com.quipoz.framework.webcontrol.ControllerServlet#notLoggingOff(String)
	 */
	public boolean notLoggingOff(String action) {
		return !action.equals("QPGENLogoff");
	}

	/** (non-Javadoc)
	 * @see com.quipoz.framework.webcontrol.ControllerServlet#initVariables(javax.servlet.http.HttpServletRequest)
	 */
	public BaseModel initVariables(final HttpServletRequest req) throws WebServerException {
		BaseModel bm = new BaseModel();
		       // ilife-3192 

				LifeAsiaAppVars appVars = (LifeAsiaAppVars) AppVars.getInstance();

				String user = (String) req.getSession().getAttribute("USERID");
              // ilife- 3192
				/*
				 * It was assigning hard coded user name; Now it is updated to read ApplicationUse12 in QuipozCfg.xml;
				 * Get use name from there.
				 */
				if (user == null) {
					// There is configuration for testing.
					if (appVars.ApplicationUse12 != null) {
						user = appVars.ApplicationUse12.split("/")[0];
					} else {
						// No user is configed for testing.
						user = "";
					}
				}
				String userProfile = user;

				appVars.setLoggedOnUser(user);
				appVars.setUserProfile("USERPRF", userProfile);
				appVars.setUserProfile("DESCRIPTION", user);

				// set remote ip address as part of userprofile
				appVars.setUserProfile(LifeAsiaAppVars.REMOTE_IP_ADDR, req.getRemoteAddr());

				LOGGER.debug("Subpath now '{}'", appVars.getAppConfig().subPath);//IJTI-1499

				// initialise and set jobInfo object
				String jobNumber = JobUtils.getJobNumber();
				JobInfo job = new JobInfo(jobNumber, user, new Date(), user);
				appVars.setJobinfo(job);

				String terminalId = JobUtils.getTerminalId();
				appVars.setTerminalId(terminalId);
			

		

		//bm.setApplicationVariables(appVars);
		req.getSession().setAttribute(BaseModel.SESSION_VARIABLE, bm);
		return bm;
	}

	public String toString() {
		return QPUtilities.dumpClass(this);
	}

	private static class CheckForExpiredSessions implements Runnable {

		public boolean runme = true;

		public void run() {
			LOGGER.debug("The I90 Conversational Servlet CheckForExpiredSessions daemon has started running. Cycle time is {}", timetowait);//IJTI-1499
			while (runme) {
				lastChecked = System.currentTimeMillis();
				LOGGER.debug("CheckForExpiredSessions checking for expired sessions.");
				try {
					SessionCleaner1.lookForExpiredSessions();
				}
				catch (Exception e) {
					LOGGER.error("run()", e);
				}
				waitabit();
			}
			lastChecked = 0;
			LOGGER.debug("I90 Conversational Servlet CheckForExpiredSessions daemon CheckForExpiredSessions has terminated.");
		}

		private synchronized void waitabit() {
			try {
				this.wait(timetowait);
			}
			catch (InterruptedException e) {
			}
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Servlet#destroy()
	 */
	@Override
	public void destroy() {
		LOGGER.info("destroy() - The I90 Conversational Servlet is being taken out of service.");
		if (runcfe != null) {
			cfe.runme = false;
			//runcfe.notify();
			runcfe = null;
			LOGGER.info("destroy() - The I90 Conversational Servlet daemon has been told to shut down.");
		}
		SessionCleaner1.lookForExpiredSessions();
		lastChecked = 0;
		super.destroy();
		LOGGER.info("destroy() - I90 Conversational Servlet shudown complete.");
	}
	public String getLanguageCode(HttpServletRequest req)
	{
		return PropertyLoader.getLanguageCode(req.getLocale().toString());
	}
	/**
	 * Added to fetch the language from the properties file for Integral i18n
	 */
	public String getUserLanguage(HttpServletRequest req)
	{
		 return PropertyLoader.getFolderName(req.getLocale().toString());

}

	public String getLanguageCode(String loc) {
		return PropertyLoader.getLanguageCode(loc);
	}

	public String getUserLanguage(String loc) {
		return PropertyLoader.getFolderName(loc);
	}
}
