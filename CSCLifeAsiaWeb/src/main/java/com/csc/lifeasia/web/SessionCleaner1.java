package com.csc.lifeasia.web;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.context.IntegralApplicationContext;
import com.csc.integral.session.IntegralJdbcSessionFactory;
import com.csc.lifeasia.SessionAppVarsStore;
import com.quipoz.framework.sessionfacade.GeneralSessionDelegate;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.QPUtilities;

/**
 * Class to clean up resources on a session timeout. This is important since
 * we're using stateful session beans that don't timeout for an hour. In
 * here if we lose a session we'll get the session bean out and call remove
 * on it.
 */
public class SessionCleaner1 implements HttpSessionListener {
	
	/**
     * Logger for this class
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionCleaner1.class);
	public static final String ROUTINE = QPUtilities.getThisClass();

	// Class variables to keep track of the number of active sessions
	//
	private static int no_sessions_created = 0;
	private static int no_sessions_destroyed = 0;
	private static int no_sessions_active = 0;
	private static List<HttpSession> knownSessions = new ArrayList<>(100);

	public void sessionCreated(HttpSessionEvent sessionEvent) {
		no_sessions_created++;
		no_sessions_active++;
		sessionEvent.getSession().setAttribute("LoginFilterAction", "home");
		LOGGER.info("HTTP Session Listener has detected the creation of a new session.");
		LOGGER.info("There are now {} active HTTP sessions, knownactive={}", no_sessions_active, knownSessions.size());//IJTI-1499
		/* Add this session to the list in a synchronised way */
		addSession(sessionEvent.getSession());
	}
	
	/** 
	 * Walk through all known sessions, to find any that should be
	 * done away with because they have timed out.
	 */
	public static void lookForExpiredSessions() {
		HttpSession sess = null;
		for (int i=0; i<knownSessions.size(); i++) {
			try {
				sess = (HttpSession) knownSessions.get(i);
				// ILIFE-3390 : Begins
				String sessionId = sess.getId();


				AppVars av = SessionAppVarsStore.get(sessionId);

				if(av == null)
				{
					deleteSession(sess);
					LOGGER.info("lookForExpiredSessions() - Error accessing appVars in Session for sessionId:{},"
							+ " hence removing it.", sessionId);//IJTI-1499
					continue;
				}
				// ILIFE-3390 : Ends
				long then = av.getHeartBeat();
				long now = System.currentTimeMillis();
				long msecs = now - then;
				LOGGER.debug("AppVars {} Comparing time={} to max={}", av.hashCode(), msecs, (2 * av.getAppConfig().userHeartbeat));//IJTI-1499
				if (msecs >= 2 * av.getAppConfig().userHeartbeat) {
					LOGGER.info("Sessioncleaner found a session to kill");
					sess.invalidate();
					LOGGER.info("And killed it.");
				}
			}
			catch (Exception e) {
				Integer checked = (Integer) sess.getAttribute("CheckCount");
				if (checked == null) {
					checked = new Integer(1);
				}
				else {
					checked = checked + 1;
				}
				sess.setAttribute("CheckCount", checked);
				if (checked > 3) {
					LOGGER.error("lookForExpiredSessions() - Error accession session " + e + ", removing it", e);
					deleteSession(sess);
				}
			}
		}

	}

	/**
	 * @param sessionEvent
	 */
	private synchronized void addSession(HttpSession session) {
		knownSessions.add(session);
	}

	/**
	 * @param sessionEvent
	 */
	private static synchronized void deleteSession(HttpSession session) {
		try {
			for (int i=0; i<knownSessions.size(); i++) {
				if (session == knownSessions.get(i)) {
					knownSessions.remove(i);
					return;
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("deleteSession(HttpSession) - Error accession session " + e, e);
		}
	}
	
	public void finalize() {
		try {
			LOGGER.info("finalize() - Finalize on {} called.", ROUTINE);//IJTI-1499
			super.finalize();
		}
		catch (Throwable e) {
			/* Why this fails I dunno, but it causes constant "breaks" in
			 * debugging which is annoying. No problem in normal runs. */
			LOGGER.error("Error occured during finalize ",e);
		}
	}

	/**
	 * clean up any resources....
	 */
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {

		no_sessions_destroyed++;
		no_sessions_active--;
		LOGGER.info("HTTP Session Listener has detected the destruction of a session.");
		LOGGER.info("There are now {} active HTTP sessions.", no_sessions_active);//IJTI-1499

		AppVars.addStaticDiagnostic("HSessionCleaner cleaning session....." + sessionEvent.getSession().getMaxInactiveInterval());
				
		HttpSession session = sessionEvent.getSession();
		
		SessionAppVarsStore.remove(session.getId());
		knownSessions.remove(session);
		/* Tell the bean on the other side of the EJB layer that its services
		 * are no longer required.
		 */
		GeneralSessionDelegate gd = null;//IBPTE-459
		BaseModel bm = null;//IBPTE-459
		try {
			gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
			if (gd != null) {
				bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
				if (bm != null) {
					Long uniq = (Long)sessionEvent.getSession().getAttribute(IntegralJdbcSessionFactory.USER_INF_KEY);
					if(uniq != null)
					{
						IntegralJdbcSessionFactory ja = IntegralApplicationContext.getApplicationContext().getBean("sessionFactory",IntegralJdbcSessionFactory.class);
						ja.updateUserId((uniq));						
					}					
				}
			}
			else {
				LOGGER.debug("Error cleaning up the EJB session, gd is null");
			}
		}
		catch (Exception e) {
			LOGGER.error("sessionDestroyed(HttpSessionEvent)", e);
            LOGGER.debug("Error ", e);//IJTI-1499
            LOGGER.debug(" cleaning up the EJB session");//IJTI-1499
		} finally {
			// IBPTE-459 starts
			// If exception occurs, cleanup the session.
			if (gd != null && bm != null) {
				gd.cleanup(bm);
			}
			// IBPTE-459 ends
		}
		
		deleteSession(session);
	}
	
	
	public static void deleteLimitSession(String sessionId)
	{
		for (int i=0; i<knownSessions.size(); i++) {
			if (sessionId.equals(((HttpSession)knownSessions.get(i)).getId())) {
				((HttpSession)knownSessions.get(i)).invalidate();
				break;
			}
		}
	}

	public void sendNtifictnMessage(String sessionID, String msg){
		ArrayList list = new ArrayList(knownSessions); 
		Iterator it= list.iterator();
		while(it.hasNext()){
			HttpSession session = (HttpSession)it.next();
			if(!sessionID.equalsIgnoreCase(session.getId())){
					 
					AppVars appVarsLcl = new  SessionAppVarsStore().get(session.getId());
					if(appVarsLcl!=null && appVarsLcl.getSessionId()!=null){
					appVarsLcl.setHoldMsgNeeded(true);
					appVarsLcl.setHoldDateTm(System.currentTimeMillis());
					appVarsLcl.setNotifyMessage(msg);
				}
			}
		}	 
	}
		
	//used for logging off al users except the admin himself.
	public void clearAllSessions(String sessionID){
		ArrayList list = new ArrayList(knownSessions); 		
		Iterator it= list.iterator();
		while(it.hasNext()){			
				HttpSession session = (HttpSession)it.next();			
				if(!sessionID.equalsIgnoreCase(session.getId())){
					SessionAppVarsStore.remove(session.getId());
					deleteLimitSession(session.getId().toLowerCase());	//ILIFE-5642				
				}			
		}	 
	}
	
	//used for stop sending notifications
	public void stopNotifications(){
		ArrayList list = new ArrayList(knownSessions); 		
		Iterator it= list.iterator();
		while(it.hasNext()){			
				HttpSession session = (HttpSession)it.next();
				
				AppVars appVarsLcl =  new  SessionAppVarsStore().get(session.getId());
				if(appVarsLcl!=null && appVarsLcl.getSessionId()!=null){
				appVarsLcl.setHoldMsgNeeded(false);		}
			
		}	 
	}
	/* INTEGRAL IPB-252  END*/
	/* INTEGRAL IPB-253  START*/
	public void clearSelectedSessions(List<String> killSessionList){
		Iterator<String> it= killSessionList.iterator();
		while(it.hasNext()){
			try{
				String sessionID = it.next();			
					SessionAppVarsStore.remove(sessionID.trim());//IJTI-1419
					deleteLimitSession(sessionID.trim().toLowerCase());
    				//usrinfoDao.invalidateSessions(killSessionList);
			}
			catch(Exception e){
				LOGGER.info("error in clearallsessions");
			}
		}	 
	}
	public void sendSelectedNtifictnMessage(List<String> remindMessageList, String notifyMessage) {
		Iterator<String> it = remindMessageList.iterator();
		while (it.hasNext()) {
			String sessionID = it.next();
			AppVars appVar = SessionAppVarsStore.get(sessionID.trim());//IJTI-1419
			if (null != appVar) {
				appVar.setHoldMsgNeeded(true);
				appVar.setHoldDateTm(System.currentTimeMillis());
				appVar.setNotifyMessage(notifyMessage);
			}
		}
	}
}
