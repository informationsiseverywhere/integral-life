package com.csc.lifeasia.web;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.impl.XSStringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.web.util.HtmlUtils;

import com.csc.integral.context.IntegralApplicationContext;
import com.csc.integral.session.IntegralJdbcSessionFactory;
import com.csc.ldap.Ldap;
import com.csc.lifeasia.SessionAppVarsStore;
import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.dao.impl.BudbwpfDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.HldusrDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.UsrinfDAOImpl;
import com.csc.smart400framework.dataaccess.model.Budbwpf;
import com.csc.smart400framework.dataaccess.model.Hldusr;
import com.csc.smart400framework.dataaccess.model.Usrinf;
import com.ia.framework.dialect.FrameworkDialectFactory;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.job.JobUtils;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.IntegralConfig;
import com.quipoz.framework.screencontrol.HTTPScreenController;

/**
 * The first servlet users access. To show the different logon pages according
 * to the language configuration in config file.
 *
 * @author Wayne Yang 2010-04-28
 */
public class FirstServlet extends HttpServlet {

	/**
     * Logger for this class
     */
    private static final Logger LOGGER;
    private static final String TENANT="tenant";//IJTI-1534
    
    public void destroy() {
        super.destroy();
    }
    
    public void init() throws ServletException {
    }
    
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) {

		// Find the config file
        final String path = System.getProperty("Quipoz.LifeAsiaWeb.XMLPath");
        if (path == null) {
            final String s = "Quipoz.LifeAsiaWeb.XMLPath is not set. \n";
            throw new RuntimeException(s);
        }
        // IBPTE-810 Start
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SAMLCredential credential = null;
        String principal = "";
        String idpId = "";
              
        if (authentication.getCredentials() instanceof SAMLCredential) {
        	final LifeAsiaAppVars appVars = new LifeAsiaAppVars(this.getInitParameter("Appname"));
        	
        	credential = (SAMLCredential) authentication.getCredentials();
        	idpId = credential.getRemoteEntityID();
        	
        	if(idpId.contains("/cas/idp")) { //CAS5 IDP
        		principal = credential.getNameID().getValue(); 
        		appVars.setIdpName(AppVars.CAS); //IBPLIFE-9550
        	} else { //Other IDP such as OKTA
        		String federationId = credential.getNameID().getValue(); //IBPLIFE-9550
	        	Attribute attr = credential.getAttribute("Integral Life Id");
	        	List<XMLObject> xmlObjs = attr.getAttributeValues();
	        	XMLObject xmlObj = xmlObjs.get(0);
	        	principal = ((XSStringImpl) xmlObj).getValue();
	        	appVars.setIdpName(AppVars.IDP); //IBPLIFE-9550
	        	appVars.setFederationId(federationId); //IBPLIFE-9550
        	}
        	LOGGER.info("Identity provider: [{}] -- Principal: [{}]", idpId, principal);  
        
			// By pass log on if the user has already been authenticated by CAS server.
	        //if (request.getUserPrincipal() instanceof AttributePrincipal) {
	
			//LOGGER.info(" ============== FirstServlet tenant attribute: {}", request.getSession().getAttribute("tenant"));//IJTI-1534			
	
			// This code is a copy of the {@link LoginServlet#doPost(HttpServletRequest, HttpServletResponse)}
			// without the LDAP authentication.
	        //final AttributePrincipal principal = (AttributePrincipal)request.getUserPrincipal();
	        //FirstServlet.LOGGER.info("======================================================");
	        //FirstServlet.LOGGER.info("The incomming request has been already authenticated in CAS ...");
	        //FirstServlet.LOGGER.info("Principal's name is: {}", principal.getName());//IJTI-1499
	        //FirstServlet.LOGGER.info("Attributes of the principal are: {}", principal.getAttributes());//IJTI-1499
	        // IBPTE-810 End
        
        	//IJTI-1534 Starts
			String tenantId ="";                                                                     
			if (IntegralConfig.isMultiTenancyEnabled()) {

				if (request.getSession().getAttribute(TENANT) != null) {
					tenantId = HtmlUtils.htmlEscape((String) request.getSession().getAttribute(TENANT));
				} else {
					tenantId = "";
				}
			}
			//IJTI-1534 Ends
	       	
	        appVars.setContextPath(request.getContextPath().replaceFirst("/", ""));
	         // Fixed concurrents users
			if (IntegralConfig.isMultiTenancyEnabled()) {
					appVars.setTenantid(tenantId);// IJTI-1534
			}

			AppVars.setInstance((AppVars)appVars);

	        //final String longuserId = principal.getName();
			final String longuserId = principal;
	        String userId = UserMapingUtils.getShortUserIdValue(longuserId);
			/*ILIFE-5986 start*/
	         if (longuserId.trim().toUpperCase().equals(userId.trim().toUpperCase())) {//ILIFE-8402
	            userId = userId.trim().toUpperCase();
	        }
			/*ILIFE-5986 end*/
	        final String lastLoginTime = "never";
	        String userDescription = "";
	        final boolean isAdmin = false;
			//ilife-3192
	        SessionAppVarsStore.set(request.getSession().getId(), (AppVars)appVars);
			// ilife-3192
				
				//ILIFE-7436 start
	        if (AppConfig.getInstance().getVersion().isEmpty()) {
	            String version = LifeAsiaAppVars.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	            final int i = version.indexOf(".jar");
	            final int j = version.lastIndexOf("csc-life-asia-app");
	            if (j >= 0 && i > j) {
	                version = version.substring(j + 18, i);
	                AppConfig.getInstance().version = version;
	            }
	            FirstServlet.LOGGER.info("Integral version: {}", (Object)version);
	        }
			//ILIFE-7436 end
	        
	        //IBPLIFE-9548 Start
	        if(AppVars.IDP.equals(appVars.getIdpName())) {
	        	try {
		        	Attribute attr = credential.getAttribute("User Description");
		        	List<XMLObject> xmlObjs = attr.getAttributeValues();
		        	XMLObject xmlObj = xmlObjs.get(0);
		        	userDescription = ((XSStringImpl) xmlObj).getValue();
	        	} catch (Exception e) {
	        		LOGGER.error("Cannot get user description from external idp");
	        		userDescription = longuserId;
	        	}
	        } else {
	        	final Ldap ldap = new Ldap();
				//lastLoginTime = updateUserInfo(userId);
				//userDescription = ldap.getUserDescription(userId + " ext").toString();
	        	userDescription = ldap.getUserDescription(userId).toString();
	        }
	        //IBPLIFE-9548 End
	        // Cache User description for next use
	        appVars.setUserDescription(userDescription);
	        
	        final String sessionId = request.getSession().getId();
	        final Usrinf usrinf = ((IntegralJdbcSessionFactory)IntegralApplicationContext.getApplicationContext().getBean("sessionFactory", (Class)IntegralJdbcSessionFactory.class)).isValid(userId, sessionId);
	        // IPB-254
			//ILIFE-8402 starts
			final Hldusr h1 = null;
	        final Hldusr hidu = ((HldusrDAOImpl)IntegralApplicationContext.getApplicationContext().getBean("hldusrDAO", (Class)HldusrDAOImpl.class)).getUserdetails(h1);
	        final HttpSession session = request.getSession();
	        session.setAttribute("LONGUSERID", (Object)longuserId);//ILIFE-5986
	        
	        if (hidu != null) {
	            final Budbwpf bud = ((BudbwpfDAOImpl)IntegralApplicationContext.getApplicationContext().getBean("budbwpfDAO", (Class)BudbwpfDAOImpl.class)).getBudbwRecordByUserId(userId);
	            if (bud != null && "1".equals(bud.getValidflag())) {
	                final boolean islogin = this.login(request, response, userId, lastLoginTime, userDescription, isAdmin, appVars, usrinf, session);
	                if (islogin) {
	                    return;
	                }
	            }
	            else {
	                final String alertMsg = "2";
	                session.setAttribute("msg1", (Object)alertMsg);
	                ((UsrinfDAOImpl)IntegralApplicationContext.getApplicationContext().getBean("usrinfDAO", (Class)UsrinfDAOImpl.class)).updateUserId(usrinf.getUniqueNumber());
	                String url = "/logon.jsp";
	                try {
	                    final String language = AppConfig.getInstance().getCurrentLanguage();
	                    if ("S".equalsIgnoreCase(language)) {
	                        url = "/logon_cn.jsp";
	                    }
	                }
	                catch (Exception e) {
	                    throw new RuntimeException(e);
	                }
	                try {
	                    this.getServletContext().getRequestDispatcher(url).forward((ServletRequest)request, (ServletResponse)response);
	                    return;
	                }
	                catch (IOException e2) {
	                    throw new RuntimeException(e2);
	                }
	                catch (ServletException e3) {
	                    throw new RuntimeException((Throwable)e3);
	                }
	            }
	        }
			// IPB-254
	        else {
	            session.setAttribute("msg1", (Object)null);
	            final boolean islogin2 = this.login(request, response, userId, lastLoginTime, userDescription, isAdmin, appVars, usrinf, session);
	            if (islogin2) {
	                return;
	            }
	        }
        } else {
        	LOGGER.error("Cannot get SAMLCredential");
        }
        
	    String url2 = "logon.jsp";
	    try {
	        final String language2 = AppConfig.getInstance().getCurrentLanguage();
	        if ("S".equalsIgnoreCase(language2)) {
	            url2 = "logon_cn.jsp";
	        }
	    }
	    catch (Exception e4) {
	        throw new RuntimeException(e4);
	    }
			
		// decide which page to show
	
	
	    try {
	        this.getServletContext().getRequestDispatcher(url2).forward((ServletRequest)request, (ServletResponse)response);
	    }
	    catch (IOException e5) {
	        throw new RuntimeException(e5);
	    }
	    catch (ServletException e6) {
	        throw new RuntimeException((Throwable)e6);
	    }
    }
    
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private boolean login(final HttpServletRequest request, final HttpServletResponse response, final String userId, String lastLoginTime, final String userDescription,final boolean isAdmin,  final LifeAsiaAppVars appVars, final Usrinf usrinf, final HttpSession session) {
        // Cache User description for next use
		appVars.setUserDescription(userDescription);  // IGB-1241
        SessionAppVarsStore.set(request.getSession().getId(), (AppVars)appVars);// IGB-1241
        if (usrinf.getLastlogonTime() != null) {
            lastLoginTime = new SimpleDateFormat().format(usrinf.getLastlogonTime());
        }
		// create user http session
		if(usrinf.getMessage().equals("1"))
		{
            SessionCleaner1.deleteLimitSession(usrinf.getSessionId());
            session.setAttribute("logoutSessionID", (Object)usrinf.getSessionId());
        }
        session.setAttribute("userInfKey", (Object)usrinf.getUniqueNumber());
        session.setAttribute("USERID", (Object)userId);
        session.setAttribute("userD", (Object)userDescription);
        session.setAttribute("LastLoginTime", (Object)lastLoginTime);
        //AWPL-Integral Integration related Changes starts
		//AWPL-Integral Integration is currently not used 
		//so commented the below AWPL code as there are security issues reported
     	
		final String source = request.getParameter("source");
        if (isAdmin) {
            session.setAttribute("ISSYSTEMADMIN", (Object)true);
            }
        else {
		//AWPL-Integral Integration related Changes end
        session.setAttribute("ISSYSTEMADMIN", (Object)false);
        }
		// initialize AppVar
        try {
			//IBPLIFE-4668 Starts
			final String ACTION = "action";
        	if(request.getParameter(ACTION)==null) {
        		this.getServletContext().getRequestDispatcher("/process?action_key=screenInit&screen=default&previousMenu=index.jsp").forward((ServletRequest)request, (ServletResponse)response);
        	}else if(request.getParameter(ACTION).equals("redirect")) {
    	        appVars.setJspConfigPath("jsp");
    	        appVars.setLoggedOnUser(userId);
    			appVars.setUserProfile("USERPRF", userId);
    			appVars.setUserProfile("DESCRIPTION", userId);
    			appVars.setUserProfile(SMARTAppVars.REMOTE_IP_ADDR, "UIUX Application");
        		String jobNumber = JobUtils.getJobNumber();
        		JobInfo job = new JobInfo(jobNumber, userId, new Date(), userId);
    			appVars.setJobinfo(job);
    			appVars.setTerminalId(UUID.randomUUID().toString());
    			AppVars.setInstance(appVars);
    			String browserLocale = appVars.getAppConfig().getBrowserLocale();
    			Locale bLocale = null;
    			if(!"Y".equals(browserLocale) && browserLocale != null)
    			{
    				 bLocale=new Locale(appVars.getAppConfig().getBrowserLocale().split("_")[0],appVars.getAppConfig().getBrowserLocale().split("_")[1]);	
    				 appVars.setLocale(bLocale);
    			}
    			else{
    				appVars.setLocale(request.getLocale());
    			}
    			
    			BaseModel bm = new BaseModel();
    			HTTPScreenController sc = null;
    			boolean success = false;
    			request.getSession().setAttribute("BaseModel", bm);
				sc = ScreenToControllerMap.getScreenController("default");
				sc.setRequest(request);
				sc.setSession(request.getSession());
				success = sc.process("screenInit");
				bm = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
				if (!success) {
					AppVars.addStaticDiagnostic("Unsuccessful processing of action", AppConfig.CONTROLLER_DEBUG);
				}
				request.getSession().setAttribute("BaseModel", bm);
				success = sc.process("PFKEY0");
				if (!success) {
					AppVars.addStaticDiagnostic("Unsuccessful processing of action", AppConfig.CONTROLLER_DEBUG);
				}
				String nextScreen = sc.getJsp();
				appVars.addTiming("Forwarding to screen" + nextScreen);
				appVars.addDiagnostic("Forwarding to screen" + nextScreen);
				if (!appVars.finalised && appVars.getAppConfig().isSingleModelJSP(nextScreen)) {
					appVars.setNextSingleModelJSPScreen(nextScreen);
					nextScreen = appVars.getAppConfig().getBaseScreen();
				}
				
				if(appVars.getAppConfig().getJspConfigPath()!=null && !appVars.getAppConfig().getJspConfigPath().isEmpty()){
					appVars.setJspConfigPath(appVars.getAppConfig().getJspConfigPath());
					nextScreen=nextScreen.substring(0, nextScreen.indexOf('.'))+"NEW"+nextScreen.substring(nextScreen.indexOf('.'));
				}
				 
				if(appVars.getAppConfig().getDtFormat()!=null && !appVars.getAppConfig().getDtFormat().isEmpty()){
					appVars.setDtFormat(appVars.getAppConfig().getDtFormat());
				}
				FrameworkDialectFactory.getInstance().getControllerServletSupport().doProcessSupport(getServletConfig(), nextScreen, sc, request, response);
        	}       
        }
        catch (IOException e) {
            FirstServlet.LOGGER.error("doGet(HttpServletRequest, HttpServletResponse)", (Throwable)e);
            return false;
        }
        catch (ServletException e2) {
            FirstServlet.LOGGER.error("doGet(HttpServletRequest, HttpServletResponse)", (Throwable)e2);
            return false;
        }
        catch (Exception e3) {
        	FirstServlet.LOGGER.error("doGet(HttpServletRequest, HttpServletResponse)", (Throwable)e3);
        	return false;
        }
        return true;
		//IBPLIFE-4668 ends
    }
   
    
	//IJTI-641 starts
    private String genSQLSelectLastLoginTime(final String dbType) {
        String query = "";
        switch (dbType) {
            case "1": {
                query = " SELECT TOP 1 LASTLOGON_TIME FROM USRINF  WHERE LTRIM(RTRIM(USERID))=? ORDER BY LASTLOGON_TIME DESC ";
                break;
            }
            case "0": {
                query = " SELECT LASTLOGON_TIME FROM (  SELECT LASTLOGON_TIME FROM USRINF  WHERE LTRIM(RTRIM(USERID))=? ORDER BY LASTLOGON_TIME DESC  ) WHERE ROWNUM <= 1 ";
                break;
            }
            default: {
                throw new IllegalArgumentException("Not supported Database: " + dbType);
            }
        }
        return query;
    }
	//IJTI-641 ends
    static {
        LOGGER = LoggerFactory.getLogger((Class)FirstServlet.class);
    }
}
//ILIFE-8402 ends