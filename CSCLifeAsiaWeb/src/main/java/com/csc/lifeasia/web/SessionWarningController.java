package com.csc.lifeasia.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.lifeasia.CoreAdminAuthenticationFilter;
import com.ia.framework.dialect.FrameworkDialectFactory;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.WebUtil;
import com.quipoz.framework.webcontrol.ControllerServlet;

/**
 * Servlet implementation class SessionWarningController
 * Process session warning message 
 */
public class SessionWarningController extends HttpServlet {
	private static final Logger LOGGER = LoggerFactory.getLogger(SessionWarningController.class);
	private static final long serialVersionUID = 1L;
	protected static final String DEFAULT_LOGOUT_JSP = "/logout.jsp";
	protected static String logoutPage = DEFAULT_LOGOUT_JSP;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionWarningController() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() {
    	logoutPage = FrameworkDialectFactory.getInstance().getControllerServletSupport().getLogoutPage(getParameter(ControllerServlet.PARAM_LOGOUT_PAGE), DEFAULT_LOGOUT_JSP);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*IJTI-932 START*/
	protected void doPost(HttpServletRequest request, HttpServletResponse response){
		/*IJTI-932 END*/
		AppVars av = AppVars.getInstance();
		if (av == null) {
			try {
				
				WebUtil.openPage(getServletConfig().getServletContext().getRequestDispatcher(logoutPage), request, response);
				
				} catch (Exception ex) {
					LOGGER.error("doPost method exception:",ex);
				// Nothing
			}
		}
		else if(av != null)
		av.setSessionExtended(true);
	}
	private String getParameter(String parameterName) {
		String parameterValue = getInitParameter(parameterName);
		if (parameterValue == null || parameterValue.trim().length() <= 0) {
			parameterValue = getServletContext().getInitParameter(parameterName);
		}
		if (parameterValue != null) {
			return parameterValue.trim();
		} else {
			return null;
		}
	}

}
