package com.csc.lifeasia.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.sessionfacade.GeneralSession;
import com.quipoz.framework.sessionfacade.GeneralSessionDelegate;
import com.quipoz.framework.sessionfacade.GeneralSessionFacade;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.ThreadLocalStore;

/**
 * This is for logout from Integral Admin.
 * @author csc
 *
 */
public class LogoutServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LogoutServlet.class);

	private final long	serialVersionUID	= 1L;

	/**
	 * URL for redirect after logout.
	 */
	private String redirectToUrl;

	/**
	 * CAS server logout URL.
	 */
	private String casServerLogoutUrl;

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init(ServletConfig config) throws ServletException {
		// must be set
	    casServerLogoutUrl = config.getInitParameter("casServerLogoutUrl");
	    if (casServerLogoutUrl == null)
	      throw new ServletException("Init parameter casServerLogoutUrl is not set!");
	    
	    // optinal, can be null. If not set CAS will not redirect user after logout.
	    redirectToUrl = config.getInitParameter("redirectToUrl");
	    if (redirectToUrl != null) {
	      try {
	        redirectToUrl = URLEncoder.encode(redirectToUrl, "UTF-8");
	      } catch (UnsupportedEncodingException e) {
	    	  /*IJTI-932 START*/
	    	  LOGGER.error("Exception occured", e);
	    	  /*IJTI-932 END*/
	        throw new ServletException(e.getMessage());
	      }
	    }
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		try {
			LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession is having a go V2 ...");
			GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
			LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession got the GD ...");
			if (gd != null) {
				BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
				if (bm != null) {
					LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession is cleaning up ...");
					GeneralSession impl = gd.getImpl();
					if (AppVars.getInstance().getAppConfig().getServer().equalsIgnoreCase("EJB")) {
						LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession is removing a Session Bean ...");
						((GeneralSessionFacade)impl).remove();
					}
					else {
						LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession is removing a Local Bean ...");
						gd.cleanup(bm);
					}
					LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession has cleaned up !");
				}
				else {
					LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - KillSession got the GD, but the BaseModel is null!");
				}
			}
			else {
				LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - Error cleaning up the EJB session, gd is null");
			}
		}
		catch (Exception e) {
			LOGGER.error("doPost(HttpServletRequest, HttpServletResponse)", e);
			LOGGER.debug("doPost(HttpServletRequest, HttpServletResponse) - Error ", e);//IJTI-1499
			LOGGER.debug(" cleaning up the EJB session");//IJTI-1499
		}
		ThreadLocalStore.clear();
		session.invalidate();
//			getServletContext().getRequestDispatcher("/logon.jsp").forward(request, response);
//			response.sendRedirect(request.getContextPath());
		String url = casServerLogoutUrl;
		if (redirectToUrl != null && redirectToUrl.length() > 0) {
			url += "?service=" + redirectToUrl;
		}
		logoutRedirect(url, response); //IJTI-639
	}
	
	//IJTI-639 starts
	
	 /* Redirect: 
     *   From LifeAsiaWeb to CAS server: Redirect URL is from web.xml Config file, not from User's Input. Therefore no need to validate.
     *   From CAS server to LifeAsiaWeb: Validate Service URL by config RegexRegisteredService in deployerConfigContext.xml of CAS.
     **/
	
	@SuppressWarnings("findsecbugs:UNVALIDATED_REDIRECT")
	private void logoutRedirect(String url, HttpServletResponse response) {
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			LOGGER.error("doPost(HttpServletRequest, HttpServletResponse)", e);
		}
	}
	//IJTI-639 ends
}
