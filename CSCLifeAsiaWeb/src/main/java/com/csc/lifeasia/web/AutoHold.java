package com.csc.lifeasia.web;

import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.context.IntegralApplicationContext;
import com.csc.integral.session.IntegralJdbcSessionFactory;
import com.csc.smart400framework.dataaccess.model.Hldusr;
import com.csc.smart400framework.dataaccess.model.Schldusr;

/**
 * @author DXC Adding this singleton class to autoschdule the hold and release
 *         process
 *
 */
public class AutoHold {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AutoHold.class);
	private static ScheduledExecutorService holdScheduler;
	private static ScheduledExecutorService releaseScheduler;
	private static AutoHold ah;
	private static LocalDateTime localTimeNow;
	private static LocalDateTime localSchdlDateTime;
	private static LocalDateTime localStopDateTime;
	private static long hldDuration;
	private static long closeDuration;
	private static String msg;
	private static SessionCleaner1 obj;
	public static String sessionID;//ILIFE-8402

	private AutoHold(String sessionID, String msg) {
		localTimeNow = LocalDateTime.now();
		localSchdlDateTime = LocalDateTime.now();
		localStopDateTime = LocalDateTime.now();
		this.sessionID = sessionID;
		this.msg = msg;

	}

	public static AutoHold getAutoHoldObj(String sessionID, String msg) {
		if (null == ah) {
			ah = new AutoHold(sessionID, msg);
		}
		return ah;
	}

	// assuming start time/duration in hh:mm and dates in yyyymmdd format
	// public static void scheduleHold(String startTime,String Drtn, String
	// frmDt, String toDt){
	public static void scheduleHold(String sessionID, String msg) {

		try {
			AutoHold.getAutoHoldObj(sessionID, msg);
			obj = new SessionCleaner1();
			Schldusr schldusr = IntegralApplicationContext
					.getApplicationContext()
					.getBean("sessionFactory", IntegralJdbcSessionFactory.class)
					.getSchldusrdao();
			if (schldusr != null) {
				if (schldusr.getScheduletype().equalsIgnoreCase("o")) {
					Date d = new Date();
					DateFormat format = new SimpleDateFormat("HH:mm");
					int ActualMins1 = add(
							mult(Integer.parseInt(format.format(d).substring(0,
									2)), 60),
							Integer.parseInt(format.format(d).substring(3)))
							.toInt();
					int StartMints1 = add(
							mult(Integer.parseInt(schldusr.getStarttime()
									.substring(0, 2)), 60),
							Integer.parseInt(schldusr.getStarttime().substring(
									3))).toInt();
					long initalDelay = sub(StartMints1, ActualMins1).toLong();

					
					// adding an executor for putting system on hold
					holdScheduler = Executors.newScheduledThreadPool(1);
					holdScheduler.scheduleAtFixedRate(ah.new HoldThread(sessionID),//ILIFE-8402
							initalDelay, 24 * 60, TimeUnit.MINUTES);
				} else {
                    int hhSartTime=0;
					int mmStartTime = 0;
				
					
					
					//extracting the time
					hhSartTime = Integer.valueOf(schldusr.getStarttime().substring(0, 2).trim());//IJTI-1419
					mmStartTime = Integer.valueOf(schldusr.getStarttime().substring(3).trim());//IJTI-1419
							
					
					//starting date time
					localTimeNow = LocalDateTime.now();
					localSchdlDateTime = LocalDateTime.now();
					localSchdlDateTime = localSchdlDateTime.withMinute(mmStartTime);	
					localSchdlDateTime = localSchdlDateTime.withHour(hhSartTime);	
					localSchdlDateTime = localSchdlDateTime.withDayOfMonth(Integer.valueOf(schldusr.getFromdate().toString().substring(6).trim()));//IJTI-1419
					localSchdlDateTime = localSchdlDateTime.withMonth(Integer.valueOf(schldusr.getFromdate().toString().substring(4, 6).trim()));//IJTI-1419
					localSchdlDateTime = localSchdlDateTime.withYear(Integer.valueOf(schldusr.getFromdate().toString().substring(0, 4).trim()));//IJTI-1419
							
					//ending date time
					localStopDateTime = LocalDateTime.now();
					localStopDateTime = localStopDateTime.withMinute(mmStartTime);	
					localStopDateTime = localStopDateTime.withHour(hhSartTime);	
					localStopDateTime = localStopDateTime.withDayOfMonth(Integer.valueOf(schldusr.getTodate().toString().substring(6).trim()));//IJTI-1419
					localStopDateTime = localStopDateTime.withMonth(Integer.valueOf(schldusr.getTodate().toString().substring(4, 6).trim()));//IJTI-1419
					localStopDateTime = localStopDateTime.withYear(Integer.valueOf(schldusr.getTodate().toString().substring(0, 4).trim()));//IJTI-1419
					
		
					//delay duration after which the system will be put on hold
					Duration delay = Duration.between(localTimeNow, localSchdlDateTime);
					long initalDelay = delay.getSeconds();
					
					// adding an executor for putting system on hold
					holdScheduler = Executors.newScheduledThreadPool(1);
					holdScheduler.scheduleAtFixedRate(ah.new HoldThread(sessionID),//ILIFE-8402
							initalDelay, 24 * 60 * 60, TimeUnit.SECONDS);

				}
			}
		} catch (Exception e) {
			LOGGER.error("in schedule hold method" , e);
		}
	}

	// hold thread
	class HoldThread implements Runnable {
		//ILIFE-8402 starts	
		public  String localsessionID;		
		public HoldThread(String localsessionID) {
			super();
			this.localsessionID = localsessionID;
		}
		//ILIFE-8402 ends	
		
		@Override
		public void run() {

			Hldusr hldusr1 = IntegralApplicationContext
					.getApplicationContext()
					.getBean("sessionFactory", IntegralJdbcSessionFactory.class)
					.getHldusrdao();

			Schldusr schldusr1 = IntegralApplicationContext
					.getApplicationContext()
					.getBean("sessionFactory", IntegralJdbcSessionFactory.class)
					.getSchldusrdao();

			long hldDuration = add(
					mult(Integer.parseInt(schldusr1.getDurationofhold()
							.substring(0, 2)), 60),
					Integer.parseInt(schldusr1.getDurationofhold().substring(3)))
					.toLong();
			// checking if system is already on hold
			if (hldusr1 == null) {
				IntegralApplicationContext
						.getApplicationContext()
						.getBean("sessionFactory",
								IntegralJdbcSessionFactory.class)
						.setHldusrdao(hldusr1, schldusr1.getUnique_number(),
								schldusr1.getUserId());

				obj.sendNtifictnMessage(localsessionID, msg);//ILIFE-8402
			}

			// executing the release system scheduler
			releaseScheduler = Executors.newScheduledThreadPool(1);
			releaseScheduler.scheduleAtFixedRate(ah.new ReleaseThread(),
					hldDuration, 24 * 60, TimeUnit.MINUTES);

		}

	}

	// release thread
	class ReleaseThread implements Runnable {
		@Override
		public void run() {
			Hldusr hldusr2 = IntegralApplicationContext
					.getApplicationContext()
					.getBean("sessionFactory", IntegralJdbcSessionFactory.class)
					.getHldusrdao();

			Schldusr schldusr2 = IntegralApplicationContext
					.getApplicationContext()
					.getBean("sessionFactory", IntegralJdbcSessionFactory.class)
					.getSchldusrdao();

			if (hldusr2 != null) {
				IntegralApplicationContext
						.getApplicationContext()
						.getBean("sessionFactory",
								IntegralJdbcSessionFactory.class)
						.updateHldusrdao(hldusr2);

				obj.stopNotifications();
			}

			// checking if the stop date has been reached. if yes terminate
			if (schldusr2 != null) {
				if (schldusr2.getTodate() == 99999999) {
					IntegralApplicationContext
							.getApplicationContext()
							.getBean("sessionFactory",
									IntegralJdbcSessionFactory.class)
							.updateSchldusrdao(hldusr2, schldusr2);
					ah.stopHoldSchedule();
				} else {
					Date date = java.util.Calendar.getInstance().getTime();
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					if (isLTE(schldusr2.getTodate(), dateFormat.format(date)
							.replaceAll("/", ""))) {
						IntegralApplicationContext
								.getApplicationContext()
								.getBean("sessionFactory",
										IntegralJdbcSessionFactory.class)
								.updateSchldusrdao(hldusr2, schldusr2);
						ah.stopHoldSchedule();
					}
				}
			}

			releaseScheduler.shutdownNow();
		}

	}

	// method to directly terminating the hold scheduler
	public static void stopHoldSchedule() {
		holdScheduler.shutdownNow();
	}

	// method to directly terminating the release scheduler
	public static void stopReleaseSchedule() {
		releaseScheduler.shutdownNow();
	}
}
