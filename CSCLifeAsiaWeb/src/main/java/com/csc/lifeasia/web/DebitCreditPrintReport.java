package com.csc.lifeasia.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.utils.SpoolPrintReport;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

/**
 * Servlet implementation class DebitCreditPrintReport
 */
public class DebitCreditPrintReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpoolPrintReport.class);
	
	public void init() throws ServletException {
		
	}
	
	@SuppressWarnings("resource")
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException  {
		// Declare variables
		String fileName = null;
		String folderName = null;		
		AppVars appVars = null;
		AppConfig appConfig = null;		
		File file = null;		
		byte[] buf = null;
		int bytesRead;
		
		try {
			// Get file name and user name from request
			fileName = request.getParameter("fileName").trim();
			folderName = request.getParameter("folderName");
			
			// Get real path of report from QuiposCfg.xml
			appVars = AppVars.getInstance();
			appConfig = appVars.getAppConfig();
			// IJTI-1249 Start
			// Read file report
			file = FileSystems.getDefault().getPath(appConfig.getTextOutput() + File.separator 
					+ folderName.trim() + File.separator + fileName).toFile(); 
			if(!file.exists()) {
				String message = "not found";
				response.setContentType("text/plain");
				response.getWriter().write(message); 
			}else {
				try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
					// Get output stream from object response of servlet
					try (ServletOutputStream out = response.getOutputStream()){
						// Create buffer size
						buf = new byte[4 * 1024];
						// Set value to content header 
						response.setContentType("text/plain");
						response.addHeader("Content-Disposition", "attachment; filename=" + StringEscapeUtils.escapeJava(fileName)); 
						response.setContentLength((int) file.length());
						
						// Out data to output stream
						while ((bytesRead = bis.read(buf)) != -1) {
							out.write(buf, 0, bytesRead);
						}
					}
				}
			}
		} catch (IOException ex) {
			LOGGER.error("DebitCreditPrintReport: Error at ", ex);
		}		
		// IJTI-1249 End
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
