package com.csc.lifeasia.web;

import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.ThreadLocalStore;
import com.csc.lifeasia.web.AutoHold;

/**
 * 
 * 
 *
 * 
 */
public class LifeWebContextBean {
	
	
	public void init(){
		
		AppVars av = new LifeAsiaAppVars("LifeAsiaWeb");

		AutoHold.scheduleHold(av.getSessionId(),"System is going on hold");
		
		ThreadLocalStore.clear();
				
	}	
}
