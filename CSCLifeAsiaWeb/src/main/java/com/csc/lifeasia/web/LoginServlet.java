package com.csc.lifeasia.web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.HtmlUtils;

import com.csc.integral.context.IntegralApplicationContext;
import com.csc.integral.session.IntegralJdbcSessionFactory;
import com.csc.ldap.Ldap;
import com.csc.lifeasia.SessionAppVarsStore;
import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;
import com.csc.smart400framework.dataaccess.dao.impl.BudbwpfDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.HldusrDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.UsrinfDAOImpl;
import com.csc.smart400framework.dataaccess.model.Budbwpf;
import com.csc.smart400framework.dataaccess.model.Hldusr;
import com.csc.smart400framework.dataaccess.model.Usrinf;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.IntegralConfig;



public class LoginServlet extends HttpServlet {
	
	
	/**
     * Logger for this class
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occure
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String userId = filterParameterValue(request.getParameter("userid"));//IJTI-436
		if(!userId.isEmpty()){
		userId = userId.trim().toUpperCase();
		}
		String lastLoginTime = "never";
		String passWord = request.getParameter("password");
		if(!passWord.isEmpty()){
		passWord = passWord.trim();
		}
		String userDescription="";
		//IJTI-1534 starts
		String tenantId = request.getParameter("tenantid");
		if(tenantId != null){
			tenantId = HtmlUtils.htmlEscape(tenantId);
		} else {
			tenantId = "";
		}
		//IJTI-1534 ends
		Ldap ldap = new Ldap();
		//ldap verifacation
		
				
		//judge whether this is system Admin

		boolean isauthoritied =ldap.isAuthenticatedUser(userId, passWord);
		if (isauthoritied) {
			
			LifeAsiaAppVars appVars = new LifeAsiaAppVars(getInitParameter("Appname"));
			
			appVars.setContextPath(request.getContextPath().replaceFirst("/", ""));			
			// Fixed concurrents users
			if (IntegralConfig.isMultiTenancyEnabled()) {
				appVars.setTenantid(tenantId);// IJTI-1534
			}
			AppVars.setInstance(appVars);

		//	lastLoginTime = updateUserInfo(userId);
			//userDescription = ldap.getUserDescription(userId + " ext")				.toString();
			userDescription = ldap.getUserDescription(userId).toString();
			SessionAppVarsStore.set(request.getSession().getId(), appVars);
			//lastLoginTime = updateUserInfo(userId);
			//userDescription=ldap.getUserDescription(userId+" ext").toString();
			//userDescription = ldap.getUserDescription(userId).toString();
			String sessionId = request.getSession().getId();
			// create user http session

			Usrinf usrinf = IntegralApplicationContext.getApplicationContext().getBean("sessionFactory",IntegralJdbcSessionFactory.class).isValid(userId,sessionId);
			
//			IPB-254
			Hldusr h1 =null;
			Hldusr hidu =   IntegralApplicationContext.getApplicationContext().getBean("hldusrDAO", HldusrDAOImpl.class).getUserdetails(h1);
			HttpSession session = request.getSession();
			//userDescription = ldap.getUserDescription(userId).toString(); 
			if(hidu!=null){
				Budbwpf bud =	IntegralApplicationContext.getApplicationContext().getBean("budbwpfDAO", BudbwpfDAOImpl.class).getBudbwRecordByUserId(userId);			
				if(bud!=null && ("1").equals(bud.getValidflag())){
					if(usrinf.getLastlogonTime()!= null){
						lastLoginTime = new SimpleDateFormat().format(usrinf.getLastlogonTime());
					}   
					login(request, response, userId, lastLoginTime,
							userDescription, appVars, usrinf, session);
				}else{
		    	   String alertMsg = "2";
		    	   session.setAttribute("msg1",alertMsg);
		    	   IntegralApplicationContext.getApplicationContext().getBean("usrinfDAO", UsrinfDAOImpl.class).updateUserId(usrinf.getUniqueNumber());
		    	   dispatchRequest(request, response);
				}
			}//IPB-254
			else{
				session.setAttribute("msg1",null);
				if(usrinf.getLastlogonTime()!= null){
					lastLoginTime = new SimpleDateFormat().format(usrinf.getLastlogonTime());
				}   
				login(request, response, userId, lastLoginTime,
						userDescription, appVars, usrinf, session);
			}
			
		}

		//judge whether this is system Admin

		else{
			try {
				String message = "Sorry, your user name or password is wrong!";
				String isWrong = "1";
				request.setAttribute("message", message);
				request.setAttribute("isWrong", isWrong);
				getServletContext().getRequestDispatcher("/logon.jsp").forward(
						request, response);
			} catch (IOException | ServletException e) {
				LOGGER.error("Exception Occurred while login", e);
			} 
		} 

	}
	
	/**
	 * Dispatches request back to First Servlet.
	 * 
	 * @param request - HttpServletRequest
	 * @param response - HttpServletResponse
	 */
	private void dispatchRequest(HttpServletRequest request, HttpServletResponse response){
		try {
			getServletContext().getRequestDispatcher("/FirstServlet").forward(request, response);
		} catch (ServletException | IOException e) {
			LOGGER.error("Exception Occurred during Login", e);
		}
	}
	
	private boolean login(HttpServletRequest request, HttpServletResponse response, String userId, String lastLoginTime,
			String userDescription, LifeAsiaAppVars appVars, Usrinf usrinf, HttpSession session){
		
		// Cache User description for next use
		appVars.setUserDescription(userDescription);   //IGB-1241
		if(usrinf.getLastlogonTime()!= null)
		{
			lastLoginTime = new SimpleDateFormat().format(usrinf.getLastlogonTime());
		}
		
		if(usrinf.getMessage().equals("1"))
			{
				SessionCleaner1.deleteLimitSession(usrinf.getSessionId());	
				session.setAttribute(IntegralJdbcSessionFactory.LOGOUT_SESSIONID,usrinf.getSessionId());
			}
		session.setAttribute("USERID", userId);
		session.setAttribute("userD", userDescription);
		session.setAttribute("LastLoginTime", lastLoginTime);
		session.setAttribute(IntegralJdbcSessionFactory.USER_INF_KEY, usrinf.getUniqueNumber());
		session.setAttribute("ISSYSTEMADMIN", false);

		// initialize AppVar
		try {
			
			getServletContext()
					.getRequestDispatcher(
							"/process?action_key=screenInit&screen=default&previousMenu=index.jsp")
					.forward(request, response);
		} catch (IOException | ServletException e) {
			LOGGER.error("Exception Occurred while login", e);
		} 
		return true;
	}
	
		// IJTI-436 starts
		/**
		 * Filter user input against black listed characters in Integral.
		 * 
		 * @param parameterValue
		 *            - value to be filtered
		 * @return - filtered value
		 */
		private String filterParameterValue(String parameterValue) {
			List<String> blackList = Arrays.asList(new String[] { "<", ">" });
			for (String blackCharacter : blackList) {
				if (parameterValue.contains(blackCharacter)) {
					return "";
				}
			}
			return parameterValue;
		}
		// IJTI-436 ends
		

}
