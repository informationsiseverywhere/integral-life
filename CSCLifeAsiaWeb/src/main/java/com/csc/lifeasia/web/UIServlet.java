package com.csc.lifeasia.web;

/*import java.io.File;*/
import java.io.IOException;
/*import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;*/

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.lifeasia.runtime.core.LifeAsiaAppLocatorCode;
import com.csc.lifeasia.runtime.variables.LifeAsiaAppVars;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.VarModel;
import com.quipoz.framework.util.WebUtil;*/

/**
 * This class is the responsible for generating JSP according to Screen(S4014) and language.
 *
 * FileName: UIServlet.java
 * Create time: 3rd July,2008
 *
 * Change history:
 * Created by Benny on 3rd July, 2008
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 * @author Benny
 * @version 1.0
 */
public class UIServlet extends HttpServlet {
	/**
	 * Logger for this class
	 */
/*	private static final Logger LOGGER = LoggerFactory.getLogger(UIServlet.class);

	private static final long serialVersionUID = 1L;
	private LifeAsiaAppVars appVars;
	private BaseModel bm;
	private SmartVarModel smartVarModel;
	private String programPath;*/
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		doPost(request, response);
	}

	/* 
	 * This method is responsible for receiving the user request and dispatcher request to corresponding servlet.
	 * 
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
/*		PrintWriter pw = response.getWriter();
		if (request.getParameter("type")==null) {
			dispalyQueryPage(request.getContextPath(), pw);
		} else if (request.getParameter("type").equals("2")) {
			String jspNo = "";
			if(StringUtils.isAlphanumeric(request.getParameter("jspno"))){
				jspNo = request.getParameter("jspno");
			}	
			String language = request.getParameter("language");
			dispalyJSPPage(request, response, jspNo,language);
		} 
*/	}

	/**
	 * This method is responsible for displaying the JSP by screenForm and language by user.
	 * @param request
	 * @param response
	 * @param screenForm
	 * @param language
	 * @throws ServletException
	 * @throws IOException
	 */
/*	private void dispalyJSPPage(HttpServletRequest request,
			HttpServletResponse response, String screenForm, String language)
			throws ServletException, IOException {
	    LifeAsiaAppVars appVars = new LifeAsiaAppVars(getServletContext().getInitParameter("Appname"));
	    appVars.setContextPath(request.getContextPath().replaceFirst("/", ""));
		bm = new BaseModel();
		// get AppVars
		appVars = getAppVarsInstance(language, screenForm);
	      //  BY YY IPNC-2848,IPNC-2839,IPNC-2842
        appVars.setBusinessdate("01/06/2015");
        appVars.setJspConfigPath("jsp");
        // end
		if (appVars != null) {

			String browserLocale = appVars.getAppConfig().getBrowserLocale();
			Locale bLocale = null;
			if(!"Y".equals(browserLocale) && browserLocale != null)
			{
				 bLocale=new Locale(appVars.getAppConfig().getBrowserLocale().split("_")[0],appVars.getAppConfig().getBrowserLocale().split("_")[1]);	
				 appVars.setLocale(bLocale);
				
			}
			else{
				appVars.setLocale(request.getLocale());
			}
		}
		smartVarModel = initSmartModel(screenForm);
		getScreenModel(screenForm);
		forwardScreen(request, response, screenForm, appVars);
	}

	*//**
	 * THis method is responsible for forward to corresponding page.
	 * @param request
	 * @param response
	 * @param screenForm
	 * @throws ServletException
	 * @throws IOException
	 *//*
	private void forwardScreen(HttpServletRequest request,
			HttpServletResponse response, String screenForm, LifeAsiaAppVars av)
			throws ServletException, IOException {
		// call write method to display content in the JSP by reflection
		Object o = null;
		try {
			try{
				o = Class.forName(programPath + "." + screenForm + "screen")
					.newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screen")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
			LOGGER.error("forwardScreen(HttpServletRequest, HttpServletResponse, String)", e);
		}
		//init SXXXscreenctl
		try {
			try{
				o = Class.forName(programPath + "." + screenForm + "screenctl").newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screenctl")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		//init SXXXscreensfl
		try {
			try{
			o = Class.forName(programPath + "." + screenForm + "screensfl").newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screensfl")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		request.getSession().setAttribute(BaseModel.SESSION_VARIABLE, bm);
		if(av.getAppConfig().getJspConfigPath()!=null && !av.getAppConfig().getJspConfigPath().isEmpty()){
			WebUtil.openPage(getServletConfig().getServletContext()
					.getRequestDispatcher("/POLAMODELNEW.jsp"), request, response);
		} else {
			WebUtil.openPage(getServletConfig().getServletContext()
					.getRequestDispatcher("/POLAMODEL.jsp"), request, response);
		}
		
	}

	*//**
	 * This method is responsible for getting screen model.
	 * @param screenForm
	 *//*
	private void getScreenModel(String screenForm) {
		ScreenModel screenModel = null;
		if (screenModel == null) {
			screenModel = new ScreenModel(screenForm, appVars, smartVarModel);
		}
		bm.setScreenModel(screenModel);
	}

	*//**
	 * This method is responsible for initializing smart model.
	 * @param screenForm
	 * @return smartVarModel
	 *//*
	private SmartVarModel initSmartModel(String screenForm) {
		//bm.setApplicationVariables(appVars);
		Object screenVars = null;
		programPath = LifeAsiaAppLocatorCode.findPath(screenForm);
		if (programPath == null) {
			throw new RuntimeException("Can't find program '"+screenForm+"' in any path.");
		}
		try {
			String classVarName = screenForm + "ScreenVars";
			programPath = programPath.replace("programs", "screens");
			LOGGER.debug("initSmartModel(String) - classVarNamePath=" + programPath);
			try{
				screenVars =  Class.forName(programPath + "." + classVarName).newInstance();
			}catch(ClassNotFoundException e){
				screenVars =  Class.forName("com.csc.fsu.general.screens." + classVarName).newInstance();
			}
			smartVarModel = (SmartVarModel) screenVars; 
			// set value for every field by reflection
			Field []field = screenVars.getClass().getDeclaredFields();
			for(int i=0;i<field.length;i++){
				String simpleTypeName = field[i].getType().getSimpleName();
				//System.out.println(field[i].getName()+"\t"+ field[i].getType().getSimpleName());
				if(simpleTypeName.equals("FixedLengthStringData")){
					int length = field[i].get(screenVars).toString().length();
					// String data = "xxxxxxxxxxxxxxxxxxxx"; 
					Object fieldOrig = field[i].get(screenVars);
					if("language".equals(field[i].getName())){
					    ((FixedLengthStringData)fieldOrig).set("E");
					}else if(field[i].getName().indexOf("optind") != -1){
					    ((FixedLengthStringData)fieldOrig).set(" ");
                    } else{
					    ((FixedLengthStringData)fieldOrig).set(String.format("%0"+length+"d", 0).replace("0", "x"));
					}
					field[i].set(screenVars, fieldOrig);
				}else if(simpleTypeName.equals("ZonedDecimalData")){
					int length = field[i].get(screenVars).toString().length();
					String data = "";
					for(int j=0;j<length-2;j++){
						data+="9";
					}
					double d = Double.parseDouble(data+".99");
					field[i].set(screenVars, new ZonedDecimalData(length,2,d));
				}else if(simpleTypeName.equals("PackedDecimalData")){
				    // UI enhancement BY YY IPNC-2667
				    Object fieldOrig = field[i].get(screenVars);
                    int length = ((PackedDecimalData)fieldOrig).getLength();
                    String data = String.format("%0"+length+"d", 9).replace("0", "9");
                    ((PackedDecimalData)fieldOrig).set(data);
                    field[i].set(screenVars, fieldOrig);
                }
			}
			return smartVarModel;
		} catch (Exception e) {
			LOGGER.error("initSmartModel(String)", e);
			return smartVarModel;
		} 
	}

	*//**
	 * This method is responsible for generating random letters.
	 * @return
	 *//*
	private String getRandomLetter() {
		StringBuffer sb = new StringBuffer("");
		for(int i=0;i<300;i++){
			int in =( int)(Math.random()*123);
			if(in>=97 && in<=122)
			sb.append((char)in);
		}
		return sb.toString();
	}

	*//**
	 * This method is responsible for getting LifeAsiaAppVars. <br>
	 * 1. initilize the QuipozCfg.xml
	 * 
	 * @param language
	 * @param screenForm
	 * @return appVars
	 *//*
	private LifeAsiaAppVars getAppVarsInstance(String language, String screenForm) {
		String prop = "Quipoz." + AppVars.getInstance().getAppConfig().getAppName() + ".XMLPath";
		String path = System.getProperty(prop);
		if(language.equals("en")){
			language = "eng";
		}else if(language.equals("zh")){
			language = "chi";
		}
		LOGGER.debug("getAppVarsInstance(String, String) - quipozConfigFile=" + path);
		System.setProperty(prop,path);
		// define master menu and system menu
		HashMap<String, String> map = new LinkedHashMap<String,String>();
		ArrayList<String> a = new ArrayList<String>();
		// a.add("UI Test System menu");
		// map.put("UITest","UITest");
		appVars = new LifeAsiaAppVars("POLAWeb");
		LifeAsiaAppVars.setInstance(appVars);
		// set next JSP screen.
		appVars.setNextSingleModelJSPScreen(screenForm);
		// set user language
		appVars.setUserLanguage(language);
		appVars.SetMenuDisplayed(false);
		appVars.setMasterMenu(map);
		appVars.setSystemMenu(a.toArray());
		return appVars;
	}

	*//**
	 * This method is responsible for generating input screen. User input screen name and language and 
	 * submit to servlet.
	 * @param pw
	 *//*
	private void dispalyQueryPage(String contextPath, PrintWriter pw) {
		StringBuffer sb = new StringBuffer(" ");
		sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
		sb.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\"theme/eng/QAStyle.jsp\" TYPE=\"text/css\">");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\"theme/eng/QAStyle.css\" TYPE=\"text/css\">");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\"/LifeAsiaWeb/theme/layout_1.css\" TYPE=\"text/css\">");
		sb.append("<TITLE>Life Asia 1.0</TITLE>");
		sb.append("</head><body><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<font size=4><B>Screen Display</B></font></br>");
		sb.append("<form name=\"form\" method=\"POST\"><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<b>Screen Name:</b><input id=\"jspno\" name=\"jspno\" type=\"text\" style=\"height:18px;\" size=\"5\">");
		sb.append("<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<b>Language:<b><select name=\"language\"><option value=\"en\">English</option><option value=\"zh\">Chinese</option></select>");
		sb.append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<input type=\"button\" name=\"Submit\" value=\"Submit\" style=\"height:18px;\" width=\"30\" onClick=\"f()\">");
		sb.append("</body></html>");
		sb.append("<script language='javascript'>");
		sb.append("function f(){form.action =\"UIServlet?type=2\"; form.submit(); }");
		sb.append("</script>");
		pw.write(sb.toString());
	}
	
	*//**
	 * This method is responsible for redirect the SXXXXForm.jsp directly.
	 * @param screenForm
	 * @param language
	 * @return
	 *//*
	@SuppressWarnings("unused")
	private String generateJSPPage(String screenForm, String language) {
		String path = LifeAsiaAppLocatorCode.findPath(screenForm);
		String array[] = path.split("[.]");
		String pathTemp = "";
		for (int i = 2; i <= array.length - 2; i++) {
			pathTemp += File.separator + path.split("[.]")[i];
		}
		LOGGER.debug("generateJSPPage(String, String) - language=" + language);
		if (language.equals("en")) {
			path = pathTemp + File.separator + "eng" + File.separator;
		} else if (language.equals("zh")) {
			path = pathTemp + File.separator + "chi" + File.separator;
		}
		return path + screenForm + "Form.jsp";
	}
*/}
