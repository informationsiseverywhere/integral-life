package com.csc.lifeasia;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.ThreadLocalStore;

public class AppVarsManagementFilter implements Filter {

	static Logger  logger = LoggerFactory.getLogger(AppVarsManagementFilter.class);
    @Override
    public void destroy() {
    }

	    @Override
	    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
	            ServletException {
	        HttpServletRequest httpReq = (HttpServletRequest) request;
        // ILife-3316 starts 
        long start = System.currentTimeMillis();
	        try {
	            // Set AppVars from SessionAppVarsStore into thread local
	            HttpSession s = httpReq.getSession(false);
	            if (s != null) {
	                String sessionId = s.getId();
	                AppVars av = SessionAppVarsStore.get(sessionId);
	                if (av != null) {           
	                	 MDC.put("tname", httpReq.getSession().getAttribute("USERID")+"");
	                	 
	                	//	logger.info("User [{}] - SessionID [{}] - Path [{}]", httpReq.getSession().getAttribute("USERID"),sessionId,uri);                  	
	                    AppVars.setInstance(av);
	                } else {           
	                	 if(s.getAttribute("USERID")!=null)
	                		 MDC.put("tname", s.getAttribute("USERID")+"");
	                	  request.setAttribute("killSession", "Killed"); //INTEGRAL IPB-253 
	                	//	logger.info("NULL NULL NULL AppVars - User [{}] - SessionID [{}] - Path [{}]", httpReq.getSession().getAttribute("USERID"),sessionId,uri);                     	                    	                	
	                }
	            }

	            chain.doFilter(request, response);
	        } finally {
	            // Set thread local's AppVars back into SessionAppVarsStore
	            HttpSession s = httpReq.getSession(false);
            String uri = httpReq.getRequestURI();
	            if (s != null) {
	                String sessionId = s.getId();
	                AppVars threadAv = AppVars.getInstance();
	                long duration = System.currentTimeMillis()-start;
	                if (threadAv != null) {                	
	                	if(duration>=500)
	                		logger.info("User [{}] - SessionID [{}] - Path [{}] -End Time in MS [{}]", httpReq.getSession().getAttribute("USERID"),sessionId,uri,duration);
	                	
	                	MDC.remove("tname");
//	                    SessionAppVarsStore.set(sessionId, threadAv);                    
	                } else {              
	                	if(duration>=500)
	                		logger.info("NULL AppVars - User [{}] - SessionID [{}] - Path [{}] -End Time in MS [{}]", httpReq.getSession().getAttribute("USERID"),sessionId,uri,duration);
	                	
	                	if(s.getAttribute("USERID")!=null)
	                		MDC.remove("tname");
	                }
            }
          // ilife-3316 ends
            // Clear ThreadLocalStore
            ThreadLocalStore.clear();
        }
    }

	    @Override
	    public void init(FilterConfig filterConfig) throws ServletException {
	    }
}
