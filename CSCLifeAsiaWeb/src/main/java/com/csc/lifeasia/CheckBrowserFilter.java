package com.csc.lifeasia;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CheckBrowserFilter implements Filter {
	
	private FilterConfig config;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		Cookie[] cookies = ((HttpServletRequest)servletRequest).getCookies();
		
		boolean isValidBrowser = false;
		
		for(int i = 0; cookies != null && i < cookies.length; i++){
			if("isValidBrowser".equals(cookies[i].getName()) && "true".equals(cookies[i].getValue()))
				isValidBrowser = true;
		}

		if (!isValidBrowser) {
			//AWPL-Integral Integration related changes. Need to add passed query String in the URL. 
			// So html file is changed into JSP file to include servier side code.  
			//servletRequest.getRequestDispatcher("/CheckBrowser.html").forward(servletRequest, servletResponse);
			servletRequest.getRequestDispatcher("/CheckBrowser.jsp").forward(servletRequest, servletResponse);
		}
		else{
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		this.config = arg0;
	}

}
