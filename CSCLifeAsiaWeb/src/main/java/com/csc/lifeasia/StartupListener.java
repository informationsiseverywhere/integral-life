package com.csc.lifeasia;

import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * StartupListener class. Configure log4j before other classes use it
 * 
 * @author tphan25
 */
public class StartupListener implements ServletContextListener {
    /**
     * Logger for this class
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StartupListener.class);

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // contextDestroyed
        /* ignore */
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        if ("true".equals(System.getProperty("aspect4pc"))) {
            URL url = this.getClass().getClassLoader()
                    .getResource("com/csc/integral/aop/logback-aop.xml");
            // assume SLF4J is bound to logback in the current environment
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
            if(url.getPath()==null){
            	LOGGER.error("Invalid Url");
            }else{
            try {
                JoranConfigurator configurator = new JoranConfigurator();
                configurator.setContext(context);
                // Call context.reset() to clear any previous configuration, e.g. default
                // configuration. For multi-step configuration, omit calling context.reset().
                context.reset();
                configurator.doConfigure(url);
            } catch (JoranException je) {
                // StatusPrinter will handle this
            	/*IJTI-932 START*/
             	 LOGGER.error("Error occured during context initialization", je);
             	 /*IJTI-932 END*/
            }
            StatusPrinter.printInCaseOfErrorsOrWarnings(context);
        }
        }
    }

}
