package com.csc.lifeasia;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jasig.cas.client.authentication.DefaultGatewayResolverImpl;
import org.jasig.cas.client.authentication.GatewayResolver;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.csc.integral.utils.ReflectionUtils;//IJTI-640

import com.quipoz.framework.util.FileUtil;

/**
 * Using this class to add languge parameter to Cas login url.
 */
public class CoreAdminAuthenticationFilter extends AbstractCasFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger(CoreAdminAuthenticationFilter.class);
    public static final String CAS_LOCALE_PARAM = "locale";
	private Path filePath;

    /**
     * The URL to the CAS Server login.
     */
    private String casServerLoginUrl;

    /**
     * Whether to send the renew request or not.
     */
    private boolean renew = false;

    @SuppressWarnings("serial")
    public static final Map<String, Locale> SUPPORTED_LOCALE = new HashMap<String, Locale>() {
        {
            put("zh", Locale.CHINA);
            put("en", Locale.US);
        }
    };
    
    public static final Map<String, String> POLISY_LANGS = new HashMap<String, String>() {
        {
            put("S", "zh");
            put("E", "en");
        }
    };
    /**
     * Whether to send the gateway request or not.
     */
    private boolean gateway = false;
    private GatewayResolver gatewayStorage = new DefaultGatewayResolverImpl();

    protected void initInternal(final FilterConfig filterConfig)
            throws ServletException {
        if (!isIgnoreInitConfiguration()) {
            super.initInternal(filterConfig);
            setCasServerLoginUrl(getPropertyFromInitParams(filterConfig,
                    "casServerLoginUrl", null));
            log.trace("Loaded CasServerLoginUrl parameter: "
                    + this.casServerLoginUrl);
            setRenew(parseBoolean(getPropertyFromInitParams(filterConfig,
                    "renew", "false")));
            log.trace("Loaded renew parameter: " + this.renew);
            setGateway(parseBoolean(getPropertyFromInitParams(filterConfig,
                    "gateway", "false")));
            log.trace("Loaded gateway parameter: " + this.gateway);

            final String gatewayStorageClass = getPropertyFromInitParams(
                    filterConfig, "gatewayStorageClass", null);

            if (gatewayStorageClass != null) {
                try {
                    this.gatewayStorage = (GatewayResolver) ReflectionUtils.
                    		resolveClass(gatewayStorageClass).newInstance();//IJTI-640
                } catch (final Exception e) {
                    log.error(e, e);
                    throw new ServletException(e);
                }
            }
        }
    }

    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.casServerLoginUrl,
                "casServerLoginUrl cannot be null.");
    }

    /**
     * Check authentication.
     */
    public final void doFilter(final ServletRequest servletRequest,
            final ServletResponse servletResponse, final FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;
        final HttpSession session = request.getSession(false);
		final Assertion assertion = session != null ? (Assertion) session
                .getAttribute(CONST_CAS_ASSERTION) : null;
				
        if (assertion == null) {
            final String serviceUrl = constructServiceUrl(request, response);
            final String ticket = CommonUtils.safeGetParameter(request,
                    getArtifactParameterName());
            final boolean wasGatewayed = this.gatewayStorage
                    .hasGatewayedAlready(request, serviceUrl);

            if (!CommonUtils.isNotBlank(ticket) && !wasGatewayed) {

                final String modifiedServiceUrl;

                log.debug("no ticket and no assertion found");
                if (this.gateway) {
                    log.debug("setting gateway attribute in session");
                    modifiedServiceUrl = this.gatewayStorage
                            .storeGatewayInformation(request, serviceUrl);
                } else {
                    modifiedServiceUrl = serviceUrl;
                }

                if (log.isDebugEnabled()) {
                    log.debug("Constructed service url: " + modifiedServiceUrl);
                }

                String urlToRedirectTo = CommonUtils.constructRedirectUrl(
                        this.casServerLoginUrl, getServiceParameterName(),
                        modifiedServiceUrl, this.renew, this.gateway);
                Locale locale = getConfigLocale();
                if (locale == null) {
                    String lang = request.getHeader("Accept-Language");
                    try {
	                    int i = lang.indexOf(",");
	                    lang = i > 0 ? lang.substring(0, i - 1) : lang;
	                    int j = lang.indexOf("-");
	                    lang = j > 0 ? lang.substring(0, j) : lang;
	                    locale = SUPPORTED_LOCALE.get(lang);
                    } catch( Exception ex){
                    	LOGGER.error("doFilter method exception:",ex);
                    }
                    if(locale == null){
                    	locale = SUPPORTED_LOCALE.get("en");
                    }
                }
                urlToRedirectTo = addLanguague(urlToRedirectTo, locale);
                if (log.isDebugEnabled()) {
                    log.debug("redirecting to \"" + urlToRedirectTo + "\"");
                }

                loginRedirect(urlToRedirectTo, response); //IJTI-639
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

	private Locale getConfigLocale() {
		Locale locale = null;
		String path = System.getProperty("Quipoz.LifeAsiaWeb.XMLPath");
		if (path == null) {
			String s = "Quipoz.LifeAsiaWeb.XMLPath is not set. \n";
			throw new RuntimeException(s);
		}
		Document doc = null;
		filePath=Paths.get(path);
		try(InputStream in=Files.newInputStream(filePath)){
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance(); 
			/*DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(in);*/ //IJTI-643 
			doc = FileUtil.trySetSAXFeature(factory, in); //IJTI-643	 
			Element root = doc.getDocumentElement();
			Node item = root.getElementsByTagName("CurrentLanguage").item(0);
			if(item != null){
				String language = item.getNodeValue();
				locale = SUPPORTED_LOCALE.get(POLISY_LANGS.get(language));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return locale;
	}
	
    private String addLanguague(String urlToRedirectTo, Locale locale) {
        String returnURL = urlToRedirectTo;
        if (locale != null) {
            returnURL = urlToRedirectTo
                    + (urlToRedirectTo.indexOf("?") != -1 ? "&" : "?")
                    + CAS_LOCALE_PARAM + "=" + locale;
        }
        return returnURL;
    }

    public final void setRenew(final boolean renew) {
        this.renew = renew;
    }

    public final void setGateway(final boolean gateway) {
        this.gateway = gateway;
    }

    public final void setCasServerLoginUrl(final String casServerLoginUrl) {
        this.casServerLoginUrl = casServerLoginUrl;
    }

    public final void setGatewayStorage(final GatewayResolver gatewayStorage) {
        this.gatewayStorage = gatewayStorage;
    }
    
    
    //IJTI-639 starts
    
    /* Redirect: 
     *   From LifeAsiaWeb to CAS server: Redirect URL is from web.xml Config file, not from User's Input. Therefore no need to validate.
     *   From CAS server to LifeAsiaWeb: Validate Service URL by config RegexRegisteredService in deployerConfigContext.xml of CAS.
     **/
	
	@SuppressWarnings("findsecbugs:UNVALIDATED_REDIRECT")
	private void loginRedirect(String url, HttpServletResponse response) {
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			log.error(e);
		}
	}
	//IJTI-639 ends
}
