package com.csc.lifeasia;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Base64;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.util.AppVars;

/**
 * @author rkothari6
 *
 */
public class RequestValidatorFilter implements Filter {

	public static final Logger LOGGER = LoggerFactory.getLogger(RequestValidatorFilter.class);

	public static final int NO_OF_BYTES = 30;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Do nothing
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;
		HttpSession session = httpReq.getSession(false);
		if ((httpReq.getRequestURL().toString()).contains("FirstServlet")
				|| (httpReq.getRequestURL().toString()).contains("LoginServlet")) {
			chain.doFilter(request, response);
			String sessionId = session.getId();
			AppVars av = SessionAppVarsStore.get(sessionId);
			SecureRandom secRan = new SecureRandom();
			byte []b = new byte[NO_OF_BYTES];
			secRan.nextBytes(b);
			av.setCsrfToken(new String(Base64.getEncoder().encode(b)));
		} else {
			String sessionId = session.getId();
			AppVars av = SessionAppVarsStore.get(sessionId);
			if (null != httpReq.getParameter("istk")
					&& av.getCsrfToken().equals(httpReq.getParameter("istk"))) {
				chain.doFilter(request, response);
				SecureRandom secRan = new SecureRandom();
				byte []b = new byte[NO_OF_BYTES];
				secRan.nextBytes(b);
				av.setCsrfToken(new String(Base64.getEncoder().encode(b)));
			} else {
				httpRes.sendRedirect("unauthorizedUser.jsp");
			}
		}
	}

	@Override
	public void destroy() {
		// Do nothing
	}
}
