package com.test;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.datatype.StringData;

public class ResourceBundleHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBundleHandler.class);

	private static ResourceBundle resourceBundle_Eng = null;
	private static ResourceBundle resourceBundle_Chi = null;
	public static final String LOCALE_ENG = "eng";
	public static final String LOCALE_CHI = "chi";
	private ResourceBundle resourceBundle = null;
	private String screenName = null;
	private String language = null;

	/**
	 * 
	 */
	public ResourceBundleHandler() {
	};

	/**
	 * 
	 * @param screenName
	 * @param language
	 */
	public ResourceBundleHandler(String screenName, String language) {
		this.screenName = screenName;
		this.language = language;
		resourceBundle = getResourceBundle(new Locale(language));
	}

	/**
	 * 
	 * @param localeValue
	 * @return
	 */
	// This block load the bundle at start up
	static {
		resourceBundle_Eng = ResourceBundle.getBundle("com.bundle.Bundle",
				new Locale(LOCALE_ENG));
		resourceBundle_Chi = ResourceBundle.getBundle("com.bundle.Bundle",
				new Locale(LOCALE_CHI));
		LOGGER.info("<<<<<<<<<Initialised Resource Bundle>>>>>");

	}

	public static ResourceBundle getResourceBundle(Locale localeValue) {

		ResourceBundle resourceBundle = null;
		if (localeValue.getLanguage().equalsIgnoreCase("eng")) {
			resourceBundle = resourceBundle_Eng;
		} else {
			resourceBundle = resourceBundle_Chi;
		}
		return resourceBundle;
	}

	public String gettingValueFromBundle(String key) {
		String value = null;
		String formattedKey = null;

		try {

			/*
			 * Locale localeValue = new Locale(this.language); ResourceBundle rb
			 * = getResourceBundle(localeValue);
			 */
			formattedKey = this.screenName + "."
					+ key.trim().replaceAll(" ", "_");
			value = resourceBundle.getString(formattedKey);

		} catch (MissingResourceException mre) {
			value = key;
			// mre.printStackTrace(); Commented to do not show error

		}

		return value;

	}

	/**
	 * @param clazz
	 * @param key
	 * @return the String wrapped in String data object to be used for in18
	 */
	public StringData gettingValueFromBundle(Class clazz, String key) {
		String value = null;
		String formattedKey = null;

		try {

			/*
			 * Locale localeValue = new Locale(this.language); ResourceBundle rb
			 * = getResourceBundle(localeValue);
			 */
			formattedKey = this.screenName + "."
					+ key.trim().replaceAll(" ", "_");
			value = resourceBundle.getString(formattedKey);

		} catch (MissingResourceException mre) {
			value = key;
			// mre.printStackTrace(); Commented to do not show error

		}

		return new StringData(value);

	}

}
