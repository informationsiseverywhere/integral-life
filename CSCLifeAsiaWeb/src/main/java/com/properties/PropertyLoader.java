package com.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyLoader.class);
	
	static Properties prop=null;
/**
 * Added to load the properties file.
 */
	private static void loadproperty() {
		try {
			InputStream is = PropertyLoader.class
					.getResourceAsStream("Integral_Life_Image_FolderName.properties");
			prop = new Properties();
			prop.load(is);
		} catch (IOException e) {
			prop = null;
			/*IJTI-932 START*/
				LOGGER.error("Exception Occurred while loading property", e);
				/*IJTI-932 END*/
		}
	}
/**
 * this method will return the folderName.
 * @param key
 * @return
 */
	public static String getFolderName(String key) {
		if (prop == null)
			loadproperty();
		if (prop == null || prop.getProperty(key) == null) {
			return "eng";
		}
		return prop.getProperty(key).split("/")[0];
	}
/**
 * this method will return the language code.
 * @param key
 * @return
 */
	public static String getLanguageCode(String key) {
		if (prop == null)
			loadproperty();
		if (prop == null || prop.getProperty(key) == null) {
			return "E";
		}
		return prop.getProperty(key).split("/")[1];
	}
}
