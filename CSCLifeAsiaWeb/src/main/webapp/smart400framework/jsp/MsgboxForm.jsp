<%@ include file="/POLACommon1NEW.jsp"%>
<%@page import="com.csc.smart400framework.screens.MsgboxScreenVars"%>
<%@page import="com.csc.smart400framework.screens.Msgboxscreen"%>
<%@page import="com.quipoz.framework.util.QPUtilities" %>
<%MsgboxScreenVars sv = (MsgboxScreenVars) fw.getVariables();%>

<%if (sv.MsgboxscreenWritten.gt(0)) {%>
	<%Msgboxscreen.clearClassString(sv);%>
	<%sv.MsgboxscreenWritten.set(0);%>
		
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Confirmation");%>
<%	generatedText0.appendClassString("label_txt");
	generatedText0.appendClassString("highlight");
%>

	

<%	sv.mgptxte01.setClassString("");%>
<%-- <%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Create Receipt");%> --%>
<%	sv.mgptxte01.appendClassString("string_fld");
	sv.mgptxte01.appendClassString("output_txt"); 
	sv.mgptxte01.appendClassString("highlight");
	
	//IFSU-140 start
	String wsaaLine01str = sv.mgptxte01.toString();
	wsaaLine01str = QPUtilities.replaceSubstring(wsaaLine01str, "<" , "(");
	wsaaLine01str = QPUtilities.replaceSubstring(wsaaLine01str, ">" , ")");
	sv.mgptxte01.set(wsaaLine01str);
	//IFSU-140 end
	%>
	

<%	sv.mgptxte02.setClassString("");%>
<%	sv.mgptxte02.appendClassString("string_fld");
	sv.mgptxte02.appendClassString("output_txt");
	sv.mgptxte02.appendClassString("highlight");%>

<%	sv.mgptxte03.setClassString("");%>
<%	sv.mgptxte03.appendClassString("string_fld");
	sv.mgptxte03.appendClassString("output_txt");
	sv.mgptxte03.appendClassString("highlight");
%>
<%	sv.mgptxte04.setClassString("");%>
<%	sv.mgptxte04.appendClassString("string_fld");
	sv.mgptxte04.appendClassString("output_txt");
	sv.mgptxte04.appendClassString("highlight");
%>
<%	sv.mgptxte05.setClassString("");%>
<%	sv.mgptxte05.appendClassString("string_fld");
	sv.mgptxte05.appendClassString("output_txt");
	sv.mgptxte05.appendClassString("highlight");
%>
<%	sv.mgptxte06.setClassString("");%>
<%	sv.mgptxte06.appendClassString("string_fld");
	sv.mgptxte06.appendClassString("output_txt");
	sv.mgptxte06.appendClassString("highlight");
%>

<%	sv.mgptxte07.setClassString("");%>
<%	sv.mgptxte07.appendClassString("string_fld"); %>
<% sv.mgptxte07.appendClassString("input_txt");%>
<%	sv.mgptxte07.appendClassString("highlight");
%>
<!-- IFSU-305 commented the code   -->
<%-- <%=smartHF.getRichText(1.5, 5, fw, sv.mgptxte07,(sv.mgptxte07.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%> --%>


	<%
{
		if (appVars.ind60.isOn()) {
			sv.mgptxte07.setInvisibility(BaseScreenData.INVISIBLE);
			sv.mgptxte07.setEnabled(BaseScreenData.DISABLED);
		}
	}

	%>
<div class="panel panel-default">
        <div class="panel-body">     
            <div class="row">
                <div class="col-md-6"> 
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte01%></div>
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte02%></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <div class="form-group">
                        <input type='checkbox' name='mgptxte07' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(mgptxte07)' onKeyUp='return checkMaxLength(this)'    
						<%
						if((sv.mgptxte07).getColor()!=null){%>
						  style='background-color:#FF0000;'
						 <%}
						if((sv.mgptxte07).toString().trim().equalsIgnoreCase("Y")){%>
						  checked
						<% }if((sv.mgptxte07).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
						   disabled
						<%}%>
						   class ='UICheck' onclick="handleCheckBox('mgptxte07')"/>
						<input type='checkbox' name='mgptxte07' value='N' 
							<% if(!(sv.mgptxte07).toString().trim().equalsIgnoreCase("Y")){%>
							checked
						<% }%>
							style="visibility: hidden" onclick="handleCheckBox('mgptxte07')"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte03%></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte04%></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte05%></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <div  id='mgptxte01' onHelp='return fieldHelp("mgptxte01")'><%= sv.mgptxte06%></div>
                </div>
            </div>
        </div>
</div>

<%}%>
<!-- <div class="pagebutton">
<ul class='clear'>
<li><img src='<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/pagebtn_bg_01.png' width='84' height='22'></li>
<li><a id='continuebutton' name='continuebutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)">
<img src='<%= request.getContextPath() %>/screenFiles/<%=imageFolder%>/btn_continue.png' width='84' height='22' alt='<%=resourceBundleHandler.gettingValueFromBundle("Continue")%>'></a></li><li><img src='<%= request.getContextPath() %>/screenFiles/pagebtn_bg_02.png' width='84' height='22'></li>
</ul>
</div> -->


<%@ include file="/POLACommon2NEW.jsp"%>
