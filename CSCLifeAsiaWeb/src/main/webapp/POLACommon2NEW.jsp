<%@ page import="com.resource.ResourceBundleHandler"%>
<%@ include file="commonScript2NEW.jsp"%>
<input type="hidden" id="businessDate" value="<%=AppVars.getInstance().getBusinessdate()%>" />
	</div><!-- end of div container -->
</div><!-- end of div mainareaDiv -->
	<div id="navigateButton" class="text-right">
		<%if(!fw.getScreenName().equals("S0017")){%>
			<%if(fw.getScreenName().equals("Msgbox")){%>
				<%=getBottomButtons(new String[]{"PFKEY12 "})%> 
			<%} 
			else if(fw.getScreenName().equals("S2145")) {%>
				<%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY05 "})%>
			<%} 
			 else if(fw.getScreenName().equals("St216")) {%>
				  <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY05 ",  "PFKEY12 "})%>
			<%} 
			else if(fw.getScreenName().equals("Sr289")) {%>
				 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY0 ", "PFKEY05 ",  "PFKEY12 "})%>
			<%}
			else if(fw.getScreenName().equals("S5684")) {%>
			 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY0 ", "PFKEY05 ",  "PFKEY12 "})%>
		<%}
			else if(fw.getScreenName().equals("S5685")) {%>
			 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY0 ", "PFKEY05 ",  "PFKEY12 "})%>
		<%}
			else if(fw.getScreenName().equals("S5686")) {%>
			 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY0 ", "PFKEY05 ",  "PFKEY12 "})%>
		<%}
			else if(fw.getScreenName().equals("S6326")) {%>
			 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY05 ",  "PFKEY0 "})%>
		<%}	
			else if(fw.getScreenName().equals("S2201")) {%>
			 <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY05 ",  "PFKEY0 "})%>
		<%}	
			 else {%>
				<%=getBottomButtons(fw.getFormActions())%>
			<%} %>
		<%}%>
	</div>	
	<% ResourceBundleHandler bundle = new ResourceBundleHandler(request.getLocale());%>
 <input type="text" id="continue_btn" value="<%=bundle.gettingValueFromBundle("Continue")%>" style="visibility: hidden;margin-top: -120px !important;">
 <input type="text" id="refresh_btn" value="<%=bundle.gettingValueFromBundle("Refresh")%>" style="visibility: hidden;">
 <input type="text" id="exit_btn" value="<%=bundle.gettingValueFromBundle("Exit")%>" style="visibility: hidden;">
 <input type="text" id="previous_btn" value="<%=bundle.gettingValueFromBundle("Previous")%>" style="visibility: hidden;"> 
<script type="text/javascript">		
	function kill(language) {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm(language,"No00031"));
		if (x == true) {
			// fix bug147
			top.frames["heartbeat"].frames["heart"].clearTimeout(
				top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}
	$(document).ready(function() {
		<%if(fw.getScreenName().equals("S1661")){%>
			$('.panel.panel-default').css('overflow-x', 'auto');
		<%} %>
		var continueBtnName = document.getElementById('continue_btn').value;
		var refreshBtnName = document.getElementById('refresh_btn').value;
		var exitBtnName = document.getElementById('exit_btn').value;
		var previousBtnName = document.getElementById('previous_btn').value;
		if(continueBtnName){			
			$("#continuebutton").text(continueBtnName);
		}
		if(refreshBtnName){			
			$("#refreshbutton").text(refreshBtnName);		
		}
		if(exitBtnName){			
			$("#exitbutton").text(exitBtnName);	
		}
		if(previousBtnName){			
			$("#previousbutton").text(previousBtnName);	
		}	
	});
</script>
<%!
	String getBottomButtons(String[] sa)
	{
		StringBuffer ret = new StringBuffer();
		String language = "Eng";
		if (AppVars.getInstance() != null) {
			if (AppVars.getInstance().getUserLanguage() != null) {
				language = AppVars.getInstance().getUserLanguage().toString();
			}
		}
		
		boolean isExitDis = false;
		boolean isRefreshDis = false;
		boolean isPreviousDis = false;
		if (sa != null) {
			for (int i = 0; i < sa.length; i++) {
				if (sa[i].indexOf("PFKEY03") >= 0) {
					isExitDis = true;
				}
				if (sa[i].indexOf("PFKEY05") >= 0) {
					isRefreshDis = true;
				}
				if (sa[i].indexOf("PFKEY12") >= 0) {
					isPreviousDis = true;
				}
			}
			 

		}
		if (isExitDis) 
		{
			ret.append("<button id='exitbutton' style='margin-right: 2px !important;' type='button' class='btn btn-danger btn-sm' onClick=\"doAction('PFKEY03');\">Exit</button>");
		}
		if (isPreviousDis) 
		{
			ret.append("<button id='previousbutton' style='margin-right: 2px !important;' type='button' class='btn btn-primary btn-sm' onClick=\"doAction('PFKEY12');\">Previous</button>");
		}	
		if (isRefreshDis) 
		{
			ret.append("<button id='refreshbutton' type='button' class='btn btn-sm' style='color: #fff;;background-color: #5bc0de;border-color: #46b8da; margin-right: 2px !important;' onClick=\"doAction('PFKEY05');\">Refresh</button>");
		}	

		ret.append("<button id='continuebutton' style='margin-right: 2px !important;' type='button' class='btn btn-success btn-sm' onClick=\"doAction('PFKEY0');\">Continue</button>");

		return ret.toString();
	}
%>


<!-- Move following functions from POLACommon3.jsp here-->
<script type="text/javascript">
var selectedRow1;
var firstTimeOver=false;
var checkedValue='1'; //IJTI-1224

function selectedRow(idt){

	document.getElementById(idt).value=checkedValue; //IJTI-1224
	document.getElementById(idt).selected=true;

	if(firstTimeOver && 
	   idt != selectedRow1 && 
	   document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value=' ';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;
	changeRemoveBtnStatus();
}

function perFormOperation(act){
	if(selectedRow1!=null && selectedRow1!=""){
		document.getElementById(selectedRow1).value=act;
		doAction('PFKEY0'); 
	}else{
		var elementSelected=false;
		for (var index = 0; index < idRowArray.length; ++index) {
			var item = idRowArray[index];
			if(item!=null && item!=""){
				document.getElementById(item).value=act;
				elementSelected=true;
			}
		}
		if(elementSelected){
			doAction('PFKEY0'); 
		}
	}
}

var idRowArray=new Array();
var radioOptionElementId;
var radioElementCounter;
function selectedRowChecked(idt1,idt2,count)
{
	count=count-1;
		if(document.getElementById(idt2).type!='radio'){
			
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value=checkedValue; //IJTI-1224
				idRowArray[count]=idt1;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
			}
		}else{
		
			if(radioOptionElementId!=null && radioOptionElementId!=""){
				var item = idRowArray[radioElementCounter]; 
				if(item!=null && item!=""){
					if(document.getElementById(radioOptionElementId).checked){
						document.getElementById(item).value=' ';
						document.getElementById(radioOptionElementId).selected=false;
						document.getElementById(radioOptionElementId).checked=false;
						idRowArray[radioElementCounter]="";
						radioOptionElementId="";
					}
				}
			}
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value=checkedValue; //IJTI-1224
				idRowArray[count]=idt1;
				radioElementCounter=count;
				radioOptionElementId=idt2;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
				radioElementCounter="";
				radioOptionElementId="";
			}
		}
}

function setDisabledMoreBtn() {	
			var moreBtnText = jQuery('#load-more').children('a');
			moreBtnText.addClass('disabled'); 
		}
function addRow(tableID) {

	var table = document.getElementById(tableID);

	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var mustInput = false;
	var requiredFieds = new Array();
	var colCount = table.rows[0].cells.length;

	for(var i=0; i<colCount; i++) {

		var newcell	= row.insertCell(i);

		newcell.innerHTML = table.rows[1].cells[i].innerHTML;
		//alert(newcell.childNodes);
		switch(newcell.childNodes[0].type) {
			case "text":
					newcell.childNodes[0].value = "";
					break;
			case "checkbox":
					newcell.childNodes[0].checked = false;
					break;
			case "select-one":
					newcell.childNodes[0].selectedIndex = 0;
					break;
		}
			var field;
			var discrField,discrName;
			if (mustInput == true)
				return false;
			
			if (i+1 < rows-1){
				$(this).next().show();
				$(".dataTable").find("tr").next().show();
			    a[i+1+1] = 1;			
			}else{
				// No more left
				doAction('PFKEY90');
			}
			return false;
	}
}

function deleteRow(tableID) {
	try {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;

	for(var i=2; i<rowCount; i++) {
		var row = table.rows[i];
		var chkbox = row.cells[0].childNodes[0];
		if(chkbox.checked) {
			if(rowCount <= 1) {
				alert("Cannot delete all the rows.");
				break;
			}
			table.deleteRow(i);
			rowCount--;
			i--;
		}


	}
	}catch(e) {
		alert(e);
	}
}

function changeRemoveBtnStatus()
{
	var delAction = false;
	var fixedTable = $(".dataTable").find("tr");
	var rowNum = fixedTable.length;
	
	for(var i=rowNum-1; i>=0; i--){
		$(fixedTable.get(i)).find("td:first input[type='checkbox']").each(function(){			 	
		    if ($(this).is(":checked")){			    	
		    	delAction = true;   
			}
		});
	}
	
	if(delAction){
		$("#subfile_remove").removeAttr("disabled");
	}else{
		$("#subfile_remove").attr("disabled", true);
	}	
}
function doFocus(pFocusObj) {
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
    aForm.focusPrevF.value = aForm.focusField.value;
    aForm.focusField.value = pFocusObj.name;
	aForm.activeField.value = pFocusObj.name;
    lastField = aForm.focusPrevF.value;
    thisField = aForm.focusField.value;
    thisElt = pFocusObj;
    if (pFocusObj.type == "text"  || pFocusObj.type == "password" || pFocusObj.tagName.toUpperCase()=="TEXTAREA" ) {
    	// fix bug786
    	pFocusObj.value = pFocusObj.value.replace(/\s+$/, '');
    	pFocusObj.select();
    }
    ii = getIndexByName(thisField);
    jj = getIndexByName(lastField);
    if (jj == ii + 1) {
      nextOPrev = "PREV";
    }
    else if (jj == ii - 1) {
      nextOPrev = "NEXT";
    }
    else {
      nextOPrev = "";
    }
    lastSelectedF = pFocusObj;
}
</script>
<!-- ILIFE-8863 STARTS-->
<%if(AppVars.getInstance().getAppConfig().isUiEnrichSubfile()){%>
<script>
$(document).ready(function() {
	var sbcount = 0;
	function adjustColumn(){
		$(".table-striped>thead>tr>th").attr("style","text-align:center;");
    	$(".table-striped>tbody>tr>td").removeAttr("style");
    	$(".table-striped>tbody>tr>td>.form-group>div").width("fit-content");
    	$(".table-striped>tbody>tr>td>.form-group>div").children().width("fit-content");
    	$(".table-striped>tbody>tr>td>div:not(.form-group)").width("fit-content");
    	$(".table-striped>tbody>tr>td>div:not(.form-group)").children().not(".radio").width("fit-content");
    	$(".table-striped>tbody>tr>td .ellipsis").css("min-width","50px");
    	$(".table-striped>tbody>tr>td .form_date").css("display", "flex");
    	$(".table-striped").removeAttr("style");
	}
	function calculateHeightDataTable(){
		var residual = 0;
		var result = 0;
		if($('.dataTables_scrollBody').outerHeight(true) > 0){
			$('.dataTables_scrollBody').each(function() {
				residual = ($('.panel-default').outerHeight(true) - $(this).parent().outerHeight())	+ 
						$('#page-header').outerHeight(true) + $('#navigateButton').outerHeight(true);
				result = parent.frames["mainForm"].innerHeight - residual;				
				var minHeight = $(".dataTables_scrollBody>.table-striped>thead").height()+$(".dataTables_scrollBody>.table-striped>tbody>tr:first").height()+20;
				if(result < minHeight){
					$(this).css('max-height',minHeight);
				}else{
					$(this).css('max-height',result);
				}	
			});
		}
	}
	function tableFixedHeader(){
		if($(".dataTables_scrollBody>.table-striped>thead>tr").length>1){
			for (i = 2; i <= $(".dataTables_scrollBody>.table-striped>thead>tr").length; i++) {
				$(".dataTables_scrollBody>.table-striped>thead>tr:nth-child("+i+")").children().css("top",$(".dataTables_scrollBody>.table-striped>thead>tr:nth-child("+(i-1)+")").height());
			}
		}
	}
	if(sessionStorage.getItem("expandSubfile") !== $(".table-responsive").children().attr("id") 
			&& sessionStorage.getItem("exp") !== "expand"){ /* ILIFE-8863 */
		sessionStorage.removeItem("expandSubfile");
	}
	$("#navigateButton").children().click(function(){
		sessionStorage.removeItem("expandSubfile");
	})
	var subfile = $(".table-responsive").parent().closest(".row");
	 $(subfile).each(function() {
		 $(this).children().first().prepend('<div class="row">'+
					'<div class="col-md-11"></div>'+
					'<div id="sf-expand" class="col-md-1">'+
						'<span id="expand" class="glyphicon glyphicon-triangle-top" style="border-radius: 5px; background-color: #d9edf7; border: 1px solid #ccc; cursor: pointer; width:17px; font-size: 15px; float: right"></span>'+
						'<span id="colapse" class="glyphicon glyphicon-triangle-bottom" style="border-radius: 5px; background-color: #d9edf7; 	border: 1px solid #ccc; cursor: pointer; width:17px;font-size: 15px; float: right"></span>'+
					'</div>'+
				'</div>');
		  });
		
	$(window).resize(function(){
		adjustColumn();	
		if(sbcount%2 == 0){
			calculateHeightDataTable();
		}
		tableFixedHeader();		
	});
	$(".dataTables_scroll").ready(function(){	
		$(window).trigger('resize');
		if(sessionStorage.getItem("expandSubfile")){
			$("#sf-expand>span").trigger("click");
		}
	});
	$(".dataTables_paginate").on("click",function(){
		$(".dataTables_scroll").ready(function(){
			adjustColumn();
			tableFixedHeader();		
		});
	});
	$('span[id="colapse"]').css("display","none");
	//ILIFE-8863 START
    $(".input-sm").on("change",function(){
        adjustColumn();	
	});
    //ILIFE-8863 END
	$("#sf-expand>span").off("click").on("click",function(){
		sbcount ++;
		var sfClick = $(this).parents().closest(".row").first();
		if(sbcount%2 != 0){
			sessionStorage.setItem("expandSubfile", $(".table-responsive").children().attr("id"));
			sfClick.find(".dataTables_scrollBody").css("max-height", parent.frames["mainForm"].innerHeight  - (sfClick.innerHeight() - $(".dataTables_scrollBody").innerHeight()) - $("#navigateButton").outerHeight(true));		
			sfClick.attr("style","position: fixed; width:99%; padding: 0 !important; background-color:#fff; top:0; z-index:999; left: 25px; height: "+parent.frames["mainForm"].innerHeight);	
			window.top.document.getElementsByName("realContent")[0].cols = "0,*";
			adjustColumn();
			$('span[id="expand"]').css("display","none");
			$('span[id="colapse"]').css("display","block");
			$("#navigateButton").css({"position": "fixed", "bottom": "0", "z-index":"999", "width":"97%"});
			sessionStorage.setItem("exp", "expand"); /* ILIFE-8863 */
		}
		else{
			sessionStorage.removeItem("expandSubfile");
			$('span[id="expand"]').css("display","block");
			$('span[id="colapse"]').css("display","none");
			$("#navigateButton").css({"position": "unset", "bottom": "unset", "width":"unset"});
			sfClick.removeAttr("style");
			sessionStorage.setItem("exp", "colapse"); /* ILIFE-8863 */
			if(sessionStorage.getItem("sidebar") === "pined"){
				window.top.document.getElementsByName("realContent")[0].cols = "275,*";
			}
			calculateHeightDataTable();
		}  		
	});
});
</script>
<%}%>
<!-- ILIFE-8863 ENDS-->
<%-- IBPTE-1528 starts --%>
<script>
$(document).ready(function() {
	$("#mainForm_OPTS").ready(function() {
		$("#mainForm_OPTS").css({"visibility": "hidden", "position": "fixed", "z-index": -1});
		let extraInfoLinksDiv = $("#mainForm_OPTS");
		loadExtraInfo(extraInfoLinksDiv[0]);
	});
});
</script>
<%-- IBPTE-1528 ends --%>