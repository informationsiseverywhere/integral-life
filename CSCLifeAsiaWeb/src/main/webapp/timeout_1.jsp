<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%@ page language="java" pageEncoding="UTF-8" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@ page contentType="text/html; charset=UTF-8" %>            
<HTML>                                                            
<HEAD>                                                            
<title> LOGOUT PAGE </title>
<%
	ResourceBundleHandler resourceBundle = new ResourceBundleHandler(request.getLocale());
	String title=resourceBundle.gettingValueFromBundle("Timeout Page");
 %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
</HEAD>                                                            
  <BODY class="main">
      <table class="main_table">
        <tr>
          <td class="page_heading" align="left" ><%=title %> </td>
        </tr>
      </table>
  </BODY>
</HTML>
