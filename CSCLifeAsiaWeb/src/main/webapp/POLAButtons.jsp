<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<HTML>
<head>  
<%@ page session="false" %>
<title>Untitled Document</title>             
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">   
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
<%@ page import="com.quipoz.framework.util.*" %>  
<%@ page import="com.quipoz.framework.screenmodel.*" %>  
</head> 
<%String ctx = request.getContextPath() + "/";%>
 <%BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  String lang1  = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase(); 
%>
<BODY class="main" onload="doResize();" onKeyDown="return false;" <%="onResize='doResize();'"%>>
<script src=js/Sidebar.js></script>
<script>    
	function doResize() {
		if (window.dialogArguments != null) {
			return;
		}
	    xsize = screen.width;
	    ysize = screen.height;
	    asize = document.body.offsetWidth;
	    bsize = document.body.offsetHeight;
	    pct   = 100;
	    if (xsize >= 1024) {
	    	pct = 130;
	    }
	    document.body.style.fontSize = (asize * pct / xsize) + "%";
	    /* alert("Result " + document.body.style.fontSize); */
	}
	function kill() {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm('<%=lang1%>',"No00031"));
		if (x == true) {
			window.showModalDialog ('KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');
			parent.location.replace('<%=ctx%>');
		}
	}
</script>
<div style="position:absolute; top:0; height:100%;">
<table cellspacing="0" cellpadding="0">
	<tr height=80%>
		<td width="*">&nbsp;</td>
		<td align="center" width="10%">
			<a href=<%="''"%> onclick="javaScript:callCommonAlert('<%=lang1%>','No00030');return false;"><img src="images/print.gif" border="0" height="100%"/></a>
		</td>
		<td align="center" width="10%">
			<a href=<%="''"%> onclick="javaScript:callCommonAlert('<%=lang1%>','No00022');return false;"><img src="images/help.gif" border="0" height="100%"/></a>
		</td>
		<td align="center" width="10%">
			<a href=<%="''"%> onclick="javaScript:kill();return false;"><img src="images/StopControl.bmp" border="0" height="100%"/></a>
		</td>
		<td align="center" width="*">
			<%HttpSession sess = request.getSession(false);
			  if (sess == null) return;
			  BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE );
			  if ( baseModel == null) return;
			  ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
			  if (fw != null) {%>
			  	<%=AppVars.hf.getHTMLFormActionButtonsHorizontal(fw.getFormActions())%>
			<%}%>
		</td>
	</tr>
</table>
</div>
</BODY>
</HTML>