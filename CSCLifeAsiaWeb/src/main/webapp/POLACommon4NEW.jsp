<%@ include file="commonScript4NEW.jsp" %>
<%!
	String getBottomButtons(String[] sa)
	{
		StringBuffer ret = new StringBuffer();
		String language = "Eng";
		if (AppVars.getInstance() != null) {
			if (AppVars.getInstance().getUserLanguage() != null) {
				language = AppVars.getInstance().getUserLanguage().toString();
			}
		}
		
		boolean isExitDis = false;
		boolean isRefreshDis = false;
		boolean isPreviousDis = false;
		if (sa != null) {
			for (int i = 0; i < sa.length; i++) {
				if (sa[i].indexOf("PFKEY03") >= 0) {
					isExitDis = true;
				}
				if (sa[i].indexOf("PFKEY05") >= 0) {
					isRefreshDis = true;
				}
				if (sa[i].indexOf("PFKEY12") >= 0) {
					isPreviousDis = true;
				}
			}
	
		}
		
		ret.append("<div class='btn-group'>");
		
		if (isExitDis) 
		{
			ret.append("<button type='button' class='btn btn-danger' onClick=\"doAction('PFKEY03');\">Exit</button>");
		}
		if (isPreviousDis) 
		{
			ret.append("<button type='button' class='btn btn-primary' onClick=\"doAction('PFKEY12');\">Previous</button>");
		}		
		if (isRefreshDis) 
		{
			ret.append("<button type='button' class='btn btn-info' onClick=\"doAction('PFKEY05');\">Refresh</button>");
		}			
		
		ret.append("<button id='continuebutton' type='button' class='btn btn-success' onClick=\"doAction('PFKEY0');\">Continue</button>");
		ret.append("</div>");
		
		return ret.toString();
	}
%>
 <div class="col-lg-12 text-right">
           		<h5><%=fw.getScreenName()%>&nbsp;&nbsp;&nbsp;<%=AppConfig.getInstance().getVersion() %></h5>
           </div>
        <input type="hidden" id="businessDate" value="<%=AppVars.getInstance().getBusinessdate()%>" />
	</div><!-- end of div container -->
</div><!-- end of div mainareaDiv -->