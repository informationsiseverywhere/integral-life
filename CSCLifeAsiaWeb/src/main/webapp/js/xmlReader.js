//add by Cary zhong 06/02/2010 start
var   xmlDoc; 
function parseXML(language){
	try{//IE
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
	}catch(e){
		try{
			// ILIFE-4957 start - The codes are used to load alertMessages_xxx.xml file on Chrome
			//Chrome
			if(window.chrome && !window.opera){
				 var xmlhttp = new XMLHttpRequest();
				 var xmlnam = "alertMessages_"+language;
				 /*  xmlhttp.open('GET', contextPathName+"/js/"+xmlnam+".xml", false);*/
				/* IJTI-932 START*/
				 var res= encodeURIComponent(contextPathName+"/js/"+xmlnam+".xml");
				  xmlhttp.open('GET', res, false);
				  /* IJTI-932 END*/
		         xmlhttp.setRequestHeader('Content-Type', 'text/xml');
		         xmlhttp.send();
		         xmlDoc = xmlhttp.responseXML;
		         return;
			}
			// ILIFE-4957 end
			//Firefox, Opera, etc.
			xmlDoc=document.implementation.createDocument("","",null);
		}catch(e){
			return;
		}
	}
	xmlDoc.async=false;
	// ILIFE-4957 start
	/*var messagesPath = "/js/alertMessages_"+language+".xml";
	xmlDoc.load(getContextPathName() + messagesPath);*/
	var locHref = location.href;
	var locArray = locHref.split("/");
	var xmlnam = "alertMessages_"+language;
	xmlDoc.load(contextPathName+"/js/"+xmlnam+".xml");
	// ILIFE-4957 end
	xmlDoc.async=false;
}

// ILIFE-151
function getContextPathName() {
	// contextPathName should be available;
	// currently it is defined in commonScript1.jsp
	return contextPathName;
}

function getmessageAlert(alertCode,language){		
	parseXML(language);
	if (language == null || language == "" || language == undefined){language = "ENG";}
	var  cNodes=xmlDoc.getElementsByTagName("alertMessages")[0];
	cNodes=cNodes.getElementsByTagName(alertCode)[0];
	//cNodes=cNodes.getElementsByTagName(language)[0];
	return cNodes.firstChild.nodeValue;
}
String.prototype.trimSpp = function() { var t = this.replace(/(^\s*)|(\s*$)/g, "");    
return t.replace(/(^ *)|( *$)/g, "");}
function callCommonAlert(){
	//ILIFE-4957 start
	if(document.getElementById('polaModal').style.display == "block")
		return;
	//ILIFE-4957 end
	try{
		var language = arguments[0].trimSpp().substr(0,3).toUpperCase();
		var alertCode = arguments[1].trimSpp();
		var msg = getmessageAlert(alertCode,language);
		for(var i=2;i<arguments.length;i++){
			msg = msg.replace("$"+(i-1),arguments[i]);
		}
		//ILIFE-4957 start
		//alert(msg);
		document.getElementById('errorMsg').innerHTML = " " + msg;
		document.getElementById('polaModal').style.display = "block"; 
		//ILIFE-4957 end
	}catch(e){//alert(e.message);
		return;
	}
}
function callCommonConfirm(){
	try{
		var language = arguments[0].trimSpp().substr(0,3).toUpperCase();
		var alertCode = arguments[1].trimSpp();
		var msg = getmessageAlert(alertCode,language);
		for(var i=2;i<arguments.length;i++){
			msg = msg.replace("$"+(i-1),arguments[i]);
		}
		return msg;
	}catch(e){//alert(e.message);
		return;
	}
}
//add by Cary zhong 06/02/2010 end