
var FS=yx_getFS(), yx_frame=1, yx_loaded=0;
var yx_sX=0, yx_sY=0, yx_oW=0, yx_oH=0, yx_wW=0, yx_wH=0, yx_winEvent="";
var yx_winOver=yx_winOut=yx_docOver=yx_docOut=yx_winDown=yx_winUp=yx_docDown=yx_docUp=yx_winClick=yx_docClick=null;
var showMenu=FS.showMenu, showMenuX=FS.showMenuX, hideMenu=FS.hideMenu, hideMenuX=FS.hideMenuX, hideAll=FS.hideAll, hideAllX=FS.hideAllX, moveMenuTo=FS.moveMenuTo, moveMenuBy=FS.moveMenuBy, getMenuDim=FS.getMenuDim, moveMenuBack=FS.moveMenuBack, openMenu=FS.openMenu; openMenuX=FS.openMenuX; closeMenu=FS.closeMenu; closeMenuX=FS.closeMenuX;
var hasMenu=FS.yx_hasMenu, addMenu=FS.addMenu, addSubMenu=FS.addSubMenu, addLink=FS.addLink, addCommand=FS.addCommand, addInfo=FS.addInfo, addSeparator=FS.addSeparator, endMenu=FS.endMenu, addInstance=FS.addInstance, setSubID=FS.yx_subID, setSubFrame=FS.yx_subFrame;
var addStylePad=FS.addStylePad, addStyleItem=FS.addStyleItem, addStyleFont=FS.addStyleFont, addStyleTag=FS.addStyleTag, addStyleSeparator=FS.addStyleSeparator, addStyleMenu=FS.addStyleMenu, addStyleGroup=FS.addStyleGroup;
var addItemEvent=FS.addItemEvent, addMenuEvent=FS.addMenuEvent;
var addImage=FS.addImage, appendImage=FS.appendImage, setImage=FS.setImage, setIcon=FS.setIcon, setBGImage=FS.setBGImage;
var yx_fMs=new Array(), yx_OMs=new Array();

function yx_getFS() {
  var _fs=parent;
  while (_fs!=top) {
    if (typeof(_fs.yx_menuSafe)!="undefined") return _fs;
    _fs=_fs.parent;
  }
  return top;
}
function _delFrame() {
  if (FS.yx_delFrame) { // IE/Mac
    for (var i=0; i<yx_fMs.length; i++) {
      FS.yx_delFrame(self,yx_fMs[i][0],yx_fMs[i][1]); 
    }
  }

  yx_otherUnload();
}
function _resizeIt() {
  if (yx_winEvent!="") { eval(yx_winEvent); }
  if (FS.yx_isN4 && (window.innerWidth!=yx_oW || window.innerHeight!=yx_oH)) {
    self.history.go(0);
  }

  for (var i=0; i<yx_fMs.length; i++) {
    if (yx_fMs[i][1]=="top" || yx_fMs[i][1]=="all") {
      FS.yx_resizeIt(yx_fMs[i][0]);
    }
  }

  yx_otherResize();
}
function _clickIt(e) {
  if (FS.yx_isN4) {
    if (typeof(e.target.yxIden)!="undefined" && e.target.yxIden==FS.yx_isItem) {
      e.target.handleEvent(e);
    }
    else if (typeof(e.target.yxItem)!="undefined" && e.target.yxItem.yxIden==FS.yx_isItem) {
      e.target.yxItem.handleEvent(e);
    }
    else {
      if (e.type=="mouseover") {
        yx_winOver(e); yx_docOver(e);
        e.target.handleEvent(e);
      }
      else if (e.type=="mouseout") {
        yx_winOut(e); yx_docOut(e);
        e.target.handleEvent(e);
      }
      else if (e.type=="mousedown") {
        yx_winDown(e); yx_docDown(e);
      }
      else if (e.type=="mouseup") {
        FS.yx_clickIt();
        yx_winUp(e); yx_docUp(e);
        if (e.target=="") {
          yx_winClick(e); yx_docClick(e);
        }
      }
    }
  }
  else if (FS.yx_isN6) {
    if (e.type=="click") {
      var isItem=FS.yx_isItem;
      FS.yx_clickIt();
      if (!isItem) { yx_docClick(e); yx_winClick(e); }
    }
    else if (e.type=="mousedown" && !FS.yx_isItem) {
      yx_docDown(e); yx_winDown(e);
    }
    else if (e.type=="mouseup" && !FS.yx_isItem) {
      yx_docUp(e); yx_winUp(e);
    }
  }
  else if (FS.yx_isK3) {
    FS.yx_clickIt();
    yx_docClick(); yx_winClick();
  }
  else {
    FS.yx_clickIt();
    yx_docClick();
  }
}
function _eval(code) { eval(code); }

function addWindowEvent(e_handler) { yx_winEvent=e_handler; }

function initMenu2(ins,mode) {
  yx_loaded=1;
  if (FS.yx_menuSafe && FS.yx_goodI) {
    yx_fMs[yx_fMs.length]=new Array(ins,mode);

    window.onunload=_delFrame;
    window.onresize=_resizeIt;

    FS.yx_regMenu(self,ins,mode)
    FS.yx_showMsg("Menu activated");
  }
}

function initSub(ins) { yx_loaded=1; FS.yx_setSub(self,ins); }
function clickMenu(n) { FS.clickMenu(self,n); }
function clickMenuX(n) { FS.clickMenuX(self,n); }

function yx_void() { return true; }

if (FS.yx_isN4) {
  yx_oW=window.innerWidth; yx_oH=window.innerHeight;

  window.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP | Event.MOUSEOVER | Event.MOUSEOUT);
  document.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP | Event.MOUSEOVER | Event.MOUSEOUT);

  yx_winOver=window.onmouseover?window.onmouseover:yx_void; yx_winOut=window.onmouseout?window.onmouseout:yx_void;
  yx_docOver=document.onmouseover?document.onmouseover:yx_void; yx_docOut=document.onmouseout?document.onmouseout:yx_void;
  window.onmouseover=_clickIt; window.onmouseout=_clickIt; document.onmouseover=null; document.onmouseout=null;
}

if (FS.yx_isN4 || FS.yx_isN6) {
  yx_winDown=window.onmousedown?window.onmousedown:yx_void; yx_winUp=window.onmouseup?window.onmouseup:yx_void; yx_winClick=window.onclick?window.onclick:yx_void;
  yx_docDown=document.onmousedown?document.onmousedown:yx_void; yx_docUp=document.onmouseup?document.onmouseup:yx_void; yx_docClick=document.onclick?document.onclick:yx_void;
}

if (FS.yx_isN4) {
  window.onmousedown=_clickIt; window.onmouseup=_clickIt;
  document.onmousedown=null; document.onmouseup=null;
}
else if (FS.yx_isN6) {
  document.onmousedown=_clickIt; document.onmouseup=_clickIt; document.onclick=_clickIt;
  window.onmousedown=null; window.onmouseup=null; window.onclick=null;
}
else if (FS.yx_isK3) {
  yx_winClick=window.onclick?window.onclick:yx_void; yx_docClick=document.onclick?document.onclick:yx_void;
  document.onclick=_clickIt; window.onclick=null;
}
else {
  yx_docClick=document.onclick?document.onclick:yx_void;
  document.onclick=_clickIt;
}

var yx_otherResize=window.onresize?window.onresize:yx_void;
var yx_otherUnload=window.onunload?window.onunload:yx_void;
