

var yxIdx=0, yx_iframe=0, yx_hasMouseEnter=(yx_isIE55||yx_isIE6);

function yx_createEle(f,tag) { return f.document.createElement(tag); }
function yx_makeIMG(f,_src,w,h,_align) {
  var xx=yx_createEle(f,"IMG");
  with (xx) { src=_src; width=w; height=h; border=0; hspace=0; vspace=0; align=_align; }
  return xx;
}
function yx_makeLayer(f,x,y,h,vis,idx) {
  var xx=yx_createEle(f,"DIV"), _h=h==null?f.document.body:h;
  with (xx.style) { position="absolute"; visibility=vis; left=x+"px"; top=y+"px"; zIndex=idx; }
  if (yx_isMac) {
    _h.appendChild(xx);
  }
  else {
    _h.insertBefore(xx,_h.firstChild);
  }
  return xx;
}

function yx_getDTD(f) {
  if (typeof(f.yx_isDTD)=="undefined") {
    f.yx_isDTD=(f.document.doctype?(f.document.doctype.name.indexOf(".dtd")!=-1):false) || (f.document.compatMode?(f.document.compatMode=="CSS1Compat"):false);
  }
  return f.yx_isDTD;
}
function yx_docBody(f) { return yx_getDTD(f)?f.document.documentElement:f.document.body; }
function yx_setZIndex(l,idx) { l.style.zIndex=idx; }
function yx_getZIndex(l) { return l.style.zIndex; }
function yx_showLayer(l) { 
	l.style.visibility="visible"; 
	/* modified  ILIFE-1328 start  xma3*/
	$(l).css("overflow","hidden");	
	$(l).find("div").each(function(){
		var backImage = $(this).css("background-image");
		if(backImage != null && backImage.indexOf("url")!=-1){
			 $(this).css("background-repeat","no-repeat");
		}
	});
	/* modified  ILIFE-1328 end   xma3*/
	}
function yx_hideLayer(l) { l.style.visibility="hidden"; }
function yx_moveLayerTo(l,x,y) { l.style.left=x+"px"; l.style.top=y+"px"; }
function yx_moveLayerBy(l,x,y) { l.style.left=(parseInt(l.style.left)+x)+"px"; l.style.top=(parseInt(l.style.top)+y)+"px"; }	
function yx_findLayer(f,n) { return f.document.getElementById(n); }
function yx_getWinWidth(f) { return (yx_docBody(f)).clientWidth; }
function yx_getWinHeight(f) { return (yx_docBody(f)).clientHeight; }
function yx_getScrollX(f) { return (yx_docBody(f)).scrollLeft; }
function yx_getScrollY(f) { return (yx_docBody(f)).scrollTop; }
function yx_getLeft(l) { return l.offsetLeft+(l.offsetParent?yx_getLeft(l.offsetParent):yx_isMac?parseInt(document.body.leftMargin):0); }
function yx_getTop(l) { return l.offsetTop+(l.offsetParent?yx_getTop(l.offsetParent):yx_isMac?parseInt(document.body.topMargin):0); }

function yx_stopEvent(f) {
	/*ILIFE-1328 yzhong4 start*/
	//f.event.cancelBubble=true;
	if(event != null){
		event.cancelBubble=true;
	}else if(f.event != null){
		f.event.cancelBubble=true;
	}else{
		var event = arguments.callee.caller.arguments[0];
		event.stopPropagation();
	}
	/*ILIFE-1328 yzhong4 end*/
}

function yx_overOther() { yx_stopAll(); yx_onItem++; }
function yx_outOther() {
  if (--yx_onItem==0) {
    yx_stopAll();
    if (!this.sticky) { yx_setIt(); }
  }
}
function yx_clickOther() { yx_stopEvent(yx_getFrame(this.menu)); }

function yx_overItem() {
  var item=this.item, menu=item.lead;

  if (menu.shown) {
    yx_onItem++;
    yx_stopAll();
    yx_showMsg(item.content.msg);

    if (this.item.item!=null) {
      this.markIt(); yx_upIt(item); yx_downIt(item);
    }
    else if (yx_isMac) {
      yx_cleanCache(menu.head.subFrame,menu.head.iden);
    }

    yx_iTimer=yx_callBack(item,"onIt","()",(menu.bar?menuTimerBar:menuTimer));
  }
}

function yx_downItem() {
  var item=this.item, type=item.content.type, sItem=item.styleI.sItem, sFont=item.styleI.sFont, iis=item.item.style;

  yx_stopEvent(yx_getFrame(item.lead));
  yx_stopIt(yx_sTimer); yx_sTimer=0;

  if (!sItem.onOver || type!=yx_M) {
    if (sFont.fColorC!="") { iis.color=sFont.fColorC; }
    if (sItem.iColorC!="none") { iis.backgroundColor=sItem.iColorC; }
    if (sItem.bSize>0 && sItem.bStyleC!="" && sItem.bColorC!="") { iis.borderColor=sItem.bColorCT+" "+sItem.bColorCR+" "+sItem.bColorCB+" "+sItem.bColorCL; }
  }
}

function yx_clickItem() {
  var item=this.item, content=item.content, type=content.type, menu=item.lead, f=yx_getFrame(menu);

  yx_stopEvent(f);

  if (type==yx_M || type==yx_L) {
    if (!item.styleI.sItem.onOver && type==yx_M && menu.head.subReady) {
      if (item.menu.shown) {
        item.menu.hideM(false); menu.open=false; this.markIt();
      }
      else if (!item.menu.go) {
        menu.open=true;
        yx_stopIt(yx_iTimer); yx_iTimer=yx_callBack(item,"onIt","()",0);
      }
    }
    else if (content.link!="") {
      yx_closeIns(null);
      yx_goLink(f,content.link,content.mode,menu.head.target)
    }
  }
  else if (content.code!="") {
    yx_closeIns(null);
    yx_doCode(f,content.code,content.mode);
  }
}

function yx_outItem() {
  var item=this.item;

  if (--yx_onItem==0) {
    yx_stopAll();
    if (!this.sticky) { yx_setIt(); }
  }
  if (item.code2!="") {
    yx_doCode(yx_getFrame(item.lead),item.code2,item.mode);
  }
}

function yx_makeS(w,h) {
  var lead=this.lead, sSeparator=lead.style.sSeparator, holder=lead.holder, sStr="";

  if (w==2 || w==1) {
    sStr='<table width="'+w+'" height="'+h+'" cellpadding="0" cellspacing="0" border="0"><tr><td bgcolor="'+sSeparator.color1+'">'+yx_sPixel+'</td>'+(w==2?('<td bgcolor="'+sSeparator.color2+'">'+yx_sPixel+'</td>'):'')+'</tr></table>';
  }
  else {
    sStr='<table width="'+w+'" height="'+h+'" cellpadding="0" cellspacing="0" border="0"><tr><td bgcolor="'+sSeparator.color1+'">'+yx_sPixel+'</td></tr>'+(h==2?('<tr><td bgcolor="'+sSeparator.color2+'">'+yx_sPixel+'</td></tr>'):'')+'</table>';
  }
  var xx=yx_makeLayer(yx_getFrame(lead),0,0,holder,"inherit",2); xx.innerHTML=sStr;

  this.width=w; this.height=h; this.item=xx; xx=null;
}

function yx_makeC() {
  var lead=this.lead, head=lead.head, content=this.content, type=content.type, f=yx_getFrame(lead), x=this.item;

  if (type!=yx_S && type!=yx_I) {
    if (yx_hasMouseEnter) {
      x.onmouseenter=yx_overItem; x.onmouseleave=yx_outItem;
    }
    else {
      x=yx_makeLayer(f,0,0,lead.holder,"inherit",3);
      with (x.style) { width=this.width+"px"; height=this.height+"px"; }

      var img=yx_makeIMG(f,yx_pImage.src,this.width,this.height,"left");
      x.insertBefore(img,x.firstChild);

      x.onmouseover=yx_overItem; x.onmouseout=yx_outItem;
    }
    x.onmousedown=yx_downItem; x.onmouseup=yx_clickOther; x.onclick=yx_clickItem;
    x.style.cursor="hand";
    x.markIt=yx_markIt;
    x.item=this; x.menu=lead;

    this.cover=x;
  }
  else {
    if (yx_hasMouseEnter) {
      x.onmouseenter=yx_overOther; x.onmouseleave=yx_outOther;
    }
    else {
      x.onmouseover=yx_overOther; x.onmouseout=yx_outOther;
    }
  }
  x.sticky=head.sticky;

  if (!lead.did) {
    var ee=yx_findItemEvent(content.group);
    if (ee!=null) { this.code1=ee.code1; this.code2=ee.code2; this.mode=ee.mode; }

    if (type==yx_M) { this.menu=new yx_menuOBJ(head,this,content); }
  }
}

function yx_makeI() {
  var lead=this.lead, content=this.content, type=content.type, holder=lead.holder, styleM=lead.style, styleI=this.getItemStyle(lead), sTag=styleI.sTag, sItem=styleI.sItem, sFont=styleI.sFont;

  if (type==yx_S) { this.width=styleM.sSeparator.sSize; this.height=styleM.sSeparator.sSize; return; }

  var pW=sItem.pWidth+sItem.bSize, pH=sItem.pHeight+sItem.bSize, f=yx_getFrame(lead);
  var w,h;

  if (lead.bar) {
    w=this.width>0?this.width:sItem.iWidth;
    h=this.height>0?this.height:styleM.sItem.iHeight;
  }
  else {
    w=this.width>0?this.width:styleM.sItem.iWidth;
    h=this.height>0?this.height:sItem.iHeight;
  }

  if (this.item==null || lead.did) {
    var xx=yx_makeLayer(f,0,0,holder,"inherit",2);

    with (xx.style) {
      if (w>0) { width=(lead.isDTD?(w-pW*2):w)+"px"; }
      if (h>0) { height=(lead.isDTD?(h-pH*2):h)+"px"; }

      if (sItem.iColorN!="") { backgroundColor=sItem.iColorN; }
      padding=sItem.pHeight+"px "+sItem.pWidth+"px";
      textAlign=w>0?sFont.fAlign:"left";
      fontSize=sFont.fSize+"px"; fontFamily=sFont.fFamily; fontWeight=sFont.fWeightN; fontStyle=sFont.fStyleN; textDecoration=sFont.textN;
      color=sFont.fColorN;

      if (sItem.bSize>0) {
        borderWidth=sItem.bSize+"px"; borderStyle="solid";
        borderColor=sItem.bColorNT+" "+sItem.bColorNR+" "+sItem.bColorNB+" "+sItem.bColorNL;
      }

      if (sItem.opacity!="") { filter="alpha(opacity="+sItem.opacity+")"; }
    }
  
    var img=content.group!=""?yx_findImage(content.group):null;
    if (img!=null) {
      this.img=yx_makeIMG(f,img.path+img.initial,img.width,img.height,(w>0?sFont.fAlign:"left"));
      xx.insertBefore(this.img,xx.firstChild);
      this.img0=img.path+img.initial; this.img1=img.path+img.swap;
      w=w==0?img.width:w; h=h==0?img.height:h;
    }
    else {
      xx.innerHTML=w>0?content.dis:("<nobr>"+content.dis+"</nobr>"); // IE5.0
    }

    var icon=yx_findIcon(content.group);
    if (icon!=null) {
      this.icon=yx_makeIMG(f,icon.path+icon.initial,icon.width,icon.height,(w>0?icon.align:"left"));
      xx.insertBefore(this.icon,xx.firstChild);
      this.icon0=icon.path+icon.initial; this.icon1=icon.path+icon.swap; this.iconHA=icon.align; this.iconVA=icon.vAlign;
    }

 // if (type==yx_M && sTag.vis) {
 //     this.tag=yx_makeIMG(f,sTag.tPath+sTag.tN,sTag.tWidth,sTag.tHeight,(w>0?sTag.tAlign:"left"));
 //     xx.insertBefore(this.tag,xx.firstChild);
 //     this.tag0=sTag.tPath+sTag.tN; this.tag1=sTag.tPath+sTag.tH;
 //   }

    var imgBG=yx_findBGImage(content.group);
    if (imgBG!=null) {
      this.imgBG0=imgBG.initial==""?"":imgBG.initial=="none"?"url(none)":("url("+imgBG.path+imgBG.initial+")");
      this.imgBG1=imgBG.swap==""?"":imgBG.swap=="none"?"url(none)":("url("+imgBG.path+imgBG.swap+")");
      if (this.imgBG0!="") { xx.style.backgroundImage=this.imgBG0; }
    }
    else {
      this.imgBG0=""; this.imgBG1="";
    }

    if (!lead.did) { this.width=w>0?w:xx.offsetWidth; this.height=h>0?h:xx.offsetHeight; }
    this.item=xx; xx=null;
  }
  else {
    with (this.item.style) {
      width=(lead.isDTD?(this.width-pW*2):this.width)+"px";
      height=(lead.isDTD?(this.height-pH*2):this.height)+"px";
      textAlign=sFont.fAlign;
    }

    if (this.img!=null) { this.img.align=sFont.fAlign; }
    if (this.icon!=null) { this.icon.align=this.iconHA; }
    if (this.tag!=null) { this.tag.align=sTag.tAlign; }
  }

  with (this.item.style) {
    overflow="hidden";
    /* modified by xma3 ILIFE-1328 */
    //clip="rect(0px "+this.width+"px "+this.height+"px 0px)";
  }

  if (sFont.vAlign=="middle") {
    if (this.img!=null) {
      this.img.vspace=Math.floor((this.height-(pH*2+this.img.height))/2); 
    }
    else {
      this.item.style.lineHeight=(this.height-pH*2)+"px";
    }
  }
  if (this.icon!=null && this.iconVA=="middle") {
    this.icon.vspace=Math.floor((this.height-(pH*2+this.icon.height))/2);
  }
  if (this.tag!=null && sTag.vAlign=="middle") {
    this.tag.vspace=Math.floor((this.height-(pH*2+sTag.tHeight))/2);
  }

  this.styleI=styleI;
}

function yx_makeP(w,h) {
  var sPad=this.style.sPad;

  if (!this.did) { this.width=w; this.height=h; }
  with (this.holder.style) { width=w+"px"; height=h+"px"; }

  if (!sPad.vis) { return; }

  var bgImg=yx_findBGImage(this.content.name);
  var pp=yx_makeLayer(yx_getFrame(this),0,0,this.holder,"inherit",1); pp.innerHTML=yx_sPixel;

  with (pp.style) {
    width=(this.isDTD?(w-sPad.bSize*2):w)+"px"; height=(this.isDTD?(h-sPad.bSize*2):h)+"px";
    if (sPad.bSize>0 && sPad.bColor!="") {
      borderWidth=sPad.bSize+"px"; borderStyle="solid";
      borderColor=sPad.bColorT+" "+sPad.bColorR+" "+sPad.bColorB+" "+sPad.bColorL;
    }

    if (sPad.sColor!="") { backgroundColor=sPad.sColor; }
    if (sPad.opacity!="") { filter="alpha(opacity="+sPad.opacity+")"; }
    if (bgImg!=null) { backgroundImage="url("+bgImg.path+bgImg.initial+")"; }
  }

  pp.onmouseover=yx_overOther; pp.onmouseout=yx_outOther;
  pp.onmousedown=yx_clickOther; pp.onmouseup=yx_clickOther; pp.onclick=yx_clickOther;
  pp.sticky=this.head.sticky; pp.menu=this;

  this.pad=pp; pp=null;
}

function yx_makeM() {
  var f=yx_getFrame(this), h=this.head;
  this.getStyle();
  this.holder=yx_makeLayer(f,0,0,null,"hidden",h.z); this.holder.iden=h.iden;
  this.isDTD=yx_getDTD(f);

  var items=this.items, sPad=this.style.sPad, sItem=this.style.sItem, totalW=0, totalH=0, maxW=210, maxH=0, reBuild=false;

  if (this.did) {
    for (var i=0; i<this.itemL; i++) {
      if (items[i].content.type==yx_S) {
        items[i].makeS(items[i].width,items[i].height);
      }
      else {
        items[i].makeI();
      }
      yx_moveLayerTo(items[i].item,items[i].x,items[i].y);

      items[i].makeC();
      if (items[i].cover!=null) {
        yx_moveLayerTo(items[i].cover,items[i].x,items[i].y);
      }
    }
    this.makeP(this.width,this.height);
  }
  else {
    this.bar=(this.lead==h && h.bar || this.lead!=h && sPad.bar)?true:false;

    for (var i=0; i<this.itemL; i++) {
      items[i]=new yx_itemOBJ(this,this.content.items[i]);
      items[i].makeI();
      totalW+=items[i].width; totalH+=items[i].height;
      if (maxW<items[i].width) { maxW=items[i].width; }
      if (maxH<items[i].height) { maxH=items[i].height; }
    }

    if (this.bar) {
      reBuild=true; 
      for (var i=0; i<this.itemL; i++) { items[i].height=maxH; }
    }
    else if (sItem.iWidth==0) {
      reBuild=true;
      for (var i=0; i<this.itemL; i++) { items[i].width=maxW; }
    }

    if (reBuild) {
      totalW=0; totalH=0;
      for (var i=0; i<this.itemL; i++) {
        items[i].makeI();
        totalW+=items[i].width; totalH+=items[i].height;
      }
    }

    var dx=sPad.bSize+sPad.sWidth, dy=sPad.bSize+sPad.sHeight;

    if (this.bar) {
      totalW+=(this.itemL-1)*sPad.iOff;
      this.makeP(totalW+dx*2,maxH+dy*2);
    }
    else {
      totalH+=(this.itemL-1)*sPad.iOff;
      this.makeP(maxW+dx*2,totalH+dy*2);
    }

    var sSize=this.style.sSeparator.sSize;
    for (var i=0; i<this.itemL; i++) {
      if (items[i].content.type==yx_S) {
        if (this.bar) {
          items[i].makeS(sSize,maxH);
        }
        else {
          items[i].makeS(maxW,sSize);
        }
      }
    }

    for (var i=0; i<this.itemL; i++) {
      items[i].makeC();
      items[i].x=dx; items[i].y=dy;
      yx_moveLayerTo(items[i].item,dx,dy);
      if (items[i].cover!=null) {
        yx_moveLayerTo(items[i].cover,dx,dy);
      }
      if (this.bar) {
        dx+=items[i].width+sPad.iOff;
      }
      else {
        dy+=items[i].height+sPad.iOff;
      }
    }

    var ee=yx_findMenuEvent(this.content.name);
    if (ee!=null) { this.code1=ee.code1; this.code2=ee.code2; this.mode=ee.mode; }
  }

  if ((yx_isIE55 || yx_isIE6) && yx_hasObject(f) && !yx_isMac) {
    var ifrm=yx_createEle(f,"IFRAME"); ifrm.src="javascript:false";
    with (ifrm.style) {
      position="absolute"; left="0px"; top="0px"; visibility="inherit"; width=this.width+"px"; height=this.height+"px"; zIndex=0;
      filter="progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
    }
    this.holder.appendChild(ifrm);
  }

  if (yx_isMac) { f.yx_OMs[f.yx_OMs.length]=this.holder; }

  this.ready=true; this.flying=false; this.did=true;
  if (this.go) { this.showM(); }
}

function yx_insOBJ(i_name,i_content,para) {
  this.name=i_name; this.content=i_content;

  this.floating=(para.search(/floating\s*:\s*yes/)!=-1);

  var ra=para.match(/position\s*:\s*(absolute|relative|slot)(\s+([\w\-]+))?(\s+([\w\-]+))?/);
  this.pos=(ra && ra[1])?ra[1]:"absolute";
  this.ref=(ra && ra[3])?ra[3]:"";
  this.ref2=(ra && ra[5])?ra[5]:"";

  this.xAlign=para.search(/align\s*:\s*(left|center|right)/)!=-1?(RegExp.$1):"left";
  this.yAlign=para.search(/valign\s*:\s*(top|middle|bottom)/)!=-1?(RegExp.$1):"top";

  this.dx=para.search(/offset-left\s*:\s*(\-?\d+)/)!=-1?(RegExp.$1-0):0;
  this.dy=para.search(/offset-top\s*:\s*(\-?\d+)/)!=-1?(RegExp.$1-0):0;

  this.bar=(para.search(/menu-form\s*:\s*bar/)!=-1);
  this.dir=para.search(/direction\s*:\s*(right-down|right-up|left-down|left-up|center-down|center-up|abs-right-down|abs-right-up|abs-left-down|abs-left-up|right-top|left-top|right-middle|left-middle|right-bottom|left-bottom)/)!=-1?(RegExp.$1):"right-down";

  this.vis=(para.search(/visibility\s*:\s*hidden/)==-1);

  this.target=para.search(/target\s*:\s*([\w\-]+)/)!=-1?(RegExp.$1):"";

  this.style=yx_findGroup(para.search(/style\s*:\s*([\w\-\s]+)/)!=-1?(RegExp.$1):"");

  this.sticky=(para.search(/sticky\s*:\s*yes/)!=-1);
  this.highlight=(para.search(/highlight\s*:\s*no/)==-1);

  this.ox=0; this.oy=0;

  this.topFrame=null; this.subFrame=null; this.isAll=false; this.subReady=false; this.subSeq=0; this.iden=++yxIdx;

  this.lead=null; this.head=this;
  this.menu=new yx_menuOBJ(this,this,this.content);

  this.timer=-1; this.timeIt=yx_timeIt; this.sX=0; this.sY=0; this.z=zBase-1;
  this.holder=null; this.getHolder=yx_getHolder;

  this.showIns=yx_showIns; this.hideIns=yx_hideIns; this.movInsTo=yx_movInsTo; this.movInsBy=yx_movInsBy;
  this.rePos=yx_rePos;
}

function yx_clickIt() { yx_onItem=0; yx_closeIns(null); }

function yx_checkFrame(menu) {
  var h=menu.head, sf=h.subFrame;

  if (menu.lead==h && sf!=null && !h.isAll) {
    h.subReady=false;
    try {
      if (sf.document.readyState=="complete") {
        if (typeof(sf.yx_OMs)=="undefined") { sf.yx_OMs=new Array(); }

        if (typeof(sf.yxSeq)=="undefined" || sf.yxSeq!=h.subSeq) {
          if (typeof(sf.yxSeq)=="undefined") {
            h.subSeq=sf.yxSeq=++yxSubIdx;
            if (typeof(sf.yx_frame)=="undefined") {
              sf.document.onclick=yx_clickIt;
            }
          }
          else {
            h.subSeq=sf.yxSeq;
          }
          yx_cleanSub(h);
          var _div=yx_createEle(sf,"DIV"); _div=null;
          if (yx_isMac) { yx_cleanCache(sf,h.iden); }
        }
        h.subReady=true;
      }
    }
    catch(err) {}
  }
}

// IE only
function yx_hasObject(f) {
  yx_iframe=f.document.getElementsByTagName("SELECT").length==0?f.document.getElementsByTagName("OBJECT").length==0?f.document.getElementsByTagName("EMBED").length==0?0:1:1:1;
  return (yx_iframe>0);
}

function yx_cleanCache(f,iden) {
  if (typeof(f.yx_OMs)!="undefined") {
    for (var i=0; i<f.yx_OMs.length; i++) {
      if (f.yx_OMs[i]!=null && f.yx_OMs[i].iden==iden) {
        yx_hideLayer(f.yx_OMs[i]);
        f.yx_OMs[i]=null;
      }
    }
  }
}
// ------
