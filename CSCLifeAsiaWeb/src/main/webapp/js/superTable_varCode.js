﻿//Author: Ai Hao 2010-08-25

//English
var moreBtnText_ENG 		= 	"More...";
var addBtnText_ENG  		= 	"Add";//IFSU-609
var removeBtnText_ENG 		= 	"Remove";
var rowNumText_ENG 			= 	"Row Number";

//Chinese
moreBtnText_CHN				=	"更多...";
addBtnText_CHN				=	"添加";
removeBtnText_CHN			=	"删除";
rowNumText_CHN				=	"行号";