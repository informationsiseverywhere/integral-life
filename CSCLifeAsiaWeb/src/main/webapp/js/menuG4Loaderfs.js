
var _webPath="";
var _scriptPath="../../../js/";
var _imagePath="";
var _contentScript="";
var _styleScript="";
var _instanceScript="";
var _nonMenuPage="";
var _menuTimer=100;
var _menuTimerBar=0;
var _floatTimer=100;
var _floatOffset=1;
var _zBase=2;
// ------

var yx_nav=navigator.userAgent.toLowerCase();

var yx_isMac=(yx_nav.indexOf("mac")!=-1);
var yx_isOpera=(yx_nav.indexOf("opera")!=-1);
var yx_isKon=(yx_nav.indexOf("konqueror")!=-1);
var yx_isIE=(!yx_isOpera && yx_nav.indexOf("msie")!=-1);
var yx_isIE4=(yx_isIE && yx_nav.indexOf("msie 4")!=-1);
var yx_isIE5=(yx_isIE && !yx_isIE4);
var yx_isIE55=(yx_isIE && yx_nav.indexOf("msie 5.5")!=-1);
var yx_isIE6=(yx_isIE && yx_nav.indexOf("msie 6")!=-1);
var yx_isN4=(yx_nav.indexOf('mozilla')!=-1 && yx_nav.search(/msie|gecko|opera|spoofer|compatible|webtv|hotjava/)==-1);
var yx_isN6=(yx_nav.indexOf("gecko")!=-1);
var yx_isO7=(yx_nav.indexOf("opera 7")!=-1);
var yx_isK3=(yx_isKon && navigator.appVersion.indexOf('Konqueror/3')!=-1);

var yx_menuSafe=(yx_isN6 || yx_isN4 || yx_isIE5 || yx_isIE4 && !yx_isMac || yx_isK3 || yx_isO7);

if (typeof(webPath)=="undefined") { webPath=_webPath; }
if (typeof(scriptPath)=="undefined") { scriptPath=_scriptPath; }
if (typeof(imagePath)=="undefined") { imagePath=_imagePath; }
if (typeof(contentScript)=="undefined") { contentScript=_contentScript; }
if (typeof(styleScript)=="undefined") { styleScript=_styleScript; }
if (typeof(instanceScript)=="undefined") { instanceScript=_instanceScript; }
if (typeof(nonMenuPage)=="undefined") { nonMenuPage=_nonMenuPage; }
if (typeof(menuTimer)=="undefined") { menuTimer=_menuTimer; }
if (typeof(menuTimerBar)=="undefined") { menuTimerBar=_menuTimerBar; }
if (typeof(floatTimer)=="undefined") { floatTimer=_floatTimer; }
if (typeof(floatOffset)=="undefined") { floatOffset=_floatOffset; }
if (typeof(zBase)=="undefined") { zBase=_zBase; }

var yx_Top="T", yx_M="M", yx_L="L", yx_C="C", yx_I="I", yx_S="S";
var yx_goodM=true, yx_goodI=true, yx_onItem=0;
var yx_cCount=0, yx_cBack=new Array(), yxIns=new Array(), yxSubIdx=0;
var yx_iTimer=0, yx_oTimer=0, yx_sTimer=0, yx_BM=0;
var yx_onePixel="screenFiles/"+"onePixel.gif", yx_pImage=new Image(1,1); yx_pImage.src=yx_onePixel;
var yx_sPixel='<img src="'+yx_pImage.src+'" width="1" height="1" border="0" hspace="0" vspace="0">';
var yx_pathRE=/(^\w+:)|(^\/)|(^\.)/, yx_badRE=/[^\w\-]/;

function yx_setIt() { yx_oTimer=setTimeout("yx_closeIns(null)",menuTimer*2); }
function yx_stopIt(t) { if (t>0) clearTimeout(t); }
function yx_stopAll() { yx_stopIt(yx_iTimer); yx_iTimer=0; yx_stopIt(yx_oTimer); yx_oTimer=0; yx_stopIt(yx_sTimer); yx_sTimer=0; }
function yx_getWebPath(path) { return yx_pathRE.test(path)?path:(webPath+path); }
function yx_getImgPath(path) { return yx_pathRE.test(path)?path:(imagePath+path); }
function yx_showMsg(msg) { window.status=msg; }
function yx_callBack(obj,func,para,t) {
  var x=yx_cCount++; yx_cBack[x]=obj;
  var tm=setTimeout(("yx_cBack["+x+"]."+func+para),t);
  if (yx_cCount>200) { yx_cCount=0; }
  return tm;
}
function yx_doCode(f,code,mode) {
  yx_stopAll();

  if (mode=="top") {
    eval(code);
  }
  else if (typeof(f._eval)!="undefined") {
    f._eval(code);
  }
  else {
    f.eval(code);
  }
}
function yx_goLink(f,link,mode,ft) {
  yx_stopAll();

  if (link.substring(0,11).toLowerCase()=="javascript:") {
    yx_doCode(f,link.substring(11),mode);
  }
  else {
    var m=mode!=""?mode:ft;
    if (m=="top") {
      top.location.href=yx_getWebPath(link);
    }
    else if (m=="self" || m=="") {
      yx_onItem=0; f.location.href=yx_getWebPath(link);
    }
    else {
      //window.open(yx_getWebPath(link),m);
      frames[1].document.form1.activeField.value = link;
      if (frames[1].document.form1.screen.value == "S0017"){
      	frames[1].document.form1.action_key.value = "PFKEY0";
      }else{
      	frames[1].document.form1.action_key.value = "PFKEY03";
      }	
      frames[1].doSub();
      //doSubMenu(link);
    }
  }
}

function yx_fgOBJ(obj,name,mode) {
  for (var i=0; i<obj.length; i++) { if (obj[i].name==name) { return obj[i]; } }
  return mode?obj[0]:null;
}
function yx_findOBJ(obj,name) { return yx_fgOBJ(obj,name,false); }
function yx_getOBJ(obj,name) { return yx_fgOBJ(obj,name,true); }

function yx_colorIt(style,color) {
  if (style=="outset") {
    return [yx_getColor(color,1),yx_getColor(color,-1)];
  }
  else if (style=="inset") {
    return [yx_getColor(color,-1),yx_getColor(color,1)];
  }
  else {
    return [color,color];
  }
}

function yx_sPadOBJ(sp_name,para) {
  this.name=sp_name;

  var ra=para.match(/visibility\s*:\s*hidden/); this.vis=ra?false:true;

  var ra=para.match(/background-color\s*:\s*(#[\-a-f\d]{6})/i); this.sColor=(ra && ra[1]!="#------")?ra[1]:"";

  ra=para.match(/border-size\s*:\s*(\d+)/); this.bSize=ra?(ra[1]-0):0;

  if (this.bSize>0) {
    ra=para.match(/border-style\s*:\s*(outset|inset|solid)/); this.bStyle=ra?ra[1]:"solid";
    ra=para.match(/border-color\s*:\s*(#[a-f\d]{6})/i); this.bColor=ra?ra[1]:"#000000";

    var pbc=yx_colorIt(this.bStyle,this.bColor);
    this.bColorT=pbc[0]; this.bColorR=pbc[1]; this.bColorB=pbc[1]; this.bColorL=pbc[0];

    ra=para.match(/border-sides\s*:\s*(1|0)\s+(1|0)\s+(1|0)\s+(1|0)/);
    if (ra && this.sColor!="") {
      if (ra[1]=="0") { this.bColorT=this.sColor; }
      if (ra[2]=="0") { this.bColorR=this.sColor; }
      if (ra[3]=="0") { this.bColorB=this.sColor; }
      if (ra[4]=="0") { this.bColorL=this.sColor; }
    }
  }

  ra=para.match(/padding\s*:\s*(\d+)(\s+(\d+))?/);
  this.sHeight=(ra && ra[1])?(ra[1]-0):0;
  this.sWidth=(ra && ra[3])?(ra[3]-0):this.sHeight;

  ra=para.match(/menu-form\s*:\s*bar/); this.bar=ra?true:false;
  ra=para.match(/direction\s*:\s*(right-down|right-up|left-down|left-up|center-down|center-up|abs-right-down|abs-right-up|abs-left-down|abs-left-up|right-top|left-top|right-middle|left-middle|right-bottom|left-bottom)/); this.dir=ra?ra[1]:"right-down";

  ra=para.match(/scroll\s*:\s*yes/); this.scroll=ra?true:false;

  ra=para.match(/offset-left\s*:\s*(\-?\d+)/); this.dx=ra?(ra[1]-0):0;
  ra=para.match(/offset-top\s*:\s*(\-?\d+)/); this.dy=ra?(ra[1]-0):0;

  ra=para.match(/item-offset\s*:\s*(\-?\d+)/); this.iOff=ra?(ra[1]-0):0;

  ra=para.match(/opacity:(\d+)/); this.opacity=ra?ra[1]:"";
}; 
var yx_sPads=new Array();
yx_sPads[0]=new yx_sPadOBJ("","border-size:2;border-style:outset;border-color:#849ec6;padding:3;background-color:#849ec6;");

function yx_sItemOBJ(si_name,para) {
  this.name=si_name;

  var ra=para.match(/width\s*:\s*(\d+)/); this.iWidth=ra?(ra[1]-0):0;
  ra=para.match(/height\s*:\s*(\d+)/); this.iHeight=ra?(ra[1]-0):0;

  ra=para.match(/padding\s*:\s*(\d+)(\s+(\d+))?/);
  this.pHeight=(ra && ra[1])?(ra[1]-0):0;
  this.pWidth=(ra && ra[3])?(ra[3]-0):this.pHeight;

  ra=para.match(/background-color\s*:\s*(#[\-a-f\d]{6})(\s+(#[\-a-f\d]{6}))?(\s+(#[\-a-f\d]{6}))?/i);
  this.iColorN=(ra && ra[1] && ra[1]!="#------")?ra[1]:"";
  this.iColorH=(ra && ra[3])?(ra[3]!="#------"?ra[3]:""):this.iColorN;
  this.iColorC=(ra && ra[5])?(ra[5]!="#------"?ra[5]:""):"none";

  ra=para.match(/border-size\s*:\s*(\d+)/); this.bSize=ra?(ra[1]-0):0;

  if (this.bSize>0) {
    ra=para.match(/border-style\s*:\s*(outset|inset|solid)(\s+(outset|inset|solid))?(\s+(outset|inset|solid))?/);
    this.bStyleN=(ra && ra[1])?ra[1]:"solid";
    this.bStyleH=(ra && ra[3])?ra[3]:this.bStyleN;
    this.bStyleC=(ra && ra[5])?ra[5]:"";

    ra=para.match(/border-color\s*:\s*(#[a-f\d]{6})(\s+(#[a-f\d]{6}))?(\s+(#[a-f\d]{6}))?/i);
    this.bColorN=(ra && ra[1])?ra[1]:"#000000";
    this.bColorH=(ra && ra[3])?ra[3]:this.bColorN;
    this.bColorC=(ra && ra[5])?ra[5]:"";

    var ibc=yx_colorIt(this.bStyleN,this.bColorN);
    this.bColorNT=ibc[0]; this.bColorNR=ibc[1]; this.bColorNB=ibc[1]; this.bColorNL=ibc[0];

    ibc=yx_colorIt(this.bStyleH,this.bColorH);
    this.bColorHT=ibc[0]; this.bColorHR=ibc[1]; this.bColorHB=ibc[1]; this.bColorHL=ibc[0];

    if (this.bStyleC!="" && this.bColorC!="") {
      ibc=yx_colorIt(this.bStyleC,this.bColorC);
      this.bColorCT=ibc[0]; this.bColorCR=ibc[1]; this.bColorCB=ibc[1]; this.bColorCL=ibc[0];
    }
    else {
      this.bColorCT=""; this.bColorCR=""; this.bColorCB=""; this.bColorCL="";
    }

    ra=para.match(/border-sides\s*:\s*([^;]+)(;|$)/);
    if (ra) {
      var ba=ra[1].split(",");
      if (ba[0] && this.iColorN!="") {
        ra=ba[0].match(/(1|0)\s+(1|0)\s+(1|0)\s+(1|0)/);
        if (ra[1]=="0") { this.bColorNT=this.iColorN; }
        if (ra[2]=="0") { this.bColorNR=this.iColorN; }
        if (ra[3]=="0") { this.bColorNB=this.iColorN; }
        if (ra[4]=="0") { this.bColorNL=this.iColorN; }
      }
      if (ba[1] && this.iColorH!="") {
        ra=ba[1].match(/(1|0)\s+(1|0)\s+(1|0)\s+(1|0)/);
        if (ra[1]=="0") { this.bColorHT=this.iColorH; }
        if (ra[2]=="0") { this.bColorHR=this.iColorH; }
        if (ra[3]=="0") { this.bColorHB=this.iColorH; }
        if (ra[4]=="0") { this.bColorHL=this.iColorH; }
      }
      if (this.bStyleC!="" && this.bColorC!="" && ba[2] && this.iColorC!="none") {
        ra=ba[2].match(/(1|0)\s+(1|0)\s+(1|0)\s+(1|0)/);
        if (ra[1]=="0") { this.bColorCT=this.iColorC; }
        if (ra[2]=="0") { this.bColorCR=this.iColorC; }
        if (ra[3]=="0") { this.bColorCB=this.iColorC; }
        if (ra[4]=="0") { this.bColorCL=this.iColorC; }
      }
    }
  }

  ra=para.match(/sub-menu\s*:\s*mouse-click/); this.onOver=ra?false:true;

  ra=para.match(/opacity:(\d+)/); this.opacity=ra?ra[1]:"";
}; var yx_sItems=new Array(); //yx_sItems[0]=new yx_sItemOBJ("","width:400;padding:1 6;border-size:0;background-color:#3b558f #849ec6;");

function yx_sFontOBJ(sf_name,para) {
  this.name=sf_name;

  var ra=para.match(/font-family\s*:\s*([\w\s,\-]+)(;|$)/); this.fFamily=ra?ra[1]:"Arial";
  ra=para.match(/font-size\s*:\s*(\d+)/); this.fSize=ra?(ra[1]-0):14;

  ra=para.match(/font-weight\s*:\s*(normal|bold)(\s+(normal|bold))?/);
  this.fWeightN=(ra && ra[1])?ra[1]:"normal"; this.fWeightH=(ra && ra[3])?ra[3]:this.fWeightN;

  ra=para.match(/font-style\s*:\s*(normal|italic)(\s+(normal|italic))?/);
  this.fStyleN=(ra && ra[1])?ra[1]:"normal"; this.fStyleH=(ra && ra[3])?ra[3]:this.fStyleN;

  ra=para.match(/text-decoration\s*:\s*(none|underline)(\s+(none|underline))?/);
  this.textN=(ra && ra[1])?ra[1]:"none"; this.textH=(ra && ra[3])?ra[3]:this.textN;

  var ra=para.match(/color\s*:\s*(#[a-f\d]{6})(\s+(#[a-f\d]{6}))?(\s+(#[a-f\d]{6}))?/i);
  this.fColorN=(ra && ra[1])?ra[1]:"#000000"; this.fColorH=(ra && ra[3])?ra[3]:this.fColorN; this.fColorC=(ra && ra[5])?ra[5]:"";

  ra=para.match(/text-align\s*:\s*(left|center|right)/); this.fAlign=ra?ra[1]:"left";
  ra=para.match(/(valign|vertical-align)\s*:\s*(top|middle)/); this.vAlign=ra?ra[2]:"top";
}; var yx_sFonts=new Array(); yx_sFonts[0]=new yx_sFontOBJ("","color:#ffffff #ffffff;");

function yx_sTagOBJ(st_name,para) {
  this.name=st_name;

  var ra=para.match(/visibility\s*:\s*hidden/); this.vis=ra?false:true;

  if (this.vis) {
    ra=para.match(/path\s*:\s*([^;]+)(;|$)/); this.tPath=yx_getImgPath(ra?ra[1]:"");

    ra=para.match(/tag-normal\s*:\s*([^;]+)(;|$)/); this.tN=ra?ra[1]:"";
    ra=para.match(/tag-highlight\s*:\s*([^;]+)(;|$)/); this.tH=ra?ra[1]:"";

    ra=para.match(/width\s*:\s*(\d+)/); this.tWidth=ra?(ra[1]-0):"";
    ra=para.match(/height\s*:\s*(\d+)/); this.tHeight=ra?(ra[1]-0):"";

    ra=para.match(/align\s*:\s*(left|right)/); this.tAlign=ra?ra[1]:"right";
    ra=para.match(/valign\s*:\s*(top|middle)/); this.vAlign=ra?ra[1]:"top";

    //this.t0=new Image(this.tWidth, this.tHeight); this.t0.src=this.tPath+this.tN;
//    this.t1=new Image(this.tWidth, this.tHeight); this.t1.src=this.tPath+this.tH;
  }
}; var yx_sTags=new Array(); yx_sTags[0]=new yx_sTagOBJ("","");//modified by Francis for master menu bug 2009-11-19.

function yx_sSeparatorOBJ(ss_name,para) {
  this.name=ss_name;

  ra=para.match(/style\s*:\s*(outset|inset|solid)/); this.sStyle=ra?ra[1]:"solid";
  ra=para.match(/color\s*:\s*(#[a-f\d]{6})/i); this.sColor=ra?ra[1]:"#000000";

  var sbc=yx_colorIt(this.sStyle,this.sColor);
  this.color1=sbc[0]; this.color2=sbc[1];

  ra=para.match(/size\s*:\s*(1|2)/); this.sSize=(ra && this.sStyle=="solid")?(ra[1]-0):2;
}; var yx_sSeparators=new Array(); yx_sSeparators[0]=new yx_sSeparatorOBJ("","style:inset;color:#3b558f;size:2");

function yx_sMenuOBJ(sm_name,sp_name,si_name,sf_name,st_name,ss_name) {
  this.name=sm_name;
  this.sPad=(yx_sMenus.length>0 && sp_name=="")?yx_sMenus[0].sPad:yx_getPad(sp_name);
  this.sItem=(yx_sMenus.length>0 && si_name=="")?yx_sMenus[0].sItem:yx_getItem(si_name);
  this.sFont=(yx_sMenus.length>0 && sf_name=="")?yx_sMenus[0].sFont:yx_getFont(sf_name);
  this.sTag=(yx_sMenus.length>0 && st_name=="")?yx_sMenus[0].sTag:yx_getTag(st_name);
  this.sSeparator=(yx_sMenus.length>0 && ss_name=="")?yx_sMenus[0].sSeparator:yx_getSeparator(ss_name);
}; var yx_sMenus=new Array(); //yx_sMenus[0]=new yx_sMenuOBJ("","","","","","");

function addStylePad(sp_name,para) { yx_sPads[yx_sPads.length]=new yx_sPadOBJ(sp_name,para); }
function addStyleItem(si_name,para) { yx_sItems[yx_sItems.length]=new yx_sItemOBJ(si_name,para); }
function addStyleFont(sf_name,para) { yx_sFonts[yx_sFonts.length]=new yx_sFontOBJ(sf_name,para); }
function addStyleTag(st_name,para) { yx_sTags[yx_sTags.length]=new yx_sTagOBJ(st_name,para); }
function addStyleSeparator(ss_name,para) { yx_sSeparators[yx_sSeparators.length]=new yx_sSeparatorOBJ(ss_name,para); }
function addStyleMenu(sm_name,sp_name,si_name,sf_name,st_name,ss_name) { yx_sMenus[yx_sMenus.length]=new yx_sMenuOBJ(sm_name,sp_name,si_name,sf_name,st_name,ss_name); }

function yx_memOBJ(menu,style) { this.menu=menu; this.style=style; }
function yx_sGroupOBJ(sg_name) { this.name=sg_name; this.member=new Array(); }; var yx_sGroups=new Array();
function addStyleGroup(sg_name,sm_name) {
  var xGroup=yx_findGroup(sg_name);

  if (xGroup==null) {
    var x=yx_sGroups.length; yx_sGroups[x]=new yx_sGroupOBJ(sg_name); xGroup=yx_sGroups[x];
  }

  var xLen=xGroup.member.length;
  for (var i=2; i<addStyleGroup.arguments.length; i++) {
    xGroup.member[xLen+i-2]=new yx_memOBJ(addStyleGroup.arguments[i],sm_name);
  }
}
function setDefaultStyle(sp_name,si_name,sf_name,st_name,ss_name) {
  if (sp_name!="") { yx_sMenus[0].sPad=yx_getPad(sp_name); }
  if (si_name!="") { yx_sMenus[0].sItem=yx_getItem(si_name); }
  if (sf_name!="") { yx_sMenus[0].sFont=yx_getFont(sf_name); }
  if (st_name!="") { yx_sMenus[0].sTag=yx_getTag(st_name); }
  if (ss_name!="") { yx_sMenus[0].sSeparator=yx_getSeparator(ss_name); }
}

function yx_getPad(sp_name) { return yx_getOBJ(yx_sPads,sp_name); }
function yx_getItem(si_name) { return yx_getOBJ(yx_sItems,si_name); }
function yx_getFont(sf_name) { return yx_getOBJ(yx_sFonts,sf_name); }
function yx_getTag(st_name) { return yx_getOBJ(yx_sTags,st_name); }
function yx_getSeparator(ss_name) { return yx_getOBJ(yx_sSeparators,ss_name); }
function yx_getMenu(sm_name) { return yx_getOBJ(yx_sMenus,sm_name); }
function yx_findGroup(sg_name) { return yx_findOBJ(yx_sGroups,sg_name); }
function yx_findContent(c_name) { return yx_findOBJ(yxContent,c_name); }
function yx_findImage(i_name) { return yx_findOBJ(yxImages,i_name); }
function yx_findIcon(i_group) { return yx_findOBJ(yxIcons,i_group); }
function yx_findBGImage(i_name) { return yx_findOBJ(yxBGImages,i_name); }
function yx_findItemEvent(e_name) { return yx_findOBJ(yxICodes,e_name); }
function yx_findMenuEvent(e_name) { return yx_findOBJ(yxMCodes,e_name); }

function yx_mlOBJ(i_type,i_dis,i_msg,i_link,i_group,i_mode) { this.type=i_type; this.dis=i_dis; this.msg=i_msg; this.link=i_link; this.group=i_group; this.mode=(typeof(i_mode)=="undefined" || yx_badRE.test(i_mode))?"":i_mode;}
function yx_mcOBJ(i_type,i_dis,i_msg,i_code,i_group,i_mode) { this.type=i_type; this.dis=i_dis; this.msg=i_msg; this.code=i_code; this.group=i_group; this.mode=(typeof(i_mode)=="undefined" || i_mode!="top")?"self":"top";}
function yx_miOBJ(i_type,i_dis,i_group) { this.type=i_type; this.dis=i_dis; this.group=i_group; }
function yx_msOBJ(i_type) { this.type=i_type; }

function yx_mmOBJ(i_type,i_dis,i_msg,i_link,sm_name,i_group,i_mode) {
  var xMenu=yx_findMenu(sm_name);

  this.type=i_type;
  this.dis=i_dis; this.msg=i_msg; this.link=i_link;
  this.name=sm_name;
  this.group=i_group;
  this.mode=(typeof(i_mode)=="undefined" || yx_badRE.test(i_mode))?"":i_mode;

  this.items=xMenu==null?new Array():xMenu.menu.items;
  this.addM=yx_addM; this.addL=yx_addL; this.addC=yx_addC; this.addI=yx_addI; this.addS=yx_addS;
}

function yx_mOBJ(i_type,m_name) {
  this.type=i_type; this.dis="";
  this.name=m_name;

  this.items=new Array();
  this.addM=yx_addM; this.addL=yx_addL; this.addC=yx_addC; this.addI=yx_addI; this.addS=yx_addS;
}; var yxMenu=new Array();

function yx_mnOBJ(m_name,m_obj){ this.name=m_name; this.menu=m_obj; }; var yxName=new Array(), yxContent=new Array(), yxNx=null;

function yx_addM(m_type,i_dis,i_msg,i_link,sm_name,i_group,i_mode) {
  var items=this.items, x=items.length;
  items[x]=new yx_mmOBJ(m_type,i_dis,i_msg,i_link,sm_name,i_group,i_mode);

  if (yx_findMenu(sm_name)==null) {
    yxName[yxName.length]=new yx_mnOBJ(sm_name,items[x]);
  }
}
function yx_addL(l_type,i_dis,i_msg,i_link,i_group,i_mode) { this.items[this.items.length]=new yx_mlOBJ(l_type,i_dis,i_msg,i_link,i_group,i_mode); }
function yx_addC(c_type,i_dis,i_msg,i_code,i_group,i_mode) { this.items[this.items.length]=new yx_mcOBJ(c_type,i_dis,i_msg,i_code,i_group,i_mode); }
function yx_addI(i_type,i_dis,i_group) { this.items[this.items.length]=new yx_miOBJ(i_type,i_dis,i_group); }
function yx_addS(s_type) { this.items[this.items.length]=new yx_msOBJ(s_type); }

function yx_findMenu(m_name) { return (yxNx!=null && yxNx.name==m_name)?yxNx:yx_findOBJ(yxName,m_name); }
function yx_hasMenu(name) { return (yx_findOBJ(yxContent,name)!=null); }

function yx_dropContent(name) {
  for (var i=0; i<yxName.length; i++) {
    if (yxName[i].name==name) {
      for (var j=0; j<yxName[i].menu.items.length; j++) {
        if (yxName[i].menu.items[j].type==yx_M) {
          yx_dropContent(yxName[i].menu.items[j].name);
        }
      }
      yxName[i].name=""; yxName[i].menu=null; break;
    }
  }
}

function addMenu(c_name,m_name) {
  if (yx_goodM) {
    yx_showMsg("Parsing ["+m_name+"]");

    for (var i=0; i<yxContent.length; i++) {
      if (yxContent[i].name==c_name) {
        yx_dropContent(yxContent[i].menu.name);
        yxMenu[i].name=""; yxMenu[i].menu=null;
        yxContent[i].name=""; yxContent[i].menu=null;
        break;
      }
    }

    var x=yxContent.length;
    yxMenu[x]=new yx_mOBJ(yx_Top,m_name);
    yxContent[x]=new yx_mnOBJ(c_name,yxMenu[x]);

    yxNx=new yx_mnOBJ(m_name,yxMenu[x]);
    yxName[yxName.length]=yxNx;
  }
}

function _yx_addSubMenu_(m_type,m_name,i_dis,i_msg,i_link,sm_name,i_group,i_mode) {
  if (yx_goodM) {
    yxNx=yx_findMenu(m_name);

    if (yxNx!=null) {
      yxNx.menu.addM(m_type,i_dis,i_msg,i_link,sm_name,i_group,i_mode);
    }
    else {
      yx_goodM=false;
      yx_showMsg("Missing declaration of menu ["+m_name+"]");
    }
  }
}
function addSubMenu(m_name,i_dis,i_msg,i_link,sm_name,i_group,i_mode) { _yx_addSubMenu_(yx_M,m_name,i_dis,i_msg,i_link,sm_name,i_group,i_mode); }

function _yx_addLink_(l_type,m_name,i_dis,i_msg,i_link,i_group,i_mode) {
  if (yx_goodM) {
    yxNx=yx_findMenu(m_name);

    if (yxNx!=null) {
      yxNx.menu.addL(l_type,i_dis,i_msg,i_link,i_group,i_mode);
    }
    else {
      yx_goodM=false;
      yx_showMsg("Missing declaration of menu ["+m_name+"]");
    }
  }
}
function addLink(m_name,i_dis,i_msg,i_link,i_group,i_mode) { _yx_addLink_(yx_L,m_name,i_dis,i_msg,i_link,i_group,i_mode); }

function _yx_addCommand_(c_type,m_name,i_dis,i_msg,i_code,i_group,i_mode) {
  if (yx_goodM) {
    yxNx=yx_findMenu(m_name);

    if (yxNx!=null) {
      yxNx.menu.addC(c_type,i_dis,i_msg,i_code,i_group,i_mode);
    }
    else {
      yx_goodM=false;
      yx_showMsg("Missing declaration of menu ["+m_name+"]");
    }
  }
}
function addCommand(m_name,i_dis,i_msg,i_code,i_group,i_mode) { _yx_addCommand_(yx_C,m_name,i_dis,i_msg,i_code,i_group,i_mode); }

function addInfo(m_name,i_dis,i_group) {
  if (yx_goodM) {
    yxNx=yx_findMenu(m_name);

    if (yxNx!=null) {
      yxNx.menu.addI(yx_I,i_dis,i_group);
    }
    else {
      yx_goodM=false;
      yx_showMsg("Missing declaration of menu ["+m_name+"]");
    }
  }
}

function addSeparator(m_name) {
  if (yx_goodM) {
    yxNx=yx_findMenu(m_name);

    if (yxNx!=null) {
      yxNx.menu.addS(yx_S);
    }
    else {
      yx_goodM=false;
      yx_showMsg("Missing declaration of menu ["+m_name+"]");
    }
  }
}

function endMenu() { if (yx_goodM) { yx_showMsg("Menu parsed"); } }

function yx_imgOBJ(i_group,i_path,i_initial,i_swap,i_width,i_height,i_align,i_valign) {
  this.name=i_group;
  this.path=yx_getImgPath(i_path);
  this.initial=i_initial; this.swap=i_swap;
  this.width=i_width; this.height=i_height; this.align=i_align; this.vAlign=i_valign;
}; var yxImages=new Array(), yxIcons=new Array(), yxBGImages=new Array();

function setImage(i_group,i_path,i_initial,i_swap,i_width,i_height) {
	yxImages[yxImages.length]=new yx_imgOBJ(i_group,i_path,i_initial,i_swap,i_width,i_height,"left","");
}; var addImage=setImage;

function setIcon(i_group,i_path,i_initial,i_swap,i_width,i_height,i_align,i_valign) {
  yxIcons[yxIcons.length]=new yx_imgOBJ(i_group,i_path,i_initial,i_swap,i_width,i_height,i_align,i_valign);
}; var appendImage=setIcon;

function setBGImage(i_group,i_path,i_initial,i_swap) {
  yxBGImages[yxBGImages.length]=new yx_imgOBJ(i_group,i_path,i_initial,i_swap,1,1,"","");
}

function yx_toHex(num) {
  var hexStr="0123456789ABCDEF", num2=num%16, num1=(num-num2)/16;
  return hexStr.charAt(num1)+hexStr.charAt(num2);
}

function yx_toTen(num) {
  var hexStr="0123456789ABCDEF";
  return hexStr.indexOf(num.toUpperCase());
}

function yx_fromHex(num) {
  var char1=num.charAt(0), char2=num.charAt(1);
  return yx_toTen(char1)*16+yx_toTen(char2);
}

function yx_getColor(color,m) {
  var red=color.substring(1,3), green=color.substring(3,5), blue=color.substring(5,7);
  var num1=yx_fromHex(red)+m*51, num2=yx_fromHex(green)+m*51, num3=yx_fromHex(blue)+m*51;
  if (num1<0) { num1=0; }
  if (num1>255) { num1=255; }
  if (num2<0) { num2=0; }
  if (num2>255) { num2=255; }
  if (num3<0) { num3=0; }
  if (num3>255) { num3=255; }
  return "#"+yx_toHex(num1)+yx_toHex(num2)+yx_toHex(num3);
}

function yx_getStyle() {
  if (this.head.style!=null) {
    var styles=this.head.style.member;
    for (var i=0; i<styles.length; i++) {
      if (styles[i].menu==this.content.name) {
        this.style=yx_getMenu(styles[i].style);
        return;
      }
    }
  }
  this.style=yx_sMenus[0];
}

function yx_getItemStyle(lead) {
  if (this.content.group!="" && lead.head.style!=null) {
    var styles=lead.head.style.member;
    for (var i=0; i<styles.length; i++) {
      if (styles[i].menu==this.content.group) {
        return yx_getMenu(styles[i].style);
      }
    }
  }
  return lead.style;
}

function yx_xyObj(x,y) { this.x=x; this.y=y; }
function yx_getSlot(f,n) {
  yx_getDim(f);
  var x=(n==0 || n==6 || n==7)?0:((n==1 || n==5 || n== 8)?Math.round(f.yx_wW/2):(f.yx_wW-1));
  var y=(n==0 || n==1 || n==2)?0:((n==3 || n==7 || n== 8)?Math.round(f.yx_wH/2):(f.yx_wH-1));
  return (new yx_xyObj(x,y));
}

function yx_findIns(name,mode) {
  var ins=yx_findOBJ(yxIns, name);
  return mode?((ins!=null && ins.menu!=null && ins.menu.ready)?ins:null):ins;
}

function yx_closeIns(ins) {
  for (var i=0; i<yxIns.length; i++) {
    if (yxIns[i].menu!=null && yxIns[i].menu.shown && yxIns[i]!=ins) { yxIns[i].hideIns(); }
  }
}

function yx_setIdx(ins,idx) { ins.z=idx; yx_setZIndex(ins.menu.holder,idx); }

function addInstance(i_name,i_menu,para) {
  if (yx_goodI) {
    var content=yx_findContent(i_menu);
    if (content!=null) {
      yx_showMsg("Adding menu ["+i_name+"]");

      for (var i=0; i<yxIns.length; i++) {
        if (yxIns[i].name==i_name) {
          yx_cleanMenu(yxIns[i].menu,true);
          yxIns[i].name=""; yxIns[i].menu=null;
          break;
        }
      }

      yxIns[yxIns.length]=new yx_insOBJ(i_name,content.menu,para);
      yx_showMsg("Menu Instance ["+i_name+"] enabled");
    }
    else {
      yx_goodI=false;
      yx_showMsg("Menu content not found: ["+i_menu+"]");
    }
  }
}

function yx_eventOBJ(e_name,e_handler1,e_handler2,e_mode) {
  this.name=e_name;
  this.code1=e_handler1; this.code2=e_handler2;
  this.mode=(typeof(e_mode)=="undefined" || e_mode!="top")?"self":"top";
}; var yxICodes=new Array(), yxMCodes=new Array();

function yx_dimOBJ(x,y,width,height,scrollx,scrolly) {
  this.x=x; this.y=y; this.width=width; this.height=height; this.scrollx=scrollx; this.scrolly=scrolly;
}

function yx_getDim(f) { f.yx_sX=yx_getScrollX(f); f.yx_sY=yx_getScrollY(f); f.yx_wW=yx_getWinWidth(f); f.yx_wH=yx_getWinHeight(f); }

function yx_getPos(mode) {
  var head=this.head, pItem=this.lead, pMenu=pItem.lead, sPad=this.style.sPad, sW=sPad.bSize+sPad.sWidth, sH=sPad.bSize+sPad.sHeight;
  var x_align=head.xAlign, y_align=head.yAlign, dir=pMenu==null?"":(pMenu.lead==head || pMenu.style.sPad.dir=="")?(head.dir):(pMenu.style.sPad.dir);
  var f=yx_getFrame(this);

  yx_getDim(f);

  var padX=0, padY=0;
  if (pItem==head) {
    if (x_align=="left") {
			padX=8;//Modified by shawn for menu position @2010-07-21
    }
    else if (x_align=="center") {
      padX=-Math.round(this.width/2);
    }
    else {
      padX=-this.width+1;
    }

    if (y_align=="top") {
			padY=90;//Modified by shawn for menu position @2010-07-21
    }
    else if (y_align=="middle") {
      padY=-Math.round(this.height/2);
    }
    else {
      padY=-this.height+1;
    }
    
		padX+=head.holder.x; padY+=head.holder.y;
  }
  else if (pMenu.bar) {
    var iX=pMenu.x+pItem.x, iY=pMenu.y+pItem.y, iW=pItem.width, iH=pItem.height;

    if (dir.indexOf("down")!=-1) {
      padY=(pMenu.lead==head && !head.isAll)?0:(iY+iH);
    }
    else if (dir.indexOf("up")!=-1) {
      padY=(pMenu.lead==head && !head.isAll)?(f.yx_wH-this.height):(iY-this.height);
    }
    else {
      padY=(pMenu.lead==head && !head.isAll)?0:(iY+iH);
    }

    if (dir.indexOf("center")!=-1) {
      padX=pMenu.x+Math.round((pMenu.width-this.width)/2);
    }
    else if (dir.indexOf("abs-left")!=-1) {
      padX=pMenu.x;
    }
    else if (dir.indexOf("abs-right")!=-1) {
      padX=pMenu.x+pMenu.width-this.width;
    }
    else if (dir.indexOf("right")!=-1) {
      padX=iX-sW;
    }
    else if (dir.indexOf("left")!=-1) {
      padX=iX+iW-this.width+sW;
    }
    else {
      padX=iX-sW;
    }
  }
  else {
    var iX=pMenu.x+pItem.x, iY=pMenu.y+pItem.y, iW=pItem.width, iH=pItem.height;

    if (dir.indexOf("right")!=-1) {
      padX=(pMenu.lead==head && !head.isAll)?0:(iX+iW);
    }
    else if (dir.indexOf("left")!=-1) {
      padX=(pMenu.lead==head && !head.isAll)?(f.yx_wW-this.width):(iX-this.width);
    }
    else {
      padX=(pMenu.lead==head && !head.isAll)?0:(iX+iW);
    }

    if (dir.indexOf("down")!=-1) {
      padY=iY-sH;
    }
    else if (dir.indexOf("up")!=-1) {
      padY=iY+iH-this.height+sH;
    }
    else if (dir.indexOf("top")!=-1) {
      padY=pMenu.y;
    }
    else if (dir.indexOf("bottom")!=-1) {
      padY=pMenu.y+pMenu.height-this.height;
    }
    else if (dir.indexOf("middle")!=-1) {
      padY=pMenu.y+Math.round((pMenu.height-this.height)/2);
    }
    else {
      padY=iY-sH;
    }
  }

  if (pMenu!=null && pMenu.lead==head && !head.isAll || pItem==head && head.isAll && head.pos=="slot") {
    padX+=f.yx_sX; padY+=f.yx_sY;
  }

  if (pItem!=head) {
    padX+=sPad.dx; padY+=sPad.dy;

    var padTop=padY, padBottom=padTop+this.height-1, padLeft=padX, padRight=padLeft+this.width-1;
    var dx=0, dy=0;

    if (padBottom-f.yx_sY>=f.yx_wH) { dy=f.yx_wH+f.yx_sY-padBottom-1-yx_BM; }
    if (padTop+dy<f.yx_sY) { dy=f.yx_sY-padTop+yx_BM; }
  
    if (padRight-f.yx_sX>=f.yx_wW && !pMenu.bar) {
      dx=-this.width-pItem.width;
      padRight+=dx;
    }
    if (padRight-f.yx_sX>=f.yx_wW) { dx=f.yx_wW+f.yx_sX-padRight-1-yx_BM; }
    if (padLeft+dx<f.yx_sX) { dx=f.yx_sX-padLeft+yx_BM; }
  
    padX+=dx; padY+=dy;
  }

  this.movMenuTo(padX,padY);

  if (mode && !this.shown && (pMenu==null || pMenu.active==pItem)) {
    if (pMenu!=null) {
      yx_setZIndex(this.holder, yx_getZIndex(pMenu.holder)+1);
    }
    else {
      yx_setZIndex(this.holder, ++zBase);
    }

    yx_showLayer(this.holder); this.shown=true;

    if (this.code1!="") { yx_doCode(f,this.code1,this.mode); }
  }

  if (pItem==head && head.floating && head.timer<0) { head.timeIt(); }
}

function yx_showM() {
  this.go=true;

  if (this.ready) {
    this.getPos(true);
    if (!this.head.highlight) { this.blurM(); }
  }
  else {
    if (!this.flying) {
      this.flying=true;
      yx_callBack(this,"makeM","()",0);
    }
  }
}

function yx_blurM() {
  this.open=false;
  if (this.active!=null) { this.active.offIt(); this.active=null; }

  yx_checkFrame(this);

  if (this.head.subReady) {
    for (var i=0; i<this.itemL; i++) {
      if (this.items[i].menu!=null) { this.items[i].menu.hideM(true); }
    }
  }
}

function yx_hideM(mode) {
  this.go=false;

  if (this.shown) {
    this.blurM();

    if (this.lead!=this.head || (mode && !this.head.vis)) {
      yx_hideLayer(this.holder); this.shown=false;
      if (this.code2!="") { yx_doCode(yx_getFrame(this),this.code2,this.mode); }
    }

    if (this.lead!=this.head) { this.lead.offIt(); }
  }
}

function yx_getDX(f,mx,ix,dis,mw,iw) {
  var dx=0;
  if (mx+ix<f.yx_sX || mx+ix-dis-iw<f.yx_sX && mx<f.yx_sX) {
    dx=iw;
  }
  else if (mx+ix+iw-f.yx_sX>f.yx_wW || mx+ix+iw+dis+iw-f.yx_sX>f.yx_wW && mx+mw-f.yx_sX>f.yx_wW) {
    dx=-iw;
  }
  return dx;
}
function yx_getDY(f,my,iy,dis,mh,ih) {
  var dy=0;
  if (my+iy<f.yx_sY || my+iy-dis-ih<f.yx_sY && my<f.yx_sY) {
    dy=ih;
  }
  else if (my+iy+ih-f.yx_sY>f.yx_wH || my+iy+ih+dis+ih-f.yx_sY>f.yx_wH && my+mh-f.yx_sY>f.yx_wH) {
    dy=-ih;
  }
  return dy;
}

function yx_movMenuTo(x,y) { yx_moveLayerTo(this.holder,x,y); this.x=x; this.y=y; }
function yx_movMenuBy(x,y) { yx_moveLayerBy(this.holder,x,y); this.x+=x; this.y+=y; }
function yx_scrollIt(dx,dy) {
  var f=yx_getFrame(this), done=true;

  if (dx>0 && this.x+dx>=f.yx_sX) {
    dx=f.yx_sX-this.x;
  }
  else if (dx<0 && this.x+this.width-f.yx_sX+dx<=f.yx_wW) {
    dx=f.yx_wW+f.yx_sX-this.x-this.width;
  }
  else if (dy>0 && this.y+dy>=f.yx_sY) {
    dy=f.yx_sY-this.y;
  }
  else if (dy<0 && this.y+this.height-f.yx_sY+dy<=f.yx_wH) {
    dy=f.yx_wH+f.yx_sY-this.y-this.height;
  }
  else {
    done=false;
  }

  this.movMenuBy(dx,dy);

  if (!done) {
    yx_sTimer=yx_callBack(this,"scrollIt","("+dx+","+dy+")",floatTimer);
  }
}

function yx_itemOBJ(lead,content) {
  this.lead=lead; this.content=content;

  this.item=null; this.cover=null;
  this.img=null; this.tag=null; this.icon=null;

  this.menu=null;

  this.x=0; this.y=0; this.width=0; this.height=0;
  this.code1=""; this.code2=""; this.mode="";

  this.makeI=yx_makeI; this.makeS=yx_makeS; this.makeC=yx_makeC; this.onIt=yx_onIt; this.offIt=yx_offIt;
  this.getItemStyle=yx_getItemStyle;
}

function yx_menuOBJ(head,lead,content) {
  this.head=head; this.lead=lead; this.content=content;

  this.holder=null; this.pad=null; this.active=null;
  this.padB=null; this.padC=null; this.me=-1;

  this.itemL=content.items.length; this.items=new Array();

  this.ready=false; this.flying=false; this.go=false; this.shown=false; this.open=false; this.did=false;

  this.x=0; this.y=0; this.width=0; this.height=0; this.getPos=yx_getPos;
  this.code1=""; this.code2=""; this.mode="";

  this.style=null; this.getStyle=yx_getStyle;

  this.makeP=yx_makeP; this.makeM=yx_makeM; this.showM=yx_showM; this.hideM=yx_hideM; this.blurM=yx_blurM; this.movMenuTo=yx_movMenuTo; this.movMenuBy=yx_movMenuBy;
  this.scrollIt=yx_scrollIt; this.bar=false;
}

function yx_showIns() {
  if (this.topFrame!=null && this.topFrame.yx_loaded==1) {
    this.getHolder(); yx_closeIns(this); this.menu.showM();
  }
}
function yx_hideIns() { this.menu.hideM(true); }
function yx_movInsTo(x,y) {
  if (this.menu.ready) {
    this.holder.x=x; this.holder.y=y;
    this.menu.hideM(false); this.menu.getPos(false);
  }
}
function yx_movInsBy(x,y) {
  if (this.menu.ready) {
    this.holder.x+=x; this.holder.y+=y;
    this.menu.hideM(false); this.menu.movMenuBy(x,y);
  }
}

function yx_timeIt() {
  var f=this.topFrame;
  yx_getDim(f);

  if (this.sX!=f.yx_sX || this.sY!=f.yx_sY) {
    var dx=Math.ceil((f.yx_sX-this.sX)/floatOffset), dy=Math.ceil((f.yx_sY-this.sY)/floatOffset); this.sX+=dx; this.sY+=dy;
    this.movInsBy(dx,dy);
  }

  this.timer=yx_callBack(this,"timeIt","()",floatTimer);
}

function yx_markIt() {
  var item=this.item, menu=item.lead, prep=menu.active, sItem=item.styleI.sItem, sFont=item.styleI.sFont;
  var type=item.content.type, isSub=(type==yx_M), iis=item.item.style;

  if (prep!=null && prep!=item) { prep.offIt(); }
  menu.active=item;

  iis.color=(isSub && menu.open && sFont.fColorC!="")?sFont.fColorC:sFont.fColorH;
  iis.backgroundColor=(isSub && menu.open && sItem.iColorC!="none")?sItem.iColorC:sItem.iColorH;
  iis.fontWeight=sFont.fWeightH; iis.fontStyle=sFont.fStyleH; iis.textDecoration=sFont.textH;
  if (sItem.bSize>0) { iis.borderColor=(isSub && menu.open && sItem.bStyleC!="" && sItem.bColorC!="")?(sItem.bColorCT+" "+sItem.bColorCR+" "+sItem.bColorCB+" "+sItem.bColorCL):(sItem.bColorHT+" "+sItem.bColorHR+" "+sItem.bColorHB+" "+sItem.bColorHL); }

  if (item.imgBG1!="") { iis.backgroundImage=item.imgBG1; }
  if (item.img!=null && item.img.src.indexOf(item.img1)==-1) { item.img.src=item.img1; }
  if (item.icon!=null && item.icon.src.indexOf(item.icon1)==-1) { item.icon.src=item.icon1; }
  if (item.tag!=null && item.tag.src.indexOf(item.tag1)==-1) { item.tag.src=item.tag1; }
}

function yx_upIt(it) {
  var x=it.lead.lead;
  if (x.lead!=null) { x.cover.markIt(); yx_upIt(x); }
}

function yx_downIt(it) {
  var x=it.menu;
  if (x!=null && x.shown) {
    for (var i=0; i<x.itemL; i++) {
      if (x.items[i].menu!=null && x.items[i].menu.shown) {
        x.items[i].cover.markIt(); yx_downIt(x.items[i]);
        break;
      }
    }
  }
}

function yx_onIt() {
  var type=this.content.type, menu=this.lead, head=menu.head, f=yx_getFrame(menu);

  yx_closeIns(head);
  if (head.z<zBase) { yx_setIdx(head,++zBase); }

  yx_checkFrame(menu);

  if (head.subReady) {
    for (var i=0; i<menu.itemL; i++) {
      if (menu.items[i]!=this && menu.items[i].menu!=null) {
        menu.items[i].menu.hideM(true);
      }
    }
  }

  if (this.code1!="") { yx_doCode(f,this.code1,this.mode); }

  var sPad=menu.style.sPad;
  var dx=!sPad.scroll?0:menu.bar?yx_getDX(f,menu.x,this.x,sPad.iOff,menu.width,this.width):0, dy=!sPad.scroll?0:menu.bar?0:yx_getDY(f,menu.y,this.y,sPad.iOff,menu.height,this.height);
  if (dx!=0 || dy!=0) {
    yx_sTimer=yx_callBack(menu,"scrollIt","("+dx+","+dy+")",menuTimer);
  }
  else if (type==yx_M && head.subReady) {
    if (this.styleI.sItem.onOver || menu.open) { this.menu.showM(); }
  }
}

function yx_offIt() {
  var item=this.item, sItem=this.styleI.sItem, sFont=this.styleI.sFont, is=item.style;

  is.color=sFont.fColorN; is.backgroundColor=sItem.iColorN;
  is.fontWeight=sFont.fWeightN; is.fontStyle=sFont.fStyleN; is.textDecoration=sFont.textN;
  if (sItem.bSize>0) { is.borderColor=sItem.bColorNT+" "+sItem.bColorNR+" "+sItem.bColorNB+" "+sItem.bColorNL; }

  if (this.imgBG0!="") { is.backgroundImage=this.imgBG0; }
  if (this.img!=null && this.img.src.indexOf(this.img0)==-1) { this.img.src=this.img0; }
  if (this.icon!=null && this.icon.src.indexOf(this.icon0)==-1) { this.icon.src=this.icon0; }
  if (this.tag!=null && this.tag.src.indexOf(this.tag0)==-1) { this.tag.src=this.tag0; }
}

function yx_getHolder() {
  var x=0, y=0;
  if (this.pos=="relative") {
    var l=yx_findLayer(this.topFrame,this.ref);
    if (l!=null) {
      x=yx_getLeft(l); y=yx_getTop(l); 
    }
  }
  else if (this.pos=="slot") {
    var s=yx_getSlot(this.topFrame,parseInt(this.ref));
    x=s.x; y=s.y;
  }

  x+=this.dx; y+=this.dy; this.ox=x; this.oy=y;
  this.holder=new yx_xyObj(x,y);
}

function yx_getFrame(menu) { return (menu.lead==menu.head?(menu.head.topFrame):(menu.head.subFrame)); }

function yx_rePos() {
  if (this.menu.ready) {
    yx_stopIt(this.timer); this.timer=0;
    this.hideIns(); this.getHolder(); this.menu.getPos(false);
  }
}

function yx_resizeIt(ins) {
  yx_stopAll();

  var _ins=yx_findIns(ins,false);
  if (_ins!=null) {
    _ins.movInsTo(0,-1000);
    yx_callBack(_ins,"rePos","()",0);
  }
}

function yx_sfOBJ(id,ins,f,m) {
  this.name=id; this.ins=ins; this.frame=f; this.ready=m;
}; var yx_subs=new Array();

function yx_regSub(f,ins,mode) {
  if (mode=="wild") {
    var _sub=yx_findOBJ(yx_subs,ins);
    if (_sub!=null) {
      _sub.frame=f; _sub.ready=true;
      if (_sub.ins!="") {
        yx_regSub(f,_sub.ins,'sub');
      }
    }
    else {
      yx_subs[yx_subs.length]=new yx_sfOBJ(ins,"",f,true);
    }
  }
  else {
    var _ins=yx_findIns(ins,false);
    if (_ins!=null) {
      _ins.subFrame=f; _ins.subReady=true;
    }
  }
}

function yx_setSub(f,ins) { yx_regSub(f,ins,yx_findIns(ins,false)!=null?'sub':'wild'); }

function yx_regMenu(f,ins,mode) {
  var _ins=yx_findIns(ins,false);
  if (_ins!=null) {
    _ins.topFrame=f;
    if (mode=="all") {
      _ins.subFrame=f; _ins.subReady=true; _ins.isAll=true;
    }

    if (_ins.vis) {
      yx_callBack(_ins,"showIns","()",10);
    }
  }
}

function yx_delFrame(f,ins,mode) {
  var _ins=yx_findIns(ins,false);
  if (_ins!=null) {
    if ((mode=="all" || mode=="top") && f==_ins.topFrame) {
      yx_cleanTop(_ins);
    }
    if ((mode=="all" || mode=="sub") && f==_ins.subFrame) {
      yx_cleanSub(_ins);
    }
  }
}

function yx_subID(ins,id) {
  var _sub=yx_findOBJ(yx_subs,id);
  if (_sub!=null) {
    _sub.ins=ins;
    if (_sub.frame!=null && _sub.ready) {
      yx_regSub(_sub.frame,ins,'sub');
    }
  }
  else {
    yx_subs[yx_subs.length]=new yx_sfOBJ(id,ins,null,false);
  }
}

function yx_subFrame(ins,f) { if (typeof(f)=="object") yx_regSub(f,ins,'sub'); }

function yx_cleanTop(ins) { yx_cleanMenu(ins.menu,false); }
function yx_cleanSub(ins) {
  var menu=ins.menu, items=menu.items;
  for (var i=0; i<items.length; i++) {
    if (items[i].menu!=null && items[i].menu.ready) {
      yx_cleanMenu(items[i].menu,true);
    }
  }

  ins.subReady=false;
}
function yx_cleanMenu(menu,mode) {
  var items=menu.items;
  for (var i=0; i<items.length; i++) {
    items[i].item=null; items[i].cover=null;
    items[i].img=null; items[i].tag=null; items[i].icon=null;

    if (mode && items[i].menu!=null && items[i].menu.ready) {
      yx_cleanMenu(items[i].menu,mode);
    }
  }
  menu.pad=null; menu.holder=null;

  menu.ready=false; menu.flying=false; menu.go=false; menu.shown=false; menu.open=false;
}
// ------

function getMenuDim(i_name) {
  var ins=yx_findIns(i_name,true);
  if (ins!=null) {
    return new yx_dimOBJ(ins.menu.x, ins.menu.y, ins.menu.width, ins.menu.height, yx_getScrollX(ins.topFrame), yx_getScrollY(ins.topFrame));
  }
  else {
    return null;
  }
}

function showMenu(n) {
  var ins=yx_findIns(n,false);
  if (ins!=null) {
    yx_stopAll();
    ins.showIns();
  }
}

function showMenuX(n) {
  var ins=yx_findIns(n,false);
  if (ins!=null) {
    yx_stopAll();
    ins.vis=true; ins.showIns();
  }
}

function hideMenu(n) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.hideIns(); }
}

function hideMenuX(n) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.vis=false; ins.hideIns(); }
}

function moveMenuTo(n,x,y) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.movInsTo(x,y); }
}

function moveMenuBy(n,x,y) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.movInsBy(x,y); }
}

function moveMenuBack(n) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.movInsTo(ins.ox,ins.oy); }
}

function addItemEvent(e_name,e_handler1,e_handler2,e_mode) {
  var x=yx_findItemEvent(e_name);
  if (x==null) { yxICodes[yxICodes.length]=new yx_eventOBJ(e_name,e_handler1,e_handler2,e_mode); }
}

function addMenuEvent(e_name,e_handler1,e_handler2,e_mode) {
  var x=yx_findMenuEvent(e_name);
  if (x==null) { yxMCodes[yxMCodes.length]=new yx_eventOBJ(e_name,e_handler1,e_handler2,e_mode); }
}

function openMenu(n) { yx_stopAll(); showMenu(n); }
function openMenuX(n) { yx_stopAll(); showMenuX(n); }
function closeMenu() { yx_stopAll(); yx_setIt(); }
function closeMenuX(n) {
  var ins=yx_findIns(n,true);
  if (ins!=null) { ins.vis=false; }
  closeMenu();
}
function clickMenu(f,n) { yx_stopEvent(f); openMenu(n); }
function clickMenuX(f,n) { yx_stopEvent(f); openMenuX(n); }

function initSub(f,ins) { if (typeof(f)=="object") yx_setSub(f,ins,'wild'); }
// ------

if (yx_menuSafe) {
 /* var yx_menuScript=scriptPath+(yx_isN6?"menuG4N6fs.js":yx_isN4?"menuG4N4fs.js":yx_isK3?"menuG4K3fs.js":yx_isO7?"menuG4O7fs.js":yx_isIE5?"menuG4IE5fs.js":"menuG4IE4fs.js");
  document.write("<scr"+"ipt language='javascript' src='"+yx_menuScript+"' type='text/javascript'><\/scr" + "ipt>");*/
//	if (contentScript!="") {
//		document.write("<scr"+"ipt language='javascript' src='"+contentScript+"' type='text/javascript'><\/scr" + "ipt>");
//	}
  if (styleScript!="") {
    document.write("<scr"+"ipt language='javascript' src='"+styleScript+"' type='text/javascript'><\/scr" + "ipt>");
  }
  if (instanceScript!="") {
    document.write("<scr"+"ipt language='javascript' src='"+instanceScript+"' type='text/javascript'><\/scr" + "ipt>");
  }
}
else if (nonMenuPage!="") {
  setTimeout("window.location.replace('"+nonMenuPage+"')",0);
}
