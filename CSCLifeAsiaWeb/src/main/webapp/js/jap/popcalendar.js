var language = 'en';	
var enablePast = 0;
var fixedX = -1;
var fixedY = -1;
var startAt = 1;
var showWeekNumber = 0;	
var showToday = 1;
var imgDir = contextPathName + "/screenFiles/";

var dayName = 'day';

var gotoString = {
		en : 'month'
};
var todayString = {
		en : 'Today: '
};
var weekString = {
		en : 'week'
};
var scrollLeftMessage = {
		en : 'last month'

};
var scrollRightMessage = {
		en : 'next month'

};
var selectMonthMessage = {
		en : 'select month.'
};
var selectYearMessage = {
		en : 'select year.'
};
var selectDateMessage = {
		en : 'Go'
};
var	monthName = {
		en : new Array('Jan.','Feb.','Mar.','Apr.','May.','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.')

};
var	monthName2 = {
		en : new Array('1','2','3','4','5','6','7','8','9','10','11','12')

};

if (startAt==0) {
	dayName = {
			en : new Array('Sun.','Mon.','Tue.','Wed.','Thu.','Fri.','Sat.')

	};
} else {
	dayName = {
			en : new Array('Mon.','Tue.','Wed.','Thu.','Fri.','Sat.','Sun.')

	};
}

var crossobj, crossMonthObj, crossYearObj, monthSelected, yearSelected, dateSelected, omonthSelected, oyearSelected, odateSelected, monthConstructed, yearConstructed, intervalID1, intervalID2, timeoutID1, timeoutID2, ctlToPlaceValue, ctlNow, dateFormat, nStartingYear, selDayAction, isPast;
var visYear  = 0;
var visMonth = 0;
var bPageLoaded = false;
var ie  = document.all;
var dom = document.getElementById;
var ns4 = document.layers;
var today    = new Date();
var dateNow  = today.getDate();
var monthNow = today.getMonth();
var yearNow  = today.getYear();
var timeNow  = 	today.getHours() + ":" + today.getMinutes() +":" + today.getSeconds();
var imgsrc   = new Array('pcaldrop1.gif','pcaldrop2.gif','pcalleft1.gif','pcalleft2.gif','pcalright1.gif','pcalright2.gif');
var img      = new Array();
var bShow    = false;
var nStartingMonth=-1;
var intervalID3;
function hideElement( elmID, overDiv ) {
	if(ie) {
		for(i = 0; i < document.all.tags( elmID ).length; i++) {
			obj = document.all.tags( elmID )[i];
			if(!obj || !obj.offsetParent) continue;

			// Find the element's offsetTop and offsetLeft relative to the BODY tag.
			objLeft   = obj.offsetLeft;
			objTop    = obj.offsetTop;
			objParent = obj.offsetParent;

			while(objParent.tagName.toUpperCase() != 'BODY') {
				objLeft  += objParent.offsetLeft;
				objTop   += objParent.offsetTop;
				objParent = objParent.offsetParent;
			}

			objHeight = obj.offsetHeight;
			objWidth  = obj.offsetWidth;

			if((overDiv.offsetLeft + overDiv.offsetWidth) <= objLeft);
			else if((overDiv.offsetTop + overDiv.offsetHeight) <= objTop);
			/* CHANGE by Charlie Roche for nested TDs*/
			else if(overDiv.offsetTop >= (objTop + objHeight + obj.height));
			/* END CHANGE */
			else if(overDiv.offsetLeft >= (objLeft + objWidth));
			else {
				obj.style.visibility = 'hidden';
			}
		}
	}
}

function showElement(elmID) {
	if(ie) {
		for(i = 0; i < document.all.tags( elmID ).length; i++) {
			obj = document.all.tags(elmID)[i];
			if(!obj || !obj.offsetParent) continue;
			obj.style.visibility = '';
		}
	}
}

function HolidayRec (d, m, y, desc) {
	this.d = d;
	this.m = m;
	this.y = y;
	this.desc = desc;
}

var HolidaysCounter = 0;
var Holidays = new Array();

function addHoliday (d, m, y, desc) {
	Holidays[HolidaysCounter++] = new HolidayRec (d, m, y, desc);
}

if (dom) {
	for	(i=0;i<imgsrc.length;i++) {
		img[i] = new Image;
		img[i].src = imgDir + imgsrc[i];
	}
	document.write ('<div onclick="bShow=true" id="calendar" style="width: 250px; z-index:+999;position: relative; visibility:hidden;"><table width="'+((showWeekNumber==1)?250:237)+'" style="font-family:ms shell dlg;font-size:12px;border: 1px solid #5085b5; border-collapse: separate;" bgcolor="#ffffff"><tr bgcolor="#5085b5"><td><table width="'+((showWeekNumber==1)?248:245)+'"><tr height="22px;"><td style="padding:2px;font-family:ms shell dlg;font-size:12px;"><font color="#ffffff' + '' /*C9D3E9*/ +'"><b><span id="caption"></span></b></font></td><td align="left"><a href="javascript:hideCalendar()"><img src="'+imgDir+'pcalclose.gif" width="15" height="13" border="0" /></a></td></tr></table></td></tr><tr><td style="padding:5px" bgcolor="#ffffff"><span id="content"></span></td></tr>');

	if (showToday == 1) {
		/*	document.write ('<tr bgcolor="#f0f0f0"><td style="padding:5px" align="center"><span id="lblToday"></span></td></tr>');*/
	}

	document.write ('</table></div><div id="selectMonth" style="z-index:+999;position: absolute;margin-top:-40px;margin-left:-10px; visibility:hidden;"></div><div id="selectYear" style="z-index:+999;position: absolute;margin-top:-40px;margin-left:-5px;visibility:hidden;"></div>');
}

var	styleAnchor = 'text-decoration:none;color:black;line-height:22px;';
var	styleLightBorder = 'border:1px solid #a0a0a0;';

function swapImage(srcImg, destImg) {
	if (ie) document.getElementById(srcImg).setAttribute('src',imgDir + destImg);
}

function accquireToday(formatStr)
{
	// return the today with the current format.   xma3 & tchi
	today    = new Date();
	dateNow  = today.getDate();
	monthNow = today.getMonth() + 1;
	yearNow  = today.getYear();
	var pattern =formatStr;

	if(pattern.indexOf("yyyy")!=-1){

		pattern=pattern.replace("yyyy",yearNow);
	}
	if(pattern.indexOf("mm")!=-1){
		pattern=pattern.replace("mm",monthNow);
	}
	if(pattern.indexOf("MM")!=-1){
		pattern=pattern.replace("MM",monthNow);
	}
	if(pattern.indexOf("dd")!=-1){
		pattern=pattern.replace("dd",dateNow);
	}


	return pattern;
}


function init() {
	var budate=document.getElementById('busddates').value;	

	var dformat=document.getElementById('dateformat').value; 

	//fix by tchi & xma3

	if(budate.replace(/(^\s*)|(\s*$)/g, "").length==0)
	{		
		budate=accquireToday(dformat);				
	}

	var budad=budate.split('/');

	//Fix bug 804 Peter Zhang
	//var dformat=document.getElementById('dateformat').value; 
	var dFormatd = dformat.split('/');

	for (i=0; i<3; i++) {
		if ((dFormatd[i] == "d") || (dFormatd[i] == "dd") || (dFormatd[i] == "DD")) {
			dateNow = parseInt(budad[i], 10);

		} else if ((dFormatd[i] == "m") || (dFormatd[i] == "mm") || (dFormatd[i] == "MM")) {
			monthNow = parseInt(budad[i], 10) - 1;

		} else if (dFormatd[i] == "yyyy") {
			yearNow = parseInt(budad[i], 10);
		}
	}	
	//end

	imgDir = document.getElementById('imgpath').value;
	
	if (!ns4)
	{
		if (!ie) yearNow += 1900;

		crossobj=(dom)?document.getElementById('calendar').style : ie? document.all.calendar : document.calendar;
		hideCalendar();

		crossMonthObj = (dom) ? document.getElementById('selectMonth').style : ie ? document.all.selectMonth : document.selectMonth;

		crossYearObj = (dom) ? document.getElementById('selectYear').style : ie ? document.all.selectYear : document.selectYear;

		monthConstructed = false;
		yearConstructed = false;

		if (showToday == 1) {
			/*document.getElementById('lblToday').innerHTML =	'<font color="#000066">' + todayString[language] + ' <a onmousemove="window.status=\''+gotoString[language]+'\'" onmouseout="window.status=\'\'" title="'+gotoString[language]+'" style="'+styleAnchor+'" href="javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();">'+weekString[language]+dateNow+','+yearNow+'/'+monthName[language][monthNow].substring(0,3)+dayName[language][(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+', ' + dateNow + '</a></font>';*/
		}

		sHTML1 = '<span id="spanLeft" style="border:1px solid #ff0;cursor:pointer"  onclick="decMonth()"  onmousedown="clearTimeout(timeoutID1);timeoutID1=setTimeout(\'StartDecMonth()\',500)" onmouseup="clearTimeout(timeoutID1);clearInterval(intervalID1)">&nbsp<img id="changeLeft" src="'+imgDir+'pcalleft1.gif" width="10" height="11" border="0">&nbsp</span>&nbsp;';
		sHTML1 += '<span id="spanRight" style="border:1px solid #ff0;cursor:pointer"  onclick="incMonth()" onmousedown="clearTimeout(timeoutID1);timeoutID1=setTimeout(\'StartIncMonth()\',500)" onmouseup="clearTimeout(timeoutID1);clearInterval(intervalID1)">&nbsp<img id="changeRight" src="'+imgDir+'pcalright1.gif" width="10" height="11" border="0">&nbsp</span>&nbsp;';
		sHTML1 += '<span id="spanMonth" style="border:1px solid #ff0;cursor:pointer"  onclick="popUpMonth()"></span>&nbsp;';
		sHTML1 += '<span id="spanYear" style="border:1px solid #ff0;cursor:pointer"  onclick="popUpYear()"></span>&nbsp;';

		
		document.getElementById('caption').textContent = sHTML1;
		
		bPageLoaded=true;
	}
}

function hideCalendar() {
	crossobj.visibility = 'hidden';
	if (crossMonthObj != null) crossMonthObj.visibility = 'hidden';
	if (crossYearObj  != null) crossYearObj.visibility = 'hidden';
	showElement('SELECT');
	showElement('APPLET');
}

function padZero(num) {
	return (num	< 10) ? '0' + num : num;
}

function constructDate(d,m,y, t) {
	sTmp = dateFormat;
	sTmp = sTmp.replace ('dd','<e>');
	sTmp = sTmp.replace ('DD','<e>');
	sTmp = sTmp.replace ('d','<d>');
	sTmp = sTmp.replace ('<e>',padZero(d));
	sTmp = sTmp.replace ('<d>',d);
	sTmp = sTmp.replace ('mmmm','<p>');
	sTmp = sTmp.replace ('mmm','<o>');
	sTmp = sTmp.replace ('mm','<n>');
	sTmp = sTmp.replace ('MM','<n>');
	sTmp = sTmp.replace ('m','<m>');
	sTmp = sTmp.replace ('<m>',m+1);
	sTmp = sTmp.replace ('<n>',padZero(m+1));
	sTmp = sTmp.replace ('<o>',monthName[language][m]);
	sTmp = sTmp.replace ('<p>',monthName2[language][m]);
	sTmp = sTmp.replace ('yyyy',y);
	sTmp = sTmp.replace ('yy',padZero(y%100));
	return sTmp.replace ('tt',t);

}

function closeCalendar() {
	var timed = new Date();
	var tt = timed.getHours() + ":" + timed.getMinutes() + ":" +timed.getSeconds() + "";
	//alert(typeof tt + "is type of dataeSelected");
	//alert(tt  +"is the return value");
	hideCalendar();

	ctlToPlaceValue.value = constructDate(dateSelected,monthSelected,yearSelected, tt );
}

/*** Month Pulldown	***/
function StartDecMonth() {
	intervalID1 = setInterval("decMonth()",80);
}

function StartIncMonth() {
	intervalID1 = setInterval("incMonth()",80);
}

function incMonth () {
	monthSelected++;
	if (monthSelected > 11) {
		monthSelected = 0;
		yearSelected++;
	}
	constructCalendar();
}

function decMonth () {
	monthSelected--;
	if (monthSelected < 0) {
		monthSelected = 11;
		yearSelected--;
	}
	constructCalendar();
}

function constructMonth() {
	popDownYear()
	if (!monthConstructed) {
		sHTML = "";
		if(monthSelected - 2 > 0 && monthSelected + 2 < 12){
			nStartingMonth=monthSelected-2;
			sHTML += '<tr><td width="51"  align="center" style="cursor:pointer" onmousedown="setInterval1(\'dec\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;-&nbsp;</font></td></tr>';/*Modified it for new style by maxia(2010-7-21)*/
			for(i=monthSelected - 2;i<=monthSelected + 2; i++){
				sName = monthName[language][i];
				if (i == monthSelected){
					sName = '<b>' + sName + '</b>';
				}
				sHTML += '<tr><td width="51" id="m' + i + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + i + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			}
			sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'inc\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;+ &nbsp;</font></td></tr>';

		}else if(monthSelected - 2 <= 0){
			nStartingMonth=0;
			sHTML += '<tr><td width="51" align="center"><font color="#ffffff">&nbsp;**&nbsp;</font></td></tr>';
			for (i=0; i<5; i++) {
				sName = monthName[language][nStartingMonth+i];
				if (nStartingMonth+i == monthSelected){
					sName = '<b>' + sName + '</b>';
				}
				sHTML += '<tr><td width="51" id="m' +(nStartingMonth+i)  + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + (nStartingMonth+i) + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			}
			sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'inc\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;+&nbsp;</font></td></tr>';

		}else{
			nStartingMonth=6;
			sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'dec\');"  onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;-&nbsp;</font></td></tr>';
			for (i=7; i<12; i++) {
				sName = monthName[language][i];
				if ( i == monthSelected){
					sName = '<b>' + sName + '</b>';
				}
				sHTML += '<tr><td width="51" id="m' + i + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + i  + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			}
			sHTML += '<tr><td width="51" align="center"><font color="#ffffff" style="font-weight:bold">&nbsp;**&nbsp;</font></td></tr>';

		}
		/*
			for (i=0; i<12; i++) {
				sName = monthName[language][i];
				if (i == monthSelected){
					sName = '<b>' + sName + '</b>';
				}
				sHTML += '<tr><td width="51" id="m' + i + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + i + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#434343">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			}*/


		document.getElementById('selectMonth').innerHTML = '<table width="51" style="font-family:ms shell dlg;font-size:12px;border:1px solid #ffff00;"  bgcolor="#5085b5" cellspacing="2" >' + sHTML + '</table>';

		monthConstructed = true;
	}
}


function incPopMonth(){

	var newMonth=0;

	sHTML = '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'dec\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;-&nbsp;</font></td></tr>';

	for (i=0; i<5; i++) {
		newMonth = (i + nStartingMonth) - 1;
		sName = monthName[language][newMonth];
		if ((nStartingMonth+i) == monthSelected){
			sName = '<b>' + sName + '</b>';
		}
		sHTML += '<tr><td width="51" id="m' + newMonth + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + newMonth + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
	}


	if(newMonth<11){
		sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'inc\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;+&nbsp;</font></td></tr>';
		nStartingMonth++;

	}
	else{
		sHTML += '<tr><td align="center" width="51"><font color="#ffffff" style="font-weight:bold">&nbsp;**&nbsp;</font></td></tr>';
		clearInterval(intervalID3);
	}
	document.getElementById('selectMonth').innerHTML = '<table width="51" style="font-family:ms shell dlg;font-size:12px;border:1px solid #ffff00;" bgcolor="#5085b5" cellspacing="2" >' + sHTML + '</table>';

	monthConstructed = true;
}	


function decPopMonth(){
	var newMonth=0;
	sHTML = "";
	if(nStartingMonth>1){

		sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'dec\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;-&nbsp;</font></td></tr>';
		nStartingMonth--;
	}else{
		sHTML += '<tr><td align="center" width="51"><font color="#ffffff" style="font-weight:bold">&nbsp;**&nbsp;</font></td></tr>';
		clearInterval(intervalID3);
	}

	for (i=0; i<5; i++) {
		newMonth = (i + nStartingMonth)-1;
		sName = monthName[language][newMonth];
		if (i == monthSelected){
			sName = '<b>' + sName + '</b>';
		}
		sHTML += '<tr><td width="51" id="m' + newMonth + '"  style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + newMonth + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
	}

	sHTML += '<tr><td width="51" align="center"  style="cursor:pointer" onmousedown="setInterval1(\'inc\');" onmouseup="clearInterval('+intervalID3+')"><font color="#ffffff" style="font-weight:bold">&nbsp;+&nbsp;</font></td></tr>';


	document.getElementById('selectMonth').innerHTML = '<table width="51" style="font-family:ms shell dlg;font-size:12px;border:1px solid #ffff00;" bgcolor="#5085b5" cellspacing="2" >' + sHTML + '</table>';

	monthConstructed = true;
}

function setInterval1(funObj){

	clearInterval(intervalID3);
	if(funObj=='dec')
		intervalID3=setInterval('decPopMonth()',30);
	else
		intervalID3=setInterval('incPopMonth()',30);

}


function popUpMonth() {
	if (visMonth == 1) {
		popDownMonth();
		visMonth--;
	} else {
		constructMonth();
		crossMonthObj.visibility = (dom||ie) ? 'visible' : 'show';
		crossMonthObj.left = parseInt(crossobj.left) + 63;//Modified it for new style by maxia(2010-7-21)
		crossMonthObj.top =	parseInt(crossobj.top) + 66;//Modified it for new style by maxia(2010-7-21)
		hideElement('SELECT', document.getElementById('selectMonth'));
		hideElement('APPLET', document.getElementById('selectMonth'));
		visMonth++;
	}
}

function popDownMonth() {
	crossMonthObj.visibility = 'hidden';
	visMonth = 0;
}

/*** Year Pulldown ***/
function incYear() {
	for	(i=0; i<6; i++) {
		newYear	= (i + nStartingYear) + 1;
		if (newYear == yearSelected)
			txtYear = '<span style="color:#ffffff;font-weight:bold;">&nbsp;' + newYear + '&nbsp;</span>';
		else
			txtYear = '<span style="color:#ffffff;">&nbsp;' + newYear + '&nbsp;</span>';
		document.getElementById('y'+i).innerHTML = txtYear;
	}
	nStartingYear++;
	bShow=true;
}

function decYear() {
	for	(i=0; i<6; i++) {
		newYear = (i + nStartingYear) - 1;
		if (newYear == yearSelected)
			txtYear = '<span style="color:#ffffff;font-weight:bold">&nbsp;' + newYear + '&nbsp;</span>';
		else
			txtYear = '<span style="color:#ffffff;">&nbsp;' + newYear + '&nbsp;</span>';
		document.getElementById('y'+i).innerHTML = txtYear;
	}
	nStartingYear--;
	bShow=true;
}

function selectYear(nYear) {
	yearSelected = parseInt(nYear + nStartingYear);
	yearConstructed = false;
	constructCalendar();
	popDownYear();
}

function constructYear() {
	popDownMonth();
	sHTML = '';
	if (!yearConstructed) {
		sHTML = '<tr><td width="51" align="center" style="cursor:pointer" onmousedown="clearInterval(intervalID1);intervalID1=setInterval(\'decYear()\',30)" onmouseup="clearInterval(intervalID1)"><font color="#ffffff" style="font-weight:bold">-</font></td></tr>';

		j = 0;
		nStartingYear =	yearSelected - 2;
		for ( i = (yearSelected-2); i <= (yearSelected+3); i++ ) {
			sName = i;
			if (i == yearSelected) sName = '<b>' + sName + '</b>';
			sHTML += '<tr><td width="51" id="y' + j + '"  style="cursor:pointer" onclick="selectYear('+j+');event.cancelBubble=true"><font color="#ffffff">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			j++;
		}

		sHTML += '<tr><td align="center" style="cursor:pointer" onmousedown="clearInterval(intervalID2);intervalID2=setInterval(\'incYear()\',30)" onmouseup="clearInterval(intervalID2)"><font color="#ffffff" style="font-weight:bold">+</font></td></tr>';

		document.getElementById('selectYear').innerHTML = '<table width="51" cellspacing="2" style="font-family:ms shell dlg;font-size:12px;border:1px solid #ffff00;"  bgcolor="#5085b5" cellspacing="2">' + sHTML + '</table>';

		yearConstructed = true;
	}
}

function popDownYear() {
	clearInterval(intervalID1);
	clearTimeout(timeoutID1);
	clearInterval(intervalID2);
	clearTimeout(timeoutID2);
	crossYearObj.visibility= 'hidden';
	visYear = 0;
}

function popUpYear() {
	var leftOffset
	if (visYear==1) {
		popDownYear();
		visYear--;
	} else {
		constructYear();
		crossYearObj.visibility	= (dom||ie) ? 'visible' : 'show';
		leftOffset = parseInt(crossobj.left) + document.getElementById('spanYear').offsetLeft;
		if (ie) leftOffset += 13;//Modify it for new style by maxia (2010-7-21)
		crossYearObj.left = leftOffset;
		crossYearObj.top = parseInt(crossobj.top) + 66;//Modify it for new style by maxia (2010-7-21)
		visYear++;
	}
}

/*** calendar ***/
function WeekNbr(n) {
	// Algorithm used:
	// From Klaus Tondering's Calendar document (The Authority/Guru)
	// http://www.tondering.dk/claus/calendar.html
	// a = (14-month) / 12
	// y = year + 4800 - a
	// m = month + 12a - 3
	// J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
	// d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
	// L = d4 / 1460
	// d1 = ((d4 - L) mod 365) + L
	// WeekNumber = d1 / 7 + 1

	year = n.getFullYear();
	month = n.getMonth() + 1;
	if (startAt == 0) {
		day = n.getDate() + 1;
	} else {
		day = n.getDate();
	}

	a = Math.floor((14-month) / 12);
	y = year + 4800 - a;
	m = month + 12 * a - 3;
	b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
	J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
	d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
	L = Math.floor(d4 / 1460);
	d1 = ((d4 - L) % 365) + L;
	week = Math.floor(d1/7) + 1;

	return week;
}

function constructCalendar () {
 
	var sysDate1 = new Date();
	if(isNaN(yearSelected)) yearSelected = sysDate1.getYear();
	if(isNaN(monthSelected))  monthSelected = sysDate1.getMonth();
	if(isNaN(dateSelected)) dateSelected = sysDate1.getDate();

	var aNumDays = Array (31,0,31,30,31,30,31,31,30,31,30,31);
	var dateMessage;
	var startDate = new Date (yearSelected,monthSelected,1);
	var endDate;

	if (monthSelected==1) {
		endDate = new Date (yearSelected,monthSelected+1,1);
		endDate = new Date (endDate - (24*60*60*1000));
		numDaysInMonth = endDate.getDate();
	} else {
		numDaysInMonth = aNumDays[monthSelected];
	}

	datePointer = 0;
	dayPointer = startDate.getDay() - startAt;

	if (dayPointer<0) dayPointer = 6;

	sHTML = '<table border="0" style="font-family:verdana;font-size:12px;table-layout:fixed;"><tr>';

	if (showWeekNumber == 1) {
		sHTML += '<td width="33" ><b>' + weekString[language] + '</b></td><td width="1" rowspan="7" bgcolor="#d0d0d0" style="padding:0px"><img src="'+imgDir+'divider.gif" width="1"></td>';
	}

	for (i = 0; i<5; i++) {
		sHTML += '<td width="33" style="word-break: keep-all; overflow: hidden;" align="right"><b><font color="#434343">' + dayName[language][i] + '</font></b></td>';
	}
	for(i=5; i<7; i++){//Add it for new style by maxia (2010-7-21)
		sHTML += '<td width="33" style="word-break: keep-all; overflow: hidden;" align="right"><b><font color="#ff0000">' + dayName[language][i] + '</font></b></td>';
	}
	sHTML += '</tr><tr>';

	if (showWeekNumber == 1) {
		sHTML += '<td align="right">' + WeekNbr(startDate) + '&nbsp;</td>';
	}

	for	( var i=1; i<=dayPointer;i++ ) {
		sHTML += '<td>&nbsp;</td>';
	}

	for	( datePointer=1; datePointer <= numDaysInMonth; datePointer++ ) {
		dayPointer++;
		sHTML += '<td align="right">';
		sStyle=styleAnchor;
		if ((datePointer == odateSelected) && (monthSelected == omonthSelected) && (yearSelected == oyearSelected))
		{ sStyle+=styleLightBorder }

		sHint = '';
		for (k = 0;k < HolidaysCounter; k++) {
			if ((parseInt(Holidays[k].d) == datePointer)&&(parseInt(Holidays[k].m) == (monthSelected+1))) {
				if ((parseInt(Holidays[k].y)==0)||((parseInt(Holidays[k].y)==yearSelected)&&(parseInt(Holidays[k].y)!=0))) {
					sStyle+= 'background-color:#fdd;';
					sHint += sHint=="" ? Holidays[k].desc : "\n"+Holidays[k].desc;
				}
			}
		}

		sHint = sHint.replace('/\"/g', '&quot;');

		dateMessage = 'onmousemove="window.status=\''+selectDateMessage[language].replace('[date]',constructDate(datePointer,monthSelected,yearSelected, timeNow))+'\'" onmouseout="window.status=\'\'" ';



		if (enablePast == 0 && ((yearSelected < yearNow) || (monthSelected < monthNow) && (yearSelected == yearNow) || (datePointer < dateNow) && (monthSelected == monthNow) && (yearSelected == yearNow))) {
			//selDayAction = '';
			selDayAction = 'href="javascript:dateSelected=' + datePointer + ';closeCalendar();"';
			isPast = 1;
		} else {
			selDayAction = 'href="javascript:dateSelected=' + datePointer + ';closeCalendar();"';
			isPast = 0;
		}
        //remove the red color for the current day, because of adapting to new style by maxia(2010-7-21)
		//add the bold attribute for each day, because of adapting to new style by maxia(2010-7-21)
		//modify the color to red for weekday, because of adapting to new style by maxia(2010-7-21) 
		/*if ((datePointer == dateNow) && (monthSelected == monthNow) && (yearSelected == yearNow)) {	///// today
			sHTML += "<b><a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+"><font color='#ff0000' style='font-weight: bold;'>&nbsp;" + datePointer + "</font>&nbsp;</a></b>";
		} else*/
		if (dayPointer % 7 == (startAt * -1)+1) {									///// SI ES DOMINGO
			if (isPast==1)
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#909090' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
			else
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#ff0000' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
		} else if ((dayPointer % 7 == (startAt * -1)+7 && startAt==1) || (dayPointer % 7 == startAt && startAt==0)) {	///// SI ES SABADO
			if (isPast==1)
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#909090' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
			else
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#ff0000' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
		} else {																			///// CUALQUIER OTRO DIA
			if (isPast==1)
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#909090' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
			else
				sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color='#434343' style='font-weight: bold;'>" + datePointer + "</font>&nbsp;</a>";
		}

		sHTML += '';
		if ((dayPointer+startAt) % 7 == startAt) {
			sHTML += '</tr><tr>';
			if ((showWeekNumber == 1) && (datePointer < numDaysInMonth)) {
				sHTML += '<td align="right">' + (WeekNbr(new Date(yearSelected,monthSelected,datePointer+1))) + '&nbsp;</td>';
			}
		}
	}

	document.getElementById('content').innerHTML   = sHTML
	document.getElementById('spanMonth').innerHTML = '&nbsp;' +	monthName[language][monthSelected] + '&nbsp;<img id="changeMonth" src="'+imgDir+'pcaldrop1.gif" width="12" height="10" border="0">'
	document.getElementById('spanYear').innerHTML  = '&nbsp;' + yearSelected	+ '&nbsp;<img id="changeYear" src="'+imgDir+'pcaldrop1.gif" width="12" height="10" border="0">';
}

function showCalendar(ctl, ctl2, format, lang, past, fx, fy) {
	if (lang != null && lang != '') language = lang;
	if (past != null) enablePast = past;
	else enablePast = 0;
	if (fx != null) fixedX = fx;
	else fixedX = -1;
	if (fy != null) fixedY = fy;
	else fixedY = -1;

	if (showToday == 1) {
		/*document.getElementById('lblToday').innerHTML = '<font color="#000066">' + todayString[language] + ' <a onmousemove="window.status=\''+gotoString[language]+'\'" onmouseout="window.status=\'\'" title="'+gotoString[language]+'" style="'+styleAnchor+'" href="javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();">'+weekString[language]+dayName[language][(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+' '+yearNow+'/'+ monthName[language][monthNow].substring(0,3)+'/'+dateNow+':</a></font>';*/
	}
	popUpCalendar(ctl, ctl2, format);
}

function popUpCalendar(ctl, ctl2, format) {
	var leftpos = 0;
	var toppos  = 0;
	if (bPageLoaded) {
		if (crossobj.visibility == 'hidden') {
			ctlToPlaceValue = ctl2;
			dateFormat = format;
			formatChar = ' ';
			aFormat = dateFormat.split(formatChar);
			if (aFormat.length < 3) {
				formatChar = '/';
				aFormat = dateFormat.split(formatChar);
				if (aFormat.length < 3) {
					formatChar = '.';
					aFormat = dateFormat.split(formatChar);
					if (aFormat.length < 3) {
						formatChar = '-';
						aFormat = dateFormat.split(formatChar);
						if (aFormat.length < 3) {
							formatChar = '';					// invalid date format

						}
					}
				}
			}

			tokensChanged = 0;
			if (formatChar != "") {
				aData =	ctl2.value.split(formatChar);			// use user's date

				for (i=0; i<3; i++) {
					if ((aFormat[i] == "d") || (aFormat[i] == "dd") || (aFormat[i] == "DD")) {
						dateSelected = parseInt(aData[i], 10);
						tokensChanged++;
					} else if ((aFormat[i] == "m") || (aFormat[i] == "mm") || (aFormat[i] == "MM")) {
						monthSelected = parseInt(aData[i], 10) - 1;
						tokensChanged++;
					} else if (aFormat[i] == "yyyy") {
						yearSelected = parseInt(aData[i], 10);
						tokensChanged++;
					} else if (aFormat[i] == "mmm" || (aFormat[i] == "MMM")) {
						for (j=0; j<12; j++) {
							if (aData[i] == monthName[language][j]) {
								monthSelected=j;
								tokensChanged++;
							}
						}
					} else if (aFormat[i] == "mmmm" || (aFormat[i] == "MMMM")) {
						for (j=0; j<12; j++) {
							if (aData[i] == monthName2[language][j]) {
								monthSelected = j;
								tokensChanged++;
							}
						}
					}
				}


			}

			if ((tokensChanged != 3) || isNaN(dateSelected) || isNaN(monthSelected) || isNaN(yearSelected)) {
				dateSelected  = dateNow;
				monthSelected = monthNow;
				yearSelected  = yearNow;				
			}

			odateSelected  = dateSelected;
			omonthSelected = monthSelected;
			oyearSelected  = yearSelected;

			aTag = ctl;
			//deal with the control in the div which has scroll by maxia (2010-4-28)
			var scrollTop = 0;
			var scrollLeft = 0;
			do {
				aTag     = aTag.offsetParent;
				leftpos += aTag.offsetLeft;
				toppos  += aTag.offsetTop;
				if(aTag.scrollTop!=null||aTag.scrollLeft!=null){
					scrollTop+=aTag.scrollTop;
					scrollLeft+=aTag.scrollLeft;
					
				}
				if(aTag.style.overflow=="scroll"){scrollTop =aTag.scrollTop;}
			} while (aTag.tagName != 'BODY');//Modified it for new lay out by maxia(2010-7-21)
			if(leftpos>=520){
				crossobj.left=520;
			}
			else{
				crossobj.left =	(fixedX ==-1) ? leftpos-scrollLeft + 'px' : fixedX + 'px'
			}
			if(toppos>=360&&scrollTop==0){
				toppos=360;
			}
				//crossobj.left =	(fixedX ==-1) ? leftpos-scrollLeft + 'px' : fixedX + 'px'
		            crossobj.top = (fixedY==-1) ?ctl.offsetTop + toppos + ctl.offsetHeight + 2 + 'px' : fixedY + 'px'
		           
			/*if(leftpos<520){
				crossobj.left = (fixedX == -1) ? ctl.offsetLeft + leftpos-10 : fixedX;
			}else{
				crossobj.left=520;
			}
            //reposition for adapting to new style by maxia(2010-7-22)
			if(toppos<=360){
				crossobj.top = (fixedY == -1) ? ctl.offsetTop + toppos + ctl.offsetHeight  - scrollTop -40 : fixedY;
			}else{
				crossobj.top=ctl.offsetTop + toppos + ctl.offsetHeight  - scrollTop  - 230;
			}*/
			
		            		constructCalendar (1, monthSelected, yearSelected)
				            crossobj.visibility=(dom||ie)? "visible" : "show";      		
			//hideElement('SELECT', document.getElementById('calendar'));
			hideElement('APPLET', document.getElementById('calendar'));			

			bShow = true;
		} else {
			hideCalendar();
			if (ctlNow!=ctl) popUpCalendar(ctl, ctl2, format);
		}
		ctlNow = ctl;
	}
}

document.onkeypress = function hidecal1 () {
	if (event.keyCode == 27) hideCalendar();
}
document.onclick = function hidecal2 () {
	if (!bShow) hideCalendar();
	bShow = false;
}
/*
	if(ie) {
		init();
	} else {
		window.onload = init;
	}
 */
if (document.all){
	window.attachEvent('onload',init)
}
else{
	window.addEventListener('load',init,false);
}
