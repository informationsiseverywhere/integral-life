<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             
<%@ page session="false" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<html>
<head>
<title> DXC Assure Policy </title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />   
<%String ctx = request.getContextPath() + "/";%>
<%  HttpSession sess = request.getSession();    
	BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE );
	ScreenModel fw1 = (ScreenModel) baseModel.getScreenModel(); 
	LifeAsiaAppVars av = (LifeAsiaAppVars)fw1.getAppVars();
	long hb = av.getAppConfig().userHeartbeat;
	String lang1  = av.getUserLanguage().toString().toUpperCase();
	
	SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw1.getScreenName(),lang1);
	smartHF.setLocale(request.getLocale());
	String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());
	response.addHeader("Expires", "Thu, 01 Jan 1970 00:00:01 GMT");
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
    response.addHeader("X-Content-Type-Options", "NOSNIFF");
    response.addHeader("Cache-Control", "no-store"); 
    response.addHeader("Pragma", "no-cache"); 
%>

<LINK REL="StyleSheet" HREF="<%=ctx%>theme/QAStyle.css" TYPE="text/css"> 
<script language='javaScript' src='<%=ctx%>js/xmlReader.js' charset=UTF-8></script>
</head>
<body  onload='beatHeart()'>
<iframe  name='heart' src="heartbeat1.jsp"></iframe>
</body>
<script>
	var timerHeartBeat;
	function beatHeart() {
		// fix bug147
		timerHeartBeat = setTimeout("delayedBeatHeart()", <%=hb%>);
	}
	
	function delayedBeatHeart() {
		try {
			// fix bug147
			//if (document.frames.heart.document.forms[0].heartcore.value == 0) {
			if (document.frames["heart"].document.forms[0].heartcore.value == 0) {
				//alert("The Server has gone down, is inaccessible, or has been restarted. You must log in again. Attempting to redirect you to the main menu ... V2'" + document.frames[0].document.forms[0].heartcore.value + "'");
				callCommonAlert(<%=imageFolder%>,"No00024",document.frames[0].document.forms[0].heartcore.value);
				parent.location.replace('<%=ctx%>');
			}
			// fix bug147
			//else if (document.frames.heart.document.forms[0].heartcore.value == -1) {
			else if (document.frames["heart"].document.forms[0].heartcore.value == -1) {
				parent.location.replace('<%=ctx%>timeout.jsp');
			}
			else {
				//document.frames.heart.location = document.frames.heart.location;
				// fix bug147
				//document.frames.heart.location.reload(false);
				//setTimeout("delayedBeatHeart()", <%=hb%>);
				document.frames["heart"].location.reload(false);
				timerHeartBeat = setTimeout("delayedBeatHeart()", <%=hb%>);
			}
		}
		catch (e) {
			//alert("Error - The Server has gone down, is inaccessible, or has been restarted. You must log in again. Attempting to redirect you to the main menu ..." + e.toString());
			callCommonAlert(<%=imageFolder%>,"No00025",e.toString());
			parent.location.replace('<%=ctx%>');
		}
	}
</script>
</html>