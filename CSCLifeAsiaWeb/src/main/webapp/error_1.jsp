<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">   
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>          
<%@page import="com.resource.ResourceBundleHandler"%>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.util.*" %>
<%String ctx = request.getContextPath() + "/";          
 	response.addHeader("X-Frame-Options", "SAMEORIGIN");
  	response.addHeader("X-Content-Type-Options", "NOSNIFF");
	response.addHeader("Cache-Control", "no-store"); 
	response.addHeader("Pragma", "no-cache"); 
%>           
<HTML> 
<HEAD>                                                            
<%
	ResourceBundleHandler resourceBundle = new ResourceBundleHandler(request.getLocale());
 %>
 <title> <%=resourceBundle.gettingValueFromBundle("ERROR PAGE") %> </title>
 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<%AppVars av = AppVars.getInstance();
	if(av.getAppConfig().getJspConfigPath()!=null && !av.getAppConfig().getJspConfigPath().isEmpty()){
		av.setJspConfigPath(av.getAppConfig().getJspConfigPath());
}%>
 	<%if(av.getJspConfigPath().equals("jsp")){ %>
	 	<meta http-equiv="pragram" content="no-cache">
	  	<meta http-equiv="cache-control" content="no-cache, must-revalidate">
	  	<meta http-equiv="expires" content="0">
		<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
		<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
		<link href="<%=ctx%>bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
	<%}else{ %>
	    <LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
	    <LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">
	<%} %>
</HEAD>
<%if(!av.getJspConfigPath().equals("jsp")){ %>                                                            
  <BODY class="main">
      <table class="main_table">
        <tr>
          <td class="page_heading" align="left" > <%=resourceBundle.gettingValueFromBundle("ERROR PAGE") %> </td>
        </tr>
      </table>
  </BODY>
 <%}else{ %> 
 	<body>
  		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-right:15px;margin-bottom: 0;background-color: #FFFFFF;"><!-- IBPLIFE-5274 -->
  			<a class="navbar-brand" style="color: #337ab7;font-size:20px;"><%=resourceBundle.gettingValueFromBundle("ERROR PAGE") %></a>
  		</nav>
  </body>
 <%} %>
</HTML>
