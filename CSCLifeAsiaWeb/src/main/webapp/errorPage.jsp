<%@ page import="java.util.*" %> 
<%@ page import="com.quipoz.framework.error.*" %>   
<%@ page import="com.quipoz.framework.exception.*" %> 
<%@ page import="com.quipoz.framework.util.*" %>  
<%@ page import="com.quipoz.framework.screenmodel.*" %> 
<%@ page import="com.properties.PropertyLoader" %> 
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page errorPage="true" %>
<%	
Locale locale = (Locale)request.getSession().getAttribute("integralLocale");
String loc;
if(locale == null){
	loc = "en_US";
}
else
{
	loc = locale.toString();
}
String imageFolder = PropertyLoader.getFolderName(loc);
// session.invalidate();//IJTI-1131
%>

<HTML>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<LINK REL="StyleSheet" HREF="<%=request.getContextPath()%>/theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=request.getContextPath()%>/theme/eng/QAStyle.css" TYPE="text/css">
<TITLE> ERROR PAGE </TITLE><!-- IBPLIFE-5274 -->
<link rel="icon" href="screenFiles/dxc-titlebar-logo.svg"><!-- IBPLIFE-5274 -->
<!-- IJTI-1210 START -->
<%
	String error = "";
	int status = ((HttpServletResponse) response).getStatus();
	if (status == 404) {
		error = "PAGE NOT FOUND";
	} else if (status == 500) {
		error = "INTERNAL SERVER ERROR";
	}
%>
<!-- IJTI-1210 END -->
<BODY class="menu">
	<table>
		<tr><td><img src="<%=request.getContextPath()%>/screenFiles/<%=imageFolder%>/dxc_symbol.jpg" class="logo"></td>
			<td class="page_heading" align="left" width="180%"><span style="color: #FFFFFF;font-weight: 500 !important"><%=error%></span></td><!-- IBPLIFE-5274 -->
		</tr>
	</table>
    <BR/>
    <P TITLE="Return to Login Page"><A class="dmd_text_label" href="<%=request.getContextPath()%>/logout" TARGET="_top">Login Again</A></P><!-- IBPLIFE-5274 -->
  </BODY>
</HTML>