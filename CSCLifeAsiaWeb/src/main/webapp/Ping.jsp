<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Timings</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.util.*" %>

</head>
<BODY class="main">
<script src="js/Sidebar.js"></script>
<h2>Network response estimate</h2>
<p>This screen submits a trivial request to the Application Web Server.
<p>No application code is used.
<p>This test can be used to see if response time issues are due to the
Network/Web Server.
<p>
<Button onClick='doAgain()'>Repeat</Button>
<br><Button onClick="window.close()">Close this window</Button>
<Script>
    sT = window.dialogArguments;
    sT1 = new Date(sT.getYear(), sT.getMonth(), sT.getDate(), sT.getHours(), sT.getMinutes(), sT.getSeconds());
    sTime = fm2(sT.getHours()) + ":" + fm2(sT.getMinutes()) + ":" + fm2(sT.getSeconds()) + "." + fm3(sT.getTime() - sT1.getTime()); 

    eT = new Date();
    eT1 = new Date(eT.getYear(), eT.getMonth(), eT.getDate(), eT.getHours(), eT.getMinutes(), eT.getSeconds());
    eTime = fm2(eT.getHours()) + ":" + fm2(eT.getMinutes()) + ":" + fm2(eT.getSeconds()) + "." + fm3(eT.getTime() - eT1.getTime()); 

	document.write("<Table>");
	document.write("<tr><td>Request  time:</td><td>" + sTime + "</td></tr>");
	document.write("<tr><td>Response received</td><td>" + eTime + "</td></tr>");
	document.write("<tr><td>Web/Network response time</td><td>" + ((eT.getTime() - sT.getTime())/1000) + "</td></tr>");
	document.write("</Table>");
  function fm2(num) {
    if (num < 10) return "0" + num;
    return num;
  }
  
  function fm3(num) {
    if (num < 10) return "00" + num;
    if (num < 100) return "0" + num;
    return num;
  }
  
  function doAgain() {
  	window.showModalDialog ('Ping.jsp', new Date(), 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');
  	window.close();
  }
</Script>
</BODY>
</HTML>