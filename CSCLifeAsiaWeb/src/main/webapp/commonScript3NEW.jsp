<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.*' %>
<%@ page import='com.quipoz.framework.screenmodel.*' %>
<%@page import="com.csc.smart400framework.SMARTAppVars"%>
<%@ page import='com.csc.smart400framework.SmartVarModel' %>
	<!--This stupid bit of code exists ONLY to stop Websphere tagging "fw"
	    as an unresolved variable in this jsp fragment.
	-->
	<%String cs1ctx = request.getContextPath();
	  BaseModel bm1 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
      ScreenModel fw1 = (ScreenModel) bm1.getScreenModel();
      String lang1  = bm1.getApplicationVariables().getUserLanguage().toString().toUpperCase() + "/"; 
      String currenttab=((SmartVarModel)fw1.getVariables()).currenttab.toString();
    %>
	
	<script language='javaScript'>
    	thisField = '<%=fw1.getValue("focusField")%>';
    	lastField = '<%=fw1.getValue("focusPrevF")%>';
    	currField = '<%=fw1.getActiveField()%>';
    	nextOPrev = '<%=fw1.getValue("focusNxtPr")%>';
    	semaphore = '<%=fw1.getSemaphore()%>';
    	popupMenu = '<%=fw1.getPopupMenu()%>';
    	todayDate = '<%=fw1.getCurrentDate()%>';
    	popup     = "<%=fw1.hasPopup()%>";
    	error     = "<%=fw1.isFieldInError()%>";
    	thisElt   = null;
    	validKeys    = null;
    	//rollUpEnabled and rollDownEnabled will be set in commonScript2.jsp
    	rollUpEnabled = true;
    	rollDownEnabled = true;
   	    cancelled = true; //Will be set to false in process action code. If true, means the user closed the browser.
	</script>

	<script language='javaScript'>
		function getCommonScreens(screen) {
			return "<%=cs1ctx%>/commonJSP/<%=lang1%>" + screen; 
		}
		function suppressit() {
			return false;
		}
		var contextPathName = "<%= request.getContextPath() %>";
	</script>
	

<%-- <script language="javaScript" src='<%=cs1ctx%>/js/popcalendar.js'></script> --%>
<script language='javaScript' src='<%=cs1ctx%>/js/xmlReader.js' charset=UTF-8></script>
<script language='javaScript' src='<%=cs1ctx%>/js/commonScript1.js' charset=UTF-8></script>
<script language='javaScript' src='<%=cs1ctx%>/js/Sidebar.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/operateTable.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/operateSuperTable.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/operatePopupTable.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/CheckboxClickHandler.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/dropdownlist_varCode.js'></script>
<script type="text/javascript" src='<%=cs1ctx%>/js/dropdownlist.js'></script>
<script language="javascript" src='<%=cs1ctx%>/js/superTables.js'></script>
<script language='javaScript' src='<%=cs1ctx%>/js/textarea.js' charset=UTF-8>
</script>
<Script>
	ctx = "<%=cs1ctx%>" + "/";
	lang = "<%=lang1%>";

	function doCancel() {
		window.showModelessDialog ("<%=cs1ctx%>/AutoKill.jsp", null, "dialogWidth:640px; dialogHeight:420px; resizable:yes;");
	}
</Script>
<form name="form1" method="post" action="<%=cs1ctx%>/process" target="_top" onSubmit="return suppressit()" onClick="selectSpecial()">
<div style='position:absolute; top:-80; left:-20'>
<input name='stopper1' class="invisibleSubmit" type="BUTTON" onFocus="selectFirstField();" TABINDEX=1>
<input name='busddates' id='busddates' style="visibility:hidden" value="<%=((SMARTAppVars)AppVars.getInstance()).getBusinessdate()%>">
<input name='dateformat' id='dateformat' style="visibility:hidden" value="<%=((SMARTAppVars)AppVars.getInstance()).getAppConfig().getDateFormat()%>">
<input name='imgpath' id='imgpath' style="visibility:hidden" value="<%=cs1ctx%>/screenFiles/">
<input name='lang1' id='lang1' style="visibility:hidden" value="<%=lang1%>">
<input name='dropSelect' id='dropSelect' style="visibility:hidden" value="<%=resourceBundleHandler.gettingValueFromBundle("Select")%>">
<input name='tableAddButton' id='tableAddButton' style="visibility:hidden" value="<%=resourceBundleHandler.gettingValueFromBundle("Add")%>">
<input name='tableRemoveButton' id='tableRemoveButton' style="visibility:hidden" value="<%=resourceBundleHandler.gettingValueFromBundle("Remove")%>">
<input name='tableMoreButton' id='tableMoreButton' style="visibility:hidden" value="<%=resourceBundleHandler.gettingValueFromBundle("More")%>">
<input name='hiddeninput' id='hiddeninput' style="position: relative; top:0; left:0; width:0; z-index:2;">
<input name='currenttab' id='currenttab' style="visibility:hidden" value="<%=currenttab%>"> 

</div>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.ACTIVE_FIELD%>" <%=AppVars.hf.getLit("value=\"\">")%>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.PLACE_HOLDER%>" value="<%=request.getSession().getAttribute(RequestParms.PLACE_HOLDER)%>"<%=AppVars.hf.getLit(">")%>
      <%=AppVars.hf.getLit("<input")%> type="hidden" name="<%=RequestParms.PROCESSTIME%>" <%=AppVars.hf.getLit("value=\"\">")%>

