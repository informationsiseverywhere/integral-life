<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             
<%@ page session="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.Set"%> 
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%><html>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<head>
<% 
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
response.setHeader("Pragma","no-cache"); //HTTP 1.0 
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>

<title>Life Asia application</title>
<meta http-equiv="pragram" content="no-cache">
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">   
<!-- sb admin begin -->
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<!-- sb admin end -->
<%String ctx = request.getContextPath() + "/";
//Move these variable here to judge system language.
	BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel == null) {
    	return;
    }
    ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
	String lan = av.getUserLanguage().toString().trim().toLowerCase();
if ("".equals(lan)){lan = "eng";}
%>
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/superTables.css" TYPE="text/css">  
<!-- sb admin begin -->
<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/morrisjs/morris.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css"	rel="stylesheet" type="text/css">
<%-- <link href="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"> --%>
<link href="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.css" rel="stylesheet" media="screen">
<link href="<%=ctx%>bootstrap/integral/integral-admin.css"	rel="stylesheet" type="text/css">
<link href="<%=ctx%>bootstrap/sb/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<script src='<%=ctx%>bootstrap/sb/vendor/jquery/jquery.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/vendor/metisMenu/metisMenu.min.js'></script>
<script src='<%=ctx%>bootstrap/sb/dist/js/sb-admin-2.js'></script>
<%-- <script src="<%=ctx%>bootstrap/datetimepicker/bootstrap-datetimepicker.min.js" charset="UTF-8"></script> --%>
<script src="<%=ctx%>bootstrap/datepicker/bootstrap-datepicker.min.js" charset="UTF-8"></script>
<script src="<%=ctx%>bootstrap/integral/integral-admin.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables/js/dataTables.min.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="<%=ctx%>bootstrap/sb/vendor/datatables/js/dataTables.fixedColumns.min.js"></script>

<!-- YY -->
<link href="<%=ctx%>bootstrap/sb/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=ctx%>bootstrap/sb/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- sb admin end -->
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.quipoz.COBOLFramework.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.*" %>
<%@ page import="com.quipoz.framework.datatype.*" %>
<%@ page import="com.quipoz.COBOLFramework.TableModel.*" %>
<%@ page import="com.csc.smart400framework.SmartVarModel" %>
<%@ page import="com.csc.smart400framework.AppVersionInfo" %>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatterX" %>
<%@ page import="java.lang.reflect.Field" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.quipoz.framework.util.DataModel"%>


</head>
<%!
     
     public String formatValue(String aValue) { 
     	return aValue;
     }
     
     public  String getKeysFromValue(Map hm, String value){
     Set set= hm.keySet(); 
     Iterator it = set.iterator();
	    while(it.hasNext()){
	       Object o=it.next();
	        if(((String)hm.get(o)).equals(value)) {
	            return (String)o;
	        }
	    }
	    return null;
	  }
     
    class KeyValueBean implements Comparable{
     
     private String key;
     
     private String value;

     
	public KeyValueBean(String key, String value) {
		this.key = key;
		this.value = value;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int compareTo(Object o) {
		return this.value.compareTo(((KeyValueBean)o).getValue());
	}


	public String toString() {
		
		return "Key is "+key+" value is "+value;
	}
     
    }
     
     //second class
     
     public class KeyComarator implements Comparator{

		public int compare(Object o1, Object o2) {
			
			return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
		}

	}
     public String makeDropDownList(Map mp , Object val , int i) {   
        
     		String opValue = "";
     		Map tmp= new HashMap(); 
			tmp = mp;
			String aValue = "";		
			if(val != null) {  
			if(val instanceof String){
			 aValue = ((String) val).trim();
			 }else if (val instanceof FixedLengthStringData){
			 aValue = ((FixedLengthStringData) val).getFormData().trim();
			 }
			} 
			
			Iterator mapIterator=tmp.entrySet().iterator();
			ArrayList keyValueList= new ArrayList();
			
			while(mapIterator.hasNext()){
			    Map.Entry entry= (Map.Entry)mapIterator.next();
				KeyValueBean bean = new KeyValueBean((String)entry.getKey(),(String)entry.getValue());
				keyValueList.add(bean);
			}
			
			
			
			int size = keyValueList.size();
			
			opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" +  "</option>"; 
			String mainValue ="";
			//Option 1 fr displaying code
			if(i==1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList,new KeyComarator());
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getKey() + "\" SELECTED>" +keyValueBean.getKey() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getKey() + "\">" +keyValueBean.getKey()+ "</option>";
						} 
			}
			}
			//Option 2 for long description
			if(i==2) {
			Collections.sort(keyValueList);
			
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						} 
			 }
			} 
			//Option 3 for Short description
		if(i==3) {
		Collections.sort(keyValueList);
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						} 
			}  
		}
			//Option 4 for format Code--Description
		if(i==4) {
		Collections.sort(keyValueList);
			opValue = "";
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" +keyValueBean.getValue()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getKey()+"--"+keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey() + "^" +keyValueBean.getValue()+ "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getKey()+"--"+ keyValueBean.getValue() + "</option>";
						} 
			 }  
		}
			return opValue; 
	}
	/* This method is added to solve the bugzilla defect 1838 --(to solve the refresh issue in subfiles)*/
	public String makeDropDownList1(Map mp , Object val , int i){
	
	   
        
     		String opValue = "";
     		Map tmp= new HashMap(); 
			tmp = mp;
			String aValue = "";		
			if(val != null) {  
			if(val instanceof String){
			 aValue = ((String) val).trim();
			 }else if (val instanceof FixedLengthStringData){
			 aValue = ((FixedLengthStringData) val).getFormData().trim();
			 }
			} 
			
			Iterator mapIterator=tmp.entrySet().iterator();
			ArrayList keyValueList= new ArrayList();
			
			while(mapIterator.hasNext()){
			    Map.Entry entry= (Map.Entry)mapIterator.next();
				KeyValueBean bean = new KeyValueBean((String)entry.getKey(),(String)entry.getValue());
				keyValueList.add(bean);
			}
			
			
			
			int size = keyValueList.size();
			
			opValue = opValue + "<option value='    ' title='---------Select---------' SELECTED>---------Select---------" +  "</option>"; 
			String mainValue ="";
			//Option 1 fr displaying code
			if(i==1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList,new KeyComarator());
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getKey() + "\" SELECTED>" +keyValueBean.getKey() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getKey() + "\">" +keyValueBean.getKey()+ "</option>";
						} 
			}
			}
			//Option 2 for long description
			if(i==2) {
			Collections.sort(keyValueList);
			
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						} 
			 }
			} 
			//Option 3 for Short description
		if(i==3) {
		Collections.sort(keyValueList);
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						} 
			}  
		}
			return opValue; 
	
	}
	
	public String makeDropDownList(ArrayList keyValueList,String strKeyValue){
		String opValue = "";
		int size = keyValueList.size();
		 
		String mainValue ="";
		//Collections.sort(keyValueList);
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" +keyValueBean.getValue()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getKey()+"--"+keyValueBean.getValue() + "</option>";
					
			} else {
				opValue = opValue + "<option value=\"" +  keyValueBean.getKey() + "^" +keyValueBean.getValue()+ "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getKey()+"--"+ keyValueBean.getValue() + "</option>";
			} 
		} 
		return opValue;
	}
	public String createDiscription(ArrayList keyValueList,String strKeyValue){
		int size = keyValueList.size();
		String longValue="";
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				longValue=keyValueBean.getValue();
			}		
		} 
		if(longValue.equals("")){
			longValue=strKeyValue;
		}
		return longValue;
	}
	 public String makeLongDescription(Map mp , Object aValue ) {   
        
     		
     		Map tmp= new HashMap(); 
			tmp = mp;			 
			String longDesc ="";
			
			
		
				longDesc = (String)tmp.get("BB");
			return longDesc; 
	}
%>
<%!SMARTHTMLFormatterX smartHF = new SMARTHTMLFormatterX();//Define the smartHF to gloable %>
<% 
	LifeAsiaAppVars appVars = av;
	boolean[] savedInds = null;
	if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */
        av.reinitVariables();
        String lang = av.getInstance().getUserLanguage().toString().trim();

	QPScreenField qpsf = null; 
	DataModel sm = null;
	String formatValue = null;
	String valueThis = null;
	String longValue = null;
	String localeimageFolder = lan;
	SMARTHTMLFormatterX smartHF = new SMARTHTMLFormatterX(fw.getScreenName(),lang);
	if (lang.equalsIgnoreCase("ENG")) {
		lang = "E";
	} else {
		lang = "S";
	}
	
	Map fieldItem=new HashMap();//used to store page Dropdown List
	String[] dropdownItems = null;  //used to store page Dropdown List
	Map mappedItems = null;
	String optionValue = null;
%>

<%
String tit ="";
String titTemp ="";
String screenNumber = this.getClass().getSimpleName();
screenNumber = screenNumber.substring(1);


 tit = (av.formatTitle(fw.getPageTitle(), (SmartVarModel)fw.getVariables())).replaceAll("&nbsp;","");
ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),lang);

 %>

<script language='javaScript'> 
$(document).ready(function() {
		//set the policy header information to the newly added fields	
	$("#screenTitleP").text($("#invisibletitle").val());
	//set error tab
	var i=1;
	var j=0;
	while (document.getElementById("content"+i) != null) {
		var isError = false;
		$("#content"+i).find("input,div").each(function(j) {
		    if ($(this).hasClass("red")||$(this).css("background-color")=="#ff0000" )
				isError = true;
		});
		if (isError){
			error_display(true,i);
			if(j==0){
				j=i;
			}
		}else{
			error_display(false,i);
		}
		i++;
	}
	if(i>0&&j>0){//When the page has tab and error fields,set the error tab as current tab. 
		if($("input[name='currenttab']").length!=0){
			$("input[name='currenttab']").val((i-1)+","+j);
		}
	}
	changeScrollBar("subfo");
	//set default tab
	//if($("input[name='currenttab']").length!=0){
	if($("input[name='tabcurrent']")!=null && $("input[name='tabcurrent']").length!=0){
		var curtab=$("input[name='currenttab']").val();
		var comp=curtab.indexOf(",");
		if(comp>0){
			var number=parseInt($.trim(curtab.substring(0,comp)));
			var index=parseInt($.trim(curtab.substring(comp+1)));
			if(j>0){
				change_option(number,j);
			}else{
				change_option(number,index);
			}
		}
	}
});

	function doCheck() {
		keyClosing = (event.altKey == 1 || event.ctrlKey == 1) && thisKey == 115;
		if (!submitted && (event.clientY <= 0 || keyClosing))  {
			event.returnValue = "Your transactions will be rolled back and you will be logged off."; 
		}
	}
	//Deal with the hyperlink which related to two fields.	
	function hyperLinkTo(nextField){
			nextField.value="X";
			doAction('PFKEY0');
	}
	function removeXfield(xfield){
	xfield.value="";
		doAction('PFKEY05');
	}
	function change_option(number,index)
	{
	 var error_flag = 0;
	 var error_index = new Array(number + 1);
	
	 if($("input[name='currenttab']").length!=0){
		$("input[name='currenttab']").val(number+","+index);
	}
	 for (var i = 1; i <= number; i++) {
	 	if((document.getElementById('current' + i).className != 'tabcurrenterror') && (document.getElementById('current' + i).className != 'taberror') )	
	    {	
	        document.getElementById('current' + i).className = 'tab';//set class for tabs by Francis
	      document.getElementById('tabContent' + i).style.display = 'none';
	    } else {
	    	error_flag ++;
			error_index[i] = i;
	    }
	 }
	 if(error_flag == 0){
	 	 document.getElementById('current' + index).className = 'tabcurrent';//set class for tabs by Francis
	  document.getElementById('tabContent' + index).style.display = 'block';
	 } else{//Modified the class name by maxia (2010-7-27)
		  for (var i = 1; i <= number; i++) {
		     if(error_index[i] != undefined) {
				 if(error_index[i] == index) {
				      if(document.getElementById('current' + error_index[i]).className == "taberror") {
				      	  document.getElementById('current' + error_index[i]).className = 'tabcurrenterror';
	    			      document.getElementById('tabContent' + error_index[i]).style.display = 'block';
	    			      continue;
				      }
				      if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
				      	  document.getElementById('tabContent' + error_index[i]).style.display = 'block';
				      }
				 } else {
	 				  if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
	    			      document.getElementById('current' + error_index[i]).className = 'taberror';
	    	              document.getElementById('tabContent' + error_index[i]).style.display = 'none';
		              } else if(document.getElementById('current' + error_index[i]).className == "taberror"){
		              	  document.getElementById('tabContent' + error_index[i]).style.display = 'none'; 
		              } else  {
		              	  document.getElementById('current' + index).className = 'tab';
	 				      document.getElementById('tabContent' + index).style.display = 'block';
		              }
					  
	             }
					 
			 } else {
			     if(i == index) {
			 	     document.getElementById('current' + i).className = 'tabcurrent';
	 	 			 document.getElementById('tabContent' + i).style.display = 'block';
			 	 }			 
	}

		  }
	 }

}

	function error_display(ind,index) 
	{
		if(ind == true){
			if(document.getElementById('current' + index).className == 'tabcurrent') {
				document.getElementById('current' + index).className = 'tabcurrenterror';
			} else {
				document.getElementById('current' + index).className = 'taberror';
			}
		} 
			
	}


	
function changeF4Image(thi){
  var sorce=thi.childNodes[0].src;  
			
  if(sorce.indexOf("_after")==-1){
    var img1 = new Image();
             img1.src=sorce.replace(".gif","_after.gif");
             thi.childNodes[0].src=img1.src;
   }else{
   	thi.onclick=function(){return  false;};
   }
}

function changeMoreImage(thi){
	   var sorce=thi.childNodes[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);
	  
	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=thi.childNodes[0].src;
	  }
	    
	   var endObj = sorce.indexOf("_hover");
	   
	   
	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";   
	   }
	   if(sorce.indexOf("_after")==-1){
	   
	   img1.src=sorce.replace(".gif","_after.gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }
	       
	       
	  
	   }else{
	   
	   }
	}

function changeMoreImageOut(thi)
{
	   var sorce=thi.childNodes[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);
	  
	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=thi.childNodes[0].src;
	  }
	    
	   var endObj = sorce.indexOf("_hover");
	   
	   
	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";   
	   }
	   if(sorce.indexOf("_after")==-1){
	   	   
	   }else{
		   img1.src=sorce.replace("_after.gif",".gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }
	   }
	}
function obj(aData,aValue,aText){ 	
	this.Data=aData;	
	this.Value=aValue;	
	this.Text=aText;
  }
  function chg(parent,child){	
	chgComitem(parent.options[parent.selectedIndex].value,child,Set_data);
	}
  function chgComitem(parentValue,child,objs){ 
	  DelAllComitem(child);	
	  parentValue = parentValue.substring(0,3);
	  for(i=0;i<objs.length;i++)
	  {		
		  if (objs[i].Data==parentValue)		
		AddComitem(child,objs[i].Value+"^"+objs[i].Text,objs[i].Text);
	   }
   }
  function DelAllComitem(aList){  
	for(i=aList.options.length-1;i>=0;i--)
	{
		aList.options[i]=null;
	}
	}
  function AddComitem(aList,aValue,aText)
  {	
	var aOption=new Option(aText,aValue);
	aList.options[aList.options.length]=aOption;
	}
	
	//This function deal with "locate to the record at the begining of the next page".
	function changeScrollBar(thi){
		if(document.getElementById(thi) != null) {
			var obj = document.getElementById(thi);
			obj.scrollTop = obj.scrollHeight;
		}	
	}
  //This function moved form POLATitle.jsp by Michelle (2010-7-20)
  function kill(language) {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm(language,"No00031"));
		if (x == true) {
			// fix bug147
			top.frames["heartbeat"].frames["heart"].clearTimeout(
				top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}
	
	
	//Added by Amit for checkbox
function handleCheckBox(name)
{
	var cbs = document.forms[0].elements[name];
	
	if(!cbs[0].checked){
		cbs[1].checked=true;
	}else{
		cbs[1].checked=false;
	}
	
	
}




function perFormOperation(act){
	if(selectedRow1!=null && selectedRow1!=""){
		document.getElementById(selectedRow1).value=act;
		doAction('PFKEY0'); 
	}else{
		var elementSelected=false;
		for (var index = 0; index < idRowArray.length; ++index) {
			var item = idRowArray[index];
			if(item!=null && item!=""){
				document.getElementById(item).value=act;
				elementSelected=true;
			}
		}
		if(elementSelected){
			doAction('PFKEY0'); 
		}
	}
}
var idRowArray=new Array();
var radioOptionElementId;
var radioElementCounter;
function selectedRowChecked(idt1,idt2,count)
{
	count=count-1;
		if(document.getElementById(idt2).type!='radio'){
			
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value='1';
				idRowArray[count]=idt1;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
			}
		}else{
		
			if(radioOptionElementId!=null && radioOptionElementId!=""){
				var item = idRowArray[radioElementCounter]; 
				if(item!=null && item!=""){
					if(document.getElementById(radioOptionElementId).checked){
						document.getElementById(item).value=' ';
						document.getElementById(radioOptionElementId).selected=false;
						document.getElementById(radioOptionElementId).checked=false;
						idRowArray[radioElementCounter]="";
						radioOptionElementId="";
					}
				}
			}
			if(document.getElementById(idt2).checked){
				document.getElementById(idt1).value='1';
				idRowArray[count]=idt1;
				radioElementCounter=count;
				radioOptionElementId=idt2;
			}else{
				document.getElementById(idt1).value=' ';
				idRowArray[count]="";
				radioElementCounter="";
				radioOptionElementId="";
			}
		}
}
function createOperationArray(idt1,idt2,count)
{
	count=count-1;
	idRowArray[count]=idt1;
	radioElementCounter=count;
	radioOptionElementId=idt2;
}

var selectedRow1;
var firstTimeOver=false;
function selectedRow(idt){

	document.getElementById(idt).value='1';
	document.getElementById(idt).selected=true;

	if(firstTimeOver && document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value=' ';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;
}


function ChangeCheckBoxYN1(idt, idt2){
		if(document.getElementById(idt2).checked){
			document.getElementById(idt).value='Y';
		}else{
			document.getElementById(idt).value='N';				
		}
}


function ChangeCheckBoxYN(idt){

		if(document.getElementById(idt).checked){
			document.getElementById(idt).value='Y';
			document.getElementById(idt).selected=true;
			document.getElementById(idt).checked=true;
		}else{
			document.getElementById(idt).value='N';
			document.getElementById(idt).selected=false;
			document.getElementById(idt).checked=false;	
		}
}

var rowID;
function selectedForDelete(idt)
{
   rowID=idt;
   //alert(rowID);
}

//**************************** Add Row Start *********************************

function addRow(strtindx , endindx) 
{
	  var table = document.getElementById("table");
	  var rowCount = table.rows.length;
	  var row=table.rows;
	  var tempvisibility = null, tempvisibility1 = 'hidden'.toString();
	  var count=0;
		
		for(var z=0;z<rowCount;z++)
		{
			tempvisibility = row[z].style.visibility;
			
			if (tempvisibility.toLowerCase() == tempvisibility1)
			{
					count=parseInt(count)+1; 
					
			}
		}

	count=parseInt(rowCount)-parseInt(count)-1;
	if(parseInt(count)<parseInt(rowCount-1))
	{	


	  var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";
		
		var check = "",flagname = true,flagtype = true, flg = true;
		var elementIndex=0;
		var visibleElement = new Array();
		for(var r=0; r<2; r++)
		{
			check[r] = 'nxtrow';
		}

	 //alert("strtindx : "+strtindx+" endindx : "+endindx);
	 for(var i=1; i<=rowCount-1; i++){
	 	 var visibility = row[i].style.visibility;
	    var disp = row[i].style.display;
	   	if (visibility.toLowerCase() != tempvisibility1){
				visibleElement[parseInt(i)-1]=true;	
			}else{
				visibleElement[parseInt(i)-1]=false;
			}
	 }	
	 for(i=1; i<=rowCount-1; i++)
	 {
	    visibility = row[i].style.visibility;
	    disp = row[i].style.display;
	   	if(visibleElement[parseInt(i)-1])
		{
		for (var j=1; j<=parseInt(endindx); j++)
		{
			str = row[i].cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;

			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}
				
				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}
		
				
				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype)
				{
					typeOfElement = 'text'
					flagtype = false;
				}
			}

			if(typeOfElement.toLowerCase() ==  'select')
				{
					if(document.getElementById(nameOfElement.toString()).selectedIndex == 0)
					{
						check[k] = 'nxtrow1';
						flg = false;
					}
						typeOfElement = "";
						nameOfElement = "";
				}
				
				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					if(document.getElementById(nameOfElement.toString()).value.trim() == null || document.getElementById(nameOfElement.toString()).value.trim() == "")
					{
						flg = false;
					}
					typeOfElement = "";
					nameOfElement = "";
				}
			
			flagtype = true;
			flagname = true;
		}
	
		}else{
			elementIndex=parseInt(i);
			break;
		}
	}
		//alert("<<<<<<<<<<<<<< Flag >>>>>>>>>>>> "+flg);
	
		if(!flg)
		{
			alert("Field can not be empty!");
		}else
		{
			  row[parseInt(elementIndex)].style.visibility='visible';
			  row[parseInt(elementIndex)].style.display = '';
		}
	}else{
					// No more left
				
					doAction('PFKEY90');
		}
}


//**************************** Add Row End *********************************

// **************************** Reset Field for Delete row operation Start *********************************
function resetfields(subStr)
{
		var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";
		var check = "", flagname = true,flagtype = true;
		var noofChild = document.getElementById(subStr).cells.length;
		var row=document.getElementById(subStr);
		
		for (var j=0; j<parseInt(noofChild); j++)
		{
			str = row.cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;
			
			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}
				
				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}

				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype)
				{
					typeOfElement = 'text'
					flagtype = false;
				}
				
			}

			
				if(typeOfElement.toLowerCase() ==  'select')
				{
					document.getElementById(nameOfElement.toString()).selectedIndex = 0;
				}
				
				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					document.getElementById(nameOfElement.toString()).value = "";
				}
			
			flagtype = true;
			flagname = true;
		}
}
// **************************** Reset Field for Delete row operation End *********************************

// **************************** Delete Row Start *********************************
var indx = 0;
 function getSelected(opt) 
 {
	var selected = new Array();
	
	for (var intLoop = 0; intLoop < opt.length; intLoop++) 
	{	
	   if (opt[intLoop].checked) 
		{
		  indx = selected.length;
		  selected[indx] = new Object;
		  selected[indx].value = opt[intLoop].value;
		  selected[indx].index = intLoop;
		
	   }
	}
	

	for(var i=0; i<selected.length;i++)
	{
		//alert("================ selected[i].value : "+ selected[i].value);
		//alert("================ selected[i].index : "+ selected[i].index);
	}
	return selected;
 }

 function removeSelected(opt) 
 {
	indx = 0;
	for (var intLoop = 0; intLoop < opt.length; intLoop++) 
	{
	   if (opt[intLoop].checked){
			opt[intLoop].checked = false;
	   }
	}
 }

 function getCheckBoxName(rowID)
 {
 	var innerHtml = document.getElementById(rowID).innerHTML;
 	var nameOfElement, subStr1;
 	var str1 = innerHtml.split("<td>");
 	var len = str1.length;
 			
 	if((parseInt(str1[0].indexOf('id='))!= parseInt(-1)))
 	{
 		 subStr1 = str1[0].substring(parseInt(str1[0].indexOf('id='))+3);
 		 nameOfElement = subStr1.substring(0,parseInt(subStr1.indexOf(" ")));
 		 
 		if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
 			nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))

 	}
 	return nameOfElement;
 }
 

	
function deleteRow(opt) 
{	

		var indexstr = 8;
		var table = document.getElementById("table");
		var row=table.rows.length;
		var totalRow;
		var substr1,substr,str;
		var sel = new Array();
		sel = getSelected(opt);
			  var tableRow=table.rows;
			  var tempvisibility = null, tempvisibility1 = 'hidden'.toString();
			  var rowCount=0;
			for(var z=0;z<row;z++){
					tempvisibility = tableRow[z].style.visibility;
					
					if (tempvisibility.toLowerCase() == tempvisibility1){
							rowCount=parseInt(rowCount)+1; 	
					}
				}		
		rowCount=parseInt(row)-parseInt(rowCount)-1;
		
		for(var i=sel.length-1; i>=0;i--)
		{
			if(rowCount!=sel.length||i!=0)
			{
			substr = parseInt(sel[i].index);
			totalRow = parseInt(row)-1;
			
			if(parseInt(substr)<totalRow){
				substr1 = parseInt(substr)+1;
			}else{
				substr1 = parseInt(substr);
			}
			var substr2 = "tablerow"+substr1; 
			var chkname = getCheckBoxName(substr2); 
					
			if(substr1<totalRow)
			{
				document.getElementById(chkname).checked = 'false';
				document.getElementById(substr2).style.visibility='hidden';
				document.getElementById(substr2).style.display='none';
			}
			if(substr1==totalRow)
			{
				document.getElementById(chkname).checked = 'false';
				document.getElementById(substr2).style.visibility='hidden';
				document.getElementById(substr2).style.display='none';
			}
			resetfields(substr2);
		}
		}
		for(var i=0; i<opt.length;i++)
		{
			//alert("opt[i].checked : "+opt[i].checked)
		}
		removeSelected(opt);

}




function moveScroll()
{
   var div1 = document.getElementById("subfo");
   var scrollAmt = div1.scrollTop;
  
  div1.scrollTop = parseInt(div1.scrollHeight);
}


function clearFocusField() {
	lastSelectedF = null;
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
	if (aForm.focusInField) {
      	aForm.focusInField.value = "";
    }
}

function setValues(index){
		var checked = false;
		$("#"+"subfile_table").find("tr").each(function(j) {
			var parent = $(this);
			$(this).find("input[type='checkbox']").each(function(i) {
				var temp = $(this).attr("checked");
				if (temp == true){
					parent.find("input[type='hidden'][name*='chk']").each(function (i){
						$(this).val("1");
					});
				}else{
					parent.find("input[type='hidden'][name*='chk']").each(function (i){
						$(this).val("");
					});
				}
			});
		});
	}
 </script>
 <body>
 
 
 
<%@ include file="commonScript3NEW.jsp"%>
	<div id="mainareaDiv">
	    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-right:15px;margin-bottom: 0">  
	          <ul class="nav navbar-top-links navbar-right">
	          <%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %>
				<li><a href="javascript:;" onClick="doAction('PFKEY02')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="<%=resourceBundleHandler.gettingValueFromBundle("Session Info")%>"><i class="fa fa-info-circle"></i></a></li>
		        <li><a href="javascript:;" onClick="doAction('PFKEY01')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="<%=resourceBundleHandler.gettingValueFromBundle("Help")%>"><i class="fa fa-question-circle"></i></a></li>
		        <li><a href="javascript:;" onClick="doAction('PFKEY16')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="<%=resourceBundleHandler.gettingValueFromBundle("Home")%>"><i class="fa fa-home"></i></a></li>
		        <li><a href="javascript:;" onClick="kill('ENG');return false;" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="<%=resourceBundleHandler.gettingValueFromBundle("Logout")%>"><i class="fa fa-sign-out"></i></a></li>
			 <%}else if(av.getUserLanguage().toString().equalsIgnoreCase("CHI")){ %>
				<li><a href="javascript:;" onClick="doAction('PFKEY02')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="会话信息>"><i class="fa fa-info-circle"></i></a></li>
		        <li><a href="javascript:;" onClick="doAction('PFKEY01')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="帮助"><i class="fa fa-question-circle"></i></a></li>
		        <li><a href="javascript:;" onClick="doAction('PFKEY16')" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="首页"><i class="fa fa-home"></i></a></li>
		        <li><a href="javascript:;" onClick="kill('CHI');return false;" style="color: #fff;font-size:20px;" data-toggle="tooltip" data-placement="bottom" title="注销"><i class="fa fa-sign-out"></i></a></li>
				<%} %>
			</ul>
	    </nav>
			
		<div class="container">
			<%if(!fw.getScreenName().equals("S0017")){%>
			<div class="row">
				<div class="col-lg-10">
					<h1 class="page-header"><%=tit%></h1>
				</div>				        
			</div>
			<%}%>
			<div class ="row">			    	
				<div class="col-lg-12 text-right">
					<%if(!fw.getScreenName().equals("S0017")){%>
					    <%if(fw.getScreenName().equals("Msgbox")){%>
						   <%=getBottomButtons(null)%>
						<%} else {%>
						   <%=getBottomButtons(new String[]{"PFKEY03 ", "PFKEY05 ", "PFKEY12 "})%>
						<%} %>
					<%}%>
				</div>	
			</div>  
			

 