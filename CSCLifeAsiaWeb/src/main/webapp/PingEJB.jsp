<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Timings</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.quipoz.framework.sessionfacade.*" %>

<%
    long t0 = System.currentTimeMillis();
	GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
	BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
	String actionClass = StatefulSessionFacadeBean.TEST_ACTION;
	gd.processAction(bm, actionClass);
	long t1 = System.currentTimeMillis();
	SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss.SSS");
	String jspstart = sdf.format(new Date(t0));
	String jspend = sdf.format(new Date(t1));
	
%>

</head>
<BODY class="main">
<script src="js/Sidebar.js"></script>
<h2>Network response + EJB estimate</h2>
<p>This screen submits a trivial request to the Application Server,
and executes a simple bit of EJB code.
<p>This test can be used to see if response time issues are due to the
Application Server.
<p>To just see the network response, use Ping.
<p>
<Button onClick='doAgain()'>Repeat</Button>
<br><Button onClick="dontDoAgain()">Close this window</Button>
<Script>
  	returnValue = "done";
    sT = window.dialogArguments;
    sT1 = new Date(sT.getYear(), sT.getMonth(), sT.getDate(), sT.getHours(), sT.getMinutes(), sT.getSeconds());
    sTime = fm2(sT.getHours()) + ":" + fm2(sT.getMinutes()) + ":" + fm2(sT.getSeconds()) + "." + fm3(sT.getTime() - sT1.getTime()); 

    eT = new Date();
    eT1 = new Date(eT.getYear(), eT.getMonth(), eT.getDate(), eT.getHours(), eT.getMinutes(), eT.getSeconds());
    eTime = fm2(eT.getHours()) + ":" + fm2(eT.getMinutes()) + ":" + fm2(eT.getSeconds()) + "." + fm3(eT.getTime() - eT1.getTime()); 

	document.write("<Table border=1>");
	document.write("<tr><td>Request  time:</td><td>" + sTime + "</td></tr>");
	document.write("<tr><td>Response received</td><td>" + eTime + "</td></tr>");
	document.write("<tr><td>Web/Response time</td><td>" + ((eT.getTime() - sT.getTime())/1000) + "</td></tr>");
	document.write("<tr><td colspan=2>EJB components:</td></tr>");
	document.write("<tr><td>EJB start time</td><td><%=jspstart%></td></tr>");
	document.write("<tr><td>EJB end time</td><td><%=jspend%></td></tr>");
	document.write("<tr><td>EJB time</td><td><%=(t1-t0)/1000.0%></td></tr>");
	document.write("</Table>");
	
  function fm2(num) {
    if (num < 10) return "0" + num;
    return num;
  }
  
  function fm3(num) {
    if (num < 10) return "00" + num;
    if (num < 100) return "0" + num;
    return num;
  }
  
  function dontDoAgain() {
  	returnValue = "done";
  	window.close();
  }
  
  function doAgain() {
  	returnValue = "redo";
  	window.close();
  }
</Script>
</BODY>
</HTML>