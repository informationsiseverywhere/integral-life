<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Checking browser ...</title>
		<%--  ILIFE-5225 APPSec Dynamic scan issues fixes --%>
		<%response.addHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");%>
		<script type="text/javascript">
		/*Ticket ILIFE-1328 yzhong4 start*/
			if (navigator.userAgent.search("MSIE") >= 0){
		    	var position = navigator.userAgent.search("MSIE") + 5;
		    	var end = navigator.userAgent.search("; Windows");
		    	var version = navigator.userAgent.substring(position,end);
		    	
		    	 if (document.compatMode == "CSS1Compat" && document.documentMode == 7) {
		    		alert("Please close the IE compatible view");
		    	} else {
		    		//ILIFE-1807 add IE10
		    		if(version == "10.0" || version == "8.0" || (version == "7.0" && navigator.userAgent.search("Trident/4.0") >= 0)){
		    			openFirstServlet();
		    		} else{
		    			document.title = "Invalid Browser";
		    			alert("Unsupported version!!!");
		    		}
		    	}
		    }else if(navigator.userAgent.indexOf("Firefox")>0){
			openFirstServlet();
		    }//else if(window.MessageEvent && !document.getBoxObjectFor && navigator.userAgent.indexOf("Chrome")>0 && navigator.userAgent.toLowerCase().match(/chrome\/([\d.]+)/)[1])
		    else if(navigator.userAgent.indexOf("Chrome")>0){
			openFirstServlet();
		    }else if(navigator.userAgent.indexOf("Trident/7.0")>0 /*&& !(window.ActiveXObject)*/){
			openFirstServlet();
		    }
			else{
				document.title = "Invalid Browser";
				alert("Unsupported browser!!!");
			}
		
			function openFirstServlet(){
				document.title = "Opening ... Please wait!";
		    	document.cookie = "isValidBrowser=true";
		    	<%-- AWPL-Integral Integration is currently not used 
		    	     so commented the below AWPL code as there are security issues reported on queryString
		    	<%
		    		//AWPL-Integral Integration related code changes. Get the query string and attached it in URL.
		    		//If query string contains the ticket information then, CAS Authentication will throw error. 
		    		//So renamed ticket property name as ticketNo. 
		    		String queryString = request.getQueryString()==null?"":"?"+request.getQueryString();
		    		if(queryString.indexOf("ticket") != -1) {
		    			queryString = queryString.replace("ticket","ticketNo");
		    		}
		    	%>
		    	window.location.assign("FirstServlet.jsp<%=queryString%>");
		    	--%>
		    	window.location.assign("FirstServlet.jsp");
			}
			/*Ticket ILIFE-1328 yzhong4 end*/
		</script>
	</head>
	<body>
		<p><b>Invalid browser!!!</b></p>
	</body>
</html>