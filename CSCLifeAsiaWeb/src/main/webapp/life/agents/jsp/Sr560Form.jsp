<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR560";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sr560ScreenVars sv = (Sr560ScreenVars) fw.getVariables();
%>

<%
	{

		if (appVars.ind21.isOn()) {
			sv.dteapp01Disp.setReverse(BaseScreenData.REVERSED);
			sv.dteapp01Disp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.dteapp01Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind21.isOn()) {
			sv.dteapp01Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.zrreptp.setReverse(BaseScreenData.REVERSED);
			sv.zrreptp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.zrreptp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind22.isOn()) {
			sv.zrreptp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.agtype01.setReverse(BaseScreenData.REVERSED);
			sv.agtype01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.agtype01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind23.isOn()) {
			sv.agtype01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.dteapp02Disp.setReverse(BaseScreenData.REVERSED);
			sv.dteapp02Disp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.dteapp02Disp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind24.isOn()) {
			sv.dteapp02Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.agtype02.setReverse(BaseScreenData.REVERSED);
			sv.agtype02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.agtype02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind25.isOn()) {
			sv.agtype02.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client       ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent Number ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent Type   ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Basic");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Report To");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Report To");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Effective");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Effective");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	appVars.rollup(new int[]{93});
%>


<%
	GeneralTable sfl = fw.getTable("sr560screensfl");
%>

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					<table><tr><td>
						<%
							if (!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntsel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntsel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr></table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>

					<%
						if (!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
					<table><tr><td>
						<%
							if (!((sv.agntype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agtydesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agtydesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 80px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>

		</div>
		<br />
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
					
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr560' width='100%'>
							<thead>
								<tr class='info'>
									<th style="min-width: 200px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Basic Agent Type")%></th>
									<th style="min-width: 150px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Report to Agent")%></th>
									<th style="min-width: 200px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Report To Type")%></th>
									<th style="min-width: 150px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Effective From")%></th>
									<th style="min-width: 150px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Effective to")%></th>
									<th style="min-width: 150px;text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
								</tr>
							</thead>
							<tbody>
								<%
									Sr560screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									Map<String, Map<String, String>> agType01Map = appVars.loadF4FieldsLong(new String[]{"agtype01"}, sv, "E",
											baseModel);
									Map<String, Map<String, String>> agType02Map = appVars.loadF4FieldsLong(new String[]{"agtype02"}, sv, "E",
											baseModel);

									while (Sr560screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr id='<%="tablerow" + count%>'>
									<td>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[]{"agtype01"}, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("agtype01");
												optionValue = makeDropDownList(mappedItems, sv.agtype01, 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.agtype01.getFormData()).toString().trim());
										%> <%=smartHF.getDropDownExt(sv.agtype01, fw, longValue, "agtype01", optionValue, 0, 255)%>
									</td>
									<td>
										<div class="input-group">
											<input
												name='<%="sr560screensfl" + "." + "zrreptp" + "_R" + count%>'
												id='<%="sr560screensfl" + "." + "zrreptp" + "_R" + count%>'
												type='text' value='<%=sv.zrreptp.getFormData()%>'
												<%if (((new Byte((sv.zrreptp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0)
						|| (sv.isScreenProtected())) {%>
												readonly="true" class="output_cell">
											<%
												} else {
											%>


											class = "
											<%=(sv.zrreptp).getColor() == null
							? "input_cell"
							: (sv.zrreptp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
											maxLength='<%=sv.zrreptp.getLength()%>'
											onFocus='doFocus(this)' onHelp='return
											fieldHelp(sr560screensfl.zrreptp)' onKeyUp='return
											checkMaxLength(this)' > </input> <span class="input-group-btn">
												<button class="btn btn-info" style="font-size: 19px;"
													type="button"
													onClick="doFocus(document.getElementById('<%="sr560screensfl" + "." + "zrreptp" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
											<%
												}
											%>
										</div>
									</td>
									<td>
										<%
											mappedItems = (Map) agType02Map.get("agtype02");
												optionValue = makeDropDownList(mappedItems, sv.agtype02, 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.agtype02.getFormData()).toString().trim());
										%> <%=smartHF.getDropDownExt(sv.agtype02, fw, longValue, "agtype02", optionValue, 0, 255)%>
									</td>
									<td align="left">
										<div class="form-group">
											<%
												if ((new Byte((sv.dteapp01Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| fw.getVariables().isScreenProtected()) {
											%>
											<input name='dteapp01Disp' type='text'
												value='<%=sv.dteapp01Disp.getFormData()%>'
												maxLength='<%=sv.dteapp01Disp.getLength()%>'
												size='<%=sv.dteapp01Disp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(dteapp01Disp)'
												onKeyUp='return checkMaxLength(this)' style="width: 100px;"
												readonly="true" class="output_cell">
											<%
												} else if ((new Byte((sv.dteapp01Disp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
											%>
											class="bold_cell" >
											<div class="input-group date form_date col-md-12"
												style="min-width: 150px;" data-date=""
												data-date-format="dd/MM/yyyy" data-link-field="dteapp01Disp"
												data-link-format="dd/mm/yyyy">
												<%=smartHF.getRichTextDateInput(fw, sv.dteapp01Disp, (sv.dteapp01Disp.getLength()))%>
												<span class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<%
												} else {
											%>
											<div class="input-group date form_date col-md-12"
												style="min-width: 150px;" data-date=""
												data-date-format="dd/MM/yyyy" data-link-field="dteapp01Disp"
												data-link-format="dd/mm/yyyy">
												<%=smartHF.getRichTextDateInput(fw, sv.dteapp01Disp, (sv.dteapp01Disp.getLength()))%>
												<span class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<%
												}
											%>
										</div>
									</td>
									<td align="left">
										<div class="form-group">
											<%
												if ((new Byte((sv.dteapp02Disp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| fw.getVariables().isScreenProtected()) {
											%>
											<input name='dteapp02Disp' type='text'
												value='<%=sv.dteapp02Disp.getFormData()%>'
												maxLength='<%=sv.dteapp02Disp.getLength()%>'
												size='<%=sv.dteapp02Disp.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(dteapp02Disp)'
												onKeyUp='return checkMaxLength(this)' style="width: 100px;"
												readonly="true" class="output_cell">
											<%
												} else if ((new Byte((sv.dteapp02Disp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
											%>
											class="bold_cell" >
											<div class="input-group date form_date col-md-12"
												style="min-width: 150px;" data-date=""
												data-date-format="dd/MM/yyyy" data-link-field="dteapp02Disp"
												data-link-format="dd/mm/yyyy">
												<%=smartHF.getRichTextDateInput(fw, sv.dteapp02Disp, (sv.dteapp02Disp.getLength()))%>
												<span class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<%
												} else {
											%>
											<div class="input-group date form_date col-md-12"
												style="min-width: 150px;" data-date=""
												data-date-format="dd/MM/yyyy" data-link-field="dteapp02Disp"
												data-link-format="dd/mm/yyyy">
												<%=smartHF.getRichTextDateInput(fw, sv.dteapp02Disp, (sv.dteapp02Disp.getLength()))%>
												<span class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<%
												}
											%>
										</div>
									</td>
									<td><%=sv.desc.getFormData()%></td>
								</tr>
								<%
									count = count + 1;
										Sr560screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-sr560').DataTable({
			ordering : false,
			searching : false,
			scrollY : "380px",
			scrollCollapse : true,
			scrollX : true,
			paging:false,
			info:false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
