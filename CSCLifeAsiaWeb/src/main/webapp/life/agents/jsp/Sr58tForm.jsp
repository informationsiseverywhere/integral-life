<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR58T";%>
<%@ include file="/POLACommon3.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr58tScreenVars sv = (Sr58tScreenVars) fw.getVariables();%>
<%{
}%>

<div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.company,(sv.company.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.tabl,( sv.tabl.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.item,( sv.item.getLength()+1),null).replace("absolute","relative")%>
<%=smartHF.getRichText(0,0,fw,sv.longdesc,( sv.longdesc.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%StringData MONTHS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Persititency For (Months)");%>
<%=smartHF.getLit(0, 0, MONTHS_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.months,( sv.months.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData EFFPERD_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"System Launch Period (MMYYYY)");%>
<%=smartHF.getLit(0, 0, EFFPERD_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.effperd,( sv.effperd.getLength()+1),null).replace("absolute","relative")%>
</td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData Sr58t_01_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Type To Be Excluded");%>
<%=smartHF.getLit(0, 0, Sr58t_01_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.chdrtype01,(sv.chdrtype01.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype01).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype02,(sv.chdrtype02.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype02).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype03,(sv.chdrtype03.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype03).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype04,(sv.chdrtype04.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype04).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype05,(sv.chdrtype05.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype05).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype06,(sv.chdrtype06.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype06).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype07,(sv.chdrtype07.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype07).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype08,(sv.chdrtype08.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype08).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype09,(sv.chdrtype09.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype09).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype10,(sv.chdrtype10.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype10).replace("absolute","relative")%>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.chdrtype11,(sv.chdrtype11.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype11).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype12,(sv.chdrtype12.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype12).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype13,(sv.chdrtype13.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype13).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype14,(sv.chdrtype14.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype14).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype15,(sv.chdrtype15.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype15).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype16,(sv.chdrtype16.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype16).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype17,(sv.chdrtype17.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype17).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype18,(sv.chdrtype18.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype18).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype19,(sv.chdrtype19.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype19).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype20,(sv.chdrtype20.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype20).replace("absolute","relative")%>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.chdrtype21,(sv.chdrtype21.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype21).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype22,(sv.chdrtype22.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype22).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype23,(sv.chdrtype23.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype23).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype24,(sv.chdrtype24.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype24).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype25,(sv.chdrtype25.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype25).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype26,(sv.chdrtype26.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype26).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype27,(sv.chdrtype27.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype27).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype28,(sv.chdrtype28.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype28).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype29,(sv.chdrtype29.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype29).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.chdrtype30,(sv.chdrtype30.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.chdrtype30).replace("absolute","relative")%>
</td>
</tr> </table>
<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData Sr58t_02_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Regular Component Code To Be Excluded For Bonus:");%>
<%=smartHF.getLit(0, 0, Sr58t_02_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.crtable01,(sv.crtable01.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable01).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable02,(sv.crtable02.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable02).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable03,(sv.crtable03.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable03).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable04,(sv.crtable04.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable04).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable05,(sv.crtable05.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable05).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable06,(sv.crtable06.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable06).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable07,(sv.crtable07.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable07).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable08,(sv.crtable08.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable08).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable09,(sv.crtable09.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable09).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable10,(sv.crtable10.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable10).replace("absolute","relative")%>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.crtable11,(sv.crtable11.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable11).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable12,(sv.crtable12.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable12).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable13,(sv.crtable13.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable13).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable14,(sv.crtable14.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable14).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable15,(sv.crtable15.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable15).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable16,(sv.crtable16.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable16).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable17,(sv.crtable17.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable17).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable18,(sv.crtable18.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable18).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable19,(sv.crtable19.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable19).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable20,(sv.crtable20.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable20).replace("absolute","relative")%>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.crtable21,(sv.crtable21.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable21).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable22,(sv.crtable22.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable22).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable23,(sv.crtable23.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable23).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable24,(sv.crtable24.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable24).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable25,(sv.crtable25.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable25).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable26,(sv.crtable26.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable26).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable27,(sv.crtable27.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable27).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable28,(sv.crtable28.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable28).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable29,(sv.crtable29.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable29).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable30,(sv.crtable30.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable30).replace("absolute","relative")%>
</td>
</tr>
<tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.crtable31,(sv.crtable31.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable31).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable32,(sv.crtable32.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable32).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable33,(sv.crtable33.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable33).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable34,(sv.crtable34.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable34).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable35,(sv.crtable35.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable35).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable36,(sv.crtable36.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable36).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable37,(sv.crtable37.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable37).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable38,(sv.crtable38.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable38).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable39,(sv.crtable39.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable39).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable40,(sv.crtable40.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable40).replace("absolute","relative")%>
</td></tr><tr style='height:22px;'><td width='753'>
<%=smartHF.getRichText(0, 0, fw, sv.crtable41,(sv.crtable41.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable41).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable42,(sv.crtable42.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable42).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable43,(sv.crtable43.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable43).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable44,(sv.crtable44.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable44).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable45,(sv.crtable45.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable45).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable46,(sv.crtable46.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable46).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable47,(sv.crtable47.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable47).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable48,(sv.crtable48.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable48).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable49,(sv.crtable49.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable49).replace("absolute","relative")%>

<%=smartHF.getRichText(0, 0, fw, sv.crtable50,(sv.crtable50.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.crtable50).replace("absolute","relative")%>
</td>
</tr>

</table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_continue.png" width='84' height='22' alt='Continue'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_refresh.png" width='84' height='22' alt='Refresh'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_previous.png" width='84' height='22' alt='Previous'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_exit.png" width='84' height='22' alt='Exit'></a></li>
</ul>
</div>
<%@ include file="/POLACommon4.jsp"%>

