
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR52Q";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr52qScreenVars sv = (Sr52qScreenVars) fw.getVariables();%>





<div class="panel panel-default">
    
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>           </div>
				    </div>
			   
			   
			   	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
		
				      		</div>
				    	</div>
			    	
			    	
			    	
			    	  <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    		<div class="input-group">
					    		
		<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				      </div></div>
				    </div>
			       </div>
			       
			    <br>   
			       
<div class="row">
<div class="col-md-2">
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Delivery Mode  ");%>
</div>
</div>
<br><br>


<div class="row">
<div class="col-md-2">
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Acknowledgement Slip");%>
</div></div>


<div class="row">
<div class="col-md-2">
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Holding Period Processing");%>
</div>

<div class="col-md-2">
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lag Time");%>
<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(days)");%>
</div>


<div class="col-md-2">
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Letter");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
</div></div>


<div class="row">
<div class="col-md-2">
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reminder  ");%>
</div></div>

<div class="row">
<div class="col-md-2">
<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deemed Received  ");%>
</div></div>

<div class="row">
<div class="col-md-12">
<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Delivery Modes Excluded  ");%>
</div></div>


	<%
	{
			if (appVars.ind01.isOn()) {
				sv.dlvrmode01.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.dlvrmode01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.daexpy01.setReverse(BaseScreenData.REVERSED);
				sv.daexpy01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.daexpy01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.letterType01.setReverse(BaseScreenData.REVERSED);
				sv.letterType01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.letterType01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.daexpy02.setReverse(BaseScreenData.REVERSED);
				sv.daexpy02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.daexpy02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.letterType02.setReverse(BaseScreenData.REVERSED);
				sv.letterType02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.letterType02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.dlvrmode02.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.dlvrmode02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.dlvrmode03.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.dlvrmode03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.dlvrmode04.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.dlvrmode04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.dlvrmode05.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.dlvrmode05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.dlvrmode06.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.dlvrmode06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.dlvrmode07.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.dlvrmode07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.dlvrmode08.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.dlvrmode08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.dlvrmode09.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.dlvrmode09.setHighLight(BaseScreenData.BOLD);
			}  %>
			
		<%} %>
		
<div class="row">
<div class="col-md-4">
<label><%=generatedText5%></label>
</div>

<div class="col-md-4">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode01)%>

</div>
</div>
&nbsp;
<div class="row">
<div class="col-md-4">
<label><%=generatedText6%></label>
</div>
</div>





<div class="row">
  <div class="col-md-4">
       <label><%=generatedText9%></label>
           <div class="row">
               <div class="col-md-4">
                  <label><%=generatedText12%></label>
               </div>
           </div>
           <div class="row">
               <div class="col-md-6">
                  <label><%=generatedText13%></label>
              </div>
           </div>
   </div>


<div class="col-md-4">
<label><%=generatedText7%></label>
<label><%=generatedText10%></label>
<div class="list-group">
<%if(((BaseScreenData)sv.daexpy01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.daexpy01,( sv.daexpy01.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.daexpy01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.daexpy01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
<%if(((BaseScreenData)sv.daexpy02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.daexpy02,( sv.daexpy02.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.daexpy02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.daexpy02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
</div>
</div>

<div class="col-md-4">
<label><%=generatedText8%></label>
<label><%=generatedText11%></label>
<div class="list-group">
<%=smartHF.getHTMLVarExt(fw, sv.letterType01)%>
<%=smartHF.getHTMLVarExt(fw, sv.letterType02)%>
</div>
</div>
</div>



<div class="row">
<div class="col-md-12"> 
<div class="form-group">
<label><%=generatedText14%></label>
<div class="list-group" >
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode02)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode03)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode04)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode05)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode06)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode07)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode08)%>
</div>
<div class="col-md-2" style="max-width: 100px;">
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode09)%>
</div>
<%-- <%=smartHF.getHTMLVarExt(fw, sv.dlvrmode03)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode04)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode05)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode06)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode07)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode08)%>
<%=smartHF.getHTMLVarExt(fw, sv.dlvrmode09)%> --%>
        <%--     <%sv.dlvrmode02.setClassString("");%>
			<%sv.dlvrmode03.setClassString("");%>
			<%sv.dlvrmode04.setClassString("");%>
			<%sv.dlvrmode05.setClassString("");%>
			<%sv.dlvrmode06.setClassString("");%>
			<%sv.dlvrmode07.setClassString("");%>
			<%sv.dlvrmode08.setClassString("");%>
			<%sv.dlvrmode09.setClassString("");%> --%>

</div></div></div></div>
			       
	<%if (sv.Sr52qprotectWritten.gt(0)) {%>
	<%Sr52qprotect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
			       
			       
			       </div> </div>  




<%-- <div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<div class="label_txt">
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
</div>
<br/>
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<div class="label_txt">
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
</div>
<br/>
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<div class="label_txt">
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
</div>
<br/>
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
</table>
</div>

<%if (sv.Sr52qscreenWritten.gt(0)) {%>
	<%Sr52qscreen.clearClassString(sv);%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Delivery Mode  ");%>
	<%sv.dlvrmode01.setClassString("");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Acknowledgement Slip");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lag Time");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Letter");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Holding Period Processing");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(days)");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reminder  ");%>
	<%sv.daexpy01.setClassString("");%>
	<%sv.letterType01.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deemed Received  ");%>
	<%sv.daexpy02.setClassString("");%>
	<%sv.letterType02.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Delivery Modes Excluded  ");%>
	<%sv.dlvrmode02.setClassString("");%>
	<%sv.dlvrmode03.setClassString("");%>
	<%sv.dlvrmode04.setClassString("");%>
	<%sv.dlvrmode05.setClassString("");%>
	<%sv.dlvrmode06.setClassString("");%>
	<%sv.dlvrmode07.setClassString("");%>
	<%sv.dlvrmode08.setClassString("");%>
	<%sv.dlvrmode09.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

		<%
	{
			if (appVars.ind01.isOn()) {
				sv.dlvrmode01.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.dlvrmode01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.daexpy01.setReverse(BaseScreenData.REVERSED);
				sv.daexpy01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.daexpy01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.letterType01.setReverse(BaseScreenData.REVERSED);
				sv.letterType01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.letterType01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.daexpy02.setReverse(BaseScreenData.REVERSED);
				sv.daexpy02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.daexpy02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.letterType02.setReverse(BaseScreenData.REVERSED);
				sv.letterType02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.letterType02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.dlvrmode02.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.dlvrmode02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.dlvrmode03.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.dlvrmode03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.dlvrmode04.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.dlvrmode04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.dlvrmode05.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.dlvrmode05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.dlvrmode06.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.dlvrmode06.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.dlvrmode07.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode07.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.dlvrmode07.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.dlvrmode08.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode08.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.dlvrmode08.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind13.isOn()) {
				sv.dlvrmode09.setReverse(BaseScreenData.REVERSED);
				sv.dlvrmode09.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind13.isOn()) {
				sv.dlvrmode09.setHighLight(BaseScreenData.BOLD);
			}
		}

		%>

	<%=smartHF.getLit(6, 12, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(6, 34.2, fw, sv.dlvrmode01).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(5.9, 34.2, fw, sv.dlvrmode01)%>

	<%=smartHF.getLit(8, 12, generatedText6)%>

	<%=smartHF.getLit(9, 52, generatedText7)%>

	<%=smartHF.getLit(9, 64.9, generatedText8)%>

	<%=smartHF.getLit(9, 12, generatedText9)%>

	<%=smartHF.getLit(9, 59, generatedText10)%>

	<%=smartHF.getLit(9, 69.7, generatedText11)%>

	<%=smartHF.getLit(10, 23, generatedText12)%>

	<%=smartHF.getHTMLVar(10, 52, fw, sv.daexpy01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLSpaceVar(10, 65, fw, sv.letterType01)%>
	<%=smartHF.getHTMLF4NSVar(9.9, 64, fw, sv.letterType01)%>

	<%=smartHF.getLit(11, 23, generatedText13)%>

	<%=smartHF.getHTMLVar(11, 52, fw, sv.daexpy02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLSpaceVar(11, 65, fw, sv.letterType02)%>
	<%=smartHF.getHTMLF4NSVar(10.9, 64, fw, sv.letterType02)%>

	<%=smartHF.getLit(13, 12, generatedText14)%>
	
	<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 Start-->	

	<%=smartHF.getHTMLSpaceVar(14, 23, fw, sv.dlvrmode02).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 23, fw, sv.dlvrmode02)%>

	<%=smartHF.getHTMLSpaceVar(14, 31, fw, sv.dlvrmode03).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 31, fw, sv.dlvrmode03)%>

	<%=smartHF.getHTMLSpaceVar(14, 39, fw, sv.dlvrmode04).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 39, fw, sv.dlvrmode04)%>

	<%=smartHF.getHTMLSpaceVar(14, 47, fw, sv.dlvrmode05).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 47, fw, sv.dlvrmode05)%>

	<%=smartHF.getHTMLSpaceVar(14, 55, fw, sv.dlvrmode06).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 55, fw, sv.dlvrmode06)%>

	<%=smartHF.getHTMLSpaceVar(14, 63, fw, sv.dlvrmode07).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 63, fw, sv.dlvrmode07)%>

	<%=smartHF.getHTMLSpaceVar(14, 71, fw, sv.dlvrmode08).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 71, fw, sv.dlvrmode08)%>

	<%=smartHF.getHTMLSpaceVar(14, 79, fw, sv.dlvrmode09).replace("width:32px","width:40px")%>
	<%=smartHF.getHTMLF4NSVar(13.9, 79, fw, sv.dlvrmode09)%>

<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 ends-->	


<%}%>

<%if (sv.Sr52qprotectWritten.gt(0)) {%>
	<%Sr52qprotect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 Start-->	 --%>

<%@ include file="/POLACommon2NEW.jsp"%>

<style>
div[id*='dlvrmode']{padding-right:2px !important}

</style>

<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 ends-->	