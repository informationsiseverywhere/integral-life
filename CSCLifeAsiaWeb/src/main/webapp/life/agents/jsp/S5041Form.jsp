
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5041";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5041ScreenVars sv = (S5041ScreenVars) fw.getVariables();%>

	 <%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Change Agent on Contract");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%> 

<%{
		if (appVars.ind31.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	<div class="panel panel-default">
    	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group" style="min-width:110px">
						    		<input name='chdrsel' id='chdrsel'
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
<class="bold_cell" >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<%
	}else { 
%>

<class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>
 --%>
<%} %>


						    		

				      			</div>
				    		</div>
			    	</div>
			       
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
    	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-12">
					<label class="radio-inline">
						<%= smartHF.buildRadioOption(sv.action, "action", "A")%><b><%=resourceBundleHandler.gettingValueFromBundle("Change Agent on Contract")%></b>
					</label>
					<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
				</div>
				        
			</div>											
		</div>
	</div>	

	

<!-- 
<div class='outerDiv'>
<br/><br/><br/><br/>
<fieldset class="fieldsetUIG">
<legend class="legendUIG"> -->
<%--  <%=resourceBundleHandler.gettingValueFromBundle("Input")%>
</legend>
<table width='100%' align ='center' class ='tableFormat' cellspacing="5px">

<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</div>



<br/>
 
<input name='chdrsel' 
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>


</td>
</tr></table></fieldset><br/><br/><br/><br/><br/>
<fieldset class="fieldsetUIG">
<legend class="legendUIG">
<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
</legend>
<table width='100%' align ='center' class ='tableFormat' cellspacing="5px">


<tr style='height:22px;'><td>

<div class="radioButtonSubmenuUIG">

<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Change Agent on Contract")%></label> 
</div>

</td>


<td>

<input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
</td>
</tr></table></fieldset><br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>

