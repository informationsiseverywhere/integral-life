<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5690";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%@page import="com.csc.smart400framework.utility.Validator"%>
<%--  <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>--%>




<%S5690ScreenVars sv = (S5690ScreenVars) fw.getVariables();
//fix bug 666 Tom Chi
    HttpSession session = request.getSession();
    boolean checkmeBoolean = true;
    session.setAttribute("checkmeBoolean",new Boolean(checkmeBoolean));    
    
    String bankreq = "";

    //get original value
    if(session.getAttribute("bankreq") != null){    
    	bankreq = (String)session.getAttribute("bankreq");
    }
    String paymentMethod = "";
    if(session.getAttribute("paymentMethod") != null){
    	paymentMethod = (String)session.getAttribute("paymentMethod");
    }
    //get new value
    if(appVars.ind01.isOn()){//error    	    	
    	bankreq = (String)session.getAttribute("bankreq");
    	session.setAttribute("bankreq",sv.bankreq.getData().toString().trim());////
    }else{//ok, should initialise
        session.setAttribute("bankreq",sv.bankreq.getData().toString().trim());
    }
    if(appVars.ind02.isOn()){//error    	    	
    	paymentMethod = (String)session.getAttribute("paymentMethod");    
    	session.setAttribute("paymentMethod",sv.paymentMethod.getData().toString().trim());////
    }else{//ok, should initialise
        session.setAttribute("paymentMethod",sv.paymentMethod.getData().toString().trim());      
    }    
//end    
%>
<%if (sv.S5690screenWritten.gt(0)) {%>
	<%S5690screen.clearClassString(sv);%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Details Required ");%>
	<%sv.bankreq.setClassString("");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay Method ");%>
	<%sv.paymentMethod.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.bankreq.setReverse(BaseScreenData.REVERSED);
			sv.bankreq.setColor(BaseScreenData.RED);
			//bug 666 Tom Chi
			//sv.bankreq.Check("MANDATORY_ENTER");
			checkmeBoolean = Validator.check(bankreq,sv.bankreq);
			if(checkmeBoolean == false){
				session.setAttribute("checkmeBoolean",new Boolean(checkmeBoolean));
			}
			//end
		}
		if (!appVars.ind01.isOn()) {
			sv.bankreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.paymentMethod.setReverse(BaseScreenData.REVERSED);
			sv.paymentMethod.setColor(BaseScreenData.RED);
			//bug 666 Tom Chi
			checkmeBoolean = Validator.check(paymentMethod,sv.paymentMethod);
			if(checkmeBoolean == false){
				session.setAttribute("checkmeBoolean",new Boolean(checkmeBoolean));
			}			
			//sv.paymentMethod.Check("MANDATORY_ENTER);
			//end
		}
		if (!appVars.ind02.isOn()) {
			sv.paymentMethod.setHighLight(BaseScreenData.BOLD);
		}
	}
	}

	%>

<div class="panel panel-default">

<div class="panel-body">
	<div class="row">
	<div class="col-md-2">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-2">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		<div class="input-group">
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-3">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Details Required")%></label>
	<div class="input-group"><input name='bankreq' 
type='text' 

<%

		formatValue = (sv.bankreq.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bankreq.getLength()%>'
maxLength='<%= sv.bankreq.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bankreq)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bankreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}
%>
/>
</div>
	</div></div>

<div class="col-md-1"></div>
<div class="col-md-6">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Method")%></label>
<div class="input-group"><%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paymentMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paymentMethod");
	optionValue = makeDropDownList( mappedItems ,sv.paymentMethod,2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.paymentMethod).toString().trim());  
%>

<% 
	if((new Byte((sv.paymentMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.paymentMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:152px;"> 
<%
} 
%>

<select name='paymentMethod' type='list' style="min-width:150px;"
<% 
	if((new Byte((sv.paymentMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.paymentMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'blank_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.paymentMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>

</div></div>
</div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

