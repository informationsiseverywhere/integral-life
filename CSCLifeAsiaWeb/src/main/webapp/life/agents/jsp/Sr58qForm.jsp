<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR58Q";%>
<%@ include file="/POLACommon3.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%Sr58qScreenVars sv = (Sr58qScreenVars) fw.getVariables();%>
<%{
}%>
              
<div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.company,(sv.company.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.tabl,( sv.tabl.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.item,( sv.item.getLength()+1),null).replace("absolute","relative")%>
<%=smartHF.getRichText(0,0,fw,sv.longdesc,( sv.longdesc.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
&nbsp;
<%StringData ITMTO_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>

<%=smartHF.getLit(0, 0, ITMTO_LBL).replace("absolute;","relative; font-weight: bold;")%>
&nbsp;
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
</td>
</td>
<td></td>
<td></td>
</tr> </table>

<br/><br/>
<table width='100%'>




<tr style='height:22px;'>
<td width='251'>
<%StringData ANNTARPRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Yearly Target Collected FYP");%>
<%=smartHF.getLit(0, 0, ANNTARPRM_LBL).replace("absolute;","relative; font-weight: bold;")%>

<br/>
<%=smartHF.getRichText(0, 0, fw, sv.anntarprm,(sv.anntarprm.getLength()),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
</td>
<td width='251'>
<%StringData BLPREM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Monthly Target Collected FYP");%>
<%=smartHF.getLit(0, 0, BLPREM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.blprem,( sv.blprem.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>

<td width='251'>
<%StringData PREMST_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Monthly Target FYP");%>
<%=smartHF.getLit(0, 0, PREMST_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.premst,( sv.premst.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>

</td>
</tr> 

<tr style='height:22px;'>

<td width='251'>

<%StringData ZLOCAMT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Monthly Leader Bonus");%>
<%=smartHF.getLit(0, 0, ZLOCAMT_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.zlocamt,( sv.zlocamt.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'></td>
<td width='251'></td>
</tr>
</table>

<br/><br/>

<table>

<tr style='height:22px;'>

<td width='251'>
<%StringData ZNADJPERC01_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,">= PR criteria payment %");%>
<%=smartHF.getLit(0, 0, ZNADJPERC01_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.znadjperc01,( sv.znadjperc01.getLength()+1),null).replace("absolute","relative")%>

</td>

<td width='251'>
<%StringData ZNADJPERC02_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"< PR criteria payment %");%>
<%=smartHF.getLit(0, 0, ZNADJPERC02_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.znadjperc02,( sv.znadjperc02.getLength()+1),null).replace("absolute","relative")%>
</td>
<td width='251'></td>
</tr> 

</table>


<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_continue.png" width='84' height='22' alt='Continue'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_refresh.png" width='84' height='22' alt='Refresh'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_previous.png" width='84' height='22' alt='Previous'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_exit.png" width='84' height='22' alt='Exit'></a></li>
</ul>
</div>
<%@ include file="/POLACommon4.jsp"%>

