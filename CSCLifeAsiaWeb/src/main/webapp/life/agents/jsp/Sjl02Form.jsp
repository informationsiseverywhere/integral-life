<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl02";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>

<%
	Sjl02ScreenVars sv = (Sjl02ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind05.isOn()) {
			sv.qlfy01.setReverse(BaseScreenData.REVERSED);
			sv.qlfy01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.qlfy01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.qlfy01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.qlfy02.setReverse(BaseScreenData.REVERSED);
			sv.qlfy02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.qlfy02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.qlfy02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.qlfy03.setReverse(BaseScreenData.REVERSED);
			sv.qlfy03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind27.isOn()) {
			sv.qlfy03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.qlfy03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.qlfy04.setReverse(BaseScreenData.REVERSED);
			sv.qlfy04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind28.isOn()) {
			sv.qlfy04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.qlfy04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.qlfy05.setReverse(BaseScreenData.REVERSED);
			sv.qlfy05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind29.isOn()) {
			sv.qlfy05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.qlfy05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.qlfy06.setReverse(BaseScreenData.REVERSED);
			sv.qlfy06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.qlfy06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.qlfy06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.qlfy07.setReverse(BaseScreenData.REVERSED);
			sv.qlfy07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.qlfy07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.qlfy07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.qlfy08.setReverse(BaseScreenData.REVERSED);
			sv.qlfy08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.qlfy08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.qlfy08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.qlfy09.setReverse(BaseScreenData.REVERSED);
			sv.qlfy09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind33.isOn()) {
			sv.qlfy09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.qlfy09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.qlfy10.setReverse(BaseScreenData.REVERSED);
			sv.qlfy10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind34.isOn()) {
			sv.qlfy10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.qlfy10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
 									longValue = null;
 									formatValue = null;
								%>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
								 	longValue = null;
								 	formatValue = null;
								%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Qualifications")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Description")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy01");
							optionValue = makeDropDownList(mappedItems, sv.qlfy01.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy01.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy01' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy01).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc01' type='text'
							<%formatValue = (sv.qlfydesc01.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc01.getLength()%>'
							maxLength='<%=sv.qlfydesc01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc01)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy02" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy02");
							optionValue = makeDropDownList(mappedItems, sv.qlfy02.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy02.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy02.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy02' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy02).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc02' type='text'
							<%formatValue = (sv.qlfydesc02.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc02.getLength()%>'
							maxLength='<%=sv.qlfydesc02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc02)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy03" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy03");
							optionValue = makeDropDownList(mappedItems, sv.qlfy03.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy03.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy03.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy03).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy03' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy03).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc03' type='text'
							<%formatValue = (sv.qlfydesc03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc03.getLength()%>'
							maxLength='<%=sv.qlfydesc03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc03)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy04" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy04");
							optionValue = makeDropDownList(mappedItems, sv.qlfy04.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy04.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy04.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy04).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy04' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy04).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc04' type='text'
							<%formatValue = (sv.qlfydesc04.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc04.getLength()%>'
							maxLength='<%=sv.qlfydesc04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc04)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy05" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy05");
							optionValue = makeDropDownList(mappedItems, sv.qlfy05.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy05.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy05.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy05).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy05' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy05).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc05' type='text'
							<%formatValue = (sv.qlfydesc05.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc05.getLength()%>'
							maxLength='<%=sv.qlfydesc05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc05)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy06" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy06");
							optionValue = makeDropDownList(mappedItems, sv.qlfy06.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy06.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy06.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy06).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy06' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy06).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc06' type='text'
							<%formatValue = (sv.qlfydesc06.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc06.getLength()%>'
							maxLength='<%=sv.qlfydesc06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc06)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy07" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy07");
							optionValue = makeDropDownList(mappedItems, sv.qlfy07.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy07.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy07.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy07).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy07' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy07).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy07).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc07' type='text'
							<%formatValue = (sv.qlfydesc07.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc07.getLength()%>'
							maxLength='<%=sv.qlfydesc07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc07)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy08" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy08");
							optionValue = makeDropDownList(mappedItems, sv.qlfy08.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy08.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy08.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy08).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy08' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy08).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy08).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc08' type='text'
							<%formatValue = (sv.qlfydesc08.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc08.getLength()%>'
							maxLength='<%=sv.qlfydesc08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc08)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy09" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy09");
							optionValue = makeDropDownList(mappedItems, sv.qlfy09.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy09.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy09.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy09).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy09' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy09).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy09).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc09' type='text'
							<%formatValue = (sv.qlfydesc09.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc09.getLength()%>'
							maxLength='<%=sv.qlfydesc09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc09)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "qlfy10" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("qlfy10");
							optionValue = makeDropDownList(mappedItems, sv.qlfy10.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.qlfy10.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.qlfy10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.qlfy10.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.qlfy10).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='qlfy10' type='list' style="width: 170px;"
								<%if ((new Byte((sv.qlfy10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.qlfy10).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.qlfy10).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='qlfydesc10' type='text'
							<%formatValue = (sv.qlfydesc10.getFormData()).toString();%>
							value='<%=XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.qlfydesc10.getLength()%>'
							maxLength='<%=sv.qlfydesc10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(qlfydesc10)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%@ include file="/POLACommon2NEW.jsp"%>