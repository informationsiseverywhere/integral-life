S5039Form.jsp<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5039";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5039ScreenVars sv = (S5039ScreenVars) fw.getVariables();%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Override %");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		<table><tr><td>
						    	<%					
		if(!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td style="min-width:1px">
</td><td>


	
  		
		<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</td></tr></table>
				    		</div>
				       </div>
			 </div> 
				    
			 <div class="row">	
			    	<div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				        <div class="col-md-1"> </div>
				     <div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
					    		<table><tr><td>
						    		<%					
		if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td style="min-width:1px">
</td><td>


		<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table> 

				    		</div>
				       </div>
				       </div>
	<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">			       
  <table class="table table-striped table-bordered table-hover" id='dataTables-s5039' width="100%"> 
  <thead>
    <tr class="info">
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></center></th>
	    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Client")%></center></th>    								
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Agent Name")%></center></th>
		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Override%")%></center></th>
	</tr>
   </thead>

   <tbody>
      <% GeneralTable sfl = fw.getTable("s5039screensfl");%>
      <%
	S5039screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5039screensfl.hasMoreScreenRows(sfl)) {	
      %>
 <tr>
 
 
    <td><%= sv.reportag.getFormData()%></td> 
     <td><%= sv.clntnum.getFormData()%></td> 
    <td><%= sv.agentname.getFormData()%></td> 
      <td><%	
           	sm = sfl.getCurrentScreenRow();
			qpsf = sm.getFieldXMLDef((sv.ovcpc).getFieldName());						
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);				
		 %>
										
	    <%
			formatValue = smartHF.getPicFormatted(qpsf,sv.ovcpc);
			if(!sv.ovcpc.getFormData().toString().trim().equalsIgnoreCase("")) {
				formatValue = formatValue( formatValue );												 		
			}%>				
			<%= formatValue%>				
				<%

				longValue = null;
				formatValue = null;
				%></td>		
 </tr>	
 <%
	count = count + 1;
	S5039screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>			
</tbody>					   


 



</table>
</div>
</div>
</div>
</div>


<script>
$(document).ready(function() {
	$('#dataTables-s5039').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '450px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
