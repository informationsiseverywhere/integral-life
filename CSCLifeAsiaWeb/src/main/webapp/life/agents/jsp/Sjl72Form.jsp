<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL72";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl72ScreenVars sv = (Sjl72ScreenVars) fw.getVariables();%>


<%{
	 	if (appVars.ind01.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind03.isOn()) {
			sv.userid.setReverse(BaseScreenData.REVERSED);
			sv.userid.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.userid.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.userid.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind04.isOn()) {
			sv.agntbr.setReverse(BaseScreenData.REVERSED);
			sv.agntbr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.agntbr.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind37.isOn()) {
			sv.agntbr.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind91.isOn()) {
			sv.aracde.setReverse(BaseScreenData.REVERSED);
			sv.aracde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind91.isOn()) {
			sv.aracde.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind38.isOn()) {
			sv.aracde.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind30.isOn()) {
			sv.saledept.setReverse(BaseScreenData.REVERSED);
			sv.saledept.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.saledept.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.saledept.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.agtype.setReverse(BaseScreenData.REVERSED);
			sv.agtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.agtype.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind40.isOn()) {
			sv.agtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.reasonmod.setReverse(BaseScreenData.REVERSED);
			sv.reasonmod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.reasonmod.setHighLight(BaseScreenData.BOLD);
		}
		 if (appVars.ind17.isOn()) {
			sv.reasondel.setReverse(BaseScreenData.REVERSED);
			sv.reasondel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.reasondel.setHighLight(BaseScreenData.BOLD);
		} 
		
		if (appVars.ind18.isOn()) {
			sv.moddateDisp.setReverse(BaseScreenData.REVERSED);
			sv.moddateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.moddateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.deldateDisp.setReverse(BaseScreenData.REVERSED);
			sv.deldateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.deldateDisp.setHighLight(BaseScreenData.BOLD);
		}  
		if (appVars.ind20.isOn()) {
			sv.reasonmod.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind21.isOn()) {
			sv.reasonmod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind24.isOn()) {
			sv.reasondel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind25.isOn()) {
			sv.reasondel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind26.isOn()) {
			sv.moddate.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.moddate.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind28.isOn()) {
			sv.deldate.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.deldate.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 
	}%>

	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.clntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='clntsel' id='clntsel'
			type='text' 
			value='<%=sv.clntsel.getFormData()%>' 
			maxLength='<%=sv.clntsel.getLength()%>' 
			size='<%=sv.clntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(clntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.clntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.clntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.clntsel).getColor()== null  ? 
			"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 103px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
			
				<div class="col-md-2" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-4" style="padding-left:50px">
				<div >
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				 <table><tr><td class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.agntbr.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agntbr).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='agntbr' id='agntbr'
			type='text' 
			value='<%=sv.agntbr.getFormData()%>' 
			maxLength='<%=sv.agntbr.getLength()%>' 
			size='<%=sv.agntbr.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(agntbr)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.agntbr).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:50px" >
			
			<%
				}else if((new Byte((sv.agntbr).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:30px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.agntbr).getColor()== null  ? 
			"input_cell" :  (sv.agntbr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:30px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3" style="padding-left:0px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:24px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:50px" >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:34px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:34px" >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
		
			  <td >		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 142px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		

		</div>
		<div class="row" style="min-height:3px"></div>
				<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="min-width:198px"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sales Representative Number"))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			
				       
	 		 <div class="col-md-2">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.leveltyp.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.leveldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	
	       
							       
	 		 <div class="col-md-5" style="padding-left:50px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.saledept).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='saledept' id='saledept'
			type='text' 
			value='<%=sv.saledept.getFormData()%>' 
			maxLength='<%=sv.saledept.getLength()%>' 
			size='<%=sv.saledept.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(saledept)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.saledept).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.saledept).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.saledept).getColor()== null  ? 
			"input_cell" :  (sv.saledept).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>

			</div>
			<div class="row" style="min-height:3px"></div>
				       <div class="row">
	       
	       
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
						<div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"agtype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("agtype");
	optionValue = makeDropDownList( mappedItems , sv.agtype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.agtype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.agtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
	
 <% if("red".equals((sv.agtype).getColor())){
%>
<div style="border:1px; border-color: #B55050;  width:214px;"> 
<%
} 
%>

<select name='agtype' type='list' style="width:212px;"
<% 
	if((new Byte((sv.agtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){   
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.agtype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.agtype).getColor()== null  ? 
"input_cell" :  (sv.agtype).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.agtype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div></div>
			</div>
					<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 120px; max-width:140px">
					<input name='userid' type='text'
						<%formatValue = (sv.userid.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.userid.getLength()%>'
						maxLength='<%=sv.userid.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(userid)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.userid).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.userid).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.userid).getColor() == null
						? "input_cell"
						: (sv.userid).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div></div>
			</div>
			
					<div class="col-md-3" style="padding-left:50px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 120px; max-width:140px">

					<%	
					longValue = sv.salestatus.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				</div></div>
			</div></div>
			
			<div class="row" style="min-height:3px"></div>
						<div class="row">
			<%	 if (!(appVars.ind27.isOn())) { %>  
							 <div class="col-md-3" >
						<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Modify Date"))%></label>
			
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="moddateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.moddateDisp, (sv.moddateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
			
				</div>
			</div>
			<%}%>
			
			<%	 if (!(appVars.ind29.isOn())) { %>  
							 <div class="col-md-3" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Date of Deletion"))%></label>
			
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="deldateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.deldateDisp, (sv.deldateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
			
				</div>
			</div>
			<%}%>
			
			 <%	 if (!(appVars.ind21.isOn())) { %>  
			<div class="col-md-2 " >
        			<div class="form-group">
        			
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Modify"))%></label>
        				<div class="input-group" style="min-width:178px">
        					<% if ((new Byte((sv.reasonmod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasonmod"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasonmod");
								optionValue = makeDropDownList( mappedItems , sv.reasonmod.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasonmod.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasonmod, fw, longValue, "reasonmod", optionValue) %>
							<%}%>
							</div>
        			</div>
        		</div>
        		<%}%>
        		
        		 <%	 if (!(appVars.ind25.isOn())) { %>  
        		<div class="col-md-2">
        			<div class="form-group">
        			
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Deletion"))%></label>
        				<div class="input-group" style="min-width:178px">
        					<% if ((new Byte((sv.reasondel).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasondel"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasondel");
								optionValue = makeDropDownList( mappedItems , sv.reasondel.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasondel.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasondel, fw, longValue, "reasondel", optionValue) %>
							<%}%>
							</div>
        			</div>
        		</div>
        		<%}%>
			
			 <%	 if ((!appVars.ind21.isOn()) || (!appVars.ind25.isOn())){ %>		
						<div class="col-md-5" style="padding-left:50px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reason Details")%></label>
				<div class="form-group"  ">
					<div class="input-group" style="min-width: 450px; max-width:600px">
					<input name='resndesc' type='text'
						<%formatValue = (sv.resndesc.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.resndesc.getLength()%>'
						maxLength='<%=sv.resndesc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(resndesc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.resndesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.resndesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.resndesc).getColor() == null
						? "input_cell"
						: (sv.resndesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div></div>
			</div><%}%>
			</div>
			
			
			
	
			</div>
	</div>




<%@ include file="/POLACommon2NEW.jsp"%>