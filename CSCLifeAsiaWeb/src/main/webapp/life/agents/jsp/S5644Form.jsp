

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5644";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5644ScreenVars sv = (S5644ScreenVars) fw.getVariables();%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Subroutine  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversal Subroutine ");%>


<div class="panel panel-default">

<div class="panel-body">
	<div class="row">
	<div class="col-md-2" >
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-2">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	<div class="input-group">
	<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	</div>
	</div>

<div class="row">

	<div class="col-md-3" >
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Subroutine")%></label>
		<input name='compysubr' 
type='text'

<%

		formatValue = (sv.compysubr.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.compysubr.getLength()%>'
maxLength='<%= sv.compysubr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(compysubr)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.compysubr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
<%
	}
%>
/>
	</div>
	</div>
	
	<div class="col-md-1"></div>
	<div class="col-md-3" >
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Reversal Subroutine")%></label>
		<input name='subrev' 
type='text'

<%

		formatValue = (sv.subrev.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subrev.getLength()%>'
maxLength='<%= sv.subrev.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subrev)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.subrev).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"

<%
	}
%>
/>
		</div>
	</div>
<!-- IBPLIFE-5252 -->
<div class="col-md-1"></div>
	<div class="col-md-3" >
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Subroutine")%></label>
		<input name='rfdsbrtine' type='text'

<%

		formatValue = (sv.rfdsbrtine.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.rfdsbrtine.getLength()%>'
maxLength='<%= sv.rfdsbrtine.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(rfdsbrtine)' onKeyUp='return checkMaxLength(this)'  

	<% 
	if((new Byte((sv.rfdsbrtine).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>
	readonly="true"

<%
	}
%>
/>
		</div>
	</div>
<!-- IBPLIFE-5252 -->
</div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

