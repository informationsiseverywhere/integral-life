<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL57";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl57ScreenVars sv = (Sjl57ScreenVars) fw.getVariables();%>


	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
			  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 103px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
			
				<div class="col-md-2" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-4" style="padding-left:50px">
				<div >
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				 <table><tr><td class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.agntbr.getFormData();  
				%>
		 
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3" style="padding-left:0px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:24px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
			
				 </div> 		</td>
			 
		
			  <td >		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 142px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		

		</div>
		<div class="row" style="min-height:3px"></div>
				<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="min-width:198px"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sales Representative Number"))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			
				       
	 		 <div class="col-md-2">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.leveltyp.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.leveldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	
	       
							       
	 		 <div class="col-md-5" style="padding-left:50px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
			
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>

			</div>
			<br><br>
			
				<%
									GeneralTable sfl = fw.getTable("Sjl57screensfl");
									GeneralTable sfl1 = fw.getTable("Sjl57screensfl");
								%>
	<div class="row">
				<div class="col-md-12">
				<div class="form-group">

					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='dataTables-sjl57' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 50px"><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Agency Name")%></th>
									<th style="text-align: center; width: 157px"><%=resourceBundleHandler.gettingValueFromBundle("Registration Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Registration Class")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Start Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("End Date")%></th>
								</tr>
							</thead>
						
							<script language="javascript">
							        $(document).ready(function(){
							        	 $(document).ready(function(){
							     			var rows = <%=sfl1.count()+1%>;
							     			var isPageDown = 1;
							     			var pageSize = 1;
							     			var headerRowCount=1;
							     			var fields = new Array();
							     			fields[0] = "select";
								<%if (false) {%>	
								operateTableForSuperTable(rows,isPageDown,pageSize,fields,"sjl57Table",null,headerRowCount);
								<%}%>	
							
							        });
							    </script>
							<tbody>
								<%
									Sjl57screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (Sjl57screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>

								<tr id='tr<%=count%>' height="30">
											<td align="center" <%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="center" <%}%> >			 
					
						
				<%-- 	 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("Sjl57screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='Sjl57screensfl.select_R<%=count%>'
						 id='Sjl57screensfl.select_R<%=count%>'
						 onClick="selectedRow('Sjl57screensfl.select_R<%=count%>')"
						 class="UICheck"
						 />	
						  --%>
						 	 <input type="checkbox" 
								 value='<%= sv.select.getFormData() %>' 
								 onFocus='doFocus(this)' onHelp='return fieldHelp("Sjl57screensfl" + "." +
								 "select")' onKeyUp='return checkMaxLength(this)' 
								 name='Sjl57screensfl.select_R<%=count%>'
								 id='Sjl57screensfl.select_R<%=count%>'
								 onClick="selectedRow('Sjl57screensfl.select_R<%=count%>'); addSelectedCheckBox('Sjl57screensfl.select_R<%=count%>');"
								 class="UICheck"
									<%if(!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0") || (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
									checked="checked"
									<%}%>
							 	/>
		      	</td>
		      		<td style="text-align: left;">
										<%if((new Byte((sv.agncynum).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.agncynum.getFormData();
												%>
										<div id="Sjl57screensfl.agncynum_R<%=count%>"
											name="Sjl57screensfl.agncynum_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.cltname).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.cltname.getFormData();
												%>
										<div id="Sjl57screensfl.cltname_R<%=count%>"
											name="Sjl57screensfl.cltname_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
							
									<td style="text-align: left;">
										<%if((new Byte((sv.regnum).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.regnum.getFormData();
												%>
										<div id="Sjl57screensfl.regnum_R<%=count%>"
											name="Sjl57screensfl.regnum_R<%=count%>"
											<%if (!(((BaseScreenData) sv.regnum) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									
									<td style="text-align: left;">
										<%if((new Byte((sv.regclass).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.regclass.getFormData();
												%>
										<div id="Sjl74screensfl.regclass_R<%=count%>"
											name="Sjl74screensfl.regclass_R<%=count%>"
											<%if (!(((BaseScreenData) sv.regclass) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									
									<td style="text-align: left;">
										<%if((new Byte((sv.startDateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.startDateDisp.getFormData();
												%>
										<div id="Sjl57screensfl.startDateDisp_R<%=count%>"
											name="Sjl57screensfl.startDateDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.dateendDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.dateendDisp.getFormData();
												%>
										<div id="Sjl57screensfl.dateendDisp_R<%=count%>"
											name="Sjl57screensfl.dateendDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>

								</tr>

								<%
									count = count + 1;
									Sjl57screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	     <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	      <!-- <input type='text' style='display:none;' id='indxflgid' name='indxflg' onFocus='doFocus(this)' value='N' onKeyUp='return checkMaxLength(this)' /> -->
 		<!--  <input type="hidden" id="flagind" name="indxflg" value=""/> -->
<%-- 	     <table>
		<tr>		

		<td>
			<div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperationSelectedKillSession(1)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Approve")%></a>
			</div>
		</td>
		
		
		<td>
			<div class="sectionbutton"><a href="javascript:;" onClick='flag("1");' class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Approve")%></a>
				
			
			</div>
		</td>
		
		</tr> --%>
	</table>
	     
	</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl57Table').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
		language: {
			"lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
			"info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
			"sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
			"sEmptyTable": dtmessage,
			"paginate": {
				"next":       nextval,
				"previous":   previousval
			}
		},
		stateSave: true,
		"fnInfoCallback": function( settings, iStart, iEnd, iMax, iTotal, sPre ) {
			var iTotal = $('#totalRecords').val();
			return showval + "  " + iStart + "  " + toval + "  " + iEnd + "  " + ofval + "  " + iTotal + "  " + entriesval;
		},
  	});
	fixedColumns: true
});


function flag(flagind) {	


	 $("#flagind").val('1');
	doAction("PFKEY05");
}
</script>


<script type="text/javascript">

	var listSelected = [];
	var newListSelected = [];
	function addSelectedCheckBox(value){	
		var select = document.getElementById(value);
		if(select.checked){
			listSelected.push(select);
		}else{
				for( var i = 0 ; i < listSelected.length; i++){
				if(listSelected[i].id != select.id){
					newListSelected.push(listSelected[i]);
				}
			}	
			listSelected =  newListSelected;
		}
	}
	

	function checkKilledSessionCheckbox(){
		var allInputs = document.getElementsByTagName("input");
		for (var i = 0, max = allInputs.length; i < max; i++){
		     if (allInputs[i].type === 'checkbox'){
		     	if(allInputs[i].checked){
				    if((allInputs[i].id).indexOf("Sjl57screensfl") == 0) {
				    	  addSelectedCheckBox(allInputs[i].id);
				    } 
		     	}
		     }
	 	}
	}
	
	function perFormOperationSelectedKillSession(act){
		checkKilledSessionCheckbox();
		for(var i = 0; i < listSelected.length; i++){
			listSelected[i].value= act;		
		}
			doActionMultiSelect('PFKEY05');
	}
	function doActionMultiSelect(act) {	
		parent.frames[1].document.form1.action_key.value = act;
		parent.frames[1].doSubmitMultiSelect();
		pageloading();
	}

	function doSubmitMultiSelect(from) {	
	    aForm = document.form1;
	      for(var i = 0; i < listSelected.length; i++){	      
	      	aForm.focusInField.value = listSelected[i].name;	     
	    }
	      aForm.submit();   
	}
	
		

	</script>	


<%@ include file="/POLACommon2NEW.jsp"%>