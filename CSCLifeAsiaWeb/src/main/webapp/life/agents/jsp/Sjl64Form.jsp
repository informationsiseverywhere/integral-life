<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL64";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl64ScreenVars sv = (Sjl64ScreenVars) fw.getVariables();%>

<%{
	 if (appVars.ind01.isOn()) {
		sv.clntsel.setReverse(BaseScreenData.REVERSED);
		sv.clntsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.clntsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind02.isOn()) {
		sv.clntsel.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind03.isOn()) {
		sv.cltname.setReverse(BaseScreenData.REVERSED);
		sv.cltname.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.cltname.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.cltname.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind05.isOn()) {
		sv.company.setReverse(BaseScreenData.REVERSED);
		sv.company.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.company.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.company.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind07.isOn()) {
		sv.brnchcd.setReverse(BaseScreenData.REVERSED);
		sv.brnchcd.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.brnchcd.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind08.isOn()) {
		sv.brnchcd.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind09.isOn()) {
		sv.brnchdesc.setReverse(BaseScreenData.REVERSED);
		sv.brnchdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind09.isOn()) {
		sv.brnchdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind10.isOn()) {
		sv.brnchdesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind11.isOn()) {
		sv.aracde.setReverse(BaseScreenData.REVERSED);
		sv.aracde.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind11.isOn()) {
		sv.aracde.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind12.isOn()) {
		sv.aracde.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind13.isOn()) {
		sv.aradesc.setReverse(BaseScreenData.REVERSED);
		sv.aradesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind13.isOn()) {
		sv.aradesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind14.isOn()) {
		sv.aradesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind15.isOn()) {
		sv.levelno.setReverse(BaseScreenData.REVERSED);
		sv.levelno.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind15.isOn()) {
		sv.levelno.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind16.isOn()) {
		sv.levelno.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind17.isOn()) {
		sv.leveltype.setReverse(BaseScreenData.REVERSED);
		sv.leveltype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind17.isOn()) {
		sv.leveltype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind18.isOn()) {
		sv.leveltype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind19.isOn()) {
		sv.leveldes.setReverse(BaseScreenData.REVERSED);
		sv.leveldes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind19.isOn()) {
		sv.leveldes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind20.isOn()) {
		sv.leveldes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind21.isOn()) {
		sv.saledept.setReverse(BaseScreenData.REVERSED);
		sv.saledept.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind21.isOn()) {
		sv.saledept.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind22.isOn()) {
		sv.saledept.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind23.isOn()) {
		sv.saledptdes.setReverse(BaseScreenData.REVERSED);
		sv.saledptdes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind23.isOn()) {
		sv.saledptdes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind24.isOn()) {
		sv.saledptdes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind25.isOn()) {
		sv.agtype.setReverse(BaseScreenData.REVERSED);
		sv.agtype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind25.isOn()) {
		sv.agtype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind26.isOn()) {
		sv.agtype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind27.isOn()) {
		sv.tclntsel.setReverse(BaseScreenData.REVERSED);
		sv.tclntsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind27.isOn()) {
		sv.tclntsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind28.isOn()) {
		sv.tclntsel.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind29.isOn()) {
		sv.tcltname.setReverse(BaseScreenData.REVERSED);
		sv.tcltname.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind29.isOn()) {
		sv.tcltname.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind30.isOn()) {
		sv.tcltname.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind31.isOn()) {
		sv.tlevelno.setReverse(BaseScreenData.REVERSED);
		sv.tlevelno.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind31.isOn()) {
		sv.tlevelno.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind32.isOn()) {
		sv.tlevelno.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind33.isOn()) {
		sv.tleveltype.setReverse(BaseScreenData.REVERSED);
		sv.tleveltype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind33.isOn()) {
		sv.tleveltype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind34.isOn()) {
		sv.tleveltype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind35.isOn()) {
		sv.tleveldes.setReverse(BaseScreenData.REVERSED);
		sv.tleveldes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind35.isOn()) {
		sv.tleveldes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind36.isOn()) {
		sv.tleveldes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind37.isOn()) {
		sv.tagtype.setReverse(BaseScreenData.REVERSED);
		sv.tagtype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind37.isOn()) {
		sv.tagtype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind38.isOn()) {
		sv.tagtype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind39.isOn()) {
		sv.uclntsel.setReverse(BaseScreenData.REVERSED);
		sv.uclntsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind39.isOn()) {
		sv.uclntsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind40.isOn()) {
		sv.uclntsel.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind41.isOn()) {
		sv.ucltname.setReverse(BaseScreenData.REVERSED);
		sv.ucltname.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind41.isOn()) {
		sv.ucltname.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind42.isOn()) {
		sv.ucltname.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind43.isOn()) {
		sv.ulevelno.setReverse(BaseScreenData.REVERSED);
		sv.ulevelno.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind43.isOn()) {
		sv.ulevelno.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind44.isOn()) {
		sv.ulevelno.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind45.isOn()) {
		sv.uleveltype.setReverse(BaseScreenData.REVERSED);
		sv.uleveltype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind45.isOn()) {
		sv.uleveltype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind46.isOn()) {
		sv.uleveltype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind47.isOn()) {
		sv.uleveldes.setReverse(BaseScreenData.REVERSED);
		sv.uleveldes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind47.isOn()) {
		sv.uleveldes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind48.isOn()) {
		sv.uleveldes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind49.isOn()) {
		sv.uagtype.setReverse(BaseScreenData.REVERSED);
		sv.uagtype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind49.isOn()) {
		sv.uagtype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind50.isOn()) {
		sv.uagtype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind51.isOn()) {
		sv.history.setReverse(BaseScreenData.REVERSED);
		sv.history.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind51.isOn()) {
		sv.history.setHighLight(BaseScreenData.BOLD);
	}  
 	if (appVars.ind52.isOn()) {
		sv.history.setEnabled(BaseScreenData.DISABLED);
	}
	 if (appVars.ind53.isOn()) {
		sv.history.setInvisibility(BaseScreenData.INVISIBLE);
	}
	 if (appVars.ind54.isOn()) {
		sv.agtdesc.setReverse(BaseScreenData.REVERSED);
		sv.agtdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind54.isOn()) {
		sv.agtdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind55.isOn()) {
		sv.agtdesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind56.isOn()) {
		sv.tagtdesc.setReverse(BaseScreenData.REVERSED);
		sv.tagtdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind56.isOn()) {
		sv.tagtdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind57.isOn()) {
		sv.tagtdesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind58.isOn()) {
		sv.uagtdesc.setReverse(BaseScreenData.REVERSED);
		sv.uagtdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind58.isOn()) {
		sv.uagtdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind59.isOn()) {
		sv.uagtdesc.setEnabled(BaseScreenData.DISABLED);
	}
}%>

	<div class="panel panel-default">
	<div class="panel-body">
	<!-- 1st row -->
		<div class="row">

			<div class="col-md-3" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.brnchcd.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.brnchcd).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='brnchcd' id='brnchcd'
			type='text' 
			value='<%=sv.brnchcd.getFormData()%>' 
			maxLength='<%=sv.brnchcd.getLength()%>' 
			size='<%=sv.brnchcd.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(brnchcd)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.brnchcd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.brnchcd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.brnchcd).getColor()== null  ? 
			"input_cell" :  (sv.brnchcd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.brnchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
		<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.saledept).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='saledept' id='saledept'
			type='text' 
			value='<%=sv.saledept.getFormData()%>' 
			maxLength='<%=sv.saledept.getLength()%>' 
			size='<%=sv.saledept.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(saledept)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.saledept).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.saledept).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.saledept).getColor()== null  ? 
			"input_cell" :  (sv.saledept).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	</div>
	
	
		<!-- 2nd row -->
		<div class="row">
		
			<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.clntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='clntsel' id='clntsel'
			type='text' 
			value='<%=sv.clntsel.getFormData()%>' 
			maxLength='<%=sv.clntsel.getLength()%>' 
			size='<%=sv.clntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(clntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.clntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.clntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.clntsel).getColor()== null  ? 
			"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level Number "))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group" style="min-width:20px">  
				<%	
					longValue = sv.leveltype.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.leveltype).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='leveltype' id='leveltype'
			type='text' 
			value='<%=sv.leveltype.getFormData()%>' 
			maxLength='<%=sv.leveltype.getLength()%>' 
			size='<%=sv.leveltype.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(leveltype)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.leveltype).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.leveltype).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.leveltype).getColor()== null  ? 
			"input_cell" :  (sv.leveltype).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.leveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 135px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>

		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent Type"))%></label>
					<%
						if (!((sv.agtdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:180px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
	</div>
	
	<br>
	<hr>
	
	<!-- 3rd row -->
	
	<h5><b><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Top Level"))%></b></h5>

	<div class="row">
		
			<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group" style="min-width:75px">  
				<%	
					longValue = sv.tclntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.tclntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='tclntsel' id='tclntsel'
			type='text' 
			value='<%=sv.tclntsel.getFormData()%>' 
			maxLength='<%=sv.tclntsel.getLength()%>' 
			size='<%=sv.tclntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(tclntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.tclntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.tclntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('tclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.tclntsel).getColor()== null  ? 
			"input_cell" :  (sv.tclntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('tclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.tcltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tcltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tcltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level Number "))%></label>
					<%
						if (!((sv.tlevelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tlevelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tlevelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group" style="min-width:20px">  
				<%	
					longValue = sv.tleveltype.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.tleveltype).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='tleveltype' id='tleveltype'
			type='text' 
			value='<%=sv.tleveltype.getFormData()%>' 
			maxLength='<%=sv.tleveltype.getLength()%>' 
			size='<%=sv.tleveltype.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(tleveltype)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.tleveltype).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.tleveltype).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('tleveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.tleveltype).getColor()== null  ? 
			"input_cell" :  (sv.tleveltype).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('tleveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.tleveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 135px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>

		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent Type"))%></label>
					<%
						if (!((sv.tagtdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tagtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tagtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:180px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
	</div>
	
	<br>
	<hr>
	
	<!-- 4th row --> 
	
	<h5><b><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Upper Level"))%></b></h5>
	
	<div class="row">
		
			<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group" style="min-width:75px">  
				<%	
					longValue = sv.uclntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.uclntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='uclntsel' id='uclntsel'
			type='text' 
			value='<%=sv.uclntsel.getFormData()%>' 
			maxLength='<%=sv.uclntsel.getLength()%>' 
			size='<%=sv.uclntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(uclntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.uclntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.uclntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('uclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.uclntsel).getColor()== null  ? 
			"input_cell" :  (sv.uclntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('uclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.ucltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ucltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ucltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level Number "))%></label>
					<%
						if (!((sv.ulevelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ulevelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ulevelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group" style="min-width:20px">  
				<%	
					longValue = sv.uleveltype.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.uleveltype).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='uleveltype' id='uleveltype'
			type='text' 
			value='<%=sv.uleveltype.getFormData()%>' 
			maxLength='<%=sv.uleveltype.getLength()%>' 
			size='<%=sv.uleveltype.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(uleveltype)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.uleveltype).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.uleveltype).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('uleveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.uleveltype).getColor()== null  ? 
			"input_cell" :  (sv.uleveltype).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('uleveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.uleveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.uleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.uleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 135px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>

		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent Type"))%></label>
					<%
						if (!((sv.uagtdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.uagtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.uagtdesc.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:180px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
	</div>
	
	<br>
	<hr>
	
	<!-- 5th row -->
	
	<h5><b><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Lower Level"))%></b></h5>
	
	<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='Sjl64Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 50px"><%=resourceBundleHandler.gettingValueFromBundle("No.")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Level Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></th>
								</tr>
							</thead>
							<%
								GeneralTable sfl = fw.getTable("sjl64screensfl");
								GeneralTable sfl1 = fw.getTable("sjl64screensfl");
							%>
							<script language="javascript">
							        $(document).ready(function(){
										var rows = <%=sfl1.count() + 1%>;
										var isPageDown = 1;
								k		var pageSize = 1;
										var headerRowCount=1;
										var fields = new Array();
										fields[0] = "slt";
								<%if (false) {%>	
									operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"Sjl64Table",null,headerRowCount);
								<%}%>	
							
							        });
							    </script>
							<tbody>
								<%
									Sjl64screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (Sjl64screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>

								<tr id='tr<%=count%>' height="30">
									<%if(false){%>
									<td><input type='checkbox' onclick='setValues(<%=count%>)'>
										<input name='<%="chk_R"+count %>' id='<%="chk_R"+count %>'
										type='hidden' value=""></td>
									<%}%>

									<%if((new Byte((sv.slt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<% 
												if((new Byte((sv.slt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().
												isScreenProtected())){ 
											%>
									<% }else {%>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.slt.getLength()%>'
											value='<%= sv.slt.getFormData() %>'
											size='<%=sv.slt.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(Sjl64screensfl.slt)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="Sjl64screensfl" 
													+ "." + "slt" + "_R" + count %>'
											id='<%="Sjl64screensfl" + "." + "slt" + "_R" + count %>'
											class="input_cell"
											style="width: 
													<%=sv.slt.getLength()*12%> px;">
									</div>
									<%}%>
									<%}%>
									<td style="text-align: center; width: 80px;"><%=count%></td>
									<td style="text-align: left;">
										<%if((new Byte((sv.lclntsel).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.lclntsel.getFormData();
												%>
										<div id="Sjl64screensfl.lclntsel_R<%=count%>"
											name="Sjl64screensfl.lclntsel_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.lcltname).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.lcltname.getFormData();
												%>
										<div id="Sjl64screensfl.lcltname_R<%=count%>"
											name="Sjl64screensfl.lcltname_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.llevelno).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.llevelno.getFormData();
												%>
										<div id="Sjl64screensfl.llevelno_R<%=count%>"
											name="Sjl64screensfl.llevelno_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.lleveldes).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.lleveldes.getFormData();
												%>
										<div id="Sjl64screensfl.lleveldes_R<%=count%>"
											name="Sjl64screensfl.lleveldes_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.lagtdesc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.lagtdesc.getFormData();
												%>
										<div id="Sjl64screensfl.lagtdesc_R<%=count%>"
											name="Sjl64screensfl.lagtdesc_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>

								</tr>

								<%
									count = count + 1;
									Sjl64screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" to ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" of ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" entries ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	      <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	</div>
</div>

<div id='mainForm_OPTS' style='visibility:hidden;'>
<%-- <%if ((new Byte((sv.history).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                        || fw.getVariables().isScreenProtected()) {%>
			<%=smartHF.getMenuLink(sv.history, resourceBundleHandler.gettingValueFromBundle("History"),true)%>
<%} else{ %> 

			<%=smartHF.getMenuLink(sv.history, resourceBundleHandler.gettingValueFromBundle("History"),false)%>
<%}	%>
 --%>
 		<ul class="nav nav-second-level" aria-expanded="true" id='sidebar_OPTS'>
						<li>
									<input name='history' id='history' type='hidden'  value="<%=sv.history.getFormData()%>">
									<!-- text -->
   								 <%  if((sv.history.getInvisible()== BaseScreenData.INVISIBLE|| sv.history.getEnabled()==BaseScreenData.DISABLED)){
                                    %>
                                    <a href="#" class="disabledLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("History ")%>
                                    <%
                                     } else {
                                    %>
                                    <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("history"))' class="hyperLink">
                                        <%=resourceBundleHandler.gettingValueFromBundle("History ")%>
                                    <%}%>
									<!-- icon -->
									<%
									if (sv.history.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.history.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('history'))"></i> 
			 						<%}%>
			 						<%
									if (sv.history.getFormData().equals("?")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}%>
			 						</a>
								</li>
</ul>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl64Table').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: false,
    	scrollY: '300',
        scrollCollapse: true,
		language: {
			"lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
			"info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
			"sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
			"sEmptyTable": dtmessage,
			"paginate": {
				"next":       nextval,
				"previous":   previousval
			}
		},
		stateSave: true,
		"fnInfoCallback": function( settings, iStart, iEnd, iMax, iTotal, sPre ) {
			var iTotal = $('#totalRecords').val();
			return showval + " " + iStart + " " + toval+ " " + iEnd + " " + ofval + " " + iTotal + " " + entriesval;
		},
  	});
	fixedColumns: true
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>	