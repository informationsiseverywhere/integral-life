

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sr664ScreenVars sv = (Sr664ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Type in BW interface file ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Type Display ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Commission Type in BW interface file ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Commission Type Display ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.zpremtype.setReverse(BaseScreenData.REVERSED);
			sv.zpremtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.zpremtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.zpremtyped.setReverse(BaseScreenData.REVERSED);
			sv.zpremtyped.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.zpremtyped.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zcmsntype.setReverse(BaseScreenData.REVERSED);
			sv.zcmsntype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zcmsntype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.zcmsntyped.setReverse(BaseScreenData.REVERSED);
			sv.zcmsntyped.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.zcmsntyped.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 300px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<%-- row 2 --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Type in BW interface file"))%></label>
					<input style="width: 100px;" name='zpremtype' type='text'
						<%formatValue = (sv.zpremtype.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.zpremtype.getLength()%>'
						maxLength='<%=sv.zpremtype.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zpremtype)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zpremtype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zpremtype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zpremtype).getColor() == null ? "input_cell"
						: (sv.zpremtype).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Type Display"))%></label>
					<input style="width: 150px;" name='zpremtyped' type='text'
						<%formatValue = (sv.zpremtyped.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.zpremtyped.getLength()%>'
						maxLength='<%=sv.zpremtyped.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zpremtyped)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zpremtyped).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zpremtyped).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zpremtyped).getColor() == null ? "input_cell"
						: (sv.zpremtyped).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>
		<%-- row 3--%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF
					.getLabel(resourceBundleHandler.gettingValueFromBundle("Commission Type in BW interface file"))%></label>
					<input style="width: 100px;" name='zcmsntype' type='text'
						<%formatValue = (sv.zcmsntype.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.zcmsntype.getLength()%>'
						maxLength='<%=sv.zcmsntype.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zcmsntype)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zcmsntype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zcmsntype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zcmsntype).getColor() == null ? "input_cell"
						: (sv.zcmsntype).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commission Type Display"))%></label>
					<input style="width: 150px;" name='zcmsntyped' type='text'
						<%formatValue = (sv.zcmsntyped.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.zcmsntyped.getLength()%>'
						maxLength='<%=sv.zcmsntyped.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zcmsntyped)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zcmsntyped).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zcmsntyped).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zcmsntyped).getColor() == null ? "input_cell"
						: (sv.zcmsntyped).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>