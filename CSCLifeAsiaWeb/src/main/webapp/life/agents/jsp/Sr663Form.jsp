

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR663";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sr663ScreenVars sv = (Sr663ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Default Profile Code ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Parameter for Agency ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"AC - Area Code");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"SU - Sales Unit");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "BR - Branch");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Default values");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agency");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Start Date ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tax ID ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.zprofile.setReverse(BaseScreenData.REVERSED);
			sv.zprofile.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.zprofile.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.zparam.setReverse(BaseScreenData.REVERSED);
			sv.zparam.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.zparam.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.dteappDisp.setReverse(BaseScreenData.REVERSED);
			sv.dteappDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.dteappDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ztaxid.setReverse(BaseScreenData.REVERSED);
			sv.ztaxid.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ztaxid.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<%-- lables row --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Profile Code")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Parameter for Agency")%></label>
				</div>
			</div>
		</div>
		<%-- row 2 --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 100px;">
					<input name='zprofile' type='text'
						<%formatValue = (sv.zprofile.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.zprofile.getLength()%>'
						maxLength='<%=sv.zprofile.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zprofile)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.zprofile).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zprofile).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zprofile).getColor() == null ? "input_cell"
						: (sv.zprofile).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						longValue = sv.zparam.getFormData();
						if ("AC".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Area Code");
						} else if ("SU".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Sales Unit");
						} else if ("BR".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Branch");
						}
						if ((new Byte((sv.zparam.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.zparam).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
						<%
							}
						%>
						<select name='zparam' type='list'
							<%if ((new Byte((sv.zparam).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.zparam).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<option value="">...</option>
							<option value="AC"
								<%if ("AC".equals(sv.zparam.getFormData())) {%> SELECTED <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></option>
							<option value="SU"
								<%if ("SU".equals(sv.zparam.getFormData())) {%> SELECTED <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Sales Unit")%></option>
							<option value="BR"
								<%if ("BR".equals(sv.zparam.getFormData())) {%> SELECTED <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></option>
						</select>
						<%
							if ("red".equals((sv.zparam).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>
		</div>
		<%-- row 3--%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default values"))%></label>
				</div>
			</div>
		</div>
		<br />
		<%-- row 4--%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agency"))%></label>
				</div>
			</div>
		</div>
		<br />
		<%-- row 5--%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Date"))%></label>
					<div class="input-group">

						<%
							if ((new Byte((sv.dteappDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<%=smartHF.getRichTextDateInput(fw, sv.dteappDisp, (sv.dteappDisp.getLength()))%>
						<%
							} else {
						%>
						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.dteappDisp, (sv.dteappDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<%
							}
						%>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Tax ID"))%></label>
					<input name='ztaxid' type='text'
						<%formatValue = (sv.ztaxid.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.ztaxid.getLength()%>'
						maxLength='<%=sv.ztaxid.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(ztaxid)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.ztaxid).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ztaxid).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ztaxid).getColor() == null ? "input_cell"
						: (sv.ztaxid).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>
	</div>
</div>




<%@ include file="/POLACommon2NEW.jsp"%>


