<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5181";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5181ScreenVars sv = (S5181ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date");%>	
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Num");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalmt");%>
<%{
}%>


<div class="panel panel-default" >
        
        <div class="panel-body" >     
            <div class="row">   
                <div class="col-md-4">
                 <div class="form-group" >
                    <%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
                   
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                        <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
					        <%  
					            qpsf = fw.getFieldXMLDef((sv.chdrnum).getFieldName());
					            qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
					            formatValue = smartHF.getPicFormatted(qpsf,sv.chdrnum);
					            
					            if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					                    if(longValue == null || longValue.equalsIgnoreCase("")) {           
					                    formatValue = formatValue( formatValue );
					                    } else {
					                    formatValue = formatValue( longValue );
					                    }
					            }
					    
					            if(!formatValue.trim().equalsIgnoreCase("")) {
					        %>
					                <div class="output_cell">   
					                    <%= XSSFilter.escapeHtml(formatValue)%>
					                </div>
					        <%
					            } else {
					        %>
					                <div class="blank_cell" > &nbsp; </div>
					        <% 
					            } 
					        %>
					        <%
					        longValue = null;
					        formatValue = null;
					        %>
					    
					 <%}%>
                    </div>
                    <%} %>
                </div>
                
                <div class="col-md-4" >
                    <%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
                                BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
                        <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {  
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.life.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.life.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
                    </div>
                    <%} %>
                </div>
               
                <div class="col-md-4">
                    <%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
                                BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group"   style="max-width:100px;">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
                        <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {  
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="row">   
                <div class="col-md-4">
                <div classs="form-group">
                    <%if ((new Byte((generatedText6).getInvisible())).compareTo(
                    		new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
                    
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
                       <table><tr><td>
                        <%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {   
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
						</td><td>
						<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width:100px; margin-left:1px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>   
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
						</td></tr></table>
                   
                    <%} %>
                     </div>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
                            BaseScreenData.INVISIBLE)) != 0) { %>
                    <div classs="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
                        <%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.rider.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.rider.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="row">   
                <div class="col-md-4">
                    <%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
                        BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
                    <div class="input-group"> 
                          <%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null)%>
                        <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp)%></td>
                    </div></div>
                    <%} %>
                </div>
               
                <div class="col-md-4">
                    <%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
                        BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
                        <%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {   
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
                    </div>
                    <%} %>
                </div>
                
                <div class="col-md-4">
                    <%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
                                BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
                        <%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {   
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>  
						    <%
						    longValue = null;
						    formatValue = null;
						    %>
						<%}%>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="row">   
                <div class="col-md-4">
                    <%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
                        BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Effective date")%></label>
                         <div class="input-group">  <%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null)%>
                        <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp)%></td>
                    </div></div>
                    <%} %>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
                            BaseScreenData.INVISIBLE)) != 0) { %>
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>
                        <%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						    <% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {    
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
						            } else {
						                formatValue = formatValue( longValue);
						            }                       
						        }else{
						            if(longValue == null || longValue.equalsIgnoreCase("")) {
						                        formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
						            }else{
						                formatValue = formatValue( longValue);
						            }
						        }
						    %>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>  
							    <%
							    longValue = null;
							    formatValue = null;
							    %>
							<%}%>
                        <%} %>
                    </div>
                </div>
               
                <div class="col-md-4">
                    <%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
                        BaseScreenData.INVISIBLE)) != 0) { %>
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Instalmt")%></label>
                        <%if ((new Byte((sv.instprem).getInvisible())).compareTo(new Byte(
                                BaseScreenData.INVISIBLE)) != 0) { %>
						    <%  
						        qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
						        valueThis=smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						    %>
							<input name='instprem' 
							type='text'
							<%if((sv.instprem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:71px;"<% }%>
							    value='<%=valueThis %>'
							    <%if(valueThis!=null&& valueThis.trim().length()>0) {%>
							     title='<%=valueThis %>'
							    <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(  sv.instprem.getLength(),  sv.instprem.getLength(),3)%>'
							maxLength='<%= sv.instprem.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(instprem)' onKeyUp='return checkMaxLength(this)'  
							style="width:100px;"
							    onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>, true ); "
							    decimal='<%=qpsf.getDecimals()%>' 
							    onPaste='return doPasteNumber(event,true);'
							    onBlur='return doBlurNumberNew(event,true);'
							<% 
							    if((new Byte((sv.instprem).getEnabled()))
							    .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
							%>  
							    readonly="false"
							    class="output_cell" 
							<%
							    }else if((new Byte((sv.instprem).getHighLight())).
							        compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>  
							    class="bold_cell"  
							<%
							    }else { 
							%>
							    class = ' <%=(sv.instprem).getColor()== null  ? 
							            "output_cell" :  (sv.instprem).getColor().equals("red") ? 
							            "output_cell red reverse" : "output_cell" %>'
							<%
							    } 
							%>
							>
						<%} %>
                    </div>
                    <%} %>
                </div>
             </div>
             <br>
             <div class="row">
                <div class="col-md-12">
	               
	                    <div class="table-responsive">
	                        <table class="table table-striped table-bordered table-hover" id='dataTables-s5181' width="100%">
	                            <thead>
			                    <tr class="info">
			                        
			                         <th><center>Fund</center></th>
									 <th><center>Fund Type</center></th>
									 <th><center>Amount - Add/ Subtract</center></th>
									 <th><center>Total Amount Summary</center></th>
			                  </tr>
			                    </thead>
			                    <tbody>
			                    <%  appVars.rollup(new int[] {93}); %>
<% 
GeneralTable sfl = fw.getTable("s5181screensfl");
GeneralTable sfl1 = fw.getTable("s5181screensfl");
S5181screensfl.set1stScreenRow(sfl, appVars, sv);   
%>
<%S5181screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (S5181screensfl.hasMoreScreenRows(sfl)) { 
hyperLinkFlag=true;
%>
<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.hseqno.getLength()%>'
 value='<%= sv.hseqno.getFormData() %>' 
 size='<%=sv.hseqno.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.hseqno)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5181screensfl" + "." + "hseqno" + "_R" + count %>'
 id='<%="s5181screensfl" + "." + "hseqno" + "_R" + count %>'          >


 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5181screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="s5181screensfl" + "." + "screenIndicArea" + "_R" + count %>'         >


 <input type='hidden' maxLength='<%=sv.updteflag.getLength()%>'
 value='<%= sv.updteflag.getFormData() %>' 
 size='<%=sv.updteflag.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.updteflag)' onKeyUp='return checkMaxLength(this)' 
 name='<%="s5181screensfl" + "." + "updteflag" + "_R" + count %>'
 id='<%="s5181screensfl" + "." + "updteflag" + "_R" + count %>'       >

<!--  Ilife- Life Cross Browser - Sprint 4 D2 : Task 1 START-->

 <td <%if(!(((BaseScreenData)sv.unitVirtualFund) instanceof StringBase)) {%>align="right"<% }else {%> align="center" <%}%> >             <%if((new Byte((sv.unitVirtualFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> 
        <% 
                    if((new Byte((sv.unitVirtualFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                    longValue = sv.unitVirtualFund.getFormData(); 
                    %> 
                    <div id="s5181screensfl.unitVirtualFund_R<%=count%>" name="s5181screensfl.unitVirtualFund_R<%=count%>" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
                                        "blank_cell" : "output_cell" %>'>  
                                            <%if(longValue != null){%>
                                                        <%=longValue%>
                
                                            <%}%>
                    </div>

                            <%longValue=null; %>
                            <% }else {%>
                        <div class="input-group" style="max-width:120px;">
                        <input name='<%="s5181screensfl" + "." +
                         "unitVirtualFund" + "_R" + count %>'
                        id='<%="s5181screensfl" + "." +
                         "unitVirtualFund" + "_R" + count %>'
                        type='text' 
                        value='<%= sv.unitVirtualFund.getFormData() %>' 
                        class = " <%=(sv.unitVirtualFund).getColor()== null  ? 
                        "input_cell" :  
                        (sv.unitVirtualFund).getColor().equals("red") ? 
                        "input_cell red reverse" : 
                        "input_cell" %>" 
                        maxLength='<%=sv.unitVirtualFund.getLength()%>' 
                        onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.unitVirtualFund)' onKeyUp='return checkMaxLength(this)' 
                        >       
                        <span class="input-group-btn">
                            <button class="btn btn-info" style="font-size: 19px;border-bottom-width: 2px !important;" type="button" onClick="doFocus(document.getElementById('<%="s5181screensfl" + "." + "unitVirtualFund" + "_R" + count %>')); doAction('PFKEY04');">
                                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                           </button>
                       </span>
                        </div>
                    <%}%>
           <%}%>
</td> 
<td <%if(!(((BaseScreenData)sv.zrectyp) instanceof StringBase)) {%>align="right"<% }else {%> align="center" <%}%> >                                 
            <%if((new Byte((sv.zrectyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.zrectyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
                    <%if((new Byte((sv.zrectyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> 
                        <% 
                        if((new Byte((sv.zrectyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                             
                        %>
                            <%longValue = sv.zrectyp.getFormData();%>
                            
                            <div id="s5181screensfl.zrectyp_R<%=count%>" name="s5181screensfl.zrectyp_R<%=count%>" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
                                                <%if(longValue != null){%>
                                                            <%=longValue%>
                             
                                                <%}%>
                            </div>
                            <%longValue=null; %>
                                                    
                        <% }else {%>
                             
                            <input type='text' 
                             maxLength='<%=sv.zrectyp.getLength()%>'
                             value='<%= sv.zrectyp.getFormData() %>' 
                             size='<%=sv.zrectyp.getLength()%>'
                             onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.zrectyp)' onKeyUp='return checkMaxLength(this)' 
                             name='<%="s5181screensfl" + "." +  "zrectyp" + "_R" + count %>'
                             id='<%="s5181screensfl" + "." + "zrectyp" + "_R" + count %>'
                             class = "input_cell"  style = "width <%=sv.zrectyp.getLength()*12%>px;">     
                            <%}%> 
                                        
                    <%}%>
</td>


<!--ILIFE-1530 STARTS-->
<td align="center">                                 
            <%if((new Byte((sv.fundAmount).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>  
                                        
    
                        <%  
                            sm = sfl.getCurrentScreenRow();
                            qpsf = sm.getFieldXMLDef((sv.fundAmount).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            formatValue = smartHF.getPicFormatted(qpsf,sv.fundAmount,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);                         
                        %>                  	
                        <% 
                    if((new Byte((sv.fundAmount).getEnabled()))
                        .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                    %> 
                    <div id="s5181screensfl.fundAmount_R<%=count%>" name="s5181screensfl.fundAmount_R<%=count%>" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                                        "blank_cell" : "output_cell" %>'>  
                                            <%if(formatValue != null){%>                                            
                                                        <%=XSSFilter.escapeHtml(formatValue)%>
                                            <%}%>
                    </div>                      
                            <% }else {%>                                                
                        <input type='text' 
                         value='<%=formatValue%>' 
                         <%if (qpsf.getDecimals() > 0) {%>
                            size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.fundAmount.getLength(), sv.fundAmount.getLength(),3)+10%>'
                            maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.fundAmount.getLength(), sv.fundAmount.getLength(),3)+1%>' 
                        <%}else{%>
                            size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.fundAmount.getLength(), sv.fundAmount.getLength(),3)%>'
                            maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.fundAmount.getLength(), sv.fundAmount.getLength(),3)%>' 
                        <%}%>
                         onFocus='doFocus(this)' onHelp='return fieldHelp(s5181screensfl.fundAmount)' onKeyUp='return checkMaxLength(this)' 
                         name='<%="s5181screensfl" + "." +
                         "fundAmount" + "_R" + count %>'
                         id='<%="s5181screensfl" + "." +
                         "fundAmount" + "_R" + count %>'
                        class = " <%=(sv.fundAmount).getColor()== null  ? 
                        "input_cell" :  
                        (sv.fundAmount).getColor().equals("red") ? 
                        "input_cell red reverse" : 
                        "input_cell" %>" 
                          style = "width: <%=sv.fundAmount.getLength()*12%> px;text-align: right;"                        
                            onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>, true ); "
                            decimal='<%=qpsf.getDecimals()%>' 
                            onPaste='return doPasteNumber(event,true);'
                            onBlur='return doBlurNumberNew(event,true);'
                          title='<%=formatValue %>'
                         >
                     <%}%>
                    <%}%>
</td>


<!--ILIFE-1530 ENDS-->

<td align="center">
            <%if((new Byte((sv.zcurprmbal).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>  
                            <span>
                            <!-- fix #ILIFE756 BY FWANG3-->
                            <div class="output_cell" style="width:150px;text-align: right;">
                            <% String sData = sv.zcurprmbal.getFormData();
                            long lData = Long.parseLong(sData);
                              
                                if (lData > 0){
                                %>
                                    <%=smartHF.getPicFormatted(qpsf,sv.zcurprmbal) %>
                                
                                <%}%>
                            </div>
                    
                            </span>                          
                
                                    <%}%>
                        </td>
</tr>
<%  count = count + 1;
S5181screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</tbody>
</table>
</div>

</div>
</div>



        </div>
</div>




<script>
$(document).ready(function() {
	$('#dataTables-s5181').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '350px',
        scrollCollapse: true,
        info:false,
        paging:false
        
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
