<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR58R";%>
<%@ include file="/POLACommon3.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr58rScreenVars sv = (Sr58rScreenVars) fw.getVariables();%>
<%{
}%>

<div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.company,(sv.company.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
<%=smartHF.getHTMLF4NSVar(0, 0, fw, sv.company).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.tabl,( sv.tabl.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.item,( sv.item.getLength()+1),null).replace("absolute","relative")%>
<%=smartHF.getRichText(0,0,fw,sv.longdesc,( sv.longdesc.getLength()+1),null).replace("absolute","relative")%>
</td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date Effective");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute","relative; font-weight: bold;")%>
<br/>

<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>
&nbsp;
<%StringData ITMTO_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%=smartHF.getLit(0, 0, ITMTO_LBL).replace("absolute","relative; font-weight: bold;")%>

<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%></td>
</tr>

<tr style='height:22px;'><td width='251'>
<%StringData MINPCNT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Persistency Ratio Criteria");%>
<%=smartHF.getLit(0, 0, MINPCNT_LBL).replace("absolute","relative; font-weight: bold;")%>

<%=smartHF.getRichText(0,0,fw,sv.minpcnt,( sv.minpcnt.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
</tr> </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_continue.png" width='84' height='22' alt='Continue'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_refresh.png" width='84' height='22' alt='Refresh'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_previous.png" width='84' height='22' alt='Previous'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_exit.png" width='84' height='22' alt='Exit'></a></li>
</ul>
</div>
<%@ include file="/POLACommon4.jsp"%>

