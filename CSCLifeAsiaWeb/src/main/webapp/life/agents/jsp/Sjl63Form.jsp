<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL63";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl63ScreenVars sv = (Sjl63ScreenVars) fw.getVariables();%>

<%{
	 if (appVars.ind01.isOn()) {
		sv.clntsel.setReverse(BaseScreenData.REVERSED);
		sv.clntsel.setColor(BaseScreenData.RED);
		sv.agclntsel.setReverse(BaseScreenData.REVERSED);
		sv.agclntsel.setColor(BaseScreenData.RED);
		sv.agncysel.setReverse(BaseScreenData.REVERSED);
		sv.agncysel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.clntsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind02.isOn()) {
		sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		sv.agclntsel.setEnabled(BaseScreenData.DISABLED);
		sv.agncysel.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind03.isOn()) {
		sv.cltname.setReverse(BaseScreenData.REVERSED);
		sv.cltname.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.cltname.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.cltname.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind05.isOn()) {
		sv.company.setReverse(BaseScreenData.REVERSED);
		sv.company.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.company.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.company.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind07.isOn()) {
		sv.brnchcd.setReverse(BaseScreenData.REVERSED);
		sv.brnchcd.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.brnchcd.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind08.isOn()) {
		sv.brnchcd.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind09.isOn()) {
		sv.brnchdesc.setReverse(BaseScreenData.REVERSED);
		sv.brnchdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind09.isOn()) {
		sv.brnchdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind10.isOn()) {
		sv.brnchdesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind11.isOn()) {
		sv.aracde.setReverse(BaseScreenData.REVERSED);
		sv.aracde.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind11.isOn()) {
		sv.aracde.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind12.isOn()) {
		sv.aracde.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind13.isOn()) {
		sv.aradesc.setReverse(BaseScreenData.REVERSED);
		sv.aradesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind13.isOn()) {
		sv.aradesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind14.isOn()) {
		sv.aradesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind15.isOn()) {
		sv.levelno.setReverse(BaseScreenData.REVERSED);
		sv.levelno.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind15.isOn()) {
		sv.levelno.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind16.isOn()) {
		sv.levelno.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind17.isOn()) {
		sv.leveltype.setReverse(BaseScreenData.REVERSED);
		sv.leveltype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind17.isOn()) {
		sv.leveltype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind18.isOn()) {
		sv.leveltype.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind19.isOn()) {
		sv.leveldes.setReverse(BaseScreenData.REVERSED);
		sv.leveldes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind19.isOn()) {
		sv.leveldes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind20.isOn()) {
		sv.leveldes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind21.isOn()) {
		sv.saledept.setReverse(BaseScreenData.REVERSED);
		sv.saledept.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind21.isOn()) {
		sv.saledept.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind22.isOn()) {
		sv.saledept.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind23.isOn()) {
		sv.saledptdes.setReverse(BaseScreenData.REVERSED);
		sv.saledptdes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind23.isOn()) {
		sv.saledptdes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind24.isOn()) {
		sv.saledptdes.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind25.isOn()) {
		sv.agtype.setReverse(BaseScreenData.REVERSED);
		sv.agtype.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind25.isOn()) {
		sv.agtype.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind26.isOn()) {
		sv.agtype.setEnabled(BaseScreenData.DISABLED);
	}
}%>

<div class="panel panel-default">
	<div class="panel-body">
	<!-- 1st row -->
		<div class="row">

			<div class="col-md-2" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.brnchcd.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.brnchcd).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='brnchcd' id='brnchcd'
			type='text' 
			value='<%=sv.brnchcd.getFormData()%>' 
			maxLength='<%=sv.brnchcd.getLength()%>' 
			size='<%=sv.brnchcd.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(brnchcd)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.brnchcd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.brnchcd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.brnchcd).getColor()== null  ? 
			"input_cell" :  (sv.brnchcd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.brnchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
		<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.saledept).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='saledept' id='saledept'
			type='text' 
			value='<%=sv.saledept.getFormData()%>' 
			maxLength='<%=sv.saledept.getLength()%>' 
			size='<%=sv.saledept.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(saledept)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.saledept).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.saledept).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.saledept).getColor()== null  ? 
			"input_cell" :  (sv.saledept).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	</div>
	
	
		<!-- 2nd row -->
		<div class="row">
		
			<div class="col-md-2">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.clntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='clntsel' id='clntsel'
			type='text' 
			value='<%=sv.clntsel.getFormData()%>' 
			maxLength='<%=sv.clntsel.getLength()%>' 
			size='<%=sv.clntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(clntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.clntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.clntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.clntsel).getColor()== null  ? 
			"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 103px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
			<div class="col-md-2" style='padding-left:30px'>
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level Number"))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.leveltype.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.leveltype).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='leveltype' id='leveltype'
			type='text' 
			value='<%=sv.leveltype.getFormData()%>' 
			maxLength='<%=sv.leveltype.getLength()%>' 
			size='<%=sv.leveltype.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(leveltype)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.leveltype).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.leveltype).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.leveltype).getColor()== null  ? 
			"input_cell" :  (sv.leveltype).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.leveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 135px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>

		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent Type"))%></label>
			<div class="input-group">
        					<% if ((new Byte((sv.agtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"agtype"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("agtype");
								optionValue = makeDropDownList( mappedItems , sv.agtype.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.agtype.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.agtype, fw, longValue, "agtype", optionValue) %>
								<%}%>
							</div>
				</div>
			</div>
			
				<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 8px; max-width:100px">
					<input name='userid' type='text'
						<%formatValue = (sv.userid.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.userid.getLength()%>'
						maxLength='<%=sv.userid.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(userid)'
						onKeyUp='return checkMaxLength(this)'
						readonly="true" class="output_cell"
						>
				</div></div>
			</div>
	</div>

	<br> <hr> <br>
	
	
	<div class="row">
	
	
				<div class="col-md-2">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.agclntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agclntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='agclntsel' id='agclntsel'
			type='text' 
			value='<%=sv.agclntsel.getFormData()%>' 
			maxLength='<%=sv.agclntsel.getLength()%>' 
			size='<%=sv.agclntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(agclntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.agclntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.agclntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('agclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.agclntsel).getColor()== null  ? 
			"input_cell" :  (sv.agclntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agclntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
				 
				  <td style="padding-left:1px">
				
				  		
						<%					
						if(!((sv.agcltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agcltname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agcltname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;max-width:100px;" >
								<%=formatValue%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td>
	</tr></table>
			</div>
		</div>
			
				<div class="col-md-2" style='padding-left:30px'>
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></label>
				
				<table><tr><td class="form-group">
				<div class="input-group">
				<%
					longValue = sv.agncysel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='agncysel'  id='agncysel'
				type='text' 
				value='<%=sv.agncysel.getFormData()%>' 
				maxLength='<%=sv.agncysel.getLength()%>' 
				size='<%=sv.agncysel.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(agncysel)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 style="min-width:75px">
				
				<%
					}else if((new Byte((sv.agncysel).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" style="min-width:75px">
				 
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.agncysel).getColor()== null  ? 
				"input_cell" :  (sv.agncysel).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>'  style="min-width:75px">
				
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%}longValue = null;} %>
		</div>	</td>	 
			
			</tr></table>
						</div>
				  </div>
				  
				  	<div class="col-md-2" >
			<div class="form-group">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Registration Number"))%></label>
					<div class="input-group">  
				<%	
					longValue = sv.regnum.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px;max-width:150px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 
			</div>
		</div>
		
			<div class="col-md-2" >
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Date"))%></label>
								<div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.srdateDisp)%></div>
     
 
				</div>
			</div>
					
			<div class="col-md-2" style="padding-left:0px">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Date"))%></label>
					   <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.endateDisp)%></div>
 
				</div>
			</div>
			
						  	<div class="col-md-2" >
			<div class="form-group">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid Flag"))%></label>
					<div class="input-group">  
				<%	
					longValue = sv.validflag.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:30px;max-width:50px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 
			</div>
		</div>
			
			</div>
	
	
	<br>
	<br>
	<!-- Table -->
	
	
	 <input type="hidden" id="flagind" name="action1" value=""/>
	
	<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='Sjl63Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center;width: 25px;"><%=resourceBundleHandler.gettingValueFromBundle("No.")%></th>
									<th style="text-align: center;width: 97px;"><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></th>
									<th style="text-align: center;width: 130px;"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th style="text-align: center;width: 100px;"><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Gender")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Birth Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("License Number")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Appointed Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Terminated Date")%></th>
								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("sjl63screensfl");
									Sjl63screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (Sjl63screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>
								<tr id='tr<%=count%>' height="30">
									<td style="text-align: center;"><%=count%></td>
									<td style="text-align: left;">
										<%if((new Byte((sv.aclntsel).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.aclntsel.getFormData();
												%>
										<div id="Sjl63screensfl.aclntsel_R<%=count%>"
											name="Sjl63screensfl.aclntsel_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.acltname).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.acltname.getFormData();
												%>
										<div id="Sjl63screensfl.acltname_R<%=count%>"
											name="Sjl63screensfl.acltname_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
										<td style="text-align: left;">
										<%if((new Byte((sv.agntnum).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.agntnum.getFormData();
												%>
										<div id="Sjl63screensfl.agntnum_R<%=count%>"
											name="Sjl63screensfl.agntnum_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.gender).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.gender.getFormData();
												%>
										<div id="Sjl63screensfl.gender_R<%=count%>"
											name="Sjl63screensfl.gender_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.dobDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.dobDisp.getFormData();
												%>
										<div id="Sjl63screensfl.dobDisp_R<%=count%>"
											name="Sjl63screensfl.dobDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.agnttyp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.agnttyp.getFormData();
												%>
										<div id="Sjl63screensfl.agnttyp_R<%=count%>"
											name="Sjl63screensfl.agnttyp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.liscno).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.liscno.getFormData();
												%>
										<div id="Sjl63screensfl.liscno_R<%=count%>"
											name="Sjl63screensfl.liscno_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.appdateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.appdateDisp.getFormData();
												%>
										<div id="Sjl63screensfl.appdateDisp_R<%=count%>"
											name="Sjl63screensfl.appdateDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.termdateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.termdateDisp.getFormData();
												%>
										<div id="Sjl63screensfl.termdateDisp_R<%=count%>"
											name="Sjl63screensfl.termdateDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>

								</tr>

								<%
									count = count + 1;
									Sjl63screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" to ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" of ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" entries ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	      <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl63Table').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
		language: {
			"lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
			"info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
			"sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
			"sEmptyTable": dtmessage,
			"paginate": {
				"next":       nextval,
				"previous":   previousval
			}
		},
		stateSave: true,
		"fnInfoCallback": function( settings, iStart, iEnd, iMax, iTotal, sPre ) {
			var iTotal = $('#totalRecords').val();
			return showval + " " + iStart + " " + toval+ " " + iEnd + " " + ofval + " " + iTotal + " " + entriesval;
		},
  	});
})


function flag(flagind) {	
	 $("#flagind").val('1');
	doAction("PFKEY0");
}
	
</script>
<%@ include file="/POLACommon2NEW.jsp"%>	