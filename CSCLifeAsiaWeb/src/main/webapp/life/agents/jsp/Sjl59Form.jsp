<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sjl59";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl59ScreenVars sv = (Sjl59ScreenVars) fw.getVariables();%>

<%{
	if (appVars.ind05.isOn()) {
		sv.company.setReverse(BaseScreenData.REVERSED);
		sv.company.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.company.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.company.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind07.isOn()) {
		sv.brnchcd.setReverse(BaseScreenData.REVERSED);
		sv.brnchcd.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.brnchcd.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind08.isOn()) {
		sv.brnchcd.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind09.isOn()) {
		sv.brnchdesc.setReverse(BaseScreenData.REVERSED);
		sv.brnchdesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind09.isOn()) {
		sv.brnchdesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind10.isOn()) {
		sv.brnchdesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind11.isOn()) {
		sv.aracde.setReverse(BaseScreenData.REVERSED);
		sv.aracde.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind11.isOn()) {
		sv.aracde.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind12.isOn()) {
		sv.aracde.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind13.isOn()) {
		sv.aradesc.setReverse(BaseScreenData.REVERSED);
		sv.aradesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind13.isOn()) {
		sv.aradesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind14.isOn()) {
		sv.aradesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind21.isOn()) {
		sv.saledept.setReverse(BaseScreenData.REVERSED);
		sv.saledept.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind21.isOn()) {
		sv.saledept.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind22.isOn()) {
		sv.saledept.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind23.isOn()) {
		sv.saledptdes.setReverse(BaseScreenData.REVERSED);
		sv.saledptdes.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind23.isOn()) {
		sv.saledptdes.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind24.isOn()) {
		sv.saledptdes.setEnabled(BaseScreenData.DISABLED);
	}
}%>

<div class="panel panel-default">
	<div class="panel-body">
	<!-- 1st row -->
		<div class="row">

			<div class="col-md-3" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.brnchcd.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.brnchcd).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='brnchcd' id='brnchcd'
			type='text' 
			value='<%=sv.brnchcd.getFormData()%>' 
			maxLength='<%=sv.brnchcd.getLength()%>' 
			size='<%=sv.brnchcd.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(brnchcd)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.brnchcd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.brnchcd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.brnchcd).getColor()== null  ? 
			"input_cell" :  (sv.brnchcd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.brnchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		
		<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.saledept).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='saledept' id='saledept'
			type='text' 
			value='<%=sv.saledept.getFormData()%>' 
			maxLength='<%=sv.saledept.getLength()%>' 
			size='<%=sv.saledept.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(saledept)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.saledept).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.saledept).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.saledept).getColor()== null  ? 
			"input_cell" :  (sv.saledept).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	</div>
	
	
		<!-- 2nd row -->
		
		<!-- 3rd row -->
	<div class="row">

      		<div class="col-md-3 ">
        			<div class="form-group">
        			
        				<label><%=resourceBundleHandler.gettingValueFromBundle("Active Status")%></label>
        				<div class="input-group">
        					<% if ((new Byte((sv.stattype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"stattype"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("stattype");
								optionValue = makeDropDownList( mappedItems , sv.stattype.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.stattype.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.stattype, fw, longValue, "stattype", optionValue) %>
								<%}%>
							</div>
        			</div>
        		</div>
        		
<%-- 		 <div class="col-md-3">
				<div class="btn-group">
					<div class="sectionbutton">
						
						<p style="font-size: 12px; font-weight: bold;">
							<a class="btn btn-info" href="#"
								onClick=" JavaScript:doAction('PFKEY0');"><%=resourceBundleHandler.gettingValueFromBundle("Search")%></a>
						</p>
					</div>
				</div>
			</div> --%>
	</div>
	<br>
	<br>
	<!-- Table -->
	
	
	<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='Sjl59Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 50px; "><%=resourceBundleHandler.gettingValueFromBundle("No.")%></th>
									<th style="text-align: center; width: 100px;"><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></th>
									<th style="text-align: center; width: 180px;"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th style="text-align: center; width: 80px;"><%=resourceBundleHandler.gettingValueFromBundle("Level No.")%></th>
									<th style="text-align: center; width: 160px;"><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></th>
									<th style="text-align: center; width: 200px;"><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></th>
									<th style="text-align: center; width: 100px;"><%=resourceBundleHandler.gettingValueFromBundle("User Id")%></th>
									<th style="text-align: center; width: 80px;"><%=resourceBundleHandler.gettingValueFromBundle("Status")%></th>
									<th style="text-align: center; width: 80px;"><%=resourceBundleHandler.gettingValueFromBundle("Reg Type")%></th>
									<th style="text-align: center; width: 100px;"><%=resourceBundleHandler.gettingValueFromBundle("Reg Date")%></th>
									<th style="text-align: center; width: 60px;"><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></th>
									<th style="text-align: center; width: 300px;"><%=resourceBundleHandler.gettingValueFromBundle("Reason Details")%></th>
									
								</tr>
							</thead>
							<%
								GeneralTable sfl = fw.getTable("sjl59screensfl");
								GeneralTable sfl1 = fw.getTable("sjl59screensfl");
							%>
							<script language="javascript">
							        $(document).ready(function(){
										var rows = <%=sfl1.count() + 1%>;
										var isPageDown = 1;
								k		var pageSize = 1;
										var headerRowCount=1;
										var fields = new Array();
										fields[0] = "slt";
								<%if (false) {%>	
									operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"Sjl59Table",null,headerRowCount);
								<%}%>	
							
							        });
							    </script>
							<tbody>
								<%
									Sjl59screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									boolean hyperLinkFlag;
									while (Sjl59screensfl.hasMoreScreenRows(sfl)) {
										hyperLinkFlag = true;
								%>

								<tr id='tr<%=count%>' height="30">
									<%if(false){%>
									<td><input type='checkbox' onclick='setValues(<%=count%>)'>
										<input name='<%="chk_R"+count %>' id='<%="chk_R"+count %>'
										type='hidden' value=""></td>
									<%}%>

									<%if((new Byte((sv.slt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<% 
												if((new Byte((sv.slt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().
												isScreenProtected())){ 
											%>
									<% }else {%>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.slt.getLength()%>'
											value='<%= sv.slt.getFormData() %>'
											size='<%=sv.slt.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(Sjl59screensfl.slt)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="Sjl59screensfl" 
													+ "." + "slt" + "_R" + count %>'
											id='<%="Sjl59screensfl" + "." + "slt" + "_R" + count %>'
											class="input_cell"
											style="width: 
													<%=sv.slt.getLength()*12%> px;">
									</div>
									<%}%>
									<%}%>
									<td style="text-align: center;"><%=count%></td>
									<td style="text-align: left;">
										<%if((new Byte((sv.clntsel).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.clntsel.getFormData();
												%>
										<div id="Sjl59screensfl.clntsel_R<%=count%>"
											name="Sjl59screensfl.clntsel_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.cltname).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.cltname.getFormData();
												%>
										<div id="Sjl59screensfl.cltname_R<%=count%>"
											name="Sjl59screensfl.cltname_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.levelno).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.levelno.getFormData();
												%>
										<div id="Sjl59screensfl.levelno_R<%=count%>"
											name="Sjl59screensfl.levelno_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.leveldes).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.leveldes.getFormData();
												%>
										<div id="Sjl59screensfl.leveldes_R<%=count%>"
											name="Sjl59screensfl.leveldes_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.agtdesc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.agtdesc.getFormData();
												%>
										<div id="Sjl59screensfl.agtdesc_R<%=count%>"
											name="Sjl59screensfl.agtdesc_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.userid).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.userid.getFormData();
												%>
										<div id="Sjl59screensfl.userid_R<%=count%>"
											name="Sjl59screensfl.userid_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.status).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.status.getFormData();
												%>
										<div id="Sjl59screensfl.status_R<%=count%>"
											name="Sjl59screensfl.status_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.regclass).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.regclass.getFormData();
												%>
										<div id="Sjl59screensfl.regclass_R<%=count%>"
											name="Sjl59screensfl.regclass_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.regdateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.regdateDisp.getFormData();
												%>
										<div id="Sjl59screensfl.regdateDisp_R<%=count%>"
											name="Sjl59screensfl.regdateDisp_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.reasonreg).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.reasonreg.getFormData();
												%>
										<div id="Sjl59screensfl.reasonreg_R<%=count%>"
											name="Sjl59screensfl.reasonreg_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="text-align: left;">
										<%if((new Byte((sv.resndetl).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.resndetl.getFormData();
												%>
										<div id="Sjl59screensfl.resndetl_R<%=count%>"
											name="Sjl59screensfl.resndetl_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
								</tr>

								<%
									count = count + 1;
									Sjl59screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" to ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" of ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle(" entries ")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	      <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl59Table').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
		language: {
			"lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
			"info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
			"sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
			"sEmptyTable": dtmessage,
			"paginate": {
				"next":       nextval,
				"previous":   previousval
			}
		},
		stateSave: true,
		"fnInfoCallback": function( settings, iStart, iEnd, iMax, iTotal, sPre ) {
			var iTotal = $('#totalRecords').val();
			return showval + " " + iStart + " " + toval+ " " + iEnd + " " + ofval + " " + iTotal + " " + entriesval;
		},
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>	