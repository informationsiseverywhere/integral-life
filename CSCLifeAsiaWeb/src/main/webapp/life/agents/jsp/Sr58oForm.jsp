<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR58O";
%>
<%@ include file="/POLACommon3NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sr58oScreenVars sv = (Sr58oScreenVars) fw.getVariables();
%>
<%
	{
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	StringData COMPANY_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company");
 %> <%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.company, (sv.company.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width").replace(" bold", "")
					.replace("input_cell", "bold_cell")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	StringData TABL_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table");
 %> <%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.tabl, (sv.tabl.getLength() + 1), null).replace("absolute",
					"relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	StringData ITEM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item");
 %> <%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<div style="width: 150px;" class="input-group"><%=smartHF.getRichText(0, 0, fw, sv.item, (sv.item.getLength() + 1), null).replace("absolute",
					"relative")%>
						<%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.longdesc.getLength() + 1), null)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	StringData ITMFRM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Dates Effective");
 %> <%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<table>
						<tr>
							<td><%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp, (sv.itmfrmDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%></td>
							<td>&nbsp;</td>
							<td>
								<%
									StringData ITMTO_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
								%> <%=smartHF.getLit(0, 0, ITMTO_LBL).replace("absolute;", "relative; font-weight: bold;")%></td>
							<td>&nbsp;</td>
							<td><%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp, (sv.itmtoDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label> <%
 	StringData Sr58o_Quarterly_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
 			"Quarterly Accumulated First Year Commission (Up To)");
 %> <%=smartHF.getLit(0, 0, Sr58o_Quarterly_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label> <%
 	StringData Sr58o_Quarterly_Bonu_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
 			"Quarterly Bonus Percentage");
 %> <%=smartHF.getLit(0, 0, Sr58o_Quarterly_Bonu_LBL).replace("absolute;",
					"relative; font-weight: bold;")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt01, (sv.totamnt01.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt01, (sv.prcnt01.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt02, (sv.totamnt02.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt02, (sv.prcnt02.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt03, (sv.totamnt03.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt03, (sv.prcnt03.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt04, (sv.totamnt04.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt04, (sv.prcnt04.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt05, (sv.totamnt05.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt05, (sv.prcnt05.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF.getRichText(0, 0, fw, sv.totamnt06, (sv.totamnt06.getLength() + 1),
					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.prcnt06, (sv.prcnt06.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							StringData Sr58o_Persistency_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"Persistency");
						%>
						<%=smartHF.getLit(0, 0, Sr58o_Persistency_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							StringData Sr58o_Passed_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Passed");
						%>
						<%=smartHF.getLit(0, 0, Sr58o_Passed_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							StringData Sr58o_Not_Passed_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"Not Passed");
						%>
						<%=smartHF.getLit(0, 0, Sr58o_Not_Passed_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>
						<%
							StringData Sr58o_Modifier_Perce_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"Modifier Percentage");
						%>
						<%=smartHF.getLit(0, 0, Sr58o_Modifier_Perce_LBL).replace("absolute;",
					"relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.oppc01, (sv.oppc01.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 150px;"><%=smartHF
					.getRichText(0, 0, fw, sv.oppc02, (sv.oppc02.getLength() + 1), COBOLHTMLFormatter.DECIMAL_NOSIGN)
					.replace("absolute", "relative")%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>

<%@ include file="/POLACommon4NEW.jsp"%>

