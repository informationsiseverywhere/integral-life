    <%@ page language="java" pageEncoding="UTF-8" %>
   <%@ page contentType="text/html; charset=UTF-8" %>
      <%String screenName = "SM507";%>
<%@ include file="/POLACommon1NEW.jsp"%>
   <%@ page import="com.csc.life.agents.screens.*" %>
    <%Sm507ScreenVars sv = (Sm507ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date appointed ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Exclusive?");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Area code ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reporting to ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"______________________________________________________________________________");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New Agent Type ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reporting to ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Downline Agents transfer to ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Parallel ORC applicable ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Parallel Agent Number ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Parallel Agent Type   ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"/");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Parallel ORC % ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Government Treaty Number");%>
<%{
		if (appVars.ind01.isOn()) {
			sv.accountType.setReverse(BaseScreenData.REVERSED);
			sv.accountType.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.accountType.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.repsel.setReverse(BaseScreenData.REVERSED);
			sv.repsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.repsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		    if (appVars.ind04.isOn()) {
			sv.agntfm.setReverse(BaseScreenData.REVERSED);
			sv.agntfm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind44.isOn()) {
			sv.agntfm.setEnabled(BaseScreenData.DISABLED);
			sv.agntfm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind04.isOn()) {
			sv.agntfm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.descn.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind47.isOn()) {
			generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind47.isOn()) {
			sv.mlprcind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.mlprcind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.currfromDisp.setReverse(BaseScreenData.REVERSED);
			sv.currfromDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.currfromDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			sv.currfromDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.currfromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.currtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.currtoDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.currtoDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			sv.currtoDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.currtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.mlparorc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			sv.mlparorc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.agntsel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			sv.agntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind47.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


	<div class="panel panel-default">
    	


    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		<table>
					    		<tr>
					    		<td>
					    		<%					
		                             if(!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
						            	if(longValue == null || longValue.equalsIgnoreCase("")) {
								        formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							            } else {
								         formatValue = formatValue( longValue);
						 	             }
							
							
					                } else  {
								
					                    if(longValue == null || longValue.equalsIgnoreCase("")) {
							         	formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							            } else {
								        formatValue = formatValue( longValue);
							       }
					
					              }
				            	%>			
				        <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						   "blank_cell" : "output_cell" %>'>
				          <%=XSSFilter.escapeHtml(formatValue)%>
			          </div>	
		                  <%
		                   longValue = null;
		                   formatValue = null;
		                   %>
					    </td>
					    <td>
			                       <%					
		                             if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							            if(longValue == null || longValue.equalsIgnoreCase("")) {
							         	formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							            } else {
								         formatValue = formatValue( longValue);
							            }
							
							
					                } else  {
								
					                    if(longValue == null || longValue.equalsIgnoreCase("")) {
								        formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							             } else {
							           	formatValue = formatValue( longValue);
							             }
					
					                    }
					                  %>			
				     <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 216px;">
				         <%=XSSFilter.escapeHtml(formatValue)%>
			          </div>	
		   <%
		longValue = null;
		formatValue = null;
		%>
  	          		</td>
  	          		</tr>
  	          		</table>
				    </div>
			    </div>
			           
			           
			    	   <div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>
					    		
		                       <%					
	                             if(!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							        if(longValue == null || longValue.equalsIgnoreCase("")) {
								    formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							        } else {
							    	formatValue = formatValue( longValue);
							        }
							
							
					              } else  {
								
					                if(longValue == null || longValue.equalsIgnoreCase("")) {
								    formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							        } else {
								    formatValue = formatValue( longValue);
							         }
					
					               }
					             %>			
				                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						           "blank_cell" : "output_cell" %>'>
				                   <%=XSSFilter.escapeHtml(formatValue)%>
			                    </div>	
		                 <%
		                   longValue = null;
		                   formatValue = null;
		                  %>  					    		
				      		</div>
				    	</div>
				    	
				    	<div class="col-md-2"> </div>
				    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>
					    		<div class="input-group">  
					    					<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	          		      			   			
					    
				    		</div></div>
			    	</div>
			    	</div>
			    	
			    	<div class="row">	
			    	  <div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					    		<div class="input-group">
					    		<%					

		if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}
					} else  {

								

					if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}
					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>'>

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	

		<%

		longValue = null;

		formatValue = null;

		%>
				      		</div></div>
				      </div>
			        
			         <div class="col-md-2"></div>
			    	  <div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Area code")%></label>
					    		<div class="input-group">
		<%					

		if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}
					} else  {

								

					if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}
					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>'>

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	

		<%

		longValue = null;

		formatValue = null;

		%>
				      		</div></div>
				      </div>
				      
				        <div class="col-md-2"></div>
				    <div class="col-md-4">
        			<div class="form-group">
        				<label><%=resourceBundleHandler.gettingValueFromBundle("Exclusive?")%>
        				
        				<input type='checkbox' name='exclAgmt' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(exclAgmt)' onKeyUp='return checkMaxLength(this)'    
							<%
							
							if((sv.exclAgmt).getColor()!=null){
										 %>style='background-color:#FF0000;'
									<%}
									if((sv.exclAgmt).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }%>
									disabled
							class ='UICheck' onclick="handleCheckBox('exclAgmt')"/>
							
							<input type='checkbox' name='exclAgmt' value=' ' 
							
							<% if(!(sv.exclAgmt).toString().trim().equalsIgnoreCase("Y")){
										%>checked
									
							      <% }%>

							style="visibility: hidden" onclick="handleCheckBox('exclAgmt')"/>

                       </label>
                      </div>
			    	
			        </div></div>
				    	
				    	<div class="row">
				    	<div class="col-md-2"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Date appointed")%></label>
				    		<div class="input-group"> 	  
					    		<%					
		          if(!((sv.dteappDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		    		      			   				    		
				      		</div>
				      </div>
				      </div>
				 
				       <div class="col-md-2"> </div>
			    	  <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Reporting to")%></label>
					    	<table><tr><td >
					    		
		<%					

		if(!((sv.reportag.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.reportag.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

							
					} else  {
                     if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.reportag.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>' style="min-width: 70px;">

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	

		<%

		longValue = null;

		formatValue = null;

		%>

  </td><td>

		<%					

		if(!((sv.reportton.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.reportton.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

							

							

					} else  {

								

					if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.reportton.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

					

					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 200px;margin-left: 1px;">

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	

		<%

		longValue = null;

		formatValue = null;

		%>
				      		</td></tr></table>
				      	</div>
				      </div>
			        </div>
	    
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group"  >  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("New Agent Type")%></label>
					    		<div class="input-group" style="width:200px;">
					    		
					    		<%	

	fieldItem=appVars.loadF4FieldsLong(new String[] {"accountType"},sv,"E",baseModel);

	mappedItems = (Map) fieldItem.get("accountType");

	optionValue = makeDropDownList( mappedItems , sv.accountType.getFormData(),2,resourceBundleHandler);  

	longValue = (String) mappedItems.get((sv.accountType.getFormData()).toString().trim());  

%>

<!--  Ilife- Life Cross Browser - Sprint 4 D1 : Task 1 START-->

<%=smartHF.getDropDownExt(sv.accountType, fw, longValue, "accountType", optionValue,0,210)%>



					      	        
			                     </div> 
				    		</div>
			    	</div>
			   
			    	<div class="col-md-4"> 
				   
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Reporting to")%></label>
				    	<table>
				    	<tr>
				    	<td class="input-group">
						<%
				          if ((new Byte((sv.repsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>          
						
							<%=smartHF.getHTMLVarExt(fw, sv.repsel)%>	                                         
				      
				        <%
							} else {
						%>
				        <div class="input-group" style="width: 115px;">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.repsel)%>
				             <span class="input-group-btn">
				             <button class="btn btn-info" type="button"
				                onClick="doFocus(document.getElementById('repsel')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
				        <%
				         }
				        %>   
						
						</td>
						<td>
							<%
								if(!((sv.repname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
											
						
													if(longValue == null || longValue.equalsIgnoreCase("")) {
						
														formatValue = formatValue( (sv.repname.getFormData()).toString()); 
						
													} else {
						
														formatValue = formatValue( longValue);
						
													}
						
													
						
													
						
											} else  {
						
														
						
											if(longValue == null || longValue.equalsIgnoreCase("")) {
						
														formatValue = formatValue( (sv.repname.getFormData()).toString()); 
						
													} else {
						
														formatValue = formatValue( longValue);
						
													}
						
											
						
											}
						
											%>			
						
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						
												"blank_cell" : "output_cell" %>' style="min-width: 100px;max-width: 250px;">
						
										<%=XSSFilter.escapeHtml(formatValue)%>
						
									</div>
						
								<%
						
								longValue = null;
						
								formatValue = null;
						
								%>
							</td>
							</tr>
							</table>
								
							</div>
						
				    	</div>
			
			    	  
   <div class="col-md-3"> 
				    		  	 
		 <div class="form-group">		    							    		<% 
	if((new Byte((sv.agntfm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) != 0){ 
%> 
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Downline Agents transfer to")%></label>
 
					    		<div class="input-group">
					    		
		<%if ((new Byte((sv.agntfm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
								<%
									longValue = sv.agntfm.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.agntfm).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
								<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "output_cell" %>'   style="min-width:180px;">  
									   		<%if(longValue != null){%>
									   		
									   		<%=longValue%>
									   		
									   		<%}%>
									   </div>
								
								<%
								longValue = null;
								%>
								<% }else {%> 
								<input name='agntfm' id='agntfm'
								type='text' 
								value='<%=sv.agntfm.getFormData()%>' 
								maxLength='<%=sv.agntfm.getLength()%>' 
								size='<%=sv.agntfm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(agntfm)' onKeyUp='return CheckMaxLengthAndChangeCase(agntfm, this)'  
								onblur = 'ChangeCase(agntfm)'
								<% 
									if((new Byte((sv.agntfm).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
								readonly="true"
								class="output_cell"	 >
								<%
								}else if((new Byte((sv.agntfm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									
								%>	
								class="bold_cell" >
								<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('agntfm'));doAction('PFKEY04')">
					        		<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        		</button>
					      		</span>
								
								<%
									}else { 
								%>
								
								class = ' <%=(sv.agntfm).getColor()== null  ? 
								"input_cell" :  (sv.agntfm).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>' >
								<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('agntfm'));doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
								
								<%}longValue = null;}} %>
								
				    		

	 <%					

		if(!((sv.descn.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.descn.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

							

							

					} else  {

								

					if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.descn.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

					

					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>' style="min-width:100px;">

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>

		<%

		longValue = null;

		formatValue = null;

		%> 

<%} %>
</class>
 </div>
 </div>
 </div>
 </div>
 
                 

 <div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>


<div class="label_txt">

<%=resourceBundleHandler.gettingValueFromBundle("______________________________________________________________________________")%>

</div>


<br/>


	

  		

		<%					

		if(!((sv.tydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	

					

							if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.tydesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

							

							

					} else  {

								

					if(longValue == null || longValue.equalsIgnoreCase("")) {

								formatValue = formatValue( (sv.tydesc.getFormData()).toString()); 

							} else {

								formatValue = formatValue( longValue);

							}

					

					}

					%>			

				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>'>

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	

		<%

		longValue = null;

		formatValue = null;

		%>

  

	


</td>
<td>
<%					
		if(!((sv.acctype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
<%					
		if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

</td>
<td>
<%					
		if(!((sv.agntbr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntbr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntbr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.aracde.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.aracde.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.aracde.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>

<td width='188'>


<div class="label_txt">

<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>

</div>

</tr></table></div>
			</div>   
			 </div>



<%@ include file="/POLACommon2NEW.jsp"%>