<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL58";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl58ScreenVars sv = (Sjl58ScreenVars) fw.getVariables();%>

<%{
	 	if (appVars.ind01.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.brnchcd.setReverse(BaseScreenData.REVERSED);
			sv.brnchcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.brnchcd.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind40.isOn()) {
			sv.brnchcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.aracde.setReverse(BaseScreenData.REVERSED);
			sv.aracde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.aracde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.aracde.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.leveltype.setReverse(BaseScreenData.REVERSED);
			sv.leveltype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.leveltype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.leveltype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.saledept.setReverse(BaseScreenData.REVERSED);
			sv.saledept.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.saledept.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.saledept.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.agtype.setReverse(BaseScreenData.REVERSED);
			sv.agtype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.agtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.agtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.uplevelno.setReverse(BaseScreenData.REVERSED);
			sv.uplevelno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.uplevelno.setHighLight(BaseScreenData.BOLD);
		}  
	 	if (appVars.ind45.isOn()) {
			sv.uplevelno.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind46.isOn()) {
			sv.uplevelno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.regdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.regdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.regdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.regdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.regdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.reasonreg.setReverse(BaseScreenData.REVERSED);
			sv.reasonreg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.reasonreg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.reasonreg.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.reasonreg.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind21.isOn()) {
			sv.resndetl.setReverse(BaseScreenData.REVERSED);
			sv.resndetl.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.resndetl.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.moddateDisp.setReverse(BaseScreenData.REVERSED);
			sv.moddateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.moddateDisp.setHighLight(BaseScreenData.BOLD);
		}  
	 	if (appVars.ind30.isOn()) {
			sv.moddateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind31.isOn()) {
			sv.moddateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.deldateDisp.setReverse(BaseScreenData.REVERSED);
			sv.deldateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.deldateDisp.setHighLight(BaseScreenData.BOLD);
		}  
		if (appVars.ind32.isOn()) {
			sv.deldateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind33.isOn()) {
			sv.deldateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.reasonmod.setReverse(BaseScreenData.REVERSED);
			sv.reasonmod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.reasonmod.setHighLight(BaseScreenData.BOLD);
		}  
		if (appVars.ind34.isOn()) {
			sv.reasonmod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind35.isOn()) {
			sv.reasonmod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind25.isOn()) {
			sv.reasondel.setReverse(BaseScreenData.REVERSED);
			sv.reasondel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.reasondel.setHighLight(BaseScreenData.BOLD);
		}  
		if (appVars.ind36.isOn()) {
			sv.reasondel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind37.isOn()) {
			sv.reasondel.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}%>
	<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.clntsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='clntsel' id='clntsel'
			type='text' 
			value='<%=sv.clntsel.getFormData()%>' 
			maxLength='<%=sv.clntsel.getLength()%>' 
			size='<%=sv.clntsel.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(clntsel)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.clntsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:111px" >
			
			<%
				}else if((new Byte((sv.clntsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:72px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.clntsel).getColor()== null  ? 
			"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:72px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 103px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
				
			
				<div class="col-md-3" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.brnchcd.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.brnchcd).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='brnchcd' id='brnchcd'
			type='text' 
			value='<%=sv.brnchcd.getFormData()%>' 
			maxLength='<%=sv.brnchcd.getLength()%>' 
			size='<%=sv.brnchcd.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(brnchcd)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.brnchcd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.brnchcd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.brnchcd).getColor()== null  ? 
			"input_cell" :  (sv.brnchcd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('brnchcd')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.brnchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.brnchdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 120px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		

		</div>
		
		<!-- 2nd row -->
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Level Number "))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
				  <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.leveltype.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.leveltype).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='leveltype' id='leveltype'
			type='text' 
			value='<%=sv.leveltype.getFormData()%>' 
			maxLength='<%=sv.leveltype.getLength()%>' 
			size='<%=sv.leveltype.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(leveltype)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.leveltype).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.leveltype).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.leveltype).getColor()== null  ? 
			"input_cell" :  (sv.leveltype).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('leveltype')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.leveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 135px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>

		
		<div class="col-md-5">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.saledept).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='saledept' id='saledept'
			type='text' 
			value='<%=sv.saledept.getFormData()%>' 
			maxLength='<%=sv.saledept.getLength()%>' 
			size='<%=sv.saledept.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(saledept)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.saledept).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.saledept).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.saledept).getColor()== null  ? 
			"input_cell" :  (sv.saledept).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	</div>
			
			<!-- 3rd row -->
			
	<div class="row">
	       
	       
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
						<div class="input-group">
							<% if ((new Byte((sv.agtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"agtype"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("agtype");
								optionValue = makeDropDownList( mappedItems , sv.agtype.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.agtype.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.agtype, fw, longValue, "agtype", optionValue) %>
							<%}%>
						</div>
				</div>
			</div>
	
	
			
		<div class="col-md-3">
		 <%	 if (!(appVars.ind46.isOn())){ %>
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Upper Level Number")%></label>
   					 <table><tr><td style="min-width:10px" class="form-group">
					<div class="input-group" style="min-width:80px">  
						<%	
							longValue = sv.uplevelno.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.uplevelno).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
						<%
						longValue = null;
						%>
						<% }else {%> 
					<input name='uplevelno' id='uplevelno'
					type='text' 
					value='<%=sv.uplevelno.getFormData()%>' 
					maxLength='<%=sv.uplevelno.getLength()%>' 
					size='<%=sv.uplevelno.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(uplevelno)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.uplevelno).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.uplevelno).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
		               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('uplevelno')); doAction('PFKEY04')">
		                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		               </button>
		             </span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.uplevelno).getColor()== null  ? 
					"input_cell" :  (sv.uplevelno).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
		               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('uplevelno')); doAction('PFKEY04')">
		                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		               </button>
		             </span>
					
					<%}longValue = null;} %>			
				</div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.upleveldes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.upleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.upleveldes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 100px;">
							<%=formatValue%>
							</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		<%}%></div>
		
	
	
		<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 120px; max-width:140px">

						<%	
							longValue = sv.status.getFormData();  
						%>
				

						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
						<%
							longValue = null;
						%>
					</div>
				</div>
			</div>
	
	</div>
	
	<!-- 4th row -->

	<div class="row">
			
			<%	 if (!(appVars.ind27.isOn())) { %>  
							 <div class="col-md-3" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Registration Date"))%></label>
					<%
						if ((new Byte((sv.regdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.regdateDisp, (sv.regdateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="regdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.regdateDisp, (sv.regdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>

				</div>
			</div>
			<%}%>
			
			<%	 if (!(appVars.ind31.isOn())) { %>  
							 <div class="col-md-3" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Modify Date"))%></label>
					<%
						if ((new Byte((sv.moddateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.moddateDisp, (sv.moddateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="moddateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.moddateDisp, (sv.moddateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>

				</div>
			</div>
			<%}%>
			
			<%	 if (!(appVars.ind33.isOn())) { %>  
							 <div class="col-md-3" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Delete Date"))%></label>
					<%
						if ((new Byte((sv.deldateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.deldateDisp, (sv.deldateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="deldateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.deldateDisp, (sv.deldateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>

				</div>
			</div>
			<%}%>
			
				<%	 if (!(appVars.ind29.isOn())) { %>  
        		<div class="col-md-3 ">
        			<div class="form-group">
        			
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Registration"))%></label>
        				<div class="input-group">
        					<% if ((new Byte((sv.reasonreg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasonreg"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasonreg");
								optionValue = makeDropDownList( mappedItems , sv.reasonreg.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasonreg.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasonreg, fw, longValue, "reasonreg", optionValue) %>
							</div>
        			</div>
        		</div>
        		<%}%>
			<%}%>
			
			<%	 if (!(appVars.ind35.isOn())) { %>  
        		<div class="col-md-3 ">
        			<div class="form-group">
        			
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Modify"))%></label>
        				<div class="input-group">
        					<% if ((new Byte((sv.reasonmod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasonmod"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasonmod");
								optionValue = makeDropDownList( mappedItems , sv.reasonmod.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasonmod.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasonmod, fw, longValue, "reasonmod", optionValue) %>
							</div>
        			</div>
        		</div>
        		<%}%>
			<%}%>
			
			<%	 if (!(appVars.ind37.isOn())) { %>  
        		<div class="col-md-3 ">
        			<div class="form-group">
        			
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Delete"))%></label>
        				<div class="input-group">
        					<% if ((new Byte((sv.reasondel).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasondel"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasondel");
								optionValue = makeDropDownList( mappedItems , sv.reasondel.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasondel.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasondel, fw, longValue, "reasondel", optionValue) %>
							</div>
        			</div>
        		</div>
        		<%}%>
			<%}%>
				
				
				<%	 if ((!appVars.ind29.isOn()) || (!appVars.ind35.isOn()) || (!appVars.ind37.isOn())){ %>	
				<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reason Details")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 450px; max-width:600px">
					<input name='resndetl' type='text'
						<%formatValue = (sv.resndetl.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.resndetl.getLength()%>'
						maxLength='<%=sv.resndetl.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(resndetl)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.resndetl).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.resndetl).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.resndetl).getColor() == null
						? "input_cell"
						: (sv.resndetl).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
					</div></div>
				</div>
				<%}%>
			</div>	
		</div>
	</div>




<%@ include file="/POLACommon2NEW.jsp"%>