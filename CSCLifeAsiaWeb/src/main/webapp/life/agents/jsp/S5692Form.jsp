<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5692";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

 <%S5692ScreenVars sv = (S5692ScreenVars) fw.getVariables();%>


	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>

	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>

	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>

	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>

	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To age ");%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comm");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To age ");%>

	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comm");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial");%>

	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial");%>

	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal");%>

	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal");%>

	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To age ");%>

	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To age ");%>

	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial");%>

	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal");%>

	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal");%>

<div class="panel panel-default">	
    <div class="panel-body">
		<div class="row">
			<div class="col-md-2">
    	 		<div class="form-group"> 	
    	 		     <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>        				    			  
					   <div class="input-group">
						   <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      </div>
				</div>
		  </div>
		  <div class="col-md-2"></div>
		
  	<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>

            <div class="col-md-2"></div>
            
            
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>
<div class="row">
	
	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>

					<table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>



					<td><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;to &nbsp;&nbsp;")%></td>


		<td>				<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td>
</tr>
</table>
<!-- 					</div> -->
				</div>
			</div>

</div>
 
<br><br>
 <div class="row">
   	<div class="col-md-1"> 
    &nbsp;   &nbsp;
    <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Age"))%></label> 
     
           </div>
     
    <div class="col-md-1" style="min-width:100px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
    </div>
    </div>
    </div>
  
    	<div class="col-md-1"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" % of Comm"))%></label>
     </div>
    
        
    	<div class="col-md-1" style="max-width:110px;text-align: center;">  
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("% of Prem"))%></label>
     </div>
     <div class="col-md-1" style="min-width:120px;"></div>
     
     	<div class="col-md-1"> 
     	  &nbsp;   &nbsp;
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Age"))%></label>
     </div>
    
    <div class="col-md-1" style="min-width:100px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
         </div>
        </div>
     </div>
     
   <div class="col-md-1"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" % of Comm"))%></label>
     </div>
    
   
     	<div class="col-md-1" style="max-width:100px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" % of Prem"))%></label>
     </div>
     
    </div>
    <br>
    <div class="row">
      <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Initial"))%></label>
     </div>
     
        <div class="col-md-1"></div>
   <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						   <%=smartHF.getHTMLVarExt(fw, sv.incmrate01, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
       
        <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						   <%=smartHF.getHTMLVarExt(fw, sv.inprempc01, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
        
          <div class="col-md-1" style="max-width:110px;"></div>
        <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Initial"))%></label>
     </div>
     <div class="col-md-1"></div>
      
        <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.incmrate02, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
         
          <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.inprempc02, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
         
     </div>
     
     <div class="row">
      <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Renewal"))%></label>
     </div>
     
        <div class="col-md-1"></div>
        <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.rwcmrate01, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
          <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group">
						  <%=smartHF.getHTMLVarExt(fw, sv.reprempc01, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
        
            <div class="col-md-1" style="max-width:110px;"></div>
        <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Renewal"))%></label>
     </div>
     <div class="col-md-1"></div>
      
       <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.rwcmrate02, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
          <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.reprempc02, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
         
     </div>
   
     <div class="row">
       
    <div class="col-md-1">
      &nbsp;   &nbsp;
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Age"))%></label>
     </div>
     
     <div class="col-md-1" style="min-width:100px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						   <%=smartHF.getHTMLVarExt(fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
    </div>
    </div>
    </div>
  
     <div class="col-md-1"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" "))%></label>
     </div>
    
   
     <div class="col-md-1" style="max-width:110px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" "))%></label>
     </div>
       
    <div class="col-md-1" style="min-width:120px;"></div>
    <div class="col-md-1">
      &nbsp;   &nbsp;
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Age"))%></label>
     </div>
     
     <div class="col-md-1" style="min-width:100px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
         </div>
        </div>
     </div>
     
    <div class="col-md-1" style="max-width:130px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(" "))%></label>
     </div>
    
   
    <div class="col-md-1" style="max-width:100px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
     </div>
     
    </div>
    <br>
    <div class="row">
      <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Initial"))%></label>
     </div>
     
        <div class="col-md-1"></div>
      <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.incmrate03, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
         <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.inprempc03, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
        
            <div class="col-md-1" style="max-width:110px;"></div>
        <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Initial"))%></label>
     </div>
     <div class="col-md-1"></div>
      
         <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					  <div class="input-group" style="min-width:70px;">
						   <%=smartHF.getHTMLVarExt(fw, sv.incmrate04, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
           <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					  <div class="input-group" style="min-width:70px;">
						  	<%=smartHF.getHTMLVarExt(fw, sv.inprempc04, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
         
     </div>
     
     <div class="row">
      <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Renewal"))%></label>
     </div>
     
        <div class="col-md-1"></div>
      <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						   <%=smartHF.getHTMLVarExt(fw, sv.rwcmrate03, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
         <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.reprempc03, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
        
          <div class="col-md-1" style="max-width:110px;"></div>
        <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Renewal"))%></label>
     </div>
     <div class="col-md-1"></div>
      
       <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.rwcmrate04, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
       </div> 
         
         <div class="col-md-1" style="min-width:110px;">
     <div class="form-group"> 				  
					   <div class="input-group" style="min-width:70px;">
						  <%=smartHF.getHTMLVarExt(fw, sv.reprempc04, COBOLHTMLFormatter.S3VS2)%>
         </div>
        </div>
        </div>
         
     </div>
    
    </div>
    </div>
      

<%@ include file="/POLACommon2NEW.jsp"%>

