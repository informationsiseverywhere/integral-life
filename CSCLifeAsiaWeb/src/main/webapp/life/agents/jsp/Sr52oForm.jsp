<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52O";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr52oScreenVars sv = (Sr52oScreenVars) fw.getVariables();%>

<%{
if (appVars.ind02.isOn()) {
	sv.packdateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
	sv.packdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind03.isOn()) {
	sv.remdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind05.isOn()) {
	sv.deemdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind07.isOn()) {
	sv.nextActDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>


<div class="panel panel-default">
   <div class="panel-body">     
	<div class="row">	
		<div class="col-md-4"> 
			<div class="form-group">  	  
			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
			<table><tr>
			<td>
				<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
				</td>
				<td>

				<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
				</td>
				<td>
				
				<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
				style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
  	
		</td>
		</tr>
		</table>
	</div>	
	</div>
	<div class="col-md-4"></div>	   
			   	
	<div class="col-md-4"> 
		<div class="form-group">  	  
			<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					  <!--<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.register.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
					}						
				}else{
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
					}else{
						formatValue = formatValue( longValue);
					}
				}
			%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			<%}%>-->

		<%	
			fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("register");
			longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
		%>
	
    
		  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 120px;">  
		   		<%if(longValue != null){%>
		   		
		   		<%=longValue%>
		   		
		   		<%}%>
		   </div>
		<%
		longValue = null;
		formatValue = null;
		%>	
		
		</div>
	</div>
			    	
</div>
			       
<div class="row">	
  <div class="col-md-4"> 
	<div class="form-group">  	  
	<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					    		
		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
		<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
		%>
		<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 145px;">
		<%=XSSFilter.escapeHtml(formatValue)%>
		</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
		<%}%>   		
	</div>
 </div>

	

	<div class="col-md-4"> 
		<div class="form-group"> 
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label> 	  
					<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>
	
	 	</div>
 	</div>
 				        	
	<div class="col-md-4"> 
	<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label> 	  
		<!--<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	<%}%>
					-->
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
					%>
					
				    
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 160px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
					<%
					longValue = null;
					formatValue = null;
					%>

		</div>
	</div>
</div>

<div class="row">
			        
	<div class="col-md-4"> 
		<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner ")%></label> 	  
		<table>
		<tr>
		<td>
					<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 100px;">
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>
			</td>
			<td>
			<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' 
			style="max-width: 300px;margin-left: 1px;">
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>

		</td>
		</tr>
		</table>
		</div>
	</div>
</div>
			        
			       
<div class="row">	
	<div class="col-md-4"> 
		<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Delivery Mode")%></label> 	 
		<table>
		<tr>
		<td> 
							<%if ((new Byte((sv.dlvrmode).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.dlvrmode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.dlvrmode.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.dlvrmode.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:auto;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		</td>
		<td>
					
					<%if ((new Byte((sv.shortds).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.shortds.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.shortds.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.shortds.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
					 style="max-width: 200px;margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>

			</td>
			</tr>
			</table>
		</div>
	</div>
			        
	
	<div class="col-md-4"> 
		<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Delivery Date")%></label> 	  
		
		<br/>
		<div  style="width: 120px;">
		<%=smartHF.getRichText(0, 0, fw, sv.despdateDisp,(sv.despdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
		<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.despdateDisp).replace("absolute","relative")%>
		</div>
		</div>
	</div>
</div>
			        
			        
<div class="row">	
	<div class="col-md-4"> 
		<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Acknowledgement Date")%></label> 
		
					
					
			    <% if ((new Byte((sv.packdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                      <div style="width:80px"> <%=smartHF.getRichTextDateInput(fw, sv.packdateDisp)%></div>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="packdateDisp"
                                  data-link-format="dd/mm/yyyy" >
                                         <%=smartHF.getRichTextDateInput(fw, sv.packdateDisp, (sv.packdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
					
					
 		</div>
	</div>
	
	
	<div class="col-md-3"> 
		<div class="form-group"> 
		<label><%=resourceBundleHandler.gettingValueFromBundle("Reminder Date ")%></label> 	
		<div style="width: 80px;"> 
			<%=smartHF.getRichText(0, 0, fw, sv.remdteDisp,(sv.remdteDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
			<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.remdteDisp).replace("absolute","relative")%> 
		</div> 
		</div>
	</div>
</div>
<div class="row">	
	<div class="col-md-4"> 
		<div class="form-group"> 
			<label><%=resourceBundleHandler.gettingValueFromBundle("Deemed Received Date")%></label> 
			<div style="width: 80px;">	  
			<%=smartHF.getRichText(0, 0, fw, sv.deemdateDisp,(sv.deemdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
		 	<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.deemdateDisp).replace("absolute","relative")%> 
		 	</div>
		</div>
	</div>
		
	<div class="col-md-4"> 
		<div class="form-group"> 
			<label><%=resourceBundleHandler.gettingValueFromBundle("Next Activity Date")%></label>
			<div style="width: 80px;">
			<%=smartHF.getRichText(0, 0, fw, sv.nextActDateDisp,(sv.nextActDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
	 		<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.nextActDateDisp).replace("absolute","relative")%> 
			</div>
		</div>
	</div>
</div>		        
			         
			      
</div>



</div>





<%@ include file="/POLACommon2NEW.jsp"%>