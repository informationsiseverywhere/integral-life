<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM509";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm509ScreenVars sv = (Sm509ScreenVars) fw.getVariables();%>
<%{
}%>


<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
    	 					<table><tr><td>
    	 					<%if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'
style="margin-left: 1px;" id="scheduleName">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</td>
</tr></table>

</div></div>

	
	  	<div class="col-md-4"> 
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
                <table><tr><td>
     <%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div style="margin-left: 1px;max-width: 100px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
</tr>
</table>

</div></div>






		<div class="col-md-4"> 
	        <div class="form-group">
    	 			<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	 			<div class="input-group">
                       <%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
                       <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute","relative")%>
</div></div></div></div>


<div class="row">	
<div class="col-md-4"> 
	        <div class="form-group">
    	 			<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                      <%if ((new Byte((sv.bcompany).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>



<div class="col-md-4"> 
	        <div class="form-group">
    	 			<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
                     <%if ((new Byte((sv.bbranch).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div>


		<div class="col-md-4"> 
	        <div class="form-group">
    	 			<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
                       <%if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div></div></div>



















<div class="row">	
<div class="col-md-4">
	<div class="form-group">
	  <label><%=resourceBundleHandler.gettingValueFromBundle("Production Period from")%></label>
	  
	  <table>
				    		<tr>
				    			<td>
				    				<%	
									longValue = sv.datefrmDisp.getFormData();  
									%>
									
									<% 
										if((new Byte((sv.datefrmDisp).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
									%>  
										 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
									<% }else {%> 
										<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datefrmDisp" data-link-format="dd/mm/yyyy">
						                    <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
											
									<% }%>
				    			</td>
				    			<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
				    			<td>
				    				<%	
									longValue = sv.datetoDisp.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.datetoDisp).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
									 <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
								<% }else {%> 
									<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy">
					                    <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
										<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					                </div>
										
								<% }%>
				    			</td>
				    		</tr>
				    	</table>
				    	
	
	
	<%-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="cltdobxDisp" data-link-format="dd/mm/yyyy">
	<%=smartHF.getRichText(0, 0, fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    </div>
   
   
   
    
    <span class="input-group-addon"><%=resourceBundleHandler.gettingValueFromBundle("to")%></span>
   



    <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="cltdobxDisp" data-link-format="dd/mm/yyyy">
    <%=smartHF.getRichText(0, 0, fw, sv.datetoDisp,(sv.datetoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    </div> --%>
  <!--  </div> -->
 </div>
   </div>
 </div>


<div class="row">	
<div class="col-md-4">
<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
	  <table><tr><td>
	  <%if(((BaseScreenData)sv.mlagttyp) instanceof StringBase) {%>
       <%=smartHF.getRichText(0,0,fw,sv.mlagttyp,( sv.mlagttyp.getLength()+1),null).replace("absolute","relative")%>
      <%}else if (((BaseScreenData)sv.mlagttyp) instanceof DecimalData){%>
      <%=smartHF.getHTMLVar(0, 0, fw, sv.mlagttyp, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
      <%}else {%>
          hello
        <%}%>
     </td>
     <td>  
       <%if ((new Byte((sv.agtydesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	  <% if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		 }
	    %>
      <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px;">
       <%=XSSFilter.escapeHtml(formatValue)%>
       </div>	
	      <%
	        longValue = null;
	        formatValue = null;
	      %>
       <%}%>
       </td>
       </tr>
       </table>

</div></div>
</div></div>



</div>
































<%-- 




<div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!-- ILIFE-853 START -- Screen Sm509 - Fixed UI Issues -->
<!--
<%StringData BSCHEDNUM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name/Number");%>
<%=smartHF.getLit(0, 0, BSCHEDNUM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%>
</div>
<br/>
<%if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData ACCTMONTH_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month/Year");%>
<%=smartHF.getLit(0, 0, ACCTMONTH_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%>
</div>
<br/>
<%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData EFFDATE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
<%=smartHF.getLit(0, 0, EFFDATE_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.effdateDisp,(sv.effdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.effdateDisp).replace("absolute","relative")%></td>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
 <td width='251'>
<!--
<%StringData BCOMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, BCOMPANY_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<br/>
<%if ((new Byte((sv.bcompany).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bcompany.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bcompany.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
 
<td width='251'>
<!--
<%StringData BBRANCH_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch");%>
<%=smartHF.getLit(0, 0, BBRANCH_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Branch")%>
</div>
<br/>
<%if ((new Byte((sv.bbranch).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bbranch.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bbranch.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->

 
<td width='251'>
<!--
<%StringData JOBQ_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue");%>
<%=smartHF.getLit(0, 0, JOBQ_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%>
</div>
<br/>
<%if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData DATEFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Production Period from");%>
<%=smartHF.getLit(0, 0, DATEFRM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Production Period from")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 Start-->

<%=smartHF.getHTMLCalNSVar(0, -11.68, fw, sv.datefrmDisp).replace("absolute","relative")%>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
<%=smartHF.getRichText(0, 0, fw, sv.datetoDisp,(sv.datetoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>

<%=smartHF.getHTMLCalNSVar(0, -11.7, fw, sv.datetoDisp).replace("absolute","relative")%>

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 ends-->
</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData MLAGTTYP_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type");%>
<%=smartHF.getLit(0, 0, MLAGTTYP_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%>
</div>
<!-- ILIFE-853 END -- Screen Sm509 - Fixed UI Issues -->
<br/>
<%if(((BaseScreenData)sv.mlagttyp) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.mlagttyp,( sv.mlagttyp.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.mlagttyp) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.mlagttyp, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
<%if ((new Byte((sv.agtydesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
<td></td>
<td></td>
</tr> </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
