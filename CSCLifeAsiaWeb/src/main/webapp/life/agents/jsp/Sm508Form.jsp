<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM508";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm508ScreenVars sv = (Sm508ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent No  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Current Agent Type  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"______________________________________________________________________________");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Dt");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1st Level");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2nd Level");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3rd Level");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"______________________________________________________________________________");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
    	    
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent No")%></label>
					    		<table>
					    		<tr>
					    		<td>
					    		<%					
								if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
  							</td>
  							<td>
								<%					
								if(!((sv.desc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.desc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.desc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width: 300pc;min-width: 100px;margin-left: 1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
	

					      	    </td>
					      	    </tr>
					      	    </table>				    		
				      		</div>
				    </div>
				     <div class="col-md-3"> </div>
				
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Current Agent Type")%></label>
					    		<table>
					    		<tr>
					    		<td>
					    		
					    		  	<%					
									if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
								</td>
								<td>
					    		
		                      
								<%					
								if(!((sv.acdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.acdes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.acdes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 200px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  

				      		</td>
				      		</tr>
				      		</table>
				    	</div>
			    	</div>
			       </div>	
			   
<div class="row">
<div class="col-md-12">	
<div class="table-responsive">
	         <table id="dataTables-sm508" class="table table-striped table-bordered table-hover" width="100%" >
               <thead>
		
			        <tr class="info">
			         <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
				     <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>			
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>																			
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
			     	 <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
			     	 
		 	        </tr>
			 </thead>
			 

  
<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.agmvty.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*6;								
			} else {		
				calculatedValue = (sv.agmvty.getFormData()).length()*6;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.benCessDateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*6;								
			} else {		
				calculatedValue = (sv.benCessDateDisp.getFormData()).length()*6;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.reportag01.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
			} else {		
				calculatedValue = (sv.reportag01.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.reportag02.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
			} else {		
				calculatedValue = (sv.reportag02.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.reportag03.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.reportag03.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.accountType.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
			} else {		
				calculatedValue = (sv.accountType.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("sm508screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>

		
		<%

	/* String backgroundcolor="#FFFFFF"; */
	
	Sm508screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sm508screensfl
	.hasMoreScreenRows(sfl)) {
	
%>
  




<tbody>

<tr>
		<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;"
					<%if((sv.agmvty).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
										<%if((new Byte((sv.agmvty).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
												
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"agmvty"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("agmvty");
								longValue = (String) mappedItems.get((sv.agmvty.getFormData()).toString().trim());  
								if(longValue==null ||"".equals(longValue)){
								longValue=sv.agmvty.getFormData();
								}
							%>
							
							<%=longValue%>
							<%
								longValue = null;
								formatValue = null;
							%>
							
														 
				
									<%}%>
			</td>

				   <td style="width:<%=tblColumnWidth[1  ]%>px;">
											
						<%= sv.benCessDateDisp.getFormData()%>
						
														 
				
									</td>
				    									<td style="width:<%=tblColumnWidth[2  ]%>px;" > 
									
											
						<%= sv.reportag01.getFormData()%>
						
														 
				
									</td>
				    									<td style="width:<%=tblColumnWidth[3  ]%>px">									
														
						<%= sv.reportag02.getFormData()%>
						
														 
				
									</td>
				    									<td style="width:<%=tblColumnWidth[4  ]%>px;">									
															
						<%= sv.reportag03.getFormData()%>
						
														 
				
									</td>
				    									<td style="width:<%=tblColumnWidth[5  ]%>px;">									
															
												
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"accountType"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("accountType");
								longValue = (String) mappedItems.get((sv.accountType.getFormData()).toString().trim());  
							%>
							
							<%= sv.accountType.getFormData()%>&nbsp;-&nbsp;<%=longValue%>
							<%
								longValue = null;
								formatValue = null;
							%>
														 
				
									</td>
				
		
		
	</tr>


	<%
	/* if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
		backgroundcolor="#ededed";
		}else{
		backgroundcolor="#FFFFFF";
		} */
	count = count + 1;
	Sm508screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
		</table>
		</div>
	
	

<div style='visibility:hidden;'><table>
<tr>
<td>
		<%					
		if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td>
</tr>

</tbody>


</table>



</div>
</div>
</div>


</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTables-sm508').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '450px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

