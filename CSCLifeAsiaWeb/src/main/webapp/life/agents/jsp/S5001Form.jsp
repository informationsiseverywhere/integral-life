<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5001";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5001ScreenVars sv = (S5001ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Change Agent From       ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To        ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transfer Commission Type -");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Comm      ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial      ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal      ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing    ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Change Servicing Agent         ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.agentfrom.setReverse(BaseScreenData.REVERSED);
			sv.agentfrom.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.agentfrom.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.agentto.setReverse(BaseScreenData.REVERSED);
			sv.agentto.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.agentto.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.initflg.setReverse(BaseScreenData.REVERSED);
			sv.initflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.initflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rnwlflg.setReverse(BaseScreenData.REVERSED);
			sv.rnwlflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rnwlflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.servflg.setReverse(BaseScreenData.REVERSED);
			sv.servflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.servflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
	<div class="panel-body">
		<%-- lables row --%>
		<div class="row">
		<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
					
					<table><tr><td>
					<%if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	</td><td>
	<%if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="margin-left: 1px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="margin-left: 1px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

					
				</td></tr></table></div>
			</div>
			<%} %>
			<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
					<table><tr><td>
<%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</td>
<td>
<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="margin-left: 1px;max-width: 100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="margin-left: 1px;min-width: 100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%></td>
 </tr>
 </table>
 </div>
			
	</div>
	<%} %>
			<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
						<div class="input-group">
<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
				</div></div>
			</div><%} %>
		</div>
		

		<div class="row">
		<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Job Queue "))%></label>
					<%
						if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jobq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jobq.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%}%>

				</div>
			</div><%} %>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company "))%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bcompany" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bcompany");
						longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());
					%>
					<%-- 	<%=smartHF.getHTMLVarReadOnly(fw, sv.bcompany)%> --%>
					<!-- 	<span style="width: 1px;"></span> -->
					<div style="position: relative; margin-left: 1px;"
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=longValue%>
						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch "))%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bbranch" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bbranch");
						longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());
					%>
					<%-- 	<%=smartHF.getHTMLVarReadOnly(fw, sv.bbranch)%> --%>
					<!-- 	<span style="width: 1px;"></span> -->
					<div style="position: relative; margin-left: 1px;"
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
	   longValue = null;
	   formatValue = null;
	%>

				</div>
			</div>			
		</div>
		<%-- Agent change from  --%>
		<div class="row">
		<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-4">
				<!-- <div class="form-group"> -->
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Change Agent From"))%></label>
					
						<table>
							<tr>
								<td>
			
						
							<%
								if ((new Byte((sv.agentfrom).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>


							<input name='agentfrom' type='text'
								<%if ((sv.agentfrom).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%>
								<%formatValue = (sv.agentfrom.getFormData()).toString();%>
								value='<%=XSSFilter.escapeHtml(formatValue)%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=formatValue%>' <%}%>
								size='<%=sv.agentfrom.getLength()%>'
								maxLength='<%=sv.agentfrom.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(agentfrom)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.agentfrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.agentfrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.agentfrom).getColor() == null ? "input_cell"
							: (sv.agentfrom).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							<%
								}
							%>
							</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;To &nbsp;&nbsp;")%></td>
							<td>
							<div class="form-group">
							<%
								if ((new Byte((sv.agentto).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>


							<input name='agentto' type='text'
								<%if ((sv.agentto).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%>
								<%formatValue = (sv.agentto.getFormData()).toString();%>
								value='<%=XSSFilter.escapeHtml(formatValue)%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=formatValue%>' <%}%>
								size='<%=sv.agentto.getLength()%>'
								maxLength='<%=sv.agentto.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(agentto)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.agentto).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.agentto).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.agentto).getColor() == null ? "input_cell"
							: (sv.agentto).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							<%
								}
							%>
							</div>
							</td>
						</tr>
					</table>
						
					
				<!-- </div> -->
			</div><%
								}
							%>
		</div>


		<div class="row">
		<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Transfer Commission Type - "))%></label>
				</div>
			</div><%}%>
		</div>
		<div class="row">
		<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Initial")%></label>
				<%if ((new Byte((sv.initflg).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input  type='checkbox' name='initflg' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(initflg)' onKeyUp='return checkMaxLength(this)'   



<%

if((sv.initflg).getColor()!=null){
			 %>style='margin-left:0px;background-color:#FF0000;'
		<%}
else{%>
	style='margin-left:0px'; 
<%}
		if((sv.initflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.initflg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('initflg')"/>

<input type='checkbox' name='initflg' value='N' 

<% if(!(sv.initflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('initflg')"/>
<%}%>
			</div><%}%>
			
			
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Renewal")%></label>
					<%if ((new Byte((sv.rnwlflg).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input  type='checkbox' name='rnwlflg' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(rnwlflg)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.rnwlflg).getColor()!=null){
			 %>style='margin-left:0px;background-color:#FF0000;'
		<%}
else{%>
	style='margin-left:0px';
<%}
		if((sv.rnwlflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.rnwlflg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('rnwlflg')"/>

<input type='checkbox' name='rnwlflg' value='N' 

<% if(!(sv.rnwlflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('rnwlflg')"/>
<%}%>

			</div>
		</div>
		<div class="row">
		<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing")%></label>
				<%if ((new Byte((sv.servflg).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input  type='checkbox' name='servflg' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(servflg)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.servflg).getColor()!=null){
			 %>style='margin-left:0px;background-color:#FF0000;'
		<%}
else{%>
	style='margin-left:0px';
<%}
		if((sv.servflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.servflg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('servflg')"/>

<input type='checkbox' name='servflg' value='N' 

<% if(!(sv.servflg).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('servflg')"/>
<%}%>
			</div><%}%>
			
			<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Change Servicing Agent")%></label>
				<%if ((new Byte((sv.comind).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input  type='checkbox' name='comind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(comind)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.comind).getColor()!=null){
			 %>style='margin-left:0px;background-color:#FF0000;'
		<%}
else{%>
	style='margin-left:0px';
<%}
		if((sv.comind).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.comind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('comind')"/>

<input type='checkbox' name='comind' value='N' 

<% if(!(sv.comind).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('comind')"/>
<%}%>
			</div><%}%>
		</div>

	</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>