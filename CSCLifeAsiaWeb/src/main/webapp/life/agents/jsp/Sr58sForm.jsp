<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR58S";%>
<%@ include file="/POLACommon3.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr58sScreenVars sv = (Sr58sScreenVars) fw.getVariables();%>
<%{
}%>

<div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.company,(sv.company.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.tabl,( sv.tabl.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.item,( sv.item.getLength()+1),null).replace("absolute","relative")%>
<%=smartHF.getRichText(0,0,fw,sv.longdesc,( sv.longdesc.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
&nbsp;
<%StringData ITMTO_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>

<%=smartHF.getLit(0, 0, ITMTO_LBL).replace("absolute;","relative; font-weight: bold;")%>
&nbsp;
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
</td>
</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
 <tr style='height:22px;'>
 <td>
<%StringData Contot__LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"3 Months Accumulated FYC");%>
<%=smartHF.getLit(0, 0, Contot__LBL).replace("absolute;","relative; font-weight: bold;")%>
</td>
<td width='251'>
<%StringData Prcnt__LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"% MPB Rate");%>
<%=smartHF.getLit(0, 0, Prcnt__LBL).replace("absolute;","relative; font-weight: bold;")%>
</td>
 <td width='251'></td>
 <td width='251'>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot01,( sv.comtot01.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent01,( sv.prcent01.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot02,( sv.comtot02.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent02,( sv.prcent02.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot03,( sv.comtot03.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent03,( sv.prcent03.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot04,( sv.comtot04.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent04,( sv.prcent04.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot05,( sv.comtot05.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent05,( sv.prcent05.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot06,( sv.comtot06.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent06,( sv.prcent06.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot07,( sv.comtot07.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent07,( sv.prcent07.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.comtot08,( sv.comtot08.getLength()+1),COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.prcent08,( sv.prcent08.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr> 
<tr style='height:22px;'>
<td width='251'>
</td>
<td width='251'>
</td>
<td width='251'>
</td>
</tr>
<tr style='height:22px;'>
 <td width='251'>
<%StringData Criteria1__LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,">= PR criteria payment % : ");%>
<%=smartHF.getLit(0, 0, Criteria1__LBL).replace("absolute;","relative; font-weight: bold;")%>
</td>
<td width='251'>
<%StringData Criteria2__LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"< PR criteria payment % : ");%>
<%=smartHF.getLit(0, 0, Criteria2__LBL).replace("absolute;","relative; font-weight: bold;")%>
</td>
<td width='251'>
</td>
</tr>
<tr style='height:22px;'>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.znadjperc01,( sv.znadjperc01.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
<%=smartHF.getRichText(0,0,fw,sv.znadjperc02,( sv.znadjperc02.getLength()+1),COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("absolute","relative")%>
</td>
<td width='251'>
</td>
</tr>
</table>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_continue.png" width='84' height='22' alt='Continue'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_refresh.png" width='84' height='22' alt='Refresh'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_previous.png" width='84' height='22' alt='Previous'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_exit.png" width='84' height='22' alt='Exit'></a></li>
</ul>
</div>
<%@ include file="/POLACommon4.jsp"%>

