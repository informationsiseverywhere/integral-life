<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL71";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl71ScreenVars sv = (Sjl71ScreenVars) fw.getVariables();%>


<%{
	 	if (appVars.ind01.isOn()) {
			sv.agncysel.setReverse(BaseScreenData.REVERSED);
			sv.agncysel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agncysel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}  
		if (appVars.ind03.isOn()) {
			sv.userid.setReverse(BaseScreenData.REVERSED);
			sv.userid.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.userid.setHighLight(BaseScreenData.BOLD);
		}
		 if (appVars.ind04.isOn()) {
			sv.salerepno.setReverse(BaseScreenData.REVERSED);
			sv.salerepno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.salerepno.setHighLight(BaseScreenData.BOLD);
		} 
		
		if (appVars.ind05.isOn()) {
			sv.salebranch.setReverse(BaseScreenData.REVERSED);
			sv.salebranch.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.salebranch.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.saledept.setReverse(BaseScreenData.REVERSED);
			sv.saledept.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.saledept.setHighLight(BaseScreenData.BOLD);
		}  
	}%>
	
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
					<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Representative Number")%></label>
					<div class="input-group" style="min-width: 120px; max-width:140px">

						<input name='salerepno' id='salerepno' type='text'
							value='<%=sv.salerepno.getFormData()%>'
							maxLength='<%=sv.salerepno.getLength()%>'
 							size='<%=sv.salerepno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(salerepno)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.salerepno).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.salerepno).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salerepno')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.salerepno).getColor()== null  ? 
"input_cell" :  (sv.salerepno).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salerepno')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>

					</div>
				</div>
			</div>
			
					<div class="col-md-1"></div>
		
			<div class="col-md-4">
				<div class="form-group" >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Branch/ Sales Section Number")%></label>
						<div class="input-group" style="min-width: 120px; max-width:140px">

						<input name='salebranch' id='salebranch' type='text'
							value='<%=sv.salebranch.getFormData()%>'
							maxLength='<%=sv.salebranch.getLength()%>'
 							size='<%=sv.salebranch.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(salebranch)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.salebranch).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.salebranch).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salebranch')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.salebranch).getColor()== null  ? 
"input_cell" :  (sv.salebranch).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salebranch')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>

					</div>
				</div>
			</div>
		
		
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></label>
						<div class="input-group" style="min-width: 120px; max-width:140px">

						<input name='agncysel' id='agncysel' type='text'
							value='<%=sv.agncysel.getFormData()%>'
							maxLength='<%=sv.agncysel.getLength()%>'
 							size='<%=sv.agncysel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(agncysel)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.agncysel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.agncysel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.agncysel).getColor()== null  ? 
"input_cell" :  (sv.agncysel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>

					</div>
				</div>
			</div>

		</div>
		
		<div class="row">
					<div class="col-md-3">
				<div class="form-group" >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Dapartment Code")%></label>
						<div class="input-group" style="min-width: 120px; max-width:140px">

						<input name='saledept' id='saledept' type='text'
							value='<%=sv.saledept.getFormData()%>'
							maxLength='<%=sv.saledept.getLength()%>'
 							size='<%=sv.saledept.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(saledept)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.saledept).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.saledept).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.saledept).getColor()== null  ? 
"input_cell" :  (sv.saledept).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('saledept')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>

					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></label>
				<div class="form-group">
					<div class="input-group" style="min-width: 120px; max-width:140px">
					<input name='userid' type='text'
						<%formatValue = (sv.userid.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.userid.getLength()%>'
						maxLength='<%=sv.userid.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(userid)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.userid).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.userid).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.userid).getColor() == null
						? "input_cell"
						: (sv.userid).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div></div>
			</div>
			</div>
	</div>
</div>



<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
	</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Representative")%> </label>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline" checked> <%=smartHF.buildRadioOption(sv.action, "action", "A")%><b><%=resourceBundleHandler.gettingValueFromBundle("Appointment")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><b><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></b>
				</label>
			</div>


			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%><b><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></b>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "D")%><b><%=resourceBundleHandler.gettingValueFromBundle("Add Agency")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline" style="min-width:500px"> <%=smartHF.buildRadioOption(sv.action, "action", "E")%><b><%=resourceBundleHandler.gettingValueFromBundle("Modify Agency Details before Approval")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "F")%><b><%=resourceBundleHandler.gettingValueFromBundle("Delete Agency after Approval")%></b>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "G")%><b><%=resourceBundleHandler.gettingValueFromBundle("Approve Add / Delete Agency ")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "H")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Hierarchy (Agency) Inquiry ")%></b>
				</label>
			</div>
		</div>
		
			<br><hr/>
		
		<div class="row">
			<div class="col-md-4">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Branch/Section")%> </label>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline" checked> <%=smartHF.buildRadioOption(sv.action, "action", "I")%><b><%=resourceBundleHandler.gettingValueFromBundle("Registration")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "J")%><b><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></b>
				</label>
			</div>


			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "K")%><b><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></b>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "L")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Hierarchy (Sales Branch) Inquiry")%></b>
				</label>
			</div>
		</div>
		
			<br><hr/>
		
		<div class="row">
			<div class="col-md-4">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%> </label>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline" checked> <%=smartHF.buildRadioOption(sv.action, "action", "M")%><b><%=resourceBundleHandler.gettingValueFromBundle("Inquiry")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "N")%><b><%=resourceBundleHandler.gettingValueFromBundle("Cancel Registration")%></b>
				</label>
			</div>


			
		</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>