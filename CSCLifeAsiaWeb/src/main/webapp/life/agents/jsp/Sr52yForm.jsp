<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR52Y";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%@ page import="java.util.*" %>
<%Sr52yScreenVars sv = (Sr52yScreenVars) fw.getVariables();%>

<%if (sv.Sr52yscreenWritten.gt(0)) {%>
	<%Sr52yscreen.clearClassString(sv);%>
	<%StringData generatedText7 = new StringData("Multiple Beneficiaries Allowed ");%>
	<%sv.multind.setClassString("");%>
	<%StringData generatedText10 = new StringData("(Y/N)");%>
	<%StringData generatedText9 = new StringData("Relationship Validation        ");%>
	<%sv.relto.setClassString("");%>
	<%StringData generatedText12 = new StringData("O - Conctact Owner");%>
	<%StringData generatedText11 = new StringData("L - Life Assured");%>
	<%StringData generatedText13 = new StringData("B - Both Owner and Life Assured");%>
	<%StringData generatedText14 = new StringData("N - No Relationship Validation");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.multind.setReverse(BaseScreenData.REVERSED);
			sv.multind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.multind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.relto.setReverse(BaseScreenData.REVERSED);
			sv.relto.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.relto.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Company:")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.company.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:30px">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</div>
		       		</div>
		       	</div>
		       	
		       
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Table:")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
					<div class="form-group">
					</div></div>
					
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.item.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.item.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:50px">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					
					<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="float: left!important;margin-left: 2px !important;border-radius: 5px !important;
						height: 25px !important;font-size: 12px !important;min-width:170px">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	</div>
		       	<br>
		      <div class="row">
		       	<div class="col-md-5">
		       		<div class="form-group">
		       		 <label>
		       		 <%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<%=resourceBundleHandler.gettingValueFromBundle("Multiple Beneficiaries Allowed")%>

							<%}%> 
					</label>
							
							<%if ((new Byte((sv.multind).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									
								<input  type='checkbox' name='multind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(multind)' onKeyUp='return checkMaxLength(this)'   
								
									
								
								<%
								
								if((sv.multind).getColor()!=null){
											 %>style='margin-left:0px;background-color:#FF0000;'
										<%}
								else{%>
									style='margin-left:0px'; 
								<%}
										if((sv.multind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.multind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('multind')"/>
							
								<input type='checkbox' name='multind' value='N' 
								
								<% if(!(sv.multind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('multind')"/>
								<%}%>
		       		
		       		</div>
		       	</div>
		  </div>
		  <br>
		    <div class="row">
		       	<div class="col-md-5">
		       		<div class="form-group">
		     			<label>
						<%=resourceBundleHandler.gettingValueFromBundle("Relationship Validation")%>
						</label>
						
						<div class="input-group">
						<%
						ScreenDataHelper relValidation = new ScreenDataHelper(fw.getVariables(), sv.relto);
						%>
						
						<%if (relValidation.isReadonly()) {%>
						
						<!--  Ilife- Life Cross Browser - Sprint 2 D1 : Task 5 Start-->
						
						<!-- ILIFE-2832 -->
						
						<%	HashMap hs = new HashMap();
										hs.put("S","Select");
										hs.put("C", "Conctact Owner");
										hs.put("L", "Life Assured");
										hs.put("B", "Both Owner and Life Assured");
										hs.put("N", "No Relationship Validation");
						
										 %>
						
						<input name='relto' type='text'  class="resizedTextbox" value='<%=(String)hs.get(relValidation.value())%>'
						
						
						       class="blank_cell" readonly
						       onFocus='doFocus(this)' onHelp='return fieldHelp(relto)'
						       onKeyUp='return checkMaxLength(this)'>
						<%} else {%>
						
						<select name="relto" type="list" class="<%=relValidation.clazz()%>">
						   <option <%=relValidation.selected("S")%> value="Select">Select</option>
						  <option <%=relValidation.selected("C")%> value="Contract Owner">Contract Owner</option>
						  <option <%=relValidation.selected("L")%> value="Life Assured">Life Assured</option>
						  <option <%=relValidation.selected("B")%> value="Both Owner and Life Assured">Both Owner and Life Assured</option>
						  <option <%=relValidation.selected("N")%> value="No Relationship Validation">No Relationship Validation</option>
						</select>
						<%}%>
						</div>
		       		</div>
		       </div>
		    </div>
		       		

		</div>
</div>

<%}%>

<%if (sv.Sr52yprotectWritten.gt(0)) {%>
	<%Sr52yprotect.clearClassString(sv);%>

	<%
{
	}

	%>

<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>