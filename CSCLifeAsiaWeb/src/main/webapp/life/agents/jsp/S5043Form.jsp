<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5043";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5043ScreenVars sv = (S5043ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Agent Appointment");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Agent Modify");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Agent Terminate");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Agent Inquiry");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Reporting Inquiry - Higher Levels");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F - Reporting Inquiry - Lower Levels");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"G - Agent Production Inquiry");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"H - Reinstate Terminated Agent");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
    	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-12"> 
				    		<div class="form-group" style="min-width:110px">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>
					    		
					    	
					    		<div class="input-group" style="min-width:110px">
						    		
<input name='agntsel' id= 'agntsel'
type='text' 
value='<%=sv.agntsel.getFormData()%>' 
maxLength='<%=sv.agntsel.getLength()%>' 
size='<%=sv.agntsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(agntsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.agntsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.agntsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('agntsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<%
	}else { 
%>

class = ' <%=(sv.agntsel).getColor()== null  ? 
"input_cell" :  (sv.agntsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%} %>


						    		

				      			</div>
				    		</div>
			    	</div>
			       
			</div>
		</div>
	</div>



<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
		<%
			if (sv.isDMSFlag.compareTo("N") != 0) {
		%>
    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
						<%=smartHF.buildRadioOption(sv.action, "action", "A", "true")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Appointment")%></b>
					</label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
							<%=smartHF.buildRadioOption(sv.action, "action", "B", "true")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Modify")%></b>
					</label>			
			    </div>		        
				
			    <div class="col-md-4">
					 	 <label class="radio-inline">
					<%=smartHF.buildRadioOption(sv.action, "action", "C", "true")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Terminate")%></b>
					  </label>
				</div>
				    
			</div>		
			<div class="row">
			<div class="col-md-4">	  			
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "D")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Inquiry")%></b>
					  </label>			
			    </div>		    	
			    <div class="col-md-4">
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "E")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reporting Inquiry - Higher Levels")%></b>
					 </label>
				</div>	  	
				<div class="col-md-4">	  		
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "F")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reporting Inquiry - Lower Levels")%></b>
					 </label>			
			    </div>		        
				</div>
				<div class="row">	
			    <div class="col-md-4">
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "G")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Production Inquiry")%></b>
					  </label>	
				</div>	  
				<div class="col-md-4">		
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "H", "true")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reinstate Terminated Agent")%></b>
					  </label>			
			    </div>	
			</div>	
												
		</div>
		<%
			} else {
		%>
		    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
						<%=smartHF.buildRadioOption(sv.action, "action", "A")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Appointment")%></b>
					</label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
							<%=smartHF.buildRadioOption(sv.action, "action", "B")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Modify")%></b>
					</label>			
			    </div>		        
				
			    <div class="col-md-4">
					 	 <label class="radio-inline">
					<%=smartHF.buildRadioOption(sv.action, "action", "C")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Terminate")%></b>
					  </label>
				</div>
				    
			</div>		
			<div class="row">
			<div class="col-md-4">	  			
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "D")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Inquiry")%></b>
					  </label>			
			    </div>		    	
			    <div class="col-md-4">
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "E")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reporting Inquiry - Higher Levels")%></b>
					 </label>
				</div>	  	
				<div class="col-md-4">	  		
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "F")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reporting Inquiry - Lower Levels")%></b>
					 </label>			
			    </div>		        
				</div>
				<div class="row">	
			    <div class="col-md-4">
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "G")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agent Production Inquiry")%></b>
					  </label>	
				</div>	  
				<div class="col-md-4">		
					  <label class="radio-inline">
					   <%=smartHF.buildRadioOption(sv.action, "action", "H")%><b><%=resourceBundleHandler.gettingValueFromBundle("Reinstate Terminated Agent")%></b>
					  </label>			
			    </div>	
			</div>	
												
		</div>
		<%
			}
		%>
	</div> 
<%@ include file="/POLACommon2NEW.jsp"%>