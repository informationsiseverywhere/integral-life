
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SD5G3";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sd5g3ScreenVars sv = (Sd5g3ScreenVars) fw.getVariables();%>

<%
{

		if (appVars.ind31.isOn()) {
			sv.salelicensetype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.salelicensetype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind64.isOn()) {
			sv.salelicensetype.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind64.isOn()) {
			sv.salelicensetype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.salelicensetype.setColor(BaseScreenData.RED);
		}
	
	}
%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type ");%>


<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	
<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=generatedText1%></label>
					    		<table><tr><td>
						    			<%					
		if(!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:1px">
</td><td style="min-width:100px">



	
  		
		<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</td></tr></table>
				    		</div>
				       </div>
			 </div> 
				    
			 <div class="row">	
			    	<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=generatedText2%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				       
				     <div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=generatedText3%></label>
					    		<table><tr><td>
						    		<%					
		if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td style="min-width:1px">
</td><td style="min-width:100px">




	
  		
		<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
				      			</div>
				    		</div>
				       </div>
				   


<div class="row">		
     <div class="col-md-12">
         <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover"  width="100%"> 

  <thead>
    <tr class='info'>
      <th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Sales License Type")%></center></th>        								
       <th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("License Number")%></center></th>
       <th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("From")%></center></th>
       <th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("To")%></center></th>
		</tr>
 </thead>
 
  <tbody>
 
  
  <%GeneralTable sfl = fw.getTable("sd5g3screensfl");%>
   <%String backgroundcolor="#FFFFFF";
   Sd5g3screensfl.set1stScreenRow(sfl, appVars, sv);
  
  int count = 1;
  while (Sd5g3screensfl
			.hasMoreScreenRows(sfl)){ 
	  %> 
	  
							<tr>
								<td
									style="color: #434343; padding-left: 5px; font-weight: bold; border-right: 1px solid #dddddd;"
									align="center">
									
								
								 <div class="form-group">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "salelicensetype" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("salelicensetype");
												optionValue = makeDropDownList(mappedItems, sv.salelicensetype.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.salelicensetype.getFormData()).toString().trim());
												if (longValue == null) {
													longValue = "";
												}
										%>
										<%
											if ((new Byte((sv.salelicensetype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
										%>
										<div class='output_cell' style="width: 60px;">
											<%=longValue%>
										</div>
										<%
											longValue = null;
										%>
										<%
											} else {
										%>
										<%
											if ("red".equals((sv.salelicensetype).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>
											
											<select
												name='<%="sd5g3screensfl" + "." + "salelicensetype" + "_R" + count%>'
												id='<%="sd5g3screensfl" + "." + "salelicensetype" + "_R" + count%>'
												type='list' style="width: 200px;"
												<% if((new Byte((sv.salelicensetype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){%>
												readonly="true" disabled="disabled" class="output_cell"
												<%} else if ((new Byte((sv.salelicensetype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" 
												<%}else {%>
												 class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											
											
											<%
												if ("red".equals((sv.salelicensetype).getColor())) {
											%>
										</div>
										<%
											}
										%>
										<%
											}
										%>
									</div> 
								</td>


								<td>
								<% if((new Byte((sv.tlaglicno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){%>
								<input type='text'
									maxLength='<%=sv.tlaglicno.getLength()%>'
									<%if ((sv.tlaglicno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%>
									value='<%=sv.tlaglicno.getFormData()%>'
									size='<%=sv.tlaglicno.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sd5g3screensfl.tlaglicno)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sd5g3screensfl" + "." + "tlaglicno" + "_R" + count%>'
									id='<%="sd5g3screensfl" + "." + "tlaglicno" + "_R" + count%>'
									class="input_cell"
									disabled="disabled"
									style="width: <%=sv.tlaglicno.getLength() * 12%> px;">
									 <%}else{ %>
								
								<input type='text'
									maxLength='<%=sv.tlaglicno.getLength()%>'
									<%if ((sv.tlaglicno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%>
									value='<%=sv.tlaglicno.getFormData()%>'
									size='<%=sv.tlaglicno.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sd5g3screensfl.tlaglicno)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sd5g3screensfl" + "." + "tlaglicno" + "_R" + count%>'
									id='<%="sd5g3screensfl" + "." + "tlaglicno" + "_R" + count%>'
									class="input_cell"
									style="width: <%=sv.tlaglicno.getLength() * 12%> px;">
									 
									 <%}%>
									
								</td>

								<td align="center">
									<div class="form-group">
										<%
											if ((new Byte((sv.frmdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| fw.getVariables().isScreenProtected()) {
										%>
										<div class="input-group " style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.frmdateDisp, (sv.frmdateDisp.getLength()))%></div>
										<%
											} else {
										%>
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field="frmdateDisp"
											data-link-format="dd/mm/yyyy" style="width: 150px;">
											<%=smartHF.getRichTextDateInput(fw, sv.frmdateDisp, (sv.frmdateDisp.getLength()))%>
											<span class="input-group-addon"> <span
												class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
										<%
											}
										%>
									</div>
								</td>

								<td align="center">
									<div class="form-group">
										<%
											if ((new Byte((sv.todateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| fw.getVariables().isScreenProtected()) {
										%>
										<div class="input-group " style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.todateDisp, (sv.todateDisp.getLength()))%></div>
										<%
											} else {
										%>
										<div class="input-group date form_date col-md-12" data-date=""
											data-date-format="dd/mm/yyyy" data-link-field="todateDisp"
											data-link-format="dd/mm/yyyy" style="width: 150px;">
											<%=smartHF.getRichTextDateInput(fw, sv.todateDisp, (sv.todateDisp.getLength()))%>
											<span class="input-group-addon"> <span
												class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
										<%
											}
										%>

									</div>
								</td>
							</tr>
						   <%
						   	if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
						   			backgroundcolor = "#ededed";
						   		} else {
						   			backgroundcolor = "#FFFFFF";
						   		}
						   		count = count + 1;
						   		Sd5g3screensfl.setNextScreenRow(sfl, appVars, sv);
						   	}
						   %> 
    
       </tbody>
      </table>
    </div>
  </div>
</div>






</div>
</div>  

 <script>
$(document).ready(function() {
	$('#dataTables-sd5g3').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

