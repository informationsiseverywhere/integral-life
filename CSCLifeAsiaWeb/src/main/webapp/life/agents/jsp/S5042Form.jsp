

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5042";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5042ScreenVars sv = (S5042ScreenVars) fw.getVariables();%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Change Type");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Comm ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Change Servicing Agent ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OverRide Commision ");%>
<%{
		if (appVars.ind31.isOn()) {
			sv.newagt.setReverse(BaseScreenData.REVERSED);
			sv.newagt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.newagt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.servcom.setReverse(BaseScreenData.REVERSED);
			sv.servcom.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.servcom.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.rnwlcom.setReverse(BaseScreenData.REVERSED);
			sv.rnwlcom.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.rnwlcom.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.inalcom.setReverse(BaseScreenData.REVERSED);
			sv.inalcom.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.inalcom.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.zrorcomm.setReverse(BaseScreenData.REVERSED);
			sv.zrorcomm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.zrorcomm.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Agent ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch          ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">    	                     
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
	                      <table><tr><td>
	                      	
	                     <%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:40px">



	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:40px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:100px">



	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;margin-left: 1px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	                      </td></tr></table>
	                     </div>	
	             </div>	
	      	<div class="col-md-2">
	      	</div>	
	       <div class="col-md-4">	  
	                    <div class="form-group">                    
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
	                      <%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
							%>
							
						  		
								<%					
								if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="max-width:100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
	                     </div>	
	                     </div>
	                     </div>
	                   <div class="row"> 
	                    <div class="col-md-2">	  
	                   		 <div class="form-group">                    
		                      		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		                      		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		                      </div>	
	                    </div>
	                     <div class="col-md-2">	 
	                     </div>
	                     <div class="col-md-2">	  
	                   		 <div class="form-group"> 
	                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
	                   		 
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	
	                   		 </div>	
	                    </div> 
	                     <div class="col-md-2">	 
	                     </div>  
					 <div class="col-md-2">	  
	                   		 <div class="form-group"> 
	                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                   		 <%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
					%>
					
				    
					   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:65px;"> 
					   <%=	(sv.cntcurr.getFormData()).toString()%>
					   </div>
					   
					   
				   		<%
						longValue = null;
						formatValue = null;
						%>
   
  
	                   		 </div>	
	                    </div>  
	       </div>
	       <div class="row"> 
	                    <div class="col-md-4">	  
	                   		 <div class="form-group">                    
		                      		<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Agent")%></label>
		                      			<table><tr><td>  
										<%					
		if(!((sv.servagnt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td style="min-width:100px">


	
  		
		<%					
		if(!((sv.servagnam.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		                      </td></tr></table>	
	                    </div>  
	                   </div>  
	                   
	                      <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
		                      			<%					
									if(!((sv.servbrname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width:150px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  
		                      </div>	
	                    </div> 
	       </div>
	       
	       
	       
	       
	       
	       
	       
	       <br><br>
	       
	       
	       
	       
	       <div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<div class="table-responsive">
	         <table  id='dataTables-s5042' class="table table-striped table-bordered table-hover" width="100%" >
               <thead>
		
			        <tr class="info">						
										<th style="width:60px"><center><%=resourceBundleHandler.gettingValueFromBundle("Select") %></center></th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Agent Number") %></center></th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Agent Name") %></center></th>										
	                               </tr>
	                            </thead>
	                            <tbody>
							<%
							GeneralTable sfl = fw.getTable("s5042screensfl");
						int count = 1;
						S5042screensfl
						.set1stScreenRow(sfl, appVars, sv);
						
						while (S5042screensfl
						.hasMoreScreenRows(sfl)) {
						
					%>

	<tr >
									
									<td style="width:80px";  >	<center>												
																	
													
					
					 					 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5042screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5042screensfl.select_R<%=count%>'
						 id='s5042screensfl.select_R<%=count%>'
						 onClick="selectedRow('s5042screensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
								</center>	</td>
				    									<td >									
																
									
											
						<%= sv.comagnt.getFormData()%>
						
														 
				
									</td>
				    									<td >									
																
									
											
						<%= sv.comagntnm.getFormData()%>
						
														 
				
									</td>
				
		
		
	</tr>


	<%
	
	count = count + 1;
	S5042screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
		</tbody>
		</table>
		</div>
                     
	                 </div>
	             </div>
	         </div>
	       
	       
	       
	       
	         
	         <div class="row"> 
	     
			<div class="col-md-5" style="padding-right:0px;"> 
				  
				        
                            <label><%=resourceBundleHandler.gettingValueFromBundle("New")%></label>
                        
				    	<div class="row">
							
							<%
                                 if((new Byte((sv.newagt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
                            %>
                            <div class="col-md-12">
                                <div class="input-group">
						    		<input name='newagt' id='newagt'
										type='text'
										value='<%=sv.newagt.getFormData()%>'
										maxLength='<%=sv.newagt.getLength()%>'
										size='<%=sv.newagt.getLength()%>'
										onFocus='doFocus(this)' onHelp='return fieldHelp(newagt)'
										onKeyUp='return checkMaxLength(this)'
										readonly="true"
										class="output_cell"	 >
									<span class="input-group-addon" style="min-width:450px;">
									    <%
		                                    fieldItem=appVars.loadF4FieldsLong(new String[] {"newagntnm"},sv,"E",baseModel);
		                                    mappedItems = (Map) fieldItem.get("newagntnm");
		                                    longValue = (String) mappedItems.get((sv.newagntnm.getFormData()).toString());
		                                %>
		                                <%
		                                longValue = null;
		                                formatValue = null;
		                                %>
		                                <%if(!((sv.newagntnm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
		                                
		                                                    if(longValue == null || longValue.equalsIgnoreCase("")) {
		                                                        formatValue = formatValue( (sv.newagntnm.getFormData()).toString());
		                                                    } else {
		                                                        formatValue = formatValue( longValue);
		                                                    }
		                                
		                                
		                                            } else  {
		                                
		                                            if(longValue == null || longValue.equalsIgnoreCase("")) {
		                                                        formatValue = formatValue( (sv.newagntnm.getFormData()).toString());
		                                                    } else {
		                                                        formatValue = formatValue( longValue);
		                                                    }
		                                
		                                            }
		                                            %>
		                                        <%=XSSFilter.escapeHtml(formatValue)%>
		                                    </span>
		                                <%
		                                longValue = null;
		                                formatValue = null;
		                                %>
								</div>
							</div>
							<%} else { %>
							<div class="col-md-5" style="padding-right:0px;">
							  <div class="form-group">
							     <div class="input-group">
								     <input name='newagt' id='newagt'
	                                        type='text'
	                                        value='<%=sv.newagt.getFormData()%>'
	                                        maxLength='<%=sv.newagt.getLength()%>'
	                                        size='<%=sv.newagt.getLength()%>'
	                                        onFocus='doFocus(this)' onHelp='return fieldHelp(newagt)'
	                                        onKeyUp='return checkMaxLength(this)'
	                                        class = ' <%=(sv.newagt).getColor()== null  ?
	                                        "input_cell" :  (sv.newagt).getColor().equals("red") ?
	                                        "input_cell red reverse" : "input_cell" %>' >
	                                        
	                                <span class="input-group-btn">
	                                    <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('newagt')); doAction('PFKEY04')">
	                                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	                                    </button>
	                                </span>
	                             </div>
	                             </div>
							</div>
							
							<div class="col-md-5" style="padding-left:0px;">
	                                <%
	                                    fieldItem=appVars.loadF4FieldsLong(new String[] {"newagntnm"},sv,"E",baseModel);
	                                    mappedItems = (Map) fieldItem.get("newagntnm");
	                                    longValue = (String) mappedItems.get((sv.newagntnm.getFormData()).toString());
	                                %>
	                                <%
	                                longValue = null;
	                                formatValue = null;
	                                %>
	                                    <!-- ILIFE-2143 by hai2 -->
	                                    <div  
	                                    class='<%= (sv.newagntnm.getFormData()).trim().length() == 0 ?
	                                                    "blank_cell" : "output_cell" %>' style="min-width:200px;margin-left: -2px;margin-top: 2px;"
	                                    >
	                                <%
	                                if(!((sv.newagntnm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
	                                
	                                                    if(longValue == null || longValue.equalsIgnoreCase("")) {
	                                                        formatValue = formatValue( (sv.newagntnm.getFormData()).toString());
	                                                    } else {
	                                                        formatValue = formatValue( longValue);
	                                                    }
	                                            } else  {
	                                
	                                            if(longValue == null || longValue.equalsIgnoreCase("")) {
	                                                        formatValue = formatValue( (sv.newagntnm.getFormData()).toString());
	                                                    } else {
	                                                        formatValue = formatValue( longValue);
	                                                    }
	                                            }
	                                            %>
	                                        <%=XSSFilter.escapeHtml(formatValue)%>
	                                    </div>
		                                <%
		                                longValue = null;
		                                formatValue = null;
		                                %>
		                            </div>
							
                              <%} %>
				    	</div>
				    	
				    </div>
			
			
			
			
			
			
			
			
									
			
	         </div>
	     
	       <div class="row"> 
	                    <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Commission Change Type")%></label>
		                      			
		                      </div>
	         			</div>
	       </div>
	        <div class="row"> 
	                    <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Change Servicing Agent")%></label>
		                      			
		                      			<input type='checkbox' name='comind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(comind)' onKeyUp='return checkMaxLength(this)'    
								<%
								
								if((sv.comind).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.comind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.comind).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('comind')"/>
								
								<input type='checkbox' name='comind' value='N' 
								
								<% if(!(sv.comind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('comind')"/>
										                      			
		                      </div>
	         			</div>
	         			<div class="col-md-1">	  
	                   		 </div>  
	         			 <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Initial")%></label>
		                      			
		                      			<input type='checkbox' name='inalcom' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(inalcom)' onKeyUp='return checkMaxLength(this)'    
									<%
									
									if((sv.inalcom).getColor()!=null){
												 %>style='background-color:#FF0000;'
											<%}
											if((sv.inalcom).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }if((sv.inalcom).getEnabled() == BaseScreenData.DISABLED){%>
									    	   disabled
											
											<%}%>
									class ='UICheck' onclick="handleCheckBox('inalcom')"/>
									
									<input type='checkbox' name='inalcom' value='N' 
									
									<% if(!(sv.inalcom).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }%>
									
									style="visibility: hidden" onclick="handleCheckBox('inalcom')"/>
		                      </div>
	         			</div>
	         			<div class="col-md-1">	  
	                   		 </div>  
	         			 <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Renewal")%></label>
		                      			
		                      			<input type='checkbox' name='rnwlcom' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(rnwlcom)' onKeyUp='return checkMaxLength(this)'    
									<%
									
									if((sv.rnwlcom).getColor()!=null){
												 %>style='background-color:#FF0000;'
											<%}
											if((sv.rnwlcom).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }if((sv.rnwlcom).getEnabled() == BaseScreenData.DISABLED){%>
									    	   disabled
											
											<%}%>
									class ='UICheck' onclick="handleCheckBox('rnwlcom')"/>
									
									<input type='checkbox' name='rnwlcom' value='N' 
									
									<% if(!(sv.rnwlcom).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }%>
									
									style="visibility: hidden" onclick="handleCheckBox('rnwlcom')"/>
		                      	</div>
	         			</div>
	       </div>
	     
	      
	                    
	        <div class="row"> 
	                    <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing")%></label>
		                      			
		                      			<input type='checkbox' name='servcom' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(servcom)' onKeyUp='return checkMaxLength(this)'    
								<%
								
								if((sv.servcom).getColor()!=null){
											 %>style='background-color:#FF0000;'
										<%}
										if((sv.servcom).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }if((sv.servcom).getEnabled() == BaseScreenData.DISABLED){%>
								    	   disabled
										
										<%}%>
								class ='UICheck' onclick="handleCheckBox('servcom')"/>
								
								<input type='checkbox' name='servcom' value='N' 
								
								<% if(!(sv.servcom).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								
								style="visibility: hidden" onclick="handleCheckBox('servcom')"/>
		                      			
		                      </div>
	                     </div>
	                     <div class="col-md-1">
	                     </div>
	                      <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("OverRide Commision")%></label>
		                      			
		                      			<input type='checkbox' name='zrorcomm' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(zrorcomm)' onKeyUp='return checkMaxLength(this)'    
									<%
									
									if((sv.zrorcomm).getColor()!=null){
												 %>style='background-color:#FF0000;'
											<%}
											if((sv.zrorcomm).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }if((sv.zrorcomm).getEnabled() == BaseScreenData.DISABLED){%>
									    	   disabled
											
											<%}%>
									class ='UICheck' onclick="handleCheckBox('zrorcomm')"/>
									
									<input type='checkbox' name='zrorcomm' value='N' 
									
									<% if(!(sv.zrorcomm).toString().trim().equalsIgnoreCase("Y")){
												%>checked
											
									      <% }%>
									
									style="visibility: hidden" onclick="handleCheckBox('zrorcomm')"/>
		                      </div>
	                     </div>
	                      
	         </div>
	     </div>		 
	       
 </div>	
 	
 	
 	
 	<script>
$(document).ready(function() {
	$('#dataTables-s5042').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
 	

<%@ include file="/POLACommon2NEW.jsp"%>



<div style='visibility:hidden;'><table>

<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>


<input name='sflchg' 
type='text'

<%

		formatValue = (sv.sflchg.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%=sv.sflchg.getLength()%>'
maxLength='<%=sv.sflchg.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(s5042screensfl.sflchg)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.sflchg).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sflchg).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sflchg).getColor()== null  ? 
			"input_cell" :  (sv.sflchg).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>

<td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Agent")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("OR Comm")%>
</div>

</tr></table></div>
