<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR521";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr521ScreenVars sv = (Sr521ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Component Code");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"SP");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top Up");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item ");%>



<div class="panel panel-default">
    	  
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	
				      		</div>
				    </div>
			   
			   
			   	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      		</div>
				    	</div>
			    	
			    	
			    		
			    	  <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    		<div class="input-group">
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                         </div>  					    		
				      </div>
				    </div>
			       </div>
			       
			       <div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					<table>
			    	   <tr>
			    	       
			    	     <td>
			    	  
					 <!--  <div class="input-group three-controller"> -->
					<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		  </td>


                        
                        <td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>


                      
                        <td> 
		<%--  <span class="input-group-addon"> <label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></span>
						 --%>
	<% 	if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		      
                         </td>
                      </tr>
                   </table>		
		<%-- <%=smartHF.getHTMLVarExt(fw, sv.itmtoDisp)%> --%>
					</div>
				</div>
			</div>
		
		  <br><br>
		  
		  
		   
			<div class="row">	
			    	<div class="col-md-12"> 
				    	 <table class="table table-bordered">
         <thead>
			
		<tr>
														<th style="width:70px;"><%=resourceBundleHandler.gettingValueFromBundle("OR Component Code  ")%></th>									
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("SP")%></th>
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Top Up")%></th>
										
														
										
		</tr>
		</thead>

 
  <tbody>
   <tr><td>


<input name='zcomcode01' 
type='text'

<%

		formatValue = (sv.zcomcode01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode01.getLength()%>'
maxLength='<%= sv.zcomcode01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell" 
<%
	}else if((new Byte((sv.zcomcode01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode01).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc01) %>'
	 <%}%>

size='<%= sv.zrspperc01.getLength()%>'
maxLength='<%= sv.zrspperc01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc01).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc01) %>'
	 <%}%>

size='<%= sv.zrtuperc01.getLength()%>'
maxLength='<%= sv.zrtuperc01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc01).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode02' 
type='text'

<%

		formatValue = (sv.zcomcode02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode02.getLength()%>'
maxLength='<%= sv.zcomcode02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode02).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc02) %>'
	 <%}%>

size='<%= sv.zrspperc02.getLength()%>'
maxLength='<%= sv.zrspperc02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc02).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc02) %>'
	 <%}%>

size='<%= sv.zrtuperc02.getLength()%>'
maxLength='<%= sv.zrtuperc02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc02).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode03' 
type='text'

<%

		formatValue = (sv.zcomcode03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode03.getLength()%>'
maxLength='<%= sv.zcomcode03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode03).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc03) %>'
	 <%}%>

size='<%= sv.zrspperc03.getLength()%>'
maxLength='<%= sv.zrspperc03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc03).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc03) %>'
	 <%}%>

size='<%= sv.zrtuperc03.getLength()%>'
maxLength='<%= sv.zrtuperc03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc03).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode04' 
type='text'

<%

		formatValue = (sv.zcomcode04.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode04.getLength()%>'
maxLength='<%= sv.zcomcode04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode04)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode04).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc04) %>'
	 <%}%>

size='<%= sv.zrspperc04.getLength()%>'
maxLength='<%= sv.zrspperc04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc04).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc04) %>'
	 <%}%>

size='<%= sv.zrtuperc04.getLength()%>'
maxLength='<%= sv.zrtuperc04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc04).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode05' 
type='text'

<%

		formatValue = (sv.zcomcode05.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode05.getLength()%>'
maxLength='<%= sv.zcomcode05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode05)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode05).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc05) %>'
	 <%}%>

size='<%= sv.zrspperc05.getLength()%>'
maxLength='<%= sv.zrspperc05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc05).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc05) %>'
	 <%}%>

size='<%= sv.zrtuperc05.getLength()%>'
maxLength='<%= sv.zrtuperc05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc05).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode06' 
type='text'

<%

		formatValue = (sv.zcomcode06.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode06.getLength()%>'
maxLength='<%= sv.zcomcode06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode06)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode06).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc06) %>'
	 <%}%>

size='<%= sv.zrspperc06.getLength()%>'
maxLength='<%= sv.zrspperc06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc06).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc06) %>'
	 <%}%>

size='<%= sv.zrtuperc06.getLength()%>'
maxLength='<%= sv.zrtuperc06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc06).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode07' 
type='text'

<%

		formatValue = (sv.zcomcode07.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode07.getLength()%>'
maxLength='<%= sv.zcomcode07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode07)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode07).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc07) %>'
	 <%}%>

size='<%= sv.zrspperc07.getLength()%>'
maxLength='<%= sv.zrspperc07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc07).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc07) %>'
	 <%}%>

size='<%= sv.zrtuperc07.getLength()%>'
maxLength='<%= sv.zrtuperc07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc07).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode08' 
type='text'

<%

		formatValue = (sv.zcomcode08.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode08.getLength()%>'
maxLength='<%= sv.zcomcode08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode08)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode08).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc08) %>'
	 <%}%>

size='<%= sv.zrspperc08.getLength()%>'
maxLength='<%= sv.zrspperc08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc08).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc08) %>'
	 <%}%>

size='<%= sv.zrtuperc08.getLength()%>'
maxLength='<%= sv.zrtuperc08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc08).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode09' 
type='text'

<%

		formatValue = (sv.zcomcode09.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode09.getLength()%>'
maxLength='<%= sv.zcomcode09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode09)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode09).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc09) %>'
	 <%}%>

size='<%= sv.zrspperc09.getLength()%>'
maxLength='<%= sv.zrspperc09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc09).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc09) %>'
	 <%}%>

size='<%= sv.zrtuperc09.getLength()%>'
maxLength='<%= sv.zrtuperc09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc09).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode10' 
type='text'

<%

		formatValue = (sv.zcomcode10.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode10.getLength()%>'
maxLength='<%= sv.zcomcode10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode10)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode10).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc10) %>'
	 <%}%>

size='<%= sv.zrspperc10.getLength()%>'
maxLength='<%= sv.zrspperc10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc10).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc10) %>'
	 <%}%>

size='<%= sv.zrtuperc10.getLength()%>'
maxLength='<%= sv.zrtuperc10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc10).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td></tr>
</tbody>
</table>  
			   
			    <div class="row"> 
			   	
			    	   <div class="col-md-2"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
					    		</div></div>
					    		<div class="col-md-2"></div> 
					    		<div class="col-md-2"> 
				    		<div class="form-group">  
					    		<table><tr><td style="min-width:70px;"> 
<input name='contitem' 
type='text'

<%

		formatValue = (sv.contitem.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table>
</div>
				      		
				    	
			    	
			   </div>
		  </div>
		  
		  </div></div>


</div></div>


<%-- 
<div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




&nbsp;
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>




	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'> 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("OR Component Code")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("SP")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Top Up")%>
</div>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode01' 
type='text'

<%

		formatValue = (sv.zcomcode01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode01.getLength()%>'
maxLength='<%= sv.zcomcode01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode01).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc01) %>'
	 <%}%>

size='<%= sv.zrspperc01.getLength()%>'
maxLength='<%= sv.zrspperc01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc01).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc01) %>'
	 <%}%>

size='<%= sv.zrtuperc01.getLength()%>'
maxLength='<%= sv.zrtuperc01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc01).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode02' 
type='text'

<%

		formatValue = (sv.zcomcode02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode02.getLength()%>'
maxLength='<%= sv.zcomcode02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode02).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc02) %>'
	 <%}%>

size='<%= sv.zrspperc02.getLength()%>'
maxLength='<%= sv.zrspperc02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc02).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc02) %>'
	 <%}%>

size='<%= sv.zrtuperc02.getLength()%>'
maxLength='<%= sv.zrtuperc02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc02).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode03' 
type='text'

<%

		formatValue = (sv.zcomcode03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode03.getLength()%>'
maxLength='<%= sv.zcomcode03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode03).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc03) %>'
	 <%}%>

size='<%= sv.zrspperc03.getLength()%>'
maxLength='<%= sv.zrspperc03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc03).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc03) %>'
	 <%}%>

size='<%= sv.zrtuperc03.getLength()%>'
maxLength='<%= sv.zrtuperc03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc03).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode04' 
type='text'

<%

		formatValue = (sv.zcomcode04.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode04.getLength()%>'
maxLength='<%= sv.zcomcode04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode04)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode04).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc04) %>'
	 <%}%>

size='<%= sv.zrspperc04.getLength()%>'
maxLength='<%= sv.zrspperc04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc04).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc04) %>'
	 <%}%>

size='<%= sv.zrtuperc04.getLength()%>'
maxLength='<%= sv.zrtuperc04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc04).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode05' 
type='text'

<%

		formatValue = (sv.zcomcode05.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode05.getLength()%>'
maxLength='<%= sv.zcomcode05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode05)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode05).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc05) %>'
	 <%}%>

size='<%= sv.zrspperc05.getLength()%>'
maxLength='<%= sv.zrspperc05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc05).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc05) %>'
	 <%}%>

size='<%= sv.zrtuperc05.getLength()%>'
maxLength='<%= sv.zrtuperc05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc05).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode06' 
type='text'

<%

		formatValue = (sv.zcomcode06.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode06.getLength()%>'
maxLength='<%= sv.zcomcode06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode06)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode06).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc06) %>'
	 <%}%>

size='<%= sv.zrspperc06.getLength()%>'
maxLength='<%= sv.zrspperc06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc06).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc06) %>'
	 <%}%>

size='<%= sv.zrtuperc06.getLength()%>'
maxLength='<%= sv.zrtuperc06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc06).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode07' 
type='text'

<%

		formatValue = (sv.zcomcode07.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode07.getLength()%>'
maxLength='<%= sv.zcomcode07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode07)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode07).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc07) %>'
	 <%}%>

size='<%= sv.zrspperc07.getLength()%>'
maxLength='<%= sv.zrspperc07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc07).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc07) %>'
	 <%}%>

size='<%= sv.zrtuperc07.getLength()%>'
maxLength='<%= sv.zrtuperc07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc07).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode08' 
type='text'

<%

		formatValue = (sv.zcomcode08.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode08.getLength()%>'
maxLength='<%= sv.zcomcode08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode08)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode08).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc08) %>'
	 <%}%>

size='<%= sv.zrspperc08.getLength()%>'
maxLength='<%= sv.zrspperc08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc08).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc08) %>'
	 <%}%>

size='<%= sv.zrtuperc08.getLength()%>'
maxLength='<%= sv.zrtuperc08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc08).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode09' 
type='text'

<%

		formatValue = (sv.zcomcode09.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode09.getLength()%>'
maxLength='<%= sv.zcomcode09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode09)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode09).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc09) %>'
	 <%}%>

size='<%= sv.zrspperc09.getLength()%>'
maxLength='<%= sv.zrspperc09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc09).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc09) %>'
	 <%}%>

size='<%= sv.zrtuperc09.getLength()%>'
maxLength='<%= sv.zrtuperc09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc09).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>


<input name='zcomcode10' 
type='text'

<%

		formatValue = (sv.zcomcode10.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.zcomcode10.getLength()%>'
maxLength='<%= sv.zcomcode10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zcomcode10)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.zcomcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zcomcode10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zcomcode10).getColor()== null  ? 
			"input_cell" :  (sv.zcomcode10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrspperc10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrspperc10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrspperc10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrspperc10) %>'
	 <%}%>

size='<%= sv.zrspperc10.getLength()%>'
maxLength='<%= sv.zrspperc10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrspperc10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrspperc10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrspperc10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrspperc10).getColor()== null  ? 
			"input_cell" :  (sv.zrspperc10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.zrtuperc10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S1ZEROS1VS2);
			
	%>

<input name='zrtuperc10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.zrtuperc10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.zrtuperc10) %>'
	 <%}%>

size='<%= sv.zrtuperc10.getLength()%>'
maxLength='<%= sv.zrtuperc10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(zrtuperc10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.zrtuperc10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrtuperc10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrtuperc10).getColor()== null  ? 
			"input_cell" :  (sv.zrtuperc10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%>
</div>



<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>


<input name='contitem' 
type='text'

<%

		formatValue = (sv.contitem.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<br/>&nbsp; &nbsp; &nbsp;</td></tr></table><br/></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>

<%@ include file="/POLACommon2NEW.jsp"%>

