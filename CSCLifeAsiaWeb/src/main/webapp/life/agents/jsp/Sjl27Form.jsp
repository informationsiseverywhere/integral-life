<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl27";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>

<%
	Sjl27ScreenVars sv = (Sjl27ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind05.isOn()) {
			sv.cmgwcolcat.setReverse(BaseScreenData.REVERSED);
			sv.cmgwcolcat.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.cmgwcolcat.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.cmgwcolcat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.cmgwcpdte.setReverse(BaseScreenData.REVERSED);
			sv.cmgwcpdte.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.cmgwcpdte.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.cmgwcpdte.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.cmgwcprot.setReverse(BaseScreenData.REVERSED);
			sv.cmgwcprot.setColor(BaseScreenData.RED);
		}
		if (appVars.ind27.isOn()) {
			sv.cmgwcprot.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.cmgwcprot.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top:90px;">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Collection Category")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cmgwcolcat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
						%>
						<div style="width: 40px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cmgwcolcat)%>
						</div>
						<%
							} else {
						%>
						<div class="input-group" style="width: 75px;">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmgwcolcat)%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('cmgwcolcat')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Date")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cmgwcpdte).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
						%>
						<div style="width: 40px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cmgwcpdte)%>
						</div>
						<%
							} else {
						%>
						<div class="input-group" style="width: 75px;">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmgwcpdte)%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('cmgwcpdte')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Route")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cmgwcprot).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
						%>
						<div style="width: 40px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cmgwcprot)%>
						</div>
						<%
							} else {
						%>
						<div class="input-group" style="width: 75px;">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmgwcprot)%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('cmgwcprot')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- Close div panel-body -->
</div> <!-- Close div panel panel-default -->
<%@ include file="/POLACommon2NEW.jsp"%>