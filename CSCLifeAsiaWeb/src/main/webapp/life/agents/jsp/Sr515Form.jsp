<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR515";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr515ScreenVars sv = (Sr515ScreenVars) fw.getVariables();%>

<%if (sv.Sr515screenWritten.gt(0)) {%>
	<%Sr515screen.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.Sr515screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("sr515screensfl");
	savedInds = appVars.saveAllInds();
	Sr515screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 12;
	String height = smartHF.fmty(12);
	smartHF.setSflLineOffset(10);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(10)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (Sr515screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.prcnt.setClassString("");%>
	<%sv.dteappDisp.setClassString("");%>
	<%sv.zrreptp.setClassString("");%>
	<%sv.deit.setClassString("");%>
<%	sv.deit.appendClassString("string_fld");
	sv.deit.appendClassString("output_txt");
	sv.deit.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>

	<%
{
		if (appVars.ind21.isOn()) {
			sv.dteappDisp.setReverse(BaseScreenData.REVERSED);
			sv.dteappDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.dteappDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.zrreptp.setReverse(BaseScreenData.REVERSED);
			sv.zrreptp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.zrreptp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.prcnt.setReverse(BaseScreenData.REVERSED);
			sv.prcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.prcnt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 4, sfl, sv.prcnt, COBOLHTMLFormatter.S3VS2)%>
		<%=smartHF.getHTMLSFSpaceVar(sflLine, 18, sfl, sv.dteappDisp)%>
		<%=smartHF.getHTMLCalSSVar(sflLine, 18, sfl, sv.dteappDisp)%>
		<%=smartHF.getHTMLSFSpaceVar(sflLine, 31, sfl, sv.zrreptp)%>
		<%=smartHF.getHTMLF4SSVar(sflLine, 31, sfl, sv.zrreptp)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 44, sfl, sv.deit)%>

		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		Sr515screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.Sr515protectWritten.gt(0)) {%>
	<%Sr515protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.Sr515screenctlWritten.gt(0)) {%>
	<%Sr515screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("sr515screensfl");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client       ");%>
	<%sv.clntsel.setClassString("");%>
<%	sv.clntsel.appendClassString("string_fld");
	sv.clntsel.appendClassString("output_txt");
	sv.clntsel.appendClassString("highlight");
%>
	<%sv.cltname.setClassString("");%>
<%	sv.cltname.appendClassString("string_fld");
	sv.cltname.appendClassString("output_txt");
	sv.cltname.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%sv.agnum.setClassString("");%>
<%	sv.agnum.appendClassString("string_fld");
	sv.agnum.appendClassString("output_txt");
	sv.agnum.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type   ");%>
	<%sv.agtype.setClassString("");%>
<%	sv.agtype.appendClassString("string_fld");
	sv.agtype.appendClassString("output_txt");
	sv.agtype.appendClassString("highlight");
%>
	<%sv.agtydesc.setClassString("");%>
<%	sv.agtydesc.appendClassString("string_fld");
	sv.agtydesc.appendClassString("output_txt");
	sv.agtydesc.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Percentage");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reporting To");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

	<%=smartHF.getLit(3, 2, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(3, 19, fw, sv.clntsel)%>
	<%=smartHF.getHTMLF4NSVar(3, 19, fw, sv.clntsel)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.cltname)%>

	<%=smartHF.getLit(4, 2, generatedText6)%>

	<%=smartHF.getHTMLVar(4, 19, fw, sv.agnum)%>

	<%=smartHF.getLit(5, 2, generatedText7)%>

	<%=smartHF.getHTMLSpaceVar(5, 19, fw, sv.agtype)%>
	<%=smartHF.getHTMLF4NSVar(5, 19, fw, sv.agtype)%>

	<%=smartHF.getHTMLVar(5, 31, fw, sv.agtydesc)%>

	<%=smartHF.getLit(8, 2, generatedText2)%>

	<%=smartHF.getLit(8, 17, generatedText3)%>

	<%=smartHF.getLit(8, 32, generatedText4)%>





<%}%>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
