<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM604";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm604ScreenVars sv = (Sm604ScreenVars) fw.getVariables();%>
<%{
}%>




<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                          <%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

           </div></div>
           <div class="col-md-4"> 
			    	     <div class="form-group">
            <label><%=resourceBundleHandler.gettingValueFromBundle("Table:")%></label>
            <%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>


</div></div>















<div class="col-md-4"> 
			    	     <div class="form-group">
            <label><%=resourceBundleHandler.gettingValueFromBundle("Item:")%></label>
            <div class="input-group">
             <%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>' style="min-width:50px;">

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>





<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

						"blank_cell" : "output_cell" %>' style="width:100px;">

				<%=XSSFilter.escapeHtml(formatValue)%>

			</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</div>

</div></div>




</div>






















<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<br>
              <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
			    	    </div></div></div>
			    	     
<div class="row">	
			    	<div class="col-md-5" style="padding-right:0" > 
			    	     <div class="form-group">
          <table>
							<tr>
								<td>

									<%
										if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										} else {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>


								<td><%=resourceBundleHandler.gettingValueFromBundle("&nbsp; to &nbsp;")%></td>

								<td>
									<%
										if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										} else {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> '>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
							</tr>
						</table>

                   </div>
                 </div>
                 </div>

<br><!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->








                 <div class="row">	
			    	<div class="col-md-4"> 
                       <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Report Title")%></label>
 <div class="input-group">
<%if(((BaseScreenData)sv.rpthead) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rpthead,( sv.rpthead.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.rpthead) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rpthead, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div></div></div></div>
<br>
 <div class="row">	
			    	<div class="col-md-4"> 
                       <div class="form-group">
                       
<label><%=resourceBundleHandler.gettingValueFromBundle("Key Result Areas ")%></label>

</div></div></div>

<div class="row">	
			    	<div class="col-md-4"> 
                       <div class="form-group">
                       
                         <label><%=resourceBundleHandler.gettingValueFromBundle("Min FYP For Group")%></label>
                         <div class="input-group">
<%if(((BaseScreenData)sv.prem01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.prem01,( sv.prem01.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.prem01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.prem01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div>
                </div></div>
                
                
                <div class="col-md-4"> 
                       <div class="form-group">
                       
                         <label><%=resourceBundleHandler.gettingValueFromBundle("And FYP For Direct Agent")%></label>
 
 <div class="input-group">
<%if(((BaseScreenData)sv.prem02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.prem02,( sv.prem02.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.prem02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.prem02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div>
                </div></div>
                
                
                <div class="col-md-4"> 
                       <div class="form-group">
                       
                         <label><%=resourceBundleHandler.gettingValueFromBundle("Min Cases")%></label>
<div class="input-group">
<%if(((BaseScreenData)sv.numofmbr) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.numofmbr,( sv.numofmbr.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.numofmbr) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.numofmbr, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div>
                </div></div>
                
                </div>


                
                
               <br> 
                </div></div>











<%-- <div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:42px;'>
<td width='251'>
<!--
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company:");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<br/>
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table:")%>
</div>
<br/>
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item:")%>
</div>
<br/>
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:42px;'>
<td width='251'>

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 Start-->
	 
<!--
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective/To");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Valid From")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%>

</td>

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 ends-->
	 
	 
<!-- END TD FOR ROW 2,5 etc -->

</tr>
 <tr style='height:42px;'>
<td width='251'>
<!--
<%StringData RPTHEAD_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Report Title");%>
<%=smartHF.getLit(0, 0, RPTHEAD_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Report Title")%>
</div>
<br/>
<%if(((BaseScreenData)sv.rpthead) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rpthead,( sv.rpthead.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.rpthead) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rpthead, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
</td>

</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='255'>
<!--
<%StringData SM604_KeyResult_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Key Result Areas ");%>
<%=smartHF.getLit(0, 0, SM604_KeyResult_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Key Result Areas ")%>
</div>
<br/>
</td><td width='255'></td><td width='255'></td>
</tr>

<tr style='height:22px;'>
<td width='255'>
<!--
<%StringData PREM01_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min FYP For Group");%>
<%=smartHF.getLit(0, 0, PREM01_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Min FYP For Group")%>
</div>
<br/>

<%if(((BaseScreenData)sv.prem01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.prem01,( COBOLHTMLFormatter.getLengthWithCommas( sv.prem01.getLength(), sv.prem01.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.prem01) instanceof DecimalData){%>
<%=smartHF.getHTMLVarExt(fw, sv.prem01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,130)%>
<%}else {%>
hello
<%}%>
</td>
<td width='255'>
<!--
<%StringData PREM02_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"And FYP For Direct Agent");%>
<%=smartHF.getLit(0, 0, PREM02_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("And FYP For Direct Agent")%>
</div>
<br/>

<%if(((BaseScreenData)sv.prem02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.prem02,( COBOLHTMLFormatter.getLengthWithCommas( sv.prem02.getLength(), sv.prem02.getScale(),3)+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.prem02) instanceof DecimalData){%>
<%=smartHF.getHTMLVarExt(fw, sv.prem02, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 0,130)%>
<%}else {%>
hello
<%}%>
</td><td width='255'>
<!--
<%StringData NUMOFMBR_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min Cases");%>
<%=smartHF.getLit(0, 0, NUMOFMBR_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Min Cases")%>
</div>
<br/>

<%if(((BaseScreenData)sv.numofmbr) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.numofmbr,( sv.numofmbr.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.numofmbr) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.numofmbr, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr> </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
