<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5694";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5694ScreenVars sv = (S5694ScreenVars) fw.getVariables();%>


	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>

	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>

	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>

	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>

	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid to ");%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment");%>

<div class="panel panel-default">	
    <div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group">  
					
				<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

				</div>
			</div>
          </div>
			<div class="col-md-2"></div>
  	          
  	          <div class="col-md-2" >
  					<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%					
		                     if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
					<div
						class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
	                     	longValue = null;
		                    formatValue = null;
		                   %>
				</div>
			</div>

 <div class="col-md-2"></div>
            
            
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>

<div class="row">
	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
					<table>
					<tr>
					<td>
                    
						<%
							if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

                      </td>
					<td>&nbsp;to&nbsp;</td>
                       <td>
						<%
							if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="min-width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
                   </tr>
                   </table>
					
				</div>
			</div>

</div>
<!-- <div class="row"></div> -->
 &nbsp;&nbsp;
  <div class="row">
   	<div class="col-md-1" style="max-width:110px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Year"))%></label>
     </div>
     
     		<div class="col-md-2" style="max-width:180px;text-align: center;"> 
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("% of Instalment"))%></label>
     </div>
     </div>
   
   <div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear01,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc01,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear02,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc02,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear03,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc03,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear04,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc04,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear05,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc05,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear06,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc06,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear07,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc07,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear08,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc08,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear09,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc09,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear10,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc10,1,65)%>
	 </div>
	</div>
	
	<div class="row">
    <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.toYear11,1,65)%>
	 </div>
	 <div class="col-md-2">
	<%=smartHF.getHTMLVarExt(fw,sv.instalpc11,1,65)%>
	 </div>
	</div>
	
				  
	</div>
	</div>
 


<%@ include file="/POLACommon2NEW.jsp"%>



