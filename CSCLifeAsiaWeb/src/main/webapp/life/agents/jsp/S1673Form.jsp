<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S1673";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>

<%
	S1673ScreenVars sv = (S1673ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind05.isOn()) {
			sv.cmgwmop.setReverse(BaseScreenData.REVERSED);
			sv.cmgwmop.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.cmgwmop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.cmgwmop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.cmgwspltyp.setReverse(BaseScreenData.REVERSED);
			sv.cmgwspltyp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.cmgwspltyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.cmgwspltyp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top:90px;">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mode of Payment")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cmgwmop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
						%>
						<div style="width: 40px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cmgwmop)%>
						</div>
						<%
							} else {
						%>
						<div class="input-group" style="width: 75px;">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmgwmop)%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('cmgwmop')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Split Type")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cmgwspltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {
						%>
						<div style="width: 40px;">
							<%=smartHF.getHTMLVarExt(fw, sv.cmgwspltyp)%>
						</div>
						<%
							} else {
						%>
						<div class="input-group" style="width: 75px;">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.cmgwspltyp)%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('cmgwspltyp')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- Close div panel-body -->
</div> <!-- Close div panel panel-default -->
<%@ include file="/POLACommon2NEW.jsp"%>