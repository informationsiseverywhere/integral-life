<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR60D";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%//@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr60dScreenVars sv = (Sr60dScreenVars) fw.getVariables();%>
<%{
	if (appVars.ind02.isOn()) {
		sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
		sv.datefrmDisp.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind02.isOn()) {
		sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind03.isOn()) {
		sv.datefrmDisp.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind04.isOn()) {
		sv.datefrmDisp.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind05.isOn()) {
		sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
		sv.datetoDisp.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.datetoDisp.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind07.isOn()) {
		sv.datetoDisp.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind08.isOn()) {
		sv.zsgtpct.setReverse(BaseScreenData.REVERSED);
		sv.zsgtpct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind08.isOn()) {
		sv.zsgtpct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind09.isOn()) {
		sv.zsgtpct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind10.isOn()) {
		sv.zsgtpct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind11.isOn()) {
		sv.zoerpct.setReverse(BaseScreenData.REVERSED);
		sv.zoerpct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind11.isOn()) {
		sv.zoerpct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind12.isOn()) {
		sv.zoerpct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind13.isOn()) {
		sv.zoerpct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind14.isOn()) {
		sv.zdedpct.setReverse(BaseScreenData.REVERSED);
		sv.zdedpct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind14.isOn()) {
		sv.zdedpct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind15.isOn()) {
		sv.zdedpct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind16.isOn()) {
		sv.zdedpct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind17.isOn()) {
		sv.zundpct.setReverse(BaseScreenData.REVERSED);
		sv.zundpct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind17.isOn()) {
		sv.zundpct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind18.isOn()) {
		sv.zundpct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind19.isOn()) {
		sv.zundpct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind20.isOn()) {
		sv.totprcnt.setReverse(BaseScreenData.REVERSED);
		sv.totprcnt.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind20.isOn()) {
		sv.totprcnt.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind21.isOn()) {
		sv.totprcnt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind22.isOn()) {
		sv.totprcnt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	if (appVars.ind23.isOn()) {
		sv.zspspct.setReverse(BaseScreenData.REVERSED);
		sv.zspspct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind23.isOn()) {
		sv.zspspct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind24.isOn()) {
		sv.zspspct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind25.isOn()) {
		sv.zspspct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	/*ILIFE-7345 start*/
		if (appVars.ind29.isOn()) {
		sv.zslryspct.setReverse(BaseScreenData.REVERSED);
		sv.zslryspct.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind29.isOn()) {
		sv.zslryspct.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind30.isOn()) {
		sv.zslryspct.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind32.isOn()) {
		sv.zslryspct.setInvisibility(BaseScreenData.INVISIBLE);
	}
	/*ILIFE-7345 end*/
	if (appVars.ind28.isOn()) {
		sv.totamnt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	/* ILIFE-7916 start */
	
	/*  if (appVars.ind45.isOn()) {
		sv.coContriPmt.setReverse(BaseScreenData.REVERSED);
		sv.coContriPmt.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind46.isOn()) {
		sv.coContriPmt.setHighLight(BaseScreenData.BOLD);
	}  */
	if (appVars.ind48.isOn()) {
		sv.coContriPmt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
/*   if (appVars.ind49.isOn()) {
		sv.coContriPmt.setReverse(BaseScreenData.REVERSED);
		sv.coContriPmt.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind49.isOn()) {
		sv.coContriPmt.setHighLight(BaseScreenData.BOLD);
	}  */
	if (appVars.ind52.isOn()) {
		sv.receivedFromAto.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	
	/* ILIFE-7916 END */
}%>


<div class="panel panel-default">
	<div class="panel-body">

	<div class="row">	

	<div class="col-md-6">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
			<table>
			<tr>
			<td>
			<%					
			if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:60px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
	
			</td>
			<td>
	
			<%					
			if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:30px;margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
	  		</td>
	  		<td>
	  		
			<%					
			if(!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ctypdesc.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:200px;margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
			</td>
		</tr>
		</table>
		</div>
	</div>

	</div>
	
	<div class="row">
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
		<table>
		<tr>
		<td>

		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style='margin-right:1px;'>
				<%=sv.lifenum.getFormData()%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		<td>
		<%					
		if(!((sv.lifedesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifedesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 200px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
		
		</div>
	</div>


	</div>
	
	<div class="row">


	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Tax period")%></label>
			<table>
			<tr>
			<td>
			
			 <% if ((new Byte((sv.datefrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp)%>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datefrmDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp, (sv.datefrmDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
			
		
			</td>
			
			
			<td>
				<label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label>				
			</td>
			
			<td style="padding-left: 5px;">
			
			
			 <% if ((new Byte((sv.datetoDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                 <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp)%>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="datetoDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp, (sv.datetoDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
			
			
			</td>
			</tr>
			</table>

		</div>
	</div>
	

	</div>
	
	<div class="row">
	<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
			<div class="form-group">			
			<label><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></label>			
			</div>
		</div>		
	<%} %>
	</div>
	
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Employer contribution")%></label>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Super guarantee")%></label>
			</div>
		</div>
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
			<div class="form-group">
			
				<%	
							qpsf = fw.getFieldXMLDef((sv.zsgtamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.zsgtamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>
				
				<input name='zsgtamt' 
				type='text' <%if((sv.zsgtamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
				readonly="true"
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zsgtamt.getLength(), sv.zsgtamt.getScale(),3)%>' 
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zsgtamt.getLength(), sv.zsgtamt.getScale(),3)-3%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zsgtamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
						<%
						if((new Byte((sv.totamnt).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
					%>
					readonly="true"
					class="output_cell"	

					<%
						}else if((new Byte((sv.totamnt).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){

					%>
					class="bold_cell" 
					<%
						}else {
					%>

					class = ' <%=(sv.totamnt).getColor()== null  ?
					"input_cell" :  (sv.totamnt).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' 

					<%} %>>
			</div>
		</div>
		<%} %>
		<%	
			qpsf = fw.getFieldXMLDef((sv.zsgtpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zsgtpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
		<div class="col-md-3">
			<div class="form-group">
			<table>
			<tr>
			<td>
			<input name='zsgtpct' id='zsgtpct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.zsgtpct.getLength()%>'
			size='<%=sv.zsgtpct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(zsgtpct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:50px; text-align:right;"
		
			<%
				if((new Byte((sv.zsgtpct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			class="output_cell"	
			
			<%
				}else if((new Byte((sv.zsgtpct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.zsgtpct).getColor()== null  ?
			"input_cell" :  (sv.zsgtpct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</td>
			
			<td><label style="margin-left: 5px;"> <%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
			</tr>
			</table>
			</div>
		</div>
	
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<label></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Other")%></label>
			</div>
		</div>
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
			<div class="form-group">
				<%	
						qpsf = fw.getFieldXMLDef((sv.zoeramt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis=smartHF.getPicFormatted(qpsf,sv.zoeramt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						%>
					
					<input name='zoeramt' 
					type='text' <%if((sv.zoeramt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
					readonly="true"
						value='<%=valueThis%>'
								 <%	 
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=valueThis%>'
						 <%}%>
					
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zoeramt.getLength(), sv.zoeramt.getScale(),3)%>' 
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zoeramt.getLength(), sv.zoeramt.getScale(),3)-3%>' 
					onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zoeramt)' onKeyUp='return checkMaxLength(this)'  
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
					
						<%
							if((new Byte((sv.totamnt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
						readonly="true"
						class="output_cell"	
					
					<%
					}else if((new Byte((sv.totamnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
					%>
						class="bold_cell" 
					<%
					}else {
					%>
				
					class = ' <%=(sv.totamnt).getColor()== null  ?
				"input_cell" :  (sv.totamnt).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
				
				<%} %>>
			</div>
		</div>
		<%} %>
		
		
		
		
		
			<%	
			qpsf = fw.getFieldXMLDef((sv.zoerpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zoerpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			%>
			<div class="col-md-3">
				<div class="form-group">
				<table>
				<tr>
				<td>								
					<input name='zoerpct' id='zoerpct'
					type='text'
					value='<%=valueThis%>'
					maxLength='<%=sv.zoerpct.getLength()%>'
					size='<%=sv.zoerpct.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(zoerpct)' onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					style="width:50px;  text-align:right;"
					<%
						if((new Byte((sv.zoerpct).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
					%>
					class="output_cell"	
					
					<%
						}else if((new Byte((sv.zoerpct).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
					%>
					class="bold_cell" 
					<%
						}else {
					%>
					
					class = ' <%=(sv.zoerpct).getColor()== null  ?
					"input_cell" :  (sv.zoerpct).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' 
					
					<%} %>>
				</td>
				<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
			</tr>
			</table>
		
			</div>
		</div>	
	
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Member contributions")%></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Concessional")%></label>
			</div>
		</div>
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
			<div class="form-group">
			<%	
			qpsf = fw.getFieldXMLDef((sv.zdedamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.zdedamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
					%>
				
				<input name='zdedamt' 
				type='text' <%if((sv.zdedamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
				readonly="true"
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zdedamt.getLength(), sv.zdedamt.getScale(),3)%>' 
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zdedamt.getLength(), sv.zdedamt.getScale(),3)-3%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zdedamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
					<%
				if((new Byte((sv.totamnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
					readonly="true"
					class="output_cell"	
			
				<%
				}else if((new Byte((sv.totamnt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
				%>
					class="bold_cell" 
				<%
					}else {
				%>
			
				 class = ' <%=(sv.totamnt).getColor()== null  ?
				"input_cell" :  (sv.totamnt).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</div>
		</div>
		
		<%} %>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zdedpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zdedpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
		%>
		
		<div class="col-md-3">
			<div class="form-group">
			<table>
			<tr>
			<td>
				<input name='zdedpct' id='zdedpct'
				type='text'
				value='<%=valueThis%>'
				maxLength='<%=sv.zdedpct.getLength()%>'
				size='<%=sv.zdedpct.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(zdedpct)' onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
				style="width:50px;  text-align:right;"
				<%
					if((new Byte((sv.zdedpct).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
			
				class="output_cell"	
				
				<%
					}else if((new Byte((sv.zdedpct).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
				%>
				class="bold_cell" 
				<%
					}else {
				%>
				
				class = ' <%=(sv.zdedpct).getColor()== null  ?
				"input_cell" :  (sv.zdedpct).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
				
				<%} %>>
			</td>
			<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
			</tr>
			</table>
			</div>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Non_Concessional")%></label>
			</div>
		</div>
		
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>		
		
		<div class="col-md-3">
			<div class="form-group">
			
			<%	
			qpsf = fw.getFieldXMLDef((sv.zundamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.zundamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
					%>
				
				<input name='zundamt' 
				type='text' <%if((sv.zundamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
				readonly="true"
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zundamt.getLength(), sv.zundamt.getScale(),3)%>' 
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zundamt.getLength(), sv.zundamt.getScale(),3)-3%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zundamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
					<%
					if((new Byte((sv.totamnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
					%>
						readonly="true"
						class="output_cell"	
				
					<%
					}else if((new Byte((sv.totamnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
						class="bold_cell" 
					<%
						}else {
					%>
				
					class = ' <%=(sv.totamnt).getColor()== null  ?
					"input_cell" :  (sv.totamnt).getColor().equals("red") ?
					"input_cell red reverse" : "input_cell" %>' 
				
					<%} %>>
			
			</div>
		</div>
		<%} %>
		<%	
			qpsf = fw.getFieldXMLDef((sv.zundpct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zundpct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
		%>
		<div class="col-md-3">
			<div class="form-group">
			<table>
			<tr>
			<td>
			
			<input name='zundpct' id='zundpct'
				type='text'
				value='<%=valueThis%>'
				maxLength='<%=sv.zundpct.getLength()%>'
				size='<%=sv.zundpct.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(zundpct)' onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
				style="width:50px;  text-align:right;"
				<%
					if((new Byte((sv.zundpct).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
				
				class="output_cell"	
				
				<%
					}else if((new Byte((sv.zundpct).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
				%>
				class="bold_cell" 
				<%
					}else {
				%>
				
				class = ' <%=(sv.zundpct).getColor()== null  ?
				"input_cell" :  (sv.zundpct).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>'
				
				<%} %>>
				</td>
				<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
				</tr>
				</table>
			</div>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Spouse contributions")%></label>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">			
			</div>
		</div>
		
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		
		<div class="col-md-3">
			<div class="form-group">
			<%	
			qpsf = fw.getFieldXMLDef((sv.zspsamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.zspsamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
					%>
				
				<input name='zspsamt' 
				type='text' <%if((sv.zspsamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
				readonly="true"
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zspsamt.getLength(), sv.zspsamt.getScale(),3)%>' 
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zspsamt.getLength(), sv.zspsamt.getScale(),3)-3%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zspsamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					readonly="true"
				
				<%
					if((new Byte((sv.totamnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
					
					class="output_cell"	
				<%
					}else if((new Byte((sv.totamnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
				class="bold_cell" 
				<%
					}else {
				%>
				
				class = ' <%=(sv.totamnt).getColor()== null  ?
				"input_cell" :  (sv.totamnt).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
				
				<%} %>>
			</div>
		</div>
		
		<%} %>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zspspct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zspspct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);		
		%>
		
		<div class="col-md-3">
			<div class="form-group">
			
			<table>
			<tr>
			<td>
			
			<input name='zspspct' id='zspspct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.zspspct.getLength()%>'
			size='<%=sv.zspspct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(zspspct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:50px; text-align:right;"
			<%
				if((new Byte((sv.zspspct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.zspspct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.zspspct).getColor()== null  ?
			"input_cell" :  (sv.zspspct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</td>
			<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
			</tr>
			</table>
		
			</div>
		</div>
	
	
	</div>
<!-- ILIFE-7345 start-->
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Salary Sacrifice")%></label>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">			
			</div>
		</div>
		
		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		
		<div class="col-md-3">
			<div class="form-group">
			<%	
			qpsf = fw.getFieldXMLDef((sv.zslrysamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.zslrysamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
					%>
				
				<input name='zslrysamt' 
				type='text' <%if((sv.zslrysamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
				readonly="true"
					value='<%=valueThis%>'
							 <%	 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=valueThis%>'
					 <%}%>
				
				size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zslrysamt.getLength(), sv.zslrysamt.getScale(),3)%>' 
				maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zslrysamt.getLength(), sv.zslrysamt.getScale(),3)-3%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zslrysamt)' onKeyUp='return checkMaxLength(this)'  
					
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					readonly="true"
				
				<%
					if((new Byte((sv.totamnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
					
					class="output_cell"	
				<%
					}else if((new Byte((sv.totamnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>
				class="bold_cell" 
				<%
					}else {
				%>
				
				class = ' <%=(sv.totamnt).getColor()== null  ?
				"input_cell" :  (sv.totamnt).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
				
				<%} %>>
			</div>
		</div>
		
		<%} %>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zslryspct).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.zslryspct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);		
		%>
		
		<div class="col-md-3">
			<div class="form-group">
			
			<table>
			<tr>
			<td>
			
			<input name='zslryspct' id='zslryspct'
			type='text'
			value='<%=valueThis%>'
			maxLength='<%=sv.zslryspct.getLength()%>'
			size='<%=sv.zslryspct.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(zslryspct)' onKeyUp='return checkMaxLength(this)'
			onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
			style="width:50px; text-align:right;"
			<%
				if((new Byte((sv.zslryspct).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
			%>
			
			class="output_cell"	
		
			<%
				}else if((new Byte((sv.zslryspct).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			
			%>
			class="bold_cell" 
			<%
				}else {
			%>
			
			class = ' <%=(sv.zslryspct).getColor()== null  ?
			"input_cell" :  (sv.zslryspct).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>' 
			
			<%} %>>
			</td>
			<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
			</tr>
			</table>
			</div>
		</div>
	</div>
	<!-- ILIFE-7345 end-->
	<!-- ILIFE-7916 START -->
			
				<div class="row">
					<%
				if ((new Byte((sv.coContriPmt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
				
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Co-Contributions Payment")%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group"></div>
			</div>
	
			<div class="col-md-3">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.coContriPmt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.coContriPmt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='coContriPmt' type='text'
						<%if ((sv.coContriPmt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%> readonly="true"
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.coContriPmt.getLength(), sv.coContriPmt.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.coContriPmt.getLength(), sv.coContriPmt.getScale(), 3)
						- 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(coContriPmt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);' readonly="true"
						<%if ((new Byte((sv.totamnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						class="output_cell"
						<%} else if ((new Byte((sv.totamnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.totamnt).getColor() == null ? "input_cell"
							: (sv.totamnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<%
				}
			%>

			<%
				qpsf = fw.getFieldXMLDef((sv.coContriPmt).getFieldName());
				valueThis = smartHF.getPicFormatted(qpsf, sv.coContriPmt,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>
	</div>
		
		
		
			<div class="row">
			
			<%
				if ((new Byte((sv.receivedFromAto).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("LISC Payment")%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group"></div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.receivedFromAto).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.receivedFromAto,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='receivedFromAto' type='text'
						<%if ((sv.receivedFromAto).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%> readonly="true"
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.receivedFromAto.getLength(), sv.receivedFromAto.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.receivedFromAto.getLength(), sv.receivedFromAto.getScale(), 3)
						- 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(receivedFromAto)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);' readonly="true"
						<%if ((new Byte((sv.totamnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						class="output_cell"
						<%} else if ((new Byte((sv.totamnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.totamnt).getColor() == null ? "input_cell"
							: (sv.totamnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<%
				}
			%>

			<%
				qpsf = fw.getFieldXMLDef((sv.receivedFromAto).getFieldName());
				valueThis = smartHF.getPicFormatted(qpsf, sv.receivedFromAto,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>

			</div>
			
			
				<!-- ILIFE-7916 ENd-->
		
	<div class="row">
	<!-- Ticket #ILIFE-6335 Modify Contribution Details has issues (mverma54 ) -->
		<%	
			qpsf = fw.getFieldXMLDef((sv.totprcnt).getFieldName());
			valueThis=smartHF.getPicFormatted(qpsf,sv.totprcnt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);		
		%>

			<div class="row">
			<!-- Ticket #ILIFE-6335 Modify Contribution Details has issues (mverma54 ) -->
			<%
				qpsf = fw.getFieldXMLDef((sv.totprcnt).getFieldName());
				valueThis = smartHF.getPicFormatted(qpsf, sv.totprcnt,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>
	<!-- ILIFE-7916 START -->
			<div class="col-md-6">
				<div class="form-group"></div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Contributions")%></label>
				</div>
			
			<%
				if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

		
<table>
					<tr>
						<td>
					<%
						qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.totamnt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='totamnt' type='text'
						<%if ((sv.totamnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%> readonly="true"
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.totamnt.getLength(), sv.totamnt.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.totamnt.getLength(), sv.totamnt.getScale(), 3) - 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(totamnt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.totamnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						class="output_cell"
						<%} else if ((new Byte((sv.totamnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.totamnt).getColor() == null ? "input_cell"
							: (sv.totamnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
						</td>
						</tr>
						</table>
				</div>
				
			
			<%
				}
			%>
			<!-- ILIFE-7916 END -->
		<%-- <div class="col-md-3">
			<div class="form-group">
			
				<table>
				<tr>
				<td>
			 <%	
			qpsf = fw.getFieldXMLDef((sv.totprcnt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.totprcnt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			%>
				<input name='totprcnt' id='totprcnt'
				type='text'
				value='<%=valueThis%>'
				maxLength='<%=sv.totprcnt.getLength()%>'
				size='<%=sv.totprcnt.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(totprcnt)' onKeyUp='return checkMaxLength(this)'
				style="width:50px; text-align:right;"
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
				decimal='<%=qpsf.getDecimals()%>' 
				onPaste='return doPasteNumber(event,true);'
				onBlur='return doBlurNumberNew(event,true);'
					readonly="true"
				<%
					if((new Byte((sv.totprcnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
				%>
				
				class="output_cell"	
				
				<%
					}else if((new Byte((sv.totprcnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
				%>
				class="bold_cell" 
				<%
					}else {
				%>
				
				class = ' <%=(sv.totprcnt).getColor()== null  ?
				"input_cell" :  (sv.totprcnt).getColor().equals("red") ?
				"input_cell red reverse" : "input_cell" %>' 
				
				<%} %>>
				</td>
				<td><label style="margin-left: 5px;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></label></td>
				</tr>
				</table>	
			</div>
		</div> --%>
		
	</div>
	
	
	</div>
	
	<!-- ILIFE-7916 START -->
	
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("S290 Notice Received")%></label>
				<table>
					<tr>
						<td>
							<%
								if (!((sv.s290NoticeReceived.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.s290NoticeReceived.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.s290NoticeReceived.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
								style="width: 60px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>

						</td>
					</tr>
				</table>
			</div>



		</div>


		<div class="col-md-4">
			<div class="form-group">
			<div>
				<label style="position: relative;left: 15px;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Premium")%></label>
				<table>
					<tr>
						<td>
							
			<%
				if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.riskPrem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.riskPrem,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='riskPrem' type='text'
						<%if ((sv.riskPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right ;right:140px; width: 145px; " <%}%> readonly="true"
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.riskPrem.getLength(), sv.riskPrem.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.riskPrem.getLength(), sv.riskPrem.getScale(), 3)
						- 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(riskPrem)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);' readonly="true"
						<%if ((new Byte((sv.totamnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						class="output_cell"
						<%} else if ((new Byte((sv.totamnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.totamnt).getColor() == null ? "input_cell"
							: (sv.totamnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<%
				}
			%>

			<%
				qpsf = fw.getFieldXMLDef((sv.riskPrem).getFieldName());
				valueThis = smartHF.getPicFormatted(qpsf, sv.riskPrem,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>
	
						</td>
					</tr>
				</table>
			</div>

</div>

		</div>
	

	<div class="col-md-4">
			<div class="form-group">
			<div>
				<label style="position: relative;left: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Contributions Tax")%></label>
				<table>
					<tr>
						<td>
							
			<%
				if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-3">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.tottax).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.tottax,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='tottax' type='text'
						<%if ((sv.tottax).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%> readonly="true"
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tottax.getLength(), sv.tottax.getScale(), 3)%>'
						maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tottax.getLength(), sv.tottax.getScale(), 3)
						- 3%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(tottax)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);' readonly="true"
						<%if ((new Byte((sv.totamnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						class="output_cell"
						<%} else if ((new Byte((sv.totamnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.totamnt).getColor() == null ? "input_cell"
							: (sv.totamnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<%
				}
			%>

			<%
				qpsf = fw.getFieldXMLDef((sv.tottax).getFieldName());
				valueThis = smartHF.getPicFormatted(qpsf, sv.tottax,
						COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%>
	
						</td>
					</tr>
				</table>
			</div>

</div>

		</div>

								</div>

								<!-- ILIFE-7916 End -->
								</div>	



<%@ include file="/POLACommon2NEW.jsp"%>