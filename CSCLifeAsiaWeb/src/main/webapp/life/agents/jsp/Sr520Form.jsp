
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR520";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr520ScreenVars sv = (Sr520ScreenVars) fw.getVariables();%>
<%{
}%>



<div class="panel panel-default">
    	   
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
      	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
      <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
         <%=XSSFilter.escapeHtml(formatValue)%>
      </div>	
	<%
	longValue = null;
	formatValue = null;
	%>
         <%}%> 					    		
				      		</div>
				    </div>
			   
			   
			   	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>		
				      		</div>
				    	</div>
			    	
			    	
			    		
			    	  <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    		<div class="input-group">
					    	<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	                        <% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			                if(longValue == null || longValue.equalsIgnoreCase("")) {
				           formatValue = formatValue( (sv.item.getFormData()).toString()); 
			                } else {
				            formatValue = formatValue( longValue);
			               }						
		                   }else{
			               if(longValue == null || longValue.equalsIgnoreCase("")) {
						  formatValue = formatValue( (sv.item.getFormData()).toString()); 
			              }else{
				          formatValue = formatValue( longValue);
			             }
		               }
	                %>
                       <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
                          <%=XSSFilter.escapeHtml(formatValue)%>
                      </div>	
	                <%
	                longValue = null;
	                formatValue = null;
	                %>
                    <%}%>

                  <%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	               <% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		 }else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
                 <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:100px;">
                   <%=XSSFilter.escapeHtml(formatValue)%>
                 </div>	
	     <%
	        longValue = null;
	        formatValue = null;
	     %>
  <%}%>			                     
                         </div>  					    		
				      </div>
				    </div>
			       </div>
			       
			       
			       
			        <div class="row">	
			    	  <div class="col-md-6"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label> 
				    		
				    					    		
				    		<table>
			    	   <tr>
			    	       
			    	     <td>
			    	  
				    			 <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%> 
					    		<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
                                                              
                          </td>


                        
                        <td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>


                      
                        <td>
			                     <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%>
                                
                                 <%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
                         </td>
                      </tr>
                   </table>			    		
				      		 </div></div>
				      		 </div>
				    
			   
		<br><br>
		  
		  
		  
		 
        
			<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-sr520'>
							<thead>
								<tr class='info'>
														<th><%=resourceBundleHandler.gettingValueFromBundle("OR Component Code  ")%></th>									
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year1  ")%></th>
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year2  ")%></th>
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year3  ")%></th>
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year4  ")%></th>
										
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year5  ")%></th>
														
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year6  ")%></th>
														
														<th><%=resourceBundleHandler.gettingValueFromBundle("Year7  ")%></th>
										
		</tr>
		</thead>

 
  <tbody>
   <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode01,( sv.zcomcode01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc01,( sv.zryrperc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc02,( sv.zryrperc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc03,( sv.zryrperc03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc04,( sv.zryrperc04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc05,( sv.zryrperc05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc06,( sv.zryrperc06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc07,( sv.zryrperc07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode02,( sv.zcomcode02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc08,( sv.zryrperc08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc09,( sv.zryrperc09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc10,( sv.zryrperc10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc11,( sv.zryrperc11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc12,( sv.zryrperc12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc13) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc13,( sv.zryrperc13.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc13) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc14) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc14,( sv.zryrperc14.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc14) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode03,( sv.zcomcode03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc15) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc15,( sv.zryrperc15.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc15) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc16) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc16,( sv.zryrperc16.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc16) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc16, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc17) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc17,( sv.zryrperc17.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc17) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc17, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc18) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc18,( sv.zryrperc18.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc18) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc18, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc19) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc19,( sv.zryrperc19.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc19) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc19, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc20) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc20,( sv.zryrperc20.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc20) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc20, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc21) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc21,( sv.zryrperc21.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc21) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc21, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode04,( sv.zcomcode04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc22) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc22,( sv.zryrperc22.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc22) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc22, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc23) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc23,( sv.zryrperc23.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc23) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc23, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc24) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc24,( sv.zryrperc24.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc24) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc24, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc25) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc25,( sv.zryrperc25.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc25) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc25, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc26) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc26,( sv.zryrperc26.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc26) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc26, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc27) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc27,( sv.zryrperc27.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc27) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc27, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc28) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc28,( sv.zryrperc28.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc28) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc28, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode05,( sv.zcomcode05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc29) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc29,( sv.zryrperc29.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc29) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc29, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc30) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc30,( sv.zryrperc30.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc30) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc30, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc31) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc31,( sv.zryrperc31.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc31) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc31, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc32) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc32,( sv.zryrperc32.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc32) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc32, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc33) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc33,( sv.zryrperc33.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc33) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc33, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc34) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc34,( sv.zryrperc34.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc34) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc34, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc35) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc35,( sv.zryrperc35.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc35) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc35, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode06,( sv.zcomcode06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc36) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc36,( sv.zryrperc36.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc36) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc36, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc37) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc37,( sv.zryrperc37.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc37) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc37, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc38) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc38,( sv.zryrperc38.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc38) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc38, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc39) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc39,( sv.zryrperc39.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc39) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc39, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc40) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc40,( sv.zryrperc40.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc40) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc40, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc41) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc41,( sv.zryrperc41.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc41) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc41, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc42) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc42,( sv.zryrperc42.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc42) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc42, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode07,( sv.zcomcode07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc43) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc43,( sv.zryrperc43.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc43) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc43, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc44) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc44,( sv.zryrperc44.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc44) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc44, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc45) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc45,( sv.zryrperc45.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc45) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc45, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc46) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc46,( sv.zryrperc46.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc46) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc46, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc47) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc47,( sv.zryrperc47.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc47) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc47, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc48) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc48,( sv.zryrperc48.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc48) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc48, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc49) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc49,( sv.zryrperc49.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc49) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc49, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode08,( sv.zcomcode08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc50) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc50,( sv.zryrperc50.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc50) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc50, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc51) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc51,( sv.zryrperc51.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc51) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc51, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc52) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc52,( sv.zryrperc52.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc52) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc52, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc53) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc53,( sv.zryrperc53.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc53) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc53, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc54) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc54,( sv.zryrperc54.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc54) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc54, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc55) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc55,( sv.zryrperc55.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc55) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc55, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc56) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc56,( sv.zryrperc56.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc56) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc56, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode09,( sv.zcomcode09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc57) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc57,( sv.zryrperc57.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc57) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc57, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc58) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc58,( sv.zryrperc58.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc58) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc58, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc59) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc59,( sv.zryrperc59.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc59) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc59, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc60) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc60,( sv.zryrperc60.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc60) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc60, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc61) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc61,( sv.zryrperc61.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc61) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc61, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc62) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc62,( sv.zryrperc62.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc62) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc62, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc63) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc63,( sv.zryrperc63.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc63) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc63, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>


 <tr>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode10,( sv.zcomcode10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc64) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc64,( sv.zryrperc64.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc64) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc64, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc65) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc65,( sv.zryrperc65.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc65) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc65, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc66) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc66,( sv.zryrperc66.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc66) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc66, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc67) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc67,( sv.zryrperc67.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc67) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc67, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc68) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc68,( sv.zryrperc68.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc68) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc68, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc69) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc69,( sv.zryrperc69.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc69) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc69, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc70) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc70,( sv.zryrperc70.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc70) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc70, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr>
</tbody>
</table>  
</div>
</div>

			   <div class="row"> 
			    	   <div class="col-md-6"> 
				    		<div class="form-group">  
				    		<div class="input-group"> 	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
					    		<%if(((BaseScreenData)sv.contitem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.contitem,( sv.contitem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.contitem) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.contitem, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
				      		</div></div>
				    	</div>
				    </div>
			   
			  
		  
		  
		  

  </div></div></div>
			        </div>



<%-- 
<div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<br/>
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>
<br/>
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>
<br/>
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>
<!--
<%StringData ITMTO_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%=smartHF.getLit(0, 0, ITMTO_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%></td>
</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'> --%>


<!--
<%-- <%StringData SR520_CompCode_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Component Code  ");%>
<%=smartHF.getLit(0, 0, SR520_CompCode_LBL).replace("absolute","relative")%> --%>
-->
<%-- <div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("OR Component Code  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year1_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year1  ");%>
<%=smartHF.getLit(0, 0, SR520_Year1_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year1  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year2_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year2  ");%>
<%=smartHF.getLit(0, 0, SR520_Year2_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year2  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year3_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year3  ");%>
<%=smartHF.getLit(0, 0, SR520_Year3_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year3  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year4_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year4  ");%>
<%=smartHF.getLit(0, 0, SR520_Year4_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year4  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year5_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year5  ");%>
<%=smartHF.getLit(0, 0, SR520_Year5_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year5  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year6_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year6  ");%>
<%=smartHF.getLit(0, 0, SR520_Year6_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year6  ")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData SR520_Year7_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year7   ");%>
<%=smartHF.getLit(0, 0, SR520_Year7_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year7   ")%>
</div>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode01,( sv.zcomcode01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc01,( sv.zryrperc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc02,( sv.zryrperc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc03,( sv.zryrperc03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc04,( sv.zryrperc04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc05,( sv.zryrperc05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc06,( sv.zryrperc06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc07,( sv.zryrperc07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode02,( sv.zcomcode02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc08,( sv.zryrperc08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc09,( sv.zryrperc09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc10,( sv.zryrperc10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc11,( sv.zryrperc11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc12,( sv.zryrperc12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc13) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc13,( sv.zryrperc13.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc13) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc14) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc14,( sv.zryrperc14.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc14) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode03,( sv.zcomcode03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc15) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc15,( sv.zryrperc15.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc15) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc16) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc16,( sv.zryrperc16.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc16) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc16, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc17) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc17,( sv.zryrperc17.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc17) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc17, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc18) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc18,( sv.zryrperc18.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc18) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc18, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc19) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc19,( sv.zryrperc19.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc19) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc19, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc20) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc20,( sv.zryrperc20.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc20) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc20, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc21) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc21,( sv.zryrperc21.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc21) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc21, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode04,( sv.zcomcode04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc22) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc22,( sv.zryrperc22.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc22) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc22, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc23) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc23,( sv.zryrperc23.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc23) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc23, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc24) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc24,( sv.zryrperc24.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc24) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc24, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc25) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc25,( sv.zryrperc25.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc25) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc25, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc26) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc26,( sv.zryrperc26.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc26) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc26, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc27) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc27,( sv.zryrperc27.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc27) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc27, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc28) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc28,( sv.zryrperc28.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc28) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc28, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode05,( sv.zcomcode05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc29) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc29,( sv.zryrperc29.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc29) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc29, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc30) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc30,( sv.zryrperc30.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc30) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc30, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc31) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc31,( sv.zryrperc31.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc31) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc31, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc32) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc32,( sv.zryrperc32.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc32) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc32, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc33) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc33,( sv.zryrperc33.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc33) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc33, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc34) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc34,( sv.zryrperc34.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc34) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc34, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc35) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc35,( sv.zryrperc35.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc35) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc35, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode06,( sv.zcomcode06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc36) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc36,( sv.zryrperc36.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc36) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc36, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc37) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc37,( sv.zryrperc37.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc37) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc37, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc38) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc38,( sv.zryrperc38.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc38) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc38, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc39) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc39,( sv.zryrperc39.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc39) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc39, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc40) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc40,( sv.zryrperc40.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc40) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc40, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc41) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc41,( sv.zryrperc41.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc41) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc41, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc42) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc42,( sv.zryrperc42.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc42) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc42, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode07,( sv.zcomcode07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc43) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc43,( sv.zryrperc43.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc43) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc43, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc44) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc44,( sv.zryrperc44.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc44) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc44, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc45) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc45,( sv.zryrperc45.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc45) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc45, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc46) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc46,( sv.zryrperc46.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc46) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc46, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc47) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc47,( sv.zryrperc47.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc47) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc47, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc48) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc48,( sv.zryrperc48.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc48) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc48, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc49) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc49,( sv.zryrperc49.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc49) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc49, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode08,( sv.zcomcode08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc50) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc50,( sv.zryrperc50.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc50) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc50, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc51) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc51,( sv.zryrperc51.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc51) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc51, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc52) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc52,( sv.zryrperc52.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc52) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc52, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc53) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc53,( sv.zryrperc53.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc53) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc53, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc54) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc54,( sv.zryrperc54.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc54) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc54, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc55) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc55,( sv.zryrperc55.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc55) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc55, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc56) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc56,( sv.zryrperc56.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc56) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc56, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode09,( sv.zcomcode09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc57) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc57,( sv.zryrperc57.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc57) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc57, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc58) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc58,( sv.zryrperc58.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc58) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc58, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc59) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc59,( sv.zryrperc59.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc59) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc59, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc60) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc60,( sv.zryrperc60.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc60) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc60, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc61) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc61,( sv.zryrperc61.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc61) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc61, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc62) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc62,( sv.zryrperc62.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc62) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc62, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc63) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc63,( sv.zryrperc63.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc63) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc63, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.zcomcode10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zcomcode10,( sv.zcomcode10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zcomcode10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zcomcode10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc64) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc64,( sv.zryrperc64.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc64) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc64, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc65) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc65,( sv.zryrperc65.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc65) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc65, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc66) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc66,( sv.zryrperc66.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc66) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc66, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc67) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc67,( sv.zryrperc67.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc67) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc67, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc68) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc68,( sv.zryrperc68.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc68) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc68, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc69) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc69,( sv.zryrperc69.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc69) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc69, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.zryrperc70) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zryrperc70,( sv.zryrperc70.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zryrperc70) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zryrperc70, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='100%'>
<!--
<%StringData CONTITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item");%>
<%=smartHF.getLit(0, 0, CONTITEM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%>
</div>
<br/>

<%if(((BaseScreenData)sv.contitem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.contitem,( sv.contitem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.contitem) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.contitem, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr> </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div> --%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<!---Ticket ILIFE-758 starts-->
<%@ include file="/POLACommon2NEW.jsp"%>

