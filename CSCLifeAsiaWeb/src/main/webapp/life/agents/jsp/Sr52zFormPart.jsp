	<%@ page language="java" pageEncoding="UTF-8" %>
	<%@ page contentType="text/html; charset=UTF-8" %>
	<%String screenName = "Sr52zFormPart";%>
	
	

<%@page import="com.csc.life.agents.screens.Sr52zScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatterNew" %>
<%@page import="com.properties.PropertyLoader"%>


<%@page import="com.quipoz.framework.screendef.QPScreenField"%>

<%-- <%@page import="com.quipoz.framework.util.AppVars" %> --%>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@ page import="java.util.*" %>  
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.properties.PropertyLoader"%>
<%@ page import="com.csc.fsu.general.screens.*" %>
<%@page import="com.quipoz.framework.util.*" %>
<%@page import="com.quipoz.framework.screenmodel.*" %>
<%@page import="com.quipoz.framework.datatype.*" %>
<%@page import="com.quipoz.COBOLFramework.util.*" %>
		
		
<%-- Start preparing data --%>
<%
String longValue = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();


LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();
LifeAsiaAppVars appVars = av;
av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatterNew smartHF = new SMARTHTMLFormatterNew(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), lang);

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%Sr52zScreenVars sv = (Sr52zScreenVars) fw.getVariables();%>		

		<%sv.bnytype09.setClassString("");%>
		<%sv.manopt09.setClassString("");%>
		<%sv.selfind09.setClassString("");%>
		<%sv.cltreln41.setClassString("");%>
		<%sv.revcflg41.setClassString("");%>
		<%sv.cltreln42.setClassString("");%>
		<%sv.revcflg42.setClassString("");%>
		<%sv.cltreln43.setClassString("");%>
		<%sv.revcflg43.setClassString("");%>
		<%sv.cltreln44.setClassString("");%>
		<%sv.revcflg44.setClassString("");%>
		<%sv.cltreln45.setClassString("");%>
		<%sv.bnytype10.setClassString("");%>
		<%sv.manopt10.setClassString("");%>
		<%sv.selfind10.setClassString("");%>
		<%sv.cltreln46.setClassString("");%>
		<%sv.revcflg46.setClassString("");%>
		<%sv.cltreln47.setClassString("");%>
		<%sv.revcflg47.setClassString("");%>
		<%sv.cltreln48.setClassString("");%>
		<%sv.revcflg48.setClassString("");%>
		<%sv.cltreln49.setClassString("");%>
		<%sv.revcflg49.setClassString("");%>
		<%sv.cltreln50.setClassString("");%>
		<%sv.revcflg50.setClassString("");%>
		<%sv.revcflg45.setClassString("");%>
		
		
		
		<% {	
			appVars.rolldown();
			appVars.rollup();
		if (appVars.ind09.isOn()) {
			sv.bnytype09.setReverse(BaseScreenData.REVERSED);
			sv.bnytype09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.bnytype09.setHighLight(BaseScreenData.BOLD);
		}	
		
		if (appVars.ind19.isOn()) {
			sv.manopt09.setReverse(BaseScreenData.REVERSED);
			sv.manopt09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.manopt09.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind19.isOn()) {
			sv.selfind09.setReverse(BaseScreenData.REVERSED);
			sv.selfind09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.selfind09.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind25.isOn()) {
			sv.cltreln41.setReverse(BaseScreenData.REVERSED);
			sv.cltreln41.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln41.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg41.setReverse(BaseScreenData.REVERSED);
			sv.revcflg41.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg41.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln42.setReverse(BaseScreenData.REVERSED);
			sv.cltreln42.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln42.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg42.setReverse(BaseScreenData.REVERSED);
			sv.revcflg42.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg42.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln43.setReverse(BaseScreenData.REVERSED);
			sv.cltreln43.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln43.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg43.setReverse(BaseScreenData.REVERSED);
			sv.revcflg43.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg43.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln44.setReverse(BaseScreenData.REVERSED);
			sv.cltreln44.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln44.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg44.setReverse(BaseScreenData.REVERSED);
			sv.revcflg44.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg44.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln45.setReverse(BaseScreenData.REVERSED);
			sv.cltreln45.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln45.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg45.setReverse(BaseScreenData.REVERSED);
			sv.revcflg45.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg45.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln46.setReverse(BaseScreenData.REVERSED);
			sv.cltreln46.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln46.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg46.setReverse(BaseScreenData.REVERSED);
			sv.revcflg46.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg46.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln47.setReverse(BaseScreenData.REVERSED);
			sv.cltreln47.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln47.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg47.setReverse(BaseScreenData.REVERSED);
			sv.revcflg47.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg47.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln48.setReverse(BaseScreenData.REVERSED);
			sv.cltreln48.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln48.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg48.setReverse(BaseScreenData.REVERSED);
			sv.revcflg48.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg48.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln49.setReverse(BaseScreenData.REVERSED);
			sv.cltreln49.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln49.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg49.setReverse(BaseScreenData.REVERSED);
			sv.revcflg49.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg49.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cltreln50.setReverse(BaseScreenData.REVERSED);
			sv.cltreln50.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cltreln50.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.revcflg50.setReverse(BaseScreenData.REVERSED);
			sv.revcflg50.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.revcflg50.setHighLight(BaseScreenData.BOLD);
		}
		
		}%>
		
		<div class="row">
			
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group">
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.bnytype09)%> --%>
			<div style="width:40px;"><%=smartHF.getHTMLVarExt(fw, sv.bnytype09)%></div>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.bnytype09)%>
			</div>
			</div>
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group" style="margin-left:30px; width:25px;">
			<%=smartHF.getHTMLVarExt(fw, sv.manopt09)%>
			</div>
			</div>
			</div>
			
						
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group" style="margin-left:15px;  width:25px !important;">
			<%=smartHF.getHTMLVarExt(fw, sv.selfind09)%>
			</div>
			</div>
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln41)%> --%>
			
			
																																												
			<table> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln41).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln41)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln41)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln41')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td>
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg41)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln41)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln41)%>


			<%=smartHF.getHTMLVarExt(fw, sv.revcflg41)%>
	
			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln42)%> --%>
			
			
																																															
			<table style="margin-left: -21px;"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln42).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln42)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln42)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln42')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg42)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln42)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln42)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg42)%>
			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln43)%> --%>
			
																																																		
			<table style="margin-left: -39px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln43).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln43)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln43)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln43')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg43)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln43)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln43)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg43)%>

			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln44)%> --%>
			
			
																																																					
			<table style="margin-left: -57px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln44).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln44)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln44)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln44')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg44)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln44)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln44)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg44)%>


			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln45)%> --%>
			
			
																																																								
			<table style="margin-left: -73px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln45).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln45)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln45)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln45')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg45)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln45)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln45)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg45)%>
	

			</div> --%>
			</div>
			</div>
			
		
		</div>
		<div class="row">
			
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group">
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.bnytype10)%> --%>
			<div style="width:40px;"><%=smartHF.getHTMLVarExt(fw, sv.bnytype10)%></div>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.bnytype10)%>
			</div>
			</div>
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group" style="margin-left:30px; width:25px;">
			<%=smartHF.getHTMLVarExt(fw, sv.manopt10)%>
			</div>
			</div>
			</div>
			
						
			<div class="col-md-1">
			<div class="form-group">
			<div class="input-group" style="margin-left:15px;  width:25px !important;">
			<%=smartHF.getHTMLVarExt(fw, sv.selfind10)%>
			</div>
			</div>
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
		<!-- 	<div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln46)%> --%>
			
			
																																																					
			<table> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln46).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln46)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln46)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln46')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg46)%></div>
	        </td>
	        </tr>
	        </table>
			
		<%-- 	
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln46)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln46)%>


			<%=smartHF.getHTMLVarExt(fw, sv.revcflg46)%>
			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln47)%> --%>
			
						
																																																					
			<table style="margin-left: -21px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln47).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln47)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln47)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln47')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg47)%></div>
	        </td>
	        </tr>
	        </table>
			
			<%-- 
			
			
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln47)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln47)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg47)%>

			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln48) %> --%>
			
			<table style="margin-left: -39px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln48).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln48)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln48)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln48')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg48)%></div>
	        </td>
	        </tr>
	        </table>
			
			
		<%-- 	
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln48)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln48)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg48)%>


			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px;"> -->
			
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln49)%> --%>
			
			
			<table style="margin-left: -57px"> 
			<tr>
			<td>
			<%
	          if ((new Byte((sv.cltreln49).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                      || fw.getVariables().isScreenProtected()) {
			%>                   
			
				<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln49)%></div>			
	                                         
	        </div>
	        <%
				} else {
			%>
	        <div class="input-group" style="width: 95px;">
	            <%=smartHF.getRichTextInputFieldLookup(fw, sv.cltreln49)%>
	             <span class="input-group-btn">
	             <button class="btn btn-info" type="button"
	                onClick="doFocus(document.getElementById('cltreln49')); doAction('PFKEY04')">
	                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
	            </button>
	        </div>
	        <%
	         }
	        %>    
	        </td>
	        <td >
	           <div style="width: 25px;"> <%=smartHF.getHTMLVarExt(fw, sv.revcflg49)%></div>
	        </td>
	        </tr>
	        </table>
			
			
		<%-- 	
			<%=smartHF.getHTMLVarExt(fw, sv.cltreln49)%>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln49)%>

			<%=smartHF.getHTMLVarExt(fw, sv.revcflg49)%>

			</div> --%>
			</div>
			</div>
			
			<div class="col-md-1">
			</div>
			
			<div class="col-md-1">
			<div class="form-group">
			<!-- <div class="input-group" style="width:125px; margin-left: -73px;"> -->
			<%-- <%=smartHF.getHTMLSpaceVar(fw, sv.cltreln50)%> --%>
			
			
			<table style="margin-left: -73px;">
			<tr>
			<td class="input-group">
			
			
			<div style="min-width: 40px;"><%=smartHF.getHTMLVarExt(fw, sv.cltreln50)%></div>
			<%=smartHF.getHTMLF4NSVarExt(fw, sv.cltreln50)%>
			</td>
			<td>
			<div style="min-width: 25px;"><%=smartHF.getHTMLVarExt(fw, sv.revcflg50)%></div>
			</td>
			</tr>
			</table>
			<!-- </div> -->
			</div>
			</div>
			
		
		</div>	
	<div class="row">
			
			<div class="col-md-2">
				<div class="form-group">
				<div class="input-group" style="width:125px;">
				<label>Continuation Item</label>
				<%=smartHF.getHTMLVarExt(fw, sv.gitem)%>
				</div>
				</div>
			</div>
	</div>
			  