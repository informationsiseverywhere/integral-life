

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR695";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sr695ScreenVars sv = (Sr695ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Dates effective ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Regular");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Single");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Top-up");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Basic Calc Method ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Basic Pay Method ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Servicing Pay Method ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Renewal Pay Method ");
%>




<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 400px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>

		<%-- 2nd row --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td style="padding-left: 10px; padding-right: 10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>

							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<%-- row 3 --%>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Regular"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Single"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Top-up"))%></label>
				</div>
			</div>
		</div>

		<%-- row 4 --%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Basic Calc Method"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "basicCommMeth" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("basicCommMeth");
						optionValue = makeDropDownList(mappedItems, sv.basicCommMeth.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.basicCommMeth.getFormData()).toString().trim());
					%>

					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 Start-->

					<%=smartHF.getDropDownExt(sv.basicCommMeth, fw, longValue, "basicCommMeth", optionValue, 0, 250)%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "basicCommMeth" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("basicCommMeth");
						optionValue = makeDropDownList(mappedItems, sv.basicCommMeth.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.basicCommMeth.getFormData()).toString().trim());
					%>

					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 Start-->

					<%=smartHF.getDropDownExt(sv.basicCommMeth, fw, longValue, "basicCommMeth", optionValue, 0, 250)%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "basscmth" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("basscmth");
						optionValue = makeDropDownList(mappedItems, sv.basscmth.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.basscmth.getFormData()).toString().trim());
					%>
					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.basscmth, fw, longValue, "basscmth", optionValue, 0, 250)%>
				</div>
			</div>
		</div>

		<%-- row 5 --%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Basic Pay Method"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bascpy" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bascpy");
						optionValue = makeDropDownList(mappedItems, sv.bascpy.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.bascpy.getFormData()).toString().trim());
					%>

					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.bastcmth, fw, longValue, "bastcmth", optionValue, 0, 250)%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "basscpy" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("basscpy");
						optionValue = makeDropDownList(mappedItems, sv.basscpy.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.basscpy.getFormData()).toString().trim());
					%>


					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.basscpy, fw, longValue, "basscpy", optionValue, 0, 250)%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bastcpy" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bastcpy");
						optionValue = makeDropDownList(mappedItems, sv.bastcpy.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.bastcpy.getFormData()).toString().trim());
					%>

					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.bastcpy, fw, longValue, "bastcpy", optionValue, 0, 250)%>
				</div>
			</div>
		</div>

		<%-- row 6 --%>
		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Servicing Pay Method"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "srvcpy" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("srvcpy");
						optionValue = makeDropDownList(mappedItems, sv.srvcpy.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.srvcpy.getFormData()).toString().trim());
					%>

					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.srvcpy, fw, longValue, "srvcpy", optionValue, 0, 250)%>
				</div>
			</div>
		</div>

		<%-- row 5 --%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Renewal Pay Method"))%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "rnwcpy" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("rnwcpy");
						optionValue = makeDropDownList(mappedItems, sv.rnwcpy.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.rnwcpy.getFormData()).toString().trim());
					%>
					<!--  Ilife- Life Cross Browser - Sprint 4 D7 : Task 1 starts-->

					<%=smartHF.getDropDownExt(sv.rnwcpy, fw, longValue, "rnwcpy", optionValue, 0, 250)%>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>