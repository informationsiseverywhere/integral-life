


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM503";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm503ScreenVars sv = (Sm503ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = new StringData("Agent number     ");%>
	<%StringData generatedText3 = new StringData("Date appointment ");%>
	<%StringData generatedText4 = new StringData("Current Agt Type ");%>
	<%StringData generatedText5 = new StringData("Branch           ");%>
	<%StringData generatedText6 = new StringData("Accounting Month ");%>
	<%StringData generatedText7 = new StringData("Year");%>
	<%StringData generatedText23 = new StringData("Personal");%>
	<%StringData generatedText9 = new StringData("Direct");%>
	<%StringData generatedText10 = new StringData("Group");%>
	<%StringData generatedText8 = new StringData("1st yr");%>
	<%StringData generatedText11 = new StringData("2nd yr");%>
	<%StringData generatedText12 = new StringData("3rd yr");%>
	<%StringData generatedText13 = new StringData("4th yr");%>
	<%StringData generatedText14 = new StringData("5th yr");%>
	<%StringData generatedText15 = new StringData("6th yr");%>
	<%StringData generatedText16 = new StringData("7th yr");%>
	<%StringData generatedText17 = new StringData("8th yr");%>
	<%StringData generatedText18 = new StringData("9th yr");%>
	<%StringData generatedText19 = new StringData("10th yr");%>
	<%StringData generatedText24 = new StringData("Personal");%>
	<%StringData generatedText25 = new StringData("Group");%>
	<%StringData generatedText20 = new StringData("Calendar YTD ");%>
	<%StringData generatedText22 = new StringData("Personal Pol. Count ");%>
	<%StringData generatedText21 = new StringData("Financial YTD ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.mnth.setReverse(BaseScreenData.REVERSED);
			sv.mnth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.mnth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.acctyr.setReverse(BaseScreenData.REVERSED);
			sv.acctyr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.acctyr.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent number")%></label>
					    		     <table>
					    		     <tr><td>
						    		<%					
		if(!((sv.agntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>
	
  		
		<%					
		if(!((sv.agtname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;max-width: 300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

			</td>
			</tr></table>
				    		</div>
					</div>
				    		
				    		
				    
		    </div>
				   
				  
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
					    		     <div class="input-group">
						    		
	
		<%					
		if(!((sv.descrip02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Current Agt Type")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.descrip01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Date appointment")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.dteappDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
			
			<div class="row">	
			
			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%></label>
    	 					<table><tr><td>	
    	 				
    	 					
<%	
			qpsf = fw.getFieldXMLDef((sv.mnth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mnth' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnth) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnth);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnth) %>'
	 <%}%>

size='<%= sv.mnth.getLength()%>'
maxLength='<%= sv.mnth.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnth)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnth).getColor()== null  ? 
			"input_cell" :  (sv.mnth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td>
<%	
			qpsf = fw.getFieldXMLDef((sv.acctyr).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='acctyr' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acctyr);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acctyr) %>'
	 <%}%>

size='<%= sv.acctyr.getLength()%>'
maxLength='<%= sv.acctyr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acctyr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acctyr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acctyr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acctyr).getColor()== null  ? 
			"input_cell" :  (sv.acctyr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
    	 				
    	 				</td></tr></table>
    	 				</div></div>	
    	 				
    	 				
    	 				
			
			
			
			    	
				  <div class="col-md-3">
				<div class="btn-group">
					<div class="sectionbutton">
						
						<p style="font-size: 12px; font-weight: bold;">
							<a class="btn btn-info" href="#"
								onClick=" JavaScript:doAction('PFKEY0');"><%=resourceBundleHandler.gettingValueFromBundle("Search")%></a>
						</p>
					</div>
				</div>
			</div>	</div>
				  
				        <div class="table-responsive">
	         <table id="sm503Table" class="table table-striped table-bordered table-hover" width='100%' >
               <thead>
		
			        <tr class="info">
			        <th><%=resourceBundleHandler.gettingValueFromBundle("year")%></th>
	    <th><%=resourceBundleHandler.gettingValueFromBundle("Personal")%></th>    								
		<th><%=resourceBundleHandler.gettingValueFromBundle("Direct")%></th>
		<th><%=resourceBundleHandler.gettingValueFromBundle("Group")%></th>
			     	 
		 	        </tr>
			 </thead>
			 

 <%
int[] tblColumnWidth = {180,180,180,180};
int totalTblWidth = 720;
int calculatedValue =0;


 %>
		

		

 


		




<tbody>
<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>;" >

	<%-- <tr style="height: 25px;background:<%= backgroundcolor1%>;">

<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[0 ]%>px;font-weight: bold;font-size: 12px; font-family: Arial" align="left"> --%>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("1st yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[1]%>;" >
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				
				
					<%= formatValue%> 
				
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[2]%>;" >
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				
					<%= formatValue%>
				
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				
					<%= formatValue%>
				
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</td>

</tr>
<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0]%>;" >
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("2nd yr")%>
</div>


</td><td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[1]%>;" >


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				
					<%= formatValue%>
			
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[2]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;"><td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("3rd yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[1]%>;" >


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp03).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[2]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp03).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp03).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp03,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;"><td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("4th yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[1]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp04).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[2]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp04).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
			<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp04).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp04,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;"><td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("5th yr")%>
</div>


</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp05).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp05).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp05).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp05,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;"><td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("6th yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp06).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp06,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp06).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp06,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp06).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp06,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>

<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("7th yr")%>
</div>


</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp07).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp07,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp07).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp07,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp07).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp07,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("8th yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp08).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp08,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp08).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp08,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
			<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp08).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp08,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("9th yr")%>
</div>


</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp09).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp09,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp09).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp09,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp09).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp09,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</tr>
<tr class="tableRowTag" style="height: 25px;">
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("10th yr")%>
</div>


</td>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlperpp10).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlperpp10,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlperpp10.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >

	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mldirpp10).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mldirpp10,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mldirpp10.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	



</td>

<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[3]%>;" >
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlgrppp10).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlgrppp10,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlgrppp10.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<%= formatValue%>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</tr>

		</table>
		</div>

				  
				  
				  
				  <table >

<tr style="height: 18px;">

<td width='151'/>
<td width='151'>


<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Personal")%></label>

</td>
<td></td>
<td width='151'>

<label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Group")%></label>

</td>
<td width='151'/>
<br><br>
</tr>




<tr style="height: 25px;">
<td width='151'>

<div class="label_txt" style="font-size: 14px;" >
<%=resourceBundleHandler.gettingValueFromBundle("Calendar YTD")%>
</div>
</td>
<td width='151'>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlytdperca).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlytdperca,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlytdperca.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		<div class="blank_cell" > &nbsp; </div>
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</td>
<td></td>

<td width='151'>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlytdgrpca).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlytdgrpca,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlytdgrpca.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

</td>


</tr>

<tr style="height: 15px;"></tr>
<tr style="height: 25px;">
<td width='151'>

<div class="label_txt" style="font-size: 14px;">
<%=resourceBundleHandler.gettingValueFromBundle("Financial YTD")%>
</div>
</td>
<td width='151'>	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlytdperfn).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlytdperfn,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlytdperfn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
	</td>
	
	<td width='50'></td>
 <td width='151'>
	





	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mlytdgrpfn).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mlytdgrpfn,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.mlytdgrpfn.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 </td>


</tr></table>
				  
				  <br><br>
				  
				  <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Personal Pol. Count")%></label>
					    		     <div class="input-group" style="width:92px;">
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.countpol).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.countpol);
			
			if(!((sv.countpol.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

				      			     </div>
				    		</div>
					</div></div>
				  </div></div>	
			


<script>
$(document).ready(function() {
    $('#sm503Table').DataTable( {
        "scrollY":        "240px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false
       
    } );
   
} );

</script>





	
<%@ include file="/POLACommon2NEW.jsp"%>

