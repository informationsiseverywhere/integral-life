
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5035";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5035ScreenVars sv = (S5035ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date appointed ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Exclusive?");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Area code ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sales unit ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reporting to ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Override % ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"License No ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Expiry Date ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Black List Status ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account payee ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay method ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay frequency ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum amount ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date Terminated  ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Basic commission ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing commission ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Renewal commission ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission class ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus allocation ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client details  ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"OR Details ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank account details ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Broker contacts ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tied agent details ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Qualifications ");%><!-- added for ILJ-4 -->
		<!-- TMLII-281 AG-01-002 -->
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Recruiter ");%>
	
	

<%{
		if (appVars.ind25.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.clntsel, appVars.ind22);
		if (!appVars.ind01.isOn()) {
			sv.clntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.dteappDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.dteappDisp.setReverse(BaseScreenData.REVERSED);
			sv.dteappDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.dteappDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.exclAgmt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.exclAgmt.setReverse(BaseScreenData.REVERSED);
			sv.exclAgmt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.exclAgmt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.agtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.agtype.setReverse(BaseScreenData.REVERSED);
			sv.agtype.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.agtype, appVars.ind23);
		if (!appVars.ind05.isOn()) {
			sv.agtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.agntbr.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind52.isOn()) {
			sv.agntbr.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind51.isOn()) {
			sv.agntbr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.agntbr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.aracde.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.aracde.setReverse(BaseScreenData.REVERSED);
			sv.aracde.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.aracde, appVars.ind24);
		if (!appVars.ind04.isOn()) {
			sv.aracde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			generatedText9.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.repsel.setReverse(BaseScreenData.REVERSED);
		}
		appVars.setFieldChange(sv.repsel, appVars.ind25);
		if (appVars.ind07.isOn()) {
			sv.repsel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind61.isOn()) {
			sv.repsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind60.isOn()) {
			sv.repsel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind07.isOn()) {
			sv.repsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.ovcpc.setReverse(BaseScreenData.REVERSED);
			sv.ovcpc.setColor(BaseScreenData.RED);
		}
		if (appVars.ind64.isOn()) {
			sv.ovcpc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind65.isOn()) {
			sv.ovcpc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind06.isOn()) {
			sv.ovcpc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.paysel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.paysel.setReverse(BaseScreenData.REVERSED);
			sv.paysel.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.paysel, appVars.ind26);
		if (!appVars.ind08.isOn()) {
			sv.paysel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.paymth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.paymth.setReverse(BaseScreenData.REVERSED);
			sv.paymth.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.paymth, appVars.ind27);
		if (!appVars.ind09.isOn()) {
			sv.paymth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.payfrq.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.payfrq.setReverse(BaseScreenData.REVERSED);
			sv.payfrq.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.payfrq, appVars.ind28);
		if (!appVars.ind10.isOn()) {
			sv.payfrq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.currcode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.currcode.setReverse(BaseScreenData.REVERSED);
			sv.currcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.currcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.minsta.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind80.isOn()) {
			sv.dtetrmDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.dtetrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.dtetrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.dtetrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.bcmtab.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.bcmtab.setReverse(BaseScreenData.REVERSED);
			sv.bcmtab.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.bcmtab, appVars.ind29);
		if (!appVars.ind11.isOn()) {
			sv.bcmtab.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.scmtab.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.scmtab.setReverse(BaseScreenData.REVERSED);
			sv.scmtab.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.scmtab, appVars.ind30);
		if (!appVars.ind12.isOn()) {
			sv.scmtab.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.rcmtab.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.rcmtab.setReverse(BaseScreenData.REVERSED);
			sv.rcmtab.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.rcmtab, appVars.ind31);
		if (!appVars.ind13.isOn()) {
			sv.rcmtab.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.agentClass.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.agentClass.setReverse(BaseScreenData.REVERSED);
			sv.agentClass.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.agentClass, appVars.ind32);
		if (!appVars.ind16.isOn()) {
			sv.agentClass.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ocmtab.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.ocmtab.setReverse(BaseScreenData.REVERSED);
			sv.ocmtab.setColor(BaseScreenData.RED);
		}
		appVars.setFieldChange(sv.ocmtab, appVars.ind33);
		if (!appVars.ind14.isOn()) {
			sv.ocmtab.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.clientind.setReverse(BaseScreenData.REVERSED);
			sv.clientind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.clientind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.bctind.setReverse(BaseScreenData.REVERSED);
			sv.bctind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.bctind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.tagd.setReverse(BaseScreenData.REVERSED);
			sv.tagd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.tagd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-53----Start
		/* if (appVars.ind22.isOn()) {
			sv.zrorind.setReverse(BaseScreenData.REVERSED);
			sv.zrorind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind63.isOn()) {
			sv.zrorind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind62.isOn()) {
			sv.zrorind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind22.isOn()) {
			sv.zrorind.setHighLight(BaseScreenData.BOLD);
		} */
		
		if (appVars.ind63.isOn()) {
			sv.zrorind.setEnabled(BaseScreenData.DISABLED); 
		}
		//ILJ-53----End		
		if (appVars.ind55.isOn()) {
			sv.tlaglicno.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.tlaglicno.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind55.isOn()) {
			sv.tlaglicno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.tlaglicno.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind21.isOn()) {
			sv.tlicexpdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind56.isOn()) {
			sv.tlicexpdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.tlicexpdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.tlicexpdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.tagsusind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind53.isOn()) {
			sv.tagsusind.setReverse(BaseScreenData.REVERSED);
			sv.tagsusind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.tagsusind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.tsalesunt.setReverse(BaseScreenData.REVERSED);
			sv.tsalesunt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.tsalesunt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind54.isOn()) {
			sv.tsalesunt.setHighLight(BaseScreenData.BOLD);
		}
		//TMLII-281 AG-01-002 START
		if (appVars.ind66.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind68.isOn()) {
			sv.zrecruit.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind68.isOn()) {
			sv.zrecruit.setColor(BaseScreenData.RED);
		}
		if (appVars.ind67.isOn()) {
			sv.zrecruit.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind66.isOn()) {
			sv.zrecruit.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind68.isOn()) {
			sv.zrecruit.setHighLight(BaseScreenData.BOLD);
		}
		//TMLII-281 AG-01-002 END
		//ICIL-12 start 
		if (appVars.ind24.isOn()) {
			sv.agentrefcode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind84.isOn()) {
			sv.agentrefcode.setColor(BaseScreenData.RED);
			sv.agentrefcode.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind84.isOn()) {
			sv.agentrefcode.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind82.isOn()) {
			sv.agentrefcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind26.isOn()) {
			sv.agencymgr.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind85.isOn()) {
			sv.agencymgr.setColor(BaseScreenData.RED);
			sv.agencymgr.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind83.isOn()) {
			sv.agencymgr.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind85.isOn()) {
			sv.agencymgr.setHighLight(BaseScreenData.BOLD);
		}
		
				
		if (appVars.ind27.isOn()) {
			sv.distrctcode.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind87.isOn()) {
			sv.distrctcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		
		if (appVars.ind28.isOn()) {
			sv.bnkinternalrole.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind88.isOn()) {
			sv.bnkinternalrole.setColor(BaseScreenData.RED);
			sv.bnkinternalrole.setReverse(BaseScreenData.REVERSED);
		} 
		if (appVars.ind89.isOn()) {
			sv.bnkinternalrole.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind88.isOn()) {
			sv.bnkinternalrole.setHighLight(BaseScreenData.BOLD);
		} 
		
		if (appVars.ind29.isOn()) {
			sv.banceindicatorsel.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind91.isOn()) {
			sv.banceindicatorsel.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		//ICIL-12 End 
		
		//ILJ-331-Start
		if (appVars.ind30.isOn()) {
			sv.agncysel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind58.isOn()) {
			sv.agncysel.setReverse(BaseScreenData.REVERSED);
			sv.agncysel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind93.isOn()) {
			sv.agncysel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-331-End
		
		if (appVars.ind31.isOn()) {
			sv.salebranch.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind57.isOn()) {
			sv.salebranch.setReverse(BaseScreenData.REVERSED);
			sv.salebranch.setColor(BaseScreenData.RED);
		}
		if (appVars.ind59.isOn()) {
			sv.salebranch.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind57.isOn()) {
			sv.salebranch.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
		
			<div class="col-md-4"> 
				<div>
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					<table><tr><td  class="form-group">
				 <div class="input-group"> 
					<%	
							longValue = sv.clntsel.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.clntsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						  <%if(longValue != null){%>
						   		
							<%=longValue%>
						   		
							<%}%>
					</div>
					
					<%
					longValue = null;
					%> 
					
							
					<% }else {%> 
					<input name='clntsel'  id='clntsel'
					type='text' 
					value='<%=sv.clntsel.getFormData()%>' 
					maxLength='<%=sv.clntsel.getLength()%>' 
					size='<%=sv.clntsel.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(clntsel)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.clntsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.clntsel).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
                         <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                    </span>
                   
					<%
						}else { 
					%>
					
					class = ' <%=(sv.clntsel).getColor()== null  ? 
					"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
                         <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                    </span> 				
					<%}longValue = null;} %>
					
							 </div> 	</td>	  <td style="min-width:80px;padding-left:1px">
							<%					
							if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>	
										
							<!--  Ilife- Life Cross Browser - Sprint 4 D1 : Task 1 Start-->
												
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:100px;"   >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							
							</td></tr></table>
			  			<!--  </div> -->
	
				</div>
			</div>
			
		
		
		<%-- 2nd row  --%>
		
			
			</div>
            <div class="row">
            <div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agent number "))%></label>
					<%
						if (!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Date Appointed"))%></label>
									<% if ((new Byte((sv.dteappDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.dteappDisp)%></div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.dteappDisp, (sv.dteappDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Date Terminated"))%></label>
					<%
						if ((new Byte((sv.dtetrmDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.dtetrmDisp, (sv.dtetrmDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.dtetrmDisp, (sv.dtetrmDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>

		
			</div>
	        <%--3rd row --%>
	       <div class="row">
	       
	       
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Type")%></label>
<div class="input-group">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "agtype" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("agtype");
						optionValue = makeDropDownList(mappedItems, sv.agtype.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.agtype.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.agtype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.agtype).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #ea4742;width: 235px;height: 28px;">
						<%
							}
						%>

						<select name='agtype' type='list'
							"
							<%if ((new Byte((sv.agtype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.agtype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.agtype).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>

				</div></div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
				    	<table><tr><td style="min-width:10px;">
			  <div class="input-group">  
<%	
	longValue = sv.agntbr.getFormData();  
%>

<% 
	if((new Byte((sv.agntbr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='agntbr' 
type='text' 
value='<%=sv.agntbr.getFormData()%>' 
maxLength='<%=sv.agntbr.getLength()%>' 
size='<%=sv.agntbr.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(agntbr)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.agntbr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.agntbr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
  <span class="input-group-btn">
                                        <button class="btn btn-info"  right: -3px !important"; type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                        </button>
                                    </span>
<%
	}else { 
%>

class = ' <%=(sv.agntbr).getColor()== null  ? 
"input_cell" :  (sv.agntbr).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

  <span class="input-group-btn">
                                        <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                        </button>
                                    </span>

<%}longValue = null;} %>

 </div>	 	</td>
			  
			  
			  <td style="padding-left: 1px;" >



<!-- </td><td style="width:1px;"></td><td> -->
	
  		
		<%					
		if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 135px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				    
				    
				    
				    
				    
				    
				    
				    
				    
				</td></tr></table> 
                 </div>
			</div>
			
	       
	 		 <div class="col-md-4">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" >
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	
	       
	       
	
			</div>
			<div class="row">
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales unit")%></label>
				
				<table><tr><td>
				<div class="input-group">
				<%
					longValue = sv.tsalesunt.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.tsalesunt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='tsalesunt'  id='tsalesunt'
				type='text' 
				value='<%=sv.tsalesunt.getFormData()%>' 
				maxLength='<%=sv.tsalesunt.getLength()%>' 
				size='<%=sv.tsalesunt.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(tsalesunt)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.tsalesunt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.tsalesunt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('tsalesunt')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.tsalesunt).getColor()== null  ? 
				"input_cell" :  (sv.tsalesunt).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('tsalesunt')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%}longValue = null;} %>
		</div>	</td>	 
			
			 <td style="padding-left:1px">
				
				  		
						<%					
						if(!((sv.tsalesdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tsalesdsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tsalesdsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td></tr></table>
						</div>
				  </div>
				<!--  ICIL-12 start  -->
				
				<!-- Agency start -->
				<%
						if ((new Byte((sv.agncysel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
				<div class="col-md-4">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></label>
				
				<table><tr><td  class="form-group">
				<div class="input-group">
				<%
					longValue = sv.agncysel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='agncysel'  id='agncysel'
				type='text' 
				value='<%=sv.agncysel.getFormData()%>' 
				maxLength='<%=sv.agncysel.getLength()%>' 
				size='<%=sv.agncysel.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(agncysel)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.agncysel).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.agncysel).getColor()== null  ? 
				"input_cell" :  (sv.agncysel).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%}longValue = null;} %>
		</div>	</td>	 
			
			 <td style="padding-left:1px">
				
				  		
						<%					
						if(!((sv.agncydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agncydesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agncydesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;max-width:100px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td></tr></table>
						</div>
				  </div>
				  <%}%>
				  <!-- Agnecy ends -->
				  
				  
				  	<%
						if ((new Byte((sv.salebranch).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
				  		<div class="col-md-4">
		
		
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Branch/ Sales Section Number")%></label>
   					 <table><tr><td style="min-width:10px" class="form-group">
					<div class="input-group">  
						<input name='salebranch' id='salebranch' type='text'
							value='<%=sv.salebranch.getFormData()%>'
							maxLength='<%=sv.salebranch.getLength()%>'
 							size='<%=sv.salebranch.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(salebranch)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.salebranch).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.salebranch).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salebranch')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.salebranch).getColor()== null  ? 
"input_cell" :  (sv.salebranch).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('salebranch')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>		
				</div> 		</td>
			 
			  
			  <td style="padding-left:1px">		  		
					<%					
					if(!((sv.salebranchdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.salebranchdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.salebranchdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div></div>
			
			
			<%} %>
				
				
				
				
					<%
						if ((new Byte((sv.banceindicatorsel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bancassurance Indicator")%></label>
					
						

					<input type='checkbox' name='banceindicatorsel' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(banceindicatorsel)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.banceindicatorsel).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
					if ((sv.banceindicatorsel).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked
						<%}
					if ((sv.banceindicatorsel).getEnabled() == BaseScreenData.DISABLED
							|| fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('banceindicatorsel')" /> <input type='checkbox'
						name='banceindicatorsel' value='N'
						<%if (!(sv.banceindicatorsel).toString().trim()
							.equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('banceindicatorsel')" />
					
					</div>
				</div>
				<%
					}
				%>
				<%
				if ((new Byte((sv.bnkinternalrole).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>	
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Internal Role")%></label>						
						
						<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bnkinternalrole" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bnkinternalrole");
						optionValue = makeDropDownList(mappedItems, sv.bnkinternalrole.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.bnkinternalrole.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.bnkinternalrole).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.bnkinternalrole).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #ea4742;width: 235px;height: 28px;">
						<%
							}
						%>

						<select name='bnkinternalrole' type='list'
							"
							<%if ((new Byte((sv.bnkinternalrole).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.bnkinternalrole).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.bnkinternalrole).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
						
					</div>
				</div>
				<%
					}
				%>
				<!--  ICIL-12 end  -->
			</div>
				
			<!-- 	ICIL-Start -->
			<%
				if ((new Byte((sv.agentrefcode).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>	
				<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("District Code")%></label>
						<%-- <%=smartHF.getHTMLVarExt(fw, sv.agentrefcode)%> --%>
						
						
						<input name='distrctcode' type='text' style="width:190px;"
						<%formatValue = (sv.distrctcode.getFormData()).toString();%>
						value='<%=XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.distrctcode.getLength()%>'
						maxLength='<%=sv.distrctcode.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(distrctcode)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.distrctcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.distrctcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.distrctcode).getColor() == null ? "input_cell"
						: (sv.distrctcode).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				
						
					</div>
				</div>
				
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Reference code")%></label>
						<%-- <%=smartHF.getHTMLVarExt(fw, sv.agentrefcode)%> --%>
						
						
						<input name='agentrefcode' type='text' style="width:190px;"
						<%formatValue = (sv.agentrefcode.getFormData()).toString();%>
						value='<%=XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.agentrefcode.getLength()%>'
						maxLength='<%=sv.agentrefcode.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(agentrefcode)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.agentrefcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.agentrefcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.agentrefcode).getColor() == null ? "input_cell"
						: (sv.agentrefcode).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				
						
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Manager")%></label>
						<%-- <%=smartHF.getHTMLVarExt(fw, sv.agencymgr)%> --%>
						
						
						
						<input name='agencymgr' type='text' style="width:150px;"
						<%formatValue = (sv.agencymgr.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.agencymgr.getLength()%>'
						maxLength='<%=sv.agencymgr.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(agencymgr)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.agencymgr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.agencymgr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.agencymgr).getColor() == null ? "input_cell"
						: (sv.agencymgr).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				
						
					</div>
				</div>
			</div>
			<%} %>
			<!--  ICIL-12 end  -->
			
			<div class="row">
			
			<div class="col-md-4">					
					<div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Reporting to")%></label>
				<table>
				<tr>
				<td class="input-group">
				<%	
					longValue = sv.repsel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.repsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div style="width: 100px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='repsel'  id='repsel'
				type='text' 
				value='<%=sv.repsel.getFormData()%>' 
				maxLength='<%=sv.repsel.getLength()%>' 
				size='<%=sv.repsel.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(repsel)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.repsel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.repsel).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                  <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('repsel')); doAction('PFKEY04')">
                     <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
               </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.repsel).getColor()== null  ? 
				"input_cell" :  (sv.repsel).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                  <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('repsel')); doAction('PFKEY04')">
                     <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
               </span>
				
				<%}longValue = null;} %>
				</td>
				
				<td  style="width:100px">
				  		
						<%					
						if(!((sv.repname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.repname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.repname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left:1px;width:100px" > <!-- style="float: left !important;margin-left: 5px !important;border-radius: 5px !important;height:25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;"> -->
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
				  </table>
				</div>
			</div>
	
		
			
				<div class="col-md-4">
			     <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Recruiter")%></label>
					<table><tr><td>
					<div class="input-group">
					<%	
						longValue = sv.zrecruit.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.zrecruit).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='zrecruit' id='zrecruit'
					type='text' 
					value='<%=sv.zrecruit.getFormData()%>' 
					maxLength='<%=sv.zrecruit.getLength()%>' 
					size='<%=sv.zrecruit.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(zrecruit)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.zrecruit).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.zrecruit).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
                       <button class="btn btn-info"  type="button" 
                             onClick="doFocus(document.getElementById('zrecruit')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                     </span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.zrecruit).getColor()== null  ? 
					"input_cell" :  (sv.zrecruit).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
                       <button class="btn btn-info"  type="button" 
                             onClick="doFocus(document.getElementById('zrecruit')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                     </span>
					
					<%}longValue = null;} %>
					
					
				</div>		</td>	 <td style="min-width:80px;padding-left:1px">
			  		
					<%					
					if(!((sv.agtname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agtname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agtname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					</td></tr></table>
				</div>
			</div>
			<!-- </div>
			<div class="row">	 -->		
				<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Override %"))%></label>

	<%	
			qpsf = fw.getFieldXMLDef((sv.ovcpc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

					<input name='ovcpc' type='text' style="text-align: right"
						value='<%=smartHF.getPicFormatted(qpsf, sv.ovcpc)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ovcpc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ovcpc)%>' <%}%>
						size='<%=sv.ovcpc.getLength()%>'
						maxLength='<%=sv.ovcpc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(ovcpc)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ovcpc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ovcpc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ovcpc).getColor() == null ? "input_cell"
						: (sv.ovcpc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			</div>
			
			<hr>
			
		<%-- 6th row --%>
		<div class="row">
				
			
					<div class="col-md-4" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("License No"))%></label>
						<div class="input-group"><input name='tlaglicno' type='text'
						<%formatValue = (sv.tlaglicno.getFormData()).toString();%>
						value='<%=XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.tlaglicno.getLength()%>'
						maxLength='<%=sv.tlaglicno.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(tlaglicno)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.tlaglicno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.tlaglicno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.tlaglicno).getColor() == null ? "input_cell"
						: (sv.tlaglicno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div></div>
			</div>
				
				 <div class="col-md-4" >
					<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Expiry Date"))%></label>
					<%
						if ((new Byte((sv.tlicexpdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.tlicexpdtDisp, (sv.tlicexpdtDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="tlicexpdtDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.tlicexpdtDisp, (sv.tlicexpdtDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>

				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Exclusive Agreement Indicator "))%></label>
					<div class="input-group" sty><input name='exclAgmt' type='text' style="width:100px;"
						<%formatValue = (sv.exclAgmt.getFormData()).toString();%>
						value='<%=XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.exclAgmt.getLength()%>'
						maxLength='<%=sv.exclAgmt.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(exclAgmt)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.exclAgmt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.exclAgmt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.exclAgmt).getColor() == null ? "input_cell"
						: (sv.exclAgmt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div></div>
		<div class="row">
		
		
		<div class="col-md-5">
			
			 <div class="form-group">
                 <label><%=resourceBundleHandler.gettingValueFromBundle("Black List Status")%></label>
				<table><tr><td >
				 <div class="input-group" > 
				<%	
					longValue = sv.tagsusind.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.tagsusind).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="width: 100px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='tagsusind'  id='tagsusind'
				type='text' 
				value='<%=sv.tagsusind.getFormData()%>' 
				maxLength='<%=sv.tagsusind.getLength()%>' 
				size='<%=sv.tagsusind.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(tagsusind)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.tagsusind).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.tagsusind).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                  <button class="btn btn-info"  type="button" 
                      onClick="doFocus(document.getElementById('tagsusind')); doAction('PFKEY04')">
                      <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
                </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.tagsusind).getColor()== null  ? 
				"input_cell" :  (sv.tagsusind).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                  <button class="btn btn-info"  type="button" 
                      onClick="doFocus(document.getElementById('tagsusind')); doAction('PFKEY04')">
                      <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
                </span>
				
				<%}longValue = null;} %>
				
				  	 </div> 	</td>	 <!--  <td> &nbsp; </td> --><td  style="min-width:80px;padding-left:1px">
						<%					
						if(!((sv.tagsusdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tagsusdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tagsusdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;padding-left:1px" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  				</td></tr></table>
  				</div>
  			</div>
	

				
		
		</div>
		
		<hr>
		
		<div class="row">
		
		
		<div class="col-md-6">
			
			<div class="form-group">
                 <label><%=resourceBundleHandler.gettingValueFromBundle("Account payee")%></label>
				<table><tr><td>
				 <div class="input-group"> 
				<%	
					longValue = sv.paysel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.paysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='paysel' id='paysel'
				type='text' 
				value='<%=sv.paysel.getFormData()%>' 
				maxLength='<%=sv.paysel.getLength()%>' 
				size='<%=sv.paysel.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(paysel)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.paysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.paysel).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                   <button class="btn btn-info"  type="button" 
                       onClick="doFocus(document.getElementById('paysel')); doAction('PFKEY04')">
                     <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                   </button>
                 </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.paysel).getColor()== null  ? 
				"input_cell" :  (sv.paysel).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                   <button class="btn btn-info"  type="button" 
                       onClick="doFocus(document.getElementById('paysel')); doAction('PFKEY04')">
                     <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                   </button>
                 </span>
				
				<%}longValue = null;} %>
				
			</div>	 </td>	<!--   <td> &nbsp; </td> --><td  style="min-width:80px;padding-left:1px">
				  		
						<%					
						if(!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
  					</td></tr></table>
  				</div>
  			</div>

			
			
			<div class="col-md-2"></div> 
			
			<div class="col-md-3">
                <div class="form-group">
                    <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum Amount"))%></label>
 <div class="input-group">
                    <%
                        qpsf = fw.getFieldXMLDef((sv.minsta).getFieldName());
                        //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                        valueThis = smartHF.getPicFormatted(qpsf, sv.minsta, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
                    %>

                    <input name='minsta' type='text' style="text-align: right"
                        value='<%=valueThis%>'
                        <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                        title='<%=valueThis%>' <%}%>
                        size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.minsta.getLength(), sv.minsta.getScale(), 3)%>'
                        maxLength='<%=sv.minsta.getLength()%>'
                        onFocus='doFocus(this),onFocusRemoveCommas(this)'
                        onHelp='return fieldHelp(minsta)'
                        onKeyUp='return checkMaxLength(this)'
                        onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                        decimal='<%=qpsf.getDecimals()%>'
                        onPaste='return doPasteNumber(event,true);'
                        onBlur='return doBlurNumberNew(event,true);'
                        <%if ((new Byte((sv.minsta).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                    || fw.getVariables().isScreenProtected()) {%>
                        readonly="true" class="output_cell"
                        <%} else if ((new Byte((sv.minsta).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                        class="bold_cell" <%} else {%>
                        class=' <%=(sv.minsta).getColor() == null ? "input_cell"
                        : (sv.minsta).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                        <%}%>>
                </div></div>
            </div>
			
		</div>
		<%-- 7th Row --%>
		
		<%-- 9th Row --%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Method ")%></label>
					 <div class="input-group"><%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "paymth" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("paymth");
						optionValue = makeDropDownList(mappedItems, sv.paymth.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.paymth.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.paymth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.paymth).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #ea4742;width: 165px;
   						 height: 28px;">
						<%
							}
						%>

						<select name='paymth' type='list'
							<%if ((new Byte((sv.paymth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.paymth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.paymth).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>

				</div>
			</div></div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pay Frequency")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "payfrq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("payfrq");
						optionValue = makeDropDownList(mappedItems, sv.payfrq.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.payfrq.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.payfrq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.payfrq).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #ea4742;width:168px;">
						<%
							}
						%>

						<select name='payfrq' type='list' style="width:165px;"
							<%if ((new Byte((sv.payfrq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.payfrq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.payfrq).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency ")%></label>
					 <div class="input-group"><%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "currcode" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("currcode");
						optionValue = makeDropDownList(mappedItems, sv.currcode.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.currcode.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.currcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.currcode).getColor())) {
					%>
					<div
						style="border: 2px; border-style: solid; border-color: #ea4742;width: 168px;height: 28px;">
						<%
							}
						%>

						<select name='currcode' type='list'
							<%if ((new Byte((sv.currcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.currcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.currcode).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>

				</div>
			</div>
		</div></div>
		
		<hr>
		<%-- 10th Row --%>
		<div class="row">
		
		
		<div class="col-md-4">
			 <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Basic commission")%></label>
				<table><tr><td >
				 <div class="input-group"> 
				<%	
					longValue = sv.bcmtab.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.bcmtab).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='bcmtab'  id='bcmtab'
				type='text' 
				value='<%=sv.bcmtab.getFormData()%>' 
				maxLength='<%=sv.bcmtab.getLength()%>' 
				size='<%=sv.bcmtab.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(bcmtab)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.bcmtab).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.bcmtab).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				 
				<span class="input-group-btn">
                   <button class="btn btn-info"  type="button" 
                      onClick="doFocus(document.getElementById('bcmtab')); doAction('PFKEY04')">
                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
                </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.bcmtab).getColor()== null  ? 
				"input_cell" :  (sv.bcmtab).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<span class="input-group-btn">
                   <button class="btn btn-info"  type="button" 
                      onClick="doFocus(document.getElementById('bcmtab')); doAction('PFKEY04')">
                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                  </button>
                </span>
				
				<%}longValue = null;} %>
				
					 </div> 	</td>	  <td style="min-width:80px;padding-left:1px">
						<%					
						if(!((sv.bcmdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bcmdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bcmdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
					</td></tr></table>
				</div>
			</div>
		
		
		
			<div class="col-md-4">
			
			    <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Servicing commission")%></label>
		
					<table><tr><td >
					 <div class="input-group"> 
					<%	
						longValue = sv.scmtab.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.scmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='scmtab'  id='scmtab'
					type='text' 
					value='<%=sv.scmtab.getFormData()%>' 
					maxLength='<%=sv.scmtab.getLength()%>' 
					size='<%=sv.scmtab.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(scmtab)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.scmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){  
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.scmtab).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
                       <button class="btn btn-info"  type="button" 
                           onClick="doFocus(document.getElementById('scmtab')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                    </span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.scmtab).getColor()== null  ? 
					"input_cell" :  (sv.scmtab).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
                       <button class="btn btn-info"  type="button" 
                           onClick="doFocus(document.getElementById('scmtab')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                    </span>
					
					<%}longValue = null;} %>
					
			 </div> </td>	  <td style="min-width:80px;padding-left:1px">
			  		
					<%					
					if(!((sv.scmdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.scmdsc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.scmdsc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  					</td></tr></table>
				</div>
			</div>	
			
		
			
			<div class="col-md-4">
			
			    <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Renewal commission")%></label>
					<table><tr><td style="min-width:80px">
					  <div class="input-group">
					<%	
						longValue = sv.rcmtab.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.rcmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='rcmtab'  id='rcmtab'
					type='text' 
					value='<%=sv.rcmtab.getFormData()%>' 
					maxLength='<%=sv.rcmtab.getLength()%>' 
					size='<%=sv.rcmtab.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(rcmtab)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.rcmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.rcmtab).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
					    <button class="btn btn-info"  type="button" 
					        onClick="doFocus(document.getElementById('rcmtab')); doAction('PFKEY04')">
					         <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					    </button>
					</span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.rcmtab).getColor()== null  ? 
					"input_cell" :  (sv.rcmtab).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
					    <button class="btn btn-info"  type="button" 
					        onClick="doFocus(document.getElementById('rcmtab')); doAction('PFKEY04')">
					         <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					    </button>
					</span>
					
					<%}longValue = null;} %>
			
			 </div>		</td>
			
			  
			  <td style="min-width:80px;padding-left:1px">
					<%					
					if(!((sv.rcmdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rcmdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rcmdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					</td></tr></table>
				</div>
			</div>
  
	
	
			
		</div>
	
	<%-- Last Row --%>
	<div class="row">
	
		<div class="col-md-4">
		
		    <div>
                   <label><%=resourceBundleHandler.gettingValueFromBundle("Commission class")%></label>
			<table><tr><td class="form-group">
			 <div class="input-group">
				
					<%	
						longValue = sv.agentClass.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.agentClass).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='agentClass'  id='agentClass'
					type='text' 
					value='<%=sv.agentClass.getFormData()%>' 
					maxLength='<%=sv.agentClass.getLength()%>' 
					size='<%=sv.agentClass.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(agentClass)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.agentClass).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.agentClass).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
                       <button class="btn btn-info" 
                        type="button" 
                           onClick="doFocus(document.getElementById('agentClass')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                     </span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.agentClass).getColor()== null  ? 
					"input_cell" :  (sv.agentClass).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
                       <button class="btn btn-info" 
                        type="button" 
                           onClick="doFocus(document.getElementById('agentClass')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                        </button>
                     </span>
					
					<%}longValue = null;} %>
					
		 </div> 
		  		</td>
			<!--   <td> &nbsp; </td> -->
			  
			  <td style="min-width:80px;">
				<%					
				if(!((sv.agclsd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.agclsd.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.agclsd.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 150px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  		
		  		</td></tr></table>
			</div>
		</div>
	
	
	<div class="col-md-6">
		
		<div class="form-group">
           <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus allocation")%></label>
<table><tr><td >
			 <div class="input-group">
					<%	
						longValue = sv.ocmtab.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.ocmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width:80px">  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					
					<input name='ocmtab'  id='ocmtab'
					type='text' 
					value='<%=sv.ocmtab.getFormData()%>' 
					maxLength='<%=sv.ocmtab.getLength()%>' 
					size='<%=sv.ocmtab.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(ocmtab)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.ocmtab).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.ocmtab).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<span class="input-group-btn">
                       <button class="btn btn-info" 
                        type="button" 
                             onClick="doFocus(document.getElementById('ocmtab')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                      </button>
                    </span>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.ocmtab).getColor()== null  ? 
					"input_cell" :  (sv.ocmtab).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
                       <button class="btn btn-info" 
                        type="button" 
                             onClick="doFocus(document.getElementById('ocmtab')); doAction('PFKEY04')">
                             <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                      </button>
                    </span>
					
					<%}longValue = null;} %>
			
			
			</div> 
		  		</td>
			<!--   <td> &nbsp; </td> -->
			  
			  <td style="min-width:80px;padding-left:1px">
			  		
					<%					
					if(!((sv.ocmdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ocmdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ocmdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div  class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
  				</div>
  			</div>
		
	
		
	</div>

<Div id='mainForm_OPTS' style='visibility:hidden;'>

<%=smartHF.getMenuLink(sv.clientind, resourceBundleHandler.gettingValueFromBundle("Client Details"))%>
<!--  ILJ-53--Start  -->
<%
if((sv.zrorind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.zrorind
.getEnabled()==BaseScreenData.DISABLED)){
%>

<div style="height: 15 px">
<a href="javascript:;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zrorind"))'
class="hyperLink" readonly="true">

 <%=smartHF.getMenuLink(sv.zrorind, resourceBundleHandler.gettingValueFromBundle("OR Details"))%>

</a>
</div>

 
 <%}else { %>
 <%=smartHF.getMenuLink(sv.zrorind, resourceBundleHandler.gettingValueFromBundle("OR Details"))%>
 <%} %>
 <!--  ILJ-53--End  -->
<%=smartHF.getMenuLink(sv.ddind, resourceBundleHandler.gettingValueFromBundle("Bank Account Details"))%>

<%=smartHF.getMenuLink(sv.bctind, resourceBundleHandler.gettingValueFromBundle("Broker contacts"))%>
<%=smartHF.getMenuLink(sv.tagd, resourceBundleHandler.gettingValueFromBundle("Tied Agent Details"))%>
<!--  ILJ-4 -->
<% if (sv.iljScreenflag.compareTo("Y") == 0){ %>
<%=smartHF.getMenuLink(sv.qualification, resourceBundleHandler.gettingValueFromBundle("Qualifications"))%>
<% } %>

<%=smartHF.getMenuLink(sv.agncysalic, resourceBundleHandler.gettingValueFromBundle("Agency Sales License"))%>

</div>
<div style='display:none'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.pymdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pymdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>

<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.pyfdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pyfdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pyfdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
</tr></table></div>



</div>

</div>

<script type="text/javascript">
$('#checkindicator').click(function() {    
    inputs = $('.input');
    inputs.each(function() {
        var value = $(this).val();	
        if( $( this ).attr( 'type' ) === 'checkbox' ) {
            value = +$(this).is( ':checked' );
        }      
        document.getElementById("checkindicator").value = value;
        document.getElementById("checkindicator1").value = value;
     
    	  document.getElementById("checkindicator1").style.visibility = 'visible';
    	  document.getElementById("checkindicator").style.visibility = 'hidden';
   
    	 //$('#checkindicator1').prop('checked', true);
        
    });    
});	

/* $('#checkindicator1').click(function() {    
    inputs = $('.input');
    inputs.each(function() {
        var value = $(this).val();	
        if( $( this ).attr( 'type' ) === 'checkbox' ) {
            value = +$(this).is( ':checked' );
        }      
        document.getElementById("checkindicator").value = value;
        document.getElementById("checkindicator1").value = value;
    
    	  document.getElementById("checkindicator1").style.visibility = 'hidden';
    	  document.getElementById("checkindicator").style.visibility = 'visible';
    	 // $('#checkindicator').prop('checked', false);
        
    });    
});
 */
</script>


<%@ include file="/POLACommon2NEW.jsp"%>