
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sjl31";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>

<%Sjl31ScreenVars sv = (Sjl31ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency Number ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Registration Number ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Brach");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Start Date");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"End Date ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initialy Provided Data Date");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Previously Provided Data Date ");%>


<%{
		if (appVars.ind20.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		
		if (appVars.ind23.isOn()) {
			sv.regnum.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.regnum.setReverse(BaseScreenData.REVERSED);
			sv.regnum.setColor(BaseScreenData.RED);
		}

		 if (appVars.ind24.isOn()) {
			sv.srdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.srdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.srdateDisp.setColor(BaseScreenData.RED);
		}
		
		if (appVars.ind25.isOn()) {
			sv.endateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.endateDisp.setReverse(BaseScreenData.REVERSED);
			sv.endateDisp.setColor(BaseScreenData.RED);
		}
		
		if (appVars.ind26.isOn()) {
			sv.ipdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.ipdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ipdateDisp.setColor(BaseScreenData.RED);
		}
		
		if (appVars.ind27.isOn()) {
			sv.ppdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.ppdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ppdateDisp.setColor(BaseScreenData.RED);
		} 
		
		if (appVars.ind10.isOn()) {
			sv.agntbr.setReverse(BaseScreenData.REVERSED);
			sv.agntbr.setColor(BaseScreenData.RED);
		}
		
		if (!appVars.ind10.isOn()) {
			sv.agntbr.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind37.isOn()) {
			sv.agntbr.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind91.isOn()) {
			sv.aracde.setReverse(BaseScreenData.REVERSED);
			sv.aracde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind91.isOn()) {
			sv.aracde.setHighLight(BaseScreenData.BOLD);
		} 
		if (appVars.ind38.isOn()) {
			sv.aracde.setEnabled(BaseScreenData.DISABLED);
		}

}%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="col-md-4">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					<table>
						<tr>
							<td class="form-group">
								<div class="input-group">
									<%	
							longValue = sv.clntsel.getFormData();  
					%>

									<% 
						if((new Byte((sv.clntsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
									<div
										class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%if(longValue != null){%>

										<%=longValue%>

										<%}%>
									</div>

									<%
					longValue = null;
					%>


									<% }else {%>
									<input name='clntsel' id='clntsel' type='text'
										value='<%=sv.clntsel.getFormData()%>'
										maxLength='<%=sv.clntsel.getLength()%>'
										size='<%=sv.clntsel.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(clntsel)'
										onKeyUp='return checkMaxLength(this)'
										<% 
						if((new Byte((sv.clntsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>
										readonly="true" class="output_cell">

									<%
						}else if((new Byte((sv.clntsel).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
						}else { 
					%>

									class = '
									<%=(sv.clntsel).getColor()== null  ? 
					"input_cell" :  (sv.clntsel).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' > <span
										class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
									<%}longValue = null;} %>

								</div>
							</td>
							<td style="min-width: 120px; padding-left: 1px">
								<%					
							if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										}
										%>

								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
									style="max-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							longValue = null;
							formatValue = null;
							%>

							</td>
						</tr>
					</table>

				</div></div>
				
				<div class="col-md-4" >
				<div >
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				 <table><tr><td class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.agntbr.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agntbr).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='agntbr' id='agntbr'
			type='text' 
			value='<%=sv.agntbr.getFormData()%>' 
			maxLength='<%=sv.agntbr.getLength()%>' 
			size='<%=sv.agntbr.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(agntbr)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.agntbr).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:50px" >
			
			<%
				}else if((new Byte((sv.agntbr).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:30px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.agntbr).getColor()== null  ? 
			"input_cell" :  (sv.agntbr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:30px">
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agntbr')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-4" >
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.aracde).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:24px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
			<input name='aracde' id='aracde'
			type='text' 
			value='<%=sv.aracde.getFormData()%>' 
			maxLength='<%=sv.aracde.getLength()%>' 
			size='<%=sv.aracde.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(aracde)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
				if((new Byte((sv.aracde).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	style="max-width:50px" >
			
			<%
				}else if((new Byte((sv.aracde).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				
			%>	
			class="bold_cell" style="min-width:34px">
			 
			<span class="input-group-btn">
               <button class="btn btn-info"   type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%
				}else { 
			%>
			
			class = ' <%=(sv.aracde).getColor()== null  ? 
			"input_cell" :  (sv.aracde).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="min-width:34px" >
			
			<span class="input-group-btn">
               <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('aracde')); doAction('PFKEY04')">
                  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
               </button>
             </span>
			
			<%}longValue = null;} %>			
				
				
				 </div> 		</td>
			 
		
			  <td >		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 142px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
			</div>
			<%-- 2nd start row  --%>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Agency Number "))%></label>
					<%
						if (!((sv.agncynum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agncynum.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agncynum.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

		<div class="col-md-4">
			<div class="form-group">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Registration Number"))%></label>
				<div class="input-group">
					<input name='regnum' type='text'
						<%formatValue = (sv.regnum.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.regnum.getLength()%>'
						maxLength='<%=sv.regnum.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(regnum)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.regnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.regnum).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.regnum).getColor() == null ? "input_cell"
						: (sv.regnum).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>

				</div>
			</div>
		</div>

       </div>
	<!-- 2nd row end -->
	
	<!-- 3rd row start -->
	<div class="row">
	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Date"))%></label>
									<% if ((new Byte((sv.srdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.srdateDisp)%></div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="yyyy/mm/dd" data-link-field="srdateDisp"
                                  data-link-format="yyyy/mm/dd" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.srdateDisp, (sv.srdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
 
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Date"))%></label>
									<% if ((new Byte((sv.endateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.endateDisp)%></div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="yyyy/mm/dd" data-link-field="endateDisp"
                                  data-link-format="yyyy/mm/dd" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.endateDisp, (sv.endateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
 
				</div>
			</div>
			</div>
	<!-- 3rd row end -->
	<!-- 4th row start -->
	<div class="row">
	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Initial Provided Data Date"))%></label>
									<% if ((new Byte((sv.ipdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.ipdateDisp)%></div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="yyyy/mm/dd" data-link-field="ipdateDisp"
                                  data-link-format="yyyy/mm/dd" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.ipdateDisp, (sv.ipdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
 
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Previously Provided Data Date"))%></label>
									<% if ((new Byte((sv.ppdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.ppdateDisp)%></div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="yyyy/mm/dd" data-link-field="ppdateDisp"
                                  data-link-format="yyyy/mm/dd" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.ppdateDisp, (sv.ppdateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 
 
				</div>
			</div>
	</div>
	<!-- 4th row end -->
	
   </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>