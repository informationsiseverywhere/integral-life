<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM506";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm506ScreenVars sv = (Sm506ScreenVars) fw.getVariables();%>

<%if (sv.Sm506screenWritten.gt(0)) {%>
	<%Sm506screen.clearClassString(sv);%>
	<%StringData generatedText3 = new StringData("A");%>
	<%StringData generatedText4 = new StringData("- Promote");%>
	<%StringData generatedText5 = new StringData("B");%>
	<%StringData generatedText6 = new StringData("- Demote");%>
	<%StringData generatedText7 = new StringData("C");%>
	<%StringData generatedText8 = new StringData("- Transfer");%>
	<%StringData generatedText9 = new StringData("D");%>
	<%StringData generatedText10 = new StringData("- Promotion History");%>
	<%StringData generatedText1 = new StringData("Agent No  ");%>
	<%sv.agntsel.setClassString("");%>
	<%StringData generatedText2 = new StringData("Action    ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.agntsel.setReverse(BaseScreenData.REVERSED);
			sv.agntsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agntsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	}%>
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent No")%></label>
					    		<div class="input-group" style="width:116px;">
						    		<input name='agntsel' id='agntsel'
		type='text'
		value='<%=sv.agntsel.getFormData()%>'
		maxLength='<%=sv.agntsel.getLength()%>'
		size='<%=sv.agntsel.getLength()%>'
		onFocus='doFocus(this)' onHelp='return fieldHelp(agntsel)' onKeyUp='return checkMaxLength(this)'
		
		<%
			if((new Byte((sv.agntsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
		%>
		readonly="true"
		class="output_cell"	 >
		
		<%
			}else if((new Byte((sv.agntsel).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
		
		%>
		<class="bold_cell" >
		<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
		
		
		<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('agntsel')); changeF4Image(this); doAction('PFKEY04')">
		<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
		</a> --%>
		
		<%
			}else {
		%>
		
		<class = ' <%=(sv.agntsel).getColor()== null  ?
		"input_cell" :  (sv.agntsel).getColor().equals("red") ?
		"input_cell red reverse" : "input_cell" %>' >
	
			<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('agntsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
		<%} %>
				      			</div>
				    		</div>
			    	</div>
			      
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-3">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Promote")%>
					</b></label>
				</div>
				<div class="col-md-3">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Demote")%>
				</b>	</label>			
			    </div>		        
				
			    <div class="col-md-3">
					  <label class="radio-inline">
					   <b><%=smartHF.buildRadioOption(sv.action, "action", "C")%><%=resourceBundleHandler.gettingValueFromBundle("Transfer")%>
					</b>   </label>
				</div>
				<div class="col-md-3">	  			
					  <label class="radio-inline">
					   <b> <%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Promotion History")%>
					</b>  </label>			
			    </div>		        
			</div>		
										
		</div>
	</div>	
	
	

<%@ include file="/POLACommon2NEW.jsp"%>