

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6688";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S6688ScreenVars sv = (S6688ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subroutine ");%>

<div class="panel panel-default">
<div class="panel-body">
<div class="row">
	<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		<div class="input-group">
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>
	</div>
</div>

<div class="row">
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
<div class="input-group" style="width:73px;">
<input name='agtchgsub' 
type='text'

<%

		formatValue = (sv.agtchgsub.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.agtchgsub.getLength()%>'
maxLength='<%= sv.agtchgsub.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(agtchgsub)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.agtchgsub).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	
<%
	}
%>
/>
</div>
</div>
</div>
</div></div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

