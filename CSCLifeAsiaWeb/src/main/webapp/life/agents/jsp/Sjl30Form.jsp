<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL30";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl30ScreenVars sv = (Sjl30ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Agency Appointment");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Agency Modify");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Agency Enquiry");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Agency Terminate");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Agency Reinstate");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agency Number");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
	if (appVars.ind01.isOn()) {
			sv.agncysel.setReverse(BaseScreenData.REVERSED);
			sv.agncysel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.agncysel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		} 
	}%>
	
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="min-width: 10px">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></label>
					<div class="input-group" style="min-width: 10px">

						<input name='agncysel' id='agncysel' type='text'
							value='<%=sv.agncysel.getFormData()%>'
							maxLength='<%=sv.agncysel.getLength()%>'
 							size='<%=sv.agncysel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(agncysel)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.agncysel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.agncysel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
	}else { 
%>

						class = '
						<%=(sv.agncysel).getColor()== null  ? 
"input_cell" :  (sv.agncysel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%} %>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>



<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
	</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline" checked> <%=smartHF.buildRadioOption(sv.action, "action", "A")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agency Appointment")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agency Modify")%></b>
				</label>
			</div>


			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "C")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agency Enquiry")%></b>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "D")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agency Terminate")%></b>
				</label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <%=smartHF.buildRadioOption(sv.action, "action", "E")%><b><%=resourceBundleHandler.gettingValueFromBundle("Agency Reinstate")%></b>
				</label>
			</div>
		</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>