<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH622";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sh622ScreenVars sv = (Sh622ScreenVars) fw.getVariables();%>

<%if (sv.Sh622screenWritten.gt(0)) {%>
	<%Sh622screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comm/Prem");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 1");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 2");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 3");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 4");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 5");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top Up");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Basis");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 6");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 7");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 8");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 9");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year 10");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(C or P)");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Direct OR");%>
	<%sv.keyopt01.setClassString("");%>
<%	sv.keyopt01.appendClassString("string_fld");
	sv.keyopt01.appendClassString("input_txt");
	sv.keyopt01.appendClassString("highlight");
%>
	<%sv.zryrperc01.setClassString("");%>
<%	sv.zryrperc01.appendClassString("num_fld");
	sv.zryrperc01.appendClassString("input_txt");
	sv.zryrperc01.appendClassString("highlight");
%>
	<%sv.zryrperc02.setClassString("");%>
<%	sv.zryrperc02.appendClassString("num_fld");
	sv.zryrperc02.appendClassString("input_txt");
	sv.zryrperc02.appendClassString("highlight");
%>
	<%sv.zryrperc03.setClassString("");%>
<%	sv.zryrperc03.appendClassString("num_fld");
	sv.zryrperc03.appendClassString("input_txt");
	sv.zryrperc03.appendClassString("highlight");
%>
	<%sv.zryrperc04.setClassString("");%>
<%	sv.zryrperc04.appendClassString("num_fld");
	sv.zryrperc04.appendClassString("input_txt");
	sv.zryrperc04.appendClassString("highlight");
%>
	<%sv.zryrperc05.setClassString("");%>
<%	sv.zryrperc05.appendClassString("num_fld");
	sv.zryrperc05.appendClassString("input_txt");
	sv.zryrperc05.appendClassString("highlight");
%>
	<%sv.zryrperc21.setClassString("");%>
<%	sv.zryrperc21.appendClassString("num_fld");
	sv.zryrperc21.appendClassString("input_txt");
	sv.zryrperc21.appendClassString("highlight");
%>
	<%sv.zryrperc06.setClassString("");%>
<%	sv.zryrperc06.appendClassString("num_fld");
	sv.zryrperc06.appendClassString("input_txt");
	sv.zryrperc06.appendClassString("highlight");
%>
	<%sv.zryrperc07.setClassString("");%>
<%	sv.zryrperc07.appendClassString("num_fld");
	sv.zryrperc07.appendClassString("input_txt");
	sv.zryrperc07.appendClassString("highlight");
%>
	<%sv.zryrperc08.setClassString("");%>
<%	sv.zryrperc08.appendClassString("num_fld");
	sv.zryrperc08.appendClassString("input_txt");
	sv.zryrperc08.appendClassString("highlight");
%>
	<%sv.zryrperc09.setClassString("");%>
<%	sv.zryrperc09.appendClassString("num_fld");
	sv.zryrperc09.appendClassString("input_txt");
	sv.zryrperc09.appendClassString("highlight");
%>
	<%sv.zryrperc10.setClassString("");%>
<%	sv.zryrperc10.appendClassString("num_fld");
	sv.zryrperc10.appendClassString("input_txt");
	sv.zryrperc10.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Indirect OR");%>
	<%sv.keyopt02.setClassString("");%>
<%	sv.keyopt02.appendClassString("string_fld");
	sv.keyopt02.appendClassString("input_txt");
	sv.keyopt02.appendClassString("highlight");
%>
	<%sv.zryrperc11.setClassString("");%>
<%	sv.zryrperc11.appendClassString("num_fld");
	sv.zryrperc11.appendClassString("input_txt");
	sv.zryrperc11.appendClassString("highlight");
%>
	<%sv.zryrperc12.setClassString("");%>
<%	sv.zryrperc12.appendClassString("num_fld");
	sv.zryrperc12.appendClassString("input_txt");
	sv.zryrperc12.appendClassString("highlight");
%>
	<%sv.zryrperc13.setClassString("");%>
<%	sv.zryrperc13.appendClassString("num_fld");
	sv.zryrperc13.appendClassString("input_txt");
	sv.zryrperc13.appendClassString("highlight");
%>
	<%sv.zryrperc14.setClassString("");%>
<%	sv.zryrperc14.appendClassString("num_fld");
	sv.zryrperc14.appendClassString("input_txt");
	sv.zryrperc14.appendClassString("highlight");
%>
	<%sv.zryrperc15.setClassString("");%>
<%	sv.zryrperc15.appendClassString("num_fld");
	sv.zryrperc15.appendClassString("input_txt");
	sv.zryrperc15.appendClassString("highlight");
%>
	<%sv.zryrperc22.setClassString("");%>
<%	sv.zryrperc22.appendClassString("num_fld");
	sv.zryrperc22.appendClassString("input_txt");
	sv.zryrperc22.appendClassString("highlight");
%>
	<%sv.zryrperc16.setClassString("");%>
<%	sv.zryrperc16.appendClassString("num_fld");
	sv.zryrperc16.appendClassString("input_txt");
	sv.zryrperc16.appendClassString("highlight");
%>
	<%sv.zryrperc17.setClassString("");%>
<%	sv.zryrperc17.appendClassString("num_fld");
	sv.zryrperc17.appendClassString("input_txt");
	sv.zryrperc17.appendClassString("highlight");
%>
	<%sv.zryrperc18.setClassString("");%>
<%	sv.zryrperc18.appendClassString("num_fld");
	sv.zryrperc18.appendClassString("input_txt");
	sv.zryrperc18.appendClassString("highlight");
%>
	<%sv.zryrperc19.setClassString("");%>
<%	sv.zryrperc19.appendClassString("num_fld");
	sv.zryrperc19.appendClassString("input_txt");
	sv.zryrperc19.appendClassString("highlight");
%>
	<%sv.zryrperc20.setClassString("");%>
<%	sv.zryrperc20.appendClassString("num_fld");
	sv.zryrperc20.appendClassString("input_txt");
	sv.zryrperc20.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

<div class="panel panel-default">
<div class="panel-body">
<div class="row">
<div class="col-md-4">
	<div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
</div>

<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</div>
</div>

<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<div class="input-group">
	<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
</div>
</div>
</div>
</div>

 <div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
			</div>


</br>


<div class="row">

<div class="col-md-2">
</div>

<div class="col-md-2" style="max-width:110px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Comm/Prem")%></label><br/>
<label>Basis&nbsp;(C&nbsp;or&nbsp;P)</label>

</div>
</div>

<div class="col-md-2" style="max-width:110px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%>&nbsp;1</label><br/>
<label><label>Year&nbsp;6</label></label>
</div>
</div>

<div class="col-md-2" style="max-width:90px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%>&nbsp;2</label><br/>
<label><label>Year&nbsp;7</label></label>
</div>
</div>

<div class="col-md-2" style="max-width:85px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%>&nbsp;3</label><br/>
<label><label>Year&nbsp;8</label></label>
</div>
</div>


<div class="col-md-2" style="max-width:110px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%>&nbsp;4</label><br/>
<label><label>Year&nbsp;9</label></label>
</div>
</div>

<div class="col-md-2" style="max-width:75px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Year")%>&nbsp;5</label><br/>
<label><label>Year&nbsp;10</label></label>
</div>
</div>

<div class="col-md-2" style="max-width:110px;text-align: center;">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Top")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Up")%></label>
</div>
</div>

</div>
<br><br>
<div class="row">
<div class="col-md-1">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Direct")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("OR")%></label>
</div>
</div>
<div class="col-md-1">
<div class="form-group">

</div>
</div>
<div class="col-md-2" style="max-width: 120px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.keyopt01)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc01)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc06)%>
</div>
</div>

<div class="col-md-2" style="width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc02)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc07)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc03)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc08)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc04)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc09)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc05)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc10)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc21)%>
</div>
</div>
</div>
<br>
<div class="row">
<div class="col-md-1">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Indirect")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("OR")%></label>
</div>
</div>
<div class="col-md-1">
<div class="form-group">

</div>
</div>
<div class="col-md-2" style="max-width: 120px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.keyopt02)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc11)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc16)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc12)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc17)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc13)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc18)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc14)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc19)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc15)%>
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc20)%>
</div>
</div>

<div class="col-md-2" style="max-width: 95px;"  >
<div class="form-group">
<%=smartHF.getHTMLVarExt(fw, sv.zryrperc22)%>
</div>
</div>
</div>
</div>
</div>	




<%}%>

<%if (sv.Sh622protectWritten.gt(0)) {%>
	<%Sh622protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<%@ include file="/POLACommon2NEW.jsp"%>
