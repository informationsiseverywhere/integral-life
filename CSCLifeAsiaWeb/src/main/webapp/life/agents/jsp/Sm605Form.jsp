<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SM605";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sm605ScreenVars sv = (Sm605ScreenVars) fw.getVariables();%>


<%if (sv.Sm605screenWritten.gt(0)) {%>
	<%Sm605screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");

%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid Promotion Codes");%>
	<%sv.mlagttyp01.setClassString("");%>
<%	sv.mlagttyp01.appendClassString("string_fld");
	sv.mlagttyp01.appendClassString("input_txt");
	sv.mlagttyp01.appendClassString("highlight");
%>
	<%sv.mlagttyp02.setClassString("");%>
<%	sv.mlagttyp02.appendClassString("string_fld");
	sv.mlagttyp02.appendClassString("input_txt");
	sv.mlagttyp02.appendClassString("highlight");
%>
	<%sv.mlagttyp03.setClassString("");%>
<%	sv.mlagttyp03.appendClassString("string_fld");
	sv.mlagttyp03.appendClassString("input_txt");
	sv.mlagttyp03.appendClassString("highlight");
%>
	<%sv.mlagttyp04.setClassString("");%>
<%	sv.mlagttyp04.appendClassString("string_fld");
	sv.mlagttyp04.appendClassString("input_txt");
	sv.mlagttyp04.appendClassString("highlight");
%>
	<%sv.mlagttyp05.setClassString("");%>
<%	sv.mlagttyp05.appendClassString("string_fld");
	sv.mlagttyp05.appendClassString("input_txt");
	sv.mlagttyp05.appendClassString("highlight");
%>
	<%sv.mlagttyp06.setClassString("");%>
<%	sv.mlagttyp06.appendClassString("string_fld");
	sv.mlagttyp06.appendClassString("input_txt");
	sv.mlagttyp06.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid Demotion codes ");%>
	<%sv.mlagttyp07.setClassString("");%>
<%	sv.mlagttyp07.appendClassString("string_fld");
	sv.mlagttyp07.appendClassString("input_txt");
	sv.mlagttyp07.appendClassString("highlight");
%>
	<%sv.mlagttyp08.setClassString("");%>
<%	sv.mlagttyp08.appendClassString("string_fld");
	sv.mlagttyp08.appendClassString("input_txt");
	sv.mlagttyp08.appendClassString("highlight");
%>
	<%sv.mlagttyp09.setClassString("");%>
<%	sv.mlagttyp09.appendClassString("string_fld");
	sv.mlagttyp09.appendClassString("input_txt");
	sv.mlagttyp09.appendClassString("highlight");
%>
	<%sv.mlagttyp10.setClassString("");%>
<%	sv.mlagttyp10.appendClassString("string_fld");
	sv.mlagttyp10.appendClassString("input_txt");
	sv.mlagttyp10.appendClassString("highlight");
%>
	<%sv.mlagttyp11.setClassString("");%>
<%	sv.mlagttyp11.appendClassString("string_fld");
	sv.mlagttyp11.appendClassString("input_txt");
	sv.mlagttyp11.appendClassString("highlight");
%>
	<%sv.mlagttyp12.setClassString("");%>
<%	sv.mlagttyp12.appendClassString("string_fld");
	sv.mlagttyp12.appendClassString("input_txt");
	sv.mlagttyp12.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid Transfer Codes ");%>
	<%sv.mlagttyp13.setClassString("");%>
<%	sv.mlagttyp13.appendClassString("string_fld");
	sv.mlagttyp13.appendClassString("input_txt");
	sv.mlagttyp13.appendClassString("highlight");
%>
	<%sv.mlagttyp14.setClassString("");%>
<%	sv.mlagttyp14.appendClassString("string_fld");
	sv.mlagttyp14.appendClassString("input_txt");
	sv.mlagttyp14.appendClassString("highlight");
%>
	<%sv.mlagttyp15.setClassString("");%>
<%	sv.mlagttyp15.appendClassString("string_fld");
	sv.mlagttyp15.appendClassString("input_txt");
	sv.mlagttyp15.appendClassString("highlight");
%>
	<%sv.mlagttyp16.setClassString("");%>
<%	sv.mlagttyp16.appendClassString("string_fld");
	sv.mlagttyp16.appendClassString("input_txt");
	sv.mlagttyp16.appendClassString("highlight");
%>
	<%sv.mlagttyp17.setClassString("");%>
<%	sv.mlagttyp17.appendClassString("string_fld");
	sv.mlagttyp17.appendClassString("input_txt");
	sv.mlagttyp17.appendClassString("highlight");
%>
	<%sv.mlagttyp18.setClassString("");%>
<%	sv.mlagttyp18.appendClassString("string_fld");
	sv.mlagttyp18.appendClassString("input_txt");
	sv.mlagttyp18.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Parallel ORC applicable");%>
<%	generatedText12.appendClassString("label_txt");
	generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind01.setClassString("");%>
<%	sv.mlprcind01.appendClassString("string_fld");
	sv.mlprcind01.appendClassString("input_txt");
	sv.mlprcind01.appendClassString("highlight");
	sv.mlprcind01.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind02.setClassString("");%>
<%	sv.mlprcind02.appendClassString("string_fld");
	sv.mlprcind02.appendClassString("input_txt");
	sv.mlprcind02.appendClassString("highlight");
	sv.mlprcind02.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind03.setClassString("");%>
<%	sv.mlprcind03.appendClassString("string_fld");
	sv.mlprcind03.appendClassString("input_txt");
	sv.mlprcind03.appendClassString("highlight");
	sv.mlprcind03.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind04.setClassString("");%>
<%	sv.mlprcind04.appendClassString("string_fld");
	sv.mlprcind04.appendClassString("input_txt");
	sv.mlprcind04.appendClassString("highlight");
	sv.mlprcind04.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind05.setClassString("");%>
<%	sv.mlprcind05.appendClassString("string_fld");
	sv.mlprcind05.appendClassString("input_txt");
	sv.mlprcind05.appendClassString("highlight");
	sv.mlprcind05.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlprcind06.setClassString("");%>
<%	sv.mlprcind06.appendClassString("string_fld");
	sv.mlprcind06.appendClassString("input_txt");
	sv.mlprcind06.appendClassString("highlight");
	sv.mlprcind06.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Yes, No, Optional)");%>
<%	generatedText21.appendClassString("label_txt");
	generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year");%>
<%	generatedText15.appendClassString("label_txt");
	generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"FYP Production Level ");%>
<%	generatedText16.appendClassString("label_txt");
	generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"For Current Level ");%>
<%	generatedText17.appendClassString("label_txt");
	generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"For Promotion ");%>
<%	generatedText18.appendClassString("label_txt");
	generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"For Parallel ORC ");%>
<%	generatedText19.appendClassString("label_txt");
	generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.toYear01.setClassString("");%>
<%	sv.toYear01.appendClassString("num_fld");
	sv.toYear01.appendClassString("input_txt");
	sv.toYear01.appendClassString("highlight");
	sv.toYear01.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlperpp01.setClassString("");%>
<%	sv.mlperpp01.appendClassString("num_fld");
	sv.mlperpp01.appendClassString("input_txt");
	sv.mlperpp01.appendClassString("highlight");
	sv.mlperpp01.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlagtprd01.setClassString("");%>
<%	sv.mlagtprd01.appendClassString("num_fld");
	sv.mlagtprd01.appendClassString("input_txt");
	sv.mlagtprd01.appendClassString("highlight");
	sv.mlagtprd01.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlgrppp01.setClassString("");%>
<%	sv.mlgrppp01.appendClassString("num_fld");
	sv.mlgrppp01.appendClassString("input_txt");
	sv.mlgrppp01.appendClassString("highlight");
	sv.mlgrppp01.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.toYear02.setClassString("");%>
<%	sv.toYear02.appendClassString("num_fld");
	sv.toYear02.appendClassString("input_txt");
	sv.toYear02.appendClassString("highlight");
	sv.toYear02.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlperpp02.setClassString("");%>
<%	sv.mlperpp02.appendClassString("num_fld");
	sv.mlperpp02.appendClassString("input_txt");
	sv.mlperpp02.appendClassString("highlight");
	sv.mlperpp02.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlagtprd02.setClassString("");%>
<%	sv.mlagtprd02.appendClassString("num_fld");
	sv.mlagtprd02.appendClassString("input_txt");
	sv.mlagtprd02.appendClassString("highlight");
	sv.mlagtprd02.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlgrppp02.setClassString("");%>
<%	sv.mlgrppp02.appendClassString("num_fld");
	sv.mlgrppp02.appendClassString("input_txt");
	sv.mlgrppp02.appendClassString("highlight");
	sv.mlgrppp02.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.toYear03.setClassString("");%>
<%	sv.toYear03.appendClassString("num_fld");
	sv.toYear03.appendClassString("input_txt");
	sv.toYear03.appendClassString("highlight");
	sv.toYear03.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlperpp03.setClassString("");%>
<%	sv.mlperpp03.appendClassString("num_fld");
	sv.mlperpp03.appendClassString("input_txt");
	sv.mlperpp03.appendClassString("highlight");
	sv.mlperpp03.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlagtprd03.setClassString("");%>
<%	sv.mlagtprd03.appendClassString("num_fld");
	sv.mlagtprd03.appendClassString("input_txt");
	sv.mlagtprd03.appendClassString("highlight");
	sv.mlagtprd03.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlgrppp03.setClassString("");%>
<%	sv.mlgrppp03.appendClassString("num_fld");
	sv.mlgrppp03.appendClassString("input_txt");
	sv.mlgrppp03.appendClassString("highlight");
	sv.mlgrppp03.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.toYear04.setClassString("");%>
<%	sv.toYear04.appendClassString("num_fld");
	sv.toYear04.appendClassString("input_txt");
	sv.toYear04.appendClassString("highlight");
	sv.toYear04.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlperpp04.setClassString("");%>
<%	sv.mlperpp04.appendClassString("num_fld");
	sv.mlperpp04.appendClassString("input_txt");
	sv.mlperpp04.appendClassString("highlight");
	sv.mlperpp04.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlagtprd04.setClassString("");%>
<%	sv.mlagtprd04.appendClassString("num_fld");
	sv.mlagtprd04.appendClassString("input_txt");
	sv.mlagtprd04.appendClassString("highlight");
	sv.mlagtprd04.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlgrppp04.setClassString("");%>
<%	sv.mlgrppp04.appendClassString("num_fld");
	sv.mlgrppp04.appendClassString("input_txt");
	sv.mlgrppp04.appendClassString("highlight");
	sv.mlgrppp04.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.toYear05.setClassString("");%>
<%	sv.toYear05.appendClassString("num_fld");
	sv.toYear05.appendClassString("input_txt");
	sv.toYear05.appendClassString("highlight");
	sv.toYear05.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlperpp05.setClassString("");%>
<%	sv.mlperpp05.appendClassString("num_fld");
	sv.mlperpp05.appendClassString("input_txt");
	sv.mlperpp05.appendClassString("highlight");
	sv.mlperpp05.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlagtprd05.setClassString("");%>
<%	sv.mlagtprd05.appendClassString("num_fld");
	sv.mlagtprd05.appendClassString("input_txt");
	sv.mlagtprd05.appendClassString("highlight");
	sv.mlagtprd05.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%sv.mlgrppp05.setClassString("");%>
<%	sv.mlgrppp05.appendClassString("num_fld");
	sv.mlgrppp05.appendClassString("input_txt");
	sv.mlgrppp05.appendClassString("highlight");
	sv.mlgrppp05.setInvisibility(BaseScreenData.INVISIBLE);
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>



<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                            <%					
	                   	if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
				     	} else  {
								
				    	if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
				    	}
					  %>			
				    <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
			      	<%=XSSFilter.escapeHtml(formatValue)%>
			      </div>	
		       <%
		         longValue = null;
	           	formatValue = null;
		          %>



</div></div>

                  <div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                           	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div>

                    <div class="col-md-4" > 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                             
                             <div class="input-group" style="max-width:500px;">
                              <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:50px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
    
                  </div></div>

</div>
























			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
			    	     
			    	     <table><tr><td>				    	     	
			    	     	<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 						
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}														
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>
		
        &nbsp <label><%=resourceBundleHandler.gettingValueFromBundle("to")%>   &nbsp</label>

        </td>
        
        <td>

<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 						
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</td></tr></table>
		     </div></div></div>
			    	     

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <br><br>
  
  		<div class="row">
<div class="col-md-3">
<label><%= generatedText10%></label>
        </div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp01)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp02)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp03)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp04)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp05)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp06)%>   </div>
	        </div>


<br>

<div class="row">
<div class="col-md-3">
			<label><%= generatedText13%></label>
			</div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp07)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp08)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp09)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp10)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp11)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp12)%>      </div>            </div>

<br>

<div class="row">
<div class="col-md-3">
			<label><%= generatedText14%></label>
			</div>
		
<div class="col-md-1">

	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp13)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp14)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp15)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp16)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp17)%></div>
<div class="col-md-1">
	<%=smartHF.getHTMLVarExt(fw, sv.mlagttyp18)%> </div>                 </div>
   


</div></div>
























<%--

 <div class='outerDiv'>
<table>

<tr style='height:42px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




&nbsp;
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>




	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table></div>
                                                   --%>



	<%-- <%=smartHF.getLit(8, 3.2, generatedText10)%> 
	
		 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 Start-->

	<%=smartHF.getHTMLVar(8, 29, fw, sv.mlagttyp01).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(8, 34, fw, sv.mlagttyp02).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(8, 39, fw, sv.mlagttyp03).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(8, 44, fw, sv.mlagttyp04).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(8, 49, fw, sv.mlagttyp05).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(8, 54, fw, sv.mlagttyp06).replace("width:21","width:26")%>

	<%=smartHF.getLit(9, 3.2, generatedText13)%>

	<%=smartHF.getHTMLVar(9, 29, fw, sv.mlagttyp07).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(9, 34, fw, sv.mlagttyp08).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(9, 39, fw, sv.mlagttyp09).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(9, 44, fw, sv.mlagttyp10).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(9, 49, fw, sv.mlagttyp11).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(9, 54, fw, sv.mlagttyp12).replace("width:21","width:26")%>

	<%=smartHF.getLit(10, 3.2, generatedText14)%>

	<%=smartHF.getHTMLVar(10, 29, fw, sv.mlagttyp13).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(10, 34, fw, sv.mlagttyp14).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(10, 39, fw, sv.mlagttyp15).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(10, 44, fw, sv.mlagttyp16).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(10, 49, fw, sv.mlagttyp17).replace("width:21","width:26")%>

	<%=smartHF.getHTMLVar(10, 54, fw, sv.mlagttyp18).replace("width:21","width:26")%>  
	
	                                                                                             --%>
	
		
		
		
		
		
		
		
		
		
		 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 ends-->

	<%-- <%=smartHF.getLit(11, 2, generatedText12)%>

	<%=smartHF.getHTMLVar(11, 29, fw, sv.mlprcind01)%>

	<%=smartHF.getHTMLVar(11, 34, fw, sv.mlprcind02)%>

	<%=smartHF.getHTMLVar(11, 39, fw, sv.mlprcind03)%>

	<%=smartHF.getHTMLVar(11, 44, fw, sv.mlprcind04)%>

	<%=smartHF.getHTMLVar(11, 49, fw, sv.mlprcind05)%>

	<%=smartHF.getHTMLVar(11, 54, fw, sv.mlprcind06)%>

	<%=smartHF.getLit(11, 59, generatedText21)%>

	<%=smartHF.getLit(12, 5, generatedText15)%>

	<%=smartHF.getLit(12, 31, generatedText16)%>

	<%=smartHF.getLit(13, 17, generatedText17)%>

	<%=smartHF.getLit(13, 42, generatedText18)%>

	<%=smartHF.getLit(13, 61, generatedText19)%>

	<%=smartHF.getHTMLVar(15, 6, fw, sv.toYear01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 17, fw, sv.mlperpp01, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(15, 38, fw, sv.mlagtprd01, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(15, 17, fw, sv.mlperpp01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 38, fw, sv.mlagtprd01, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(15, 59, fw, sv.mlgrppp01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 6, fw, sv.toYear02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 17, fw, sv.mlperpp02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 38, fw, sv.mlagtprd02, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(16, 59, fw, sv.mlgrppp02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 6, fw, sv.toYear03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 17, fw, sv.mlperpp03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 38, fw, sv.mlagtprd03, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(17, 59, fw, sv.mlgrppp03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 6, fw, sv.toYear04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 17, fw, sv.mlperpp04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 38, fw, sv.mlagtprd04, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(18, 59, fw, sv.mlgrppp04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 6, fw, sv.toYear05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 17, fw, sv.mlperpp05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 38, fw, sv.mlagtprd05, COBOLHTMLFormatter.S15VS2)%>

	<%=smartHF.getHTMLVar(19, 59, fw, sv.mlgrppp05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(22, 73, generatedText20)%>

	<%=smartHF.getLit(22, 75, generatedText11)%> --%>





<%}%>

<%-- <%if (sv.Sm605protectWritten.gt(0)) {%>
	<%Sm605protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%> --%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%> 
<%@ include file="/POLACommon2NEW.jsp"%>

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 Start-->

<!-- <style>
div[id*='mlagttyp']{padding-right:4px !important} 

</style>  -->

	 <!--  Ilife- Life Cross Browser - Sprint 4 D5 : Task 1 ends-->