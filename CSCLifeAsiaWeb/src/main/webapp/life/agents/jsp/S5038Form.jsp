
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5038";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5038ScreenVars sv = (S5038ScreenVars) fw.getVariables();%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.contact.setReverse(BaseScreenData.REVERSED);
			sv.contact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.contact.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>
<%		appVars.rollup(new int[] {93});
%>



<div class="panel panel-default">
<div class="panel-body">     
			 <div class="row">	
			    	
<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		<table><tr><td>
						    			<%					
		if(!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="min-width:1px">
</td><td style="min-width:100px">



	
  		
		<%					
		if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</td></tr></table>
				    		</div>
				       </div>
			 </div> 
				    
			 <div class="row">	
			    	<div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				       
				     <div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
					    		<table><tr><td>
						    		<%					
		if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td style="min-width:1px">
</td><td style="min-width:100px">




	
  		
		<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
				      			</div>
				    		</div>
				       </div>
				   



<div class="row">		
     <div class="col-md-12">
         <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover" id="dataTables-s5038" width="100%"> 

  <thead>
    <tr class='info'>
      <th width="1%"><center><%=resourceBundleHandler.gettingValueFromBundle("Client")%></center></th>        								
       <th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Name")%></center></th>
		</tr>
 </thead>
 
  <tbody>
 
  
  <%GeneralTable sfl = fw.getTable("s5038screensfl");%>
  <%String backgroundcolor="#FFFFFF";
  S5038screensfl.set1stScreenRow(sfl, appVars, sv);
  int count = 1;
  while (S5038screensfl
			.hasMoreScreenRows(sfl)){
	  %>
	  
		<tr style="background:<%= backgroundcolor%>;">
															
		
							

	
	<!--  <div class="input-group"> -->
	 	 <td  style="min-width:120px"
									<%if(!(((BaseScreenData)sv.contact) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
													<%if((new Byte((sv.contact).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
																											<% 
							if((new Byte((sv.contact).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							longValue = sv.contact.getFormData(); 
						%> 
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 110px;">  
	   										<%if(longValue != null){%>
	   		 											<%=longValue%>
				
	   										<%}%>
	   						</div>
								<%longValue=null; %>
						<% }else {%>
						<div class="input-group" style="max-width:130px">
							<input name='<%="s5038screensfl" + "." + "contact" + "_R" + count%>'
							id='<%="s5038screensfl" + "." + "contact" + "_R" + count%>'
							type='text' 
							value='<%= sv.contact.getFormData() %>' 
							class = " <%=(sv.contact).getColor()== null  ? 
							"input_cell" :  
							(sv.contact).getColor().equals("red") ? 
							"input_cell red reverse" : 
							"input_cell" %>" 
							maxLength='<%=sv.contact.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(s5038screensfl.contact)' onKeyUp='return checkMaxLength(this)' 
							 style = "width:<%=sv.contact.getLength()*12+40%>px;"
							>	
							<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('<%="s5038screensfl" + "." + "contact" + "_R" + count%>')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
								
                          </div>
						<%}%>
																					<%}%>
				
			<!-- </div> -->
		</td>
		<td >									
			<%= sv.contactnam.getFormData()%>
		</td>
	</tr>
						  <%
	if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
		backgroundcolor="#ededed";
		}else{
		backgroundcolor="#FFFFFF";
		}
	count = count + 1;
	S5038screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%> 
    
       </tbody>
      </table>
    </div>
  </div>
</div>

</div>
</div>  

 <script>
$(document).ready(function() {
	$('#dataTables-s5038').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

