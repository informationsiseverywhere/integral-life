<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5695";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5695ScreenVars sv = (S5695ScreenVars) fw.getVariables();%>

<%if (sv.S5695screenWritten.gt(0)) {%>
	<%S5695screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid to ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"%");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max %");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Earned");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From    To");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission %");%>
	<%sv.freqFrom01.setClassString("");%>
<%	sv.freqFrom01.appendClassString("string_fld");
	sv.freqFrom01.appendClassString("input_txt");
	sv.freqFrom01.appendClassString("highlight");
%>
	<%sv.freqTo01.setClassString("");%>
<%	sv.freqTo01.appendClassString("string_fld");
	sv.freqTo01.appendClassString("input_txt");
	sv.freqTo01.appendClassString("highlight");
%>
	<%sv.cmrate01.setClassString("");%>
<%	sv.cmrate01.appendClassString("num_fld");
	sv.cmrate01.appendClassString("input_txt");
	sv.cmrate01.appendClassString("highlight");
%>
	<%sv.premaxpc01.setClassString("");%>
<%	sv.premaxpc01.appendClassString("num_fld");
	sv.premaxpc01.appendClassString("input_txt");
	sv.premaxpc01.appendClassString("highlight");
%>
	<%sv.commenpc01.setClassString("");%>
<%	sv.commenpc01.appendClassString("num_fld");
	sv.commenpc01.appendClassString("input_txt");
	sv.commenpc01.appendClassString("highlight");
%>
	<%sv.freqFrom02.setClassString("");%>
<%	sv.freqFrom02.appendClassString("string_fld");
	sv.freqFrom02.appendClassString("input_txt");
	sv.freqFrom02.appendClassString("highlight");
%>
	<%sv.freqTo02.setClassString("");%>
<%	sv.freqTo02.appendClassString("string_fld");
	sv.freqTo02.appendClassString("input_txt");
	sv.freqTo02.appendClassString("highlight");
%>
	<%sv.cmrate02.setClassString("");%>
<%	sv.cmrate02.appendClassString("num_fld");
	sv.cmrate02.appendClassString("input_txt");
	sv.cmrate02.appendClassString("highlight");
%>
	<%sv.premaxpc02.setClassString("");%>
<%	sv.premaxpc02.appendClassString("num_fld");
	sv.premaxpc02.appendClassString("input_txt");
	sv.premaxpc02.appendClassString("highlight");
%>
	<%sv.commenpc02.setClassString("");%>
<%	sv.commenpc02.appendClassString("num_fld");
	sv.commenpc02.appendClassString("input_txt");
	sv.commenpc02.appendClassString("highlight");
%>
	<%sv.freqFrom03.setClassString("");%>
<%	sv.freqFrom03.appendClassString("string_fld");
	sv.freqFrom03.appendClassString("input_txt");
	sv.freqFrom03.appendClassString("highlight");
%>
	<%sv.freqTo03.setClassString("");%>
<%	sv.freqTo03.appendClassString("string_fld");
	sv.freqTo03.appendClassString("input_txt");
	sv.freqTo03.appendClassString("highlight");
%>
	<%sv.cmrate03.setClassString("");%>
<%	sv.cmrate03.appendClassString("num_fld");
	sv.cmrate03.appendClassString("input_txt");
	sv.cmrate03.appendClassString("highlight");
%>
	<%sv.premaxpc03.setClassString("");%>
<%	sv.premaxpc03.appendClassString("num_fld");
	sv.premaxpc03.appendClassString("input_txt");
	sv.premaxpc03.appendClassString("highlight");
%>
	<%sv.commenpc03.setClassString("");%>
<%	sv.commenpc03.appendClassString("num_fld");
	sv.commenpc03.appendClassString("input_txt");
	sv.commenpc03.appendClassString("highlight");
%>
	<%sv.freqFrom04.setClassString("");%>
<%	sv.freqFrom04.appendClassString("string_fld");
	sv.freqFrom04.appendClassString("input_txt");
	sv.freqFrom04.appendClassString("highlight");
%>
	<%sv.freqTo04.setClassString("");%>
<%	sv.freqTo04.appendClassString("string_fld");
	sv.freqTo04.appendClassString("input_txt");
	sv.freqTo04.appendClassString("highlight");
%>
	<%sv.cmrate04.setClassString("");%>
<%	sv.cmrate04.appendClassString("num_fld");
	sv.cmrate04.appendClassString("input_txt");
	sv.cmrate04.appendClassString("highlight");
%>
	<%sv.premaxpc04.setClassString("");%>
<%	sv.premaxpc04.appendClassString("num_fld");
	sv.premaxpc04.appendClassString("input_txt");
	sv.premaxpc04.appendClassString("highlight");
%>
	<%sv.commenpc04.setClassString("");%>
<%	sv.commenpc04.appendClassString("num_fld");
	sv.commenpc04.appendClassString("input_txt");
	sv.commenpc04.appendClassString("highlight");
%>
	<%sv.freqFrom05.setClassString("");%>
<%	sv.freqFrom05.appendClassString("string_fld");
	sv.freqFrom05.appendClassString("input_txt");
	sv.freqFrom05.appendClassString("highlight");
%>
	<%sv.freqTo05.setClassString("");%>
<%	sv.freqTo05.appendClassString("string_fld");
	sv.freqTo05.appendClassString("input_txt");
	sv.freqTo05.appendClassString("highlight");
%>
	<%sv.cmrate05.setClassString("");%>
<%	sv.cmrate05.appendClassString("num_fld");
	sv.cmrate05.appendClassString("input_txt");
	sv.cmrate05.appendClassString("highlight");
%>
	<%sv.premaxpc05.setClassString("");%>
<%	sv.premaxpc05.appendClassString("num_fld");
	sv.premaxpc05.appendClassString("input_txt");
	sv.premaxpc05.appendClassString("highlight");
%>
	<%sv.commenpc05.setClassString("");%>
<%	sv.commenpc05.appendClassString("num_fld");
	sv.commenpc05.appendClassString("input_txt");
	sv.commenpc05.appendClassString("highlight");
%>
	<%sv.freqFrom06.setClassString("");%>
<%	sv.freqFrom06.appendClassString("string_fld");
	sv.freqFrom06.appendClassString("input_txt");
	sv.freqFrom06.appendClassString("highlight");
%>
	<%sv.freqTo06.setClassString("");%>
<%	sv.freqTo06.appendClassString("string_fld");
	sv.freqTo06.appendClassString("input_txt");
	sv.freqTo06.appendClassString("highlight");
%>
	<%sv.cmrate06.setClassString("");%>
<%	sv.cmrate06.appendClassString("num_fld");
	sv.cmrate06.appendClassString("input_txt");
	sv.cmrate06.appendClassString("highlight");
%>
	<%sv.premaxpc06.setClassString("");%>
<%	sv.premaxpc06.appendClassString("num_fld");
	sv.premaxpc06.appendClassString("input_txt");
	sv.premaxpc06.appendClassString("highlight");
%>
	<%sv.commenpc06.setClassString("");%>
<%	sv.commenpc06.appendClassString("num_fld");
	sv.commenpc06.appendClassString("input_txt");
	sv.commenpc06.appendClassString("highlight");
%>
	<%sv.freqFrom07.setClassString("");%>
<%	sv.freqFrom07.appendClassString("string_fld");
	sv.freqFrom07.appendClassString("input_txt");
	sv.freqFrom07.appendClassString("highlight");
%>
	<%sv.freqTo07.setClassString("");%>
<%	sv.freqTo07.appendClassString("string_fld");
	sv.freqTo07.appendClassString("input_txt");
	sv.freqTo07.appendClassString("highlight");
%>
	<%sv.cmrate07.setClassString("");%>
<%	sv.cmrate07.appendClassString("num_fld");
	sv.cmrate07.appendClassString("input_txt");
	sv.cmrate07.appendClassString("highlight");
%>
	<%sv.premaxpc07.setClassString("");%>
<%	sv.premaxpc07.appendClassString("num_fld");
	sv.premaxpc07.appendClassString("input_txt");
	sv.premaxpc07.appendClassString("highlight");
%>
	<%sv.commenpc07.setClassString("");%>
<%	sv.commenpc07.appendClassString("num_fld");
	sv.commenpc07.appendClassString("input_txt");
	sv.commenpc07.appendClassString("highlight");
%>
	<%sv.freqFrom08.setClassString("");%>
<%	sv.freqFrom08.appendClassString("string_fld");
	sv.freqFrom08.appendClassString("input_txt");
	sv.freqFrom08.appendClassString("highlight");
%>
	<%sv.freqTo08.setClassString("");%>
<%	sv.freqTo08.appendClassString("string_fld");
	sv.freqTo08.appendClassString("input_txt");
	sv.freqTo08.appendClassString("highlight");
%>
	<%sv.cmrate08.setClassString("");%>
<%	sv.cmrate08.appendClassString("num_fld");
	sv.cmrate08.appendClassString("input_txt");
	sv.cmrate08.appendClassString("highlight");
%>
	<%sv.premaxpc08.setClassString("");%>
<%	sv.premaxpc08.appendClassString("num_fld");
	sv.premaxpc08.appendClassString("input_txt");
	sv.premaxpc08.appendClassString("highlight");
%>
	<%sv.commenpc08.setClassString("");%>
<%	sv.commenpc08.appendClassString("num_fld");
	sv.commenpc08.appendClassString("input_txt");
	sv.commenpc08.appendClassString("highlight");
%>
	<%sv.freqFrom09.setClassString("");%>
<%	sv.freqFrom09.appendClassString("string_fld");
	sv.freqFrom09.appendClassString("input_txt");
	sv.freqFrom09.appendClassString("highlight");
%>
	<%sv.freqTo09.setClassString("");%>
<%	sv.freqTo09.appendClassString("string_fld");
	sv.freqTo09.appendClassString("input_txt");
	sv.freqTo09.appendClassString("highlight");
%>
	<%sv.cmrate09.setClassString("");%>
<%	sv.cmrate09.appendClassString("num_fld");
	sv.cmrate09.appendClassString("input_txt");
	sv.cmrate09.appendClassString("highlight");
%>
	<%sv.premaxpc09.setClassString("");%>
<%	sv.premaxpc09.appendClassString("num_fld");
	sv.premaxpc09.appendClassString("input_txt");
	sv.premaxpc09.appendClassString("highlight");
%>
	<%sv.commenpc09.setClassString("");%>
<%	sv.commenpc09.appendClassString("num_fld");
	sv.commenpc09.appendClassString("input_txt");
	sv.commenpc09.appendClassString("highlight");
%>
	<%sv.freqFrom10.setClassString("");%>
<%	sv.freqFrom10.appendClassString("string_fld");
	sv.freqFrom10.appendClassString("input_txt");
	sv.freqFrom10.appendClassString("highlight");
%>
	<%sv.freqTo10.setClassString("");%>
<%	sv.freqTo10.appendClassString("string_fld");
	sv.freqTo10.appendClassString("input_txt");
	sv.freqTo10.appendClassString("highlight");
%>
	<%sv.cmrate10.setClassString("");%>
<%	sv.cmrate10.appendClassString("num_fld");
	sv.cmrate10.appendClassString("input_txt");
	sv.cmrate10.appendClassString("highlight");
%>
	<%sv.premaxpc10.setClassString("");%>
<%	sv.premaxpc10.appendClassString("num_fld");
	sv.premaxpc10.appendClassString("input_txt");
	sv.premaxpc10.appendClassString("highlight");
%>
	<%sv.commenpc10.setClassString("");%>
<%	sv.commenpc10.appendClassString("num_fld");
	sv.commenpc10.appendClassString("input_txt");
	sv.commenpc10.appendClassString("highlight");
%>
	<%sv.freqFrom11.setClassString("");%>
<%	sv.freqFrom11.appendClassString("string_fld");
	sv.freqFrom11.appendClassString("input_txt");
	sv.freqFrom11.appendClassString("highlight");
%>
	<%sv.freqTo11.setClassString("");%>
<%	sv.freqTo11.appendClassString("string_fld");
	sv.freqTo11.appendClassString("input_txt");
	sv.freqTo11.appendClassString("highlight");
%>
	<%sv.cmrate11.setClassString("");%>
<%	sv.cmrate11.appendClassString("num_fld");
	sv.cmrate11.appendClassString("input_txt");
	sv.cmrate11.appendClassString("highlight");
%>
	<%sv.premaxpc11.setClassString("");%>
<%	sv.premaxpc11.appendClassString("num_fld");
	sv.premaxpc11.appendClassString("input_txt");
	sv.premaxpc11.appendClassString("highlight");
%>
	<%sv.commenpc11.setClassString("");%>
<%	sv.commenpc11.appendClassString("num_fld");
	sv.commenpc11.appendClassString("input_txt");
	sv.commenpc11.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>
	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group">
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div></div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller">
				
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						
					
						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="width:100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>

		<%-- 2nd row --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
					<table>
						<tr>
							<td>

								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
		
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
		
									} else {
		
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
		
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>

							<td><label><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;to &nbsp;&nbsp;")%></label></td>
						
							<td>

								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
		
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
		
									} else {
		
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
		
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%> 
								</div>
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
						</tr>
					</table>

					
				</div>
			</div>

		</div>

	</div>
</div>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Frequency"))%></label>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("% Commission"))%></label>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Max % Premium"))%></label>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Earned Commission %"))%></label>
					
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
				
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("From"))%></label>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
				
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To"))%></label>
				</div>
			</div>
		</div>
		
		<%-- 1 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom01)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo01)%>
				</div>
			</div>
		
		<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
				<div class="input-group" style="width:237px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate01)%>	
				</div></div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc01)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc01)%>			
				</div>
			</div>
		</div>
		<%-- 2 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom02)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo02)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate02)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc02)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc02)%>			
				</div>
			</div>
		</div>
		<%-- 3 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom03)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo03)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate03)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc03)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc03)%>			
				</div>
			</div>
		</div>
		<%-- 4 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom04)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo04)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate04)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc04)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc04)%>			
				</div>
			</div>
		</div>
		<%-- 5 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom05)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo05)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate05)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc05)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc05)%>			
				</div>
			</div>
		</div>
		<%-- 6 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom06)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo06)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate06)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc06)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc06)%>			
				</div>
			</div>
		</div>
		<%-- 7 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom07)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo07)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate07)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc07)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc07)%>			
				</div>
			</div>
		</div>
		<%-- 8 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom08)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo08)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate08)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc08)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc08)%>			
				</div>
			</div>
		</div>
		<%-- 9 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom09)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo09)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate09)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc09)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc09)%>			
				</div>
			</div>
		</div>
		<%-- 10 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom10)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo10)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate10)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc10)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc10)%>			
				</div>
			</div>
		</div>
		<%-- 11 row --%>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqFrom11)%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<%=smartHF.getHTMLVarExt(fw, sv.freqTo11)%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.cmrate11)%>	
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.premaxpc11)%>			
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="width:65px;">
				<%=smartHF.getHTMLVarExt(fw, sv.commenpc11)%>			
				</div>
			</div>
		</div>
	</div>
</div>

<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>


