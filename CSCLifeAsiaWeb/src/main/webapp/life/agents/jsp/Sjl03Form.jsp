<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SJL03";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%
	Sjl03ScreenVars sv = (Sjl03ScreenVars) fw.getVariables();
%>



<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client       ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent Number ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Agent Type   ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Qualification  ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description   ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "License/Certificate No.   ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Start Date ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "End Date   ");
%>
<%
	appVars.rollup(new int[]{93});
%>

</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					<table><tr><td>
						<%
							if (!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntsel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.clntsel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr></table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>

					<%
						if (!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.agnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
					<table><tr><td>
						<%
							if (!((sv.agntype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agntype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agtydesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.agtydesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 80px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>

		</div>
		<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[11];
int totalTblWidth = 0;
int calculatedValue =0;

			if(resourceBundleHandler.gettingValueFromBundle("agntqualification").length() >= (sv.agntqualification.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("agntqualification").length())*12;								
			} else {		
				calculatedValue = (sv.agntqualification.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
				tblColumnWidth[0]= calculatedValue;
			
			if(resourceBundleHandler.gettingValueFromBundle("Description").length() >= (sv.quadesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Description").length())*10;								
			} else {		
				calculatedValue = (sv.quadesc.getFormData()).length()*10;								
			}		
				totalTblWidth += calculatedValue;
				tblColumnWidth[1]= calculatedValue;
			
			if(resourceBundleHandler.gettingValueFromBundle("License/Certificate No.").length() >= (sv.liceno.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("License/Certificate No.").length())*12;								
			} else {		
				calculatedValue = (sv.liceno.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
				tblColumnWidth[2]= calculatedValue;
			
			if(resourceBundleHandler.gettingValueFromBundle("Start Date").length() >= (sv.startdte.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Start Date").length())*12;								
			} else {		
				calculatedValue = (sv.startdte.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
				tblColumnWidth[3]= calculatedValue;
			
			if(resourceBundleHandler.gettingValueFromBundle("End Date").length() >= (sv.enddte.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("End Date").length())*12;								
			} else {		
				calculatedValue = (sv.enddte.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
				tblColumnWidth[4]= calculatedValue;
			
			
			%>		
<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
					
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sjl03' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Qualification")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("License/Certificate No.")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Start Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Expiry Date")%></th>
								</tr>
							</thead>
							<tbody>
								<%
					     		GeneralTable sfl = fw.getTable("sjl03screensfl");
								Sjl03screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sjl03screensfl.hasMoreScreenRows(sfl)) {
									 {
										if (appVars.ind09.isOn()) {
											sv.startdteDisp.setReverse(BaseScreenData.REVERSED);
											sv.startdteDisp.setColor(BaseScreenData.RED);
										}
										if (appVars.ind10.isOn()) {
											sv.enddteDisp.setReverse(BaseScreenData.REVERSED);
											sv.enddteDisp.setColor(BaseScreenData.RED);
										}
										if (appVars.ind07.isOn()) {
											sv.liceno.setReverse(BaseScreenData.REVERSED);
											sv.liceno.setColor(BaseScreenData.RED);
										}
										if (appVars.ind11.isOn()) {
											sv.startdteDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind12.isOn()) {
											sv.enddteDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind13.isOn()) {
											sv.liceno.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind14.isOn()) {
											sv.agntqualification.setEnabled(BaseScreenData.DISABLED);
										}
									} 
							%>
								<tr id='<%="tablerow" + count%>'>
									<td>
										<%
										fieldItem = appVars.loadF4FieldsLong(new String[] { "agntqualification" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("agntqualification");
										optionValue = makeDropDownList(mappedItems, sv.agntqualification.getFormData(), 1, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.agntqualification.getFormData()).toString().trim());
										%>
										<%
							if ((new Byte((sv.agntqualification).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.agntqualification.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							}else{
						%>	
						<%=smartHF.getDropDownExt(sv.agntqualification, fw, longValue, "agntqualification", optionValue, 0, 150)%>
						<%
								}
							%>			
									</td>
									<td>
									<div>
						<input name='quadesc' type='text'
							<%formatValue = (sv.quadesc.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.quadesc.getLength()%>'
							maxLength='<%=sv.quadesc.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(quadesc)'
							onKeyUp='return checkMaxLength(this)' readonly="true"
							class="output_cell">
					</div>
									
									
									
										<%-- <%=smartHF.getHTMLVarExt(fw, sv.quadesc)%> --%>
									</td>
									
									<!-- <td> -->
			<%-- <%=smartHF.getHTMLVarExt(fw, sv.liceno)%> --%>
			
			<td
						style="color:#434343; padding-left: 5px;width:180px;font-weight: bold;border-right: 1px solid #dddddd;"
						align="left">
						<div class="form-group">
					
						
						<input style="height:18px;padding-top: 4px" type='text'
							maxLength='<%=sv.liceno.getLength()%>'
							value='<%=sv.liceno.getFormData()%>'
							size='<%=sv.liceno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(sjl03screensfl.liceno)'
							onKeyUp='return checkMaxLength(this)'
							name='<%="sjl03screensfl" + "." +"liceno" + "_R" + count%>'
							id='<%="sjl03screensfl" + "." + "liceno" + "_R" + count%>'
						<%if (((new Byte((sv.liceno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0)
								|| (sv.isScreenProtected()))  {%>
							readonly="true" class="output_cell"
						<%} else if((new Byte((sv.liceno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell"
						<%} else {%>
							class=' <%=(sv.liceno).getColor()== null  ?
								"input_cell" :  (sv.liceno).getColor().equals("red") ?
								"input_cell red reverse" : "input_cell"%>'
						<%}%>
						>
						</div>
					</td>
			
			
			
			
				
				<!-- </td> -->
									
									<td align="left">
										<% if ((new Byte((sv.startdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                       || fw.getVariables().isScreenProtected()) {       %>
						              <div style="width: 80px;">   <%=smartHF.getRichTextDateInput(fw, sv.startdteDisp)%> </div>
						                                       
						                             
						                <%}else{%>
						                           <div class="input-group date form_date col-md-12" data-date=""
						                                  data-date-format="dd/mm/yyyy" data-link-field="startdteDisp"
						                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
						                                         <%=smartHF.getRichTextDateInput(fw, sv.startdteDisp, (sv.startdteDisp.getLength()))%>
						                                         <span class="input-group-addon">
						                                         <span class="glyphicon glyphicon-calendar"></span>
						                                         </span>
						                           </div>
						                                  
						              <%}%>
									</td>
									<td align="left">
										<% if ((new Byte((sv.enddteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                       || fw.getVariables().isScreenProtected()) {       %>
						              <div style="width: 80px;">   <%=smartHF.getRichTextDateInput(fw, sv.enddteDisp)%></div>
						                                       
						                             
						                <%}else{%>
						                           <div class="input-group date form_date col-md-12" data-date=""
						                                  data-date-format="dd/mm/yyyy" data-link-field="enddteDisp"
						                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
						                                         <%=smartHF.getRichTextDateInput(fw, sv.enddteDisp, (sv.enddteDisp.getLength()))%>
						                                         <span class="input-group-addon">
						                                         <span class="glyphicon glyphicon-calendar"></span>
						                                         </span>
						                           </div>
						                                  
						              <%}%>
									</td>
									
								</tr>
								<%
									count = count + 1;
										Sjl03screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>
							</tbody>
						</table>
					
				</div>
			</div>
		</div>
	</div>
</div>				
<script>
	$(document).ready(function() {
		$('#dataTables-sjl03').DataTable({
			ordering : false,
			searching : false,
			scrollY : "380px",
			scrollCollapse : true,
			scrollX : true,
			paging:false,
			info:false
		});
	});
</script>
<style>
#startdteDisp,#enddteDisp{width:85px;}
</style>
<%@ include file="/POLACommon2NEW.jsp"%>			