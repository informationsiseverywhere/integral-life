

  <%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6657";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6657ScreenVars sv = (S6657ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pay agent from bank account -");%>

<%{
		if (appVars.ind10.isOn()) {
			sv.bankcode.setReverse(BaseScreenData.REVERSED);
			sv.bankcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.bankcode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
	<div class="panel-body">
		
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
						<div class="input-group three-controller">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
					<%-- 2nd field --%>
				
						<%
						if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.longdesc.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.longdesc.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
					%>


					</div>
					</div>
				</div>
				</div>
		<%-- 2nd row --%>
		<%-- lables row --%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pay agent from bank account -")%></label>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					
						<table>
							<tr>
								<td>

									<%
										if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										} else {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>


								<td><%=resourceBundleHandler.gettingValueFromBundle("&nbsp; to &nbsp;")%></td>

								<td>
									<%
										if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										} else {
			
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}
			
										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> ' style="min-width:100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
							</tr>
						</table>

					
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bankcode, (sv.bankcode.getLength()))%>
						<%=smartHF.getHTMLF4NSVar(fw, sv.bankcode)%>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
