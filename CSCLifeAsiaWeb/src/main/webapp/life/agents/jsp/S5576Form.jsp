<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5576";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5576ScreenVars sv = (S5576ScreenVars) fw.getVariables();%>


	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>

	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>

	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>

	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>

	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To Age");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% Per Annum");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Age");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum  %");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum  %");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flat %");%>

	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Month");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Day");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Higher ");%>

	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lower ");%>

	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual ");%>


<div class="panel panel-default">	
    <div class="panel-body">     
        <div class="row">	
			    	<div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				       <div class="col-md-1"> </div>
				       <div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				       			       <div class="col-md-1"> </div>
				       <div class="col-md-4"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    		<div class="input-group">
						    			<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%-- 2nd field --%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
		  </div>
		  <div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
					<div class="form-group">
							 <table>
					<tr>
					<td>
						<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
				<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
				<td>
							<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
						</td>
						</tr>
						</table>		
						
					</div>
				</div>
			</div>
		</div> 
	<br>
<div class="row">
    
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("ToAge"))%></label>
     </div>
     <div class="col-md-1"></div>
     <div class="col-md-2">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("%PerAnnum"))%></label>
     </div>
     <div class="col-md-2">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum Age"))%></label>
     </div>
     <div class="col-md-2">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum%"))%></label>
     </div>
     <div class="col-md-2">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum%"))%></label>
     </div>
     <div class="col-md-2">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;Flat%"))%></label>
     </div>
  </div>   

<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age01,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
   <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc01,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax01,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	  <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt01,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt01,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt01,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age02,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
<div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc02,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	<div class="col-md-1"></div> 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax02,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt02,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	<div class="col-md-1"></div> 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt02,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	<div class="col-md-1"></div> 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt02,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age03,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
     <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc03,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax03,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt03,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt03,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt03,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age04,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
    <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc04,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax04,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt04,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt04,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt04,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age05,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
  <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc05,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax05,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt05,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt05,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt05,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age06,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
     <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc06,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax06,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt06,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt06,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt06,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 
<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age07,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
    <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc07,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	  <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax07,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt07,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt07,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt07,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>
&nbsp;&nbsp; 

<div class="row">
  
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.age08,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
     <div class="col-md-1"></div>
      <div class="col-md-1">  
	 <%=smartHF.getHTMLVarExt(fw,sv.annumpc08,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.agemax08,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.minpcnt08,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.maxpcnt08,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.flatpcnt08,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
</div>

&nbsp;&nbsp; 
&nbsp;&nbsp;
<div class="row">
    
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
     </div>
     <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Year"))%></label>
     </div>
     <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Month"))%></label>
     </div>
     <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Day"))%></label>
     </div>
  </div>   
&nbsp;&nbsp; 
<div class="row">
    
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Higher"))%></label>
     </div>
     
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.highyr,1)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.highmth,1)%>
	 </div>
</div>	 
	&nbsp;&nbsp;  
<div class="row">
    
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Lower"))%></label>
     </div>
     
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.lowyr,1)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.lowmth,1)%>
	 </div>
</div>	 
&nbsp;&nbsp;  
<div class="row">
    
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Actual"))%></label>
     </div>
     
    <div class="col-md-1">
      
     </div>
     
     <div class="col-md-1">
      
     </div>
     
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.actualday,1)%>
	 </div>
	 
	
</div>	 

</div>
</div>




<%@ include file="/POLACommon2NEW.jsp"%>


