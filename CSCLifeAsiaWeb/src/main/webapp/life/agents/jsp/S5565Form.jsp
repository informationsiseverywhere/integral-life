<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5565";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>

<%S5565ScreenVars sv = (S5565ScreenVars) fw.getVariables();%>


	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>

	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>

	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>

	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>

	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid to ");%>

	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Term");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentages");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"   ---------------------------------------------------------------------------------");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>

	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"|");%>


	<%
{
		if (!appVars.ind03.isOn()) {
			sv.anbAtCcd01.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind04.isOn()) {
			sv.anbAtCcd02.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind05.isOn()) {
			sv.anbAtCcd03.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind06.isOn()) {
			sv.anbAtCcd04.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind07.isOn()) {
			sv.anbAtCcd05.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind08.isOn()) {
			sv.anbAtCcd06.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind09.isOn()) {
			sv.anbAtCcd07.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind10.isOn()) {
			sv.anbAtCcd08.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind11.isOn()) {
			sv.anbAtCcd09.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind12.isOn()) {
			sv.anbAtCcd10.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<div class="panel panel-default">	
    <div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<div class="input-group">
						    		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>	<div class="col-md-1"></div>
				       <div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<div class="input-group">
						    			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      			</div>
				    		</div>
				       </div>
				       <div class="col-md-1"></div>
				       	<div class="col-md-4"> 
    	 					 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                             
                             <div class="input-group">
                              <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important;min-width: 110px;text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
    
                  </div></div> 
		       </div>
		       
	 <div class="row">	
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
				   <table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td><%=resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;to &nbsp;&nbsp;")%></td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
		       </div>
	
	<div class="row">
	<center>Term &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Percentages<br>------------------------------------------------------------------------------------------------------------------------------------------------</center>
	</div>
	 &nbsp;&nbsp; 
   <div class="row">
    <div class="col-md-1">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Age"))%></label>
     </div>
      
      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.termMax01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
		<%=smartHF.getHTMLVarExt(fw, sv.termMax02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
		<%=smartHF.getHTMLVarExt(fw, sv.termMax03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
     <%=smartHF.getHTMLVarExt(fw, sv.termMax04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.termMax05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.termMax06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt( fw, sv.termMax07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt( fw, sv.termMax08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.termMax09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>
</div>	 
	 &nbsp;&nbsp; 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.anbAtCcd01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
		<%=smartHF.getHTMLVarExt(fw, sv.prcnt01, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt02, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt03, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt04, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt05, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt06, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt07, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt08, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt09, COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
  <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.anbAtCcd02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt10, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt11, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt12, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt13, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt14, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt15, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt16, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt17, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt18, COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;

 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.anbAtCcd03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt19, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt20, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt21, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
		<%=smartHF.getHTMLVarExt(fw, sv.prcnt22, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt23, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt24, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt25, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt26, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt27, COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
<%=smartHF.getHTMLVarExt(fw, sv.anbAtCcd04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt28, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt29, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt30, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt31, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
		<%=smartHF.getHTMLVarExt(fw, sv.prcnt32, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
<%=smartHF.getHTMLVarExt(fw, sv.prcnt33, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt34, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt35, COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw, sv.prcnt36, COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd05,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt37,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt38,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt39,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt40,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt41,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt42,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt43,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt44,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt45,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd06,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt46,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt47,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt48,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt49,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt50,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt51,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt52,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt53,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt54,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd07,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt55,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt56,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt57,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt58,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt59,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt60,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt61,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt62,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt63,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd08,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt64,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt65,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt66,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt67,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt68,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt69,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt70,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt71,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt72,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd09,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt73,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt74,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt75,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt76,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt77,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt78,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt79,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt80,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt81,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 
 <div class="row">
     <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.anbAtCcd10,COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	 </div>

      <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt82,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt83,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt84,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt85,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt86,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt87,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt88,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt89,COBOLHTMLFormatter.S3VS2)%>
	 </div>
	 
	 <div class="col-md-1">  
	<%=smartHF.getHTMLVarExt(fw,sv.prcnt90,COBOLHTMLFormatter.S3VS2)%>
	 </div>
</div>
 &nbsp;&nbsp;
 </div>
 </div>
 
<%@ include file="/POLACommon2NEW.jsp"%>

