
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52R";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr52rScreenVars sv = (Sr52rScreenVars) fw.getVariables();%>

<%if (sv.Sr52rscreenWritten.gt(0)) {%>
	<%Sr52rscreen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age Limit  ");%>
	<%sv.agelimit.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Up to Term");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"AP/SP Factor");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min factor");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max Factor");%>
	<%sv.toterm01.setClassString("");%>
	<%sv.factorsa01.setClassString("");%>
	<%sv.factsamn01.setClassString("");%>
	<%sv.factsamx01.setClassString("");%>
	<%sv.toterm02.setClassString("");%>
	<%sv.factorsa02.setClassString("");%>
	<%sv.factsamn02.setClassString("");%>
	<%sv.factsamx02.setClassString("");%>
	<%sv.toterm03.setClassString("");%>
	<%sv.factorsa03.setClassString("");%>
	<%sv.factsamn03.setClassString("");%>
	<%sv.factsamx03.setClassString("");%>
	<%sv.toterm04.setClassString("");%>
	<%sv.factorsa04.setClassString("");%>
	<%sv.factsamn04.setClassString("");%>
	<%sv.factsamx04.setClassString("");%>
	<%sv.toterm05.setClassString("");%>
	<%sv.factorsa05.setClassString("");%>
	<%sv.factsamn05.setClassString("");%>
	<%sv.factsamx05.setClassString("");%>
	<%sv.toterm06.setClassString("");%>
	<%sv.factorsa06.setClassString("");%>
	<%sv.factsamn06.setClassString("");%>
	<%sv.factsamx06.setClassString("");%>
	<%sv.toterm07.setClassString("");%>
	<%sv.factorsa07.setClassString("");%>
	<%sv.factsamn07.setClassString("");%>
	<%sv.factsamx07.setClassString("");%>
	<%sv.toterm08.setClassString("");%>
	<%sv.factorsa08.setClassString("");%>
	<%sv.factsamn08.setClassString("");%>
	<%sv.factsamx08.setClassString("");%>
	<%sv.toterm09.setClassString("");%>
	<%sv.factorsa09.setClassString("");%>
	<%sv.factsamn09.setClassString("");%>
	<%sv.factsamx09.setClassString("");%>
	<%sv.toterm10.setClassString("");%>
	<%sv.factorsa10.setClassString("");%>
	<%sv.factsamn10.setClassString("");%>
	<%sv.factsamx10.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Casual Premium SA qualifying factor ");%>
	<%sv.dsifact.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.agelimit.setReverse(BaseScreenData.REVERSED);
			sv.agelimit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.agelimit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.toterm01.setReverse(BaseScreenData.REVERSED);
			sv.toterm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.toterm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.factorsa01.setReverse(BaseScreenData.REVERSED);
			sv.factorsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.factorsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.factsamn01.setReverse(BaseScreenData.REVERSED);
			sv.factsamn01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.factsamn01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.factsamx01.setReverse(BaseScreenData.REVERSED);
			sv.factsamx01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.factsamx01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.toterm02.setReverse(BaseScreenData.REVERSED);
			sv.toterm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.toterm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.factorsa02.setReverse(BaseScreenData.REVERSED);
			sv.factorsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.factorsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.factsamn02.setReverse(BaseScreenData.REVERSED);
			sv.factsamn02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.factsamn02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.factsamx02.setReverse(BaseScreenData.REVERSED);
			sv.factsamx02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.factsamx02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.toterm03.setReverse(BaseScreenData.REVERSED);
			sv.toterm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.toterm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.factorsa03.setReverse(BaseScreenData.REVERSED);
			sv.factorsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.factorsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.factsamn03.setReverse(BaseScreenData.REVERSED);
			sv.factsamn03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.factsamn03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.factsamx03.setReverse(BaseScreenData.REVERSED);
			sv.factsamx03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.factsamx03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.toterm04.setReverse(BaseScreenData.REVERSED);
			sv.toterm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.toterm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.factorsa04.setReverse(BaseScreenData.REVERSED);
			sv.factorsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.factorsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.factsamn04.setReverse(BaseScreenData.REVERSED);
			sv.factsamn04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.factsamn04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.factsamx04.setReverse(BaseScreenData.REVERSED);
			sv.factsamx04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.factsamx04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.toterm05.setReverse(BaseScreenData.REVERSED);
			sv.toterm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.toterm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.factorsa05.setReverse(BaseScreenData.REVERSED);
			sv.factorsa05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.factorsa05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.factsamn05.setReverse(BaseScreenData.REVERSED);
			sv.factsamn05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.factsamn05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.factsamx05.setReverse(BaseScreenData.REVERSED);
			sv.factsamx05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.factsamx05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.toterm06.setReverse(BaseScreenData.REVERSED);
			sv.toterm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.toterm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.factorsa06.setReverse(BaseScreenData.REVERSED);
			sv.factorsa06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.factorsa06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.factsamn06.setReverse(BaseScreenData.REVERSED);
			sv.factsamn06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.factsamn06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.factsamx06.setReverse(BaseScreenData.REVERSED);
			sv.factsamx06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind74.isOn()) {
			sv.factsamx06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.toterm07.setReverse(BaseScreenData.REVERSED);
			sv.toterm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.toterm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.factorsa07.setReverse(BaseScreenData.REVERSED);
			sv.factorsa07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind76.isOn()) {
			sv.factorsa07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.factsamn07.setReverse(BaseScreenData.REVERSED);
			sv.factsamn07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.factsamn07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.factsamx07.setReverse(BaseScreenData.REVERSED);
			sv.factsamx07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.factsamx07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.toterm08.setReverse(BaseScreenData.REVERSED);
			sv.toterm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.toterm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.factorsa08.setReverse(BaseScreenData.REVERSED);
			sv.factorsa08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.factorsa08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.factsamn08.setReverse(BaseScreenData.REVERSED);
			sv.factsamn08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.factsamn08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.factsamx08.setReverse(BaseScreenData.REVERSED);
			sv.factsamx08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.factsamx08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.toterm09.setReverse(BaseScreenData.REVERSED);
			sv.toterm09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind83.isOn()) {
			sv.toterm09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.factorsa09.setReverse(BaseScreenData.REVERSED);
			sv.factorsa09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.factorsa09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.factsamn09.setReverse(BaseScreenData.REVERSED);
			sv.factsamn09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.factsamn09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.factsamx09.setReverse(BaseScreenData.REVERSED);
			sv.factsamx09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.factsamx09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.toterm10.setReverse(BaseScreenData.REVERSED);
			sv.toterm10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.toterm10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.factorsa10.setReverse(BaseScreenData.REVERSED);
			sv.factorsa10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind88.isOn()) {
			sv.factorsa10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.factsamn10.setReverse(BaseScreenData.REVERSED);
			sv.factsamn10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind89.isOn()) {
			sv.factsamn10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.factsamx10.setReverse(BaseScreenData.REVERSED);
			sv.factsamx10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind90.isOn()) {
			sv.factsamx10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.dsifact.setReverse(BaseScreenData.REVERSED);
			sv.dsifact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind91.isOn()) {
			sv.dsifact.setHighLight(BaseScreenData.BOLD);
		}
	}%>





<div class="panel panel-default">
    	  
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	
				      		</div>
				    </div>
			   
			   
			   	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					    		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				      		</div>
				    	</div>
			    	
			    	
			    		
			    	  <div class="col-md-4" > 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					    		<div class="input-group" style="max-width:500px;">
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                         </div>  					    		
				      </div>
				    </div>
			       </div>
			       
			        <div class="row">	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label> 
				    		<table>
			    	   <tr>
			    	       
			    	     <td>
				    	<!-- 	<div class="input-group three-controller"> -->
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		 </td>


                      <td>  &nbsp; </td>
                        <td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
                        <td>  &nbsp; </td>


                      
                        <td> 


<%-- <span class="input-group-addon"> <label><%=resourceBundleHandler.gettingValueFromBundle("to")%></label></span> --%>
 

  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 

                                        "blank_cell" : "output_cell" %>'style="min-width:70px;">

                                        <%=XSSFilter.escapeHtml(formatValue)%>

                                     </div>
				
		<%
		longValue = null;
		formatValue = null;
		%>
   </td>
                      </tr>
                   </table>		
	

				      		 </div></div>
				      </div>
			        
			        <div class="row">
			        <div class="col-md-4" style="max-width:500px;"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Age Limit  ")%></label>
					    		<div class="input-group">
<%if(((BaseScreenData)sv.agelimit) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.agelimit,( sv.agelimit.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.agelimit) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.agelimit, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div>
				      		</div>
				    	</div></div>
				    	
	<br>			    	
<div class="row">
<div class="col-md-1">
</div>
<div class="col-md-2">
<label><%= generatedText8%></label>
<div class="list-group">
<div style="width:65px;"><%if(((BaseScreenData)sv.toterm01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.toterm01,( sv.toterm01.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.toterm01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.toterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm02)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm03)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm04)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm05)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm06)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm07)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm08)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm09)%>
<%=smartHF.getHTMLVarExt(fw, sv.toterm10)%></div>
</div>
</div>
<div class="col-md-2">
<label><%=generatedText9%></label>
<div class="list-group">
	<div style="width:65px;"><%=smartHF.getHTMLVarExt(fw, sv.factorsa01)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa02)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa03)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa04)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa05)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa06)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa07)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa08)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa09)%>
	<%=smartHF.getHTMLVarExt(fw, sv.factorsa10)%></div>

</div>
</div>
<div class="col-md-2">
<label><%= generatedText10%></label>
<div class="list-group">
<div style="width:65px;"><%if(((BaseScreenData)sv.factsamn01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.factsamn01,( sv.factsamn01.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.factsamn01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.factsamn01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn02)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn03)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn04)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn05)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn06)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn07)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn08)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn09)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamn10)%></div>
</div>
</div>
<div class="col-md-2">
<label><%= generatedText11%></label>
<div class="list-group">
<div style="width:65px;"><%=smartHF.getHTMLVarExt(fw, sv.factsamx01)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx02)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx03)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx04)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx05)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx06)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx07)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx08)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx09)%>
<%=smartHF.getHTMLVarExt(fw, sv.factsamx10)%></div>

</div>
</div>
</div>

<%}%>

<div class="row">
			        <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Casual Premium SA qualifying factor")%></label>
					    		<div class="input-group">
<%if(((BaseScreenData)sv.dsifact) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.dsifact,( sv.dsifact.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.dsifact) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.dsifact, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>
        </div>
				      		</div>
				    	</div></div>
				    	
				   
				   
				   
				    	
				    
			   </div>
		  </div>








<%-- --------------------------------------

<%if (sv.Sr52rscreenWritten.gt(0)) {%>
	<%Sr52rscreen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age Limit  ");%>
	<%sv.agelimit.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Up to Term");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"AP/SP Factor");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min factor");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max Factor");%>
	<%sv.toterm01.setClassString("");%>
	<%sv.factorsa01.setClassString("");%>
	<%sv.factsamn01.setClassString("");%>
	<%sv.factsamx01.setClassString("");%>
	<%sv.toterm02.setClassString("");%>
	<%sv.factorsa02.setClassString("");%>
	<%sv.factsamn02.setClassString("");%>
	<%sv.factsamx02.setClassString("");%>
	<%sv.toterm03.setClassString("");%>
	<%sv.factorsa03.setClassString("");%>
	<%sv.factsamn03.setClassString("");%>
	<%sv.factsamx03.setClassString("");%>
	<%sv.toterm04.setClassString("");%>
	<%sv.factorsa04.setClassString("");%>
	<%sv.factsamn04.setClassString("");%>
	<%sv.factsamx04.setClassString("");%>
	<%sv.toterm05.setClassString("");%>
	<%sv.factorsa05.setClassString("");%>
	<%sv.factsamn05.setClassString("");%>
	<%sv.factsamx05.setClassString("");%>
	<%sv.toterm06.setClassString("");%>
	<%sv.factorsa06.setClassString("");%>
	<%sv.factsamn06.setClassString("");%>
	<%sv.factsamx06.setClassString("");%>
	<%sv.toterm07.setClassString("");%>
	<%sv.factorsa07.setClassString("");%>
	<%sv.factsamn07.setClassString("");%>
	<%sv.factsamx07.setClassString("");%>
	<%sv.toterm08.setClassString("");%>
	<%sv.factorsa08.setClassString("");%>
	<%sv.factsamn08.setClassString("");%>
	<%sv.factsamx08.setClassString("");%>
	<%sv.toterm09.setClassString("");%>
	<%sv.factorsa09.setClassString("");%>
	<%sv.factsamn09.setClassString("");%>
	<%sv.factsamx09.setClassString("");%>
	<%sv.toterm10.setClassString("");%>
	<%sv.factorsa10.setClassString("");%>
	<%sv.factsamn10.setClassString("");%>
	<%sv.factsamx10.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Casual Premium SA qualifying factor ");%>
	<%sv.dsifact.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.agelimit.setReverse(BaseScreenData.REVERSED);
			sv.agelimit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.agelimit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.toterm01.setReverse(BaseScreenData.REVERSED);
			sv.toterm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.toterm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.factorsa01.setReverse(BaseScreenData.REVERSED);
			sv.factorsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.factorsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.factsamn01.setReverse(BaseScreenData.REVERSED);
			sv.factsamn01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.factsamn01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.factsamx01.setReverse(BaseScreenData.REVERSED);
			sv.factsamx01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.factsamx01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.toterm02.setReverse(BaseScreenData.REVERSED);
			sv.toterm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.toterm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.factorsa02.setReverse(BaseScreenData.REVERSED);
			sv.factorsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.factorsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.factsamn02.setReverse(BaseScreenData.REVERSED);
			sv.factsamn02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.factsamn02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.factsamx02.setReverse(BaseScreenData.REVERSED);
			sv.factsamx02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.factsamx02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.toterm03.setReverse(BaseScreenData.REVERSED);
			sv.toterm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.toterm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.factorsa03.setReverse(BaseScreenData.REVERSED);
			sv.factorsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.factorsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.factsamn03.setReverse(BaseScreenData.REVERSED);
			sv.factsamn03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.factsamn03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.factsamx03.setReverse(BaseScreenData.REVERSED);
			sv.factsamx03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.factsamx03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.toterm04.setReverse(BaseScreenData.REVERSED);
			sv.toterm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.toterm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.factorsa04.setReverse(BaseScreenData.REVERSED);
			sv.factorsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.factorsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.factsamn04.setReverse(BaseScreenData.REVERSED);
			sv.factsamn04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.factsamn04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.factsamx04.setReverse(BaseScreenData.REVERSED);
			sv.factsamx04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.factsamx04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.toterm05.setReverse(BaseScreenData.REVERSED);
			sv.toterm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.toterm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.factorsa05.setReverse(BaseScreenData.REVERSED);
			sv.factorsa05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.factorsa05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.factsamn05.setReverse(BaseScreenData.REVERSED);
			sv.factsamn05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.factsamn05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.factsamx05.setReverse(BaseScreenData.REVERSED);
			sv.factsamx05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.factsamx05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.toterm06.setReverse(BaseScreenData.REVERSED);
			sv.toterm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.toterm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.factorsa06.setReverse(BaseScreenData.REVERSED);
			sv.factorsa06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.factorsa06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.factsamn06.setReverse(BaseScreenData.REVERSED);
			sv.factsamn06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.factsamn06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.factsamx06.setReverse(BaseScreenData.REVERSED);
			sv.factsamx06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind74.isOn()) {
			sv.factsamx06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.toterm07.setReverse(BaseScreenData.REVERSED);
			sv.toterm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.toterm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.factorsa07.setReverse(BaseScreenData.REVERSED);
			sv.factorsa07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind76.isOn()) {
			sv.factorsa07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.factsamn07.setReverse(BaseScreenData.REVERSED);
			sv.factsamn07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.factsamn07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.factsamx07.setReverse(BaseScreenData.REVERSED);
			sv.factsamx07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.factsamx07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.toterm08.setReverse(BaseScreenData.REVERSED);
			sv.toterm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.toterm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.factorsa08.setReverse(BaseScreenData.REVERSED);
			sv.factorsa08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.factorsa08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.factsamn08.setReverse(BaseScreenData.REVERSED);
			sv.factsamn08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.factsamn08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.factsamx08.setReverse(BaseScreenData.REVERSED);
			sv.factsamx08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.factsamx08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.toterm09.setReverse(BaseScreenData.REVERSED);
			sv.toterm09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind83.isOn()) {
			sv.toterm09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.factorsa09.setReverse(BaseScreenData.REVERSED);
			sv.factorsa09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.factorsa09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.factsamn09.setReverse(BaseScreenData.REVERSED);
			sv.factsamn09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.factsamn09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.factsamx09.setReverse(BaseScreenData.REVERSED);
			sv.factsamx09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.factsamx09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.toterm10.setReverse(BaseScreenData.REVERSED);
			sv.toterm10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.toterm10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.factorsa10.setReverse(BaseScreenData.REVERSED);
			sv.factorsa10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind88.isOn()) {
			sv.factorsa10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.factsamn10.setReverse(BaseScreenData.REVERSED);
			sv.factsamn10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind89.isOn()) {
			sv.factsamn10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.factsamx10.setReverse(BaseScreenData.REVERSED);
			sv.factsamx10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind90.isOn()) {
			sv.factsamx10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.dsifact.setReverse(BaseScreenData.REVERSED);
			sv.dsifact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind91.isOn()) {
			sv.dsifact.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(2, 2, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 2, fw, sv.company,12)%>
	<%=smartHF.getHTMLF4NSVar(3, 12, fw, sv.company)%>

	<%=smartHF.getLit(2, 33, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(3, 33, fw, sv.tabl)%>
	<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.tabl)%>

	<%=smartHF.getLit(2, 65, generatedText4)%>

	<%=smartHF.getHTMLVar(3, 65, fw, sv.item)%>

	<%=smartHF.getHTMLVar(3, 74, fw, sv.longdesc,120)%>

	<%=smartHF.getLit(4.2, 2, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(5, 2, fw, sv.itmfrmDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 24, fw, sv.itmfrmDisp)%>

	<%=smartHF.getLit(5, 13, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(5, 15, fw, sv.itmtoDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 40, fw, sv.itmtoDisp)%>
	
	<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 Start-->	

	<%=smartHF.getLit(7.2, 2, generatedText7)%>

	<%=smartHF.getHTMLVar(7.9, 2, fw, sv.agelimit, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(10, 8, generatedText8)%>

	<%=smartHF.getLit(10, 22, generatedText9)%>

	<%=smartHF.getLit(10, 40, generatedText10)%>

	<%=smartHF.getLit(10, 58, generatedText11)%>

	<%=smartHF.getHTMLVar(11, 9, fw, sv.toterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 22, fw, sv.factorsa01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 40, fw, sv.factsamn01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 58, fw, sv.factsamx01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 9, fw, sv.toterm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 22, fw, sv.factorsa02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 40, fw, sv.factsamn02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 58, fw, sv.factsamx02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 9, fw, sv.toterm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 22, fw, sv.factorsa03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 40, fw, sv.factsamn03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 58, fw, sv.factsamx03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 9, fw, sv.toterm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 22, fw, sv.factorsa04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 40, fw, sv.factsamn04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 58, fw, sv.factsamx04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 9, fw, sv.toterm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 22, fw, sv.factorsa05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 40, fw, sv.factsamn05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 58, fw, sv.factsamx05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 9, fw, sv.toterm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 22, fw, sv.factorsa06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 40, fw, sv.factsamn06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 58, fw, sv.factsamx06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 9, fw, sv.toterm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 22, fw, sv.factorsa07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 40, fw, sv.factsamn07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 58, fw, sv.factsamx07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 9, fw, sv.toterm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 22, fw, sv.factorsa08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 40, fw, sv.factsamn08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 58, fw, sv.factsamx08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 9, fw, sv.toterm09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 22, fw, sv.factorsa09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 40, fw, sv.factsamn09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 58, fw, sv.factsamx09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 9, fw, sv.toterm10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 22, fw, sv.factorsa10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 40, fw, sv.factsamn10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 58, fw, sv.factsamx10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(22, 2, generatedText12)%>

	<%=smartHF.getHTMLVar(23, 2, fw, sv.dsifact, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>


<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 ends-->	

<%}%>

<%if (sv.Sr52rprotectWritten.gt(0)) {%>
	<%Sr52rprotect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>



<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 Start-->	

<style>
div[id*='itmtoDisp']{padding-right:2px !important}

</style>

 --%>
<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 ends-->	

<%@ include file="/POLACommon2NEW.jsp"%>