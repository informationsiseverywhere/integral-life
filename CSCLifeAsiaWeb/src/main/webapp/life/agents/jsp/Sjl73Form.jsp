<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SJL73";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*"%>
<%Sjl73ScreenVars sv = (Sjl73ScreenVars) fw.getVariables();%>


<%{
	 	
		if (appVars.ind09.isOn()) {
			sv.reasonreg.setReverse(BaseScreenData.REVERSED);
			sv.reasonreg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.reasonreg.setHighLight(BaseScreenData.BOLD);
		}

		if (appVars.ind20.isOn()) {
			sv.reasonreg.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind21.isOn()) {
			sv.reasonreg.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.agncysel.setReverse(BaseScreenData.REVERSED);
			sv.agncysel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.agncysel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.agncysel.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind29.isOn()) {
			sv.agncysel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 
		if (appVars.ind18.isOn()) {
			sv.regdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.regdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.regdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.regdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.regdateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind16.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.resndesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.resndesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
			
		 
	}%>

	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			 		 <div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.clntsel.getFormData();  
				%>
			  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 103px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
			
				<div class="col-md-2" >
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        				<div class="input-group" style="max-width:120px;min-width:70px;">
        				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		dropdownItemsUIG=new String[][]{{"company"},{},{}};
		fieldItem=appVars.getLongDesc(dropdownItemsUIG,"E","0",baseModel,sv);
		mappedItems = (Map) fieldItem.get("company");
		longValue = (String) mappedItems.get((sv.company.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        		</div>
        		</div></div>
        		
			
						<div class="col-md-4" style="padding-left:50px">
				<div >
				    <label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
				 <table><tr><td class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.agntbr.getFormData();  
				%>
		 
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.agbrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.agbrdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
                 </div>
			</div>
			
	       
	 		 <div class="col-md-3" style="padding-left:0px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Area Code")%></label>
    <table><tr><td  class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.aracde.getFormData();  
				%>

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:24px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
			
				 </div> 		</td>
			 
		
			  <td >		  		
					<%					
					if(!((sv.aradesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.aradesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 142px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
		

		</div>
		<div class="row" style="min-height:3px"></div>
				<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="min-width:198px"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sales Representative Number"))%></label>
					<%
						if (!((sv.levelno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.levelno.getFormData()).toString());
							} else {
								formatValue = formatValue( longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style=width:80px;'>
						<%=formatValue%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			
				       
	 		 <div class="col-md-2">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Level Type")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.leveltyp.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.leveldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.leveldesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 150px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>
	
	       
							       
	 		 <div class="col-md-5" style="padding-left:50px">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sales Department")%></label>
   			 <table><tr><td style="min-width:10px" class="form-group">
			<div class="input-group">  
				<%	
					longValue = sv.saledept.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
			
				 </div> 		</td>
			 
			  
			  <td>		  		
					<%					
					if(!((sv.saledptdes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.saledptdes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 210px;">
							<%=formatValue%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</td></tr></table>
			</div>
		</div>

			</div>
			<div class="row" style="min-height:3px"></div>
			<div class="row">
			
				<div class="col-md-3">
				<div >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Agency Number")%></label>
				
				<table><tr><td class="form-group">
				<div class="input-group">
				<%
					longValue = sv.agncysel.getFormData();  
				%>
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:80px">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='agncysel'  id='agncysel'
				type='text' 
				value='<%=sv.agncysel.getFormData()%>' 
				maxLength='<%=sv.agncysel.getLength()%>' 
				size='<%=sv.agncysel.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(agncysel)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.agncysel).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 style="min-width:75px">
				
				<%
					}else if((new Byte((sv.agncysel).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" style="min-width:75px">
				 
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.agncysel).getColor()== null  ? 
				"input_cell" :  (sv.agncysel).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>'  style="min-width:75px">
				
				<span class="input-group-btn">
                     <button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('agncysel')); doAction('PFKEY04')">
                          <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                     </button>
                 </span>
				
				<%}longValue = null;} %>
		</div>	</td>	 
			
			 <td style="padding-left:1px">
				
				  		
						<%					
						if(!((sv.agncydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agncydesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.agncydesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:80px;max-width:100px;" >
								<%=formatValue%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td></tr></table>
						</div>
				  </div>
				  
				  	<div class="col-md-2" >
			<div class="form-group">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Registration Number"))%></label>
					<div class="input-group">  
				<%	
					longValue = sv.regnum.getFormData();  
				%>
				

				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px;max-width:150px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div>
				
				<%
				longValue = null;
				%>
				 </div> 
			</div>
		</div>
		
			<div class="col-md-4" style="padding-left:50px">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Start Date"))%></label>
								<div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.srdateDisp)%></div>
     
 
				</div>
			</div>
					
			<div class="col-md-3" style="padding-left:0px">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("End Date"))%></label>
					   <div style="width:80px">   <%=smartHF.getRichTextDateInput(fw, sv.endateDisp)%></div>
 
				</div>
			</div>
			
			</div>
			
			
			<div class="row" style="min-height:3px"></div>
						<div class="row">
							 <div class="col-md-3" >
						<div class="form-group">
						<% if (sv.tranflag.compareTo("N") != 0) { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Registration Date"))%></label>
        				<% }else{ %>
        				<label style='min-width:159px'><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Date Terminated"))%></label>
					<%}%>
			<%
								if((new Byte((sv.regdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
			<%=smartHF.getRichTextDateInput(fw, sv.regdateDisp,(sv.regdateDisp.getLength()))%>
			<%}else{%>
					<div class="input-group date form_date col-md-10" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="regdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.regdateDisp, (sv.regdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> <%} %>
			
				</div>
			</div>
			<div class="col-md-2 " >
        			<div class="form-group">
        			 	<% if (sv.tranflag.compareTo("N") != 0) { %>
        				<label style='min-width:159px'><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Registration"))%></label>
        				<% }else{ %>
        				<label style='min-width:159px'><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reason for Termination"))%></label>
        				<%}%>
        				<div class="input-group" style="min-width:178px">
        					<% if ((new Byte((sv.reasonreg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasonreg"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasonreg");
								optionValue = makeDropDownList( mappedItems , sv.reasonreg.getFormData(),2,resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.reasonreg.getFormData()).toString().trim());
								if (null == longValue){ longValue = ""; }
							%>
								<%=smartHF.getDropDownExt(sv.reasonreg, fw, longValue, "reasonreg", optionValue) %>
							<%}%>
							</div>
        			</div>
        		</div>	
						<div class="col-md-5" style="padding-left:50px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Reason Details")%></label>
				<div class="form-group"  ">
					<div class="input-group" style="min-width: 450px; max-width:600px">
					<input name='resndesc' type='text'
						<%formatValue = (sv.resndesc.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.resndesc.getLength()%>'
						maxLength='<%=sv.resndesc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(resndesc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.resndesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.resndesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.resndesc).getColor() == null
						? "input_cell"
						: (sv.resndesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div></div>
			</div>
			</div>
			
			
			
	
			</div>
	</div>




<%@ include file="/POLACommon2NEW.jsp"%>