   <%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR52U";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%Sr52uScreenVars sv = (Sr52uScreenVars) fw.getVariables();%>
<%{
}%>



<div class="panel panel-default">
	<div class="panel-body">
	        <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 80px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4"> 
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		       		</div>
		       	</div>
		       	</div>
		     </div>
		     
		     <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prm Status")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 80px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4"> 
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.register.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.register.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		     </div>
		     <div class="row">
		     <div class="col-md-4">
		       	<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' >
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					<td>
					<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 300px;min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
					<%}%>
		       	</td>
		       	</tr>
		       	</table>
		       	</div>  
		     </div>
		  </div>
		  
		   <div class="row">
		     <div class="col-md-4">
		       	<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Current Plan")%></label>
		       		<table><tr>
		       		<td>
		       		<%if ((new Byte((sv.chdrtype01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.chdrtype01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.chdrtype01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrtype01.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						
					</td>
					<td>	
						<%if ((new Byte((sv.descrip01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.descrip01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.descrip01.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.descrip01.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 300px;min-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
						<%}%>
		       	</td></tr></table>
		       </div>
		     </div>
		  </div>
		  
		   <div class="row">
		     <div class="col-md-4">
		       	<div class="form-group">
		       	<label><%=resourceBundleHandler.gettingValueFromBundle("New Plan")%></label>
		       	<div class="input-group" style="min-width:120px;">
		       	<% 
					fieldItem=appVars.loadF4FieldsLong(new String[] {"chdrtype02"},sv,"E",baseModel);
					mappedItems = (Map) fieldItem.get("chdrtype02");
					optionValue = makeDropDownList( mappedItems , sv.chdrtype02.getFormData(),2);  
					longValue = (String) mappedItems.get((sv.chdrtype02.getFormData()).toString().trim());
					/* int colWidth = 0; 
					int dropDownDescWidth=0;
					dropDownDescWidth = ((sv.chdrtype02.getFormData()).length()*12)+88;	 */
				%>
				
				<!--  Ilife- Life Cross Browser - Sprint 4 D6 : Task 1 Start-->	
				
				<%=smartHF.getDropDownExt(sv.chdrtype02, fw, longValue, "chdrtype02", optionValue,0,270)%>
						       	
		       	</div></div>
		     </div>
		   </div>
		</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

