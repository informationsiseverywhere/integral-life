
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5036";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.agents.screens.*" %>
<%S5036ScreenVars sv = (S5036ScreenVars) fw.getVariables();%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent Type ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Appointed ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission and Payment Details");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type of Termination");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Enter R or Q)");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instant Credit%");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed Percent ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deduct tax ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax code ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax allowed ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"I.R.D. No ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Superannuation code ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Superannuation % ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Consolidate Payment to A/c.Payee ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Collateral Details");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Collateral %");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Outstanding B/L ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max Limit ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Financial Details");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Housing Loan ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Car Loan ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Computer Loan ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Office Rent ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other ");%>
	

<%{
		if (appVars.ind05.isOn()) {
			sv.bmaflg.setReverse(BaseScreenData.REVERSED);
			sv.bmaflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.bmaflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.intcrd.setReverse(BaseScreenData.REVERSED);
			sv.intcrd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.intcrd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.fixprc.setReverse(BaseScreenData.REVERSED);
			sv.fixprc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.fixprc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.taxmeth.setReverse(BaseScreenData.REVERSED);
			sv.taxmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.taxmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.taxcde.setReverse(BaseScreenData.REVERSED);
			sv.taxcde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.taxcde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.sprschm.setReverse(BaseScreenData.REVERSED);
			sv.sprschm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.sprschm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.agccqind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind16.isOn()) {
			sv.agccqind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.agccqind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.agccqind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.houseLoan.setReverse(BaseScreenData.REVERSED);
			sv.houseLoan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.houseLoan.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.computerLoan.setReverse(BaseScreenData.REVERSED);
			sv.computerLoan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.computerLoan.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.carLoan.setReverse(BaseScreenData.REVERSED);
			sv.carLoan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.carLoan.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.officeRent.setReverse(BaseScreenData.REVERSED);
			sv.officeRent.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.officeRent.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.otherLoans.setReverse(BaseScreenData.REVERSED);
			sv.otherLoans.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.otherLoans.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.tcolprct.setReverse(BaseScreenData.REVERSED);
			sv.tcolprct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.tcolprct.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.tcolmax.setReverse(BaseScreenData.REVERSED);
			sv.tcolmax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.tcolmax.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
    	  
                 
         
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4">
 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
					    		<table><tr><td>
					    		 
					    		<%					
		                             if(!((sv.clntsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
						            	if(longValue == null || longValue.equalsIgnoreCase("")) {
								        formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							            } else {
								         formatValue = formatValue( longValue);
						 	             }
							
							
					                } else  {
								
					                    if(longValue == null || longValue.equalsIgnoreCase("")) {
							         	formatValue = formatValue( (sv.clntsel.getFormData()).toString()); 
							            } else {
								        formatValue = formatValue( longValue);
							       }
					
					              }
				            	%>			
				        <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						   "blank_cell" : "output_cell" %>' >
				          <%=XSSFilter.escapeHtml(formatValue)%>
			          </div>	
		                  <%
		                   longValue = null;
		                   formatValue = null;
		                   %>
		                   </td><td>
					    		
			                       <%					
		                             if(!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							            if(longValue == null || longValue.equalsIgnoreCase("")) {
							         	formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							            } else {
								         formatValue = formatValue( longValue);
							            }
							
							
					                } else  {
								
					                    if(longValue == null || longValue.equalsIgnoreCase("")) {
								        formatValue = formatValue( (sv.cltname.getFormData()).toString()); 
							             } else {
							           	formatValue = formatValue( longValue);
							             }
					
					                    }
					                  %>			
				     <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;min-width:120px;">
				         <%=XSSFilter.escapeHtml(formatValue)%>
			          </div>	
		   <%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
  	          		      			   			
					      	        
			                     					    		
				      		</div>
				    </div>
			   </div>
			   
			   <div class="row">	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Number")%></label>
					    		<div class="input-group"> 
					    		
		                       <%					
	                             if(!((sv.agnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							        if(longValue == null || longValue.equalsIgnoreCase("")) {
								    formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							        } else {
							    	formatValue = formatValue( longValue);
							        }
							
							
					              } else  {
								
					                if(longValue == null || longValue.equalsIgnoreCase("")) {
								    formatValue = formatValue( (sv.agnum.getFormData()).toString()); 
							        } else {
								    formatValue = formatValue( longValue);
							         }
					
					               }
					             %>			
				                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						           "blank_cell" : "output_cell" %>'  >
				                   <%=XSSFilter.escapeHtml(formatValue)%>
			                    </div>	
		                 <%
		                   longValue = null;
		                   formatValue = null;
		                  %> 
		                  </div> 					    		
				      		</div>
				    	</div>
			    	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Agent Type")%></label>
					    		<table><tr><td>
					    		<%					
		             if(!((sv.agtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
  
		<%					
		if(!((sv.agtydesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.agtydesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px;min-width:120px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
			                     					    		
				      		</div>
				      </div>
			      	
			    	  <div class="col-md-4"> 
				    		<div class="form-group"> 
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Appointed")%></label> 	  
					    		<div class="input-group"> <%					
		          if(!((sv.dteappDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dteappDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		    		      			   				    		
				      		</div>
				      </div></div>
			        </div>
			     
			    	
			 <hr>
			 <br>
			 <!-- <br> -->
    	     <div class="row">	
			    	<div class="col-md-4">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Commission and Payment Details")%></label>
			    	</div>
			 </div>
    	 
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Type of Termination")%></label>
					    			<div class="input-group">
					    			<input name='bmaflg' type='text'

                                  <%

		                          formatValue = (sv.bmaflg.getFormData()).toString();

                                  %>
	                              value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

                                  size='<%= sv.bmaflg.getLength()%>'
                                  maxLength='<%= sv.bmaflg.getLength()%>' 
                                  onFocus='doFocus(this)' onHelp='return fieldHelp(bmaflg)' onKeyUp='return checkMaxLength(this)'  


                                  <% 
	                              if((new Byte((sv.bmaflg).getEnabled()))
	                              .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                                  %>  
                                  
	                           readonly="true"
	                           class="output_cell"
                                        <%
	                                    }else if((new Byte((sv.bmaflg).getHighLight())).
		                                compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                                        %>	
		                          class="bold_cell" 

                                 <%
	                              }else { 
                                  %>

	                             class = ' <%=(sv.bmaflg).getColor()== null  ? 
			                     "input_cell" :  (sv.bmaflg).getColor().equals("red") ? 
			                     "input_cell red reverse" : "input_cell" %>'
 
                                 <%
	                               } 
                                  %>
                                  >
					     </div>
				    </div>
				    </div>
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Instant Credit%")%></label>
					    		<div class="input-group">
	                            <%	
			                    qpsf = fw.getFieldXMLDef((sv.intcrd).getFieldName());
			                    qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	                            %>

                               <input name='intcrd' type='text'

	                           value='<%=smartHF.getPicFormatted(qpsf,sv.intcrd) %>'
			                    <%
	                           valueThis=smartHF.getPicFormatted(qpsf,sv.intcrd);
	                          if(valueThis!=null&& valueThis.trim().length()>0) {%>
	                          title='<%=smartHF.getPicFormatted(qpsf,sv.intcrd) %>'
	                            <%}%>

                               size='<%= sv.intcrd.getLength()%>'
                               maxLength='<%= sv.intcrd.getLength()%>' 
                               onFocus='doFocus(this)' onHelp='return fieldHelp(intcrd)' onKeyUp='return checkMaxLength(this)'  

	                           onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	                           decimal='<%=qpsf.getDecimals()%>' 
	                           onPaste='return doPasteNumber(event);'
	                           onBlur='return doBlurNumber(event);'

                               <% 
	                           if((new Byte((sv.intcrd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                               %>  
	                           readonly="true"
	                           class="output_cell"
                                <%
	                            }else if((new Byte((sv.intcrd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                                %>	
		                       class="bold_cell" 

                              <%
	                           }else { 
                               %>

	                            class = ' <%=(sv.intcrd).getColor()== null  ? 
			                    "input_cell" :  (sv.intcrd).getColor().equals("red") ? 
			                     "input_cell red reverse" : "input_cell" %>'
 
                                 <%
	                              } 
                                  %>
                                  >				    		
				      	</div></div>
				    </div>
				    
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Fixed Percent")%></label>
					    			<div class="input-group"><%	
			                     qpsf = fw.getFieldXMLDef((sv.fixprc).getFieldName());
			                     qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	                             %>

                             <input name='fixprc' type='text'

	                         value='<%=smartHF.getPicFormatted(qpsf,sv.fixprc) %>'
			                  <%
	                           valueThis=smartHF.getPicFormatted(qpsf,sv.fixprc);
	                           if(valueThis!=null&& valueThis.trim().length()>0) {%>
	                           title='<%=smartHF.getPicFormatted(qpsf,sv.fixprc) %>'
	                           <%}%>

                              size='<%= sv.fixprc.getLength()%>'
                              maxLength='<%= sv.fixprc.getLength()%>' 
                              onFocus='doFocus(this)' onHelp='return fieldHelp(fixprc)' onKeyUp='return checkMaxLength(this)'  

	                          onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	                          decimal='<%=qpsf.getDecimals()%>' 
	                          onPaste='return doPasteNumber(event);'
	                          onBlur='return doBlurNumber(event);'

                               <% 
	                          if((new Byte((sv.fixprc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                               %>  
	                          readonly="true"
	                          class="output_cell"
                             <%
	                         }else if((new Byte((sv.fixprc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                              %>	
		                     class="bold_cell" 
 
                             <%
	                        }else { 
                              %>

	                         class = ' <%=(sv.fixprc).getColor()== null  ? 
			                 "input_cell" :  (sv.fixprc).getColor().equals("red") ? 
			                 "input_cell red reverse" : "input_cell" %>'
 
                             <%
	                           } 
                              %>
                               >
			                 </div>  					    		
				        </div>
				    </div></div>
				    
				    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Deduct tax")%></label>
					    		
					    		
					    		
					    		
					    	<%
					          if ((new Byte((sv.taxmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					                                                      || fw.getVariables().isScreenProtected()) {
							%>          
							
								<%-- <%=smartHF.getHTMLVarExt(fw, sv.taxmeth)%> --%>	                                         
					      <input name='taxmeth' type='text' style="width:190px;"    <%--ILIFE-9391 start--%>
						<%formatValue = (sv.taxmeth.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.taxmeth.getLength()%>'
						maxLength='<%=sv.taxmeth.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(agentrefcode)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.taxmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxmeth).getColor() == null ? "input_cell"
						: (sv.taxmeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>> <%--ILIFE-9391 end--%>
					        <%
								} else {
							%>
					        <div class="input-group" style="width: 115px;">
					            <%=smartHF.getRichTextInputFieldLookup(fw, sv.taxmeth)%>
					             <span class="input-group-btn">
					             <button class="btn btn-info" type="button"
					                onClick="doFocus(document.getElementById('taxmeth')); doAction('PFKEY04')">
					                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					            </button>
					        </div>
					        <%
					         }
					        %>      		
				    </div>
				</div>
				    
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Tax code")%></label>
					    	
				    		
					    	<%
					          if ((new Byte((sv.taxcde).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					                                                      || fw.getVariables().isScreenProtected()) {
							%>          
							
								<%-- <%=smartHF.getHTMLVarExt(fw, sv.taxcde)%>	 --%>           
								<input name='taxcde' type='text' style="width:190px;"   <%--ILIFE-9391 start--%>
						<%formatValue = (sv.taxcde.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.taxcde.getLength()%>'
						maxLength='<%=sv.taxcde.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(taxcde)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.taxcde).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxcde).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxcde).getColor() == null ? "input_cell"
						: (sv.taxcde).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>                              <%--ILIFE-9391 end--%>  
					      
					        <%
								} else {
							%>
					        <div class="input-group" style="width: 115px;">
					            <%=smartHF.getRichTextInputFieldLookup(fw, sv.taxcde)%>
					             <span class="input-group-btn">
					             <button class="btn btn-info" type="button"
					                onClick="doFocus(document.getElementById('taxcde')); doAction('PFKEY04')">
					                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					            </button>
					        </div>
					        <%
					         }
					        %>    
				    
				    </div>
				  </div>
				    
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Tax allowed")%></label>
					    		<div class="input-group">	<%	
			qpsf = fw.getFieldXMLDef((sv.taxalw).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxalw,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<!-- ILIFE-1535 START by nnazeer -->
<input name='taxalw' 
type='text' style="text-align:right;"

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxalw.getLength(), sv.taxalw.getScale(),3)%>'
maxLength='<%= sv.taxalw.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxalw)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxalw).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxalw).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxalw).getColor()== null  ? 
			"input_cell" :  (sv.taxalw).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<!-- ILIFE-1535 END -->
			                     </div>  					    		
				      		</div></div>
				    </div>
				    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("I.R.D. No")%></label>
					    		<div class="input-group">
					    		<input name='irdno' 
type='text'

<%

		formatValue = (sv.irdno.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.irdno.getLength()%>'
maxLength='<%= sv.irdno.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(irdno)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.irdno).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.irdno).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.irdno).getColor()== null  ? 
			"input_cell" :  (sv.irdno).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					    		
					    		
					    		</div>
					    		
					    		</div></div>
					    		<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Superannuation Code")%></label>
			
					    	
					    	
					    			<%
          if ((new Byte((sv.sprschm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                      || fw.getVariables().isScreenProtected()) {
		%>          
		
			<%-- <%=smartHF.getHTMLVarExt(fw, sv.sprschm)%>	 --%>    
			<input name='sprschm' type='text' style="width:190px;"     <%--ILIFE-9391 start--%>
						<%formatValue = (sv.sprschm.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.sprschm.getLength()%>'
						maxLength='<%=sv.sprschm.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(agentrefcode)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.sprschm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.sprschm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.sprschm).getColor() == null ? "input_cell"
						: (sv.sprschm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>                       <%--ILIFE-9391 end--%>                
      
        <%
			} else {
		%>
        <div class="input-group" style="width: 115px;">
            <%=smartHF.getRichTextInputFieldLookup(fw, sv.sprschm)%>
             <span class="input-group-btn">
             <button class="btn btn-info" type="button"
                onClick="doFocus(document.getElementById('sprschm')); doAction('PFKEY04')">
                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
            </button>
        </div>
        <%
         }
        %>      
					    		
					    		</div></div>
					    		<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Superannuation Percentage")%></label>
					    	<div class="input-group">
					    	<%	
			qpsf = fw.getFieldXMLDef((sv.sprprc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='sprprc' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.sprprc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.sprprc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.sprprc) %>'
	 <%}%>

size='<%= sv.sprprc.getLength()%>'
maxLength='<%= sv.sprprc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(sprprc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.sprprc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sprprc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sprprc).getColor()== null  ? 
			"input_cell" :  (sv.sprprc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					    	</div></div></div>
					    		
					    		
					    		</div>
				    
				    
				   <%--  <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("I.R.D. No")%></label>
					    			<div class="input-group">
					    			<input name='irdno' 
type='text'

<%

		formatValue = (sv.irdno.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.irdno.getLength()%>'
maxLength='<%= sv.irdno.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(irdno)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.irdno).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.irdno).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.irdno).getColor()== null  ? 
			"input_cell" :  (sv.irdno).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					    		</div></div>
				    </div>
				    
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Superannuation code")%></label>
					    		<div class="input-group" style="min-width:120px">
					    		<%	
	longValue = sv.sprschm.getFormData();  
%>

<% 
	if((new Byte((sv.sprschm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "input_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='sprschm' id='sprschm'
type='text' 
value='<%=sv.sprschm.getFormData()%>' 
maxLength='<%=sv.sprschm.getLength()%>' 
size='<%=sv.sprschm.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(sprschm)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.sprschm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.sprschm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
<class="bold_cell" >
                                  
                                    <span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('sprschm')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
		    		
				      		</div>
 
<a href="javascript:;" onClick="doFocus(document.getElementById('sprschm')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

<class = ' <%=(sv.sprschm).getColor()== null  ? 
"input_cell" :  (sv.sprschm).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('sprschm')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<a href="javascript:;" onClick="doFocus(document.getElementById('sprschm')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}} %>
				    		
				      		</div>
				    </div>
				    
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Superannuation %")%></label>
					    		<div class="input-group" >	<%	
			qpsf = fw.getFieldXMLDef((sv.sprprc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='sprprc' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.sprprc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.sprprc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.sprprc) %>'
	 <%}%>

size='<%= sv.sprprc.getLength()%>'
maxLength='<%= sv.sprprc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(sprprc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.sprprc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sprprc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sprprc).getColor()== null  ? 
			"input_cell" :  (sv.sprprc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
			                     </div>  </div>					    		
				      		</div>
				    </div> --%>
					
				    <div class="row">
				    <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Consolidate Payment to A/c.Payee")%></label>
					    		<div class="input-group" ><input name='agccqind' 
type='text'

<%

		formatValue = (sv.agccqind.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.agccqind.getLength()%>'
maxLength='<%= sv.agccqind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(agccqind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.agccqind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.agccqind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.agccqind).getColor()== null  ? 
			"input_cell" :  (sv.agccqind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					    		</div></div>
				         </div>
				    </div>
				    <hr>
				<br>
    	<div class="row">	
			    	<div class="col-md-4">
			    	<label><%=resourceBundleHandler.gettingValueFromBundle("Commission Collateral Details")%></label>
			    	</div></div>
				 
				     <div class="row">	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Collateral %")%></label>
					    	<div class="input-group" >	
	<%	
			qpsf = fw.getFieldXMLDef((sv.tcolprct).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='tcolprct' 
type='text' style="text-align:right;" 

	value='<%=smartHF.getPicFormatted(qpsf,sv.tcolprct) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.tcolprct);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.tcolprct) %>'
	 <%}%>

size='<%= sv.tcolprct.getLength()%>'
maxLength='<%= sv.tcolprct.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(tcolprct)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.tcolprct).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.tcolprct).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.tcolprct).getColor()== null  ? 
			"input_cell" :  (sv.tcolprct).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
> 					    		
				      		</div></div>
				    	</div>
				    	 <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Outstanding B/L")%></label>
					    		<div class="input-group" style="min-width:80px" ><%	
			qpsf = fw.getFieldXMLDef((sv.tcolbal).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S12VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tcolbal,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.tcolbal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="text-align:right;" >	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
  					    		
				      		</div></div>
				    	</div>
				    	 <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Max Limit")%></label>
					    	<div class="input-group" >	<%	
			qpsf = fw.getFieldXMLDef((sv.tcolmax).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.tcolmax,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='tcolmax' 
type='text'  style="text-align:right;" 

	value='<%=valueThis%>'
			 <%
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.tcolmax.getLength(), sv.tcolmax.getScale(),3)%>'
maxLength='<%= sv.tcolmax.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(tcolmax)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.tcolmax).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.tcolmax).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.tcolmax).getColor()== null  ? 
			"input_cell" :  (sv.tcolmax).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  					    		
				      		</div>
				    	</div>
			    	</div></div>
			      <!-- <hr>
			      <br> -->
			      <hr>
			      <br>
    	 <div class="row">	
			    	   <div class="col-md-4"> 
				    		 	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Financial Details")%></label>
					    		</div></div>
			    	
			    	
			    	 <div class="row">	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Housing Loan")%></label>
					    	<div class="input-group" >	<input name='houseLoan' 
type='text'

<%

		formatValue = (sv.houseLoan.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.houseLoan.getLength()%>'
maxLength='<%= sv.houseLoan.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(houseLoan)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.houseLoan).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.houseLoan).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.houseLoan).getColor()== null  ? 
			"input_cell" :  (sv.houseLoan).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  					    		
				      		</div></div>
				    	</div>
				    	 <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Car Loan")%></label>
					    <div class="input-group" >		<input name='carLoan' 
type='text'

<%

		formatValue = (sv.carLoan.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.carLoan.getLength()%>'
maxLength='<%= sv.carLoan.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(carLoan)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.carLoan).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.carLoan).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.carLoan).getColor()== null  ? 
			"input_cell" :  (sv.carLoan).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
> 					    		
				      		</div>
				    	</div></div>
				    	 <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Computer Loan")%></label>
					    	<div class="input-group" >	<input name='computerLoan' 
type='text'

<%

		formatValue = (sv.computerLoan.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.computerLoan.getLength()%>'
maxLength='<%= sv.computerLoan.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(computerLoan)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.computerLoan).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.computerLoan).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.computerLoan).getColor()== null  ? 
			"input_cell" :  (sv.computerLoan).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  					    		
				      		</div>
				    	</div></div>
			    	</div>
			    	
			    	 <div class="row">	
			    	   <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Office Rent")%></label>
					    	<div class="input-group" >	<input name='officeRent' 
type='text'

<%

		formatValue = (sv.officeRent.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.officeRent.getLength()%>'
maxLength='<%= sv.officeRent.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(officeRent)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.officeRent).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.officeRent).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.officeRent).getColor()== null  ? 
			"input_cell" :  (sv.officeRent).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  					    		
				      		</div></div>
				    	</div>
				    	 <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Other")%></label>
					    	<div class="input-group" >	<input name='otherLoans' 
type='text'

<%

		formatValue = (sv.otherLoans.getFormData()).toString();

%>
	value='<%=XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.otherLoans.getLength()%>'
maxLength='<%= sv.otherLoans.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(otherLoans)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.otherLoans).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.otherLoans).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.otherLoans).getColor()== null  ? 
			"input_cell" :  (sv.otherLoans).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  					    		
				      		</div>
				    	</div>
			    	</div>
				    </div>
				    
				    
				    
				    
				    
				    </div>     
			   </div>           
			   

<%@ include file="/POLACommon2NEW.jsp"%>
