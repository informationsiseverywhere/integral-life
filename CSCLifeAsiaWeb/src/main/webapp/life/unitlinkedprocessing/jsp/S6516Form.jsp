<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6516";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6516ScreenVars sv = (S6516ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Print Unit Statement");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Reprint Unit Statement");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Outstanding Statement Requests");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>




<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
				    </div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>		
	 	<div class="panel-body">  
			<div class="row">	
			    <div class="col-md-3">
					<label class="radio-inline"><b>
						<%= smartHF.buildRadioOption(sv.action, "action", "A")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Print Unit Statement")%>
					</b></label>
				</div>				
				
				 <div class="col-md-3">
					<label class="radio-inline"><b>
					<%= smartHF.buildRadioOption(sv.action, "action", "B")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Reprint Unit Statement")%>
					</b></label>
				</div>
				
				<div class="col-md-6">
					<label class="radio-inline"><b>
					<%= smartHF.buildRadioOption(sv.action, "action", "C")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Outstanding Statement Requests")%>
					</b></label>
				</div>					        
			</div>
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
