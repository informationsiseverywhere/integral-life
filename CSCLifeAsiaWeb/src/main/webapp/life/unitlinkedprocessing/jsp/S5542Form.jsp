

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S5542";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5542ScreenVars sv = (S5542ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Dates effective ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Freq   Min.From RCD   Min.Withdrawal Amount   Min Amount Remaining /");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"/ Flat Withdrawal Fee     %Fee          Minimum Fee          Maximum Fee");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------------------------------------------------------------");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "|");
%>

<%
	{
		//ILIFE-2638-STARTS
		if (appVars.ind08.isOn()) {
			sv.billfreq01.setReverse(BaseScreenData.REVERSED);
			sv.billfreq01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq01.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount01.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem01.setReverse(BaseScreenData.REVERSED);
			sv.wdrem01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt01.setReverse(BaseScreenData.REVERSED);
			sv.ffamt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc01.setReverse(BaseScreenData.REVERSED);
			sv.feepc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin01.setReverse(BaseScreenData.REVERSED);
			sv.feemin01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax01.setReverse(BaseScreenData.REVERSED);
			sv.feemax01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax01.setHighLight(BaseScreenData.BOLD);
		}
		//ILIFE-2638-ENDS
		if (appVars.ind08.isOn()) {
			sv.billfreq02.setReverse(BaseScreenData.REVERSED);
			sv.billfreq02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq02.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount02.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem02.setReverse(BaseScreenData.REVERSED);
			sv.wdrem02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt02.setReverse(BaseScreenData.REVERSED);
			sv.ffamt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc02.setReverse(BaseScreenData.REVERSED);
			sv.feepc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin02.setReverse(BaseScreenData.REVERSED);
			sv.feemin02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax02.setReverse(BaseScreenData.REVERSED);
			sv.feemax02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq03.setReverse(BaseScreenData.REVERSED);
			sv.billfreq03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq03.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount03.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem03.setReverse(BaseScreenData.REVERSED);
			sv.wdrem03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt03.setReverse(BaseScreenData.REVERSED);
			sv.ffamt03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc03.setReverse(BaseScreenData.REVERSED);
			sv.feepc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin03.setReverse(BaseScreenData.REVERSED);
			sv.feemin03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax03.setReverse(BaseScreenData.REVERSED);
			sv.feemax03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq04.setReverse(BaseScreenData.REVERSED);
			sv.billfreq04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq04.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount04.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem04.setReverse(BaseScreenData.REVERSED);
			sv.wdrem04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt04.setReverse(BaseScreenData.REVERSED);
			sv.ffamt04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc04.setReverse(BaseScreenData.REVERSED);
			sv.feepc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin04.setReverse(BaseScreenData.REVERSED);
			sv.feemin04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax04.setReverse(BaseScreenData.REVERSED);
			sv.feemax04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq05.setReverse(BaseScreenData.REVERSED);
			sv.billfreq05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq05.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount05.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem05.setReverse(BaseScreenData.REVERSED);
			sv.wdrem05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt05.setReverse(BaseScreenData.REVERSED);
			sv.ffamt05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc05.setReverse(BaseScreenData.REVERSED);
			sv.feepc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin05.setReverse(BaseScreenData.REVERSED);
			sv.feemin05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax05.setReverse(BaseScreenData.REVERSED);
			sv.feemax05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq06.setReverse(BaseScreenData.REVERSED);
			sv.billfreq06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.wdlFreq06.setReverse(BaseScreenData.REVERSED);
			sv.wdlFreq06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.wdlFreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.wdlAmount06.setReverse(BaseScreenData.REVERSED);
			sv.wdlAmount06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.wdlAmount06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.wdrem06.setReverse(BaseScreenData.REVERSED);
			sv.wdrem06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wdrem06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.ffamt06.setReverse(BaseScreenData.REVERSED);
			sv.ffamt06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.ffamt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc06.setReverse(BaseScreenData.REVERSED);
			sv.feepc06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.feemin06.setReverse(BaseScreenData.REVERSED);
			sv.feemin06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.feemin06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.feemax06.setReverse(BaseScreenData.REVERSED);
			sv.feemax06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.feemax06.setHighLight(BaseScreenData.BOLD);
		}
		//ILIFE-7956 START
		if (appVars.ind10.isOn()) {
			sv.maxwith01.setInvisibility(BaseScreenData.INVISIBLE);
		}

		if (appVars.ind11.isOn()) {
			sv.maxwith02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.maxwith03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind13.isOn()) {
			sv.maxwith04.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.maxwith05.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.maxwith06.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-7956END
	}
%>



<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

.input-group-addon.ellipsis {
    max-width: 200px !important;
    }
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 90px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						</tr>
						</table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
<%
int[] tblColumnWidth = new int[]{120,120,120,120,120,120,120,120,120};
int totalTblWidth = 1080;
int calculatedValue =0;
int count = 1;

 %>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5542' width='100%'>
						<thead>
							<tr class='info'>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Min. From RCD")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Min.Withdrawal Amount")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Min Amount Remaining")%></th>
								<!-- ILIFE-7956START -->
								<%
						if ((new Byte((sv.maxwith01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Max Withdrawal %")%></th>
							<%} %>	
								<!-- ILIFE-7956 END -->
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Flat Withdrawal Fee")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Percentage Fee")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Minimum Fee")%></th>

								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Maximum Fee")%></th>
							</tr>
						</thead>

						<tbody>
							<%-- <%
								String backgroundcolor1 = "#FFFFFF";
								String backgroundcolor2 = "#EEEEEE";
							%> --%>
							<tr>
								<td>
								<div class="input-group">
									<%
										longValue = sv.billfreq01.getFormData();
									%> <%
									 	if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									 %>
								<%-- <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "input_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							  	 </div> --%>
							  	 <input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									 <%
								 	longValue = null;
								 %> <%
								 	} else {
								 %>
									  <input name='billfreq01' id='billfreq01' type='text' style="width: 50px !important;"
											value='<%=sv.billfreq01.getFormData()%>'
											maxLength='<%=sv.billfreq01.getLength()%>'
																		size='<%=sv.billfreq01.getLength()%>' onFocus='doFocus(this)'
																		onHelp='return fieldHelp(billfreq01)'
																		onKeyUp='return checkMaxLength(this)'
																		<%if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| fw.getVariables().isScreenProtected()) {%>
																		readonly="true" class="output_cell"> <%
									 	} else if ((new Byte((sv.billfreq01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									 %> class="bold_cell" ><span class="input-group-btn">
												<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq01')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span><%
									 	} else {
									 %> class = ' <%=(sv.billfreq01).getColor() == null
																? "input_cell"
																: (sv.billfreq01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
												<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq01')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span> <%
									 	}
									 	}
									 %>
 								</div>
								</td>
								</td>
								<td><input
									name='wdlFreq01' type='text'
									<%formatValue = (sv.wdlFreq01.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq01.getLength()%>'
									maxLength='<%=sv.wdlFreq01.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq01)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq01).getColor() == null
						? "input_cell"
						: (sv.wdlFreq01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td>
									<!-- ILIFE-1568 STARTS--> <%
 	qpsf = fw.getFieldXMLDef((sv.wdlAmount01).getFieldName());
 	//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
 	valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount01,
 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='wdlAmount01' type='text'
									<%if ((sv.wdlAmount01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount01.getLength(), sv.wdlAmount01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount01.getLength(), sv.wdlAmount01.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount01).getColor() == null
						? "input_cell"
						: (sv.wdlAmount01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem01).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem01' type='text'
									<%if ((sv.wdrem01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem01.getLength(), sv.wdrem01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem01.getLength(), sv.wdrem01.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem01).getColor() == null
						? "input_cell"
						: (sv.wdrem01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
								<%
						if ((new Byte((sv.maxwith01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.maxwith01).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith01' type='text'
									<%if ((sv.maxwith01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith01.getLength(), sv.maxwith01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith01.getLength(), sv.maxwith01.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith01).getColor() == null
						? "input_cell"
						: (sv.maxwith01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%} %>
								<!-- ILIFE-7956 END -->
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt01).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt01' type='text'
									<%if ((sv.ffamt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt01.getLength(), sv.ffamt01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt01.getLength(), sv.ffamt01.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt01).getColor() == null
						? "input_cell"
						: (sv.ffamt01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc01).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc01' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc01)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc01)%>' <%}%>
									size='<%=sv.feepc01.getLength()%>'
									maxLength='<%=sv.feepc01.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc01).getColor() == null
						? "input_cell"
						: (sv.feepc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin01).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin01,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin01' type='text'
									<%if ((sv.feemin01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%//valueThis=smartHF.getPicFormatted(qpsf,sv.feemin02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin01.getLength(), sv.feemin01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin01.getLength(), sv.feemin01.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin01).getColor() == null
						? "input_cell"
						: (sv.feemin01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax01).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax01,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax01' type='text'
									<%if ((sv.feemax01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax01.getLength(), sv.feemax01.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax01.getLength(), sv.feemax01.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax01)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax01).getColor() == null
						? "input_cell"
						: (sv.feemax01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
							</tr>
							<%
								count = count + 1;
							%>
							<!-- ILIFE-2638-ENDS -->
							<tr>
								<td>
								<div class="input-group">
									<%
										longValue = sv.billfreq02.getFormData();
									%> <%
 	if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<%-- <div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div>  --%>
									<input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									<%
							 	longValue = null;
							 %> <%
							 	} else {
							 %>
 					 <input name='billfreq02' id='billfreq02' type='text' style="width: 50px !important;"
									value='<%=sv.billfreq02.getFormData()%>'
									maxLength='<%=sv.billfreq02.getLength()%>'
									size='<%=sv.billfreq02.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(billfreq02)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.billfreq02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span><%
 	} else {
 %> class = ' <%=(sv.billfreq02).getColor() == null
							? "input_cell"
							: (sv.billfreq02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
			<button class="btn btn-info" type="button" onclick="doFocus(document.getElementById('billfreq02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span><%
 	}
 	}
 %>
 </div>
								</td>
								</td>
								<td><input
									name='wdlFreq02' type='text'
									<%formatValue = (sv.wdlFreq02.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq02.getLength()%>'
									maxLength='<%=sv.wdlFreq02.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq02)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq02).getColor() == null
						? "input_cell"
						: (sv.wdlFreq02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td>
									<!-- ILIFE-1568 STARTS--> <%
 	qpsf = fw.getFieldXMLDef((sv.wdlAmount02).getFieldName());
 	//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
 	valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount02,
 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='wdlAmount02' type='text'
									<%if ((sv.wdlAmount02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount02.getLength(), sv.wdlAmount02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount02.getLength(), sv.wdlAmount02.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount02).getColor() == null
						? "input_cell"
						: (sv.wdlAmount02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem02).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem02' type='text'
									<%if ((sv.wdrem02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem02.getLength(), sv.wdrem02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem02.getLength(), sv.wdrem02.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem02).getColor() == null
						? "input_cell"
						: (sv.wdrem02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
										<%
						if ((new Byte((sv.maxwith02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
							
								<td>
								<%
										qpsf = fw.getFieldXMLDef((sv.maxwith02).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith02' type='text'
									<%if ((sv.maxwith02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith02.getLength(), sv.maxwith02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith02.getLength(), sv.maxwith02.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith02).getColor() == null
						? "input_cell"
						: (sv.maxwith02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%} %>
								<!-- ILIFE-7956 END -->
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt02).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt02' type='text'
									<%if ((sv.ffamt02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt02.getLength(), sv.ffamt02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt02.getLength(), sv.ffamt02.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt02).getColor() == null
						? "input_cell"
						: (sv.ffamt02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc02).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc02' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc02)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc02)%>' <%}%>
									size='<%=sv.feepc02.getLength()%>'
									maxLength='<%=sv.feepc02.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc02).getColor() == null
						? "input_cell"
						: (sv.feepc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin02).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin02,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin02' type='text'
									<%if ((sv.feemin02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%//valueThis=smartHF.getPicFormatted(qpsf,sv.feemin02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin02.getLength(), sv.feemin02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin02.getLength(), sv.feemin02.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin02).getColor() == null
						? "input_cell"
						: (sv.feemin02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax02).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax02,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax02' type='text'
									<%if ((sv.feemax02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax02.getLength(), sv.feemax02.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax02.getLength(), sv.feemax02.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax02)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax02).getColor() == null
						? "input_cell"
						: (sv.feemax02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
							</tr>
							<%
								count = count + 1;
							%>
							<tr >
								<td>
								<div class="input-group">
									<%
										longValue = sv.billfreq03.getFormData();
									%> <%
 	if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<%-- <div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> --%>
									<input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									 <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='billfreq03' id='billfreq03' type='text' style="width: 50px !important;"
									value='<%=sv.billfreq03.getFormData()%>'
									maxLength='<%=sv.billfreq03.getLength()%>'
									size='<%=sv.billfreq03.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(billfreq03)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.billfreq03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	} else {
 %> class = ' <%=(sv.billfreq03).getColor() == null
							? "input_cell"
							: (sv.billfreq03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span><%
 	}
 	}
 %>
 </div>
								</td>
								</td>
								<td><input
									name='wdlFreq03' type='text'
									<%formatValue = (sv.wdlFreq03.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq03.getLength()%>'
									maxLength='<%=sv.wdlFreq03.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq03)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq03).getColor() == null
						? "input_cell"
						: (sv.wdlFreq03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[2]%>px;" align="left">
									<%
										qpsf = fw.getFieldXMLDef((sv.wdlAmount03).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount03,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdlAmount03' type='text'
									<%if ((sv.wdlAmount03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount03.getLength(), sv.wdlAmount03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount03.getLength(), sv.wdlAmount03.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount03).getColor() == null
						? "input_cell"
						: (sv.wdlAmount03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem03).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem03' type='text'
									<%if ((sv.wdrem03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem03.getLength(), sv.wdrem03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem03.getLength(), sv.wdrem03.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem03).getColor() == null
						? "input_cell"
						: (sv.wdrem03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
								<%
						if ((new Byte((sv.maxwith03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.maxwith03).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith03' type='text'
									<%if ((sv.maxwith03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith03.getLength(), sv.maxwith03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith03.getLength(), sv.maxwith03.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith03).getColor() == null
						? "input_cell"
						: (sv.maxwith03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%}%>
								<!-- ILIFE-7956 END -->
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt03).getFieldName());
										//		qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt03' type='text'
									<%if ((sv.ffamt03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt03.getLength(), sv.ffamt03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt03.getLength(), sv.ffamt03.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt03).getColor() == null
						? "input_cell"
						: (sv.ffamt03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc03).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc03' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc03)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc03)%>' <%}%>
									size='<%=sv.feepc03.getLength()%>'
									maxLength='<%=sv.feepc03.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc03).getColor() == null
						? "input_cell"
						: (sv.feepc03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin03).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin03,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin03' type='text'
									<%if ((sv.feemin03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin03.getLength(), sv.feemin03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin03.getLength(), sv.feemin03.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin03).getColor() == null
						? "input_cell"
						: (sv.feemin03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax03).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax03,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax03' type='text'
									<%if ((sv.feemax03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax03.getLength(), sv.feemax03.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax03.getLength(), sv.feemax03.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax03)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax03).getColor() == null
						? "input_cell"
						: (sv.feemax03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
							</tr>
							<%
								count = count + 1;
							%>
							<tr>
								<td>
								<div class="input-group">
									<%
										longValue = sv.billfreq04.getFormData();
									%> <%
 	if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<%-- <div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> --%>
									<input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									 <%
 	longValue = null;
 %> <%
 	} else {
 %> <input name='billfreq04' id='billfreq04' type='text' style="width: 50px !important;"
									value='<%=sv.billfreq04.getFormData()%>'
									maxLength='<%=sv.billfreq04.getLength()%>'
									size='<%=sv.billfreq04.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(billfreq04)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.billfreq04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" > <span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	} else {
 %> class = ' <%=(sv.billfreq04).getColor() == null
							? "input_cell"
							: (sv.billfreq04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
			<button class="btn btn-info" type="button" onclick="doFocus(document.getElementById('billfreq04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	}
 	}
 %>
 </div>
								</td>
								</td>
								<td><input
									name='wdlFreq04' type='text'
									<%formatValue = (sv.wdlFreq04.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq04.getLength()%>'
									maxLength='<%=sv.wdlFreq04.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq04)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq04).getColor() == null
						? "input_cell"
						: (sv.wdlFreq04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdlAmount04).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount04,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdlAmount04' type='text'
									<%if ((sv.wdlAmount04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount04.getLength(), sv.wdlAmount04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount04.getLength(), sv.wdlAmount04.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount04).getColor() == null
						? "input_cell"
						: (sv.wdlAmount04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem04).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem04' type='text'
									<%if ((sv.wdrem04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem04.getLength(), sv.wdrem04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem04.getLength(), sv.wdrem04.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem04).getColor() == null
						? "input_cell"
						: (sv.wdrem04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
								<%
						if ((new Byte((sv.maxwith04).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.maxwith04).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith04' type='text'
									<%if ((sv.maxwith04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith04.getLength(), sv.maxwith04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith04.getLength(), sv.maxwith04.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith04).getColor() == null
						? "input_cell"
						: (sv.maxwith04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%}%>
								<!-- ILIFE-7956 END -->
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt04).getFieldName());
										//		qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt04' type='text'
									<%if ((sv.ffamt04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt04.getLength(), sv.ffamt04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt04.getLength(), sv.ffamt04.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt04).getColor() == null
						? "input_cell"
						: (sv.ffamt04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc04).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc04' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc04)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc04)%>' <%}%>
									size='<%=sv.feepc04.getLength()%>'
									maxLength='<%=sv.feepc04.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc04).getColor() == null
						? "input_cell"
						: (sv.feepc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin04).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin04,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin04' type='text'
									<%if ((sv.feemin04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin04.getLength(), sv.feemin04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin04.getLength(), sv.feemin04.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin04).getColor() == null
						? "input_cell"
						: (sv.feemin04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax04).getFieldName());
										//		qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax04,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax04' type='text'
									<%if ((sv.feemax04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax04.getLength(), sv.feemax04.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax04.getLength(), sv.feemax04.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax04)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax04).getColor() == null
						? "input_cell"
						: (sv.feemax04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
							</tr>
							<%
								count = count + 1;
							%>
							<tr>
								<td >
								<div class="input-group">
								<%
										longValue = sv.billfreq05.getFormData();
									%> <%
								 	if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								 %>
									<%-- <div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>' 
										style="width: 50px;">
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> --%>
									<input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									 <%
 	longValue = null;
 %> 
 <%
 	} else {
 %> <input name='billfreq05' id='billfreq05' type='text' style="width: 50px !important;"
									value='<%=sv.billfreq05.getFormData()%>'
									maxLength='<%=sv.billfreq05.getLength()%>'
									size='<%=sv.billfreq05.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(billfreq05)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.billfreq05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	} else {
 %> class = ' <%=(sv.billfreq05).getColor() == null
							? "input_cell"
							: (sv.billfreq05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	}
 	}
 %>
 </div>
								</td>
								</td>
								<td><input
									name='wdlFreq05' type='text'
									<%formatValue = (sv.wdlFreq05.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq05.getLength()%>'
									maxLength='<%=sv.wdlFreq05.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq05)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq05).getColor() == null
						? "input_cell"
						: (sv.wdlFreq05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td class="tableDataTag"
									style="width:<%=tblColumnWidth[2]%>px;" align="left">
									<%
										qpsf = fw.getFieldXMLDef((sv.wdlAmount05).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount05,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdlAmount05' type='text'
									<%if ((sv.wdlAmount05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount05.getLength(), sv.wdlAmount05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount05.getLength(), sv.wdlAmount05.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount05).getColor() == null
						? "input_cell"
						: (sv.wdlAmount05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem05).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem05' type='text'
									<%if ((sv.wdrem05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem05.getLength(), sv.wdrem05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem05.getLength(), sv.wdrem05.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem05).getColor() == null
						? "input_cell"
						: (sv.wdrem05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
										<%
						if ((new Byte((sv.maxwith05).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.maxwith05).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith05' type='text'
									<%if ((sv.maxwith05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith05.getLength(), sv.maxwith05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith05.getLength(), sv.maxwith05.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith05).getColor() == null
						? "input_cell"
						: (sv.maxwith05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%}%>
								<!-- ILIFE-7956 END -->
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt05).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt05' type='text'
									<%if ((sv.ffamt05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt05.getLength(), sv.ffamt05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt05.getLength(), sv.ffamt05.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt05).getColor() == null
						? "input_cell"
						: (sv.ffamt05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc05).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc05' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc05)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc05)%>' <%}%>
									size='<%=sv.feepc05.getLength()%>'
									maxLength='<%=sv.feepc05.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc05).getColor() == null
						? "input_cell"
						: (sv.feepc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin05).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin05,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin05' type='text'
									<%if ((sv.feemin05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin05.getLength(), sv.feemin05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin05.getLength(), sv.feemin05.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin05).getColor() == null
						? "input_cell"
						: (sv.feemin05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax05).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax05,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax05' type='text'
									<%if ((sv.feemax05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax05.getLength(), sv.feemax05.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax05.getLength(), sv.feemax05.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax05)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax05).getColor() == null
						? "input_cell"
						: (sv.feemax05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
							</tr>
							<%
								count = count + 1;
							%>
							<tr >
								<td >
								<div class="input-group">
									<%
										longValue = sv.billfreq06.getFormData();
									%> <%
 	if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<%-- <div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>' 
										style="width: 50px;">
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> --%>
									<input  type='text' style="text-align: right;" value="<%=longValue%>" disabled='true'>
									 <%
 	longValue = null;
 %>
  <%
 	} else {
 %> <input name='billfreq06' id='billfreq06' type='text' style="width: 50px !important;"
									value='<%=sv.billfreq06.getFormData()%>'
									maxLength='<%=sv.billfreq06.getLength()%>'
									size='<%=sv.billfreq06.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(billfreq06)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.billfreq06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span><%
 	} else {
 %> class = ' <%=(sv.billfreq06).getColor() == null
							? "input_cell"
							: (sv.billfreq06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' ><span class="input-group-btn">
			<button class="btn btn-info"  type="button" onclick="doFocus(document.getElementById('billfreq06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			</button>
		</span> <%
 	}
 	}
 %>
 </div>	
								</td>
								</td>
								<td ><input
									name='wdlFreq06' type='text'
									<%formatValue = (sv.wdlFreq06.getFormData()).toString();%>
									value='<%= XSSFilter.escapeHtml(formatValue)%>'
									<%if (formatValue != null && formatValue.trim().length() > 0) {%>
									title='<%=formatValue%>' <%}%>
									size='<%=sv.wdlFreq06.getLength()%>'
									maxLength='<%=sv.wdlFreq06.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(wdlFreq06)'
									onKeyUp='return checkMaxLength(this)'
									<%if ((new Byte((sv.wdlFreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlFreq06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlFreq06).getColor() == null
						? "input_cell"
						: (sv.wdlFreq06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>></td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdlAmount06).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdlAmount06,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdlAmount06' type='text'
									<%if ((sv.wdlAmount06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount06.getLength(), sv.wdlAmount06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdlAmount06.getLength(), sv.wdlAmount06.getScale(), 3)
					- 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdlAmount06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdlAmount06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdlAmount06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdlAmount06).getColor() == null
						? "input_cell"
						: (sv.wdlAmount06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.wdrem06).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.wdrem06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='wdrem06' type='text'
									<%if ((sv.wdrem06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem06.getLength(), sv.wdrem06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.wdrem06.getLength(), sv.wdrem06.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(wdrem06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.wdrem06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.wdrem06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.wdrem06).getColor() == null
						? "input_cell"
						: (sv.wdrem06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>
								</td>
								<!-- ILIFE-7956 START -->
												<%
						if ((new Byte((sv.maxwith06).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.maxwith06).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.maxwith06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='maxwith06' type='text'
									<%if ((sv.maxwith06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith06.getLength(), sv.maxwith06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.maxwith06.getLength(), sv.maxwith06.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(maxwith06)' 
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.maxwith06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.maxwith06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.maxwith06).getColor() == null
						? "input_cell"
						: (sv.maxwith06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<%}%>
								<!-- ILIFE-7956 START -->
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.ffamt06).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='ffamt06' type='text'
									<%if ((sv.ffamt06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt06.getLength(), sv.ffamt06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt06.getLength(), sv.ffamt06.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(ffamt06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.ffamt06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.ffamt06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.ffamt06).getColor() == null
						? "input_cell"
						: (sv.ffamt06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td>
									<%
										qpsf = fw.getFieldXMLDef((sv.feepc06).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
									%> <input name='feepc06' type='text'
									value='<%=smartHF.getPicFormatted(qpsf, sv.feepc06)%>'
									<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=smartHF.getPicFormatted(qpsf, sv.feepc06)%>' <%}%>
									size='<%=sv.feepc06.getLength()%>'
									maxLength='<%=sv.feepc06.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(feepc06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);'
									<%if ((new Byte((sv.feepc06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feepc06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feepc06).getColor() == null
						? "input_cell"
						: (sv.feepc06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.feemin06).getFieldName());
										//			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemin06,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemin06' type='text'
									<%if ((sv.feemin06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin06.getLength(), sv.feemin06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin06.getLength(), sv.feemin06.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemin06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemin06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemin06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemin06).getColor() == null
						? "input_cell"
						: (sv.feemin06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>>

								</td>
								<td >
									<%
										qpsf = fw.getFieldXMLDef((sv.feemax06).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										valueThis = smartHF.getPicFormatted(qpsf, sv.feemax06,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <input name='feemax06' type='text'
									<%if ((sv.feemax06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=valueThis%>'
									<%if (valueThis != null && valueThis.trim().length() > 0) {%>
									title='<%=valueThis%>' <%}%>
									size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax06.getLength(), sv.feemax06.getScale(), 3)%>'
									maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax06.getLength(), sv.feemax06.getScale(), 3) - 3%>'
									onFocus='doFocus(this),onFocusRemoveCommas(this)'
									onHelp='return fieldHelp(feemax06)'
									onKeyUp='return checkMaxLength(this)'
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									<%if ((new Byte((sv.feemax06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
									readonly="true" class="output_cell"
									<%} else if ((new Byte((sv.feemax06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%>
									class=' <%=(sv.feemax06).getColor() == null
						? "input_cell"
						: (sv.feemax06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
									<%}%>> <!-- ILIFE-1568 ENDS-->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<script>
	$(document).ready(function() {
		$('#dataTables-s5542').DataTable({
			ordering : false,
			searching : false,
			info:false,
			paging:false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2595 Life Cross Browser - Sprint 2 D5 : Task 5  -->
<style>
div[class*='input_cell'] {
	padding-left: 48px !important
}
</style>
<!-- ILIFE-2595 Life Cross Browser - Sprint 2 D5 : Task 5  -->
