<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6332";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S6332ScreenVars sv = (S6332ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6332screenWritten.gt(0)) {
%>
<%
	S6332screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sum Insured Min ");
%>
<%
	sv.sumInsMin.setClassString("");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Prem. Multiplier for S.I  ");
%>
<%
	sv.premmult.setClassString("");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Max ");
%>
<%
	sv.sumInsMax.setClassString("");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Unit Reserve Flag ");
%>
<%
	sv.rsvflg.setClassString("");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Lien Codes ");
%>
<%
	sv.liencd01.setClassString("");
%>
<%
	sv.liencd02.setClassString("");
%>
<%
	sv.liencd03.setClassString("");
%>
<%
	sv.liencd04.setClassString("");
%>
<%
	sv.liencd05.setClassString("");
%>
<%
	sv.liencd06.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmfrmDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmtoDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.sumInsMin.setReverse(BaseScreenData.REVERSED);
				sv.sumInsMin.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.sumInsMin.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.premmult.setReverse(BaseScreenData.REVERSED);
				sv.premmult.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.premmult.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.sumInsMax.setReverse(BaseScreenData.REVERSED);
				sv.sumInsMax.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.sumInsMax.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.rsvflg.setReverse(BaseScreenData.REVERSED);
				sv.rsvflg.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.rsvflg.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.liencd01.setReverse(BaseScreenData.REVERSED);
				sv.liencd01.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.liencd01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.liencd02.setReverse(BaseScreenData.REVERSED);
				sv.liencd02.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.liencd02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.liencd03.setReverse(BaseScreenData.REVERSED);
				sv.liencd03.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.liencd03.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.liencd04.setReverse(BaseScreenData.REVERSED);
				sv.liencd04.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.liencd04.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind11.isOn()) {
				sv.liencd05.setReverse(BaseScreenData.REVERSED);
				sv.liencd05.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind11.isOn()) {
				sv.liencd05.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind12.isOn()) {
				sv.liencd06.setReverse(BaseScreenData.REVERSED);
				sv.liencd06.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind12.isOn()) {
				sv.liencd06.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarExt(fw, sv.tabl)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.item)%>
						<%=smartHF.getHTMLVarExt(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<table>
						<tr>
							<td>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmfrmDisp, (sv.itmfrmDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=smartHF.getLit(generatedText6)%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmtoDisp, (sv.itmtoDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumInsMin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.premmult, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<label>&nbsp;&nbsp;<%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.sumInsMax, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.rsvflg)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd01, (sv.liencd01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd02, (sv.liencd02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd03, (sv.liencd03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd04, (sv.liencd04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd05, (sv.liencd05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group" style="width: 100px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.liencd06, (sv.liencd06.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd06')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S6332protectWritten.gt(0)) {
%>
<%
	S6332protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>
