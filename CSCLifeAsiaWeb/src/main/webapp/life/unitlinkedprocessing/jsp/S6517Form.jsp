

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6517";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6517ScreenVars sv = (S6517ScreenVars) fw.getVariables();%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Previous Statements");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"S - Summary");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select     Stmt     Stmt Date");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Detail");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
	        <div class="col-md-2">
	        	<div class="form-group">
	        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
	        		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	            </div>
	          </div>
	          
	          <div class="col-md-3"></div>
	          <div class="col-md-2">
	        	<div class="form-group">
	        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Owner"))%></label>
	        	<table><tr><td>
	        		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px; margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
	        	
	            </div>
	          </div>  		
	       </div>   
	       
	       <br> 		
	             <div class="col-md-5"></div>
	        <div class="row">
	        <div class="col-md-6">
	        	<div class="form-group">
	        		<label><center><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Previous Statements"))%></center></label>
	        		
	          </div>
	        </div>
	     </div>     		
	       <div class="row">
				<div class="col-md-12">
					<div class="form-group">
           <div class="table-responsive">
              
    	 	  <table class="table table-striped table-bordered table-hover" id='dataTables-s6517' width='100%'>	
    	 	   <thead>
    	<tr class='info'>
    	    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Statement")%></center></th>									
			<th><center><%=resourceBundleHandler.gettingValueFromBundle("Statement Date")%></center></th>
			<th><center><%=resourceBundleHandler.gettingValueFromBundle("Summary/Detail(S/D)")%></center></th>
			
			</tr>
 		 <tbody>
		<%
		GeneralTable sfl = fw.getTable("s6517screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		 <%
	S6517screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6517screensfl
	.hasMoreScreenRows(sfl)) {	
%> 

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[3];
int totalTblWidth = 0;
int calculatedValue =0;

	
						if(resourceBundleHandler.gettingValueFromBundle("Previous Statements").length() >= (sv.ustmno.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Previous Statements").length())*12;								
			} else {		
				calculatedValue = (sv.ustmno.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("S - Summary").length() >= (sv.stmtClDateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("S - Summary").length())*12;								
			} else {		
				calculatedValue = (sv.stmtClDateDisp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Select     Stmt     Stmt Date").length() >= (sv.stmtlevel.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Select     Stmt     Stmt Date").length())*12;								
			} else {		
				calculatedValue = (sv.stmtlevel.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			%>

<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    										
													
					
					 						 
						 
					<div style='display:none; visiblity:hidden;'>
						  <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s6517screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6517screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s6517screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%> px;"
						  
						  >
						  
						  						 	</div>
						  						 	
						 <td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef(sv.ustmno.getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%>
					
											 
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6517screensfl" + "." +
					 		"select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=smartHF.getPicFormatted(qpsf,sv.ustmno)%></span></a>						 		
					
				</td>
				
				<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
					<%= sv.stmtClDateDisp.getFormData()%>
				</td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="left">									
					<%= sv.stmtlevel.getFormData()%>
				</td>
					
	</tr>

	<%
	count = count + 1;
	S6517screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	</tbody> 
	</table>
	
	</div>

</div>

</div>

</div>

</div>
</div> 						 	

<script language="javascript">
        $(document).ready(function(){
	
			new superTable("s6517Table", {
				fixedCols : 0,	
			
				colWidths : [300,200,203],
				hasHorizonScroll :"Y",
				moreBtn: "N",
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>








<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/>



<Table>
<tr style='height:22px;'>
<td width='251'></td>
<td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Previous Statements")%>
</div>

</td>
</tr>
</Table>
			
		
		
		


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[3];
int totalTblWidth = 0;
int calculatedValue =0;

	
						if(resourceBundleHandler.gettingValueFromBundle("Previous Statements").length() >= (sv.ustmno.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Previous Statements").length())*12;								
			} else {		
				calculatedValue = (sv.ustmno.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("S - Summary").length() >= (sv.stmtClDateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("S - Summary").length())*12;								
			} else {		
				calculatedValue = (sv.stmtClDateDisp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Select     Stmt     Stmt Date").length() >= (sv.stmtlevel.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Select     Stmt     Stmt Date").length())*12;								
			} else {		
				calculatedValue = (sv.stmtlevel.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s6517screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
<div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" class ="tablePos" style='top:21px; width: <%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%> height: 205px; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 210 ) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		
		<DIV id="subf" style="POSITION: relative; WIDTH: <%=totalTblWidth%>px; HEIGHT: 205px;">
		<table style="width:<%=totalTblWidth%>px;" bgcolor="#dddddd" cellspacing="1px" class="tableTag" id="table">		
		
		<tr style="height: 25px;" class="tableRowHeader">
								
														<td class="tableDataHeader tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Statement")%></td>									
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[1 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Statement Date")%></td>
										
														<td class="tableDataHeader" style="width:<%=tblColumnWidth[2 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Summary / Detail (S/D)")%></td>
										
				
			
		</tr>
	
 
 <style type="text/css">
.fakeContainer {
	width:720px;		
	height:350px;	/*ILIFE-2143*/
	top: 90px;
	left:3px;			
}
.sSky th, .sSky td{
font-size:12px !important;
}
.s6517Table tr{height:25px}
</style>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("s6517Table", {
				fixedCols : 0,	
			
				colWidths : [300,200,203],
				hasHorizonScroll :"Y",
				moreBtn: "N",
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<div id="bottomCover" class="bottomCover">&nbsp;</div>

<div id="topCover" class="topCover">&nbsp;</div>

<div class="fakeContainer" id="container">
<table id="s6517Table" class="s6517Table">
 <tr >
		
		<th><%=resourceBundleHandler.gettingValueFromBundle("Statement")%></th>
		
		    							
				<th  ><%=resourceBundleHandler.gettingValueFromBundle("Statement Date")%></th>
				<th ><%=resourceBundleHandler.gettingValueFromBundle("Summary/Detail(S/D)")%></th>
	
			
			
			
	
					
	</tr>
 	
	<%
	S6517screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6517screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    										
													
					
					 						 
						 
					<div style='display:none; visiblity:hidden;'>
						  <input type='text' 
						maxLength='<%=sv.select.getLength()%>'
						 value='<%= sv.select.getFormData() %>' 
						 size='<%=sv.select.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s6517screensfl.select)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6517screensfl" + "." +
						 "select" + "_R" + count %>'
						 id='<%="s6517screensfl" + "." +
						 "select" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.select.getLength()*12%> px;"
						  
						  >
						  
						  						 	</div>
						 						 
										
					
				<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef(sv.ustmno.getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%>
					
											 
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6517screensfl" + "." +
					 		"select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=smartHF.getPicFormatted(qpsf,sv.ustmno)%></span></a>						 		
					
				</td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
					<%= sv.stmtClDateDisp.getFormData()%>
				</td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="left">									
					<%= sv.stmtlevel.getFormData()%>
				</td>
					
	</tr>

	<%
	count = count + 1;
	S6517screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
<!-- </DIV> -->
	
<br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>
