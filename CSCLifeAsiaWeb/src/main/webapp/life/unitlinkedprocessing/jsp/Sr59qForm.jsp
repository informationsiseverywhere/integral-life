<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR59Q";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.newbusiness.screens.*" %>

<%Sr59qScreenVars sv = (Sr59qScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Guaranteed Minimum Income Benefit (GMIB)  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit option ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Guaranteed Minimum Death Benefit (GMDB)");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit option  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Guaranteed Minimum Withdrawal Benefit (GMWB)");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit option  ");%>
<%{		
	if (appVars.ind03.isOn()) {
		sv.gmib.setReverse(BaseScreenData.REVERSED);
		sv.gmib.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.gmib.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.gmib.setEnabled(BaseScreenData.DISABLED);
	}
	
	if (appVars.ind07.isOn()) {
		sv.gmibsel.setReverse(BaseScreenData.REVERSED);
		sv.gmibsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.gmibsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind08.isOn()) {
		sv.gmibsel.setEnabled(BaseScreenData.DISABLED);
	}
	
	
	
	if (appVars.ind05.isOn()) {
		sv.gmdb.setReverse(BaseScreenData.REVERSED);
		sv.gmdb.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind05.isOn()) {
		sv.gmdb.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.gmdb.setEnabled(BaseScreenData.DISABLED);
	}
	
	if (appVars.ind09.isOn()) {
		sv.gmdbsel.setReverse(BaseScreenData.REVERSED);
		sv.gmdbsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind09.isOn()) {
		sv.gmdbsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind10.isOn()) {
		sv.gmdbsel.setEnabled(BaseScreenData.DISABLED);
	}
	
	
	if (appVars.ind11.isOn()) {
		sv.gmwb.setReverse(BaseScreenData.REVERSED);
		sv.gmwb.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind11.isOn()) {
		sv.gmwb.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind12.isOn()) {
		sv.gmwb.setEnabled(BaseScreenData.DISABLED);
	}
	
	if (appVars.ind13.isOn()) {
		sv.gmwbsel.setReverse(BaseScreenData.REVERSED);
		sv.gmwbsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind13.isOn()) {
		sv.gmwbsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind14.isOn()) {
		sv.gmwbsel.setEnabled(BaseScreenData.DISABLED);
	}
	
	
}%>



<div class="panel panel-default">
	<div class="panel-body">

	<div class="row">

		<div class="col-md-6">
			<div class="form-group">
			<table>
			<tr>
			<td>
			
			<%if (appVars.ind03.isOn()) {%>
				<span for="gmib" style="background-color:red; padding-top:1px; padding-bottom: 2px;">
				<% } else {%>
				<span for="gmib">
			<%}%>
			</span>

			<input type='checkbox' name='gmib' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(gmib)' onKeyUp='return checkMaxLength(this)'    
			<%
			
					if((sv.gmib).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.gmib).getEnabled() == BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('gmib')"/>

			<input type='checkbox' name='gmib' value='N' 
			
			<% if(!(sv.gmib).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('gmib')"/>

		</span>
		</td>
		<td>
		<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Guaranteed Minimum Income Benefit (GMIB)  ")%> <!-- ILIFE-3752 -->
		</label>
		</td>
		</tr>
		</table>			
		
		</div>
	</div>

	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit option")%></label>

			<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"gmibsel"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("gmibsel");
				optionValue = makeDropDownList( mappedItems , sv.gmibsel.getFormData(),1,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.gmibsel.getFormData()).toString().trim());  
			%>
			
			<% 
				if((new Byte((sv.gmibsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>  
				   		<%if(longValue != null){%>
				   		
				   		<%=sv.gmibsel.getFormData() %>
				   		
				   		<%}%>
				   </div>
			
			<%
			longValue = null;
			%>
			
				<% }else {%>
				
			<% if("red".equals((sv.gmibsel).getColor())){
			%>
			<div style="border:2px; border-style: solid; border-color: #e05050;  width:143px;"> 
			<%
			} 
			%>
			
			<select name='gmibsel' type='list' style="width:140px;"
			<% 
				if((new Byte((sv.gmibsel).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.gmibsel).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
				class = 'input_cell' 
			<%
				} 
			%>
			>
			<%=optionValue%>
			</select>
			<% if("red".equals((sv.gmibsel).getColor())){
			%>
			</div>
			<%
			} 
			%>
			
			<%
			} 
			%>
		</div>
	</div>
<!-- 	<div class="col-md-3"></div> -->
	<div class="col-md-4">
		<div class="form-group">
			<label></label>
			<div class='<%= (sv.gmiblongdesc.getFormData()).trim().length() == 0 ?
								"blank_cell" : "output_cell" %>' style="min-width: 150px;max-width: 200px;">
			<%
			if(!((sv.gmiblongdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmiblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
			
						} else  {
			
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmiblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
						}
						%>
					<%=XSSFilter.escapeHtml(formatValue)%> 
				</div>


			</div>
		</div>

	</div>
	

	<div class="row">

	<div class="col-md-6">
		<div class="form-group">
			<table>
			<tr>
			<td>

			<%if (appVars.ind05.isOn()) {%>
				<span for="gmdb" style="background-color:red; padding-top:1px; padding-bottom:1px;">
				<% } else {%>
				<span for="gmdb">
			<%}%>
			
			<input type='checkbox' name='gmdb' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(gmdb)' onKeyUp='return checkMaxLength(this)'    
			<%
			
					if((sv.gmdb).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.gmdb).getEnabled() == BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('gmdb')"/>
			
			<input type='checkbox' name='gmdb' value='N' 
			
			<% if(!(sv.gmdb).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('gmdb')"/>
			
			</span>
			
			
			</td>
			<td>
			
			<label><%=resourceBundleHandler.gettingValueFromBundle("Guaranteed Minimum Death Benefit (GMDB)")%></label>
			</td>
			</tr>
			</table>
			
		</div>
		</div>

	</div>
	
	<div class="row">


	<div class="col-md-4">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit option")%></label>

			<%
			fieldItem=appVars.loadF4FieldsLong(new String[] {"gmdbsel"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("gmdbsel");
			optionValue = makeDropDownList( mappedItems , sv.gmdbsel.getFormData(),1,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.gmdbsel.getFormData()).toString().trim());  
			%>
			
			<% 
			if((new Byte((sv.gmdbsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>  
			   		<%if(longValue != null){%>
			   		
			   		<%=sv.gmdbsel.getFormData() %>
			   		
			   		<%}%>
			   </div>
			
			<%
			longValue = null;
			%>
			
			<% }else {%>
			
			<% if("red".equals((sv.gmdbsel).getColor())){
			%>
			<div style="border:2px; border-style: solid; border-color: #e05050;  width:143px;"> 
			<%
			} 
			%>
			
			<select name='gmdbsel' type='list' style="width:140px;"
			<% 
			if((new Byte((sv.gmdbsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
			readonly="true"
			disabled
			class="output_cell"
			<%
			}else if((new Byte((sv.gmdbsel).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
				class="bold_cell" 
			<%
			}else { 
			%>
			class = 'input_cell' 
			<%
			} 
			%>
			>
			<%=optionValue%>
			</select>
			<% if("red".equals((sv.gmdbsel).getColor())){
			%>
			</div>
			<%
			} 
			%>
			
			<%
			} 
			%>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label></label>
			<div class='<%= (sv.gmdblongdesc.getFormData()).trim().length() == 0 ?
								"blank_cell" : "output_cell" %>' style="max-width: 200px;min-width: 150px;">
			<%
			if(!((sv.gmdblongdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmdblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
			
						} else  {
			
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmdblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
						}
						%>
					<%=XSSFilter.escapeHtml(formatValue)%> 
				</div>
			</div>
		</div>

	</div>
	
	<div class="row">
	

	<div class="col-md-6">
		<div class="form-group">
			
			<table>
			<tr>
			<td>
			<%if (appVars.ind11.isOn()) {%>
				<span for="gmwb" style="background-color:red; padding-top:1px; padding-bottom: 2px;">
				<% } else {%>
				<span for="gmwb">
			<%}%>
			
			<input type='checkbox' name='gmwb' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(gmwb)' onKeyUp='return checkMaxLength(this)'    
			<%
			
					if((sv.gmwb).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }if((sv.gmwb).getEnabled() == BaseScreenData.DISABLED||fw.getVariables().isScreenProtected()){%>
			    	   disabled
					
					<%}%>
			class ='UICheck' onclick="handleCheckBox('gmwb')"/>
			
			<input type='checkbox' name='gmwb' value='N' 
			
			<% if(!(sv.gmwb).toString().trim().equalsIgnoreCase("Y")){
						%>checked
					
			      <% }%>
			
			style="visibility: hidden" onclick="handleCheckBox('gmwb')"/>
			
			</span>
			
		</td>
		<td>
			<label>	
			<%=resourceBundleHandler.gettingValueFromBundle("Guaranteed Minimum Withdrawal Benefit (GMWB)  ")%>	<!-- ILIFE-3752 -->
			</label>
		</td>
		</tr>
		</table>
	</div>
	</div>
	</div>

	<div class="row">
	
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit option")%></label>
		
			<%
			fieldItem=appVars.loadF4FieldsLong(new String[] {"gmwbsel"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("gmwbsel");
			optionValue = makeDropDownList( mappedItems , sv.gmwbsel.getFormData(),1,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.gmwbsel.getFormData()).toString().trim());  
			%>
			
			<% 
			if((new Byte((sv.gmwbsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>  
			   		<%if(longValue != null){%>
			   		
			   		<%=sv.gmwbsel.getFormData() %>
			   		
			   		<%}%>
			   </div>
			
			<%
			longValue = null;
			%>
			
			<% }else {%>
			
			<%  if("red".equals((sv.gmwbsel).getColor())){ 
			%>
			<div style="border:2px; border-style: solid; border-color: #e05050;  width:143px;"> 
			<%
			}  
			%>
			
			<select name='gmwbsel' type='list' style="width:140px;"
			<% 
			if((new Byte((sv.gmwbsel).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
			readonly="true"
			disabled
			class="output_cell"
			<%
			}else if((new Byte((sv.gmwbsel).getHighLight())).
				compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
				class="bold_cell" 
			<%
			}else { 
			%>
			class = 'input_cell' 
			<%
			} 
			%>
			>
			<%=optionValue%>
			</select>
			<% if("red".equals((sv.gmwbsel).getColor())){
			%>
			</div>
			<%
			} 
			%>
			
			<%
			} 
			%>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
		<label></label>
			<div class='<%= (sv.gmwblongdesc.getFormData()).trim().length() == 0 ?
								"blank_cell" : "output_cell" %>' style="max-width: 200px;min-width: 150px;">
			<%
			if(!((sv.gmwblongdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmwblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
			
						} else  {
			
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.gmwblongdesc.getFormData()).toString());
								} else {
									formatValue = formatValue( longValue);
								}
			
						}
						%>
				<%=XSSFilter.escapeHtml(formatValue)%> 
				</div>


			</div>
		</div>

	</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
