

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5536";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5536ScreenVars sv = (S5536ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"To    %      %       ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"To    %      %       ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"To    %      %       ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Units  Init       ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Units  Init       ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Units  Init       ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "/Prem");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "/Prem");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "/Prem");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Over Target Allocation Item Key");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.billfreq01.setReverse(BaseScreenData.REVERSED);
			sv.billfreq01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.billfreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.billfreq02.setReverse(BaseScreenData.REVERSED);
			sv.billfreq02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.billfreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.billfreq03.setReverse(BaseScreenData.REVERSED);
			sv.billfreq03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.billfreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.maxPerioda01.setReverse(BaseScreenData.REVERSED);
			sv.maxPerioda01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.maxPerioda01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.pcUnitsa01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.pcUnitsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.pcInitUnitsa01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.pcInitUnitsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.maxPeriodb01.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodb01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.maxPeriodb01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.pcUnitsb01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsb01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.pcUnitsb01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.pcInitUnitsb01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsb01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.pcInitUnitsb01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.maxPeriodc01.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.maxPeriodc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.pcUnitsc01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.pcUnitsc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.pcInitUnitsc01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.pcInitUnitsc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.maxPerioda02.setReverse(BaseScreenData.REVERSED);
			sv.maxPerioda02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.maxPerioda02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.pcUnitsa02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.pcUnitsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.pcInitUnitsa02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.pcInitUnitsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.maxPeriodb02.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodb02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.maxPeriodb02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.pcUnitsb02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsb02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.pcUnitsb02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.pcInitUnitsb02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsb02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.pcInitUnitsb02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.maxPeriodc02.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.maxPeriodc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.pcUnitsc02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.pcUnitsc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.pcInitUnitsc02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.pcInitUnitsc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.maxPerioda03.setReverse(BaseScreenData.REVERSED);
			sv.maxPerioda03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.maxPerioda03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.pcUnitsa03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.pcUnitsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.pcInitUnitsa03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.pcInitUnitsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.maxPeriodb03.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodb03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.maxPeriodb03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.pcUnitsb03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsb03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.pcUnitsb03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.pcInitUnitsb03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsb03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.pcInitUnitsb03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.maxPeriodc03.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.maxPeriodc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.pcUnitsc03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.pcUnitsc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.pcInitUnitsc03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.pcInitUnitsc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.maxPerioda04.setReverse(BaseScreenData.REVERSED);
			sv.maxPerioda04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.maxPerioda04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.pcUnitsa04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.pcUnitsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.pcInitUnitsa04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.pcInitUnitsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.maxPeriodb04.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodb04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.maxPeriodb04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.pcUnitsb04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsb04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.pcUnitsb04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.pcInitUnitsb04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsb04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.pcInitUnitsb04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.maxPeriodc04.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.maxPeriodc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.pcUnitsc04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.pcUnitsc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.pcInitUnitsc04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.pcInitUnitsc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.billfreq04.setReverse(BaseScreenData.REVERSED);
			sv.billfreq04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.billfreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.billfreq05.setReverse(BaseScreenData.REVERSED);
			sv.billfreq05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.billfreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.billfreq06.setReverse(BaseScreenData.REVERSED);
			sv.billfreq06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.billfreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.maxPeriodd01.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.maxPeriodd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.pcUnitsd01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.pcUnitsd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.pcInitUnitsd01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.pcInitUnitsd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.maxPeriode01.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriode01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.maxPeriode01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind58.isOn()) {
			sv.pcUnitse01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitse01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.pcUnitse01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.pcInitUnitse01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitse01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.pcInitUnitse01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.maxPeriodf01.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodf01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.maxPeriodf01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.pcUnitsf01.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsf01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.pcUnitsf01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.pcInitUnitsf01.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsf01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.pcInitUnitsf01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.maxPeriodd02.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.maxPeriodd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.pcUnitsd02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.pcUnitsd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.pcInitUnitsd02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.pcInitUnitsd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.maxPeriode02.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriode02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.maxPeriode02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.pcUnitse02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitse02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.pcUnitse02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.pcInitUnitse02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitse02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.pcInitUnitse02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind72.isOn()) {
			sv.maxPeriodf02.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodf02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.maxPeriodf02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.pcUnitsf02.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsf02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.pcUnitsf02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind74.isOn()) {
			sv.pcInitUnitsf02.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsf02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind74.isOn()) {
			sv.pcInitUnitsf02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.maxPeriodd03.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.maxPeriodd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.pcUnitsd03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.pcUnitsd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.pcInitUnitsd03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.pcInitUnitsd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.maxPeriode03.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriode03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.maxPeriode03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.pcUnitse03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitse03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.pcUnitse03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.pcInitUnitse03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitse03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.pcInitUnitse03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			sv.maxPeriodf03.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodf03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.maxPeriodf03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind76.isOn()) {
			sv.pcUnitsf03.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsf03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind76.isOn()) {
			sv.pcUnitsf03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			sv.pcInitUnitsf03.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsf03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.pcInitUnitsf03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.maxPeriodd04.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.maxPeriodd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.pcUnitsd04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.pcUnitsd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.pcInitUnitsd04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.pcInitUnitsd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.maxPeriode04.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriode04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.maxPeriode04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind67.isOn()) {
			sv.pcUnitse04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitse04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.pcUnitse04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind68.isOn()) {
			sv.pcInitUnitse04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitse04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.pcInitUnitse04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind78.isOn()) {
			sv.maxPeriodf04.setReverse(BaseScreenData.REVERSED);
			sv.maxPeriodf04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.maxPeriodf04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind79.isOn()) {
			sv.pcUnitsf04.setReverse(BaseScreenData.REVERSED);
			sv.pcUnitsf04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.pcUnitsf04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind80.isOn()) {
			sv.pcInitUnitsf04.setReverse(BaseScreenData.REVERSED);
			sv.pcInitUnitsf04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.pcInitUnitsf04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.yotalmth.setReverse(BaseScreenData.REVERSED);
			sv.yotalmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.yotalmth.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					<td>


						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:width: 145px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group" >
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq01" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq01");
									optionValue = makeDropDownList(mappedItems, sv.billfreq01.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq01.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
							<%-- 	<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> --%> 
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
								<%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq01).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq01' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq01).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq02" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq02");
									optionValue = makeDropDownList(mappedItems, sv.billfreq02.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq02.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<%-- <div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div>  --%>
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
								<%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq02).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq02' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq02).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq03" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq03");
									optionValue = makeDropDownList(mappedItems, sv.billfreq03.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq03.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<%-- <div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> --%>
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
								 <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq03).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq03' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq03).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px;">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr >
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPerioda07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsa07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsa07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodb07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsb07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsb07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px;">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq04" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq04");
									optionValue = makeDropDownList(mappedItems, sv.billfreq04.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq04.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<%-- <div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> --%>
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
<%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq04).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq04' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq04).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq05" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq05");
									optionValue = makeDropDownList(mappedItems, sv.billfreq05.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq05.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<%-- <div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> --%>
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
								 <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq05).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq05' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq05).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "billfreq06" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("billfreq06");
									optionValue = makeDropDownList(mappedItems, sv.billfreq06.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.billfreq06.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<%-- <div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div>  --%>
								<input type="text" value='<%=longValue%>' style="width: 100px;"  disabled>
								<%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.billfreq06).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 122px;">
									<%
										}
									%>

									<select name='billfreq06' type='list' style="width: 120px;"
										<%if ((new Byte((sv.billfreq06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.billfreq06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.billfreq06).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px;">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodd07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsd07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsd07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px;">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriode07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitse07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitse07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<table>
						<tr>
							<td style="text-align: center;"><label style="font-size: 14px;">To</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Units<br />/&nbsp;Premium
							</label></td>
							<td style="text-align: center;"><label style="font-size: 14px;">%<br />Initial
							</label></td>
						</tr>
						<tr><td style="padding-top: 28px;"></td></tr>
						<tr>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td style="width: 65px;"><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
						
						
						<tr>
							<td><%=smartHF.getHTMLVarExt(fw, sv.maxPeriodf07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcUnitsf07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.pcInitUnitsf07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
					.replace("42px", "32px")%></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<table>
						<tr>
							<td><label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Over Target Allocation Item Key")%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<div class="input-group">
									<%
										longValue = sv.yotalmth.getFormData();
									%>

									<%
										if ((new Byte((sv.yotalmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>
									<div
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div>

									<%
										longValue = null;
									%>
									<%
										} else {
									%>
									<input name='yotalmth' type='text'
										value='<%=sv.yotalmth.getFormData()%>'
										maxLength='<%=sv.yotalmth.getLength()%>'
										size='<%=sv.yotalmth.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(yotalmth)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.yotalmth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.yotalmth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
											type="button"
											onclick="doFocus(document.getElementById('yotalmth')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
										} else {
									%>

									class = '
									<%=(sv.yotalmth).getColor() == null ? "input_cell"
							: (sv.yotalmth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
										class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
											type="button"
											onclick="doFocus(document.getElementById('yotalmth')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

									<%
										}
										}
									%>

								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

