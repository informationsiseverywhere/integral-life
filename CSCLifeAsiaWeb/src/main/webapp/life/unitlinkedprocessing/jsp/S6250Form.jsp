<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6250";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
//ILB-488
	S6250ScreenVars sv = (S6250ScreenVars) fw.getVariables();

	{
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					
					<div style="display: table;">
					<div class="input-group">
						<input id='chdrsel' name='chdrsel' type='text'
							value='<%=sv.chdrsel.getFormData()%>'
							maxLength='<%=sv.chdrsel.getLength()%>'
							size='<%=sv.chdrsel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(chdrsel)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.chdrsel).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" 
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							} else {
						%>

						class = '
						<%=(sv.chdrsel).getColor() == null ? "input_cell"
						: (sv.chdrsel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
		</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<label class="radio-inline"><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
		<b><%=resourceBundleHandler.gettingValueFromBundle("Redirect Premiums")%></b></label>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<label class="radio-inline"><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
<b><%=resourceBundleHandler.gettingValueFromBundle("Component Inquiry")%></b></label>
			</div>
			<div class="col-md-3">
				<input name='action' type='hidden'
					value='<%=sv.action.getFormData()%>'
					size='<%=sv.action.getLength()%>'
					maxLength='<%=sv.action.getLength()%>' class="input_cell"
					onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
					onKeyUp='return checkMaxLength(this)'>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

