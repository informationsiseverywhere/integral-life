<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR508";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%Sr508ScreenVars sv = (Sr508ScreenVars) fw.getVariables();%>

<%if (sv.Sr508screenWritten.gt(0)) {%>
	<%Sr508screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%sv.scheduleName.setClassString("");%>
<%	sv.scheduleName.appendClassString("string_fld");
	sv.scheduleName.appendClassString("output_txt");
	sv.scheduleName.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%sv.acctmonth.setClassString("");%>
<%	sv.acctmonth.appendClassString("num_fld");
	sv.acctmonth.appendClassString("output_txt");
	sv.acctmonth.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%sv.scheduleNumber.setClassString("");%>
<%	sv.scheduleNumber.appendClassString("num_fld");
	sv.scheduleNumber.appendClassString("output_txt");
	sv.scheduleNumber.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%sv.acctyear.setClassString("");%>
<%	sv.acctyear.appendClassString("num_fld");
	sv.acctyear.appendClassString("output_txt");
	sv.acctyear.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%sv.effdateDisp.setClassString("");%>
<%	sv.effdateDisp.appendClassString("string_fld");
	sv.effdateDisp.appendClassString("output_txt");
	sv.effdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%sv.bcompany.setClassString("");%>
<%	sv.bcompany.appendClassString("string_fld");
	sv.bcompany.appendClassString("output_txt");
	sv.bcompany.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%sv.jobq.setClassString("");%>
<%	sv.jobq.appendClassString("string_fld");
	sv.jobq.appendClassString("output_txt");
	sv.jobq.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%sv.bbranch.setClassString("");%>
<%	sv.bbranch.appendClassString("string_fld");
	sv.bbranch.appendClassString("output_txt");
	sv.bbranch.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all Contracts From      ");%>
	<%sv.datefrmDisp.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date From)");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To      ");%>
	<%sv.datetoDisp.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date To)");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 7, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 31, fw, sv.scheduleName)%>

	<%=smartHF.getLit(3, 49, generatedText3)%>

	<%=smartHF.getHTMLVar(3, 73, fw, sv.acctmonth, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(4, 16, generatedText4)%>

	<%=smartHF.getHTMLVar(4, 31, fw, sv.scheduleNumber, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(4, 60, generatedText5)%>

	<%=smartHF.getHTMLVar(4, 73, fw, sv.acctyear, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(5, 7, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(5, 31, fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 31, fw, sv.effdateDisp)%>

	<%=smartHF.getLit(5, 49, generatedText7)%>

	<%=smartHF.getHTMLVar(5, 73, fw, sv.bcompany)%>

	<%=smartHF.getLit(6, 7, generatedText8)%>

	<%=smartHF.getHTMLVar(6, 31, fw, sv.jobq)%>

	<%=smartHF.getLit(6, 49, generatedText9)%>

	<%=smartHF.getHTMLVar(6, 73, fw, sv.bbranch)%>

	<%=smartHF.getLit(13, 16, generatedText10)%>

	<%=smartHF.getHTMLSpaceVar(13, 50, fw, sv.datefrmDisp)%>
	<%=smartHF.getHTMLCalNSVar(13, 50, fw, sv.datefrmDisp)%>

	<%=smartHF.getLit(13, 62, generatedText11)%>

	<%=smartHF.getLit(15, 40, generatedText16)%>

	<%=smartHF.getHTMLSpaceVar(15, 50, fw, sv.datetoDisp)%>
	<%=smartHF.getHTMLCalNSVar(15, 50, fw, sv.datetoDisp)%>

	<%=smartHF.getLit(15, 62, generatedText12)%>




<%}%>

<%if (sv.Sr508protectWritten.gt(0)) {%>
	<%Sr508protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
