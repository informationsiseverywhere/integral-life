

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5412";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5412ScreenVars sv = (S5412ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Create New Bare Prices");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"E - Enquire on Existing Prices");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Modify Bare Prices");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"F - Input Bid/Offer Prices");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"C - Calculate Bid/Offer Prices");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"D - Modify Bid/Offer Prices");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"G - Activate Bid/Offer Prices");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Date ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(Option E only)");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Partial Fund ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(If search required; A, B, D, F)");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To Date ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(Option E only)");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action ");
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.dteeffDisp.setReverse(BaseScreenData.REVERSED);
			sv.dteeffDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.dteeffDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.jobno.setReverse(BaseScreenData.REVERSED);
			sv.jobno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.jobno.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.todateDisp.setReverse(BaseScreenData.REVERSED);
			sv.todateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.todateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.unitPartFund.setReverse(BaseScreenData.REVERSED);
			sv.unitPartFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitPartFund.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='dteeffDisp' type='text'
							value='<%=sv.dteeffDisp.getFormData()%>'
							maxLength='<%=sv.dteeffDisp.getLength()%>'
							size='<%=sv.dteeffDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dteeffDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dteeffDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.dteeffDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.dteeffDisp).getColor() == null ? "input_cell"
						: (sv.dteeffDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job No")%></label>
					<div style="width: 150px;">
						<input name='jobno' type='text'
							<%formatValue = (sv.jobno.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.jobno.getLength()%>'
							maxLength='<%=sv.jobno.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(jobno)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.jobno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.jobno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.jobno).getColor() == null ? "input_cell"
						: (sv.jobno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></label>
					<div>
						<%
							if ((new Byte((sv.unitVirtualFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("unitVirtualFund");
								optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.unitVirtualFund.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:100px;>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 240px;">
							<%
								}
							%>

							<select name='unitVirtualFund' type='list' style="width: 240px;"
								<%if ((new Byte((sv.unitVirtualFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Partial Fund")%></label>
					<div style="width: 70px;">
						<input name='unitPartFund' type='text'
							<%formatValue = (sv.unitPartFund.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.unitPartFund.getLength()%>'
							maxLength='<%=sv.unitPartFund.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitPartFund)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.unitPartFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPartFund).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPartFund).getColor() == null ? "input_cell"
						: (sv.unitPartFund).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("To Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='todateDisp' type='text'
							value='<%=sv.todateDisp.getFormData()%>'
							maxLength='<%=sv.todateDisp.getLength()%>'
							size='<%=sv.todateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(todateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.todateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.todateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.todateDisp).getColor() == null ? "input_cell"
						: (sv.todateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>

					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Create New Bare Prices")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "E")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Enquire on Existing Prices")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Modify Bare Prices")%></b>
					</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "F")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Input Bid/Offer Prices")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Calculate Bid/Offer Prices")%></b>
					</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "D")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Modify Bid/Offer Prices")%></b>
					</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "G")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Activate Bid/Offer Prices")%></b>
					</label>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("(Option E only)")%></label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("(If search required; A, B, D, F)")%></label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("(Option E only)")%></label>
				<div>
					<input name='action' type='hidden'
						value='<%=sv.action.getFormData()%>'
						size='<%=sv.action.getLength()%>'
						maxLength='<%=sv.action.getLength()%>' class="input_cell"
						onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
						onKeyUp='return checkMaxLength(this)'>
				</div>
			</div>
		</div>
	</div>
</div>
