<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5545Part";%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>


<%@ page session="false" %>
<%@ page import="java.util.Map" %> 
<%@ page import="java.util.*" %>  
<%@ page import="java.util.HashMap" %>  
<%@ page import="com.quipoz.framework.screendef.QPScreenField" %>   
<%@ page import="com.csc.life.newbusiness.screens.*" %>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%@ page import="com.csc.life.enquiries.screens.*" %> 
<%@ page import="com.csc.life.cashdividends.screens.*" %> 
<%@ page import="com.csc.life.contractservicing.screens.*" %>
<%@ page import="com.csc.life.terminationclaims.screens.*" %> 
<%@ page import="com.csc.smart.screens.*" %>
<%@ page import="com.csc.fsu.financials.screens.*" %>
<%@ page import="com.csc.life.productdefinition.screens.*" %>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="java.util.Map.Entry"%>

<%@page import="com.csc.smart.screens.S1661ScreenVars"%>
<%-- <%@page import="com.csc.fsu.general.screens.Sr343ScreenVars"%> --%>
<%@page import="com.csc.smart400framework.batch.cls.Qlrsetce"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.properties.PropertyLoader"%>
<%@ page import="com.csc.fsu.general.screens.*" %>
<%@page import="com.quipoz.framework.util.*" %>
<%@page import="com.quipoz.framework.screenmodel.*" %>
<%@page import="com.quipoz.framework.datatype.*" %>
<%@page import="com.quipoz.COBOLFramework.util.*" %>
<%  
	BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel == null) {
    	return;
    }
    ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	LifeAsiaAppVars av = (LifeAsiaAppVars)baseModel.getApplicationVariables();
	//SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter)AppVars.hf;
	LifeAsiaAppVars appVars = av;
	boolean[] savedInds = null;
	if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */
        av.reinitVariables();
        String lang = av.getInstance().getUserLanguage().toString().trim();
        String localeimageFolder = lang;
	/* Added For Internationalization */

	String tit ="";
	QPScreenField qpsf = null; 
	DataModel sm = null;
	String formatValue = null;
		String valueThis = null;
	String longValue = null;
	
	SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);
	smartHF.setLocale(request.getLocale());// used to store locale in smartHF

	
	//modified for the I18N of integral life. 
	//ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),lang);
	
	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),request.getLocale());
	
        //smartHF.setName(fw.getScreenName());
	//smartHF.setLang(lang);
	
	//StringDataHelper stringDataHelper = new StringDataHelper(fw.getScreenName(),lang);
	
	/** if (lang.equalsIgnoreCase("ENG")) {
		lang = "E";
	} else {
		lang = "S";
	}*/
	
	Map fieldItem=new HashMap();//used to store page Dropdown List
	String[] dropdownItems = null;  //used to store page Dropdown List
	Map mappedItems = null;
	String optionValue = null;
	
	
	String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
	smartHF.setFolderName(imageFolder);
%>

<%!
public String formatValue(String aValue) { 
    
return aValue;
	/*
	 This method was converting  o to space
	 now commented
	char[] valueCharArray = new char[aValue.length()];
	boolean nonZeroFlag = false; 
	
	if(!aValue.trim().equalsIgnoreCase("")) {
			valueCharArray = aValue.toCharArray();
			for (int i = 0; i < valueCharArray.length; i++) {
					if(valueCharArray[i] != '0')  {
						nonZeroFlag =  true;
						break;
					}							
			}     	
	} else {
	
	return " ";
	
	}
		
	if(nonZeroFlag) { 
		return aValue;
	}
	else {
		return " ";
	}
 */
}
//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}
public String makeDropDownList(Map mp, Object val, int i) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" + "</option>";
	String mainValue = "";
	
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}

	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);
		opValue = "";
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" + keyValueBean.getValue()
						+ "\" title=\"" + keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "^" + keyValueBean.getValue()
						+ "\" title=\"" + keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			}
		}
	}
	return opValue;
}
%>

<%S5545ScreenVars sv = (S5545ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq      Prem     %");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq      Prem      %");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq      Prem      % ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>

<%{
		if (appVars.ind03.isOn()) {
			sv.billfreq01.setReverse(BaseScreenData.REVERSED);
			sv.billfreq01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.billfreq01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.enhanaPrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.enhanaPrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.enhanaPc01.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.enhanaPc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.enhanaPrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.enhanaPrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.enhanaPc02.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.enhanaPc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.enhanaPrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.enhanaPrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.enhanaPc03.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.enhanaPc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.enhanaPrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.enhanaPrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.enhanaPc04.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.enhanaPc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.enhanaPrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.enhanaPrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.enhanaPc05.setReverse(BaseScreenData.REVERSED);
			sv.enhanaPc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.enhanaPc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.billfreq02.setReverse(BaseScreenData.REVERSED);
			sv.billfreq02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.billfreq02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.enhanbPrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.enhanbPrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.enhanbPc01.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.enhanbPc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.enhanbPrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.enhanbPrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.enhanbPc02.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.enhanbPc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.enhanbPrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.enhanbPrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.enhanbPc03.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.enhanbPc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.enhanbPrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.enhanbPrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.enhanbPc04.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.enhanbPc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.enhanbPrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.enhanbPrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.enhanbPc05.setReverse(BaseScreenData.REVERSED);
			sv.enhanbPc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.enhanbPc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.billfreq03.setReverse(BaseScreenData.REVERSED);
			sv.billfreq03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.billfreq03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.enhancPrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhancPrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.enhancPrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.enhancPc01.setReverse(BaseScreenData.REVERSED);
			sv.enhancPc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.enhancPc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.enhancPrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhancPrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.enhancPrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.enhancPc02.setReverse(BaseScreenData.REVERSED);
			sv.enhancPc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.enhancPc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.enhancPrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhancPrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.enhancPrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.enhancPc03.setReverse(BaseScreenData.REVERSED);
			sv.enhancPc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.enhancPc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.enhancPrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhancPrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.enhancPrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.enhancPc04.setReverse(BaseScreenData.REVERSED);
			sv.enhancPc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.enhancPc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.enhancPrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhancPrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.enhancPrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.enhancPc05.setReverse(BaseScreenData.REVERSED);
			sv.enhancPc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.enhancPc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.billfreq04.setReverse(BaseScreenData.REVERSED);
			sv.billfreq04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.billfreq04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.enhandPrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhandPrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.enhandPrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.enhandPc01.setReverse(BaseScreenData.REVERSED);
			sv.enhandPc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.enhandPc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.enhandPrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhandPrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.enhandPrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.enhandPc02.setReverse(BaseScreenData.REVERSED);
			sv.enhandPc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.enhandPc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.enhandPrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhandPrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.enhandPrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.enhandPc03.setReverse(BaseScreenData.REVERSED);
			sv.enhandPc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.enhandPc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.enhandPrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhandPrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.enhandPrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.enhandPc04.setReverse(BaseScreenData.REVERSED);
			sv.enhandPc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.enhandPc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.enhandPrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhandPrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.enhandPrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.enhandPc05.setReverse(BaseScreenData.REVERSED);
			sv.enhandPc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.enhandPc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.billfreq05.setReverse(BaseScreenData.REVERSED);
			sv.billfreq05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.billfreq05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.enhanePrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhanePrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.enhanePrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.enhanePc01.setReverse(BaseScreenData.REVERSED);
			sv.enhanePc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.enhanePc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.enhanePrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhanePrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.enhanePrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.enhanePc02.setReverse(BaseScreenData.REVERSED);
			sv.enhanePc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.enhanePc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.enhanePrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhanePrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.enhanePrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.enhanePc03.setReverse(BaseScreenData.REVERSED);
			sv.enhanePc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.enhanePc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.enhanePrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhanePrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.enhanePrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.enhanePc04.setReverse(BaseScreenData.REVERSED);
			sv.enhanePc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.enhanePc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.enhanePrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhanePrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.enhanePrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.enhanePc05.setReverse(BaseScreenData.REVERSED);
			sv.enhanePc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.enhanePc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.billfreq06.setReverse(BaseScreenData.REVERSED);
			sv.billfreq06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.billfreq06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.enhanfPrm01.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.enhanfPrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.enhanfPc01.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.enhanfPc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.enhanfPrm02.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.enhanfPrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.enhanfPc02.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.enhanfPc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.enhanfPrm03.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.enhanfPrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.enhanfPc03.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.enhanfPc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.enhanfPrm04.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.enhanfPrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.enhanfPc04.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.enhanfPc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.enhanfPrm05.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.enhanfPrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.enhanfPc05.setReverse(BaseScreenData.REVERSED);
			sv.enhanfPc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.enhanfPc05.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="row">
	<div class="col-md-2">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Freq      Prem     %")%></label>
			<div class="input-group" style="width: 95px;">
				<%-- <%	
					longValue = sv.billfreq01.getFormData();  
				%> --%>
				
				<% 
					if((new Byte((sv.billfreq01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<%-- <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "input_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div> --%>
					   <input type='text' value='<%=sv.billfreq01.getFormData()%>'  disabled >
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='billfreq01' id='billfreq01' 
				type='text' 
				value='<%=sv.billfreq01.getFormData()%>' 
				maxLength='<%=sv.billfreq01.getLength()%>' 
				size='<%=sv.billfreq01.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(billfreq01)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.billfreq01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.billfreq01).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.billfreq01).getColor()== null  ? 
				"input_cell" :  (sv.billfreq01).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<%}%>
				<span class="input-group-btn">
					<button class="btn btn-info"
						style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
						type="button"
						onClick="doFocus(document.getElementById('billfreq01')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</span>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-md-offset-2">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency ")%></label>
			<div class="input-group" style="width: 95px;">
				<%-- <%	
					longValue = sv.billfreq02.getFormData();  
				%> --%>
				
				<% 
					if((new Byte((sv.billfreq02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<%-- <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "input_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div> --%>
					    <input type='text' value='<%=sv.billfreq02.getFormData()%>'  disabled >
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='billfreq02' id='billfreq02' 
				type='text' 
				value='<%=sv.billfreq02.getFormData()%>' 
				maxLength='<%=sv.billfreq02.getLength()%>' 
				size='<%=sv.billfreq02.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(billfreq02)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.billfreq02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.billfreq02).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				
				<%
					}else { 
				%>
				
				class = ' <%=(sv.billfreq02).getColor()== null  ? 
				"input_cell" :  (sv.billfreq02).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<%}%>
				<span class="input-group-btn">
					<button class="btn btn-info"
						style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
						type="button"
						onClick="doFocus(document.getElementById('billfreq02')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</span>
				<%
					}
				%>

			</div>
		</div>
	</div>
	<div class="col-md-2 col-md-offset-2">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency ")%></label>
			<div class="input-group" style="width: 95px;">
				<%-- <%	
					longValue = sv.billfreq03.getFormData();  
				%> --%>
				
				<% 
					if((new Byte((sv.billfreq03).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<%-- <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "input_cell" %>'>  
					   		<%if(longValue != null){%>
					   		
					   		<%=longValue%>
					   		
					   		<%}%>
					   </div> --%>
					    <input type='text' value='<%=sv.billfreq03.getFormData()%>'  disabled >
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<input name='billfreq03' id='billfreq03' 
				type='text' 
				value='<%=sv.billfreq03.getFormData()%>' 
				maxLength='<%=sv.billfreq03.getLength()%>' 
				size='<%=sv.billfreq03.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(billfreq03)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
					if((new Byte((sv.billfreq03).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
					}else if((new Byte((sv.billfreq03).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					
				%>	
				class="bold_cell" >
				<%
					}else { 
				%>
				
				class = ' <%=(sv.billfreq03).getColor()== null  ? 
				"input_cell" :  (sv.billfreq03).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<%}%>
				<span class="input-group-btn">
					<button class="btn btn-info"
						style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
						type="button"
						onClick="doFocus(document.getElementById('billfreq03')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</span>
				<%
					}
				%>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<table style="width: 100%">
			<tbody>
			<tr>
				<td colspan="2" class="form-group">
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Premium")%>
					</label> &nbsp;
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("From")%>
					</label>
				</td>
				<td>
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Percentage")%>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanaPrm01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPrm01).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPrm01,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPrm01' type='text'
					<%if ((sv.enhanaPrm01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm01.getLength(), sv.enhanaPrm01.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm01.getLength(), sv.enhanaPrm01.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanaPrm01)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanaPrm01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPrm01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPrm01).getColor() == null ? "input_cell"
							: (sv.enhanaPrm01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td class="form-group">
					<%
						if ((new Byte((sv.enhanaPc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPc01).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPc01' type='text'
					<%if ((sv.enhanaPc01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc01)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPc01);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc01)%>' <%}%>
					size='<%=sv.enhanaPc01.getLength() + 1%>'
					maxLength='<%=sv.enhanaPc01.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanaPc01)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanaPc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPc01).getColor() == null ? "input_cell"
							: (sv.enhanaPc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanaPrm02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPrm02).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPrm02,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPrm02' type='text'
					<%if ((sv.enhanaPrm02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm02.getLength(), sv.enhanaPrm02.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm02.getLength(), sv.enhanaPrm02.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanaPrm02)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanaPrm02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPrm02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPrm02).getColor() == null ? "input_cell"
							: (sv.enhanaPrm02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanaPc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPc02).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPc02' type='text'
					<%if ((sv.enhanaPc02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc02)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPc02);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc02)%>' <%}%>
					size='<%=sv.enhanaPc02.getLength() + 1%>'
					maxLength='<%=sv.enhanaPc02.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanaPc02)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanaPc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPc02).getColor() == null ? "input_cell"
							: (sv.enhanaPc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanaPrm03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPrm03).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPrm03,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPrm03' type='text'
					<%if ((sv.enhanaPrm03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm03.getLength(), sv.enhanaPrm03.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm03.getLength(), sv.enhanaPrm03.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanaPrm03)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanaPrm03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPrm03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPrm03).getColor() == null ? "input_cell"
							: (sv.enhanaPrm03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanaPc03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPc03).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPc03' type='text'
					<%if ((sv.enhanaPc03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc03)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPc03);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc03)%>' <%}%>
					size='<%=sv.enhanaPc03.getLength() + 1%>'
					maxLength='<%=sv.enhanaPc03.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanaPc03)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanaPc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPc03).getColor() == null ? "input_cell"
							: (sv.enhanaPc03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanaPrm04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPrm04).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPrm04,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPrm04' type='text'
					<%if ((sv.enhanaPrm04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm04.getLength(), sv.enhanaPrm04.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm04.getLength(), sv.enhanaPrm04.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanaPrm04)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanaPrm04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPrm04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPrm04).getColor() == null ? "input_cell"
							: (sv.enhanaPrm04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanaPc04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPc04).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPc04' type='text'
					<%if ((sv.enhanaPc04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc04)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPc04);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc04)%>' <%}%>
					size='<%=sv.enhanaPc04.getLength() + 1%>'
					maxLength='<%=sv.enhanaPc04.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanaPc04)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanaPc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPc04).getColor() == null ? "input_cell"
							: (sv.enhanaPc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanaPrm05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPrm05).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPrm05,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPrm05' type='text'
					<%if ((sv.enhanaPrm05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm05.getLength(), sv.enhanaPrm05.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanaPrm05.getLength(), sv.enhanaPrm05.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanaPrm05)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanaPrm05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPrm05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPrm05).getColor() == null ? "input_cell"
							: (sv.enhanaPrm05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanaPc05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanaPc05).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanaPc05' type='text'
					<%if ((sv.enhanaPc05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc05)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanaPc05);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanaPc05)%>' <%}%>
					size='<%=sv.enhanaPc05.getLength() + 1%>'
					maxLength='<%=sv.enhanaPc05.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanaPc05)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanaPc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanaPc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanaPc05).getColor() == null ? "input_cell"
							: (sv.enhanaPc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 10px"></tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-4">
		<table style="width: 100%">
			<tr>
				<td colspan="2" class="form-group">
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Premium")%>
					</label> &nbsp;
					<label style="font-size: 14px;"> 
						<%=resourceBundleHandler.gettingValueFromBundle("From")%>
					</label>
				</td>
				<td>
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Percentage")%>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanbPrm01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPrm01).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPrm01,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPrm01' type='text'
					<%if ((sv.enhanbPrm01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm01.getLength(), sv.enhanbPrm01.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm01.getLength(), sv.enhanbPrm01.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanbPrm01)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanbPrm01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPrm01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPrm01).getColor() == null ? "input_cell"
							: (sv.enhanbPrm01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>

				<td>
					<%
						if ((new Byte((sv.enhanbPc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPc01).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPc01' type='text'
					<%if ((sv.enhanbPc01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc01)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPc01);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc01)%>' <%}%>
					size='<%=sv.enhanbPc01.getLength() + 1%>'
					maxLength='<%=sv.enhanbPc01.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanbPc01)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanbPc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPc01).getColor() == null ? "input_cell"
							: (sv.enhanbPc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanbPrm02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPrm02).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPrm02,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPrm02' type='text'
					<%if ((sv.enhanbPrm02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm02.getLength(), sv.enhanbPrm02.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm02.getLength(), sv.enhanbPrm02.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanbPrm02)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanbPrm02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPrm02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPrm02).getColor() == null ? "input_cell"
							: (sv.enhanbPrm02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanbPc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPc02).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPc02' type='text'
					<%if ((sv.enhanbPc02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc02)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPc02);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc02)%>' <%}%>
					size='<%=sv.enhanbPc02.getLength() + 1%>'
					maxLength='<%=sv.enhanbPc02.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanbPc02)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanbPc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPc02).getColor() == null ? "input_cell"
							: (sv.enhanbPc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanbPrm03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPrm03).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPrm03,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPrm03' type='text'
					<%if ((sv.enhanbPrm03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm03.getLength(), sv.enhanbPrm03.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm03.getLength(), sv.enhanbPrm03.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanbPrm03)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanbPrm03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPrm03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPrm03).getColor() == null ? "input_cell"
							: (sv.enhanbPrm03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanbPc03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPc03).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPc03' type='text'
					<%if ((sv.enhanbPc03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc03)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPc03);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc03)%>' <%}%>
					size='<%=sv.enhanbPc03.getLength() + 1%>'
					maxLength='<%=sv.enhanbPc03.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanbPc03)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanbPc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPc03).getColor() == null ? "input_cell"
							: (sv.enhanbPc03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanbPrm04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPrm04).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPrm04,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPrm04' type='text'
					<%if ((sv.enhanbPrm04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm04.getLength(), sv.enhanbPrm04.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm04.getLength(), sv.enhanbPrm04.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanbPrm04)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanbPrm04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPrm04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPrm04).getColor() == null ? "input_cell"
							: (sv.enhanbPrm04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanbPc04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPc04).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPc04' type='text'
					<%if ((sv.enhanbPc04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc04)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPc04);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc04)%>' <%}%>
					size='<%=sv.enhanbPc04.getLength() + 1%>'
					maxLength='<%=sv.enhanbPc04.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanbPc04)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanbPc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPc04).getColor() == null ? "input_cell"
							: (sv.enhanbPc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhanbPrm05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPrm05).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPrm05,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPrm05' type='text'
					<%if ((sv.enhanbPrm05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm05.getLength(), sv.enhanbPrm05.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhanbPrm05.getLength(), sv.enhanbPrm05.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhanbPrm05)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhanbPrm05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPrm05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPrm05).getColor() == null ? "input_cell"
							: (sv.enhanbPrm05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhanbPc05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhanbPc05).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhanbPc05' type='text'
					<%if ((sv.enhanbPc05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc05)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhanbPc05);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhanbPc05)%>' <%}%>
					size='<%=sv.enhanbPc05.getLength() + 1%>'
					maxLength='<%=sv.enhanbPc05.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhanbPc05)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhanbPc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhanbPc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhanbPc05).getColor() == null ? "input_cell"
							: (sv.enhanbPc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
		</table>
	</div>
	<div class="col-md-4">
		<table style="width: 100%">
			<tr>
				<td colspan="2" class="form-group">
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Premium")%>
					</label> &nbsp;
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("From")%>
					</label>
				</td>
				<td>
					<label style="font-size: 14px;">
						<%=resourceBundleHandler.gettingValueFromBundle("Percentage")%>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhancPrm01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPrm01).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPrm01,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPrm01' type='text'
					<%if ((sv.enhancPrm01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm01.getLength(), sv.enhancPrm01.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm01.getLength(), sv.enhancPrm01.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhancPrm01)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhancPrm01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPrm01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPrm01).getColor() == null ? "input_cell"
							: (sv.enhancPrm01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>

				<td>
					<%
						if ((new Byte((sv.enhancPc01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPc01).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPc01' type='text'
					<%if ((sv.enhancPc01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc01)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPc01);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc01)%>' <%}%>
					size='<%=sv.enhancPc01.getLength() + 1%>'
					maxLength='<%=sv.enhancPc01.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhancPc01)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhancPc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPc01).getColor() == null ? "input_cell"
							: (sv.enhancPc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhancPrm02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPrm02).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPrm02,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPrm02' type='text'
					<%if ((sv.enhancPrm02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm02.getLength(), sv.enhancPrm02.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm02.getLength(), sv.enhancPrm02.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhancPrm02)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhancPrm02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPrm02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPrm02).getColor() == null ? "input_cell"
							: (sv.enhancPrm02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>

				<td>
					<%
						if ((new Byte((sv.enhancPc02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPc02).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPc02' type='text'
					<%if ((sv.enhancPc02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc02)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPc02);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc02)%>' <%}%>
					size='<%=sv.enhancPc02.getLength() + 1%>'
					maxLength='<%=sv.enhancPc02.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhancPc02)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhancPc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPc02).getColor() == null ? "input_cell"
							: (sv.enhancPc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhancPrm03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPrm03).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPrm03,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPrm03' type='text'
					<%if ((sv.enhancPrm03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm03.getLength(), sv.enhancPrm03.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm03.getLength(), sv.enhancPrm03.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhancPrm03)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhancPrm03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPrm03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPrm03).getColor() == null ? "input_cell"
							: (sv.enhancPrm03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhancPc03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPc03).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPc03' type='text'
					<%if ((sv.enhancPc03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc03)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPc03);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc03)%>' <%}%>
					size='<%=sv.enhancPc03.getLength() + 1%>'
					maxLength='<%=sv.enhancPc03.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhancPc03)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhancPc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPc03).getColor() == null ? "input_cell"
							: (sv.enhanaPrm04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhancPrm04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPrm04).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPrm04,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPrm04' type='text'
					<%if ((sv.enhancPrm04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm04.getLength(), sv.enhancPrm04.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm04.getLength(), sv.enhancPrm04.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhancPrm04)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhancPrm04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPrm04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPrm04).getColor() == null ? "input_cell"
							: (sv.enhancPrm04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhancPc04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPc04).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPc04' type='text'
					<%if ((sv.enhancPc04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc04)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPc04);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc04)%>' <%}%>
					size='<%=sv.enhancPc04.getLength() + 1%>'
					maxLength='<%=sv.enhancPc04.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhancPc04)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhancPc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPc04).getColor() == null ? "input_cell"
							: (sv.enhancPc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
			<tr style="height: 5px"></tr>
			<tr>
				<td colspan="2" class="form-group">
					<%
						if ((new Byte((sv.enhancPrm05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPrm05).getFieldName());
 		//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 		valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPrm05,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPrm05' type='text'
					<%if ((sv.enhancPrm05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%> value='<%=valueThis%>'
					<%if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=valueThis%>' <%}%>
					size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm05.getLength(), sv.enhancPrm05.getScale(),
						3)%>'
					maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.enhancPrm05.getLength(), sv.enhancPrm05.getScale(), 3)
								- 3%>'
					onFocus='doFocus(this),onFocusRemoveCommas(this)'
					onHelp='return fieldHelp(enhancPrm05)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
					<%if ((new Byte((sv.enhancPrm05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPrm05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPrm05).getColor() == null ? "input_cell"
							: (sv.enhancPrm05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>> <%
 	}
 %>
				</td>
				<td>
					<%
						if ((new Byte((sv.enhancPc05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> <%
 	qpsf = fw.getFieldXMLDef((sv.enhancPc05).getFieldName());
 		qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
 %> <input name='enhancPc05' type='text'
					<%if ((sv.enhancPc05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc05)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.enhancPc05);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.enhancPc05)%>' <%}%>
					size='<%=sv.enhancPc05.getLength() + 1%>'
					maxLength='<%=sv.enhancPc05.getLength() + 1%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(enhancPc05)'
					onKeyUp='return checkMaxLength(this)'
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					<%if ((new Byte((sv.enhancPc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.enhancPc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					class="bold_cell" <%} else {%>
					class=' <%=(sv.enhancPc05).getColor() == null ? "input_cell"
							: (sv.enhancPc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%
	} 
%>> <%}%>
				</td>
			</tr>
		</table>
	</div>
</div>