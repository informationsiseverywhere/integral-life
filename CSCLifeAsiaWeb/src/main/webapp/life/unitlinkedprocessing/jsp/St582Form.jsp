

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "St582";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>
<%St582ScreenVars sv = (St582ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"NAV Directory");%>
	<%-- <%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To       ");%> --%>

<%{
		if (appVars.ind01.isOn()) {
			sv.navdir.setReverse(BaseScreenData.REVERSED);
			sv.navdir.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.navdir.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.navdir.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* if (appVars.ind02.isOn()) {
			sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
		} */
	}

	%>
<div class="panel panel-default">
    	<div class="panel-body">
    	
   <div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number"))%></label> 	
					<table>
					<tr>
					<td>
					<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="nameid_div" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
        
					
					</td>
					<td style="padding-left:1px;">
					 <%  
            qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
            qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
            formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
            
            if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
                    if(longValue == null || longValue.equalsIgnoreCase("")) {           
                    formatValue = formatValue( formatValue );
                    } else {
                    formatValue = formatValue( longValue );
                    }
            }
    
            if(!formatValue.trim().equalsIgnoreCase("")) {
        %>
                <div class="output_cell">   
                    <%=XSSFilter.escapeHtml(formatValue)%>
                </div>
        <%
            } else {
        %>
        
                <div class="blank_cell" />
        
        <% 
            }
            
            longValue = null;
            formatValue = null;
        %>
					</td>
					</tr></table></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year"))%></label> 	
					<table>
					<tr>
					<td>
					<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" />
		
		<% 
			} 
		%>
					</td>
					<td style="padding-left:1px;">
					<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" />
		
		<% 
			} 
		%>
					</td>
					</tr></table></div></div>
					<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label> 
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width:80px"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>

					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>	
					
					</div></div>
					
					
	</div>
	 <div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Job Queue"))%></label>
					 <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>'>
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<div class="input-group" style="min-width:80px;">
					<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   		</div>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
					
					</div></div>
					<div class="col-md-4">
				<div class="form-group">
				<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
					<div class="input-group" style="min-width:80px;">
					<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   		</div>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
					
					</div></div>
					
					
	 </div> 	
	 <div class="row">
			<div class="col-md-10">
				<div class="form-group">
				<%if ((new Byte((sv.navdir).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("NAV Directory"))%></label>
					<%}else{ %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Branch Code Upload Directory"))%></label>
					<%} %>
					<input name='navdir' type='text'

<%

		formatValue = (sv.navdir.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.navdir.getLength()%>'
maxLength='<%= sv.navdir.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(navdir)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.navdir).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.navdir).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.navdir).getColor()== null  ? 
			"input_cell" :  (sv.navdir).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					
					
					</div></div></div>
					
					
					
</div></div>

<script>
       $(document).ready(function() {
    	   $("#nameid_div").width('84')
    	   
    	  
          
       });
</script>  



<%@ include file="/POLACommon2NEW.jsp"%>

