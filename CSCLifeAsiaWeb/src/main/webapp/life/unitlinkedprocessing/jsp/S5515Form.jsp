

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5515";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5515ScreenVars sv = (S5515ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Type (D,U) ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Currency ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Management Charge (%) ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Unit Types  (I,A,B) ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Price change tolerance (%) ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Initial");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Accumulation");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Rounding Of Bid/Offer ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bid/Offer Differential ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Initial and Accumulation Units to have same details Y/N ? ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Switch Basis - Bid to Bid ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bid to Offer ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bid to Discounted Offer ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Discount % ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Offer to Offer ");
%>

<%
	{
		if (appVars.ind06.isOn()) {
			sv.currcode.setReverse(BaseScreenData.REVERSED);
			sv.currcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.currcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.managementCharge.setReverse(BaseScreenData.REVERSED);
			sv.managementCharge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.managementCharge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.unitType.setReverse(BaseScreenData.REVERSED);
			sv.unitType.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.unitType.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.tolerance.setReverse(BaseScreenData.REVERSED);
			sv.tolerance.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.tolerance.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.initialRounding.setReverse(BaseScreenData.REVERSED);
			sv.initialRounding.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.initialRounding.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.accumRounding.setReverse(BaseScreenData.REVERSED);
			sv.accumRounding.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.accumRounding.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.initBidOffer.setReverse(BaseScreenData.REVERSED);
			sv.initBidOffer.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.initBidOffer.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.acumbof.setReverse(BaseScreenData.REVERSED);
			sv.acumbof.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.acumbof.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.initAccumSame.setReverse(BaseScreenData.REVERSED);
			sv.initAccumSame.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.initAccumSame.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.btobid.setReverse(BaseScreenData.REVERSED);
			sv.btobid.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.btobid.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.btooff.setReverse(BaseScreenData.REVERSED);
			sv.btooff.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.btooff.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.btodisc.setReverse(BaseScreenData.REVERSED);
			sv.btodisc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.btodisc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.otoff.setReverse(BaseScreenData.REVERSED);
			sv.otoff.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.otoff.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.discountOfferPercent.setReverse(BaseScreenData.REVERSED);
			sv.discountOfferPercent.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.discountOfferPercent.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


</td><td>






						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("From")%></label>
					<table>
						<tr>
							<td style="width:100px;">
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td style="width:100px;">
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Type (D,U)")%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.zfundtyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.zfundtyp.getFormData()).toString()).trim().equalsIgnoreCase("D")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Interest Bearing Deposit Fund");
								}
								if (((sv.zfundtyp.getFormData()).toString()).trim().equalsIgnoreCase("U")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Unit Linked Fund");
								}
						%>

						<%
							if ((new Byte((sv.zfundtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.zfundtyp).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 210px;">
							<%
								}
							%>

							<select name='zfundtyp' style="width: 210px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(zfundtyp)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.zfundtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.zfundtyp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="D"
									<%if (((sv.zfundtyp.getFormData()).toString()).trim().equalsIgnoreCase("D")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Interest Bearing Deposit Fund")%></option>
								<option value="U"
									<%if (((sv.zfundtyp.getFormData()).toString()).trim().equalsIgnoreCase("U")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Unit Linked Fund")%></option>


							</select>
							<%
								if ("red".equals((sv.zfundtyp).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Currency")%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "currcode" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("currcode");
							optionValue = makeDropDownList(mappedItems, sv.currcode.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.currcode.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.currcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.currcode).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='currcode' type='list' style="width: 170px;"
								<%if ((new Byte((sv.currcode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.currcode).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.currcode).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Management Charge (%)")%></label>
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.managementCharge).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='managementCharge' type='text'
							<%if ((sv.managementCharge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.managementCharge)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.managementCharge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.managementCharge)%>'
							<%}%> size='<%=sv.managementCharge.getLength() + 3%>'
							maxLength='<%=sv.managementCharge.getLength() + 1%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(managementCharge)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.managementCharge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.managementCharge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.managementCharge).getColor() == null ? "input_cell"
						: (sv.managementCharge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Unit Types  (I,A,B)")%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.unitType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("A")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("A");
								}
								if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("I")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("I");
								}
								if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("B");
								}
						%>

						<%
							if ((new Byte((sv.unitType).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitType).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 100px;">
							<%
								}
							%>

							<select name='unitType' style="width: 100px;"
								onFocus='doFocus(this)' onHelp='return fieldHelp(unitType)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.unitType).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitType).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="A"
									<%if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("A")%></option>
								<option value="I"
									<%if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("I")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("I")%></option>
								<option value="B"
									<%if (((sv.unitType.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("B")%></option>

							</select>
							<%
								if ("red".equals((sv.unitType).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 "></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Price change tolerance (%)")%></label>
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.tolerance).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='tolerance' type='text'
							<%if ((sv.tolerance).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.tolerance)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.tolerance);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.tolerance)%>' <%}%>
							size='<%=sv.tolerance.getLength() + 4%>'
							maxLength='<%=sv.tolerance.getLength() + 1%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(tolerance)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.tolerance).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tolerance).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tolerance).getColor() == null ? "input_cell"
						: (sv.tolerance).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
		<div class="col-md-4 "></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Initial")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accumulation")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rounding Of Bid/Offer")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.initialRounding).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='initialRounding' type='text'
							<%if ((sv.initialRounding).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.initialRounding)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.initialRounding);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.initialRounding)%>'
							<%}%> size='<%=sv.initialRounding.getLength() + 5%>'
							maxLength='<%=sv.initialRounding.getLength() + 1%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(initialRounding)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.initialRounding).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.initialRounding).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.initialRounding).getColor() == null ? "input_cell"
						: (sv.initialRounding).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.accumRounding).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='accumRounding' type='text'
							<%if ((sv.accumRounding).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.accumRounding)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.accumRounding);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.accumRounding)%>'
							<%}%> size='<%=sv.accumRounding.getLength() + 5%>'
							maxLength='<%=sv.accumRounding.getLength() + 1%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(accumRounding)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.accumRounding).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.accumRounding).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.accumRounding).getColor() == null ? "input_cell"
						: (sv.accumRounding).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid/Offer Differential")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.initBidOffer).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='initBidOffer' type='text'
							<%if ((sv.initBidOffer).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.initBidOffer)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.initBidOffer);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.initBidOffer)%>' <%}%>
							size='<%=sv.initBidOffer.getLength()%>'
							maxLength='<%=sv.initBidOffer.getLength() + 1%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(initBidOffer)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.initBidOffer).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.initBidOffer).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.initBidOffer).getColor() == null ? "input_cell"
						: (sv.initBidOffer).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 120px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.acumbof).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='acumbof' type='text'
							<%if ((sv.acumbof).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.acumbof)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.acumbof);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.acumbof)%>' <%}%>
							size='<%=sv.acumbof.getLength()%>'
							maxLength='<%=sv.acumbof.getLength() + 1%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(acumbof)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.acumbof).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.acumbof).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.acumbof).getColor() == null ? "input_cell"
						: (sv.acumbof).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Initial and Accumulation Units to have same details Y/N ?")%></label>
				</div>
			
				<div class="input-group">
			
						<input type='checkbox' name='initAccumSame' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(initAccumSame)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.initAccumSame).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.initAccumSame).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.initAccumSame).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck'
							onclick="handleCheckBox('initAccumSame')" /> <input
							type='checkbox' name='initAccumSame' value='N'
							<%if (!(sv.initAccumSame).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('initAccumSame')" />
					</div>
				
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Switch Basis")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Switch Basis - Bid to Bid")%></label>
					<input type='checkbox' name='btobid' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(btobid)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.btobid).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.btobid).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.btobid).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('btobid')" />

					<input type='checkbox' name='btobid' value='N'
						<%if (!(sv.btobid).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('btobid')" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid to Offer")%></label>
					<input type='checkbox' name='btooff' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(btooff)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.btooff).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.btooff).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.btooff).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('btooff')" />

					<input type='checkbox' name='btooff' value='N'
						<%if (!(sv.btooff).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('btooff')" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid to Discounted Offer")%></label>
					<input type='checkbox' name='btodisc' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(btodisc)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.btodisc).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.btodisc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.btodisc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('btodisc')" />

					<input type='checkbox' name='btodisc' value='N'
						<%if (!(sv.btodisc).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('btodisc')" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discount %")%></label>
					<div style="width: 90px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.discountOfferPercent).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='discountOfferPercent' type='text'
							<%if ((sv.discountOfferPercent).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.discountOfferPercent)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.discountOfferPercent);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.discountOfferPercent)%>'
							<%}%> size='<%=sv.discountOfferPercent.getLength()%>'
							maxLength='<%=sv.discountOfferPercent.getLength() + 1%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(discountOfferPercent)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.discountOfferPercent).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.discountOfferPercent).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.discountOfferPercent).getColor() == null ? "input_cell"
						: (sv.discountOfferPercent).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Offer to Offer")%></label>
					<input type='checkbox' name='otoff' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(otoff)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.otoff).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.otoff).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.otoff).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('otoff')" />

					<input type='checkbox' name='otoff' value='N'
						<%if (!(sv.otoff).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('otoff')" />
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
