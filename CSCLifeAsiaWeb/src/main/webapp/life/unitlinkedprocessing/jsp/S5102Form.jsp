<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5102";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5102ScreenVars sv = (S5102ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract No    ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cmpnt  ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life           ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Owner          ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"RCD            ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status   ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid-to-date   ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to-date     ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "CCY ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Number  ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to 1");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policies in Plan   ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payment No   ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"-----------------------------------------------------------------------------");
%>
<%
	StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Withdrawal Type ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Withdrawal Status   ");
%>
<%
	StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payment Method  ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Frequency           ");
%>
<%
	StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payee           ");
%>
<%
	StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Withdrawal %    ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Destination Key     ");
%>
<%
	StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Withdrawal Amount  ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Paid to Date  ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Estim. value  ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payment Currency   ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Approval Date       ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date  ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Review Date         ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"First Payment Date ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Last Paid Date      ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Next Payment Date  ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Anniversary Date    ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Final Payment Date ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cancellation Date   ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Follow Ups      ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bank Account        ");
%>

<%
	{
		if (appVars.ind50.isOn()) {
			sv.plansfx.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			generatedText10.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind50.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.rgpymop.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind34.isOn()) {
			sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.rgpymop.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.rgpymop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.regpayfreq.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind35.isOn()) {
			sv.regpayfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.regpayfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.regpayfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.payclt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
			sv.payclt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.payclt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.payclt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.prcnt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.prcnt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind60.isOn()) {
			sv.prcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.prcnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.destkey.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind37.isOn()) {
			sv.destkey.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.destkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.destkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.pymt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.pymt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.pymt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.pymt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.claimcur.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind75.isOn()) {
			sv.claimcur.setReverse(BaseScreenData.REVERSED);
			sv.claimcur.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.claimcur.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.crtdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.crtdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.crtdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.crtdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.revdteDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
			sv.revdteDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.revdteDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.revdteDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.firstPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.firstPaydateDisp.setReverse(BaseScreenData.REVERSED);
			sv.firstPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.firstPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind76.isOn()) {
			sv.nextPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind40.isOn()) {
			sv.nextPaydateDisp.setReverse(BaseScreenData.REVERSED);
			sv.nextPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.nextPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.anvdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind42.isOn()) {
			sv.anvdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.anvdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.anvdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.finalPaydateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind43.isOn()) {
			sv.finalPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.finalPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.finalPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind31.isOn()) {
			sv.cancelDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
	}
%>
<!-- ILIFE-2590 Life Cross Browser -Coding and UT- Sprint 2 D4: Task 6  starts -->
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
	.iconpos {
		margin-bottom: 1px
	}
	.blank_cell {
		margin-top: 1px;
	}
}
</style>
<!-- ILIFE-2590 Life Cross Browser -Coding and UT- Sprint 2 D4: Task 6 ends -->


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 100px !important;
	text-align: left;
}
</style>

<script>
$(document).ready(function(){

	$("div[style*='background-color: rgb(238, 238, 238)']").each(function(){
		
		if($(this).text().replace(/(^\s+|\s+$)/g, "").length > 10  && $(this).text().replace(/(^\s+|\s+$)/g, "").length <= 18 ){
			$(this).css('width','88px');//reduce size amount
		}
	});
    
  });	 

</script>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					</label>
					<%
						}
					%>
					 <table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%>
									</td><td>
							<%=smartHF.getHTMLVarExt(fw, sv.cnttype, 2)%>
							  
				  		</td><td style="max-width:160px;">
									<%=smartHF.getHTMLVarExt(fw, sv.ctypedes, 2)%>
									</td></tr></table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("CCY")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.currcd, true)%>
									</td><td>
									<%=smartHF.getHTMLVarExt(fw, sv.currds, 2)%>
									</td></tr></table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.rstate, true)%>
									</td><td>
									<%=smartHF.getHTMLVarExt(fw, sv.pstate, 2)%>
									</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.lifcnum, true)%>
									</td><td>
									<%=smartHF.getHTMLVarExt(fw, sv.linsname, 2)%>
									</td></tr></table>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%>
									</td><td>
									<%=smartHF.getHTMLVarExt(fw, sv.ownername, 2)%>
									</td></tr></table>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText36).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Payee")%>
					</label>
					<%
						}
					%>
					<table><tr>
					<%-- <td><%=smartHF.getHTMLVar(0, 0, fw, sv.payclt, true)%></td> --%>
					<td>
					
							<%
						          if ((new Byte((sv.payclt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						                                                      || fw.getVariables().isScreenProtected()) {
								%>                   
								
									<%=smartHF.getHTMLVarExt(fw, sv.payclt)%>			
						                                         
						        </div>
						        <%
									} else {
								%>
						        <div class="input-group" style="width: 150px;">
						            <%=smartHF.getRichTextInputFieldLookup(fw, sv.payclt)%>
						             <span class="input-group-btn">
						             <button class="btn btn-info" type="button"
						                onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
						                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						            </button>
						        </div>
						        <%
						         }
						        %> 
				
						 </td>
							 <td style="width:100px;">
								<%=smartHF.getHTMLVarExt(fw, sv.payenme, 2)%>
							</td>
							</tr>
							</table>
							
						</div>
						
					</div>
				</div>
			
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td >
						<%
							if ((new Byte((sv.plansfx).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.plansfx).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
								formatValue = smartHF.getPicFormatted(qpsf, sv.plansfx);

								if (!((sv.plansfx.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="width: 70px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 70px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
						</td>
					
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<td> &nbsp;  &nbsp; </td>
					<td><label><%=resourceBundleHandler.gettingValueFromBundle("to 1")%>
					</label></td>
					<%
						}
					%>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.polinc).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
								formatValue = smartHF.getPicFormatted(qpsf, sv.polinc);

								if (!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Payment No")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.rgpynum).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
								formatValue = smartHF.getPicFormatted(qpsf, sv.rgpynum);

								if (!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Cmpnt")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtable.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtable.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			
				 <!-- ILJ-48 Starts -->
			<% if (sv.iljCntDteFlag.compareTo("Y") == 0){ %>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>

                    <%
                        if (!((sv.riskcommdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 85px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
            <%} %>
			<!-- ILJ-48 End -->
		</div>
		
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText34).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Withdrawal Type")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.rgpytypesd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.rgpytypesd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rgpytypesd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rgpytypesd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Withdrawal Status")%>
					</label>
					<%
						}
					%>
					 <table><tr><td>
	                      		<%=smartHF.getHTMLVar(0, 0, fw, sv.rgpystat, true)%>
									</td><td>
							<%=smartHF.getHTMLVarExt(fw, sv.statdsc, 2)%>
							  
				  		
									</td></tr></table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Destination Key")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.destkey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<input name='destkey' type='text'
							<%if ((sv.destkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.destkey.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.destkey.getLength()%>'
							maxLength='<%=sv.destkey.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(destkey)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.destkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.destkey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.destkey).getColor() == null ? "input_cell"
							: (sv.destkey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%>
					</label>
					<%
						}
					%>
					<table>
						<tr >
							<td>
								<%
									if ((new Byte((sv.rgpymop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "rgpymop" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("rgpymop");
										optionValue = makeDropDownList(mappedItems, sv.rgpymop.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.rgpymop).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 210px;  margin-left:-2px;">
									<%
										}
									%>

									<select name='rgpymop' type='list' style="width: 210px;  margin-left:-2px;"
										<%if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.rgpymop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.rgpymop).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.rgpyshort).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.rgpyshort.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rgpyshort.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.rgpyshort.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width:70px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
					</label>
					<%
						}
					%>
					<table>
						<tr class="input-group three-controller">
							<td>
								<%
									if ((new Byte((sv.regpayfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "regpayfreq" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("regpayfreq");
										optionValue = makeDropDownList(mappedItems, sv.regpayfreq.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.regpayfreq.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.regpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.regpayfreq).getColor())) {
 %>
								<div
									style="border: 2px; border-style: solid; border-color: #ec7572 !important; width: 143px;height: 32px">
									<%
										}
									%>

									<select name='regpayfreq' type='list' style="width: 140px;"
										<%if ((new Byte((sv.regpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.regpayfreq).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.regpayfreq).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.frqdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.frqdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.frqdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' 
									style="    width: 100px !important;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
					</label>
					<%
						}
					%>
					<table>
						<tr >
							<td>
								<%
									if ((new Byte((sv.claimcur).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "claimcur" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("claimcur");
										optionValue = makeDropDownList(mappedItems, sv.claimcur.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.claimcur.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.claimcur).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div 
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.claimcur).getColor())) {
 %>
								<div
									style="border: 2px; border-style: solid; border-color: #B55050; width: 173px;">
									<%
										}
									%>

									<select name='claimcur' type='list' style="width: 170px;"
										<%if ((new Byte((sv.claimcur).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.claimcur).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell' <%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.claimcur).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 	}
 %>
							</td>
							<td style="width: 100px;">
								
							<%if ((new Byte((sv.clmcurdsc).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.clmcurdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Withdrawal %")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.prcnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						%>

						<input name='prcnt' type='text'
							<%if ((sv.prcnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.prcnt)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.prcnt);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.prcnt)%>' <%}%>
							size='<%=sv.prcnt.getLength()%>'
							maxLength='<%=sv.prcnt.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(prcnt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.prcnt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.prcnt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.prcnt).getColor() == null ? "input_cell"
							: (sv.prcnt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Withdrawal Amount")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.pymt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						%>

						<input name='pymt' type='text'
							<%if ((sv.pymt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.pymt)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.pymt);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.pymt)%>' <%}%>
							size='<%=sv.pymt.getLength()%>'
							maxLength='<%=sv.pymt.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(pymt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.pymt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.pymt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.pymt).getColor() == null ? "input_cell"
							: (sv.pymt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Paid to Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totamnt);

								if (!((sv.totamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Estim. value")%>
					</label>
					<%
						}
					%>
					<div style="width: 150px;">
						<%
							if ((new Byte((sv.totestamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.totestamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.totestamt);

								if (!((sv.totestamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
					</label>
					<%
						}
					%>
					
					
					
	                <% if ((new Byte((sv.crtdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                                 <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp)%>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="crtdateDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp, (sv.crtdateDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
					
					
					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%
							if ((new Byte((sv.crtdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								longValue = sv.crtdateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.crtdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='crtdateDisp' type='text'
							value='<%=sv.crtdateDisp.getFormData()%>'
							maxLength='<%=sv.crtdateDisp.getLength()%>'
							size='<%=sv.crtdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(crtdateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.crtdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.crtdateDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.crtdateDisp).getColor() == null ? "input_cell"
								: (sv.crtdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
								}
							}
						%>

					</div> --%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Approval Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.aprvdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.aprvdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.aprvdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.aprvdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Review Date")%>
					</label>
					<%
						}
					%>
					
					
					   <% if ((new Byte((sv.revdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                       <div style="width: 80px;"><%=smartHF.getRichTextDateInput(fw, sv.revdteDisp)%></div>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="revdteDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp, (sv.revdteDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%>
					</label>
					<%
						}
					%>
					
					
					
					  <% if ((new Byte((sv.firstPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                                 <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp)%>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="firstPaydateDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp, (sv.firstPaydateDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
				
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Last Paid Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.lastPaydateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lastPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lastPaydateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lastPaydateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Next Payment Date")%>
					</label>
					<%
						}
					%>
					
					
					
					<% if ((new Byte((sv.nextPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                   <div style="width: 80px;"><%=smartHF.getRichTextDateInput(fw, sv.nextPaydateDisp)%></div>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="nextPaydateDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.nextPaydateDisp, (sv.nextPaydateDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
				
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%>
					</label>
					<%
						}
					%>
					
					
					
					<% if ((new Byte((sv.anvdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                      <div style="width: 80px"><%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp)%></div>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp, (sv.anvdateDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
					
					
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%>
					</label>
					<%
						}
					%>
					
					
					
					
					<% if ((new Byte((sv.finalPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                                       || fw.getVariables().isScreenProtected()) {       %>
	                   <div style="width: 80px"><%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp)%></div>
	                                      
	                               
	                <%}else{%>
	                           <div class="input-group date form_date col-md-12" data-date=""
	                                  data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp"
	                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
	                                         <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp, (sv.finalPaydateDisp.getLength()))%>
	                                         <span class="input-group-addon">
	                                         <span class="glyphicon glyphicon-calendar"></span>
	                                         </span>
	                           </div>
	                                  
	             	 <%}%>
					
				
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Cancellation Date")%>
					</label>
					<%
						}
					%>
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.cancelDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cancelDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cancelDateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cancelDateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<div id='mainForm_OPTS' >


<%

if(!(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="margin-left: 40px !important;">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>    

</a>
</div>	
</div>
<%} %>

<div>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg
.getFormData()%>">
</div>

<%

if(!(sv.ddind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="margin-left: 40px !important;">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%>


</a>
</div>	
</div>
<%} %>

<div>
<input name='ddind' id='ddind' type='hidden'  value="<%=sv.ddind
.getFormData()%>">
</div>

</div>




<%@ include file="/POLACommon2NEW.jsp"%>

