<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5535";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5535ScreenVars sv = (S5535ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"After Durations ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Extra");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "%");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Alloc ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium/Units ");
%>

<%
	{
		if (appVars.ind32.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAfterDur01.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitAfterDur01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.unitExtPcAlloc01.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.unitExtPcAlloc01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.percOrAmtInd01.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.percOrAmtInd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.unitAfterDur02.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.unitAfterDur02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.unitExtPcAlloc02.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitExtPcAlloc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.percOrAmtInd02.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.percOrAmtInd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.unitAfterDur03.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.unitAfterDur03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.unitExtPcAlloc03.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.unitExtPcAlloc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.percOrAmtInd03.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.percOrAmtInd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.unitAfterDur04.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitAfterDur04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.unitExtPcAlloc04.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitExtPcAlloc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.percOrAmtInd04.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.percOrAmtInd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.unitAfterDur05.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.unitAfterDur05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.unitExtPcAlloc05.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.unitExtPcAlloc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.percOrAmtInd05.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.percOrAmtInd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.unitAfterDur06.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitAfterDur06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.unitExtPcAlloc06.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.unitExtPcAlloc06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.percOrAmtInd06.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.percOrAmtInd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.unitAfterDur07.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.unitAfterDur07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.unitExtPcAlloc07.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.unitExtPcAlloc07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.percOrAmtInd07.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.percOrAmtInd07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.unitAfterDur08.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.unitAfterDur08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.unitExtPcAlloc08.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.unitExtPcAlloc08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.percOrAmtInd08.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.percOrAmtInd08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.unitAfterDur09.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.unitAfterDur09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.unitExtPcAlloc09.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.unitExtPcAlloc09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.percOrAmtInd09.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.percOrAmtInd09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.unitAfterDur10.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.unitAfterDur10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.unitExtPcAlloc10.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.unitExtPcAlloc10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.percOrAmtInd10.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.percOrAmtInd10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.unitAfterDur11.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.unitAfterDur11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.unitExtPcAlloc11.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.unitExtPcAlloc11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.percOrAmtInd11.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.percOrAmtInd11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.unitAfterDur12.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitAfterDur12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.unitExtPcAlloc12.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.unitExtPcAlloc12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.percOrAmtInd12.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.percOrAmtInd10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind13.isOn()) {
			sv.unitAfterDur13.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.unitAfterDur13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.unitExtPcAlloc13.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.unitExtPcAlloc13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.percOrAmtInd13.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.percOrAmtInd13.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind14.isOn()) {
			sv.unitAfterDur14.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitAfterDur14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.unitExtPcAlloc14.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.unitExtPcAlloc14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.percOrAmtInd14.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.percOrAmtInd10.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind15.isOn()) {
			sv.unitAfterDur15.setReverse(BaseScreenData.REVERSED);
			sv.unitAfterDur15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.unitAfterDur15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.unitExtPcAlloc15.setReverse(BaseScreenData.REVERSED);
			sv.unitExtPcAlloc15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.unitExtPcAlloc15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.percOrAmtInd15.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmtInd15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.percOrAmtInd15.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group three-controller" style ="max-width:300px">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("After Durations")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Extra")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium/Units")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur01' type='text'
							<%if ((sv.unitAfterDur01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur01)%>'
							<%}%> size='<%=sv.unitAfterDur01.getLength()%>'
							maxLength='<%=sv.unitAfterDur01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur01).getColor() == null ? "input_cell"
						: (sv.unitAfterDur01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc01' type='text'
							<%if ((sv.unitExtPcAlloc01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc01)%>'
							<%}%> size='<%=sv.unitExtPcAlloc01.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc01.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc01).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd01' type='text'
							<%if ((sv.percOrAmtInd01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd01.getLength()%>'
							maxLength='<%=sv.percOrAmtInd01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd01).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur02' type='text'
							<%if ((sv.unitAfterDur02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur02)%>'
							<%}%> size='<%=sv.unitAfterDur02.getLength()%>'
							maxLength='<%=sv.unitAfterDur02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur02).getColor() == null ? "input_cell"
						: (sv.unitAfterDur02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc02' type='text'
							<%if ((sv.unitExtPcAlloc02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc02)%>'
							<%}%> size='<%=sv.unitExtPcAlloc02.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc02.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc02).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd02' type='text'
							<%if ((sv.percOrAmtInd02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd02.getLength()%>'
							maxLength='<%=sv.percOrAmtInd02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd02).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur03' type='text'
							<%if ((sv.unitAfterDur03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur03)%>'
							<%}%> size='<%=sv.unitAfterDur03.getLength()%>'
							maxLength='<%=sv.unitAfterDur03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur03).getColor() == null ? "input_cell"
						: (sv.unitAfterDur03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc03' type='text'
							<%if ((sv.unitExtPcAlloc03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc03)%>'
							<%}%> size='<%=sv.unitExtPcAlloc03.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc03.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc03).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd03' type='text'
							<%if ((sv.percOrAmtInd03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd03.getLength()%>'
							maxLength='<%=sv.percOrAmtInd03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd03).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur04' type='text'
							<%if ((sv.unitAfterDur04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur04)%>'
							<%}%> size='<%=sv.unitAfterDur04.getLength()%>'
							maxLength='<%=sv.unitAfterDur04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur04).getColor() == null ? "input_cell"
						: (sv.unitAfterDur04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc04' type='text'
							<%if ((sv.unitExtPcAlloc04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc04)%>'
							<%}%> size='<%=sv.unitExtPcAlloc04.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc04.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc04).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd04' type='text'
							<%if ((sv.percOrAmtInd04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd04.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd04.getLength()%>'
							maxLength='<%=sv.percOrAmtInd04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd04).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur05' type='text'
							<%if ((sv.unitAfterDur05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur05)%>'
							<%}%> size='<%=sv.unitAfterDur05.getLength()%>'
							maxLength='<%=sv.unitAfterDur05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur05).getColor() == null ? "input_cell"
						: (sv.unitAfterDur05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc05' type='text'
							<%if ((sv.unitExtPcAlloc05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc05)%>'
							<%}%> size='<%=sv.unitExtPcAlloc05.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc05.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc05).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd05' type='text'
							<%if ((sv.percOrAmtInd05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd05.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd05.getLength()%>'
							maxLength='<%=sv.percOrAmtInd05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd05).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur06' type='text'
							<%if ((sv.unitAfterDur06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur06)%>'
							<%}%> size='<%=sv.unitAfterDur06.getLength()%>'
							maxLength='<%=sv.unitAfterDur06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur06).getColor() == null ? "input_cell"
						: (sv.unitAfterDur06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc06' type='text'
							<%if ((sv.unitExtPcAlloc06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc06)%>'
							<%}%> size='<%=sv.unitExtPcAlloc06.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc06.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc06).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd06' type='text'
							<%if ((sv.percOrAmtInd06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd06.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd06.getLength()%>'
							maxLength='<%=sv.percOrAmtInd06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd06).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur07' type='text'
							<%if ((sv.unitAfterDur07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur07)%>'
							<%}%> size='<%=sv.unitAfterDur07.getLength()%>'
							maxLength='<%=sv.unitAfterDur07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur07).getColor() == null ? "input_cell"
						: (sv.unitAfterDur07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc07' type='text'
							<%if ((sv.unitExtPcAlloc07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc07)%>'
							<%}%> size='<%=sv.unitExtPcAlloc07.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc07.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc07).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd07' type='text'
							<%if ((sv.percOrAmtInd07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd07.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd07.getLength()%>'
							maxLength='<%=sv.percOrAmtInd07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd07).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur08' type='text'
							<%if ((sv.unitAfterDur08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur08)%>'
							<%}%> size='<%=sv.unitAfterDur08.getLength()%>'
							maxLength='<%=sv.unitAfterDur08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur08).getColor() == null ? "input_cell"
						: (sv.unitAfterDur08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc08' type='text'
							<%if ((sv.unitExtPcAlloc08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc08)%>'
							<%}%> size='<%=sv.unitExtPcAlloc08.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc08.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc08).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd08' type='text'
							<%if ((sv.percOrAmtInd08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd08.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd08.getLength()%>'
							maxLength='<%=sv.percOrAmtInd08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd08).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur09' type='text'
							<%if ((sv.unitAfterDur09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur09)%>'
							<%}%> size='<%=sv.unitAfterDur09.getLength()%>'
							maxLength='<%=sv.unitAfterDur09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur09).getColor() == null ? "input_cell"
						: (sv.unitAfterDur09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc09' type='text'
							<%if ((sv.unitExtPcAlloc09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc09)%>'
							<%}%> size='<%=sv.unitExtPcAlloc09.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc09.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc09).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd09' type='text'
							<%if ((sv.percOrAmtInd09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd09.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd09.getLength()%>'
							maxLength='<%=sv.percOrAmtInd09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd09).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur10' type='text'
							<%if ((sv.unitAfterDur10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur10)%>'
							<%}%> size='<%=sv.unitAfterDur10.getLength()%>'
							maxLength='<%=sv.unitAfterDur10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur10).getColor() == null ? "input_cell"
						: (sv.unitAfterDur10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc10' type='text'
							<%if ((sv.unitExtPcAlloc10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc10)%>'
							<%}%> size='<%=sv.unitExtPcAlloc10.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc10.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc10).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd10' type='text'
							<%if ((sv.percOrAmtInd10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd10.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd10.getLength()%>'
							maxLength='<%=sv.percOrAmtInd10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd10).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur11' type='text'
							<%if ((sv.unitAfterDur11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur11)%>'
							<%}%> size='<%=sv.unitAfterDur11.getLength()%>'
							maxLength='<%=sv.unitAfterDur11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur11).getColor() == null ? "input_cell"
						: (sv.unitAfterDur11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc11' type='text'
							<%if ((sv.unitExtPcAlloc11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc11)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc11);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc11)%>'
							<%}%> size='<%=sv.unitExtPcAlloc11.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc11.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc11)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc11).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd11' type='text'
							<%if ((sv.percOrAmtInd11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd11.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd11.getLength()%>'
							maxLength='<%=sv.percOrAmtInd11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd11)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd11).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur12' type='text'
							<%if ((sv.unitAfterDur12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur12)%>'
							<%}%> size='<%=sv.unitAfterDur12.getLength()%>'
							maxLength='<%=sv.unitAfterDur12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur12).getColor() == null ? "input_cell"
						: (sv.unitAfterDur12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc12).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc12' type='text'
							<%if ((sv.unitExtPcAlloc12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc12)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc12);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc12)%>'
							<%}%> size='<%=sv.unitExtPcAlloc12.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc12.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc12)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc12).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd12' type='text'
							<%if ((sv.percOrAmtInd12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd12.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd12.getLength()%>'
							maxLength='<%=sv.percOrAmtInd12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd12)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd12).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
								<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur13).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur13' type='text'
							<%if ((sv.unitAfterDur13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur13)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur13);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur13)%>'
							<%}%> size='<%=sv.unitAfterDur13.getLength()%>'
							maxLength='<%=sv.unitAfterDur13.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur13)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur13).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur13).getColor() == null ? "input_cell"
						: (sv.unitAfterDur13).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc13).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc13' type='text'
							<%if ((sv.unitExtPcAlloc13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc13)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc13);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc13)%>'
							<%}%> size='<%=sv.unitExtPcAlloc13.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc13.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc13)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc13).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc13).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc13).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd13' type='text'
							<%if ((sv.percOrAmtInd13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd13.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd13.getLength()%>'
							maxLength='<%=sv.percOrAmtInd13.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd13)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd13).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd13).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd13).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd13).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur14).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur14' type='text'
							<%if ((sv.unitAfterDur14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur14)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur14);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur14)%>'
							<%}%> size='<%=sv.unitAfterDur14.getLength()%>'
							maxLength='<%=sv.unitAfterDur14.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur14)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur14).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur14).getColor() == null ? "input_cell"
						: (sv.unitAfterDur14).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc14).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc14' type='text'
							<%if ((sv.unitExtPcAlloc14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc14)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc14);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc14)%>'
							<%}%> size='<%=sv.unitExtPcAlloc14.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc14.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc14)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc14).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc14).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc14).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 50px;">
						<input name='percOrAmtInd14' type='text'
							<%if ((sv.percOrAmtInd14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd14.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd14.getLength()%>'
							maxLength='<%=sv.percOrAmtInd14.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd14)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd14).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd14).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd14).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd14).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitAfterDur15).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitAfterDur15' type='text'
							<%if ((sv.unitAfterDur15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur15)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitAfterDur15);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitAfterDur15)%>'
							<%}%> size='<%=sv.unitAfterDur15.getLength()%>'
							maxLength='<%=sv.unitAfterDur15.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(unitAfterDur15)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitAfterDur15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitAfterDur15).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitAfterDur15).getColor() == null ? "input_cell"
						: (sv.unitAfterDur15).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitExtPcAlloc15).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='unitExtPcAlloc15' type='text'
							<%if ((sv.unitExtPcAlloc15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc15)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc15);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitExtPcAlloc15)%>'
							<%}%> size='<%=sv.unitExtPcAlloc15.getLength()%>'
							maxLength='<%=sv.unitExtPcAlloc15.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitExtPcAlloc15)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.unitExtPcAlloc15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitExtPcAlloc15).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitExtPcAlloc15).getColor() == null ? "input_cell"
						: (sv.unitExtPcAlloc15).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 55px;">
						<input name='percOrAmtInd15' type='text'
							<%if ((sv.percOrAmtInd15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.percOrAmtInd15.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.percOrAmtInd15.getLength()%>'
							maxLength='<%=sv.percOrAmtInd15.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(percOrAmtInd15)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.percOrAmtInd15).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.percOrAmtInd15).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.percOrAmtInd15).getColor() == null ? "input_cell"
						: (sv.percOrAmtInd15).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>				
		<div style='visibility: hidden;'>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("%")%></label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Alloc")%></label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

