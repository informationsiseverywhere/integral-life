<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5126";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5126ScreenVars sv = (S5126ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5126screenWritten.gt(0)) {
%>
<%
	S5126screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Effective Date ");
%>
<%
	sv.effdateDisp.setClassString("");
%>
<%
	sv.effdateDisp.appendClassString("string_fld");
		sv.effdateDisp.appendClassString("output_txt");
		sv.effdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Accounting Month ");
%>
<%
	sv.acctmonth.setClassString("");
%>
<%
	sv.acctmonth.appendClassString("num_fld");
		sv.acctmonth.appendClassString("output_txt");
		sv.acctmonth.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Year ");
%>
<%
	sv.acctyear.setClassString("");
%>
<%
	sv.acctyear.appendClassString("num_fld");
		sv.acctyear.appendClassString("output_txt");
		sv.acctyear.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job Name ");
%>
<%
	sv.jobname.setClassString("");
%>
<%
	sv.jobname.appendClassString("string_fld");
		sv.jobname.appendClassString("output_txt");
		sv.jobname.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Run Id ");
%>
<%
	sv.runid.setClassString("");
%>
<%
	sv.runid.appendClassString("string_fld");
		sv.runid.appendClassString("output_txt");
		sv.runid.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Job Queue ");
%>
<%
	sv.jobq.setClassString("");
%>
<%
	sv.jobq.appendClassString("string_fld");
		sv.jobq.appendClassString("output_txt");
		sv.jobq.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Library ");
%>
<%
	sv.runlib.setClassString("");
%>
<%
	sv.runlib.appendClassString("string_fld");
		sv.runlib.appendClassString("output_txt");
		sv.runlib.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Effective Date ");
%>
<%
	sv.date_var.setClassString("");
%>
<%
	sv.date_var.appendClassString("string_fld");
		sv.date_var.appendClassString("input_txt");
		sv.date_var.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText1)%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.acctmonth, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.acctyear, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div style="width: 150px;"><%=smartHF.getHTMLVarExt(fw, sv.jobname)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.runid)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText6)%></label>
					<div style="width: 150px;"><%=smartHF.getHTMLVarExt(fw, sv.jobq)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
					<div style="width: 150px;"><%=smartHF.getHTMLVarExt(fw, sv.runlib)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label>
					<%
						if ((new Byte((sv.date_var).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.date_var)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.date_var, (sv.date_var.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5126protectWritten.gt(0)) {
%>
<%
	S5126protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>
