<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5691";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%S5691ScreenVars sv = (S5691ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Initial (Contract Issue) Fee Amount ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Periodic Fee Amount ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Administrative Charges (% of SP) ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top up fees and Admin Charges ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top up fees ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top Up Admin Charge (% of SP) ");%>
	<%
	if (appVars.ind12.isOn()) {
		sv.advfeepc.setInvisibility(BaseScreenData.INVISIBLE);
	}
	%>
<style>
.three-controller > .input-group-addon{
	max-width: 265px !important;
	text-align: left;
}
#zradmnpc, #inifeeamn, #zrtupadpc {
	width: 50% !important;
}
#perfeeamn{
	width: 70% !important;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%					
					if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%					
					if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group"  style="padding-right: 318px;">
						<%					
						if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						<%					
						if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>	
		</div>
		<%-- <div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
				<table style="width: 100%">
					<tr>
						<td>
							<div class="form-group">
							<%					
								if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=formatValue%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</div>
						</td>
						<td>
							<label style="margin-bottom: 15px !important;">&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("To")%>&nbsp;</label>
						</td>
						<td>
							<div class="form-group">
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=formatValue%>
								</div>
								<%
									longValue = null;
									formatValue = null;
								%>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div> --%>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Initial (Contract Issue) Fee Amount")%></label>
				<div class="form-group">
					<%	
					qpsf = fw.getFieldXMLDef((sv.inifeeamn).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
					valueThis=smartHF.getPicFormatted(qpsf,sv.inifeeamn,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='inifeeamn' id='inifeeamn' type='text'
						<%if ((sv.inifeeamn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.inifeeamn.getLength(), sv.inifeeamn.getScale(), 3)%>'
						maxLength='<%=sv.inifeeamn.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(inifeeamn)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.inifeeamn).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.inifeeamn).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.inifeeamn).getColor() == null ? "input_cell"
						: (sv.inifeeamn).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Periodic Fee Amount")%></label>
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.perfeeamn).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.perfeeamn,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input id='perfeeamn' name='perfeeamn' type='text'
						<%if ((sv.perfeeamn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.perfeeamn.getLength(), sv.perfeeamn.getScale(), 3)%>'
						maxLength='<%=sv.perfeeamn.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(perfeeamn)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.perfeeamn).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.perfeeamn).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.perfeeamn).getColor() == null ? "input_cell"
						: (sv.perfeeamn).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Administrative Charges (% of SP)")%></label>
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.zradmnpc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zradmnpc' id='zradmnpc' type='text'
						<%if ((sv.zradmnpc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.zradmnpc)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.zradmnpc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.zradmnpc)%>' <%}%>
						size='<%=sv.zradmnpc.getLength()%>'
						maxLength='<%=sv.zradmnpc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zradmnpc)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.zradmnpc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zradmnpc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zradmnpc).getColor() == null ? "input_cell"
						: (sv.zradmnpc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Top up fees and Admin Charges")%></label>
			</div>
		</div>	
		<br />
		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Top up fees")%></label>
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.zrtupfee).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S9VS2);
						valueThis = smartHF.getPicFormatted(qpsf, sv.zrtupfee,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zrtupfee' type='text'
						<%if ((sv.zrtupfee).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zrtupfee.getLength(), sv.zrtupfee.getScale(), 3)%>'
						maxLength='<%=sv.zrtupfee.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(zrtupfee)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.zrtupfee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zrtupfee).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zrtupfee).getColor() == null ? "input_cell"
						: (sv.zrtupfee).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Top Up Admin Charge (% of SP)")%></label>
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.zrtupadpc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='zrtupadpc' id='zrtupadpc' type='text'
						<%if ((sv.zrtupadpc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.zrtupadpc)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.zrtupadpc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.zrtupadpc)%>' <%}%>
						size='<%=sv.zrtupadpc.getLength()%>'
						maxLength='<%=sv.zrtupadpc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(zrtupadpc)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.zrtupadpc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.zrtupadpc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.zrtupadpc).getColor() == null ? "input_cell"
						: (sv.zrtupadpc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<%	if ((new Byte((sv.advfeepc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Advisor Fees %")%></label>
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.advfeepc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='advfeepc' id='advfeepc' type='text'
						<%if ((sv.advfeepc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right;width: 100px " <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.advfeepc)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.advfeepc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.advfeepc)%>' <%}%>
						size='<%=sv.advfeepc.getLength()%>'
						maxLength='<%=sv.advfeepc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advfeepc)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.advfeepc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.advfeepc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.advfeepc).getColor() == null ? "input_cell"
						: (sv.advfeepc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<%}%>
		</div>	
	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

