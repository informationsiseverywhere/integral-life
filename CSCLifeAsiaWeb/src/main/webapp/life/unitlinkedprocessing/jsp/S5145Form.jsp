

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5145";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5145ScreenVars sv = (S5145ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Switch Date     ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Percentage/Amounts/Units");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(P/A/U)");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Number   ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policies in Plan  ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Selected Component ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life No ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage No ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider No ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Type Fund  No. of Units         Est. Value");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Percent/Amount/Units");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Curr ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Fund");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Curr.)");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.percentAmountInd.setReverse(BaseScreenData.REVERSED);
			sv.percentAmountInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.percentAmountInd.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 150px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div style="width: 130px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"cntcurr"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div style="width: 120px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<div style="width: 120px;">
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div style="width: 110px;">
						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>


							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;max-width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
					<!-- </div> -->
				</div>
			</div>

			<!-- </div>

		<div class="row"> -->
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					
					<table>
						<tr>
							<td>
								<%
									if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="width: 100px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="width: 100px; margin-left: 1px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
					<!-- </div> -->
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Switch Date")%></label>
					<%-- <%	
						longValue = sv.effdateDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.effdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					<input name='effdateDisp' 
					type='text' 
					value='<%=sv.effdateDisp.getFormData()%>' 
					maxLength='<%=sv.effdateDisp.getLength()%>' 
					size='<%=sv.effdateDisp.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.effdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
					%>  
					readonly="true"
					class="output_cell"	>
					
					<%
						}else if((new Byte((sv.effdateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
					<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
					</a>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.effdateDisp).getColor()== null  ? 
					"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
					<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
					</a>
					
					<%} }%>
					<%
					longValue = null;
					%> --%>
					
					<%	
						longValue = sv.effdateDisp.getFormData();  
					%>
					<% 
						if((new Byte((sv.effdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width: 80px;">  
					   		<%if(longValue != null){%>
					   		
					   		<%=XSSFilter.escapeHtml(longValue)%>
					   		
					   		<%}%>
					</div>
				
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					longValue = null;
					%>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
					<div style="width: 80px;">
						<%	
						qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
						
						if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
							<div class="output_cell">	
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
					<%
						} else {
					%>
					
							<div class="blank_cell" > &nbsp; </div>
					
					<% 
						} 
					%>
					<%
					longValue = null;
					formatValue = null;
					%>
	
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.numpols);

							if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Percentage/Amounts/Units")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.percentAmountInd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

								if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Percentage");
								}
								if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Amounts");
								}
								if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("U")) {
									longValue = resourceBundleHandler.gettingValueFromBundle("Units");
								}
						%>

						<%
							if ((new Byte((sv.percentAmountInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="width: 100px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.percentAmountInd).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 100px;">
							<%
								}
							%>

							<select name='percentAmountInd' style="width: 100px;"
								onFocus='doFocus(this)'
								onHelp='return fieldHelp(percentAmountInd)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.percentAmountInd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.percentAmountInd).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>

								<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
								</option>
								<option value="P"
									<%if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></option>
								<option value="A"
									<%if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Amounts")%></option>
								<option value="U"
									<%if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("U")) {%>
									Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Units")%></option>

							</select>
							<%
								if ("red".equals((sv.percentAmountInd).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#component_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%></label></a>
					</li>
					<li><a href="#details_tab" data-toggle="tab"><label>
								<%=resourceBundleHandler.gettingValueFromBundle("Fund Details")%></label></a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="component_tab">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
									<div style="width: 70px;">
										<%
											if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.life.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.life.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
									<div style="width: 70px;">
										<%
											if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.coverage.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.coverage.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
									<!-- <div class="input-group three-controller"> -->
									<table>
										<tr>
											<td>
												<%
													if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.crtable.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.crtable.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
												%>
												<div
													class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
													<%=XSSFilter.escapeHtml(formatValue)%>
												</div> <%
 	longValue = null;
 	formatValue = null;
 %>


											</td>
											<td>
												<%
													if (!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
												%>
												<div
													class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
													style="margin-left: 1px;">
													<%=XSSFilter.escapeHtml(formatValue)%>
												</div> <%
 	longValue = null;
 	formatValue = null;
 %>
											</td>
										</tr>
									</table>
									<!-- </div> -->
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
									<div style="width: 70px;">
										<%
											if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rider.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rider.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end contract tab  -->

					<div class="tab-pane fade" id="details_tab">
						<%
							GeneralTable sfl = fw.getTable("s5145screensfl");
						%>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover"
										id='dataTables-s5145' width='100%'>
										<thead>
											<tr class='info'>
												<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
												<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
												<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Currency")%></center></th>
												<th><center><%=resourceBundleHandler.gettingValueFromBundle("No. of Units")%></center></th>
												<th><center><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></center></th>
												<th style="border-right-style: solid; border-width: 1px;"><center><%=resourceBundleHandler.gettingValueFromBundle(" Percentage/Amounts/Units ")%></center></th>
											</tr>
										</thead>

										<tbody>

											<%
												String backgroundcolor = "white";

												S5145screensfl.set1stScreenRow(sfl, appVars, sv);
												int count = 1;
												while (S5145screensfl.hasMoreScreenRows(sfl)) {
											%>

											<tr style="background:<%=backgroundcolor%>;">

												<td
													style="width: 80px; z-index: 8; position: relative; left: expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft +2); font-weight: bold; border-right: 2px solid #dddddd;"
													align="left"><%=formatValue(sv.unitVirtualFund.getFormData())%>



												</td>
												<td style="width: 80px; font-weight: bold;" align="left">

													<%=formatValue(sv.fndtyp.getFormData())%>



												</td>
												<td style="width: 80px; font-weight: bold;" align="left">

													<%=formatValue(sv.currcy.getFormData())%>



												</td>
												<td
													style="padding-right: 5px; width: 150px; font-weight: bold;"
													align="right">
													<%
														sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.nofDunits).getFieldName());
															//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.nofDunits,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS))%>



												</td>
												<td
													style="padding-right: 5px; width: 150px; font-weight: bold;"
													align="right">
													<%
														sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.estval).getFieldName());
															//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													%> <%=formatValue(smartHF.getPicFormatted(qpsf, sv.estval,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS))%>



												</td>
												<td
													style="width: 180px; font-weight: bold; border-right-style: solid; border-width: 1px;"
													align="right">
													<%
														sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.pcntamt).getFieldName());
															//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													%> <input type='text' maxLength='<%=sv.pcntamt.getLength()%>'
													<%if ((sv.pcntamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
													style="text-align: right" <%}%>
													value='<%=formatValue(smartHF.getPicFormatted(qpsf, sv.pcntamt,
						COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS))%>'
													size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.pcntamt.getLength(), sv.pcntamt.getScale(), 3)%>'
													onFocus='doFocus(this),onFocusRemoveCommas(this)'
													onHelp='return fieldHelp(s5145screensfl.pcntamt)'
													onKeyUp='return checkMaxLength(this)'
													name='<%="s5145screensfl" + "." + "pcntamt" + "_R" + count%>'
													class="input_cell"
													style="width: <%=sv.pcntamt.getLength() * 8%> px;"
													onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
													decimal='<%=qpsf.getDecimals()%>'
													onPaste='return doPasteNumber(event,true);'
													onBlur='return doBlurNumberNew(event,true);'>




												</td>
											</tr>


											<%
												if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
														backgroundcolor = "#E0FFFF";
													} else {
														backgroundcolor = "#FFFFFF";
													}
													count = count + 1;
													S5145screensfl.setNextScreenRow(sfl, appVars, sv);
												}
											%>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-s5145').DataTable({
			ordering : false,
			searching : false,

			paging: false,
			info: false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

