<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6326";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6326ScreenVars sv = (S6326ScreenVars) fw.getVariables();
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract no ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life no ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage no ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider no ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life assured ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Stat. fund ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub-Section ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Joint life ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sum assured ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Maturity age/term ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Maturity date ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium cess age/term ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium  date ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reserve units ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Loaded Premium ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reserve units date ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Lump Sum Amount ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Lien code ");
%>

<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Mortality Class ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life (J/L) ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total policies in plan ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Number available ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Special Terms ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Number applicable ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Reassurance   ");
%>

<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium with Tax ");
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tax Detail ");
%>
<!--ILIFE-2964 IL_BASE_ITEM-001-Base development(added Benefit Schedule)-->
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Schedule ");
%>
<%
	StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Var Annu Benefit");
%><!-- BRD-NBP-012 -->
<%
	{
		appVars.rolldown(new int[] { 27 });
		appVars.rollup(new int[] { 27 });
		if (appVars.ind44.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.sumin.setReverse(BaseScreenData.REVERSED);
			sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.matage.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.matage.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.matage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.matage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.mattrm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind15.isOn()) {
			sv.mattrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.mattrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.mattrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.mattcessDisp.setReverse(BaseScreenData.REVERSED);
			sv.mattcessDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind18.isOn()) {
			sv.mattcessDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premcessDisp.setEnabled(BaseScreenData.DISABLED);
		}

		if (!appVars.ind17.isOn()) {
			sv.mattcessDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.premcessDisp.setReverse(BaseScreenData.REVERSED);
			sv.premcessDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.premcessDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.premcessDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind19.isOn()) {
			sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.reserveUnitsInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
			sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind23.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind40.isOn()) { // vrathi4
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.singlePremium.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.singlePremium.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind26.isOn()) {
			sv.singlePremium.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.singlePremium.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind35.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind34.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.lumpContrib.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind39.isOn()) {
			sv.lumpContrib.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind38.isOn()) {
			sv.lumpContrib.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.lumpContrib.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.lumpContrib.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind42.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			sv.numavail.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind30.isOn()) {
			sv.numapp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind27.isOn()) {
			sv.numapp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind28.isOn()) {
			sv.numapp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.numapp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.numapp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind31.isOn()) {
			sv.ratypind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind33.isOn()) {
			sv.ratypind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind41.isOn()) {
			sv.ratypind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind31.isOn()) {
			sv.ratypind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.ratypind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind50.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind51.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind61.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3188
		if (appVars.ind70.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind61.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind61.isOn()) {
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		if (appVars.ind57.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.optsmode.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind57.isOn()) {
			sv.optsmode.setEnabled(BaseScreenData.DISABLED);
			sv.optsmode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.optsmode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.optsmode.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//BRD-NBP-012-STARTS
		if (appVars.ind63.isOn()) {
			sv.optind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind64.isOn()) {
			sv.optind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind64.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-012-ENDS
		
		//ILIFE-7805 - START
		if (appVars.ind65.isOn()) {
			sv.singpremtype.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind66.isOn()) {
			sv.singpremtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind65.isOn()) {
			sv.singpremtype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind67.isOn()) { 
			sv.singpremtype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind65.isOn()) {
			sv.singpremtype.setHighLight(BaseScreenData.BOLD);		
		}
		//ILIFE-7805 - END
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract no"))%></label>
					<table>
						<tr>
						<td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>

						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
					</td>
					<td>
						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width: 200px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>

				</td>
				</tr>
				</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life Assured"))%></label>
					
					<table><tr><td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
					</td>
					<td>
						<%
							if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;min-width: 65px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td></tr></table>
				
				</div>
			</div>

		<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div id='zagelit' class='label_txt' class='label_txt'
						onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

					<%
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

						if (!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 50px;" />

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint life"))%></label>
				
					<table><tr><td>
					
						<%
							if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width: 65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
					</td><td>
						<%
							}
						%>
						<%-- <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;"> --%>
						<%
							if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlinsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlinsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;min-width: 65px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
	
						<%
							}
						%>
					</td>
					</tr>
					</table>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life Number"))%></label>
					<%
						if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Number"))%></label>
					<%
						if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rider Number"))%></label>
					<%
						if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Statistical Fund"))%></label>
					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("statFund");
						longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Section"))%></label>
					<%
						if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sub-Section"))%></label>
					<%
						if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSubsect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSubsect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sum Assured"))%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='sumin' type='text'
						<%if ((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px" <%}%>
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.sumin.getLength(), sv.sumin.getScale(), 3)%>'
						maxLength='<%=sv.sumin.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(sumin)'
						onKeyUp='return checkMaxLength(this)' style="width:140px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.sumin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" width="140px"
						<%} else if ((new Byte((sv.sumin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.sumin).getColor() == null ? "input_cell"
						: (sv.sumin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>


				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Mortality class"))%></label>
					<div class="input-group">
						<%
	if ((new Byte((sv.mortcls).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
%>

<%
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>
							
	   		<%if(longValue != null){%>

	   		<%=XSSFilter.escapeHtml(longValue)%>

	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

<% if("red".equals((sv.mortcls).getColor())){
%>
<div style="border:2px; border-style: solid; border-style: solid; border-color: #ec7572 !important;
    /* width: 149px; */height: 34px">
<%
}
%>

<select name='mortcls' type='list' style="width:145px;"
<%
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
%>
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mortcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
		class="bold_cell"
<%
	}else {
%>
	class = 'input_cell'
<%
	}
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mortcls).getColor())){
%>
</div>
<%
}
%>
<%
formatValue=null;
longValue = null;
%>
<%
}}
%>
					</div>
				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Currency"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("currcd");
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
					%>


					<%
						if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currcd.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Policies in Plan"))%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.polinc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
						formatValue = smartHF.getPicFormatted(qpsf, sv.polinc);

						if (!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 40px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 40px;" />

					<%
						}
					%>


					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maturity Age/Term"))%></label>
					<table>
						<tr class="input-group three-controller">
							<td style="width:72px;">
								<%
									qpsf = fw.getFieldXMLDef((sv.matage).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								%> <input name='matage'
								<%--ILIFE-1609  --%> 
<%if ((sv.matage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%> type='text'
								value='<%=smartHF.getPicFormatted(qpsf, sv.matage)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.matage);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.matage)%>' <%}%>
								size='<%=sv.matage.getLength()%>'
								maxLength='<%=sv.matage.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(matage)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.matage).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.matage).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.matage).getColor() == null ? "input_cell"
						: (sv.matage).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' 
								<%}%>>
							</td>
							<td style="width:72px;padding-left: 2px;">
								<%
									qpsf = fw.getFieldXMLDef((sv.mattrm).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								%> <input name='mattrm'
								<%--ILIFE-1609  --%> 
<%if ((sv.mattrm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%> type='text'
								value='<%=smartHF.getPicFormatted(qpsf, sv.mattrm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.mattrm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.mattrm)%>' <%}%>
								size='<%=sv.mattrm.getLength()%>'
								maxLength='<%=sv.mattrm.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(mattrm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.mattrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.mattrm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.mattrm).getColor() == null ? "input_cell"
						: (sv.mattrm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width: 80px;">
					<label  style="white-space: nowrap;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maturity Date"))%></label>
					<%
						if ((new Byte((sv.mattcessDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<%=smartHF.getRichTextDateInput(fw, sv.mattcessDisp, (sv.mattcessDisp.getLength()))
						.replace("<pre></pre>", "")%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" style="width: 145px;"
						data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.mattcessDisp, (sv.mattcessDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<!-- vrathi4 -->
					<% if((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0){%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>				
				<%}else{%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Executive")%></label>
				
				 <%}%>
					<div class="input-group" style="width: 140px;">
						<input name='liencd' id='liencd' type='text'
							value='<%=sv.liencd.getFormData()%>'
							maxLength='<%=sv.liencd.getLength()%>'
							size='<%=sv.liencd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(liencd)'
							onKeyUp='return checkMaxLength(this)' style="width: 145px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.liencd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('liencd')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.liencd).getColor() == null ? "input_cell"
						: (sv.liencd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('liencd')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number Available"))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.numavail).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
						formatValue = smartHF.getPicFormatted(qpsf, sv.numavail);

						if (!((sv.numavail.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width:40px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width:40px;" />

					<%
						}
					%>



					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Cessation Age/Term"))%></label>
					<table>
						<tr class="input-group three-controller">
							<td style="width:72px;">
								<%
									qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								%> <input name='premCessAge'
								<%--ILIFE-1609  --%> 
<%if ((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%> type='text'
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							</td>
							<td style="width:72px;padding-left: 2px;">
								<%
									qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								%> <input name='premCessTerm'
								<%--ILIFE-1609  --%> 
<%if ((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%> type='text'
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group" style="width: 80px;">
					<label  style="white-space: nowrap;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Cessation Date"))%></label>
					<%
						if ((new Byte((sv.premcessDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>

					<%=smartHF.getRichTextDateInput(fw, sv.premcessDisp, (sv.premcessDisp.getLength()))
						.replace("<pre></pre>", "")%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" style="width: 145px;"
						data-link-field="toDateDisp" data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.premcessDisp, (sv.premcessDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Lump Sum Amount"))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.lumpContrib).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.lumpContrib,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='lumpContrib'
						<%--ILIFE-1609  --%> 
<%if ((sv.lumpContrib).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 145px;" <%}%> type='text'
						value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.lumpContrib.getLength(), sv.lumpContrib.getScale(), 3)%>'
						maxLength='<%=sv.lumpContrib.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(lumpContrib)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.lumpContrib).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.lumpContrib).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.lumpContrib).getColor() == null ? "input_cell"
						: (sv.lumpContrib).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number Applicable"))%></label>
					<div >
						<%
							qpsf = fw.getFieldXMLDef((sv.numapp).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
						%>

						<input name='numapp' style="width: 65px;"
type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.numapp);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>' <%}%>
							size='<%=sv.numapp.getLength()%>'
							maxLength='<%=sv.numapp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(numapp)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.numapp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell" style="width: 145px;"
							<%} else if ((new Byte((sv.numapp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.numapp).getColor() == null ? "input_cell"
						: (sv.numapp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>


					<%
						longValue = sv.reserveUnitsInd.getFormData();
						if ("".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Select");
						} else if ("Y".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Y");
						} else if ("N".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("N");
						}
						if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>

						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.reserveUnitsInd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
						<%
							}
						%>
						<select name='reserveUnitsInd' type='list' style="width: 100px;"
							<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>

							<option value=""
								<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
							<option value="Y"
								<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
							<option value="N"
								<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
						</select>
						<%
							if ("red".equals((sv.reserveUnitsInd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group" style="max-width: 150px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>

					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> --%>
					<%--  <div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
						data-link-format="dd/mm/yyyy" style="min-width:162px;">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
							
					</div> --%>
					
					<% 
								if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
							%>
								<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
							<% }else {%>
			                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>	
			                <%} %>	
					
				</div>
			</div>
			
			<!-- ILIFE-7805-starts -->
			<div class="col-md-3">
				<div class="form-group">
				<%
					if ((new Byte((sv.singpremtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) { 
				%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Single Premium Type"))%></label>
				<%
				 } 
				%>
					<div class="input-group">
						<%
 	if ((new Byte((sv.singpremtype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { 
	fieldItem=appVars.loadF4FieldsLong(new String[] {"singpremtype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("singpremtype");
	optionValue = makeDropDownList( mappedItems , sv.singpremtype.getFormData(),2,resourceBundleHandler);
	longValue = (String) mappedItems.get((sv.singpremtype.getFormData()).toString().trim());
%>

<%
	if((new Byte((sv.singpremtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>
							
	   		<%if(longValue != null){%>

	   		<%=XSSFilter.escapeHtml(longValue)%>

	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

<% if("red".equals((sv.singpremtype).getColor())){
%>
<div style="border:2px; border-style: solid; border-style: solid; border-color: #ec7572 !important;
    /* width: 149px; */height: 34px">
<%
}
%>

<select name='singpremtype' type='list' style="width:145px;"
<%
	if((new Byte((sv.singpremtype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
%>
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.singpremtype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
		class="bold_cell"
<%
	}else {
%>
	class = 'input_cell'
<%
	}
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.singpremtype).getColor())){
%>
</div>
<%
}
%>
<%
formatValue=null;
longValue = null;
%>
<%
}
} 
%>
					</div>
				</div>
			</div>
			<!-- ILIFE-7805-ends -->
			
		</div>

	<br>
	<hr>
	<br>
    	 					
    	<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>					
    	 <div class="row">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
    	 					
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div>  <%} %>
                        
                     <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                      <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                   <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%} %>
                        
                        
                        
                        </div>   <%} %>	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					 <div class="row">
    	 				<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>

<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

    	 					
    	 					</div></div><%} }%>	
    	 					
    	 					
    	 		<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">	
			    	     <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
			    	    			BaseScreenData.INVISIBLE)) != 0){ %>		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
    	 				<%} else { %>	
    	 				<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
    	 				<%} %>
<%if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
<%if(sv.zlinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>			
    	 					</div></div><%} %>
    	 					
    	 					<!-- ALS-7685 -starts-->
			<%
				if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
					<%
						if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
					%>
					<%
						if (sv.zstpduty01.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ",
										"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>
			<!-- ALS-7685 -ends-->
    	 					
    	 				<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
    	 				<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>

<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div><%}} %>
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					    	 					 <div class="row">
    	 					    	 					 
    	 					    	 					 
    	 					    	 					 <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
	<%
			qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='singlePremium'
<%--ILIFE-1609  --%> 
<%if((sv.singlePremium).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>

type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.singlePremium.getLength(), sv.singlePremium.getScale(),3)+2%>'
maxLength='<%= sv.singlePremium.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(singlePremium)' onKeyUp='return checkMaxLength(this)' 
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<%
	if((new Byte((sv.singlePremium).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
%>
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.singlePremium).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>
		class="bold_cell"

<%
	}else {
%>

	class = ' <%=(sv.singlePremium).getColor()== null  ?
			"input_cell" :  (sv.singlePremium).getColor().equals("red") ?
			"input_cell red reverse" : "input_cell" %>'

<%
	}
%>
>
<%
longValue = null;
formatValue = null;
%>
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					<%
if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE || sv.taxind.getEnabled() != BaseScreenData.DISABLED) {
%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='taxamt' 
type='text'

<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>


size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)-3%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
		decimal='<%=qpsf.getDecimals()%>' 
		onPaste='return doPasteNumber(event,true);'
		onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

<%
longValue = null;
formatValue = null;
%>
	

    	 					
    	 					</div></div> <% } %>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 						
    	 						
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 					<div id='' style='visibility:hidden'>

<table style="background-color: white">

<tr><style>
.sidebar {	margin-left: 0px;}
</style><td>
<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind
.getFormData()%>">
<%
if (sv.optextind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right"
style="margin-right: 120px;margin-top: 5px;">

<%}else {
	
    if (sv.optextind.getFormData().equals("X")) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optextind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optextind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right">
<%
}
%>
</td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial;" >
<%if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optextind.getEnabled() != BaseScreenData.DISABLED){%>
	<div style="height: 15px; margin-left:20px;">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></a>
  	</div>
<%} %>
</td></tr>
<!-- ////////////////// -->
<tr>
<td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial" >
<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%>
	<div style="height: 15 px; margin-left:20px;margin-top: 8px;">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%></a>
  	</div>
<%} %>
</td>
<td>
<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
<%
if (sv.taxind.getFormData().equals("+")) {
%>
<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'
	class="hyperLink"
	>
<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" style="margin-left:110px;margin-top: 10px;" >

<%}else {
	
    if (sv.taxind.getFormData().equals("X") ) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.taxind.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.taxind'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" 
style="margin-top:5px;">
<%
}
%>
</td>
</tr>
<!--ILIFE-2964 IL_BASE_ITEM-001-Base development starts-->

<tr>
<%if (sv.optsmode.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optsmode.getEnabled() != BaseScreenData.DISABLED){%><td>
<input name='optsmode' id='optsmode' type='hidden'  value="<%=sv.optsmode
.getFormData()%>">
<%
if (sv.optsmode
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right"
style="margin-right: 120px;margin-top: 5px;">

<%}else {
	
    if (sv.optsmode.getFormData().equals("X")) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optsmode
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optsmode'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<%
}
%>
</td>
<td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial">

	<div style="height: 15px">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optsmode"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%></a>
  	</div>

</td><%} %></tr>

<!--ILIFE-2964 IL_BASE_ITEM-001-Base development ends-->
<tr><td>
<input name='ratypind' id='ratypind' type='hidden'  value="<%=sv.ratypind
.getFormData()%>">
<%
if (sv.ratypind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right"
 style="margin-right: 120px;margin-top: 5px;">

<%}else {
	
    if (sv.ratypind.getFormData().equals("X") || sv.ratypind.getInvisible() == BaseScreenData.INVISIBLE
	        || sv.ratypind.getEnabled() == BaseScreenData.DISABLED) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.ratypind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ratypind'))" style="cursor:pointer;margin-right: 120px;margin-top: 5px;"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right">
<%
}
%>
</td>
<td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial">
	<%if (sv.ratypind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.ratypind.getEnabled() != BaseScreenData.DISABLED){%>

	<div style="height: 15 px">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ratypind"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%></a>
  	</div>

	<%} %>
</td></tr>
<!-- /// -->

<tr><td>
<!-- brd-nbp-012-starts -->
<%if((new Byte((sv.optind).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
<input name='optind' id='optind' type='hidden'  value="<%=sv.optind.getFormData()%>">
<%
if (sv.optind.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0" align="right" style="margin-right: 120px; margin-top: 5px">

<%}else {
	
    if (sv.optind.getFormData().equals("X") ) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind'))" style="cursor:pointer;margin-right: 120px; margin-top: 5px"
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right" >
<%
}
%>
</td>
<td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial">
<%if (sv.optind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind.getEnabled() != BaseScreenData.DISABLED){%>
	<div style="height: 15 px">
	<a href="javascript:;"
	onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind"))'
	class="hyperLink"
	>
	<%=resourceBundleHandler.gettingValueFromBundle("Var Annu Benefit")%></a>
  	</div>
<%} %>
<%} %>
</td></tr>
<!-- brd-nbp-012-ends -->
</table>


	
</div>
    	 					
    	 				<%-- <BODY style="background-color: #f8f8f8;">
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<span>
							<ul class="nav nav-second-level" aria-expanded="true">
								
								<li>
									<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind.getFormData()%>">
									<!-- text -->
									<%if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optextind.getEnabled() != BaseScreenData.DISABLED){%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
									<!-- icon --><%} %>
									<%
									 	if (sv.optextind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.optextind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
									<!-- text -->
									<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%><a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.taxind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.taxind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='optsmode' id='optsmode' type='hidden'  value="<%=sv.optsmode
.getFormData()%>">
									<%if (sv.optsmode.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optsmode.getEnabled() != BaseScreenData.DISABLED){%>
					        <a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optsmode"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.optsmode.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.optsmode.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='ratypind' id='ratypind' type='hidden'  value="<%=sv.ratypind
.getFormData()%>">
									<%if (sv.ratypind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.ratypind.getEnabled() != BaseScreenData.DISABLED){%>
	<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ratypind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.ratypind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.ratypind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
							<%if((new Byte((sv.optind).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
								<li>
									<input name='optind' id='optind' type='hidden'  value="<%=sv.optind.getFormData()%>">
									<!-- text -->
									<%if (sv.optind.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind.getEnabled() != BaseScreenData.DISABLED){%><a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Var Annu Benefit")%>
									<%} %>
									<!-- icon -->
									<%
if (sv.optind.getFormData().equals("+")) {
%>

<i class="fa fa-tasks fa-fw sidebar-icon"></i>
<%}else {
	
    if (sv.optind.getFormData().equals("X") ) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind.getFormData().equals("X")) {
%>
<i class="fa fa-warning fa-fw sidebar-icon"></i><%
}
%></a></li>
<%
}
%>
							</ul>
					</span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</BODY>			 --%>

	
			</div>
		</div>
	
		


		
		
	

<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>


<%@ include file="/POLACommon2NEW.jsp"%>