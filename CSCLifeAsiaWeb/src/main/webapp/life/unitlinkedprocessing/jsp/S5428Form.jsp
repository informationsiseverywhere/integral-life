<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5428";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5428ScreenVars sv = (S5428ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.initBarePrice.setReverse(BaseScreenData.REVERSED);
			sv.initBarePrice.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.initBarePrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.initBidPrice.setReverse(BaseScreenData.REVERSED);
			sv.initBidPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.initBidPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.initBidPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.initOfferPrice.setReverse(BaseScreenData.REVERSED);
			sv.initOfferPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.initOfferPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.initOfferPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.accBarePrice.setReverse(BaseScreenData.REVERSED);
			sv.accBarePrice.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.accBarePrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.accBidPrice.setReverse(BaseScreenData.REVERSED);
			sv.accBidPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind10.isOn()) {
			sv.accBidPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.accBidPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.accOfferPrice.setReverse(BaseScreenData.REVERSED);
			sv.accOfferPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind10.isOn()) {
			sv.accOfferPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.accOfferPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.update.setReverse(BaseScreenData.REVERSED);
			sv.update.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.update.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Initial      Units");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Accumulation  Units");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Last");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Update");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind09.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.jobno.setReverse(BaseScreenData.REVERSED);
			sv.jobno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.jobno.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 50px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div style="width: 120px;">
						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="width:70px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job No")%></label>
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.jobno).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.jobno);

							if (!((sv.jobno.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue).replace(",","") %>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[8];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.unitVirtualFund.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.unitVirtualFund.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.initBarePrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
			} else {
				calculatedValue = (sv.initBarePrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.initBidPrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.initBidPrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.initOfferPrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.initOfferPrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.accBarePrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = (sv.accBarePrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.accBidPrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 12;
			} else {
				calculatedValue = (sv.accBidPrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.accOfferPrice.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 12;
			} else {
				calculatedValue = (sv.accOfferPrice.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.update.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 12;
			} else {
				calculatedValue = (sv.update.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("s5428screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5428' width='100%'>
						<thead>
							<tr class='info'>

								<th rowspan="2" style="text-align:center;padding-bottom:20px"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
								<th colspan="3" style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Initial Units")%></th>
								<th colspan="3" style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Accumulation Units")%></th>
								<th rowspan="2" style="text-align:center;padding-bottom:20px"><%=resourceBundleHandler.gettingValueFromBundle("Last Update")%></th>
							</tr>
							<tr class='info'>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bare")%></th>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></th>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></th>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bare")%></th>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></th>
								<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								String backgroundcolor = "#FFFFFF";

								S5428screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5428screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<td style="text-align:center;"><%=sv.unitVirtualFund.getFormData()%></td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.initBarePrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.initBarePrice);
 		if (!sv.initBarePrice.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=XSSFilter.escapeHtml(formatValue)%> <%
 	longValue = null;
 		formatValue = null;
 %>




								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.initBidPrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.initBidPrice);
									%> <input type='text' maxLength='<%=sv.initBidPrice.getLength()%>'
									<%if ((sv.initBidPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=formatValue%>'
									size='<%=sv.initBidPrice.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(s5428screensfl.initBidPrice)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="s5428screensfl" + "." + "initBidPrice" + "_R" + count%>'
									id='<%="s5428screensfl" + "." + "initBidPrice" + "_R" + count%>'
									<%if ((new Byte((sv.flag).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
									disabled class="output_cell" <%} else {%>
									class="input_cell" <%}%>
									style="width: <%=sv.initBidPrice.getLength() * 12%> px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>



								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.initOfferPrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.initOfferPrice);
									%> <input type='text'
									maxLength='<%=sv.initOfferPrice.getLength()%>'
									<%if ((sv.initOfferPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=formatValue%>'
									size='<%=sv.initOfferPrice.getLength()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(s5428screensfl.initOfferPrice)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="s5428screensfl" + "." + "initOfferPrice" + "_R" + count%>'
									id='<%="s5428screensfl" + "." + "initOfferPrice" + "_R" + count%>'
									<%if ((new Byte((sv.flag).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
									disabled class="output_cell" <%} else {%>
									class="input_cell" <%}%>
									style="width: <%=sv.initOfferPrice.getLength() * 12%> px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>



								</td>
								<td style="text-align:center;">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.accBarePrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.accBarePrice);
 		if (!sv.accBarePrice.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=XSSFilter.escapeHtml(formatValue)%> <%
 	longValue = null;
 		formatValue = null;
 %>




								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.accBidPrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.accBidPrice);
									%> <input type='text' maxLength='<%=sv.accBidPrice.getLength()%>'
									<%if ((sv.accBidPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=formatValue%>'
									size='<%=sv.accBidPrice.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(s5428screensfl.accBidPrice)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="s5428screensfl" + "." + "accBidPrice" + "_R" + count%>'
									id='<%="s5428screensfl" + "." + "accBidPrice" + "_R" + count%>'
									<%if ((new Byte((sv.flag).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
									disabled class="output_cell" <%} else {%>
									class="input_cell" <%}%>
									style="width: <%=sv.accBidPrice.getLength() * 12%> px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>



								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.accOfferPrice).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.accOfferPrice);
									%> <input type='text'
									maxLength='<%=sv.accOfferPrice.getLength()%>'
									<%if ((sv.accOfferPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									style="text-align: right" <%}%> value='<%=formatValue%>'
									size='<%=sv.accOfferPrice.getLength()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(s5428screensfl.accOfferPrice)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="s5428screensfl" + "." + "accOfferPrice" + "_R" + count%>'
									id='<%="s5428screensfl" + "." + "accOfferPrice" + "_R" + count%>'
									<%if ((new Byte((sv.flag).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
									disabled style="color:#880000;" <%} else {%>
									class="input_cell" <%}%>
									style="width: <%=sv.accOfferPrice.getLength() * 12%> px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>



								</td>
								<td style="text-align:center;"><%=sv.update.getFormData()%></td>

							</tr>

							<%
								if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
										backgroundcolor = "#EEEEEE";
									} else {
										backgroundcolor = "#FFFFFF";
									}
									count = count + 1;
									S5428screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<!-- <script>
	$(document).ready(function() {
		$('#dataTables-s5428').DataTable({
			ordering : false,
			searching : false,
			paging : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,

		});
	});
</script> -->

<script>
$(document).ready(function() {
	$('#dataTables-s5428').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400px',
        scrollCollapse: true,
        
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
