<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5519";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5519ScreenVars sv = (S5519ScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company:")%></label>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.company).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table:")%></label>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item:")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if ((new Byte((sv.item).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td>
						<%
							if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td style=width:90px;><%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp, (sv.itmfrmDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
								<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute", "relative")%>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td ><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td style=width:90px;><%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp, (sv.itmtoDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
								<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute", "relative")%>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Initial Units")%></label>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("By deduction of Units:")%></label>
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.unitdeduc) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.unitdeduc, (sv.unitdeduc.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.unitdeduc) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.unitdeduc, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("By differing price:")%></label>
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.diffprice) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.diffprice, (sv.diffprice.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.diffprice) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.diffprice, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("With Unit Adjustment:")%></label>
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.unitadj) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.unitadj, (sv.unitadj.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.unitadj) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.unitadj, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annual Charge:")%></label>
					<div style="width: 120px;">
						<%
							if (((BaseScreenData) sv.annchg) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.annchg, (sv.annchg.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.annchg) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.annchg, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Initial Unit Charge Frequency:")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "initUnitChargeFreq" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("initUnitChargeFreq");
							optionValue = makeDropDownList(mappedItems, sv.initUnitChargeFreq.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.initUnitChargeFreq.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.initUnitChargeFreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell'>
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.initUnitChargeFreq).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>
							<select name='initUnitChargeFreq' type='list'
								style="width: 177px;"
								<%if ((new Byte((sv.initUnitChargeFreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.initUnitChargeFreq).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.initUnitChargeFreq).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Based upon: Fixed Term")%></label>
					<div style="width: 70px;">
						<%
							if (((BaseScreenData) sv.fixdtrm) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.fixdtrm, (sv.fixdtrm.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.fixdtrm) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.fixdtrm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Processing Subroutine:")%></label>
					<div style="width: 120px;">
						<%
							if (((BaseScreenData) sv.subprog) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.subprog, (sv.subprog.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.subprog) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.subprog, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							} else {
						%>
						hello
						<%
							}
						%>
					</div>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
