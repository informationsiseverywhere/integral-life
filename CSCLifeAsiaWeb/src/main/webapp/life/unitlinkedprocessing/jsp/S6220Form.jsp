<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6220";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S6220ScreenVars sv = (S6220ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Select");
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Date");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No");
%>
<%
	appVars.rollup(new int[]{93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[4];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.select.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.effdateDisp.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.effdateDisp.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.jobno.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.jobno.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.validtype.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = (sv.validtype.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("s6220screensfl");
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6220' width='100%'>
						<thead>
							<tr class='info'>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Date")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Job No")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Valid Type")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								S6220screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S6220screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr class="tableRowTag" id='<%="tablerow" + count%>'>
								<%
									if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td
									<%if ((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="center" <%} else {%> align="center" <%}%>><input
									type="checkbox" value='<%=sv.select.getFormData()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp("s6220screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='s6220screensfl.select_R<%=count%>'
									id='s6220screensfl.select_R<%=count%>'
									onClick="selectedRow('s6220screensfl.select_R<%=count%>')"
									class="UICheck" /></td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td
									<%if ((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.effdateDisp.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.jobno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td
									<%if ((sv.jobno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="left" <%} else {%> align="left" <%}%>><%=sv.jobno.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.validtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td
									<%if ((sv.validtype).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.validtype.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>

							</tr>

							<%
								count = count + 1;
									S6220screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-s6220').DataTable({
			ordering : false,
			searching : false,
			scrollY : "350px",
			scrollCollapse : true,
			scrollX : true,
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

