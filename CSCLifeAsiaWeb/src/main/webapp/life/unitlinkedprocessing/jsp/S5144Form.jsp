

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5144";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*, java.util.*, com.quipoz.framework.datatype.FixedLengthStringData"%>

<%
	S5144ScreenVars sv = (S5144ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Number   ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "to 1");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policies in Plan  ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bill Curr ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Selected Component ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life No ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage No ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider No ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Suspense Curr ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Ex. Rate ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date  ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"% or Amounts ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(P/A)");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Split Plan ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premium ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium for Transaction  ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Amount In Suspense ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "% / Amount");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Commission      ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prem w/tax ");
%>

<%
	{
		if (!appVars.ind16.isOn()) {
			sv.planSuffix.setColor(BaseScreenData.WHITE);
		}
		if (appVars.ind16.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			generatedText10.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind16.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.paycurr.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.paycurr.setReverse(BaseScreenData.REVERSED);
			sv.paycurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.paycurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.percentAmountInd.setReverse(BaseScreenData.REVERSED);
			sv.percentAmountInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.percentAmountInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.virtFundSplitMethod.setReverse(BaseScreenData.REVERSED);
			sv.virtFundSplitMethod.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.virtFundSplitMethod.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.virtFundSplitMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.instprem.setReverse(BaseScreenData.REVERSED);
			sv.instprem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.instprem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.unitVirtualFund01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.unitVirtualFund01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind31.isOn()) {
			sv.unitVirtualFund01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.unitAllocPercAmt01.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.unitAllocPercAmt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.unitVirtualFund02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind22.isOn()) {
			sv.unitVirtualFund02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.unitVirtualFund02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.unitAllocPercAmt02.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.unitAllocPercAmt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind23.isOn()) {
			sv.unitVirtualFund03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind33.isOn()) {
			sv.unitVirtualFund03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.unitAllocPercAmt03.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.unitAllocPercAmt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.unitVirtualFund04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.unitVirtualFund04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.unitAllocPercAmt04.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.unitAllocPercAmt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.unitVirtualFund05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind35.isOn()) {
			sv.unitVirtualFund05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.unitAllocPercAmt05.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.unitAllocPercAmt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.unitVirtualFund06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind36.isOn()) {
			sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.unitAllocPercAmt06.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.unitAllocPercAmt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund07.setColor(BaseScreenData.RED);
		}
		if (appVars.ind27.isOn()) {
			sv.unitVirtualFund07.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind37.isOn()) {
			sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.unitAllocPercAmt07.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.unitAllocPercAmt07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund08.setColor(BaseScreenData.RED);
		}
		if (appVars.ind28.isOn()) {
			sv.unitVirtualFund08.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind38.isOn()) {
			sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.unitAllocPercAmt08.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.unitAllocPercAmt08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund09.setColor(BaseScreenData.RED);
		}
		if (appVars.ind29.isOn()) {
			sv.unitVirtualFund09.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind39.isOn()) {
			sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.unitAllocPercAmt09.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.unitAllocPercAmt09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund10.setColor(BaseScreenData.RED);
		}
		if (appVars.ind30.isOn()) {
			sv.unitVirtualFund10.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind40.isOn()) {
			sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.unitAllocPercAmt10.setReverse(BaseScreenData.REVERSED);
			sv.unitAllocPercAmt10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.unitAllocPercAmt10.setHighLight(BaseScreenData.BOLD);
		}

		if (appVars.ind51.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind51.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind31.isOn()) {
			sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
				}
				
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind32.isOn()) {
			sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
				}
				
				 if(appVars.ind69.isOn()){
					sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
					sv.reserveUnitsDate.setInvisibility(BaseScreenData.INVISIBLE);
				}
	//ILIFE-8164 -STARTS			 
	 if (appVars.ind101.isOn()) {
			sv.newFundList01.setInvisibility(BaseScreenData.INVISIBLE);
			}
					
			if (appVars.ind102.isOn()) {
			sv.newFundList02.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind103.isOn()) {
				sv.newFundList03.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind104.isOn()) {
				sv.newFundList04.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind105.isOn()) {
				sv.newFundList05.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind106.isOn()) {
				sv.newFundList06.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind107.isOn()) {
				sv.newFundList07.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind108.isOn()) {
				sv.newFundList08.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind109.isOn()) {
				sv.newFundList09.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind110.isOn()) {
				sv.newFundList10.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind111.isOn()) {
				sv.newFundList11.setInvisibility(BaseScreenData.INVISIBLE);
			}
			
			if (appVars.ind112.isOn()) {
				sv.newFundList12.setInvisibility(BaseScreenData.INVISIBLE);
			}	
			//ILIFE-8164  -ENDS
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}

</style>
<div class="panel panel-default" >
	<div class="panel-body" >
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr >
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							 	longValue = null;
							 	formatValue = null;
							 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="margin-left: 2px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						 	longValue = null;
						 	formatValue = null;
						 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div 
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									 style="max-width: 150px; margin-left: 2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
							 	longValue = null;
							 	formatValue = null;
							 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div style="width: 70px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "cntcurr" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<div style="width: 120px;">
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div>
						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 115px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div style="width: 70px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div  style="margin-left: 2px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<table>
					<tr>
					<td style="width: 100px;">
						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td style="width: 100px;">

						<%
							if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div style="margin-left: 2px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
					<table>
						<tr>
							<td>
								<%
									qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

									if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue(formatValue);
										} else {
											formatValue = formatValue(longValue);
										}
									}

									if (!formatValue.trim().equalsIgnoreCase("")) {
								%>
								<div class="output_cell">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	} else {
								 %>

								<div class="blank_cell" style="width: 70px;">&nbsp;</div> <%
							 	}
							 %> <%
							 	longValue = null;
							 	formatValue = null;
							 %>
							</td>
							<td>&nbsp;</td>
							<td><label><%=resourceBundleHandler.gettingValueFromBundle("to 1")%></label></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.numpols);

							if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bill Curr")%></label>
					<div style="width: 70px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "billcurr" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("billcurr");
							longValue = (String) mappedItems.get((sv.billcurr.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.billcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.billcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Prem w/tax")%>
					</label>
					<%
						}
					%>
					<div style="width: 71px;">
						<%
							if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt);

								if (!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}
								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell">&nbsp;</div>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#component_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
						</label></a></li>
					<li><a href="#detail_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Fund Details")%></label></a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="component_tab">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
									<div style="width: 70px;">
										<%
											if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.crtable.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.crtable.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Curr")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "paycurr" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("paycurr");
											optionValue = makeDropDownList(mappedItems, sv.paycurr.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.paycurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.paycurr).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
											<%
												}
											%>

											<select name='paycurr' type='list' style="width: 170px;"
												<%if ((new Byte((sv.paycurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.paycurr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>
												<%=optionValue%>
											</select>
											<%
												if ("red".equals((sv.paycurr).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Ex. Rate")%></label>
									<div style="width: 71px;"><%=smartHF.getHTMLVarExt(fw, sv.exrat)%></div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
									<div class="input-group date form_date col-md-12" data-date=""
										data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
										data-link-format="dd/mm/yyyy" style="width: 150px;">
										<%
											longValue = sv.effdateDisp.getFormData();
										%>

										<%
											if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>
										<%
											} else {
										%>
										<input name='effdateDisp' type='text'
											value='<%=sv.effdateDisp.getFormData()%>'
											maxLength='<%=sv.effdateDisp.getLength()%>'
											size='<%=sv.effdateDisp.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(effdateDisp)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell">

										<%
											} else if ((new Byte((sv.effdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
										%>
										class="bold_cell" > <span class="input-group-addon"><span
											class="glyphicon glyphicon-calendar"></span></span>

										<%
											} else {
										%>

										class = '
										<%=(sv.effdateDisp).getColor() == null ? "input_cell"
							: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										> <span class="input-group-addon"><span
											class="glyphicon glyphicon-calendar"></span></span>

										<%
											}
											}
										%>

									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("% or Amounts")%></label>
									<div>
										<%
											if ((new Byte((sv.percentAmountInd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {

												if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
													longValue = resourceBundleHandler.gettingValueFromBundle("Percentage");
												}
												if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {
													longValue = resourceBundleHandler.gettingValueFromBundle("Amounts");
												}
										%>

										<%
											if ((new Byte((sv.percentAmountInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
										%>
										<div
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%
												}
											%>
										</div>

										<%
											longValue = null;
										%>

										<%
											} else {
										%>

										<%
											if ("red".equals((sv.percentAmountInd).getColor())) {
										%>
										<div
											style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
											<%
												}
											%>

											<select name='percentAmountInd' style="width: 140px;"
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(percentAmountInd)'
												onKeyUp='return checkMaxLength(this)'
												<%if ((new Byte((sv.percentAmountInd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
												readonly="true" disabled class="output_cell"
												<%} else if ((new Byte((sv.percentAmountInd).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell" <%} else {%> class='input_cell' <%}%>>

												<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--
												</option>
												<option value="P"
													<%if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%>
													Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></option>
												<option value="A"
													<%if (((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%>
													Selected <%}%>><%=resourceBundleHandler.gettingValueFromBundle("Amounts")%></option>


											</select>
											<%
												if ("red".equals((sv.percentAmountInd).getColor())) {
											%>
										</div>
										<%
											}
										%>

										<%
											}
												longValue = null;
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Split Plan")%></label>
									<div style="width: 200px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "virtFundSplitMethod" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("virtFundSplitMethod");
											optionValue = makeDropDownList(mappedItems, sv.virtFundSplitMethod.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.virtFundSplitMethod.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.virtFundSplitMethod, fw, longValue, "virtFundSplitMethod", optionValue)%>

									</div>
								</div>
							</div>
						</div>
	<!-- 						Single Premium Top-Up Reserve Unit -->
<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>


					<%
						longValue = sv.reserveUnitsInd.getFormData();
						if ("".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Select");
						} else if ("Y".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Y");
						} else if ("N".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("N");
						}
						if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>

						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.reserveUnitsInd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
						<%
							}
						%>
						<select name='reserveUnitsInd' type='list' style="width: 100px;"
							<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>

							<option value=""
								<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
							<option value="Y"
								<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
							<option value="N"
								<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
						</select>
						<%
							if ("red".equals((sv.reserveUnitsInd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>

<!-- 			<div class="col-md-1"></div> -->
			<div class="col-md-4">
				<div class="form-group" style="max-width: 150px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>

					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> --%>
					<%--  <div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
						data-link-format="dd/mm/yyyy" style="min-width:162px;">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
							
					</div> --%>
					
					<% 
								if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
							%>
								<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
							<% }else {%>
			                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>	
			                <%} %>	
					
				</div>
			</div>
		</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></label>
									<div style="width: 71px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											valueThis = smartHF.getPicFormatted(qpsf, sv.instprem,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='instprem' type='text'
											<%if ((sv.instprem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: right" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.instprem.getLength(), sv.instprem.getScale(), 3)%>'
											maxLength='<%=sv.instprem.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(instprem)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.instprem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.instprem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.instprem).getColor() == null ? "input_cell"
						: (sv.instprem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Premium for Transaction")%></label>
									<div style="width: 71px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.totlprem).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											formatValue = smartHF.getPicFormatted(qpsf, sv.totlprem,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

											if (!((sv.totlprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue(formatValue);
												} else {
													formatValue = formatValue(longValue);
												}
											}

											if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Total Amount In Suspense")%></label>
									<div class="input-group" style="width: 71px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.susamt).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											formatValue = smartHF.getPicFormatted(qpsf, sv.susamt,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

											if (!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue(formatValue);
												} else {
													formatValue = formatValue(longValue);
												}
											}

											if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Total Tax Amount")%></label>
									<div style="width: 71px;">
										<%
											qpsf = fw.getFieldXMLDef((sv.totTax).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.totTax, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.totTax);

											if (!((sv.totTax.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue(formatValue);
												} else {
													formatValue = formatValue(longValue);
												}
											}

											if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end contract tab  -->

					<div class="tab-pane fade" id="detail_tab">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("% / Amount")%></label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
<!-- ILIFE-8164-START-->						
								<%
						if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund01"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund01");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund01, fw, longValue, "unitVirtualFund01", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->							
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund01" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund01");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund01, fw, longValue, "unitVirtualFund01", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy01.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy01.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt01).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt01,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt01' type='text'
											<%if ((sv.unitAllocPercAmt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt01.getLength(),
					sv.unitAllocPercAmt01.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt01.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt01)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt01).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt01).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
<!-- ILIFE-8164-START-->								
								<%
						if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund02"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund02");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund02, fw, longValue, "unitVirtualFund02", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund02" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund02");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund02, fw, longValue, "unitVirtualFund02", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy02.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy02.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt02).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt02,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt02' type='text'
											<%if ((sv.unitAllocPercAmt02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt02.getLength(),
					sv.unitAllocPercAmt02.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt02.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt02)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt02).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt02).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
	<!-- ILIFE-8164-START-->								
									<%
						if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund03"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund03");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund03, fw, longValue, "unitVirtualFund03", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->							
								<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund03" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund03");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund03, fw, longValue, "unitVirtualFund03", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy03.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy03.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy03.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt03).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt03,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt03' type='text'
											<%if ((sv.unitAllocPercAmt03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt03.getLength(),
					sv.unitAllocPercAmt03.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt03.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt03)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt03).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt03).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund04"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund04");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund04, fw, longValue, "unitVirtualFund04", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->							
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund04" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund04");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund04, fw, longValue, "unitVirtualFund04", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy04.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy04.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy04.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt04).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt04,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt04' type='text'
											<%if ((sv.unitAllocPercAmt04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt04.getLength(),
					sv.unitAllocPercAmt04.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt04.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt04)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt04).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt04).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund05"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund05");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund05, fw, longValue, "unitVirtualFund05", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->							
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund05" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund05");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund05, fw, longValue, "unitVirtualFund05", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy05.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy05.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy05.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt05).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt05,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt05' type='text'
											<%if ((sv.unitAllocPercAmt05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt05.getLength(),
					sv.unitAllocPercAmt05.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt05.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt05)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt05).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt05).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund06"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund06");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund06, fw, longValue, "unitVirtualFund06", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->							
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund06" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund06");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund06, fw, longValue, "unitVirtualFund06", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy06.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy06.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy06.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt06).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt06,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt06' type='text'
											<%if ((sv.unitAllocPercAmt06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt06.getLength(),
					sv.unitAllocPercAmt06.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt06.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt06)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt06).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt06).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
<!-- ILIFE-8164-START-->								
								<%
						if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund07"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund07");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund07, fw, longValue, "unitVirtualFund07", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund07" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund07");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund07, fw, longValue, "unitVirtualFund07", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy07.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy07.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy07.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt07).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt07,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt07' type='text'
											<%if ((sv.unitAllocPercAmt07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt07.getLength(),
					sv.unitAllocPercAmt07.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt07.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt07)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt07).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt07).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund08"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund08");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund08, fw, longValue, "unitVirtualFund08", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund08" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund08");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund08, fw, longValue, "unitVirtualFund08", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy08.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy08.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy08.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt08).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt08,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt08' type='text'
											<%if ((sv.unitAllocPercAmt08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt08.getLength(),
					sv.unitAllocPercAmt08.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt08.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt08)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt08).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt08).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund09"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund09");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund09, fw, longValue, "unitVirtualFund09", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund09" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund09");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund09, fw, longValue, "unitVirtualFund09", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy09.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy09.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy09.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt09).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt09,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt09' type='text'
											<%if ((sv.unitAllocPercAmt09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt09.getLength(),
					sv.unitAllocPercAmt09.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt09.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt09)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt09).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt09).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
		<!-- ILIFE-8164-START-->						
								<%
						if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund10"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund10");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund10, fw, longValue, "unitVirtualFund10", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
									<div style="width: 210px;">
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund10" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("unitVirtualFund10");
											optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
										%>
										<!-- ILIFE-2591 Life Cross Browser - Sprint 2 D5 : Task 1  -->
										<%=smartHF.getDropDownExt(sv.unitVirtualFund10, fw, longValue, "unitVirtualFund10", optionValue)%>

									</div>
									<%}%>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div style="width: 70px;">
										<%
											if (!((sv.currcy10.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy10.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.currcy10.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div>
										<%
											qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt10).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt10,
													COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>

										<input name='unitAllocPercAmt10' type='text'
											<%if ((sv.unitAllocPercAmt10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
											style="text-align: left" <%}%> value='<%=valueThis%>'
											<%if (valueThis != null && valueThis.trim().length() > 0) {%>
											title='<%=valueThis%>' <%}%>
											size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt10.getLength(),
					sv.unitAllocPercAmt10.getScale(), 3)%>'
											maxLength='<%=sv.unitAllocPercAmt10.getLength()%>'
											onFocus='doFocus(this),onFocusRemoveCommas(this)'
											onHelp='return fieldHelp(unitAllocPercAmt10)'
											onKeyUp='return checkMaxLength(this)'
											onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
											decimal='<%=qpsf.getDecimals()%>'
											onPaste='return doPasteNumber(event,true);'
											onBlur='return doBlurNumberNew(event,true);'
											<%if ((new Byte((sv.unitAllocPercAmt10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
											readonly="true" class="output_cell"
											<%} else if ((new Byte((sv.unitAllocPercAmt10).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
											class="bold_cell" <%} else {%>
											class=' <%=(sv.unitAllocPercAmt10).getColor() == null ? "input_cell"
						: (sv.unitAllocPercAmt10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
											<%}%>>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<div id='mainForm_OPTS' style='visibility: hidden'>


	<%
		if (!(sv.comind.getInvisible() == BaseScreenData.INVISIBLE
				|| sv.comind.getEnabled() == BaseScreenData.DISABLED) || fw.getVariables().isScreenProtected()) {
	%>
	<div style="background-color: white;">
		<div id='null' style="margin-left: 35px;">
			<a href="javascript:;"
				onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))'
				class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Commission")%>


			</a>
		</div>
	</div>
	<%
		}
	%>

	<div>
		<input name='comind' id='comind' type='hidden'  value="<%=sv.comind.getFormData()%>">
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>

				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
				</div>


			</td>
			<td width='188'>

				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("(P/A)")%>
				</div>
		</tr>
	</table>
</div>









