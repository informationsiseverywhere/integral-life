

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6659";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6659ScreenVars sv = (S6659ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Indicators ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statement to be produced on anniversary");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"or payment date? (A/P) ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statement to be produced if there are");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"outstanding unit transactions? (Y/N) ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statement Frequency ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Subroutine ");%>

<%{
		if (appVars.ind10.isOn()) {
			sv.annOrPayInd.setReverse(BaseScreenData.REVERSED);
			sv.annOrPayInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.annOrPayInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.osUtrnInd.setReverse(BaseScreenData.REVERSED);
			sv.osUtrnInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.osUtrnInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.freq.setReverse(BaseScreenData.REVERSED);
			sv.freq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.freq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.subprog.setReverse(BaseScreenData.REVERSED);
			sv.subprog.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.subprog.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- ILIFE-2617 Life Cross Browser - Sprint 2 D6 : Task 5  -->
	
	<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
}

</style>
<!-- ILIFE-2617 Life Cross Browser - Sprint 2 D6 : Task 5 -->


<div class="panel panel-default">
		<div class="panel-body">
		
		<div class="row">
				<div class="col-md-1">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div>
				</div>
			
			
			<div class="col-md-3"></div>
				
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				    </div>
				 </div>   	
				 
				 <div class="col-md-1"></div>
				
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">	
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
					</div>
				</div>
			</div>
		</div>	
		
		<div class="row">	
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
				   <table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;</td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
		 </div>
		 
		 <div class="row">	
		       	<div class="col-md-4">
				<div class="form-group">
				   <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Processing Indicators"))%></label> 
				</div>
			</div>
		</div>		
		
		<div class="row">	
		       	<div class="col-md-6">
				<div class="form-group">
				   <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Statement to be produced on anniversary"))%></label> 
				   <label><%=resourceBundleHandler.gettingValueFromBundle("or payment date? (A/P)")%></label>
				   
				   <input name='annOrPayInd' 
type='text'

<%

		formatValue = (sv.annOrPayInd.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.annOrPayInd.getLength()%>'
maxLength='<%= sv.annOrPayInd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(annOrPayInd)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.annOrPayInd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"  style="max-width:80px;"
<%
	}else if((new Byte((sv.annOrPayInd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"   style="max-width:80px;"

<%
	}else { 
%>

	class = ' <%=(sv.annOrPayInd).getColor()== null  ? 
			"input_cell" :  (sv.annOrPayInd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
				   <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Statement to be produced if there are"))%></label> 
				   <label><%=resourceBundleHandler.gettingValueFromBundle("outstanding unit transactions? (Y/N)")%></label>
				   <input name='osUtrnInd' 
type='text'

<%

		formatValue = (sv.osUtrnInd.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.osUtrnInd.getLength()%>'
maxLength='<%= sv.osUtrnInd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(osUtrnInd)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.osUtrnInd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="max-width:80px;"
<%
	}else if((new Byte((sv.osUtrnInd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="max-width:80px;"

<%
	}else { 
%>

	class = ' <%=(sv.osUtrnInd).getColor()== null  ? 
			"input_cell" :  (sv.osUtrnInd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
				</div>
			</div>
		</div>
		
		 <div class="row">	
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Statement Frequency")%></label>
					
					<input name='freq' 
type='text'

<%

		formatValue = (sv.freq.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.freq.getLength()%>'
maxLength='<%= sv.freq.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(freq)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.freq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"  style="max-width:75px;"
<%
	}else if((new Byte((sv.freq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  style="max-width:75px;"

<%
	}else { 
%>

	class = ' <%=(sv.freq).getColor()== null  ? 
			"input_cell" :  (sv.freq).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					 
					</div>
				</div>
				
				<div class="col-md-3"></div>
				<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Processing Subroutine")%></label>
					
					<input name='subprog' 
type='text'

<%

		formatValue = (sv.subprog.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subprog.getLength()%>'
maxLength='<%= sv.subprog.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subprog)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.subprog).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="max-width:75px;"
<%
	}else if((new Byte((sv.subprog).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="max-width:75px;"

<%
	}else { 
%>

	class = ' <%=(sv.subprog).getColor()== null  ? 
			"input_cell" :  (sv.subprog).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					 
					</div>
				</div>
		  </div>

</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

