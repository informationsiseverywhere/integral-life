<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6520";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6520ScreenVars sv = (S6520ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Detail/");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reprint");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Stmt. Date  Summary");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Ind");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Stmt");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Originating Transaction");
%>
<%
	appVars.rollup(new int[]{93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>

					<%
						if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrnum.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>

				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Owner"))%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">

						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s6520' width='100%'>
							<thead>
								<tr class='info'>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Statement Date")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Detail/Summary")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Reprint Ind")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Reprint Statement")%></center></th>
									<th><center><%=resourceBundleHandler.gettingValueFromBundle("Originating Transaction")%></center></th>

								</tr>
							</thead>
							<tbody style="font-weight: bold;">
								<%
									GeneralTable sfl = fw.getTable("s6520screensfl");
								%>

								<%
									S6520screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S6520screensfl.hasMoreScreenRows(sfl)) {
								%>


								<%
									/* This block of jsp code is to calculate the variable width of the table at runtime.*/
										int[] tblColumnWidth = new int[5];
										int totalTblWidth = 0;
										int calculatedValue = 0;

										if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.effdateDisp.getFormData())
												.length()) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
										} else {
											calculatedValue = (sv.effdateDisp.getFormData()).length() * 12;
										}
										totalTblWidth += calculatedValue;
										tblColumnWidth[0] = calculatedValue;

										if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.stmtlevel.getFormData())
												.length()) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
										} else {
											calculatedValue = (sv.stmtlevel.getFormData()).length() * 12;
										}
										totalTblWidth += calculatedValue;
										tblColumnWidth[1] = calculatedValue;

										if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.stmtType.getFormData())
												.length()) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
										} else {
											calculatedValue = (sv.stmtType.getFormData()).length() * 12;
										}
										totalTblWidth += calculatedValue;
										tblColumnWidth[2] = calculatedValue;

										if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.ustmno.getFormData())
												.length()) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
										} else {
											calculatedValue = (sv.ustmno.getFormData()).length() * 12;
										}
										totalTblWidth += calculatedValue;
										tblColumnWidth[3] = calculatedValue;

										if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.trandesc.getFormData())
												.length()) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
										} else {
											calculatedValue = (sv.trandesc.getFormData()).length() * 12;
										}
										totalTblWidth += calculatedValue;
										tblColumnWidth[4] = calculatedValue;
								%>

								<tr class="tableRowTag" id='<%="tablerow" + count%>'>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0]%>px;" align="left"><%=sv.effdateDisp.getFormData()%>



									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[1]%>px;" align="left"><%=sv.stmtlevel.getFormData()%>



									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[2]%>px;" align="left"><%=sv.stmtType.getFormData()%>



									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[3]%>px;" align="left">
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.ustmno).getFieldName());
												qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.ustmno);
 		if (!sv.ustmno.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>




									</td>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[4]%>px;" align="left"><%=sv.trandesc.getFormData()%>



									</td>

								</tr>
								<%
									count = count + 1;
										S6520screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>

						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("table tr:nth-child(even)").addClass("striped");
	});
</script>

<script>
	$(document).ready(function() {
		$('#dataTables-s6520').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));

	});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

