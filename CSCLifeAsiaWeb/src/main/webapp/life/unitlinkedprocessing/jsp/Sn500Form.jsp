<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SN500";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%Sn500ScreenVars sv = (Sn500ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number   ");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan  ");%>
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill Curr ");%>
<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage ");%>
<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suspense Curr ");%>
<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ex. Rate ");%>
<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% or Amounts ");%>
<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(P/A)");%>
<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund Split Plan ");%>
<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium ");%>
<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium for Transaction  ");%>
<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured ");%>
<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Amount In Suspense ");%>
<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total S/A   ");%>
<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund");%>
<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% / Amount");%>
<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Top up S/A  ");%>
<%{
if (!appVars.ind16.isOn()) {
sv.planSuffix.setColor(BaseScreenData.WHITE);
}
if (appVars.ind16.isOn()) {
sv.planSuffix.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind19.isOn()) {
sv.paycurr.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind18.isOn()) {
sv.paycurr.setReverse(BaseScreenData.REVERSED);
sv.paycurr.setColor(BaseScreenData.RED);
}
if (!appVars.ind18.isOn()) {
sv.paycurr.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind10.isOn()) {
sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
sv.effdateDisp.setColor(BaseScreenData.RED);
}
if (!appVars.ind10.isOn()) {
sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind11.isOn()) {
sv.percentAmountInd.setReverse(BaseScreenData.REVERSED);
sv.percentAmountInd.setColor(BaseScreenData.RED);
}
if (!appVars.ind11.isOn()) {
sv.percentAmountInd.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind13.isOn()) {
sv.virtFundSplitMethod.setReverse(BaseScreenData.REVERSED);
sv.virtFundSplitMethod.setColor(BaseScreenData.RED);
}
if (appVars.ind60.isOn()) {
sv.virtFundSplitMethod.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind13.isOn()) {
sv.virtFundSplitMethod.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind12.isOn()) {
sv.instprem.setReverse(BaseScreenData.REVERSED);
sv.instprem.setColor(BaseScreenData.RED);
}
if (!appVars.ind12.isOn()) {
sv.instprem.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind31.isOn()) {
sv.unitVirtualFund01.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind21.isOn()) {
sv.unitVirtualFund01.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind31.isOn()) {
sv.unitVirtualFund01.setColor(BaseScreenData.RED);
}
if (!appVars.ind31.isOn()) {
sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind41.isOn()) {
sv.unitAllocPercAmt01.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt01.setColor(BaseScreenData.RED);
}
if (!appVars.ind41.isOn()) {
sv.unitAllocPercAmt01.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind32.isOn()) {
sv.unitVirtualFund02.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind22.isOn()) {
sv.unitVirtualFund02.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind32.isOn()) {
sv.unitVirtualFund02.setColor(BaseScreenData.RED);
}
if (!appVars.ind32.isOn()) {
sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind42.isOn()) {
sv.unitAllocPercAmt02.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt02.setColor(BaseScreenData.RED);
}
if (!appVars.ind42.isOn()) {
sv.unitAllocPercAmt02.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind33.isOn()) {
sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind23.isOn()) {
sv.unitVirtualFund03.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind33.isOn()) {
sv.unitVirtualFund03.setColor(BaseScreenData.RED);
}
if (!appVars.ind33.isOn()) {
sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind43.isOn()) {
sv.unitAllocPercAmt03.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt03.setColor(BaseScreenData.RED);
}
if (!appVars.ind43.isOn()) {
sv.unitAllocPercAmt03.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind34.isOn()) {
sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind24.isOn()) {
sv.unitVirtualFund04.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind34.isOn()) {
sv.unitVirtualFund04.setColor(BaseScreenData.RED);
}
if (!appVars.ind34.isOn()) {
sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind44.isOn()) {
sv.unitAllocPercAmt04.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt04.setColor(BaseScreenData.RED);
}
if (!appVars.ind44.isOn()) {
sv.unitAllocPercAmt04.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind35.isOn()) {
sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind25.isOn()) {
sv.unitVirtualFund05.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind35.isOn()) {
sv.unitVirtualFund05.setColor(BaseScreenData.RED);
}
if (!appVars.ind35.isOn()) {
sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind45.isOn()) {
sv.unitAllocPercAmt05.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt05.setColor(BaseScreenData.RED);
}
if (!appVars.ind45.isOn()) {
sv.unitAllocPercAmt05.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind36.isOn()) {
sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
sv.unitVirtualFund06.setColor(BaseScreenData.RED);
}
if (appVars.ind26.isOn()) {
sv.unitVirtualFund06.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind36.isOn()) {
sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind46.isOn()) {
sv.unitAllocPercAmt06.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt06.setColor(BaseScreenData.RED);
}
if (!appVars.ind46.isOn()) {
sv.unitAllocPercAmt06.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind37.isOn()) {
sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
sv.unitVirtualFund07.setColor(BaseScreenData.RED);
}
if (appVars.ind27.isOn()) {
sv.unitVirtualFund07.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind37.isOn()) {
sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind47.isOn()) {
sv.unitAllocPercAmt07.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt07.setColor(BaseScreenData.RED);
}
if (!appVars.ind47.isOn()) {
sv.unitAllocPercAmt07.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind38.isOn()) {
sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
sv.unitVirtualFund08.setColor(BaseScreenData.RED);
}
if (appVars.ind28.isOn()) {
sv.unitVirtualFund08.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind38.isOn()) {
sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind48.isOn()) {
sv.unitAllocPercAmt08.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt08.setColor(BaseScreenData.RED);
}
if (!appVars.ind48.isOn()) {
sv.unitAllocPercAmt08.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind39.isOn()) {
sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
sv.unitVirtualFund09.setColor(BaseScreenData.RED);
}
if (appVars.ind29.isOn()) {
sv.unitVirtualFund09.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind39.isOn()) {
sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind49.isOn()) {
sv.unitAllocPercAmt09.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt09.setColor(BaseScreenData.RED);
}
if (!appVars.ind49.isOn()) {
sv.unitAllocPercAmt09.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind60.isOn()) {
sv.comind.setReverse(BaseScreenData.REVERSED);
sv.comind.setColor(BaseScreenData.RED);
}
if (!appVars.ind60.isOn()) {
sv.comind.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind40.isOn()) {
sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
sv.unitVirtualFund10.setColor(BaseScreenData.RED);
}
if (appVars.ind30.isOn()) {
sv.unitVirtualFund10.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind40.isOn()) {
sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind50.isOn()) {
sv.unitAllocPercAmt10.setReverse(BaseScreenData.REVERSED);
sv.unitAllocPercAmt10.setColor(BaseScreenData.RED);
}
if (!appVars.ind50.isOn()) {
sv.unitAllocPercAmt10.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind17.isOn()) {
sv.zrtotsumin.setReverse(BaseScreenData.REVERSED);
sv.zrtotsumin.setColor(BaseScreenData.RED);
}
if (!appVars.ind17.isOn()) {
sv.zrtotsumin.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind18.isOn()) {
sv.zrtopusum.setReverse(BaseScreenData.REVERSED);
sv.zrtopusum.setColor(BaseScreenData.RED);
}
if (!appVars.ind18.isOn()) {
sv.zrtopusum.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind19.isOn()) {
sv.optextind.setReverse(BaseScreenData.REVERSED);
sv.optextind.setColor(BaseScreenData.RED);
}
if (!appVars.ind19.isOn()) {
sv.optextind.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind20.isOn()) {
sv.sumins.setReverse(BaseScreenData.REVERSED);
sv.sumins.setColor(BaseScreenData.RED);
}
if (!appVars.ind20.isOn()) {
sv.sumins.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind70.isOn()) {
sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind31.isOn()) {
sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind70.isOn()) {
sv.reserveUnitsInd.setColor(BaseScreenData.RED);
}
if (!appVars.ind70.isOn()) {
sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind71.isOn()) {
sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind32.isOn()) {
sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind71.isOn()) {
sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
}
if (!appVars.ind71.isOn()) {
sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
}
if(appVars.ind69.isOn()){
sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
sv.reserveUnitsDate.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILIFE-8164 -STARTS		 
if (appVars.ind101.isOn()) {
sv.newFundList01.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind102.isOn()) {
sv.newFundList02.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind103.isOn()) {
sv.newFundList03.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind104.isOn()) {
sv.newFundList04.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind105.isOn()) {
sv.newFundList05.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind106.isOn()) {
sv.newFundList06.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind107.isOn()) {
sv.newFundList07.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind108.isOn()) {
sv.newFundList08.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind109.isOn()) {
sv.newFundList09.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind110.isOn()) {
sv.newFundList10.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind111.isOn()) {
sv.newFundList11.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind112.isOn()) {
sv.newFundList12.setInvisibility(BaseScreenData.INVISIBLE);
}	
//ILIFE-8164
}
%>
<!-- ILIFE-2618 Life Cross Browser -Coding and UT- Sprint 2 D6: Task 6  starts -->
<style>
@media \0screen\,screen\9
{
.output_cell,.blank_cell{margin-left:1px}
.iconpos{margin-bottom:1px;}
}
.panel{
height: 750px;
}
</style>
<!-- ILIFE-2618 Life Cross Browser -Coding and UT- Sprint 2 D6: Task 6  ends -->
<div class="panel panel-default">
<div class="panel-body">
<div class="row">    
<div class="col-md-2">
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
<table><tr><td>
<%}%>
<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td><td>
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td><td>
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="max-width:400px; margin-left:2px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td></tr></table>
</div></div>	
<div class="col-md-6"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<%}%>
<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
<%}%>
<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
<%}%>
<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
<%}%>
<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.register.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
<table><tr><td>
<%}%>
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td><td>
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="margin-left:1px;max-width: 150px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td></tr></table>
</div></div>
<div class="col-md-6"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
<table><tr><td>
<%}%>
<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:100px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td><td>
<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:100px; margin-left:1px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</td></tr></table>
</div></div></div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
<div class="input-group" style="width:71px;">
<%}%>
<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
if(longValue == null || longValue.equalsIgnoreCase("")) { 			
formatValue = formatValue( formatValue );
} else {
formatValue = formatValue( longValue );
}
}
if(!formatValue.trim().equalsIgnoreCase("")) {
%>
<div class="output_cell">	
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%
} else {
%>
<div class="blank_cell" > &nbsp; </div>
<% 
} 
%>
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
<%}%>
<br/>
<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
if(longValue == null || longValue.equalsIgnoreCase("")) { 			
formatValue = formatValue( formatValue );
} else {
formatValue = formatValue( longValue );
}
}
if(!formatValue.trim().equalsIgnoreCase("")) {
%>
<div class="output_cell">	
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%
} else {
%>
<div class="blank_cell" > &nbsp; </div>
<% 
} 
%>
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Bill Curr")%></label>
<%}%>
<%if ((new Byte((sv.billcurr).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.billcurr.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<br/><br/><div><!-- tab div -->
<div class="row">
<div class="col-md-12">
<ul class="nav nav-tabs">
<li class="active">
<a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Selected Components")%></a>
</li>
<li>
<a href="#dates_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Funds")%></a>
</li>
</ul>
</div>
</div>
<div class="tab-content">
<div class="tab-pane fade in active" id="basic_tab">
<div class="row">
<div class="col-md-2">	
<div class="form-group">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
<%}%>
<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.life.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
<%}%>
<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
<%}%>
<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.rider.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
<%}%>
<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Curr")%></label>
<%}%>
<%	
if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("paycurr");
optionValue = makeDropDownList( mappedItems , sv.paycurr.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.paycurr).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.paycurr).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='paycurr' type='list' style="width:170px;"
<% 
if((new Byte((sv.paycurr).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.paycurr).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.paycurr).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Ex. Rate")%></label>
<div class="input-group" style="width:71px;">
<%}%>
<%if ((new Byte((sv.exrat).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.exrat).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
formatValue = smartHF.getPicFormatted(qpsf,sv.exrat,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
if(!((sv.exrat.getFormData()).toString()).trim().equalsIgnoreCase("")) {
if(longValue == null || longValue.equalsIgnoreCase("")) { 			
formatValue = formatValue( formatValue );
} else {
formatValue = formatValue( longValue );
}
}
if(!formatValue.trim().equalsIgnoreCase("")) {
%>
<div class="output_cell">	
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%
} else {
%>
<div class="blank_cell" > &nbsp; </div>
<% 
} 
%>
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div></div>
<div class="row">
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
<%
if((new Byte((sv.effdateDisp).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
%>
<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
<%
}else{
%>
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
<%
}
%>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("% or Amounts")%></label>
<%}%>
<%	
if ((new Byte((sv.percentAmountInd).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
if(((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
longValue=resourceBundleHandler.gettingValueFromBundle("Percentage");
}
if(((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {
longValue=resourceBundleHandler.gettingValueFromBundle("Amounts");
}
%>
<% 
if((new Byte((sv.percentAmountInd).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.percentAmountInd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='percentAmountInd' style="width:100px;" 	
onFocus='doFocus(this)'
onHelp='return fieldHelp(percentAmountInd)'
onKeyUp='return checkMaxLength(this)'
<% 
if((new Byte((sv.percentAmountInd).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.percentAmountInd).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="P"<% if(((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></option>
<option value="A"<% if(((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("A")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Amounts")%></option>
</select>
<% if("red".equals((sv.percentAmountInd).getColor())){
%>
</div>
<%
} 
%>
<%
}longValue = null;} 
%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Split Plan")%></label>
<%}%>
<%	
if ((new Byte((sv.virtFundSplitMethod).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"virtFundSplitMethod"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("virtFundSplitMethod");
optionValue = makeDropDownList( mappedItems , sv.virtFundSplitMethod.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.virtFundSplitMethod.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.virtFundSplitMethod).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.virtFundSplitMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='virtFundSplitMethod' type='list' style="width:205px;"
<% 
if((new Byte((sv.virtFundSplitMethod).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.virtFundSplitMethod).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.virtFundSplitMethod).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
</div></div></div>
<div class="row">
<div class="col-md-4">
<div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>
<%
longValue = sv.reserveUnitsInd.getFormData();
if ("".equals(longValue)) {
longValue = resourceBundleHandler.gettingValueFromBundle("Select");
} else if ("Y".equals(longValue)) {
longValue = resourceBundleHandler.gettingValueFromBundle("Y");
} else if ("N".equals(longValue)) {
longValue = resourceBundleHandler.gettingValueFromBundle("N");
}
if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
%>
<div
class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>
<%
if (longValue != null) {
%>
<%=XSSFilter.escapeHtml(longValue)%>
<%
}
%>
</div>
<%
longValue = null;
%>
<%
} else {
%>
<%
if ("red".equals((sv.reserveUnitsInd).getColor())) {
%>
<div
style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
<%
}
%>
<select name='reserveUnitsInd' type='list' style="width: 100px;"
<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
readonly="true" disabled class="output_cell"
<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
class="bold_cell" <%} else {%> class='input_cell'
<%}%>>
<option value=""
<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
<option value="Y"
<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
<option value="N"
<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
</select>
<%
if ("red".equals((sv.reserveUnitsInd).getColor())) {
%>
</div>
<%
}
%>
<%
}
%>
</div>
</div>
<!-- 			<div class="col-md-1"></div> -->
<div class="col-md-4">
<div class="form-group" style="max-width: 150px;">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>
<%-- <div class="input-group date form_date col-md-12" data-date=""
data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
data-link-format="dd/mm/yyyy">
<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
<span class="input-group-addon"><span
class="glyphicon glyphicon-calendar"></span></span>
</div> --%>
<%--  <div class="input-group date form_date col-md-8" data-date=""
data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
data-link-format="dd/mm/yyyy" style="min-width:162px;">
<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
<span class="input-group-addon"><span
class="glyphicon glyphicon-calendar"></span></span>
</div> --%>
<% 
if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
%>
<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
<% }else {%>
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>	
<%} %>	
</div>
</div>
</div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></label>
<%}%>
<%if ((new Byte((sv.instprem).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
valueThis=smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='instprem' 
type='text'
<%if((sv.instprem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:71px;"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instprem.getLength(), sv.instprem.getScale(),3)%>'
maxLength='<%= sv.instprem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instprem)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.instprem).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.instprem).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.instprem).getColor()== null  ? 
"input_cell" :  (sv.instprem).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Premium for Transaction")%></label>
<div class="input-group" style="width:71px;">
<%}%>
<%if ((new Byte((sv.totlprem).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.totlprem).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
formatValue = smartHF.getPicFormatted(qpsf,sv.totlprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
if(!((sv.totlprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
if(longValue == null || longValue.equalsIgnoreCase("")) { 			
formatValue = formatValue( formatValue );
} else {
formatValue = formatValue( longValue );
}
}
if(!formatValue.trim().equalsIgnoreCase("")) {
%>
<div class="output_cell">	
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%
} else {
%>
<div class="blank_cell" > &nbsp; </div>
<% 
} 
%>
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div></div>
<div class="col-md-1"></div>
<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
<%}%>
<%if ((new Byte((sv.sumins).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.sumins).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='sumins' 
type='text'
<%if((sv.sumins).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:80px;"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins.getLength(), sv.sumins.getScale(),3)%>'
maxLength='<%= sv.sumins.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumins)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.sumins).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.sumins).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.sumins).getColor()== null  ? 
"input_cell" :  (sv.sumins).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div></div>
<div class="row">
<div class="col-md-4">
<div class="form-group">
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Amount In Suspense")%></label>
<%}%>
<%if ((new Byte((sv.susamt).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.susamt).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
formatValue = smartHF.getPicFormatted(qpsf,sv.susamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
if(!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
if(longValue == null || longValue.equalsIgnoreCase("")) { 			
formatValue = formatValue( formatValue );
} else {
formatValue = formatValue( longValue );
}
}
if(!formatValue.trim().equalsIgnoreCase("")) {
%>
<div class="output_cell">	
<%=XSSFilter.escapeHtml(formatValue)%>
</div>
<%
} else {
%>
<div class="blank_cell" > &nbsp; </div>
<% 
} 
%>
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div></div>
<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Total S/A")%></label>
<%}%>
<%if ((new Byte((sv.zrtotsumin).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.zrtotsumin).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.zrtotsumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='zrtotsumin' 
type='text'
<%if((sv.zrtotsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:80px;"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zrtotsumin.getLength(), sv.zrtotsumin.getScale(),3)%>'
maxLength='<%= sv.zrtotsumin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zrtotsumin)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.zrtotsumin).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.zrtotsumin).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.zrtotsumin).getColor()== null  ? 
"input_cell" :  (sv.zrtotsumin).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Top up S/A")%></label>
<%}%>
<%if ((new Byte((sv.zrtopusum).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.zrtopusum).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.zrtopusum,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='zrtopusum' 
type='text'
<%if((sv.zrtopusum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:80px;"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zrtopusum.getLength(), sv.zrtopusum.getScale(),3)%>'
maxLength='<%= sv.zrtopusum.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zrtopusum)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.zrtopusum).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.zrtopusum).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.zrtopusum).getColor()== null  ? 
"input_cell" :  (sv.zrtopusum).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div></div></div>
<div class="tab-pane fade" id="dates_tab">
<div class="row">
<div class="col-md-2">
<%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></label>
<%}%>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
<%}%>
<br/></div>
<div class="col-md-2">
<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("% / Amount")%>
</div>
<%}%>
</div></div><div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->								
<%
if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
%>
<td><div class="form-group" style="width: 200px;">
<%	
if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund01"}, sv, "E", baseModel);
Set<String> set = new HashSet<String>();
for(FixedLengthStringData value: sv.newFundList){
if(value != null)
set.add(value.toString());
}
mappedItems = (Map) fieldItem.get("unitVirtualFund01");
mappedItems.keySet().retainAll(set);
optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 2,
resourceBundleHandler);
longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
%>
<%=smartHF.getDropDownExt(sv.unitVirtualFund01, fw, longValue, "unitVirtualFund01", optionValue,0, 255)%>
<%	}%>
</div></td>
<%}else{ %>
<!-- ILIFE-8164-ENDS-->	
<%	
if ((new Byte((sv.unitVirtualFund01).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund01"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund01");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund01.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund01).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund01' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund01).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund01).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund01).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%></div>						
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy01).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy01.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy01.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="min-width:71px;width: 40px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt01).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt01).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt01' 
type='text'
<%if((sv.unitAllocPercAmt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt01.getLength(), sv.unitAllocPercAmt01.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt01.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt01)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt01).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt01).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt01).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div><div class="row"><div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund02"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund02");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund02, fw, longValue, "unitVirtualFund02", optionValue, 0, 255)%>
										<%	}%>
									</div>
								</td>
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->	
<%	
if ((new Byte((sv.unitVirtualFund02).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund02"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund02");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund02.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund02).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund02' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund02).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund02).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund02).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy02).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy02.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy02.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="min-width:71px;width: 40px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt02).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt02).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt02' 
type='text'
<%if((sv.unitAllocPercAmt02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt02.getLength(), sv.unitAllocPercAmt02.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt02.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt02)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt02).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt02).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt02).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->								
								<%
						if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund03"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund03");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund03, fw, longValue, "unitVirtualFund03", optionValue, 0, 255)%>
										<%}	%>
									</div>	</td>
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->					
<%	
if ((new Byte((sv.unitVirtualFund03).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund03"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund03");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund03.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund03).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund03' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund03).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund03).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund03).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%} %></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy03).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy03.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy03.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="min-width:71px;width: 40px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt03).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt03).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt03' 
type='text'
<%if((sv.unitAllocPercAmt03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt03.getLength(), sv.unitAllocPercAmt03.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt03.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt03)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt03).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt03).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt03).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div><div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->				
				<%
						if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund04"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund04");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund04, fw, longValue, "unitVirtualFund04", optionValue, 0, 255)%>
										<%}	%>
									</div></td>
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->		
<%	
if ((new Byte((sv.unitVirtualFund04).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund04"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund04");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund04.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund04).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund04' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund04).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund04).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund04).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy04).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy04.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy04.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="min-width:71px;width: 40px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt04).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt04).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt04' 
type='text'
<%if((sv.unitAllocPercAmt04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt04.getLength(), sv.unitAllocPercAmt04.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt04.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt04)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt04).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt04).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt04).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund05"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund05");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund05, fw, longValue, "unitVirtualFund05", optionValue,
						0, 255)%>
										<%
											}
										%>
									</div>
								</td>
							<%}else{ %>
<!-- ILIFE-8164-ENDS-->			
<%	
if ((new Byte((sv.unitVirtualFund05).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund05"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund05");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund05.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund05).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund05' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund05).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund05).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund05).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy05).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy05.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy05.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="min-width:71px;width: 40px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt05).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt05).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);	
%>
<input name='unitAllocPercAmt05' 
type='text'
<%if((sv.unitAllocPercAmt05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt05.getLength(), sv.unitAllocPercAmt05.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt05.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt05)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt05).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt05).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt05).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund06"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund06");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund06, fw, longValue, "unitVirtualFund06", optionValue, 0, 255)%>
										<%}	%>
									</div></td>
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->				
							
<%	
if ((new Byte((sv.unitVirtualFund06).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund06"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund06");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund06.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund06).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund06' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund06).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund06).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund06).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy06).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy06.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy06.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt06).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt06).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt06' 
type='text'
<%if((sv.unitAllocPercAmt06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt06.getLength(), sv.unitAllocPercAmt06.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt06.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt06)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt06).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt06).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt06).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund07"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund07");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund07, fw, longValue, "unitVirtualFund07", optionValue, 0, 255)%>
										<%}	%>
									</div></td>
							<%}else{ %>	
<%	
if ((new Byte((sv.unitVirtualFund07).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund07"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund07");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund07.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund07).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund07' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund07).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund07).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund07).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%} %></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy07).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy07.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy07.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt07).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt07).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt07,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt07' 
type='text'
<%if((sv.unitAllocPercAmt07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt07.getLength(), sv.unitAllocPercAmt07.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt07.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt07)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt07).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt07).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt07).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt07).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund08"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund08");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund08, fw, longValue, "unitVirtualFund08", optionValue, 0, 255)%>
										<%}	%>
									</div></td>
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->	
<%	
if ((new Byte((sv.unitVirtualFund08).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund08"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund08");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund08.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund08).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund08' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund08).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund08).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund08).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%} %></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy08).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy08.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy08.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt08).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt08).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt08,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt08' 
type='text'
<%if((sv.unitAllocPercAmt08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt08.getLength(), sv.unitAllocPercAmt08.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt08.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt08)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt08).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt08).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt08).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt08).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td><div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund09"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund09");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund09, fw, longValue, "unitVirtualFund09", optionValue, 0, 255)%>
										<%}	%>
									</div>
								</td>
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->	
<%	
if ((new Byte((sv.unitVirtualFund09).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund09"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund09");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund09.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund09).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund09' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund09).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund09).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund09).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%></div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy09).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy09.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy09.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt09).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt09).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt09,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt09' 
type='text'
<%if((sv.unitAllocPercAmt09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt09.getLength(), sv.unitAllocPercAmt09.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt09.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt09)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt09).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt09).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt09).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt09).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div>
<div class="row">
<div class="col-md-2">
<!-- ILIFE-8164-START-->
<%
						if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<td>
									<div class="form-group" style="width: 200px;">
										<%	
											if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund10"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund10");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund10, fw, longValue, "unitVirtualFund10", optionValue, 0, 255)%>
										<%}	%>
									</div></td>
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->		
<%	
if ((new Byte((sv.unitVirtualFund10).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {
fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund10"},sv,"E",baseModel);
mappedItems = (Map) fieldItem.get("unitVirtualFund10");
optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund10.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());  
%>
<% 
if((new Byte((sv.unitVirtualFund10).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
"blank_cell" : "output_cell" %>'>  
<%if(longValue != null){%>
<%=XSSFilter.escapeHtml(longValue)%>
<%}%>
</div>
<%
longValue = null;
%>
<% }else {%>
<% if("red".equals((sv.unitVirtualFund10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='unitVirtualFund10' type='list' style="width:255px;"
<% 
if((new Byte((sv.unitVirtualFund10).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
disabled
class="output_cell"
<%
}else if((new Byte((sv.unitVirtualFund10).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = 'input_cell' 
<%
} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.unitVirtualFund10).getColor())){
%>
</div>
<%
} 
%>
<%
}} 
%>
<%
longValue = null;
%>
<%}%>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
<%if ((new Byte((sv.currcy10).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%					
if(!((sv.currcy10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy10.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
} else  {
if(longValue == null || longValue.equalsIgnoreCase("")) {
formatValue = formatValue( (sv.currcy10.getFormData()).toString()); 
} else {
formatValue = formatValue( longValue);
}
}
%>			
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
"blank_cell" : "output_cell" %>' style="width:71px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
<%
longValue = null;
formatValue = null;
%>
<%}%>
</div>
<div class="col-md-2">
<%if ((new Byte((sv.unitAllocPercAmt10).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<%	
qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt10).getFieldName());
//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
valueThis=smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt10,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
<input name='unitAllocPercAmt10' 
type='text'
<%if((sv.unitAllocPercAmt10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>
value='<%=valueThis%>'
<%	 
if(valueThis!=null&& valueThis.trim().length()>0) {%>
title='<%=valueThis%>'
<%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.unitAllocPercAmt10.getLength(), sv.unitAllocPercAmt10.getScale(),3)%>'
maxLength='<%= sv.unitAllocPercAmt10.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt10)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'
<% 
if((new Byte((sv.unitAllocPercAmt10).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.unitAllocPercAmt10).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.unitAllocPercAmt10).getColor()== null  ? 
"input_cell" :  (sv.unitAllocPercAmt10).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
</div></div> 
<Div id='mainForm_OPTS' style='visibility:hidden;'>
<%
if(!(sv.optextind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optextind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 20px;margin-left: 35px;">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%> 
</a>
</div>	
</div>
<%} %>
<div>
<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind
.getFormData()%>">
</div>
<%
if(!(sv.comind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.comind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 20px;margin-left: 35px;">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Commission")%> 
</a>
</div>	
</div>
<%} %>
<div>
<input name='comind' id='comind' type='hidden'  value="<%=sv.comind
.getFormData()%>">
</div>
</Div>
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((sv.percentAmountInd).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) {%>
<input name='percentAmountInd' 
type='text'
<%if((sv.percentAmountInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
<%
formatValue = (sv.percentAmountInd.getFormData()).toString();
%>
value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.percentAmountInd.getLength()%>'
maxLength='<%= sv.percentAmountInd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(percentAmountInd)' onKeyUp='return checkMaxLength(this)'  
<% 
if((new Byte((sv.percentAmountInd).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"
<%
}else if((new Byte((sv.percentAmountInd).getHighLight())).
compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" 
<%
}else { 
%>
class = ' <%=(sv.percentAmountInd).getColor()== null  ? 
"input_cell" :  (sv.percentAmountInd).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>'
<%
} 
%>
>
<%}%>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(P/A)")%>
</div>
<%}%>
</td></tr></table>
</div>
</div></div></div></div> </div>
<%@ include file="/POLACommon2NEW.jsp"%>