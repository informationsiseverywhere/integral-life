

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5540";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5540ScreenVars sv = (S5540ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Units ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Alloc. Basis Std/Incr ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Review Processing ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Default Fund Split Plan ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cancellation of Initial units ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Initial Units");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Discount Basis ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Loyalty/Persistency ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Discount Factor ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Withdrawal Method ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Or Discount Factor ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(Whole of Life)");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Funds ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Available Fund List ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Auto Fund Rebalancing (AFR) Option ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"AFR Trigger Percentage ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Max No Funds/Coverage ");
%>

<%
	{
		if (appVars.ind07.isOn()) {
			sv.allbas.setReverse(BaseScreenData.REVERSED);
			sv.allbas.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.allbas.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.rvwproc.setReverse(BaseScreenData.REVERSED);
			sv.rvwproc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.rvwproc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.fundSplitPlan.setReverse(BaseScreenData.REVERSED);
			sv.fundSplitPlan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.fundSplitPlan.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.iuDiscBasis.setReverse(BaseScreenData.REVERSED);
			sv.iuDiscBasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.iuDiscBasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.ltypst.setReverse(BaseScreenData.REVERSED);
			sv.ltypst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.ltypst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.iuDiscFact.setReverse(BaseScreenData.REVERSED);
			sv.iuDiscFact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.iuDiscFact.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.wdmeth.setReverse(BaseScreenData.REVERSED);
			sv.wdmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.wdmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.alfnds.setReverse(BaseScreenData.REVERSED);
			sv.alfnds.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.alfnds.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.maxfnd.setReverse(BaseScreenData.REVERSED);
			sv.maxfnd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.maxfnd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.wholeIuDiscFact.setReverse(BaseScreenData.REVERSED);
			sv.wholeIuDiscFact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.wholeIuDiscFact.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.unitCancInit.setReverse(BaseScreenData.REVERSED);
			sv.unitCancInit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.unitCancInit.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<!-- ILIFE-2595 Life Cross Browser - Sprint 2 D5 : Task 5  -->

<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<!-- ILIFE-2595 Life Cross Browser - Sprint 2 D5 : Task 5 -->



<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td style="min-width: 80px;">
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td style="min-width: 80px;">
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Units")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Alloc Basis Std/Incr")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "allbas" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("allbas");
							optionValue = makeDropDownList(mappedItems, sv.allbas.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.allbas.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.allbas).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.allbas).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 222px;">
							<%
								}
							%>

							<select name='allbas' type='list' style="width: 220px;"
								<%if ((new Byte((sv.allbas).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.allbas).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.allbas).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 "></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Review Processing")%></label>
					<div style="width: 70px;">
						<input name='rvwproc' type='text'
							<%if ((sv.rvwproc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.rvwproc.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.rvwproc.getLength()%>'
							maxLength='<%=sv.rvwproc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(rvwproc)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.rvwproc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rvwproc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rvwproc).getColor() == null ? "input_cell"
						: (sv.rvwproc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Fund Split Plan")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "fundSplitPlan" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("fundSplitPlan");
							optionValue = makeDropDownList(mappedItems, sv.fundSplitPlan.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.fundSplitPlan.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.fundSplitPlan).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.fundSplitPlan).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 212px;">
							<%
								}
							%>

							<select name='fundSplitPlan' type='list' style="width: 210px;"
								<%if ((new Byte((sv.fundSplitPlan).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.fundSplitPlan).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.fundSplitPlan).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cancellation of Initial units")%></label>
					<input type='checkbox' name='unitCancInit' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(unitCancInit)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.unitCancInit).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.unitCancInit).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.unitCancInit).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('unitCancInit')" /> <input type='checkbox'
						name='unitCancInit' value='N'
						<%if (!(sv.unitCancInit).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('unitCancInit')" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Initial Units")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discount Basis")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "iuDiscBasis" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("iuDiscBasis");
							optionValue = makeDropDownList(mappedItems, sv.iuDiscBasis.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.iuDiscBasis.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.iuDiscBasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.iuDiscBasis).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='iuDiscBasis' type='list' style="width: 170px;"
								<%if ((new Byte((sv.iuDiscBasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.iuDiscBasis).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.iuDiscBasis).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discount Factor")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "iuDiscFact" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("iuDiscFact");
							optionValue = makeDropDownList(mappedItems, sv.iuDiscFact.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.iuDiscFact.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.iuDiscFact).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.iuDiscFact).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='iuDiscFact' type='list' style="width: 170px;"
								<%if ((new Byte((sv.iuDiscFact).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.iuDiscFact).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.iuDiscFact).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Or Discount Factor")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("(Whole of Life)")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "wholeIuDiscFact" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("wholeIuDiscFact");
							optionValue = makeDropDownList(mappedItems, sv.wholeIuDiscFact.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.wholeIuDiscFact.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.wholeIuDiscFact).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.wholeIuDiscFact).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 207px;">
							<%
								}
							%>

							<select name='wholeIuDiscFact' type='list' style="width: 205px;"
								<%if ((new Byte((sv.wholeIuDiscFact).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.wholeIuDiscFact).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.wholeIuDiscFact).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loyalty/Persistency")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "ltypst" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("ltypst");
							optionValue = makeDropDownList(mappedItems, sv.ltypst.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.ltypst.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.ltypst).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.ltypst).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='ltypst' type='list' style="width: 170px;"
								<%if ((new Byte((sv.ltypst).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.ltypst).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.ltypst).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Withdrawal Method")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "wdmeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("wdmeth");
							optionValue = makeDropDownList(mappedItems, sv.wdmeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.wdmeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.wdmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.wdmeth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 222px;">
							<%
								}
							%>

							<select name='wdmeth' type='list' style="width: 220px;"
								<%if ((new Byte((sv.wdmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.wdmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.wdmeth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Funds")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Available Fund List")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "alfnds" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("alfnds");
							optionValue = makeDropDownList(mappedItems, sv.alfnds.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.alfnds.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.alfnds).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.alfnds).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 202px;">
							<%
								}
							%>

							<select name='alfnds' type='list' style="width: 200px;"
								<%if ((new Byte((sv.alfnds).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.alfnds).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.alfnds).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Fund Rebalancing (AFR) Option ")%></label>
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "zafropt1" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("zafropt1");
							optionValue = makeDropDownList(mappedItems, sv.zafropt1.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.zafropt1.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.zafropt1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.zafropt1).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='zafropt1' type='list' style="width: 170px;"
								<%if ((new Byte((sv.zafropt1).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.zafropt1).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.zafropt1).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("AFR Trigger Percentage ")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zvariance).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='zvariance' type='text'
							<%if ((sv.zvariance).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.zvariance)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.zvariance);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.zvariance)%>' <%}%>
							size='<%=sv.zvariance.getLength()%>'
							maxLength='<%=sv.zvariance.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(zvariance)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.zvariance).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.zvariance).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zvariance).getColor() == null ? "input_cell"
						: (sv.zvariance).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Max No Funds/Coverage")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.maxfnd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='maxfnd' type='text'
							<%if ((sv.maxfnd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: LEFT" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.maxfnd)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.maxfnd);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.maxfnd)%>' <%}%>
							size='<%=sv.maxfnd.getLength()%>'
							maxLength='<%=sv.maxfnd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(maxfnd)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.maxfnd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.maxfnd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.maxfnd).getColor() == null ? "input_cell"
						: (sv.maxfnd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>