<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5118";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5118ScreenVars sv = (S5118ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5118screenWritten.gt(0)) {
%>
<%
	S5118screen.clearClassString(sv);
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		}
%>

<%=smartHF.getLit(23, 4, generatedText14)%>


<%
	}
%>

<%-- <%
	if (sv.S5118screensflWritten.gt(0)) {
%>
<%
	GeneralTable sfl = fw.getTable("s5118screensfl");
		savedInds = appVars.saveAllInds();
		S5118screensfl.set1stScreenRow(sfl, appVars, sv);
		double sflLine = 0.0;
		int doingLine = 0;
		int sflcols = 1;
		int linesPerCol = 11;
		String height = smartHF.fmty(11);
		smartHF.setSflLineOffset(11);
%>
<div
	style='position: absolute; left: 0%; top: <%=smartHF.fmty(11)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%
		while (S5118screensfl.hasMoreScreenRows(sfl)) {
	%>
	<%
		sv.select.setClassString("");
	%>
	<%
		sv.planSuffix.setClassString("");
	%>
	<%
		sv.planSuffix.appendClassString("num_fld");
				sv.planSuffix.appendClassString("output_txt");
				sv.planSuffix.appendClassString("highlight");
	%>
	<%
		sv.covRiskStat.setClassString("");
	%>
	<%
		sv.covRiskStat.appendClassString("string_fld");
				sv.covRiskStat.appendClassString("output_txt");
				sv.covRiskStat.appendClassString("highlight");
	%>
	<%
		sv.rstatdesc.setClassString("");
	%>
	<%
		sv.rstatdesc.appendClassString("string_fld");
				sv.rstatdesc.appendClassString("output_txt");
				sv.rstatdesc.appendClassString("highlight");
	%>
	<%
		sv.covPremStat.setClassString("");
	%>
	<%
		sv.covPremStat.appendClassString("string_fld");
				sv.covPremStat.appendClassString("output_txt");
				sv.covPremStat.appendClassString("highlight");
	%>
	<%
		sv.pstatdesc.setClassString("");
	%>
	<%
		sv.pstatdesc.appendClassString("string_fld");
				sv.pstatdesc.appendClassString("output_txt");
				sv.pstatdesc.appendClassString("highlight");
	%>
	<%
		sv.screenIndicArea.setClassString("");
	%>
	<%
		sv.rider.setClassString("");
	%>
	<%
		sv.coverage.setClassString("");
	%>
	<%
		sv.life.setClassString("");
	%>

	<%
		{
					if (appVars.ind01.isOn()) {
						sv.select.setReverse(BaseScreenData.REVERSED);
						sv.select.setColor(BaseScreenData.RED);
					}
					if (appVars.ind05.isOn()) {
						sv.select.setEnabled(BaseScreenData.DISABLED);
					}
					if (!appVars.ind01.isOn()) {
						sv.select.setHighLight(BaseScreenData.BOLD);
					}
				}
	%>

	<%=smartHF.getTableHTMLVarQual(sflLine, 3, sfl, sv.select)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 6, sfl, sv.planSuffix,
							COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 12, sfl, sv.covRiskStat)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 12, sfl, sv.covRiskStat)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 15, sfl, sv.rstatdesc)%>
	<%=smartHF.getHTMLSFSpaceVar(sflLine, 46, sfl, sv.covPremStat)%>
	<%=smartHF.getHTMLF4SSVar(sflLine, 46, sfl, sv.covPremStat)%>
	<%=smartHF.getTableHTMLVarQual(sflLine, 49, sfl, sv.pstatdesc)%>




	<%
		sflLine += 1;
				doingLine++;
				if (doingLine % linesPerCol == 0 && sflcols > 1) {
					sflLine = 0.0;
				}
				S5118screensfl.setNextScreenRow(sfl, appVars, sv);
			}
	%>
</div>
<%
	appVars.restoreAllInds(savedInds);
%>


<%
	}
%> --%>

<%
	if (sv.S5118protectWritten.gt(0)) {
%>
<%
	S5118protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>

<%
	if (sv.S5118screenctlWritten.gt(0)) {
%>
<%
	S5118screenctl.clearClassString(sv);
%>
<%
	GeneralTable sfl = fw.getTable("s5118screensfl");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract No    ");
%>
<%
	sv.chdrnum.setClassString("");
%>
<%
	sv.chdrnum.appendClassString("string_fld");
		sv.chdrnum.appendClassString("output_txt");
		sv.chdrnum.appendClassString("highlight");
%>
<%
	sv.cnttype.setClassString("");
%>
<%
	sv.cnttype.appendClassString("string_fld");
		sv.cnttype.appendClassString("output_txt");
		sv.cnttype.appendClassString("highlight");
%>
<%
	sv.ctypedes.setClassString("");
%>
<%
	sv.ctypedes.appendClassString("string_fld");
		sv.ctypedes.appendClassString("output_txt");
		sv.ctypedes.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cmpnt  ");
%>
<%
	sv.crtable.setClassString("");
%>
<%
	sv.crtable.appendClassString("string_fld");
		sv.crtable.appendClassString("output_txt");
		sv.crtable.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Life           ");
%>
<%
	sv.lifcnum.setClassString("");
%>
<%
	sv.lifcnum.appendClassString("string_fld");
		sv.lifcnum.appendClassString("output_txt");
		sv.lifcnum.appendClassString("highlight");
%>
<%
	sv.linsname.setClassString("");
%>
<%
	sv.linsname.appendClassString("string_fld");
		sv.linsname.appendClassString("output_txt");
		sv.linsname.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Owner          ");
%>
<%
	sv.cownnum.setClassString("");
%>
<%
	sv.cownnum.appendClassString("string_fld");
		sv.cownnum.appendClassString("output_txt");
		sv.cownnum.appendClassString("highlight");
%>
<%
	sv.ownername.setClassString("");
%>
<%
	sv.ownername.appendClassString("string_fld");
		sv.ownername.appendClassString("output_txt");
		sv.ownername.appendClassString("highlight");
%>
<%-- <%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"RCD            ");
%> --%>
<!-- ILJ-49 Starts -->
					<% StringData generatedText5 = null;
               		 if (sv.iljCntDteFlag.compareTo("Y") != 0){ 
               			generatedText5= resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"RCD            ");
        			} else { 
        				generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Date ");
        			} %>
        			<!-- ILJ-49 ends-->
<%
	sv.occdateDisp.setClassString("");
%>
<%
	sv.occdateDisp.appendClassString("string_fld");
		sv.occdateDisp.appendClassString("output_txt");
		sv.occdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Risk/Prem Status   ");
%>
<%
	sv.rstate.setClassString("");
%>
<%
	sv.rstate.appendClassString("string_fld");
		sv.rstate.appendClassString("output_txt");
		sv.rstate.appendClassString("highlight");
%>
<%
	sv.pstate.setClassString("");
%>
<%
	sv.pstate.appendClassString("string_fld");
		sv.pstate.appendClassString("output_txt");
		sv.pstate.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Paid-to-date   ");
%>
<%
	sv.ptdateDisp.setClassString("");
%>
<%
	sv.ptdateDisp.appendClassString("string_fld");
		sv.ptdateDisp.appendClassString("output_txt");
		sv.ptdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Billed-to-date     ");
%>
<%
	sv.btdateDisp.setClassString("");
%>
<%
	sv.btdateDisp.appendClassString("string_fld");
		sv.btdateDisp.appendClassString("output_txt");
		sv.btdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "CCY ");
%>
<%
	sv.currcd.setClassString("");
%>
<%
	sv.currcd.appendClassString("string_fld");
		sv.currcd.appendClassString("output_txt");
		sv.currcd.appendClassString("highlight");
		sv.currcd.appendClassString("highlight");
%>
<%
	sv.currds.setClassString("");
%>
<%
	sv.currds.appendClassString("string_fld");
		sv.currds.appendClassString("output_txt");
		sv.currds.appendClassString("highlight");
		sv.currds.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Policies in Plan   ");
%>
<%
	sv.polinc.setClassString("");
%>
<%
	sv.polinc.appendClassString("num_fld");
		sv.polinc.appendClassString("output_txt");
		sv.polinc.appendClassString("highlight");
		sv.polinc.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"-----------------------------------------------------------------------------");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sel Plan  Risk Status                       Premium Status");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>
<%
	sv.subfilePosition.setClassString("");
%>

<%
	{
			appVars.rollup(new int[] { 93 });
		}
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<table>
						<tr class="input-group three-controller">
							<td><%=smartHF.getHTMLVarExt(fw, sv.chdrnum)%></td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.cnttype)%> <%=smartHF.getHTMLF4NSVarExt(fw, sv.cnttype)%>
							</td>
							<td><%=smartHF.getHTMLVarExt(fw, sv.ctypedes)%>
							<td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText13)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.crtable)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.crtable)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.lifcnum)%>
						<%=smartHF.getHTMLVarExt(fw, sv.linsname)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.cownnum)%>
						<%=smartHF.getHTMLVarExt(fw, sv.ownername)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<%
						if ((new Byte((sv.occdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.occdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.occdateDisp, (sv.occdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText6)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.rstate)%>
						<%=smartHF.getHTMLVarExt(fw, sv.pstate)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label> <%=smartHF.getLit(generatedText7)%></label>
					<%
						if ((new Byte((sv.ptdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.ptdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.ptdateDisp, (sv.ptdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label> <%=smartHF.getLit(generatedText8)%></label>
					<%
						if ((new Byte((sv.btdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.btdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.btdateDisp, (sv.btdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText9)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.currcd)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.currcd)%>
						<%=smartHF.getHTMLVarExt(fw, sv.currds)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText10)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.polinc, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5118' width='100%'>
						<thead>
							<tr class='info'>
								<th>Sel</th>
								<th>Plan</th>
								<th>Risk Status</th>
								<th>Premium Status</th>
							</tr>
						</thead>

						<tbody>
							<%
								if (sv.S5118screensflWritten.gt(0)) {
							%>
							<%
								/* GeneralTable sfl = fw.getTable("s5118screensfl"); */
										savedInds = appVars.saveAllInds();
										S5118screensfl.set1stScreenRow(sfl, appVars, sv);
										double sflLine = 0.0;
										int doingLine = 0;
										int sflcols = 1;
										int linesPerCol = 11;
										String height = smartHF.fmty(11);
										smartHF.setSflLineOffset(11);
							%>
							<div>
								<%
									while (S5118screensfl.hasMoreScreenRows(sfl)) {
								%>
								<%
									sv.select.setClassString("");
								%>
								<%
									sv.planSuffix.setClassString("");
								%>
								<%
									sv.planSuffix.appendClassString("num_fld");
												sv.planSuffix.appendClassString("output_txt");
												sv.planSuffix.appendClassString("highlight");
								%>
								<%
									sv.covRiskStat.setClassString("");
								%>
								<%
									sv.covRiskStat.appendClassString("string_fld");
												sv.covRiskStat.appendClassString("output_txt");
												sv.covRiskStat.appendClassString("highlight");
								%>
								<%
									sv.rstatdesc.setClassString("");
								%>
								<%
									sv.rstatdesc.appendClassString("string_fld");
												sv.rstatdesc.appendClassString("output_txt");
												sv.rstatdesc.appendClassString("highlight");
								%>
								<%
									sv.covPremStat.setClassString("");
								%>
								<%
									sv.covPremStat.appendClassString("string_fld");
												sv.covPremStat.appendClassString("output_txt");
												sv.covPremStat.appendClassString("highlight");
								%>
								<%
									sv.pstatdesc.setClassString("");
								%>
								<%
									sv.pstatdesc.appendClassString("string_fld");
												sv.pstatdesc.appendClassString("output_txt");
												sv.pstatdesc.appendClassString("highlight");
								%>
								<%
									sv.screenIndicArea.setClassString("");
								%>
								<%
									sv.rider.setClassString("");
								%>
								<%
									sv.coverage.setClassString("");
								%>
								<%
									sv.life.setClassString("");
								%>

								<%
									{
													if (appVars.ind01.isOn()) {
														sv.select.setReverse(BaseScreenData.REVERSED);
														sv.select.setColor(BaseScreenData.RED);
													}
													if (appVars.ind05.isOn()) {
														sv.select.setEnabled(BaseScreenData.DISABLED);
													}
													if (!appVars.ind01.isOn()) {
														sv.select.setHighLight(BaseScreenData.BOLD);
													}
												}
								%>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.select)%></td>


								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.planSuffix,
								COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%></td>
							<td>
								<div class="input-group three-controller">
									<%=smartHF.getHTMLSFSpaceVar(0, 0, sfl, sv.covRiskStat)%>
									<%=smartHF.getHTMLF4SSVar(0, 0, sfl, sv.covRiskStat)%>
									<%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.rstatdesc)%>
								</div>
							</td>

							<td>
								<div class="input-group three-controller">
									<%=smartHF.getHTMLSFSpaceVar(0, 0, sfl, sv.covPremStat)%>
									<%=smartHF.getHTMLF4SSVar(0, 0, sfl, sv.covPremStat)%>
									<%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.pstatdesc)%>
								</div>
							</td>

							<%
								sflLine += 1;
											doingLine++;
											if (doingLine % linesPerCol == 0 && sflcols > 1) {
												sflLine = 0.0;
											}
											S5118screensfl.setNextScreenRow(sfl, appVars, sv);
										}
							%>
							</div>
							<%
								appVars.restoreAllInds(savedInds);
							%>


							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>





	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>
