<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5429";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5429ScreenVars sv = (S5429ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5429screenWritten.gt(0)) {
%>
<%
	S5429screen.clearClassString(sv);
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		}
%>

<%=smartHF.getLit(23, 4, generatedText17)%>


<%
	}
%>



<%
	if (sv.S5429protectWritten.gt(0)) {
%>
<%
	S5429protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>

<%
	if (sv.S5429screenctlWritten.gt(0)) {
%>
<%
	S5429screenctl.clearClassString(sv);
%>
<%
	GeneralTable sfl = fw.getTable("s5429screensfl");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Effective Date ");
%>
<%
	sv.effdateDisp.setClassString("");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No ");
%>
<%
	sv.jobno.setClassString("");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Validflag ");
%>
<%
	sv.validflag.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Initial      Units");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Accumulation  Units");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Last");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Update");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>
<%
	sv.subfilePosition.setClassString("");
%>

<%
	{
			appVars.rollup(new int[] { 93 });
			if (appVars.ind09.isOn()) {
				sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
				sv.effdateDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind10.isOn()) {
				sv.jobno.setReverse(BaseScreenData.REVERSED);
				sv.jobno.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind10.isOn()) {
				sv.jobno.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind01.isOn()) {
				sv.validflag.setReverse(BaseScreenData.REVERSED);
				sv.validflag.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.validflag.setHighLight(BaseScreenData.BOLD);
			}
		}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div style="width: 120px;"><%=smartHF.getHTMLVarExt(fw, sv.jobno, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.validflag)%></div>
				</div>
			</div>
		</div>

		<%
			if (sv.S5429screensflWritten.gt(0)) {
		%>
		<%
			/* GeneralTable sfl = fw.getTable("s5429screensfl"); */
					savedInds = appVars.saveAllInds();
					S5429screensfl.set1stScreenRow(sfl, appVars, sv);
					double sflLine = 0.0;
					int doingLine = 0;
					int sflcols = 1;
					int linesPerCol = 15;
					String height = smartHF.fmty(15);
					smartHF.setSflLineOffset(8);
		%>
		<div>
			<%
				while (S5429screensfl.hasMoreScreenRows(sfl)) {
			%>
			<%
				sv.unitVirtualFund.setClassString("");
			%>
			<%
				sv.initBarePrice.setClassString("");
			%>
			<%
				sv.initBidPrice.setClassString("");
			%>
			<%
				sv.initOfferPrice.setClassString("");
			%>
			<%
				sv.accBarePrice.setClassString("");
			%>
			<%
				sv.accBidPrice.setClassString("");
			%>
			<%
				sv.accOfferPrice.setClassString("");
			%>
			<%
				sv.update.setClassString("");
			%>
			<%
				sv.screenIndicArea.setClassString("");
			%>

			<%
				{
								if (appVars.ind01.isOn()) {
									sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
									sv.unitVirtualFund.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind01.isOn()) {
									sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind02.isOn()) {
									sv.initBarePrice.setReverse(BaseScreenData.REVERSED);
									sv.initBarePrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind02.isOn()) {
									sv.initBarePrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind03.isOn()) {
									sv.initBidPrice.setReverse(BaseScreenData.REVERSED);
									sv.initBidPrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind03.isOn()) {
									sv.initBidPrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind04.isOn()) {
									sv.initOfferPrice.setReverse(BaseScreenData.REVERSED);
									sv.initOfferPrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind04.isOn()) {
									sv.initOfferPrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind05.isOn()) {
									sv.accBarePrice.setReverse(BaseScreenData.REVERSED);
									sv.accBarePrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind05.isOn()) {
									sv.accBarePrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind06.isOn()) {
									sv.accBidPrice.setReverse(BaseScreenData.REVERSED);
									sv.accBidPrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind06.isOn()) {
									sv.accBidPrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind07.isOn()) {
									sv.accOfferPrice.setReverse(BaseScreenData.REVERSED);
									sv.accOfferPrice.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind07.isOn()) {
									sv.accOfferPrice.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind08.isOn()) {
									sv.update.setReverse(BaseScreenData.REVERSED);
									sv.update.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind08.isOn()) {
									sv.update.setHighLight(BaseScreenData.BOLD);
								}
							}
			%>

			<div class="row">
				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s5679' width='100%'>
							<thead>
								<tr class='info'>
									<th></th>
									<th colspan="3"><%=smartHF.getLit(generatedText6)%></th>
									<th colspan="3"><%=smartHF.getLit(generatedText7)%></th>
									<th><%=smartHF.getLit(generatedText8)%></th>
								</tr>
								<tr class='info'>
									<th><%=smartHF.getLit(generatedText9)%></th>
									<th><%=smartHF.getLit(generatedText10)%></th>
									<th><%=smartHF.getLit(generatedText11)%></th>
									<th><%=smartHF.getLit(generatedText12)%></th>
									<th><%=smartHF.getLit(generatedText13)%></th>
									<th><%=smartHF.getLit(generatedText14)%></th>
									<th><%=smartHF.getLit(generatedText15)%></th>
									<th><%=smartHF.getLit(generatedText16)%></th>
								</tr>
							</thead>

							<tbody>
								<td><%=smartHF.getHTMLSFSpaceVar(0, 0, sfl, sv.unitVirtualFund)%><%=smartHF.getHTMLF4SSVar(0, 0, sfl, sv.unitVirtualFund)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.initBarePrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.initBidPrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.initOfferPrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.accBarePrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.accBidPrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.accOfferPrice,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%></td>
								<td><%=smartHF.getTableHTMLVarQual(0, 0, sfl, sv.update)%></td>
							</tbody>
						</table>
					</div>

				</div>
			</div>



			<%
				sflLine += 1;
							doingLine++;
							if (doingLine % linesPerCol == 0 && sflcols > 1) {
								sflLine = 0.0;
							}
							S5429screensfl.setNextScreenRow(sfl, appVars, sv);
						}
			%>
		</div>
		<%
			appVars.restoreAllInds(savedInds);
		%>


		<%
			}
		%>


	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>
