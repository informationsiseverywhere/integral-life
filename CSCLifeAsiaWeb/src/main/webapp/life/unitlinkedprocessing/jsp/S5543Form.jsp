

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5543";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5543ScreenVars sv = (S5543ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Available Funds ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Code");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.unitVirtualFund01.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund02.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.unitVirtualFund11.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.unitVirtualFund11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc11.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.unitVirtualFund12.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitVirtualFund12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.vfundesc12.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>

<!-- ILIFE-2596 Life Cross Browser -Coding and UT- Sprint 2 D5: Task 6  starts -->
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<!-- ILIFE-2596 Life Cross Browser -Coding and UT- Sprint 2 D5: Task 6  ends -->


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Available Funds")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Code")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Description")%></label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Months")%></label>
				</div>
			</div>	
			
			
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund01");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund01.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund01' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund01).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc01' type='text'
							<%formatValue = (sv.vfundesc01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc01.getLength()%>'
							maxLength='<%=sv.vfundesc01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc01).getColor() == null ? "input_cell"
						: (sv.vfundesc01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div></div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod01' type='text'
							<%formatValue = (sv.allowperiod01.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod01.getLength()%>'
							maxLength='<%=sv.allowperiod01.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod01)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod01).getColor() == null ? "input_cell"
						: (sv.allowperiod01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund02" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund02");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund02.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund02' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund02).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc02' type='text'
							<%formatValue = (sv.vfundesc02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc02.getLength()%>'
							maxLength='<%=sv.vfundesc02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc02).getColor() == null ? "input_cell"
						: (sv.vfundesc02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod02' type='text'
							<%formatValue = (sv.allowperiod02.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod02.getLength()%>'
							maxLength='<%=sv.allowperiod02.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod02)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod01).getColor() == null ? "input_cell"
						: (sv.allowperiod02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>	
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund03" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund03");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund03.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund03).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund03' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund03).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc03' type='text'
							<%formatValue = (sv.vfundesc03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc03.getLength()%>'
							maxLength='<%=sv.vfundesc03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc03).getColor() == null ? "input_cell"
						: (sv.vfundesc03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			
				<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod03' type='text'
							<%formatValue = (sv.allowperiod03.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod03.getLength()%>'
							maxLength='<%=sv.allowperiod03.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod03)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod03).getColor() == null ? "input_cell"
						: (sv.allowperiod03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund04" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund04");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund04.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund04).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund04' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund04).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc04' type='text'
							<%formatValue = (sv.vfundesc04.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc04.getLength()%>'
							maxLength='<%=sv.vfundesc04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc04).getColor() == null ? "input_cell"
						: (sv.vfundesc04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod04' type='text'
							<%formatValue = (sv.allowperiod04.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod04.getLength()%>'
							maxLength='<%=sv.allowperiod04.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod04)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod04).getColor() == null ? "input_cell"
						: (sv.allowperiod04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund05" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund05");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund05.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund05).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund05' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund05).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc05' type='text'
							<%formatValue = (sv.vfundesc05.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc05.getLength()%>'
							maxLength='<%=sv.vfundesc05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc05).getColor() == null ? "input_cell"
						: (sv.vfundesc05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod05' type='text'
							<%formatValue = (sv.allowperiod05.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod05.getLength()%>'
							maxLength='<%=sv.allowperiod05.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod05)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod05).getColor() == null ? "input_cell"
						: (sv.allowperiod05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund06" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund06");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund06.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund06).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund06' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund06).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc06' type='text'
							<%formatValue = (sv.vfundesc06.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc06.getLength()%>'
							maxLength='<%=sv.vfundesc06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc06).getColor() == null ? "input_cell"
						: (sv.vfundesc06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod06' type='text'
							<%formatValue = (sv.allowperiod06.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod06.getLength()%>'
							maxLength='<%=sv.allowperiod06.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod06)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod06).getColor() == null ? "input_cell"
						: (sv.allowperiod06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund07" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund07");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund07.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund07).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund07' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund07).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund07).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc07' type='text'
							<%formatValue = (sv.vfundesc07.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc07.getLength()%>'
							maxLength='<%=sv.vfundesc07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc07).getColor() == null ? "input_cell"
						: (sv.vfundesc07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod07' type='text'
							<%formatValue = (sv.allowperiod07.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod07.getLength()%>'
							maxLength='<%=sv.allowperiod07.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod07)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod07).getColor() == null ? "input_cell"
						: (sv.allowperiod07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund08" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund08");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund08.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund08).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund08' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund08).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund08).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc08' type='text'
							<%formatValue = (sv.vfundesc08.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc08.getLength()%>'
							maxLength='<%=sv.vfundesc08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc08).getColor() == null ? "input_cell"
						: (sv.vfundesc08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod08' type='text'
							<%formatValue = (sv.allowperiod08.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod08.getLength()%>'
							maxLength='<%=sv.allowperiod08.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod08)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod08).getColor() == null ? "input_cell"
						: (sv.allowperiod08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund09" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund09");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund09.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund09).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund09' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund09).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund09).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc09' type='text'
							<%formatValue = (sv.vfundesc09.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc09.getLength()%>'
							maxLength='<%=sv.vfundesc09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc09).getColor() == null ? "input_cell"
						: (sv.vfundesc09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod09' type='text'
							<%formatValue = (sv.allowperiod09.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod09.getLength()%>'
							maxLength='<%=sv.allowperiod09.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod09)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod09).getColor() == null ? "input_cell"
						: (sv.allowperiod09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund10" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund10");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund10.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund10).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund10' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund10).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund10).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc10' type='text'
							<%formatValue = (sv.vfundesc10.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc10.getLength()%>'
							maxLength='<%=sv.vfundesc10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc10).getColor() == null ? "input_cell"
						: (sv.vfundesc10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod10' type='text'
							<%formatValue = (sv.allowperiod10.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod10.getLength()%>'
							maxLength='<%=sv.allowperiod10.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod10)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod10).getColor() == null ? "input_cell"
						: (sv.allowperiod10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund11" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund11");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund11.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund11.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund11.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund11).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund11' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund11).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund11).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc11' type='text'
							<%formatValue = (sv.vfundesc11.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc11.getLength()%>'
							maxLength='<%=sv.vfundesc11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc11)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc11).getColor() == null ? "input_cell"
						: (sv.vfundesc11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod11' type='text'
							<%formatValue = (sv.allowperiod11.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod11.getLength()%>'
							maxLength='<%=sv.allowperiod11.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod11)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod11).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod11).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod11).getColor() == null ? "input_cell"
						: (sv.allowperiod11).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund12" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund12");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund12.getFormData(), 1, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund12.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.unitVirtualFund12.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund12).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund12' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund12).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund12).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='vfundesc12' type='text'
							<%formatValue = (sv.vfundesc12.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.vfundesc12.getLength()%>'
							maxLength='<%=sv.vfundesc12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(vfundesc12)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.vfundesc12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.vfundesc12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.vfundesc12).getColor() == null ? "input_cell"
						: (sv.vfundesc12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<input name='allowperiod12' type='text'
							<%formatValue = (sv.allowperiod12.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.allowperiod12.getLength()%>'
							maxLength='<%=sv.allowperiod12.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(allowperiod12)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.allowperiod12).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.allowperiod12).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.allowperiod12).getColor() == null ? "input_cell"
						: (sv.allowperiod12).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

