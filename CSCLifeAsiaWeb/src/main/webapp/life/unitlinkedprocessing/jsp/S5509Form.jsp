<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5509";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5509ScreenVars sv = (S5509ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5509screenWritten.gt(0)) {
%>
<%
	S5509screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Initial and Accumulation Units to have same details Y/N ? ");
%>
<%
	sv.initAccumSame.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Initial");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Accumulation");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Bid/Offer Differential ");
%>
<%
	sv.initBidOffer.setClassString("");
%>
<%
	sv.acumbof.setClassString("");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Rounding Of Bid/Offer ");
%>
<%
	sv.initialRounding.setClassString("");
%>
<%
	sv.accumRounding.setClassString("");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Management Charge (%) ");
%>
<%
	sv.managementCharge.setClassString("");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Price change tolerance (%) ");
%>
<%
	sv.tolerance.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind02.isOn()) {
				sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmfrmDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmtoDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.initAccumSame.setReverse(BaseScreenData.REVERSED);
				sv.initAccumSame.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.initAccumSame.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.initBidOffer.setReverse(BaseScreenData.REVERSED);
				sv.initBidOffer.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.initBidOffer.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind06.isOn()) {
				sv.acumbof.setReverse(BaseScreenData.REVERSED);
				sv.acumbof.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind06.isOn()) {
				sv.acumbof.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind07.isOn()) {
				sv.initialRounding.setReverse(BaseScreenData.REVERSED);
				sv.initialRounding.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind07.isOn()) {
				sv.initialRounding.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind08.isOn()) {
				sv.accumRounding.setReverse(BaseScreenData.REVERSED);
				sv.accumRounding.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind08.isOn()) {
				sv.accumRounding.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind09.isOn()) {
				sv.managementCharge.setReverse(BaseScreenData.REVERSED);
				sv.managementCharge.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind09.isOn()) {
				sv.managementCharge.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind55.isOn()) {
				sv.tolerance.setReverse(BaseScreenData.REVERSED);
				sv.tolerance.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind55.isOn()) {
				sv.tolerance.setHighLight(BaseScreenData.BOLD);
			}
		}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 50px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 90px;">
						<%=smartHF.getHTMLVarExt(fw, sv.tabl)%>
						<%=smartHF.getHTMLF4NSVar(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.item)%>
						<%=smartHF.getHTMLVarExt(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<table>
						<tr>
							<td><div class="input-group date form_date col-md-12"
									data-date="" data-date-format="dd/mm/yyyy"
									data-link-field="dobDisp" data-link-format="dd/mm/yyyy"
									style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmfrmDisp, (sv.itmfrmDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div></td>
							<td>&nbsp;&nbsp;</td>
							<td><label style="padding-top: 10px;"><%=smartHF.getLit(generatedText13)%></label></td>
							<td>&nbsp;&nbsp;</td>
							<td><div class="input-group date form_date col-md-12"
									data-date="" data-date-format="dd/mm/yyyy"
									data-link-field="dobDisp" data-link-format="dd/mm/yyyy"
									style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmtoDisp, (sv.itmtoDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label style="padding-top: 8px;"><%=smartHF.getLit(generatedText6)%></label>
				</div>

			</div>
			<div class="col-md-1">
				<div class="form-group">
					<div><%=smartHF.getHTMLVarExt(fw, sv.initAccumSame)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 110px;"><%=smartHF.getHTMLVarExt(fw, sv.initBidOffer, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 110px;"><%=smartHF.getHTMLVarExt(fw, sv.acumbof, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.initialRounding, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.accumRounding, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.managementCharge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 80px;"><%=smartHF.getHTMLVarExt(fw, sv.tolerance, COBOLHTMLFormatter.S2VS2)%></div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5509protectWritten.gt(0)) {
%>
<%
	S5509protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>