

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5544";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5544ScreenVars sv = (S5544ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Switch Basis ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid To Bid ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid To Offer ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bid To Discounted Offer ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Discount");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "%");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Offer To Offer ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid To Fund ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Now/Deferred ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Number of Free Switches ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Per Policy Year(s) ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Carry Forward ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fee ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Flat ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "% ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Min ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Max ");
%>

<%
	{
		if (appVars.ind03.isOn()) {
			sv.btobid.setReverse(BaseScreenData.REVERSED);
			sv.btobid.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.btobid.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.btooff.setReverse(BaseScreenData.REVERSED);
			sv.btooff.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.btooff.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.btodisc.setReverse(BaseScreenData.REVERSED);
			sv.btodisc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.btodisc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.otooff.setReverse(BaseScreenData.REVERSED);
			sv.otooff.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.otooff.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.nowDeferInd.setReverse(BaseScreenData.REVERSED);
			sv.nowDeferInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.nowDeferInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.noOfFreeSwitches.setReverse(BaseScreenData.REVERSED);
			sv.noOfFreeSwitches.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.noOfFreeSwitches.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ppyears.setReverse(BaseScreenData.REVERSED);
			sv.ppyears.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ppyears.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.cfrwd.setReverse(BaseScreenData.REVERSED);
			sv.cfrwd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.cfrwd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.ffamt.setReverse(BaseScreenData.REVERSED);
			sv.ffamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.ffamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.feepc.setReverse(BaseScreenData.REVERSED);
			sv.feepc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.feepc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.feemin.setReverse(BaseScreenData.REVERSED);
			sv.feemin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.feemin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.feemax.setReverse(BaseScreenData.REVERSED);
			sv.feemax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.feemax.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.bidToFund.setReverse(BaseScreenData.REVERSED);
			sv.bidToFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.bidToFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.discountOfferPercent.setReverse(BaseScreenData.REVERSED);
			sv.discountOfferPercent.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.discountOfferPercent.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- ILIFE-2596 Life Cross Browser -Coding and UT- Sprint 2 D5: Task 6  starts -->
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<!-- ILIFE-2596 Life Cross Browser -Coding and UT- Sprint 2 D5: Task 6  ends -->

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group"  style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>









						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Switch Basis")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid To Bid")%></label>
					<input type='checkbox' name='btobid' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(btobid)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.btobid).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.btobid).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.btobid).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck' onclick="handleCheckBox('btobid')" />

					<input type='checkbox' name='btobid' value='N'
						<%if (!(sv.btobid).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('btobid')" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid To Offer")%></label>
					<div style="width: 70px;">
						<input name='btooff' type='text'
							<%if ((sv.btooff).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.btooff.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.btooff.getLength()%>'
							maxLength='<%=sv.btooff.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(btooff)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.btooff).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.btooff).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.btooff).getColor() == null ? "input_cell"
						: (sv.btooff).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid To Discounted Offer")%></label>
					<div style="width: 70px;">
						<input name='btodisc' type='text'
							<%if ((sv.btodisc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.btodisc.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.btodisc.getLength()%>'
							maxLength='<%=sv.btodisc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(btodisc)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.btodisc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.btodisc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.btodisc).getColor() == null ? "input_cell"
						: (sv.btodisc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discount")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("%")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.discountOfferPercent).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
						%>

						<input name='discountOfferPercent' type='text'
							<%if ((sv.discountOfferPercent).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.discountOfferPercent)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.discountOfferPercent);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.discountOfferPercent)%>'
							<%}%> size='<%=sv.discountOfferPercent.getLength()%>'
							maxLength='<%=sv.discountOfferPercent.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(discountOfferPercent)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.discountOfferPercent).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.discountOfferPercent).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.discountOfferPercent).getColor() == null ? "input_cell"
						: (sv.discountOfferPercent).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Offer To Offer")%></label>
					<div style="width: 70px;">
						<input name='otooff' type='text'
							<%if ((sv.otooff).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.otooff.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.otooff.getLength()%>'
							maxLength='<%=sv.otooff.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(otooff)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.otooff).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.otooff).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.otooff).getColor() == null ? "input_cell"
						: (sv.otooff).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bid To Fund")%></label>
					<div style="width: 70px;">
						<input name='bidToFund' type='text'
							<%if ((sv.bidToFund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.bidToFund.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.bidToFund.getLength()%>'
							maxLength='<%=sv.bidToFund.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(bidToFund)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.bidToFund).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.bidToFund).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.bidToFund).getColor() == null ? "input_cell"
						: (sv.bidToFund).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Now/Deferred")%></label>
					<div style="width: 70px;">
						<input name='nowDeferInd' type='text'
							<%if ((sv.nowDeferInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.nowDeferInd.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.nowDeferInd.getLength()%>'
							maxLength='<%=sv.nowDeferInd.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(nowDeferInd)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.nowDeferInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.nowDeferInd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.nowDeferInd).getColor() == null ? "input_cell"
						: (sv.nowDeferInd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Free Switches")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.noOfFreeSwitches).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='noOfFreeSwitches' type='text'
							<%if ((sv.noOfFreeSwitches).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.noOfFreeSwitches)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.noOfFreeSwitches);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.noOfFreeSwitches)%>'
							<%}%> size='<%=sv.noOfFreeSwitches.getLength()%>'
							maxLength='<%=sv.noOfFreeSwitches.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(noOfFreeSwitches)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.noOfFreeSwitches).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.noOfFreeSwitches).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.noOfFreeSwitches).getColor() == null ? "input_cell"
						: (sv.noOfFreeSwitches).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Per Policy Year(s)")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.ppyears).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ppyears' type='text'
							<%if ((sv.ppyears).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.ppyears)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.ppyears);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.ppyears)%>' <%}%>
							size='<%=sv.ppyears.getLength()%>'
							maxLength='<%=sv.ppyears.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(ppyears)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.ppyears).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ppyears).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ppyears).getColor() == null ? "input_cell"
						: (sv.ppyears).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Carry Forward")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cfrwd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cfrwd' type='text'
							<%if ((sv.cfrwd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.cfrwd)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.cfrwd);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.cfrwd)%>' <%}%>
							size='<%=sv.cfrwd.getLength()%>'
							maxLength='<%=sv.cfrwd.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(cfrwd)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.cfrwd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cfrwd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cfrwd).getColor() == null ? "input_cell"
						: (sv.cfrwd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fee")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.ffamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.ffamt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='ffamt' type='text'
							<%if ((sv.ffamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.ffamt.getLength(), sv.ffamt.getScale(), 3)%>'
							maxLength='<%=sv.ffamt.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(ffamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.ffamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.ffamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.ffamt).getColor() == null ? "input_cell"
						: (sv.ffamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("%")%></label>
					<div style="width: 70px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.feepc).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
						%>

						<input name='feepc' type='text'
							<%if ((sv.feepc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.feepc)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.feepc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.feepc)%>' <%}%>
							size='<%=sv.feepc.getLength()%>'
							maxLength='<%=sv.feepc.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(feepc)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.feepc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.feepc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.feepc).getColor() == null ? "input_cell"
						: (sv.feepc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Min")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.feemin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.feemin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='feemin' type='text'
							<%if ((sv.feemin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemin.getLength(), sv.feemin.getScale(), 3)%>'
							maxLength='<%=sv.feemin.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(feemin)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.feemin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.feemin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.feemin).getColor() == null ? "input_cell"
						: (sv.feemin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Max")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.feemax).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.feemax, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='feemax' type='text'
							<%if ((sv.feemax).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.feemax.getLength(), sv.feemax.getScale(), 3)%>'
							maxLength='<%=sv.feemax.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(feemax)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.feemax).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.feemax).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.feemax).getColor() == null ? "input_cell"
						: (sv.feemax).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

