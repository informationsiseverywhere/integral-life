

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5133";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5133ScreenVars sv = (S5133ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Switch Funds");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Component Inquiry");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No  ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Continue to Premium Redirection?");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(Y/N)");
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.redirectionRequired.setReverse(BaseScreenData.REVERSED);
			sv.redirectionRequired.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.redirectionRequired.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>		
				
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Continue to Premium Redirection?")%></label>
					<div style="width: 50px;">
						<input name='redirectionRequired' type='text'
							<%formatValue = (sv.redirectionRequired.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.redirectionRequired.getLength()%>'
							maxLength='<%=sv.redirectionRequired.getLength()%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(redirectionRequired)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.redirectionRequired).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.redirectionRequired).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.redirectionRequired).getColor() == null ? "input_cell"
						: (sv.redirectionRequired).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Switch Funds")%>
					</b></label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Component Inquiry")%>
					</b></label>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "D")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("AFR Maintenance")%>
					</b></label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label class="radio-inline"><%=smartHF.buildRadioOption(sv.action, "action", "E")%>
						<b><%=resourceBundleHandler.gettingValueFromBundle("Multi Switch Funds")%>
					</b></label>
				</div>
			</div>
		</div>
		
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%></label>
						<div>
							<input name='action' type='hidden'
								value='<%=sv.action.getFormData()%>'
								size='<%=sv.action.getLength()%>'
								maxLength='<%=sv.action.getLength()%>' class="input_cell"
								onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
								onKeyUp='return checkMaxLength(this)'>
						</div>
					</div>
				</div>
			</div>
		</div>
