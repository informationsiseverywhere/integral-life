<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5530";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5530ScreenVars sv = (S5530ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5530screenWritten.gt(0)) {
%>
<%
	S5530screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid From ");
%>
<%
	sv.itmfrmDisp.setClassString("");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	sv.itmtoDisp.setClassString("");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Statutory Fund ");
%>
<%
	sv.statfund.setClassString("");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	sv.statSect.setClassString("");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sub-section ");
%>
<%
	sv.stsubsect.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind01.isOn()) {
				sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmfrmDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind01.isOn()) {
				sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
				sv.itmtoDisp.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind02.isOn()) {
				sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind03.isOn()) {
				sv.statfund.setReverse(BaseScreenData.REVERSED);
				sv.statfund.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.statfund.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind04.isOn()) {
				sv.statSect.setReverse(BaseScreenData.REVERSED);
				sv.statSect.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind04.isOn()) {
				sv.statSect.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.stsubsect.setReverse(BaseScreenData.REVERSED);
				sv.stsubsect.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind05.isOn()) {
				sv.stsubsect.setHighLight(BaseScreenData.BOLD);
			}
		}
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarExt(fw, sv.tabl)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.item)%>
						<%=smartHF.getHTMLVarExt(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
					<table>
						<tr>
							<td>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmfrmDisp, (sv.itmfrmDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=smartHF.getLit(generatedText6)%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<div class="input-group date form_date col-md-12" data-date=""
									data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
									data-link-format="dd/mm/yyyy" style="width: 150px;">
									<%=smartHF.getRichTextDateInput(fw, sv.itmtoDisp, (sv.itmtoDisp.getLength()))%>
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
					<div class="input-group" style="width: 110px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.statfund, (sv.statfund.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('statfund')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label>
					<div class="input-group" style="width: 110px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.statSect, (sv.statSect.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('statSect')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText9)%></label>
					<div class="input-group" style="width: 110px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.stsubsect, (sv.stsubsect.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('stsubsect')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5530protectWritten.gt(0)) {
%>
<%
	S5530protect.clearClassString(sv);
%>

<%
	{
		}
%>


<%
	}
%>



<%@ include file="/POLACommon2NEW.jsp"%>
