<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH608";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%Sh608ScreenVars sv = (Sh608ScreenVars) fw.getVariables();%>

<%if (sv.Sh608screenWritten.gt(0)) {%>
	<%Sh608screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1234567890123456789012345678901");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"January");%>
	<%sv.daywh01.setClassString("");%>
<%	sv.daywh01.appendClassString("string_fld");
	sv.daywh01.appendClassString("input_txt");
	sv.daywh01.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"WE = Weekend");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"February");%>
	<%sv.daywh02.setClassString("");%>
<%	sv.daywh02.appendClassString("string_fld");
	sv.daywh02.appendClassString("input_txt");
	sv.daywh02.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"March");%>
	<%sv.daywh03.setClassString("");%>
<%	sv.daywh03.appendClassString("string_fld");
	sv.daywh03.appendClassString("input_txt");
	sv.daywh03.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"H = Holiday");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"April");%>
	<%sv.daywh04.setClassString("");%>
<%	sv.daywh04.appendClassString("string_fld");
	sv.daywh04.appendClassString("input_txt");
	sv.daywh04.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"May");%>
	<%sv.daywh05.setClassString("");%>
<%	sv.daywh05.appendClassString("string_fld");
	sv.daywh05.appendClassString("input_txt");
	sv.daywh05.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D = Half Day");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"June");%>
	<%sv.daywh06.setClassString("");%>
<%	sv.daywh06.appendClassString("string_fld");
	sv.daywh06.appendClassString("input_txt");
	sv.daywh06.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"July");%>
	<%sv.daywh07.setClassString("");%>
<%	sv.daywh07.appendClassString("string_fld");
	sv.daywh07.appendClassString("input_txt");
	sv.daywh07.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"T = Transaction Date");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"August");%>
	<%sv.daywh08.setClassString("");%>
<%	sv.daywh08.appendClassString("string_fld");
	sv.daywh08.appendClassString("input_txt");
	sv.daywh08.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"September");%>
	<%sv.daywh09.setClassString("");%>
<%	sv.daywh09.appendClassString("string_fld");
	sv.daywh09.appendClassString("input_txt");
	sv.daywh09.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"X = Unavailable");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"October");%>
	<%sv.daywh10.setClassString("");%>
<%	sv.daywh10.appendClassString("string_fld");
	sv.daywh10.appendClassString("input_txt");
	sv.daywh10.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"November");%>
	<%sv.daywh11.setClassString("");%>
<%	sv.daywh11.appendClassString("string_fld");
	sv.daywh11.appendClassString("input_txt");
	sv.daywh11.appendClassString("highlight");
%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,". = Normal Working Day");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"December");%>
	<%sv.daywh12.setClassString("");%>
<%	sv.daywh12.appendClassString("string_fld");
	sv.daywh12.appendClassString("input_txt");
	sv.daywh12.appendClassString("highlight");
%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Working Day");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>
<!-- ILIFE-2618 Life Cross Browser -Coding and UT- Sprint 2 D6: Task 6  starts -->
<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
}
</style>
<!-- ILIFE-2618 Life Cross Browser -Coding and UT- Sprint 2 D6: Task 6  ends -->




<div class="panel panel-default">
<div class="panel-body">

<div class="row">
	<div class="col-md-4">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>	
			<div>
				<%					
				if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.company.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.company.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'   style="  max-width: 35px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  </div>  
	</div>
	</div>
	

	<div class="col-md-4">
	<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	
			<div>  		
				<%					
				if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="max-width: 73px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
	  	</div>
	
	</div>
	</div>


	<div class="col-md-3">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		
		<div class="input-group">	  		
				<%					
				if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.item.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.item.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  
	
			
		  		
				<%					
				if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="max-width: 210px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
	  </div>
  
  </div>
  </div>
 
</div>

<!-- </div>
</div> -->

<%-- 	<%=smartHF.getLit(7, 30, generatedText4)%> --%>

<%-- 	<%=smartHF.getLit(7, 40, generatedText5)%> --%>

<%-- 	<%=smartHF.getLit(7, 50, generatedText6)%> --%>

<%-- 	<%=smartHF.getLit(8, 21.5, generatedText7).replace("style='", "style=\'font-family:\"Courier New\", Courier, monospace; font-size:14px;")%> --%>

<%-- 	<%=smartHF.getLit(10, 8, generatedText8)%> --%>

<%-- 	<%=smartHF.getHTMLVar(10, 21, fw, sv.daywh01).replace("style='", "style=\'font-family:\"Courier New\", Courier, monospace; font-size:13px;").replace("width:*px", "width:255px")%> --%>

<%-- 	<%=smartHF.getLit(10, 57, generatedText20)%> --%>

<%-- 	<%=smartHF.getLit(11, 8, generatedText9)%> --%>

<%-- 	<%=smartHF.getHTMLVar(11, 21, fw, sv.daywh02)%> --%>

<%-- 	<%=smartHF.getLit(12, 8, generatedText10)%> --%>

<%-- 	<%=smartHF.getHTMLVar(12, 21, fw, sv.daywh03)%> --%>

<%-- 	<%=smartHF.getLit(12, 58, generatedText21)%> --%>

<%-- 	<%=smartHF.getLit(13, 8, generatedText11)%> --%>

<%-- 	<%=smartHF.getHTMLVar(13, 21, fw, sv.daywh04)%> --%>

<%-- 	<%=smartHF.getLit(14, 8, generatedText12)%> --%>

<%-- 	<%=smartHF.getHTMLVar(14, 21, fw, sv.daywh05)%> --%>

<%-- 	<%=smartHF.getLit(14, 58, generatedText22)%> --%>

<%-- 	<%=smartHF.getLit(15, 8, generatedText13)%> --%>

<%-- 	<%=smartHF.getHTMLVar(15, 21, fw, sv.daywh06)%> --%>

<%-- 	<%=smartHF.getLit(16, 8, generatedText14)%> --%>

<%-- 	<%=smartHF.getHTMLVar(16, 21, fw, sv.daywh07)%> --%>

<%-- 	<%=smartHF.getLit(16, 58, generatedText23)%> --%>

<%-- 	<%=smartHF.getLit(17, 8, generatedText15)%> --%>

<%-- 	<%=smartHF.getHTMLVar(17, 21, fw, sv.daywh08)%> --%>

<%-- 	<%=smartHF.getLit(18, 8, generatedText16)%> --%>

<%-- 	<%=smartHF.getHTMLVar(18, 21, fw, sv.daywh09)%> --%>

<%-- 	<%=smartHF.getLit(18, 58, generatedText24)%> --%>

<%-- 	<%=smartHF.getLit(19, 8, generatedText17)%> --%>

<%-- 	<%=smartHF.getHTMLVar(19, 21, fw, sv.daywh10)%> --%>

<%-- 	<%=smartHF.getLit(20, 8, generatedText18)%> --%>

<%-- 	<%=smartHF.getHTMLVar(20, 21, fw, sv.daywh11)%> --%>

<%-- 	<%=smartHF.getLit(20, 58, generatedText25)%> --%>

<%-- 	<%=smartHF.getLit(21, 8, generatedText19)%> --%>

<%-- 	<%=smartHF.getHTMLVar(21, 21, fw, sv.daywh12)%> --%>

<%-- 	<%=smartHF.getLit(21, 62, generatedText26)%> --%>

<div class="row" style=" margin-top: 60px;">
	<div class="col-md-12">
		<div class="form-group" >
			<!-- <div class="table-responsive"> -->
			<div class="row" style="margin-left: 58px;">
				<div class="col-md-3"></div>
				<div class="col-md-1">
				<div class="form-group"><%=generatedText4 %></div>
				</div>
				
				<div class="col-md-1">
				<div class="form-group"><%=generatedText5 %></div>
				</div>
				
				<div class="col-md-1">
				<div class="form-group"><%=generatedText6 %></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6" style="margin-left: 15px;">
				<div class=""><%=generatedText7 %></div><!-- font-size changed by pmujavadiya -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">			
					<div class="form-group"><%=generatedText8 %></div>
				</div>
			<!-- </div> -->
			
				<% 
				if((new Byte((sv.daywh01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class='col-md-4'>
				
						<input name='daywh01' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh01.getFormData() %>" maxLength='50'
							 onHelp='return fieldHelp("daywh01")' disabled="true">
					<%-- <div id='daywh01' 
						
						onHelp='return fieldHelp("daywh01")'><%=sv.daywh01.getFormData() %>
					</div> --%>
				</div>
				<%} else { %>
				<div class='col-md-4'>
					<div >
						<input name='daywh01' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							
							value="<%=sv.daywh01.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh01")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
				<div class='col-md-4'>
					<div><%=generatedText20 %></div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="form-group"><%=generatedText9 %></div>
				</div>
			
				<% 
				if((new Byte((sv.daywh02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class='col-md-4'>		
				
					<%-- <div id='daywh02'				
						onHelp='return fieldHelp("daywh02")'><%=sv.daywh02.getFormData() %>
					</div> --%>
					<input name='daywh02' type='text'	
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
							value="<%=sv.daywh02.getFormData() %>" maxLength='50'
							onHelp='return fieldHelp("daywh02")' disabled="true">
				</div>
				<%} else { %>
				<div class='col-md-4'>
					<div >
						<input name='daywh02' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh02.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh02")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="form-group"><%=generatedText10 %></div>
				</div>
			
				<% 
				if((new Byte((sv.daywh03).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class='col-md-4'>
					<%-- <div id='daywh03' class='form-group'						
						onHelp='return fieldHelp("daywh03")'><%=sv.daywh03.getFormData() %></div> --%>
						
						<input name='daywh03' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'							
							value="<%=sv.daywh03.getFormData() %>" maxLength='50'
							 onHelp='return fieldHelp("daywh03")' disabled="true">						
				</div>
				
				<%} else { %>
				<div class='col-md-4'>
					<div >
						<input name='daywh03' type='text'	
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
							value="<%=sv.daywh03.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh03")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
					<%} %>
			
					<div class='col-md-4'>
						<div><%=generatedText21 %></div>
					</div>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div><%=generatedText11 %></div>
				</div>
				<% 
				if((new Byte((sv.daywh04).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">
					<%-- <div id='daywh04' onHelp='return fieldHelp("daywh04")'><%=sv.daywh04.getFormData() %></div> --%>
					<input name='daywh04' type='text'
					
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
												
							value="<%=sv.daywh04.getFormData() %>" maxLength='50'
							 onHelp='return fieldHelp("daywh04")' disabled="true">
				</div>
				<%} else { %>
				<div class="col-md-4">
					<div>
						<input name='daywh04' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'							
							value="<%=sv.daywh04.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh04")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
			
			</div>
			<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-2">			
						<div ><%=generatedText12 %></div>
					</div>
			
					<% 
					if((new Byte((sv.daywh05).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>
					<div class="col-md-4">
						<%-- <div id='daywh05' 
							
							onHelp='return fieldHelp("daywh05")'><%=sv.daywh05.getFormData() %></div> --%>
							
							<input name='daywh05' type='text'
								style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'								
								value="<%=sv.daywh05.getFormData() %>" maxLength='50'
								 onHelp='return fieldHelp("daywh05")'
								 disabled="true">
					</div>
					<%} else { %>
					<div class="col-md-4">
						<div>
							<input name='daywh05' type='text'
								style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
								value="<%=sv.daywh05.getFormData() %>" maxLength='50'
								onFocus='doFocus(this)' onHelp='return fieldHelp("daywh05")'
								onKeyUp="return checkMaxLength(this)" style='text-align:left'
								class='input_cell  string_fld input_txt highlight'>
						</div>
					</div>
						<%} %>
			
					<div class="col-md-4">
						<div ><%=generatedText22 %></div>
					</div>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">	
					<div><%=generatedText13 %></div>
				</div>
				
					<% 
					if((new Byte((sv.daywh06).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>
				<div class="col-md-4">				
					<input name='daywh06' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'							
							value="<%=sv.daywh06.getFormData() %>" maxLength='50'
							onHelp='return fieldHelp("daywh06")' disabled="true">		
					<%-- <div id='daywh06' 
						onHelp='return fieldHelp("daywh06")'><%=sv.daywh06.getFormData() %></div> --%>
				</div>
				<%} else { %>
				<div class="col-md-4">
					<div>
						<input name='daywh06' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'							
							value="<%=sv.daywh06.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh06")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
				
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">				
					<div><%=generatedText14 %></div>
				</div>
					<% 
					if((new Byte((sv.daywh07).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>
					<div class="col-md-4">
						<%-- <div id='daywh07' 
							onHelp='return fieldHelp("daywh07")'><%=sv.daywh07.getFormData() %></div> --%>
							
							<input name='daywh07' type='text'
								style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
								value="<%=sv.daywh07.getFormData() %>" maxLength='50'
								 onHelp='return fieldHelp("daywh07")' disabled="true">							
					</div>
					<%} else { %>
					<div class="col-md-4">
					<div>
						<input name='daywh07' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh07.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh07")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
					</div>
					<%} %>
					<div class="col-md-4">
						<div>T = Transaction Date</div>
					</div>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div><%=generatedText15 %></div>
				</div>
					
				<% 
				if((new Byte((sv.daywh08).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">
				<%-- <div id='daywh08' class='input_cell  string_fld input_txt highlight'
					style='position: absolute; top: 294px; margin-top: 15px;left: 168px; width: 255px; height: 19px;
						font-family: "Courier New", Courier, monospace; font-size: 13px; font-weight:bold;'
					onHelp='return fieldHelp("daywh08")'><%=sv.daywh08.getFormData() %></div> --%>
					
					<input name='daywh08' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
						value="<%=sv.daywh08.getFormData() %>" maxLength='50'
						 onHelp='return fieldHelp("daywh08")' disabled="true">
					
				</div>
				<%} else { %>
				<div class="col-md-4">
					<div>
						<input name='daywh08' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'							
							value="<%=sv.daywh08.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh08")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
			
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div><%=generatedText16 %></div>
				</div>
				
				<% 
				if((new Byte((sv.daywh09).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">
					<%-- <div id='daywh09' class='input_cell  string_fld input_txt highlight'
						style='position: absolute; top: 314px;margin-top: 15px; left: 168px; width: 255px; height: 19px;
							font-family: "Courier New", Courier, monospace; font-size: 13px; font-weight:bold;'
						onHelp='return fieldHelp("daywh09")'><%=sv.daywh09.getFormData() %></div> --%>
						
						<input name='daywh09' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
						value="<%=sv.daywh09.getFormData() %>" maxLength='50'
						onHelp='return fieldHelp("daywh09")' disabled="">
				</div>
				<%} else { %>
				<div class="col-md-4">
					<div>
						<input name='daywh09' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh09.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh09")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
					<%} %>
				
				<div class="col-md-4">
					<div><%=generatedText24 %></div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div><%=generatedText17 %></div>
				</div>
				
				<% 
				if((new Byte((sv.daywh10).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">
				<%-- <div id='daywh10' class='input_cell  string_fld input_txt highlight'
					style='position: absolute; top: 334px;margin-top: 15px; left: 168px; width: 255px; height: 19px;
						font-family: "Courier New", Courier, monospace; font-size: 13px; font-weight:bold;'
					onHelp='return fieldHelp("daywh10")'><%=sv.daywh10.getFormData() %></div> --%>
					<input name='daywh10' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
						value="<%=sv.daywh10.getFormData() %>" maxLength='50'
						 onHelp='return fieldHelp("daywh10")' disabled="true">
				</div>
									
				<%} else { %>
				<div class="col-md-4">
				<div>
					<input name='daywh10' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
						value="<%=sv.daywh10.getFormData() %>" maxLength='50'
						onFocus='doFocus(this)' onHelp='return fieldHelp("daywh10")'
						onKeyUp="return checkMaxLength(this)" style='text-align:left'
						class='input_cell  string_fld input_txt highlight'>
				</div>
				</div>
				<%} %>
			
			</div>
			<div class="row">
			<div class="col-md-1"></div>
				<div class="col-md-2">				
					<div><%=generatedText18 %></div>
				</div>
				
				
				<% 
				if((new Byte((sv.daywh11).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">	
					<%-- <div id='daywh11' 
						onHelp='return fieldHelp("daywh11")'><%=sv.daywh11.getFormData() %></div> --%>
						<input name='daywh11' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
						value="<%=sv.daywh11.getFormData() %>" maxLength='50'
						onHelp='return fieldHelp("daywh11")' disabled="true">
				</div>
				<%} else { %>
				<div class="col-md-4">	
					<div>
						<input name='daywh11' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh11.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh11")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
				
				<div class="col-md-4">	
					<div><%=generatedText25 %></div><!-- "overflow-y: hidden;" removed by pmujavadiya  -->
				</div>
			
			</div>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-2">			
					<div><%=generatedText19 %></div>
				</div>
				
				<% 
				if((new Byte((sv.daywh12).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>
				<div class="col-md-4">
				<%-- <div id='daywh12' class='input_cell  string_fld input_txt highlight'
					'
					onHelp='return fieldHelp("daywh12")'><%=sv.daywh12.getFormData() %></div> --%>
					
					<input name='daywh12' type='text'
						style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'						
						value="<%=sv.daywh12.getFormData() %>" maxLength='50'
						onHelp='return fieldHelp("daywh12")' disabled="true">
				</div>
				<%} else { %>
				<div class="col-md-4">
					<div>
						<input name='daywh12' type='text'
							style='font-size: 13px; padding-left: 19px;font-family: "Courier New", Courier, monospace;'
							value="<%=sv.daywh12.getFormData() %>" maxLength='50'
							onFocus='doFocus(this)' onHelp='return fieldHelp("daywh12")'
							onKeyUp="return checkMaxLength(this)" style='text-align:left'
							class='input_cell  string_fld input_txt highlight'>
					</div>
				</div>
				<%} %>
			</div>

		 <!-- </div> -->
	 </div>
 </div>

</div>
<%}%>

</div>
</div>
<!-- <div -->
<%-- 	style='position: absolute; overflow-y: hidden; top: 424px; left: 496px; width: 88px; height: 16px;'><%=generatedText26 %></div> --%>




<%if (sv.Sh608protectWritten.gt(0)) {%>
	<%Sh608protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>


<%@ include file="/POLACommon2NEW.jsp"%>
