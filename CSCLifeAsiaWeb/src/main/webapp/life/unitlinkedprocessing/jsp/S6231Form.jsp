<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6231";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S6231ScreenVars sv = (S6231ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.initBarePrice.setReverse(BaseScreenData.REVERSED);
			sv.initBarePrice.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.initBarePrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.initBidPrice.setReverse(BaseScreenData.REVERSED);
			sv.initBidPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.initBidPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.initBidPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.initOfferPrice.setReverse(BaseScreenData.REVERSED);
			sv.initOfferPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.initOfferPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.initOfferPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.accBarePrice.setReverse(BaseScreenData.REVERSED);
			sv.accBarePrice.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.accBarePrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.accBidPrice.setReverse(BaseScreenData.REVERSED);
			sv.accBidPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind13.isOn()) {
			sv.accBidPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.accBidPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.accOfferPrice.setReverse(BaseScreenData.REVERSED);
			sv.accOfferPrice.setColor(BaseScreenData.RED);
		}
		if (appVars.ind14.isOn()) {
			sv.accOfferPrice.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.accOfferPrice.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.update.setReverse(BaseScreenData.REVERSED);
			sv.update.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.update.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Initial      Units");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Accumulation  Units");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Last");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bare");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bid");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Offer");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Update");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind09.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.jobno.setReverse(BaseScreenData.REVERSED);
			sv.jobno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.jobno.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div>
						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job No")%></label>
					<div>
						<%
							qpsf = fw.getFieldXMLDef((sv.jobno).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.jobno);

							if (!((sv.jobno.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue.replace(",","")) %>
						</div>  
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<%
			GeneralTable sfl = fw.getTable("s6231screensfl");
		%>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<%-- <div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div> --%>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-s6231' width='100%'>
							<thead>
								<tr class='info'>
									<th class="text-center" rowspan="2"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
									<th class="text-center" colspan="3"><%=resourceBundleHandler.gettingValueFromBundle("Initial Units")%></th>
									<th class="text-center" colspan="3"><%=resourceBundleHandler.gettingValueFromBundle("Accumulation Units")%></th>
									<th class="text-center" rowspan="2"><%=resourceBundleHandler.gettingValueFromBundle("Last Update")%></th>
								</tr>
								<tr class='info'>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Bare")%></th>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></th>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></th>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Bare")%></th>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></th>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									S6231screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S6231screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
								<td>
									<%=sv.unitVirtualFund.getFormData()%>



									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.initBarePrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.initBarePrice,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 		if (!sv.initBarePrice.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>




									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.initBidPrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.initBidPrice,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <input type='text' maxLength='<%=sv.initBidPrice.getLength()%>'
										<%if ((sv.initBidPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <%}%> value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.initBidPrice.getLength(),
						sv.initBidPrice.getScale(), 3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(s6231screensfl.initBidPrice)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s6231screensfl" + "." + "initBidPrice" + "_R" + count%>'
										id='<%="s6231screensfl" + "." + "initBidPrice" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.initBidPrice.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event,true);'
										 
										title='<%=formatValue%>'>



									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.initOfferPrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.initOfferPrice,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <input type='text'
										maxLength='<%=sv.initOfferPrice.getLength()%>'
										<%if ((sv.initOfferPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <%}%> value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.initOfferPrice.getLength(),
						sv.initOfferPrice.getScale(), 3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(s6231screensfl.initOfferPrice)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s6231screensfl" + "." + "initOfferPrice" + "_R" + count%>'
										id='<%="s6231screensfl" + "." + "initOfferPrice" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.initOfferPrice.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event,true);'
										 
										title='<%=formatValue%>'>



									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.accBarePrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.accBarePrice,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 		if (!sv.accBarePrice.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>




									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.accBidPrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.accBidPrice,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <input type='text' maxLength='<%=sv.accBidPrice.getLength()%>'
										<%if ((sv.accBidPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <%}%> value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.accBidPrice.getLength(), sv.accBidPrice.getScale(),
						3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(s6231screensfl.accBidPrice)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s6231screensfl" + "." + "accBidPrice" + "_R" + count%>'
										id='<%="s6231screensfl" + "." + "accBidPrice" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.accBidPrice.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event,true);'
										title='<%=formatValue%>'>



									</td>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.accOfferPrice).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.accOfferPrice,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%> <input type='text'
										maxLength='<%=sv.accOfferPrice.getLength()%>'
										<%if ((sv.accOfferPrice).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <%}%> value='<%=formatValue%>'
										size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.accOfferPrice.getLength(),
						sv.accOfferPrice.getScale(), 3)%>'
										onFocus='doFocus(this),onFocusRemoveCommas(this)'
										onHelp='return fieldHelp(s6231screensfl.accOfferPrice)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="s6231screensfl" + "." + "accOfferPrice" + "_R" + count%>'
										id='<%="s6231screensfl" + "." + "accOfferPrice" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.accOfferPrice.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event,true);'									
										title='<%=formatValue%>' >
									</td>
									<td><%=sv.update.getFormData()%></td>

								</tr>

								<%
									count = count + 1;
										S6231screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<!-- ILIFE-7549-- Start --> 
<!-- <script>
	$(document).ready(function() {
		$('#dataTables-s6231').DataTable({
			ordering : false,
			searching : false,
			scrollY : "400px",
			scrollCollapse : true,
			scrollX : true,
			pageLength:100 

		});
		/* $('#load-more').appendTo($('.col-sm-6:eq(-1)')); */
	});
</script> -->
<!-- ILIFE-7549-- End --> 
<%@ include file="/POLACommon2NEW.jsp"%>
