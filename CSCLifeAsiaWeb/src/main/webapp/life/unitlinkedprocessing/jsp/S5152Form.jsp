

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5152";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5152ScreenVars sv = (S5152ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life No ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cov. No ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider No ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life Insured ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Coverage Table ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub Section ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Joint Life ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "    Premium ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Split Plan ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"% or Prem Amnt ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Allocation ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "% / Amount");
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.crtable.setReverse(BaseScreenData.REVERSED);
			sv.crtable.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.zagelit.setReverse(BaseScreenData.REVERSED);
			sv.zagelit.setColor(BaseScreenData.RED);
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.instprem.setReverse(BaseScreenData.REVERSED);
			sv.instprem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.instprem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.virtFundSplitMethod.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.virtFundSplitMethod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			sv.virtFundSplitMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.virtFundSplitMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.percOrAmntInd.setReverse(BaseScreenData.REVERSED);
			sv.percOrAmntInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.percOrAmntInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.unitAllocPercAmt01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.unitAllocPercAmt01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitAllocPercAmt01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.unitVirtualFund02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.unitVirtualFund02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.unitAllocPercAmt02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.unitAllocPercAmt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.unitAllocPercAmt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.unitVirtualFund03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.unitAllocPercAmt03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.unitAllocPercAmt03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitAllocPercAmt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.unitVirtualFund04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.unitAllocPercAmt04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.unitAllocPercAmt04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.unitAllocPercAmt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.unitVirtualFund05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.unitAllocPercAmt05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
			sv.unitAllocPercAmt05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.unitAllocPercAmt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind24.isOn()) {
			sv.unitVirtualFund06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.unitAllocPercAmt06.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind26.isOn()) {
			sv.unitAllocPercAmt06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.unitAllocPercAmt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind28.isOn()) {
			sv.unitVirtualFund07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.unitAllocPercAmt07.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.unitAllocPercAmt07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.unitAllocPercAmt07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.unitVirtualFund08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.unitAllocPercAmt08.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.unitAllocPercAmt08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.unitAllocPercAmt08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind36.isOn()) {
			sv.unitVirtualFund09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.unitAllocPercAmt09.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind38.isOn()) {
			sv.unitAllocPercAmt09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.unitAllocPercAmt09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind40.isOn()) {
			sv.unitVirtualFund10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.unitAllocPercAmt10.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.unitAllocPercAmt10.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind42.isOn()) {
			sv.unitAllocPercAmt10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.unitAllocPercAmt10.setHighLight(BaseScreenData.BOLD);
		}
	if (appVars.ind49.isOn()) {
			sv.zafropt1.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind50.isOn()) {
			sv.zafritem.setEnabled(BaseScreenData.DISABLED);
		}
		//ILIFE-8164 -STARTS			 
		if (appVars.ind101.isOn()) {
			sv.newFundList01.setInvisibility(BaseScreenData.INVISIBLE);
		}
						
		if (appVars.ind102.isOn()) {
			sv.newFundList02.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind103.isOn()) {
			sv.newFundList03.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind104.isOn()) {
			sv.newFundList04.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind105.isOn()) {
			sv.newFundList05.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind106.isOn()) {
			sv.newFundList06.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind107.isOn()) {
			sv.newFundList07.setInvisibility(BaseScreenData.INVISIBLE);
		}
			
		if (appVars.ind108.isOn()) {
			sv.newFundList08.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind109.isOn()) {
			sv.newFundList09.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind110.isOn()) {
			sv.newFundList10.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind111.isOn()) {
			sv.newFundList11.setInvisibility(BaseScreenData.INVISIBLE);
		}
				
		if (appVars.ind112.isOn()) {
			sv.newFundList12.setInvisibility(BaseScreenData.INVISIBLE);
		}	
		//ILIFE-8164  -ENDS
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:60px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				</div>
			</div><div class="col-md-4"></div>
			<div class="col-md-4">
			<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Code")%></label>
					<div class="input-group" style="max-width:100px">
				
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			
			</div></div>
		</div>
</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<div class="input-group" style="max-width:100px"><%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div></div>
			</div>
			<div class="col-md-4"></div>
				
				
			<div class="col-md-4">
				<div class="form-group">
				<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "label_txt" %>'>
				<%=resourceBundleHandler.gettingValueFromBundle("Age Last Birthday")%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  


	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
			</div>
		</div>

		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
				<!-- 	<div class="input-group three-controller"> -->
				<table><tr><td>
						<%
							if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



</td><td>





						<%
							if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlinsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlinsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> --></td></tr></table>
				</div>
			</div>
		</div>
		
		
		
		<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Number")%></label>
    	 					

		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Number")%></label>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider Number")%></label>
	
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Statutory Fund")%></label>

  		
		<%					
		if(!((sv.statfund.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statfund.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statfund.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>

	
		<%					
		if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>

	<%					
		if(!((sv.stsubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.stsubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.stsubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				</div>
    	 					
    	 					
    	 					
		<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label>
								<%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%>
								</label>
					    		     <div class="input-group">
						    		
	
		<%	
			qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.instprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" >	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label>
							<%=resourceBundleHandler.gettingValueFromBundle("Fund Split Plan")%>
							</label>
							<div class="input-group" style="min-width:300px">
						    		<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"virtFundSplitMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("virtFundSplitMethod");
	optionValue = makeDropDownList( mappedItems , sv.virtFundSplitMethod.getFormData(),2,resourceBundleHandler);
	longValue = (String) mappedItems.get((sv.virtFundSplitMethod.getFormData()).toString().trim());
%>
<%=smartHF.getDropDownExt(sv.virtFundSplitMethod, fw, longValue, "virtFundSplitMethod", optionValue, 0, 200) %>

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label>
							<%=resourceBundleHandler.gettingValueFromBundle("% or Premium Amount")%>
							</label>
							<div class="input-group">
						    		
<% 
   longValue=sv.percOrAmntInd.getFormData();
   if("".equals(longValue)){
   longValue="Select";
   }else if("P".equals(longValue)){
    longValue="P";}
   else if("A".equals(longValue)){
    longValue="A";
    }
	if((new Byte((sv.percOrAmntInd.getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()))){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
<% if("red".equals((sv.percOrAmntInd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:70px;"> 
					<%
					} 
					%>
					<select name='percOrAmntInd' type='list' style="width:90px;"
					<% 
				if((new Byte((sv.percOrAmntInd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.percOrAmntInd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
	class = 'input_cell' 
			<%
				} 
			%>
			>
					
					<option value="" <%if("".equals(sv.percOrAmntInd.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
					<option value="P" <%if("P".equals(sv.percOrAmntInd.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("P")%></option>
					<option value="A" <%if("A".equals(sv.percOrAmntInd.getFormData())){ %> SELECTED<%}%> ><%=resourceBundleHandler.gettingValueFromBundle("A")%></option>
					</select>
					<% if("red".equals((sv.percOrAmntInd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					} 
					%>
	
				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label>
								<%=resourceBundleHandler.gettingValueFromBundle("Auto Fund Rebalancing")%>
								</label>
					    		     <div class="input-group">
						    			<%
			fieldItem = appVars.loadF4FieldsLong(
					new String[] { "zafropt1" }, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("zafropt1");
			optionValue = makeDropDownList(mappedItems, sv.zafropt1
					.getFormData(), 2);
		longValue = (String) mappedItems.get((sv.zafropt1.getFormData()).toString().trim());  
		%>
	
		<% 
			if((new Byte((sv.zafropt1).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
		%>  
		<%=smartHF.getDropDownExt(sv.virtFundSplitMethod, fw, longValue, "zafropt1", optionValue, 0, 170) %>	
		
		
			<% }else {%>

<% if("red".equals((sv.zafropt1).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
		
		<select name='zafropt1' type='list' style="width: 140px;"
<% 
	if((new Byte((sv.zafropt1).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zafropt1).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
			<%=optionValue%>
		</select>
<% if("red".equals((sv.zafropt1).getColor())){
%>
</div>
<%
}  
%> 
		
		<%
			} 
		%> 

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label>
							<%=resourceBundleHandler.gettingValueFromBundle("AFR Freq/Risk Prof")%>
							</label>
							<div class="input-group">
						    		
<%
			fieldItem = appVars.loadF4FieldsLong(
					new String[] { "zafritem" }, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("zafritem");
			optionValue = makeDropDownList(mappedItems, sv.zafritem
					.getFormData(), 2);
		longValue = (String) mappedItems.get((sv.zafritem.getFormData()).toString().trim());  
		%>
		<input id="afrval" type="hidden" value ="<%=sv.zafritem.getFormData().toString().trim() %>" />
		<% 
			if((new Byte((sv.zafritem).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
		<%=smartHF.getDropDownExt(sv.virtFundSplitMethod, fw, longValue, "zafritem", optionValue, 0, 170) %>		
			<% }else {%>

<% if("red".equals((sv.zafritem).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
		
		<select name='zafritem' id='zafritem' type='list' style="width: 140px;"
<% 
	if((new Byte((sv.zafritem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zafritem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
			<%=optionValue%>
		</select>
<% if("red".equals((sv.zafritem).getColor())){
%>
</div>
<%
}  
%> 
		
		<%
			} 
		%> 
				      			     </div>
						</div>
				   </div>		
			
			    
		    </div>
				   
		
		
		
 <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5152' width='100%'>
						<thead>
							<tr class='info'>
			       	<th colspan="4"><center><%=resourceBundleHandler.gettingValueFromBundle("Allocation")%></center></th>
		</tr>
		<tr>     							
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center></th>			<!-- ILIFE-3189 by vjain60  -->
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
				<th><center><%=resourceBundleHandler.gettingValueFromBundle("Percentage/Amount")%></center></th><!-- ilife-3584 -->
			<th><center><%=resourceBundleHandler.gettingValueFromBundle("Price")%></center></th>
			
			
	
		 	        </tr>
			 </thead>
			
		
					<script language="javascript">
					        $(document).ready(function(){
								var rows = $("table[id='dataTables-s5152']").find("tr:not(:hidden):gt(1)").length + 2;
								var isPageDown = 1;
								var pageSize = 1;
								var fields = new Array("unitAllocPercAmt");
								operateTableForSuperTable(rows,isPageDown,pageSize,fields,"dataTables-s5152",null,2);	 
					        });
					    </script>
						<tbody>
    
			
	<tr style="height: 20px; background: #FFFFFF ;<% if (sv.unitAllocPercAmt01.getFormData().trim().equals("")) 
														{%> display:none;<%} %>" id='tablerow1'>
		<td class="tableDataTag" style="width:30px;" align="center">
		<!-- smalchi2 for ILIFE-1032 -->
		<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R1' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R1' class="UICheck" />
			<%} %>
			<!-- ENDS-->
		</td>
		
		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
<!-- ILIFE-8164-START-->						
								<%
						if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund01"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund01");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund01, fw, longValue, "unitVirtualFund01", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->							
			<div id="unitVirtualFund01Div">
			<%	
			if ((new Byte((sv.unitVirtualFund01).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund01"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund01");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund01.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund01, fw, longValue, "unitVirtualFund01", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
		<!-- smalchi2 for ILIFE-1032 added protected in if condition STARTS -->
		<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt01).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt01' id='unitAllocPercAmt01' type='text' style="text-align:right"  <%--ILIFE-2997 style added by pmujavadiya --%>
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt01.getLength(),sv.unitAllocPercAmt01.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt01.getLength()%>'
			onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(unitAllocPercAmt01)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt01).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt01).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt01).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt01).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice01).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice01' id='unitBidPrice01' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice01.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice01)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice01).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice01).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice01).getColor() == null ? "input_cell"
								: (sv.unitBidPrice01).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
				</td>
		</tr>
		<tr style="height: 22px;background: #ededed ; <%if(sv.unitVirtualFund02.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow2'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R2' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R2' class="UICheck" />
			<%} %>
			<!-- ENDS -->
</td>
		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
	<!-- ILIFE-8164-START-->								
								<%
						if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList02).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund02"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund02");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund02, fw, longValue, "unitVirtualFund02", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->		
			<div id="unitVirtualFund02Div">
			<%	
			if ((new Byte((sv.unitVirtualFund02).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund02"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund02");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund02.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund02, fw, longValue, "unitVirtualFund02", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
		<td class="tableDataTag" style="width:120px;" align="center">


			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt02).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt02' id='unitAllocPercAmt02' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt02.getLength(),sv.unitAllocPercAmt02.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt02.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt02)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt02).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt02).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt02).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt02).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice02).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice02' id='unitBidPrice02' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice02.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice02)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice02).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 	
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice02).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice02).getColor() == null ? "input_cell"
								: (sv.unitBidPrice02).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
		<tr style="height: 22px;background: #FFFFFF ; <%if(sv.unitVirtualFund03.getFormData().trim().equals("")){%> display:none; <%} %>"  id='tablerow3'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
			<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R3' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R3' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
	<!-- ILIFE-8164-START-->								
									<%
						if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList03).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund03"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund03");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund03, fw, longValue, "unitVirtualFund03", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->						
			<div id="unitVirtualFund03Div">
			<%	
			if ((new Byte((sv.unitVirtualFund03).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund03"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund03");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund03.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund03, fw, longValue, "unitVirtualFund03", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt03).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt03' id='unitAllocPercAmt03' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt03.getLength(),sv.unitAllocPercAmt03.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt03.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt03)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt03).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())){%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt03).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt03).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt03).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
				<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice03).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice03'  id='unitBidPrice03' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				
				
				size="20px" maxLength='<%=sv.unitBidPrice03.getLength()%>'
				
				
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice03)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice03).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())){
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice03).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice03).getColor() == null ? "input_cell"
								: (sv.unitBidPrice03).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
	<tr style="height: 22px;  background: #ededed; <%if(sv.unitVirtualFund04.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow4'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
			<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R4' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R4' class="UICheck" />
			<%} %>
			
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
		<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList04).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund04"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund04");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund04, fw, longValue, "unitVirtualFund04", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->					
			<div id="unitVirtualFund04Div">
			<%	
			if ((new Byte((sv.unitVirtualFund04).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund04"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund04");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund04.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund04, fw, longValue, "unitVirtualFund04", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
		<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt04).getFieldName());
				//qpsf
				//		.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt04' id='unitAllocPercAmt04' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt04.getLength(),sv.unitAllocPercAmt04.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt04.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt04)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt04).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())){%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt04).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt04).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt04).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice04).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice04' id='unitBidPrice04' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice04.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice04)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice04).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice04).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice04).getColor() == null ? "input_cell"
								: (sv.unitBidPrice04).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
		<tr style="height: 22px;  background: #FFFFFF; <%if(sv.unitVirtualFund05.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow5'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
			<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R5' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R5' class="UICheck" />
			<%} %>
			<!-- ENDS -->
				</td>
		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList05).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund05"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund05");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund05, fw, longValue, "unitVirtualFund05", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->							
			<div id="unitVirtualFund05Div">
			<%	
			if ((new Byte((sv.unitVirtualFund05).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund05"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund05");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund05.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund05, fw, longValue, "unitVirtualFund05", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
		<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt05).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt05' id='unitAllocPercAmt05' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt05.getLength(),sv.unitAllocPercAmt05.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt05.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt05)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt05).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0)|| (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt05).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt05).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt05).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice05).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice05' id='unitBidPrice05' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice05.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice05)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice05).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice05).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice05).getColor() == null ? "input_cell"
								: (sv.unitBidPrice05).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
		<tr style="height: 22px;  background: #ededed; <%if(sv.unitVirtualFund06.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow6'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
			<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R6' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R6' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>
		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList06).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund06"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund06");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund06, fw, longValue, "unitVirtualFund06", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
<!-- ILIFE-8164-ENDS-->						
			<div id="unitVirtualFund06Div">
			<%	
			if ((new Byte((sv.unitVirtualFund06).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund06"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund06");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund06.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund06, fw, longValue, "unitVirtualFund06", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
		<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt06).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt06' id='unitAllocPercAmt06' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt06.getLength(),sv.unitAllocPercAmt06.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt06.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt06)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt06).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt06).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt06).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt06).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice06).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice06' id='unitBidPrice06' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice06.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice06)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice06).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())){
					// ILIFE-1702 ENDS
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice06).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice06).getColor() == null ? "input_cell"
								: (sv.unitBidPrice06).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
	<tr style="height: 22px;  background: #FFFFFF;<%if(sv.unitVirtualFund07.getFormData().trim().equals(""))	{%> display:none; <%} %>" id='tablerow7'>
		<td class="tableDataTag" style="width:30px;" align="center">
		<!-- smalchi2 for ILIFE-1032 -->
		<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R7' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R7' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
		<!-- ILIFE-8164-START-->								
								<%
						if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList07).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund07"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund07");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund07, fw, longValue, "unitVirtualFund07", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->
			<div id="unitVirtualFund07Div">
			<%	
			if ((new Byte((sv.unitVirtualFund07).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund07"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund07");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund07.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund07, fw, longValue, "unitVirtualFund07", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
				<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt07).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt07,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt07' id='unitAllocPercAmt07' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt07.getLength(),sv.unitAllocPercAmt07.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt07.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt07)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt07).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt07).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt07).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt07).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
				<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice07).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice07,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice07' id='unitBidPrice07' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice07.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice07)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice07).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())){
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice07).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice07).getColor() == null ? "input_cell"
								: (sv.unitBidPrice07).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
	<tr style="height: 22px;  background: #ededed;<%if(sv.unitVirtualFund08.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow8'>
		<td class="tableDataTag" style="width:30px;" align="center">
		<!-- smalchi2 for ILIFE-1032 -->
		<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R8' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R8' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
		<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList08).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund08"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund08");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund08, fw, longValue, "unitVirtualFund08", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->					
			<div id="unitVirtualFund08Div">
			<%	
			if ((new Byte((sv.unitVirtualFund08).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund08"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund08");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund08.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund08, fw, longValue, "unitVirtualFund08", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
	<td class="tableDataTag" style="width:120px;" align="center">


			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt08).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt08,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt08' id='unitAllocPercAmt08' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt08.getLength(),sv.unitAllocPercAmt08.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt08.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt08)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt08).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt08).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt08).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt08).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
		<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice08).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice08,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice08' id='unitBidPrice08' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice08.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice08)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice08).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice08).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice08).getColor() == null ? "input_cell"
								: (sv.unitBidPrice08).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
	<tr style="height: 22px;  background: #FFFFFF;  <%if(sv.unitVirtualFund09.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow9'>
		<td class="tableDataTag" style="width:30px;" align="center">
		<!-- smalchi2 for ILIFE-1032 -->
		<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R9' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R9' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
	<!-- ILIFE-8164-START-->							
								<%
						if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList09).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund09"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund09");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund09, fw, longValue, "unitVirtualFund09", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->						
			<div id="unitVirtualFund09Div">
			<%	
			if ((new Byte((sv.unitVirtualFund09).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund09"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund09");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund09.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund09, fw, longValue, "unitVirtualFund09", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
	<td class="tableDataTag" style="width:120px;" align="center">


			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt09).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt09,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt09' id='unitAllocPercAmt09' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt09.getLength(),sv.unitAllocPercAmt09.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt09.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt09)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt09).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt09).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt09).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt09).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
		<td class="tableDataTag" style="width:120px;" align="center">
				<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice09).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice09,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice09' id='unitBidPrice09' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice09.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice09)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice09).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())){
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice09).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice09).getColor() == null ? "input_cell"
								: (sv.unitBidPrice09).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
		<tr style="height: 22px;  background: #ededed;<%if(sv.unitVirtualFund10.getFormData().trim().equals("")){%> display:none; <%} %>" id='tablerow10'>
			<td class="tableDataTag" style="width:30px;" align="center">
			<!-- smalchi2 for ILIFE-1032 -->
			<%if ((fw.getVariables().isScreenProtected())) {%>
					<INPUT type="checkbox" name="chk_R" id='chk_R10' class="UICheck" disabled/>
					<%} else {%>
					
			<INPUT type="checkbox" name="chk_R" id='chk_R10' class="UICheck" />
			<%} %>
			<!-- ENDS -->
			</td>

		<td class="tableDataTag" style="width:250px;" align="left" valign="top">
	<!-- ILIFE-8164-START-->						
								<%
						if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
								<div style="width: 210px;">
										<%	
											if ((new Byte((sv.newFundList10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
												fieldItem = appVars.loadF4FieldsLong(new String[]{"unitVirtualFund10"}, sv, "E", baseModel);
												Set<String> set = new HashSet<String>();
												for(FixedLengthStringData value: sv.newFundList){
													if(value != null)
														set.add(value.toString());
												}
												mappedItems = (Map) fieldItem.get("unitVirtualFund10");
												mappedItems.keySet().retainAll(set);
												optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 2,
														resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
												%>
												<%=smartHF.getDropDownExt(sv.unitVirtualFund10, fw, longValue, "unitVirtualFund10", optionValue)%>
												
										<%
											}
										%>
									</div>
								
							<%}else{ %>	
	<!-- ILIFE-8164-ENDS-->				
			<div id="unitVirtualFund10Div">
			<%	
			if ((new Byte((sv.unitVirtualFund10).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
			fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund10"},sv,"E",baseModel); 
			mappedItems = (Map) fieldItem.get("unitVirtualFund10");
			optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund10.getFormData(),2,resourceBundleHandler);  
			longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());  
			%>
				<%=smartHF.getDropDownExt(sv.unitVirtualFund10, fw, longValue, "unitVirtualFund10", optionValue, 0, 160) %><!--ILIFE-1725 -->
			<%}%>
			</div>
			<%}%>
		</td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt10).getFieldName());
				//qpsf
					//	.setPicinHTML(COBOLHTMLFormatter.S15VS2);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitAllocPercAmt10,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitAllocPercAmt10' id='unitAllocPercAmt10' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>'
				<%}%> size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.unitAllocPercAmt10.getLength(),sv.unitAllocPercAmt10.getScale(),3)%>'
				maxLength='<%=sv.unitAllocPercAmt10.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'
				onHelp='return fieldHelp(unitAllocPercAmt10)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if (((new Byte((sv.unitAllocPercAmt10).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) || (fw.getVariables().isScreenProtected())) {%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitAllocPercAmt10).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitAllocPercAmt10).getColor() == null ? "input_cell"
								: (sv.unitAllocPercAmt10).getColor().equals(
										"red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>></td>
			<td class="tableDataTag" style="width:120px;" align="center">
			<%
				qpsf = fw.getFieldXMLDef((sv.unitBidPrice10).getFieldName());
				//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
				valueThis = smartHF.getPicFormatted(qpsf, sv.unitBidPrice10,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			%> <input name='unitBidPrice10' id='unitBidPrice10' type='text' style="text-align:right"
				value='<%=valueThis%>'
				<%
			if (valueThis != null && valueThis.trim().length() > 0) {%>
				title='<%=valueThis%>' <%}%>
				size="20px" maxLength='<%=sv.unitBidPrice10.getLength()%>'
				onFocus='doFocus(this),onFocusRemoveCommas(this)'	 onHelp='return fieldHelp(unitBidPrice10)'
				onKeyUp='return checkMaxLength(this)'
				onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
				<%if ((new Byte((sv.unitBidPrice10).getEnabled()))
						// ILIFE-1702 STARTS BY SLAKKALA 
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| (fw.getVariables().isScreenProtected())) {
					// ILIFE-1702 ENDS 
					%>
				readonly="true" class="output_cell"
				<%} else if ((new Byte((sv.unitBidPrice10).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
				class="bold_cell" <%} else {%>
				class=' <%=(sv.unitBidPrice10).getColor() == null ? "input_cell"
								: (sv.unitBidPrice10).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
				<%}%>>
		</tr>
		</tbody>
		</table>
		
		



</div></div></div>

<div class="row">
			<div class="col-md-4"  >
				<div class="form-group" >
					<div class="btn-group">
						<div class="sectionbutton">
							<p style="font-size: 12px; font-weight: bold;">
								<a id="subfile_add" class="btn btn-success" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
								<a id="subfile_remove" class="btn btn-danger"
									href='javascript:;' disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
							</p>
						</div>
					</div>
				</div>
			</div></div>

</div></div> 
<script>
	$(document).ready(function() {
		$('#dataTables-s5152').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
        	scrollX: true,
      	});
		$(".dataTables_info").hide();

    });
</script>



<%@ include file="/POLACommon2NEW.jsp"%>

