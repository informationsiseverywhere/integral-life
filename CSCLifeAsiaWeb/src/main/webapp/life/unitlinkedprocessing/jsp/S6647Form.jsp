<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6647";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>

<%S6647ScreenVars sv = (S6647ScreenVars) fw.getVariables();%>

<%if (sv.S6647screenWritten.gt(0)) {%>
	<%S6647screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Switch Rules ");%>
	<%sv.swmeth.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Enhanced Allocation ");%>
	<%sv.enhall.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date for transaction ");%>
	<%sv.efdcode.setClassString("");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allocation ");%>
	<%sv.aloind.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deallocation ");%>
	<%sv.dealin.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Sequence No ");%>
	<%sv.procSeqNo.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Statement Method ");%>
	<%sv.unitStatMethod.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Cancellation Order ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Up to Month)");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Negative Units ");%>
	<%sv.monthsNegUnits.setClassString("");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Debt ");%>
	<%sv.monthsDebt.setClassString("");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lapse ");%>
	<%sv.monthsLapse.setClassString("");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Error ");%>
	<%sv.monthsError.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Use Bid or Offer Price? (B/O) ");%>
	<%sv.bidoffer.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.swmeth.setReverse(BaseScreenData.REVERSED);
			sv.swmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.swmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.enhall.setReverse(BaseScreenData.REVERSED);
			sv.enhall.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.enhall.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.efdcode.setReverse(BaseScreenData.REVERSED);
			sv.efdcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.efdcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.aloind.setReverse(BaseScreenData.REVERSED);
			sv.aloind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.aloind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.dealin.setReverse(BaseScreenData.REVERSED);
			sv.dealin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.dealin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.procSeqNo.setReverse(BaseScreenData.REVERSED);
			sv.procSeqNo.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.procSeqNo.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.unitStatMethod.setReverse(BaseScreenData.REVERSED);
			sv.unitStatMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.unitStatMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.monthsNegUnits.setReverse(BaseScreenData.REVERSED);
			sv.monthsNegUnits.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.monthsNegUnits.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.monthsDebt.setReverse(BaseScreenData.REVERSED);
			sv.monthsDebt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.monthsDebt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.monthsLapse.setReverse(BaseScreenData.REVERSED);
			sv.monthsLapse.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.monthsLapse.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.monthsError.setReverse(BaseScreenData.REVERSED);
			sv.monthsError.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.monthsError.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.bidoffer.setReverse(BaseScreenData.REVERSED);
			sv.bidoffer.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.bidoffer.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
		<div class="panel-body">

		 <div class="row">
		<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>

  		
			<%					
			if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
								} else {
				formatValue = formatValue( longValue);
			}
						
		}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
  
					</div>
				</div>


	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			
			
			<%					
			if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
								} else {
				formatValue = formatValue( longValue);
			}
						
		}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
				    </div>
	</div>
				 
				
	
	<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			<table>
			<tr>
			<td>
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  			</td>
  			<td>
  
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width: 150px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
			%>
  			</td>
  			</tr>
  			</table>
					</div>
				</div>
	
			</div>
		
	<div class="row">

	<div class="col-md-4">
				<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
			
				   <table>
					<tr>
					<td>
			<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	  	</td>
	  	<td>
  
			<label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>
		</td>

		<td style="padding-left: 5px;">
	
  		
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
				</div>
			</div>

	</div>


      <div class="row">


	<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Switch Rules")%></label>
			<div class="input-group" style="width: 100px;">
					<input name='swmeth' id="swmeth"
			type='text'
			
			<%
			
		formatValue = (sv.swmeth.getFormData()).toString();
			
			%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
			
			size='<%= sv.swmeth.getLength()%>'
			maxLength='<%= sv.swmeth.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(swmeth)' onKeyUp='return checkMaxLength(this)'  
			
			
			<% 
	if((new Byte((sv.swmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
			%>  
	readonly="true"
	class="output_cell"
			<%
	}else if((new Byte((sv.swmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
		class="bold_cell" >
					<%--  <a href="javascript:;" onClick="doFocus(document.getElementById('swmeth')); changeF4Image(this); doAction('PFKEY04')"> 
			<img  src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
		 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('swmeth')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
		
					
			
			<%
	}else { 
			%>
			
	class = ' <%=(sv.swmeth).getColor()== null  ? 
			"input_cell" :  (sv.swmeth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
			 <%--  <a href="javascript:;" onClick="doFocus(document.getElementById('swmeth')); changeF4Image(this); doAction('PFKEY04')"> 
			<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('swmeth')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
			
			<%
	} 
			%>
			
			<!-- ILIFE-1325 ends -->
					</div>
			</div>
		</div>
		

	<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Enhanced Allocation")%></label>

					<%	
	longValue = sv.enhall.getFormData();  
			%>
			
			<% 
	if((new Byte((sv.enhall).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
			
			<%
			longValue = null;
			%>
			<% }else {%> 
			<div class="input-group" style="width: 100px;">
			<input name='enhall' id="enhall"
			type='text' 
			value='<%=sv.enhall.getFormData()%>' 
			maxLength='<%=sv.enhall.getLength()%>' 
			size='<%=sv.enhall.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(enhall)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
	if((new Byte((sv.enhall).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
	}else if((new Byte((sv.enhall).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
			%>	
			class="bold_cell" >
 
			<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('enhall')); changeF4Image(this); doAction('PFKEY04')"> 
			<img style="margin-left: 0.5px;"src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('enhall')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
			</div>
			<%
	}else { 
			%>
			<div class="input-group" style="width: 100px;">
			class = ' <%=(sv.enhall).getColor()== null  ? 
			"input_cell" :  (sv.enhall).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
			
			<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('enhall')); changeF4Image(this); doAction('PFKEY04')"> 
			<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('enhall')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
			</div>
			<%}} %>

					</div>
			</div>

		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Effective date for transaction")%></label>


					<%	
	longValue = sv.efdcode.getFormData();  
				%>
				
				<% 
	if((new Byte((sv.efdcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<div class="input-group" style="width: 100px;">
				<input name='efdcode' id="efdcode"
				type='text' 
				value='<%=sv.efdcode.getFormData()%>' 
				maxLength='<%=sv.efdcode.getLength()%>' 
				size='<%=sv.efdcode.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(efdcode)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
	if((new Byte((sv.efdcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
	}else if((new Byte((sv.efdcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
				%>	
				class="bold_cell" >
 
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('efdcode')); changeF4Image(this); doAction('PFKEY04')"> 
				<img style="margin-left: 0.5px; " src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('efdcode')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
				</div>
				<%
	}else { 
				%>
				<div class="input-group" style="width: 100px;">
				class = ' <%=(sv.efdcode).getColor()== null  ? 
				"input_cell" :  (sv.efdcode).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
				
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('efdcode')); changeF4Image(this); doAction('PFKEY04')"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
 <span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('efdcode')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
				</div>
				<%}} %>

					</div>
			</div>

		</div>
 
  <div class="row">


	<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Allocation")%></label>

					<%	
	longValue = sv.aloind.getFormData();  
				%>
				
				<% 
	if((new Byte((sv.aloind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<div class="input-group" style="width: 100px;">
				<input name='aloind' id="aloind"
				type='text' 
				value='<%=sv.aloind.getFormData()%>' 
				maxLength='<%=sv.aloind.getLength()%>' 
				size='<%=sv.aloind.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(aloind)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
	if((new Byte((sv.aloind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
	}else if((new Byte((sv.aloind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
				%>	
				class="bold_cell" >
 
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('aloind')); changeF4Image(this); doAction('PFKEY04')"> 
				<img style="margin-left: 0.5px; " src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
				<span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('aloind')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
				</div>
				<%
	}else { 
				%>
				<div class="input-group" style="width: 100px;">
				class = ' <%=(sv.aloind).getColor()== null  ? 
				"input_cell" :  (sv.aloind).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
                           
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('aloind')); changeF4Image(this); doAction('PFKEY04')"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
				<span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('aloind')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
					</div>
				<%}} %>


			</div>
	 </div>

		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Deallocation")%></label>

					<%	
	longValue = sv.dealin.getFormData();  
				%>
				
				<% 
	if((new Byte((sv.dealin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%>  
				<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
				
				<%
				longValue = null;
				%>
				<% }else {%> 
				<div class="input-group" style="width: 100px;">
				<input name='dealin'  id="dealin"
				type='text' 
				value='<%=sv.dealin.getFormData()%>' 
				maxLength='<%=sv.dealin.getLength()%>' 
				size='<%=sv.dealin.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(dealin)' onKeyUp='return checkMaxLength(this)'  
				
				<% 
	if((new Byte((sv.dealin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
				readonly="true"
				class="output_cell"	 >
				
				<%
	}else if((new Byte((sv.dealin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
				%>	
				class="bold_cell" >
 
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('dealin')); changeF4Image(this); doAction('PFKEY04')"> 
				<img style="margin-left: 0.5px;" src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
				<span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('dealin')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
				</div>
				<%
	}else { 
				%>
				<div class="input-group" style="width: 100px;">
				class = ' <%=(sv.dealin).getColor()== null  ? 
				"input_cell" :  (sv.dealin).getColor().equals("red") ? 
				"input_cell red reverse" : "input_cell" %>' >
                           
				<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('dealin')); changeF4Image(this); doAction('PFKEY04')"> 
				<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
				</a> --%>
				<span class="input-group-btn">
	               <button class="btn btn-info" type="button"
	                   onClick="doFocus(document.getElementById('dealin')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
				</div>
				<%}} %>


					</div>
			</div>

		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Processing Sequence No")%></label>



					<%	
			qpsf = fw.getFieldXMLDef((sv.procSeqNo).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
				
				<input name='procSeqNo' 
				type='text' style="width: 100px;"
				
	value='<%=smartHF.getPicFormatted(qpsf,sv.procSeqNo) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.procSeqNo);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.procSeqNo) %>'
	 <%}%>
				
				size='<%= sv.procSeqNo.getLength()%>'
				maxLength='<%= sv.procSeqNo.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(procSeqNo)' onKeyUp='return checkMaxLength(this)'  
				
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
				
				<% 
	if((new Byte((sv.procSeqNo).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
				%>  
	readonly="true"
					class="output_cell"
				<%
	}else if((new Byte((sv.procSeqNo).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
	}else { 
				%>
				
	class = ' <%=(sv.procSeqNo).getColor()== null  ? 
			"input_cell" :  (sv.procSeqNo).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
				<%
	} 
				%>
				>
			</div>
	 </div>


	</div>
	
	<div class="row">

	<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Unit Statement Method")%></label>

					<%	
	longValue = sv.unitStatMethod.getFormData();  
			%>
			
			<% 
	if((new Byte((sv.unitStatMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
			
			<%
			longValue = null;
			%>
			<% }else {%> 
			<div class="input-group" style="width: 100px;">
			<input name='unitStatMethod' id="unitStatMethod"
			type='text' 
			value='<%=sv.unitStatMethod.getFormData()%>' 
			maxLength='<%=sv.unitStatMethod.getLength()%>' 
			size='<%=sv.unitStatMethod.getLength()%>'
			onFocus='doFocus(this)' onHelp='return fieldHelp(unitStatMethod)' onKeyUp='return checkMaxLength(this)'  
			
			<% 
	if((new Byte((sv.unitStatMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
			%>  
			readonly="true"
			class="output_cell"	 >
			
			<%
	}else if((new Byte((sv.unitStatMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
			%>	
			class="bold_cell" >
 
			<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('unitStatMethod')); changeF4Image(this); doAction('PFKEY04')"> 
			<img style="margin-left: 0.5px;" src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
			<span class="input-group-btn">
               <button class="btn btn-info" type="button"
                   onClick="doFocus(document.getElementById('unitStatMethod')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
			</div>
			<%
	}else { 
			%>
			<div class="input-group" style="width: 100px;">
			class = ' <%=(sv.unitStatMethod).getColor()== null  ? 
			"input_cell" :  (sv.unitStatMethod).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' >
                           
			<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('unitStatMethod')); changeF4Image(this); doAction('PFKEY04')"> 
			<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
			</a> --%>
			
			<span class="input-group-btn">
               <button class="btn btn-info" type="button"
                   onClick="doFocus(document.getElementById('unitStatMethod')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
					</div>
			<%}} %>
			
			
				</div>
		    </div>
<!-- 		    <div class="col-md-1"></div> -->
		     <div class="col-md-3">
		    <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Monies Date Indicator")%></label>
 <div class="input-group" style="min-width:80px;">


<input name='moniesDate' 
type='text'

<%

		formatValue = (sv.moniesDate.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.moniesDate.getLength()%>'
maxLength='<%= sv.moniesDate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(moniesDate)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.moniesDate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.moniesDate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.moniesDate).getColor()== null  ? 
			"input_cell" :  (sv.moniesDate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
		    </div>
		  </div>  	
		  
		  <div class="row">
		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Unit Cancellation Order")%></label>
			</div>
		</div>
		<div class="col-md-4">
		    <div class="form-group">
				<label>(<%=resourceBundleHandler.gettingValueFromBundle("Up")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Month")%>)</label>
			</div>
		</div>	
		</div>
	
	<div class="row">
		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Negative")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Units")%>&nbsp;</label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
				<%=smartHF.getHTMLVar(fw, sv.monthsNegUnits, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>	
		</div>
	
	<div class="row">
		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Debt")%></label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
				<%=smartHF.getHTMLVar(fw, sv.monthsDebt, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>	
		</div>
	
	<div class="row">
		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Lapse")%>&nbsp;</label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
				<%=smartHF.getHTMLVar(fw, sv.monthsLapse, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>	
		</div>
	
	<div class="row">
		<div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Error")%>&nbsp;</label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
				<%=smartHF.getHTMLVar(fw, sv.monthsError, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>	
		</div>
	
	<div class="row">
        <div class="col-md-4">
		    <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Use")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Bid")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("or")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Offer")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Price")%>?&nbsp;(B/O)&nbsp;</label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
				<%=smartHF.getHTMLVar(fw, sv.bidoffer, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replaceAll("width:10px","width:28px")%>
			</div>
		</div>	
		</div>




	</div>
</div>




<%}%>

<%if (sv.S6647protectWritten.gt(0)) {%>
	<%S6647protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<%@ include file="/POLACommon2NEW.jsp"%>

