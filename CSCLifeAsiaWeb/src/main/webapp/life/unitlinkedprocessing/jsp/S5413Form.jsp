<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5413";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5413ScreenVars sv = (S5413ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.ffnddsc.setReverse(BaseScreenData.REVERSED);
			sv.ffnddsc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ffnddsc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.initut.setReverse(BaseScreenData.REVERSED);
			sv.initut.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.initut.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.initut.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.acumut.setReverse(BaseScreenData.REVERSED);
			sv.acumut.setColor(BaseScreenData.RED);
		}
		if (appVars.ind06.isOn()) {
			sv.acumut.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.acumut.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.updateDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.updateDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.updateDateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Job No ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund Description");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Initial");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Accumulation");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Last");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Units");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Units");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Update");
%>
<%
	appVars.rollup(new int[] { 93 });
%>
<%
	{
		if (appVars.ind05.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.jobno.setReverse(BaseScreenData.REVERSED);
			sv.jobno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.jobno.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="input-group">
					<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 35px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div class="input-group" style="width:100px;">
					<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Job No")%></label>
					<div class="input-group">
					<%	
			qpsf = fw.getFieldXMLDef((sv.jobno).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.jobno);
			
			if(!((sv.jobno.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue).replace(",","") %>
				</div>
		<%
			} else {
		%>
		<!-- .replace(",","") -->
				<div class="blank_cell" > &nbsp; </div> 
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[5];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.unitVirtualFund.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
			} else {		
				calculatedValue = (sv.unitVirtualFund.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.ffnddsc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
			} else {		
				calculatedValue = (sv.ffnddsc.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.initut.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
						} else {		
							calculatedValue = (sv.initut.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.acumut.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
						} else {		
							calculatedValue = (sv.acumut.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.updateDateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.updateDateDisp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s5413screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<%-- <div id="load-more" class="col-md-offset-10">
							<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
								style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
							</a>
						</div> --%>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id='dataTables-s5413' width='100%'>
								<thead>
									<tr class='info'>
										<th><center> <%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
		         								
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Description")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Initial Units")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Accumulation Units")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Last Update")%></center></th>
									</tr>
								</thead>

								<tbody>
									<%
	S5413screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5413screensfl
	.hasMoreScreenRows(sfl)) {	
%>
<tr>
<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																
									
											
						<%= sv.unitVirtualFund.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																
									
											
						<%= sv.ffnddsc.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="center">									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.initut).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.initut);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.initut.getLength()%>'
						<%if((sv.initut).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						 value='<%=formatValue %>' 
						 size='<%=sv.initut.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5413screensfl.initut)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5413screensfl" + "." +
						 "initut" + "_R" + count %>'
						 id='<%="s5413screensfl" + "." +
						 "initut" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.initut.getLength()*12%> px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="center">									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.acumut).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.acumut);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.acumut.getLength()%>'
						 <%if((sv.acumut).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						 value='<%=formatValue %>' 
						 size='<%=sv.acumut.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s5413screensfl.acumut)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s5413screensfl" + "." +
						 "acumut" + "_R" + count %>'
						 id='<%="s5413screensfl" + "." +
						 "acumut" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.acumut.getLength()*12%> px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																
									
											
						<%= sv.updateDateDisp.getFormData()%>
						
														 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5413screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->



<script>
	$(document).ready(function() {
		$('#dataTables-s5413').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

