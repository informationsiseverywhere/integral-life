

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5510";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>

<%
	S5510ScreenVars sv = (S5510ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Percentage");
%>

<%
	{
		if (appVars.ind21.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund01.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.unitVirtualFund02.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.unitVirtualFund02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.unitVirtualFund03.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.unitVirtualFund03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.unitVirtualFund04.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.unitVirtualFund04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.unitVirtualFund05.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.unitVirtualFund05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.unitVirtualFund06.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.unitVirtualFund06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.unitVirtualFund07.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.unitVirtualFund07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.unitVirtualFund08.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.unitVirtualFund08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.unitVirtualFund09.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.unitVirtualFund09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.unitVirtualFund10.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.unitVirtualFund10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.unitPremPercent01.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.unitPremPercent01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.unitPremPercent02.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.unitPremPercent02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.unitPremPercent03.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.unitPremPercent03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.unitPremPercent04.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.unitPremPercent04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.unitPremPercent05.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.unitPremPercent05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.unitPremPercent06.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.unitPremPercent06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.unitPremPercent07.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.unitPremPercent07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.unitPremPercent08.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.unitPremPercent08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.unitPremPercent09.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.unitPremPercent09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.unitPremPercent10.setReverse(BaseScreenData.REVERSED);
			sv.unitPremPercent10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.unitPremPercent10.setHighLight(BaseScreenData.BOLD);
		}

	}

	int sfLength = 0;
%>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


</td><td>






						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></label>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund01");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund01.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund01' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund01).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div><div class="col-md-4"></div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent01.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent01' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent01)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent01)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent01).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent01).getColor() == null ? "input_cell"
						: (sv.unitPremPercent01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund02" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund02");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund02.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund02' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund02).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div><div class="col-md-4"></div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent02).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent02.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent02' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent02)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent02)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent02).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent02).getColor() == null ? "input_cell"
						: (sv.unitPremPercent02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund03" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund03");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund03.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund03).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund03' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund03).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent03).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent03.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent03' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent03)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent03)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent03).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent03).getColor() == null ? "input_cell"
						: (sv.unitPremPercent03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund04" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund04");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund04.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund04).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund04' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund04).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent04).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent04.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent04' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent04)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent04)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent04).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent04).getColor() == null ? "input_cell"
						: (sv.unitPremPercent04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund05" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund05");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund05.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund05).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund05' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund05).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent05).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent05.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent05' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent05)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent05)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent05).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent05).getColor() == null ? "input_cell"
						: (sv.unitPremPercent05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund06" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund06");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund06.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund06).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund06' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund06).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent06).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent06.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent06' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent06)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent06)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent06).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent06).getColor() == null ? "input_cell"
						: (sv.unitPremPercent06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund07" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund07");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund07.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund07).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund07' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund07).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund07).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent07).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent07.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent07' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent07)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent07)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent07).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent07).getColor() == null ? "input_cell"
						: (sv.unitPremPercent07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund08" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund08");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund08.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund08).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund08' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund08).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund08).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent08).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent08.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent08' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent08)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent08)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent08).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent08).getColor() == null ? "input_cell"
						: (sv.unitPremPercent08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund09" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund09");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund09.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund09).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund09' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund09).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund09).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent09).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent09.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent09' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent09)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent09)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent09)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent09).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent09).getColor() == null ? "input_cell"
						: (sv.unitPremPercent09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "unitVirtualFund10" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund10");
							optionValue = makeDropDownList(mappedItems, sv.unitVirtualFund10.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.unitVirtualFund10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.unitVirtualFund10).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
							<%
								}
							%>

							<select name='unitVirtualFund10' type='list'
								style="width: 170px;"
								<%if ((new Byte((sv.unitVirtualFund10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.unitVirtualFund10).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.unitVirtualFund10).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unitPremPercent10).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
							sfLength = sv.unitPremPercent10.getLength();
							if (qpsf.getDecimals() > 0) {
								sfLength++;
							}
						%>

						<input name='unitPremPercent10' type='text'
							value='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent10)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.unitPremPercent10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.unitPremPercent10)%>'
							<%}%> size='<%=sfLength%>' maxLength='<%=sfLength%>'
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitPremPercent10)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);' style='text-align: right'
							<%if ((new Byte((sv.unitPremPercent10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.unitPremPercent10).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.unitPremPercent10).getColor() == null ? "input_cell"
						: (sv.unitPremPercent10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-4"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>
