

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5533";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%
	S5533ScreenVars sv = (S5533ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contribution Limits");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Frequency");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Minimun");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Maximum");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Casual");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Rounding factor  ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prem unit ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.frequency01.setReverse(BaseScreenData.REVERSED);
			sv.frequency01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.frequency01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.cmin01.setReverse(BaseScreenData.REVERSED);
			sv.cmin01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.cmin01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.cmax01.setReverse(BaseScreenData.REVERSED);
			sv.cmax01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.cmax01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.frequency02.setReverse(BaseScreenData.REVERSED);
			sv.frequency02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.frequency02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.cmin02.setReverse(BaseScreenData.REVERSED);
			sv.cmin02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.cmin02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cmax02.setReverse(BaseScreenData.REVERSED);
			sv.cmax02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cmax02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.frequency03.setReverse(BaseScreenData.REVERSED);
			sv.frequency03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.frequency03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.cmin03.setReverse(BaseScreenData.REVERSED);
			sv.cmin03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.cmin03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.cmax03.setReverse(BaseScreenData.REVERSED);
			sv.cmax03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.cmax03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.frequency04.setReverse(BaseScreenData.REVERSED);
			sv.frequency04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.frequency04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.cmin04.setReverse(BaseScreenData.REVERSED);
			sv.cmin04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.cmin04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.cmax04.setReverse(BaseScreenData.REVERSED);
			sv.cmax04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.cmax04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.frequency05.setReverse(BaseScreenData.REVERSED);
			sv.frequency05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.frequency05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.cmin05.setReverse(BaseScreenData.REVERSED);
			sv.cmin05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.cmin05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.cmax05.setReverse(BaseScreenData.REVERSED);
			sv.cmax05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.cmax05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.frequency06.setReverse(BaseScreenData.REVERSED);
			sv.frequency06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.frequency06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.cmin06.setReverse(BaseScreenData.REVERSED);
			sv.cmin06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.cmin06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.cmax06.setReverse(BaseScreenData.REVERSED);
			sv.cmax06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.cmax06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.frequency07.setReverse(BaseScreenData.REVERSED);
			sv.frequency07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.frequency07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.cmin07.setReverse(BaseScreenData.REVERSED);
			sv.cmin07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.cmin07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.cmax07.setReverse(BaseScreenData.REVERSED);
			sv.cmax07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.cmax07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.frequency08.setReverse(BaseScreenData.REVERSED);
			sv.frequency08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.frequency08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.cmin08.setReverse(BaseScreenData.REVERSED);
			sv.cmin08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.cmin08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.cmax08.setReverse(BaseScreenData.REVERSED);
			sv.cmax08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.cmax08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.casualContribMin.setReverse(BaseScreenData.REVERSED);
			sv.casualContribMin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.casualContribMin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.casualContribMax.setReverse(BaseScreenData.REVERSED);
			sv.casualContribMax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.casualContribMax.setHighLight(BaseScreenData.BOLD);
		}
		//ALS-4566 
		if (appVars.ind31.isOn()) {
			sv.cIncrmin01.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.cIncrmin01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.cIncrmax01.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.cIncrmax01.setHighLight(BaseScreenData.BOLD);
		}		
		if (appVars.ind32.isOn()) {
			sv.cIncrmin02.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.cIncrmin02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.cIncrmax02.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.cIncrmax02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.cIncrmin03.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.cIncrmin03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.cIncrmax03.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.cIncrmax03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.cIncrmin04.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.cIncrmin04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.cIncrmax04.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.cIncrmax04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.cIncrmin05.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.cIncrmin05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.cIncrmax05.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.cIncrmax05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.cIncrmin06.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.cIncrmin06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.cIncrmax06.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.cIncrmax06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.cIncrmin07.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.cIncrmin07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.cIncrmax07.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.cIncrmax07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.cIncrmin08.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmin08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.cIncrmin08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.cIncrmax08.setReverse(BaseScreenData.REVERSED);
			sv.cIncrmax08.setColor(BaseScreenData.RED);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>





</td><td>



						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					<!-- </div> --></td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="width:90px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="width:90px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

		<div class="row">
		<div class="col-md-3"></div>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("----------------------Contribution Limits----------------------")%></label>
				</div>
			</div>
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-4 ">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("----------------Contribution Increase Limits----------------")%></label>
				</div>
			</div><%} %>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum")%></label>
				</div>
			</div>
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum")%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum")%></label>
				</div>
			</div>
			<%} %>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency01");
							optionValue = makeDropDownList(mappedItems, sv.frequency01.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency01' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency01).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin01' type='text'
							<%if ((sv.cmin01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin01.getLength(), sv.cmin01.getScale(), 3)%>'
							maxLength='<%=sv.cmin01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin01).getColor() == null ? "input_cell"
						: (sv.cmin01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax01).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax01' type='text'
							<%if ((sv.cmax01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax01.getLength(), sv.cmax01.getScale(), 3)%>'
							maxLength='<%=sv.cmax01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax01).getColor() == null ? "input_cell"
						: (sv.cmax01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<!-- ALS-4566 -->
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin01).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin01' type='text'
							<%if ((sv.cIncrmin01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin01.getLength(), sv.cIncrmin01.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin01).getColor() == null ? "input_cell"
						: (sv.cIncrmin01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax01).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax01' type='text'
							<%if ((sv.cIncrmax01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax01.getLength(), sv.cIncrmax01.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax01.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax01).getColor() == null ? "input_cell"
						: (sv.cIncrmax01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency02" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency02");
							optionValue = makeDropDownList(mappedItems, sv.frequency02.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency02.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency02).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency02' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency02).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin02' type='text'
							<%if ((sv.cmin02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin02.getLength(), sv.cmin02.getScale(), 3)%>'
							maxLength='<%=sv.cmin02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin02).getColor() == null ? "input_cell"
						: (sv.cmin02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax02' type='text'
							<%if ((sv.cmax02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax02.getLength(), sv.cmax02.getScale(), 3)%>'
							maxLength='<%=sv.cmax02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax02).getColor() == null ? "input_cell"
						: (sv.cmax02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<!-- ALS-4566 -->
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin02).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin02' type='text'
							<%if ((sv.cIncrmin02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin02.getLength(), sv.cIncrmin02.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin02).getColor() == null ? "input_cell"
						: (sv.cIncrmin02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax02).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax02' type='text'
							<%if ((sv.cIncrmax02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax02.getLength(), sv.cIncrmax02.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax02)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax02).getColor() == null ? "input_cell"
						: (sv.cIncrmax02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency03" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency03");
							optionValue = makeDropDownList(mappedItems, sv.frequency03.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency03.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency03).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency03' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency03).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin03' type='text'
							<%if ((sv.cmin03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin03.getLength(), sv.cmin03.getScale(), 3)%>'
							maxLength='<%=sv.cmin03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin03).getColor() == null ? "input_cell"
						: (sv.cmin03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax03).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax03' type='text'
							<%if ((sv.cmax03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax03.getLength(), sv.cmax03.getScale(), 3)%>'
							maxLength='<%=sv.cmax03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax03).getColor() == null ? "input_cell"
						: (sv.cmax03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
				<!-- ALS-4566 -->
				<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin03).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin03' type='text'
							<%if ((sv.cIncrmin03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin03.getLength(), sv.cIncrmin03.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin03).getColor() == null ? "input_cell"
						: (sv.cIncrmin03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax03).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax03' type='text'
							<%if ((sv.cIncrmax03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax03.getLength(), sv.cIncrmax03.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax03.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax03)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax03).getColor() == null ? "input_cell"
						: (sv.cIncrmax03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency04" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency04");
							optionValue = makeDropDownList(mappedItems, sv.frequency04.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency04.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency04).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency04' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency04).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin04' type='text'
							<%if ((sv.cmin04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin04.getLength(), sv.cmin04.getScale(), 3)%>'
							maxLength='<%=sv.cmin04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin04).getColor() == null ? "input_cell"
						: (sv.cmin04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax04).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax04' type='text'
							<%if ((sv.cmax04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax04.getLength(), sv.cmax04.getScale(), 3)%>'
							maxLength='<%=sv.cmax04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax04).getColor() == null ? "input_cell"
						: (sv.cmax04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
				<!-- ALS-4566 -->
				<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin04).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin04' type='text'
							<%if ((sv.cIncrmin04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin04.getLength(), sv.cIncrmin04.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin04).getColor() == null ? "input_cell"
						: (sv.cIncrmin04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax04).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax04' type='text'
							<%if ((sv.cIncrmax04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax04.getLength(), sv.cIncrmax04.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax04.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax04)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax04).getColor() == null ? "input_cell"
						: (sv.cIncrmax04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency05" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency05");
							optionValue = makeDropDownList(mappedItems, sv.frequency05.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency05.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency05).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency05' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency05).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin05' type='text'
							<%if ((sv.cmin05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin05.getLength(), sv.cmin05.getScale(), 3)%>'
							maxLength='<%=sv.cmin05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin05).getColor() == null ? "input_cell"
						: (sv.cmin05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax05).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax05' type='text'
							<%if ((sv.cmax05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax05.getLength(), sv.cmax05.getScale(), 3)%>'
							maxLength='<%=sv.cmax05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax05).getColor() == null ? "input_cell"
						: (sv.cmax05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<!-- ALS-4566 -->
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin05).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin05' type='text'
							<%if ((sv.cIncrmin05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin05.getLength(), sv.cIncrmin05.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin05).getColor() == null ? "input_cell"
						: (sv.cIncrmin05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax05).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax05' type='text'
							<%if ((sv.cIncrmax05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax05.getLength(), sv.cIncrmax05.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax05.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax05)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax05).getColor() == null ? "input_cell"
						: (sv.cIncrmax05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency06" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency06");
							optionValue = makeDropDownList(mappedItems, sv.frequency06.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency06.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency06).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency06' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency06).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin06' type='text'
							<%if ((sv.cmin06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin06.getLength(), sv.cmin06.getScale(), 3)%>'
							maxLength='<%=sv.cmin06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin06).getColor() == null ? "input_cell"
						: (sv.cmin06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax06).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax06' type='text'
							<%if ((sv.cmax06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax06.getLength(), sv.cmax06.getScale(), 3)%>'
							maxLength='<%=sv.cmax06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax06).getColor() == null ? "input_cell"
						: (sv.cmax06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<!-- ALS-4566 -->
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin06).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin06' type='text'
							<%if ((sv.cIncrmin06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin06.getLength(), sv.cIncrmin06.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin06).getColor() == null ? "input_cell"
						: (sv.cIncrmin06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax06).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax06' type='text'
							<%if ((sv.cIncrmax06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax06.getLength(), sv.cIncrmax06.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax06.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax06)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax06).getColor() == null ? "input_cell"
						: (sv.cIncrmax06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency07" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency07");
							optionValue = makeDropDownList(mappedItems, sv.frequency07.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency07.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency07).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency07' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency07).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin07' type='text'
							<%if ((sv.cmin07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin07.getLength(), sv.cmin07.getScale(), 3)%>'
							maxLength='<%=sv.cmin07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin07).getColor() == null ? "input_cell"
						: (sv.cmin07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax07).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax07' type='text'
							<%if ((sv.cmax07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax07.getLength(), sv.cmax07.getScale(), 3)%>'
							maxLength='<%=sv.cmax07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax07).getColor() == null ? "input_cell"
						: (sv.cmax07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
			<!-- ALS-4566 -->
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin07).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin07' type='text'
							<%if ((sv.cIncrmin07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin07.getLength(), sv.cIncrmin07.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin07).getColor() == null ? "input_cell"
						: (sv.cIncrmin07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax07).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax07, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax07' type='text'
							<%if ((sv.cIncrmax07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax07.getLength(), sv.cIncrmax07.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax07.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax07)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax07).getColor() == null ? "input_cell"
						: (sv.cIncrmax07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<div>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "frequency08" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("frequency08");
							optionValue = makeDropDownList(mappedItems, sv.frequency08.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.frequency08.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.frequency08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 140px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.frequency08).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 160px;">
							<%
								}
							%>

							<select name='frequency08' type='list' style="width: 160px;"
								<%if ((new Byte((sv.frequency08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.frequency08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.frequency08).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmin08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmin08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmin08' type='text'
							<%if ((sv.cmin08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmin08.getLength(), sv.cmin08.getScale(), 3)%>'
							maxLength='<%=sv.cmin08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmin08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmin08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmin08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmin08).getColor() == null ? "input_cell"
						: (sv.cmin08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cmax08).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.cmax08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cmax08' type='text'
							<%if ((sv.cmax08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cmax08.getLength(), sv.cmax08.getScale(), 3)%>'
							maxLength='<%=sv.cmax08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cmax08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cmax08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cmax08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cmax08).getColor() == null ? "input_cell"
						: (sv.cmax08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<%if(sv.contIncrLimitFlag.equals("Y")){%>
				<!-- ALS-4566 -->
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmin08).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmin08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmin08' type='text'
							<%if ((sv.cIncrmin08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmin08.getLength(), sv.cIncrmin08.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmin02.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmin08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmin08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmin08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmin08).getColor() == null ? "input_cell"
						: (sv.cIncrmin08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.cIncrmax08).getFieldName());
							valueThis = smartHF.getPicFormatted(qpsf, sv.cIncrmax08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='cIncrmax08' type='text'
							<%if ((sv.cIncrmax08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.cIncrmax08.getLength(), sv.cIncrmax08.getScale(), 3)%>'
							maxLength='<%=sv.cIncrmax08.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(cIncrmax08)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.cIncrmax08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.cIncrmax08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.cIncrmax08).getColor() == null ? "input_cell"
						: (sv.cIncrmax08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><%} %>
			<!-- ALS-4566 -->
		</div>

		<div class="row">
			<div class="col-md-3 col-md-offset-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Casual")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-md-offset-3">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.casualContribMin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.casualContribMin,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='casualContribMin' type='text'
							<%if ((sv.casualContribMin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.casualContribMin)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.casualContribMin);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.casualContribMin)%>'
							<%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.casualContribMin.getLength(),
					sv.casualContribMin.getScale(), 3)%>'
							maxLength='<%=sv.casualContribMin.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(casualContribMin)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.casualContribMin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.casualContribMin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.casualContribMin).getColor() == null ? "input_cell"
						: (sv.casualContribMin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.casualContribMax).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							valueThis = smartHF.getPicFormatted(qpsf, sv.casualContribMax,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='casualContribMax' type='text'
							<%if ((sv.casualContribMax).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.casualContribMax.getLength(),
					sv.casualContribMax.getScale(), 3)%>'
							maxLength='<%=sv.casualContribMax.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(casualContribMax)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.casualContribMax).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.casualContribMax).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.casualContribMax).getColor() == null ? "input_cell"
						: (sv.casualContribMax).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rounding factor")%></label>
					<div style="width: 100px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.rndfact).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='rndfact' type='text'
							<%if ((sv.rndfact).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.rndfact)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.rndfact);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.rndfact)%>' <%}%>
							size='<%=sv.rndfact.getLength()%>'
							maxLength='<%=sv.rndfact.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(rndfact)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.rndfact).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.rndfact).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.rndfact).getColor() == null ? "input_cell"
						: (sv.rndfact).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem unit")%></label>
					<div style="width: 80px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.premUnit).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='premUnit' type='text'
							<%if ((sv.premUnit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.premUnit)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.premUnit);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.premUnit)%>' <%}%>
							size='<%=sv.premUnit.getLength()%>'
							maxLength='<%=sv.premUnit.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(premUnit)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.premUnit).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.premUnit).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.premUnit).getColor() == null ? "input_cell"
						: (sv.premUnit).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

