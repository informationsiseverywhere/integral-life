<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6504";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S6504ScreenVars sv = (S6504ScreenVars) fw.getVariables();%>


<%if (sv.S6504screenWritten.gt(0)) {%>
	<%S6504screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%sv.effdateDisp.setClassString("");%>
<%	sv.effdateDisp.appendClassString("string_fld");
	sv.effdateDisp.appendClassString("output_txt");
	sv.effdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%sv.acctmonth.setClassString("");%>
<%	sv.acctmonth.appendClassString("num_fld");
	sv.acctmonth.appendClassString("output_txt");
	sv.acctmonth.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%sv.acctyear.setClassString("");%>
<%	sv.acctyear.appendClassString("num_fld");
	sv.acctyear.appendClassString("output_txt");
	sv.acctyear.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%sv.jobname.setClassString("");%>
<%	sv.jobname.appendClassString("string_fld");
	sv.jobname.appendClassString("output_txt");
	sv.jobname.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%sv.runid.setClassString("");%>
<%	sv.runid.appendClassString("string_fld");
	sv.runid.appendClassString("output_txt");
	sv.runid.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%sv.jobq.setClassString("");%>
<%	sv.jobq.appendClassString("string_fld");
	sv.jobq.appendClassString("output_txt");
	sv.jobq.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%sv.runlib.setClassString("");%>
<%	sv.runlib.appendClassString("string_fld");
	sv.runlib.appendClassString("output_txt");
	sv.runlib.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Process transactions on or before ");%>
	<%sv.stmEffdteDisp.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind10.isOn()) {
			sv.stmEffdteDisp.setReverse(BaseScreenData.REVERSED);
			sv.stmEffdteDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.stmEffdteDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 2, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 18, fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(3, 18, fw, sv.effdateDisp)%>

	<%=smartHF.getLit(3, 30, generatedText3)%>

	<%=smartHF.getHTMLVar(3, 48, fw, sv.acctmonth, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(3, 51, generatedText4)%>

	<%=smartHF.getHTMLVar(3, 57, fw, sv.acctyear, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(5, 2, generatedText5)%>

	<%=smartHF.getHTMLVar(5, 12, fw, sv.jobname)%>

	<%=smartHF.getLit(5, 21, generatedText6)%>

	<%=smartHF.getHTMLVar(5, 29, fw, sv.runid)%>

	<%=smartHF.getLit(5, 32, generatedText7)%>

	<%=smartHF.getHTMLVar(5, 43, fw, sv.jobq)%>

	<%=smartHF.getLit(5, 54, generatedText8)%>

	<%=smartHF.getHTMLVar(5, 63, fw, sv.runlib)%>

	<%=smartHF.getLit(9, 3, generatedText9)%>

	<%=smartHF.getHTMLSpaceVar(9, 42, fw, sv.stmEffdteDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 42, fw, sv.stmEffdteDisp)%>




<%}%>

<%if (sv.S6504protectWritten.gt(0)) {%>
	<%S6504protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2.jsp"%>
