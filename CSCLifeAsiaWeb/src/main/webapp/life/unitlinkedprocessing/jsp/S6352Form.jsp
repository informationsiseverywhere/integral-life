<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6352";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6352ScreenVars sv = (S6352ScreenVars) fw.getVariables();%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind29.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.unitType.setReverse(BaseScreenData.REVERSED);
			sv.unitType.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.unitType.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.nofUnits.setReverse(BaseScreenData.REVERSED);
			sv.nofUnits.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.nofUnits.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.nofDunits.setReverse(BaseScreenData.REVERSED);
			sv.nofDunits.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.nofDunits.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Journals");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"no ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date    ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date  ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Num ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to 1");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalmt ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Press Refresh for Redisplay/Summary Enquiry");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit - Add/Subtract");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Summary Totals");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"typ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Real Units");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deemed Units");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Real Units");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deemed Units");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind05.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind05.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			generatedText25.setColor(BaseScreenData.BLUE);
		}
	}

	%>


<div class="panel panel-default"> 
	<div class="panel-body">


<div class="row">
	<div class="col-md-4">
	 <div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		
		<div>
  		
			<%					
			if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
  
	</div>
 </div>
</div>

<div class="col-md-4">
 <div class="form-group">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
		
	<div>
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 45px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
	

</div>
</div>

<div class="col-md-4">
 <div class="form-group">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%>
</label>
<div>
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 45px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
	

</div>
</div>

</div>


<div class="row">

	<div class="col-md-4">
		<div class="form-group">

		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>		
		
		<table><tr><td>
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td><td style="min-width:2px">
	</td><td style="min-width:80px">
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
	
</div>
</div>
<!-- <style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style> -->

<div class="col-md-4"></div>

<div class="col-md-4">
		<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
<div>
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 45px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>

</div>
</div>

</div>




<div class="row">

	
<!-- <table> -->
<!-- <tr style='height:42px;'> -->

<div class="col-md-4">
		<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>

<div>
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>

</div>
</div>



<div class="col-md-4">
		<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>

<div>
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
			
			if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style=" max-width: 58px;">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 </div>
 </div>
 </div>


<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	
  		<div>
		<%			
		
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
				
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="  max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
	
	</div>
	</div>
	



</div>


<div class="row">

<div class="col-md-4">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective date")%></label>
		<div>
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div>
	

</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Num")%></label>

		<div >
		<table>
  		<tr>
  		<td style="width: 107px;">
		<%	
			qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
			
			if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</td>
	<td style="padding-left:6px;">

 
 <label>
<%=resourceBundleHandler.gettingValueFromBundle("to 1")%>
</label>
</td>
</tr>
</table>
</div>
</div>
</div>



<div class="col-md-4">
	<div class="form-group" style="max-width: 123px;">

<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment")%></label>

<div>
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.instprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
</div>
</div>
</div>	


</div>

	
		

		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							 id="s6352DataTable" width='100%'>
							
								<tr class='info'>
									<td rowspan="2"  align="center" style="min-width:100px;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></td>
									<!-- ILIFE-1112 starts		 -->							
									<td rowspan="2"   align="center" style="min-width:100px;"><strong>  <%=resourceBundleHandler.gettingValueFromBundle("Header2")%></strong></td>
									<td colspan="2"  align="center" style="min-width:100px;"><strong><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></strong></td>
									<td colspan="2"  align="center" style="min-width:100px;"><strong><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></strong></td>
									<!-- ILIFE-1112 ends	 -->		
		
								</tr> 
								
								<tr class="info">
									<!-- ILIFE-1112 starts	 -->
									<td  align="center"><b>  <%=resourceBundleHandler.gettingValueFromBundle("Header3")%></b></td>
									<td  align="center" style="min-width:100px;"><b><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></b></td>
									<td  align="center" style="min-width:100px;"><b><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></b></td>
									<td  align="center" style="min-width:100px;"><b><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></b></td>
									<!-- ILIFE-1112 ends -->
								</tr>
								
								<tbody>
									<%
									GeneralTable sfl = fw.getTable("s6352screensfl");
									int height;
									if(sfl.count()*27 > 210) {
									height = 210 ;
									} else {
									height = sfl.count()*27;
									}	
									%>
									
									<%
									String backgroundcolor="#FFFFFF";
									
								
								
									S6352screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									Map<String,Map<String,String>> unitVirtualFundMap = appVars.loadF4FieldsLong(new String[] {"unitVirtualFund"},sv,"E",baseModel);
									Map<String,Map<String,String>> unitTypeMap = fieldItem=appVars.loadF4FieldsLong(new String[] {"unitType"},sv,"E",baseModel);
									while (S6352screensfl.hasMoreScreenRows(sfl)) {	
								
								
									%>
									
								
								
								<%
								/* This block of jsp code is to calculate the variable width of the table at runtime.*/
								int[] tblColumnWidth = new int[6];
								int totalTblWidth = 0;
								int calculatedValue =0;
								
																											if((resourceBundleHandler.gettingValueFromBundle("Header1").length()*12) >= (sv.unitVirtualFund.getFormData()).length()*12+32+16 ) {
															calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
														} else {		
															calculatedValue = ((sv.unitVirtualFund.getFormData()).length()*12)+32+16;								
														}		
																	totalTblWidth += calculatedValue;
										tblColumnWidth[0]= calculatedValue;
											
																											if((resourceBundleHandler.gettingValueFromBundle("Header2").length()*12) >= (sv.unitType.getFormData()).length()*12+32+16 ) {
															calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
														} else {		
															calculatedValue = ((sv.unitType.getFormData()).length()*12)+32+16;								
														}		
																	totalTblWidth += calculatedValue;
										tblColumnWidth[1]= calculatedValue;
											
																						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.nofUnits.getFormData()).length() ) {
															calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
														} else {		
															calculatedValue = (sv.nofUnits.getFormData()).length()*12;								
														}					
																						totalTblWidth += calculatedValue;
										tblColumnWidth[2]= calculatedValue;
											
																						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.nofDunits.getFormData()).length() ) {
															calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
														} else {		
															calculatedValue = (sv.nofDunits.getFormData()).length()*12;								
														}					
																						totalTblWidth += calculatedValue;
										tblColumnWidth[3]= calculatedValue;
											
														if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.currentUnitBal.getFormData()).length() ) {
												calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
											} else {		
												calculatedValue = (sv.currentUnitBal.getFormData()).length()*12;								
											}		
												totalTblWidth += calculatedValue;
										tblColumnWidth[4]= calculatedValue;
											
														if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.currentDunitBal.getFormData()).length() ) {
												calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
											} else {		
												calculatedValue = (sv.currentDunitBal.getFormData()).length()*12;								
											}		
												totalTblWidth += calculatedValue;
										tblColumnWidth[5]= calculatedValue;
											%>
											
											
											
											
											<tr class="tableRowTag" id='<%="tablerow"+count%>' >
								<td  class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																							
																		
											<%	
												mappedItems = (Map) unitVirtualFundMap.get("unitVirtualFund");
												optionValue = makeDropDownList( mappedItems , sv.unitVirtualFund,1,resourceBundleHandler);  
												longValue = (String) mappedItems.get((sv.unitVirtualFund.getFormData()).toString().trim());  
												
												if(longValue==null)
												{
													longValue="   ";
												}
											%>
											<% if((new Byte((sv.unitVirtualFund).getEnabled()))
												.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
											%>  
											<div class='output_cell' style="width: 100px;"> 
											   <%=XSSFilter.escapeHtml(longValue)%>
											</div>
											
											<%
											longValue = null;
											%>
						
											<% }else {%>
											<% if("red".equals((sv.unitVirtualFund).getColor())){
											%>
											<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
											<%
											} 
											%>
											<select name='<%="s6352screensfl.unitVirtualFund_R"+count%>' id='<%="s6352screensfl.unitVirtualFund_R"+count%>' type='list' style="width: 155px;"
											
											class = 'input_cell'
											>
											<%=optionValue%>
											
											
											</select>
											<% if("red".equals((sv.unitVirtualFund).getColor())){
											%>
											</div>
											<%
											} 
											%>
										    <%
												} 
											%>
										    
																	
										  </td>
										  <td  class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																							
																		
										      				     <%	
												mappedItems = (Map) unitTypeMap.get("unitType");
												optionValue = makeDropDownList( mappedItems , sv.unitType,2,resourceBundleHandler);  
												longValue = (String) mappedItems.get((sv.unitType.getFormData()).toString().trim()); 
												if(longValue==null)
												{
													longValue="   ";
												}
											%>
											<% if((new Byte((sv.unitType).getEnabled()))
												.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
											%>  
											<div class='output_cell' style="width: 110px;"> 
											   <%=XSSFilter.escapeHtml(longValue)%>
											</div>
											
											<%
											longValue = null;
											%>
						
											<% }else {%>
											<% if("red".equals((sv.unitType).getColor())){
											%>
											<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
											<%
											} 
											%>
											<select name='<%="s6352screensfl.unitType_R"+count%>' id='<%="s6352screensfl.unitType_R"+count%>' type='list' style="width: 180px;"
											
											class = 'input_cell'
											>
											<%=optionValue%>
											</select>
											<% if("red".equals((sv.unitType).getColor())){
											%>
											</div>
											<%
											} 
											%>
										    <%
												} 
											%>				
															</td>
										    					<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="center">									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.nofUnits).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.nofUnits,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.nofUnits.getLength()%>'
						
						 value='<%=formatValue %>' 
						 size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.nofUnits.getLength(),sv.nofUnits.getScale(),3)%>'
						 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s6352screensfl.nofUnits)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6352screensfl" + "." +
						 "nofUnits" + "_R" + count %>'
						 id='<%="s6352screensfl" + "." +
						 "nofUnits" + "_R" + count %>'
						 <%-- ILIFE-1113 - added condition for disability in inquiry mode --%>
						 <% if((new Byte((sv.nofUnits).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
						%>
						class = "output_cell"
						<%} else { %>  
						 class = "input_cell"
						<% } %>
						  style = "width: <%=sv.nofUnits.getLength()*12%> px;"
						  <%if((sv.nofUnits).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="center">									
																	
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.nofDunits).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.nofDunits,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.nofDunits.getLength()%>'
						<%if((sv.nofDunits).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						 value='<%=formatValue %>' 
						 size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.nofDunits.getLength(),sv.nofDunits.getScale(),3)%>'
						 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s6352screensfl.nofDunits)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6352screensfl" + "." +
						 "nofDunits" + "_R" + count %>'
						 id='<%="s6352screensfl" + "." +
						 "nofDunits" + "_R" + count %>'
						 <%-- ILIFE-1113 - added condition for disability in inquiry mode --%>
						 <% if((new Byte((sv.nofDunits).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
						 %>
							class = "output_cell"
						<%} else { %>  
						 	class = "input_cell"
						<% } %>
						  style = "width: <%=sv.nofDunits.getLength()*12%> px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
										
					
											
									</td>
										    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																						
																													
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.currentUnitBal).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.currentUnitBal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
													if(!sv.
													currentUnitBal
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
											 			 		
									 		
									    				 
										
															</td>
										    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" align="left">									
																						
																													
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.currentDunitBal).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.currentDunitBal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
													if(!sv.
													currentDunitBal
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
											 			 		
									 		
									    				 
										
										</td>
											
								</tr>
										
										<%
										if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
												backgroundcolor="#EEEEEE";
												}else{
												backgroundcolor="#FFFFFF";
												}
											
											count = count + 1;
											S6352screensfl
											.setNextScreenRow(sfl, appVars, sv);
											}
											%>	
								
								</tbody>
								
						</table>
					</div>
				</div>
			</div>
		</div>
						

<%-- <script language="javascript">
        $(document).ready(function(){
	
			new superTable("s5417Table", {
				fixedCols : 0,	
				headerRows:2,
				colWidths : [150,150,190,190,110,110],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script> --%>
<script>
	$(document).ready(function() {
		$('#s6352DataTable').DataTable({
			ordering : false,
			searching : false,
			scrollY : "450px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false,
	        columnDefs: [{
	        	targets: [ 3 ]
	        	}]
		});
	});
</script>  

	
<br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("-")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("no")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Instalmt")%>
</div>

</tr></table></div><br/>



</div>

</div>




<%@ include file="/POLACommon2NEW.jsp"%>

