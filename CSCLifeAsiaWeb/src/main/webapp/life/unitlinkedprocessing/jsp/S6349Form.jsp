<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6349";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>

<%S6349ScreenVars sv = (S6349ScreenVars) fw.getVariables();%>
<%-- <%if (sv.S6349screenWritten.gt(0)) {%> --%>
	<%-- <%S6349screen.clearClassString(sv);%> --%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Unit Journal");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Bonus Journal");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Interest Journal");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"H - Journal Enquiry (Interest Bearing)");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"I - Journal Enquiry (Unit Linked only)");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No     ");%>
	<%sv.chdrsel.setClassString("");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date  ");%>
	<%sv.effdateDisp.setClassString("");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action          ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>




<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group">
						    		<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%> 
						    		<span class="input-group-btn">
					        			<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
					        			type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        			</button>
					      			</span>
				      			</div>
				    		</div>
			    	</div>
			  <!-- </div>
			  <div class="row"> -->
			  <div class="col-md-3"></div>
			    	<div class="col-md-3">
			    		<div class="form-group">
				    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<%
								if((new Byte((sv.effdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
							<%
								}else{
							%>
							<div id="effdatePicker" class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
				   		 </div>
			    	
			    	</div>			       
			</div>
		</div>
	</div>


		
		
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>		
	 	<div class="panel-body">  
			<div class="row">	
			    <div class="col-md-4">
					<label class="radio-inline">
						<%= smartHF.buildRadioOption(sv.action, "action", "A")%>
					<b><%=resourceBundleHandler.gettingValueFromBundle("Unit Journal")%></b>
					</label>
				</div>
				<div class="col-md-4">			
					<label class="radio-inline">
						<%= smartHF.buildRadioOption(sv.action, "action", "B")%>
					<b><%=resourceBundleHandler.gettingValueFromBundle("Bonus Journal")%></b>
					</label>			
			    </div>	
			    <div class="col-md-4">
					<label class="radio-inline">
					<%= smartHF.buildRadioOption(sv.action, "action", "C")%>
					<b><%=resourceBundleHandler.gettingValueFromBundle("Interest Journal")%></b>
					</label>
				</div>	     
					
			</div>
	
			<div class="row">	
			    
			<div class="col-md-4">			
					<label class="radio-inline">
						<%= smartHF.buildRadioOption(sv.action, "action", "H")%>
					<b><%=resourceBundleHandler.gettingValueFromBundle("Journal Enquiry (Interest Bearing)")%></b>
					</label>			
			    </div>   
			    
			    <div class="col-md-4">
					<label class="radio-inline">
					<%= smartHF.buildRadioOption(sv.action, "action", "I")%>
					<b><%=resourceBundleHandler.gettingValueFromBundle("Journal Enquiry (Unit Linked only)")%></b>
					</label>
				</div>	
				 <div class="col-md-4"></div>	        
			</div>
		
		<!-- 
			<div class="row">	
			    
					        
			</div> -->
		
	</div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $('#effdatePicker').datetimepicker()
    .on('changeDate', function(e) {
        document.getElementById('billcdDisp').value='';
});
});

</script>

<%@ include file="/POLACommon2NEW.jsp"%>
