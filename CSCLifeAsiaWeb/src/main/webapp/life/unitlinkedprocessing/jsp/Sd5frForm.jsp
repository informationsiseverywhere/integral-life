<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5fr";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.unitlinkedprocessing.screens.*"%>
<%@ page import="java.io.*"%>

<%
	Sd5frScreenVars sv = (Sd5frScreenVars) fw.getVariables();
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.vfund.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.vfund.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.vfund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.vfund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.percentage.setReverse(BaseScreenData.REVERSED);
			sv.percentage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.percentage.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Switch Date     ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Percentages/Amounts/Units");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "(P/A/U)");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Number   ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policies in Plan  ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Selected Component ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life No ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage No ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider No ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Override Fee (Y/N) ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"% / Amnt / Units");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"-- Target Details --");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Curr ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Fund   Curr  Percent");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind15.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind16.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.overrideFee.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind22.isOn()) {
			sv.overrideFee.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.overrideFee.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.overrideFee.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
					</label>
					<%
						}
					%>
					<table>
						<tr class="input-group three-controller">
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Register")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
					</label>
					<%
						}
					%>
					<div style="width: 120px;">
						<%
							if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<%
						if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
					</label>
					<%
						}
					%>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>






						<%
							if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<%
						if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%>
					</label>
					<%
						}
					%>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>






						<%
							if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Switch Date")%>
					</label>
					<%
						}
					%>
					<%if((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){%>
			<div style="width: 150px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
			<%}else{%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%
							if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								longValue = sv.effdateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='effdateDisp' type='text'
							value='<%=sv.effdateDisp.getFormData()%>'
							maxLength='<%=sv.effdateDisp.getLength()%>'
							size='<%=sv.effdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(effdateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.effdateDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.effdateDisp).getColor() == null
								? "input_cell"
								: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
								}
							}
						%>

					</div>
					<%} %>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

								if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.numpols);

								if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Percentages/Amounts/Units")%>
					</label>
					<%
						}
					%>
					<div style="width: 100px;">
						<%
							if ((new Byte((sv.percentAmountInd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"percentAmountInd"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("percentAmountInd");
								longValue = (String) mappedItems.get((sv.percentAmountInd.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.percentAmountInd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.percentAmountInd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.percentAmountInd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>


							<%
								if (formatValue.toString().equalsIgnoreCase("P")) {
										formatValue = "Percentage";
									} else if (formatValue.toString().equalsIgnoreCase("A")) {
										formatValue = "Amounts";
									} else if (formatValue.toString().equalsIgnoreCase("U")) {
										formatValue = "Units";
									}
							%><%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Life No")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Rider No")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<%
						if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Coverage")%>
					</label>
					<%
						}
					%>
					<div class="input-group three-controller">
						<%
							if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtable.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtable.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>






						<%
							if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>

				</div>
			</div>
			<div class="col-md-3 col-md-offset-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Override Fee")%>
					</label>
					<%
						}
					%>

					<%
						if ((new Byte((sv.overrideFee).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<input type='checkbox' name='overrideFee' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(overrideFee)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.overrideFee).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
				if ((sv.overrideFee).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
				if ((sv.overrideFee).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('overrideFee')" /> <input type='checkbox'
						name='overrideFee' value='N'
						<%if (!(sv.overrideFee).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden"
						onclick="handleCheckBox('overrideFee')" />
					<%
						}
					%>

				</div>
			</div>
		</div>
		
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Fund")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.fund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.fund.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fund.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fund.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Type")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.fndtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.fndtype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fndtype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fndtype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Fund Currency")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.fndcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.fndcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fndcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.fndcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<%
						if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Percentage/Amounts/Units")%>
					</label>
					<%
						}
					%>
					<div style="width: 70px;">
						<%
							if ((new Byte((sv.pcnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.pcnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pcnt.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pcnt.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>							
						 
							
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		
		<%
			GeneralTable sfl = fw.getTable("Sd5frscreensfl");
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-Sd5fr' width='100%'>
							<thead>
								 
								<tr class='info'>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
									<th><%=resourceBundleHandler.gettingValueFromBundle("Percent")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sd5frscreensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sd5frscreensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
									   
									<%
										if ((new Byte((sv.vfund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td>
										<!-- 					MIBT- 176 non editable fields  --> <input
										type='text' maxLength='<%=sv.vfund.getLength()%>'
										value='<%=sv.vfund.getFormData()%>'
										size='<%=sv.vfund.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(Sd5frscreensfl.vfund)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="Sd5frscreensfl" + "." + "vfund" + "_R" + count%>'
										id='<%="Sd5frscreensfl" + "." + "vfund" + "_R" + count%>'
										class="output_cell" readonly="true"
										style="width: <%=sv.vfund.getLength() * 12%>px;">





									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.fndcur).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.fndcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.fndcur.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.percentage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td>
										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.percentage).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.percentage);
										%> <input type='text'
										<%if ((sv.percentage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										style="text-align: right" <%}%>
										maxLength='<%=sv.percentage.getLength()%>'
										value='<%=formatValue%>'
										size='<%=sv.percentage.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(Sd5frscreensfl.percentage)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="Sd5frscreensfl" + "." + "percentage" + "_R" + count%>'
										id='<%="Sd5frscreensfl" + "." + "percentage" + "_R" + count%>'
										class="input_cell"
										style="width: <%=sv.percentage.getLength() * 12%>px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return getdoBlurNumber(event);'
										title='<%=formatValue%>'>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>

								</tr>

								<%
									count = count + 1;
										Sd5frscreensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
	$(document).ready(function() {
		$('#dataTables-Sd5fr').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>
				<%
					if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("(P/A/U)")%>
				</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
			</td>
			<td width='188'>
				<%
					if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
				</div> <%
 	}
 %>
			
		</tr>
	</table>
</div>
