
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sa657";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.workflow.screens.*"%>
<%
	Sa657ScreenVars sv = (Sa657ScreenVars) fw.getVariables();

%>
<!-- IBPTE-1239 START -->
<head>
<style>
/* The Close Button */
.close-1 {
	float: right;
	font-size: 20px;
	font-weight: bold;
}

.close-1:hover, .close-1:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>
</head>
<!-- IBPTE-1239 END -->
<div>
	<h3>
		<%= sv.screenLabel.getFormData().trim()%>
	</h3>
</div>
<script type="text/javascript">
	
	function getImage(taskURL) {

		$
				.ajax({
					type : "GET",
					url : taskURL,

					headers : {
						'Accept' : 'text/html,application/xhtml+xml,application/xml,image/svg+xml;q=0.9,*/*;q=0.8',
					},
					crossDomain : true,
					 beforeSend:function(xhr){
			                xhr.setRequestHeader ("Authorization", <%=sv.authStringForJBPM%>);
			         },
					success : function(response) {
						console.log(response);
						var svg = $(response).find('svg');
						$("#main").html(svg);
						$("#close-process-image").show();//IBPTE-1239
					},
					error : function(qXHR, textStatus, errorThrown) {
						console.log(textStatus);
	$("#service-error-message")
								.text(
										'Workflow service to fatch image is either down or not working properly');//IBPTE-1239
						$("#service-error").show();//IBPTE-1239
					}
				});

		
	}
</script>




<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Decision")%></label>
					<div style="display: table;">
						<div class="input-group">
							<select name='jbpmUWdecision' id='jbpmUWdecision'
								style="width: 100px;">

								<option value="Y">Yes</option>
								<option value="N">No</option>

							</select>

						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-md-offset-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Process Status")%></label>
					<%
							String jbpmProcessInstanceUrl = IntegralWorkflowProperties.getWorkflowServiceRuntimeURL()
									+ sv.jbpmContainerId.trim() + "/images/processes/instances/" + sv.jbpmprocessinstanceID;
						%>
					<div>
						<button id="processImage" style="background: #398439;"
							class="btn btn-success btn-sm"
							onclick="return getImage('<%=jbpmProcessInstanceUrl%>')"><%=resourceBundleHandler.gettingValueFromBundle("Process State")%></button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<%@ include file="/POLACommon2NEW.jsp"%>
<!-- IBPTE-1239 START -->
<br />
<br />
<br />
<div id="service-error"
	style="display: none; padding-left: 25px; padding-right: 900px;">
	<div class="close-1" id="close-error-box">&times;</div>
	<div id="service-error-message" style="color: red; padding-top: 10px;"></div>
</div>
<div id="close-process-image" style="display: none;">
	<span class="close-1" id="close-image">&times;</span>
	<div id="main"></div>
</div>
<script>
	// When the user clicks on <span> (x), close the modal
	
	var spanProcessImage = document.getElementById("close-image");
	var closeProcessImage = document.getElementById("close-process-image");
	var errorBoxButton = document.getElementById("close-error-box");
	var errorBoxMessage = document.getElementById("service-error");

	errorBoxButton.onclick = function() {
		errorBoxMessage.style.display = "none";
	}

	spanProcessImage.onclick = function() {
		closeProcessImage.style.display = "none";
	}
</script>
<!-- IBPTE-1239 END -->



