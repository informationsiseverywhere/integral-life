<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sa578";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.workflow.screens.*"%>
<%Sa578ScreenVars sv = (Sa578ScreenVars) fw.getVariables();


if (appVars.ind01.isOn()) {
	sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
	sv.effdateDisp.setColor(BaseScreenData.RED);
}

if (!appVars.ind01.isOn()) {
	sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
}

if (appVars.ind07.isOn()) {
	sv.zdoctor.setReverse(BaseScreenData.REVERSED);
	sv.zdoctor.setColor(BaseScreenData.RED);
}

if (!appVars.ind07.isOn()) {
	sv.zdoctor.setHighLight(BaseScreenData.BOLD);
}


%>
<!-- IBPTE-1239 START -->
<head>
<style>
/* The Close Button */
.close-1 {
	float: right;
	font-size: 20px;
	font-weight: bold;
}

.close-1:hover, .close-1:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>
</head>
<!-- IBPTE-1239 END -->
<script type="text/javascript">
	 	
	function getImage(taskURL) {

		$
				.ajax({
					type : "GET",
					url : taskURL,

					headers : {
						'Accept' : 'text/html,application/xhtml+xml,application/xml,image/svg+xml;q=0.9,*/*;q=0.8',
					},
					crossDomain : true,
					beforeSend : function(xhr) {
						xhr.setRequestHeader("Authorization",
<%=sv.authStringForJBPM%>
	); //IJTI-1131
					},
					success : function(response) {
						console.log(response);
						var svg = $(response).find('svg');
						$("#main").html(svg);
						$("#close-process-image").show();//IBPTE-1239    
					},
					error : function(qXHR, textStatus, errorThrown) {
						console.log(textStatus);
						//IBPTE-1239
						$("#service-error-message")
								.text(
										'Workflow service to fatch image is either down or not working properly');
						$("#service-error").show();//IBPTE-1239
					}
				});
	}
</script>




<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Receive_Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp,(sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Doctor_ID")%></label>
					<div style="display: table;">
						<div class="input-group">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.zdoctor, sv.zdoctor.getLength())%>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"
									onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Process_State")%></label>
						<%-- IJTI-1090 starts --%>
						<%
							String jbpmProcessInstanceUrl = IntegralWorkflowProperties.getWorkflowServiceRuntimeURL()
									+ sv.jbpmContainerId.trim() + "/images/processes/instances/" + sv.jbpmprocessinstanceID;
						%>
						<div>
							<button id="processImage" style="background: #398439;"
								class="btn btn-success btn-sm"
								onclick="return getImage('<%=jbpmProcessInstanceUrl%>')"><%=resourceBundleHandler.gettingValueFromBundle("Process State")%></button>
						</div>
						<%-- IJTI-1090 ends --%>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
<!-- IBPTE-1239 START -->
<br />
<br />
<br />
<div id="service-error"
	style="display: none; padding-left: 25px; padding-right: 900px;">
	<div class="close-1" id="close-error-box">&times;</div>
	<div id="service-error-message" style="color: red; padding-top: 10px;"></div>
</div>
<div id="close-process-image" style="display: none;">
	<span class="close-1" id="close-image">&times;</span>
	<div id="main"></div>
</div>
<script>
	// When the user clicks on <span> (x), close the modal

	var spanProcessImage = document.getElementById("close-image");
	var closeProcessImage = document.getElementById("close-process-image");
	var errorBoxButton = document.getElementById("close-error-box");
	var errorBoxMessage = document.getElementById("service-error");
	errorBoxButton.onclick = function() {
		errorBoxMessage.style.display = "none";
	}

	spanProcessImage.onclick = function() {
		closeProcessImage.style.display = "none";
	}
</script>
<!-- IBPTE-1239 END -->



