<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sa651";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.workflow.screens.*"%>
<%
	Sa651ScreenVars sv = (Sa651ScreenVars) fw.getVariables();

	if (appVars.ind14.isOn()) {
		sv.huwdcdteDisp.setHighLight(BaseScreenData.BOLD);
		sv.huwdcdteDisp.setColor(BaseScreenData.RED);
	}
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
	response.addHeader("X-Content-Type-Options", "NOSNIFF");
	response.addHeader("Cache-Control", "no-store");
	response.addHeader("Pragma", "no-cache");
%>
<!-- IBPTE-1239 START -->
<head>
<style>
/* The Close Button */
.close-1 {
	float: right;
	font-size: 20px;
	font-weight: bold;
}

.close-1:hover, .close-1:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>
</head>
<!-- IBPTE-1239 END -->
<script type="text/javascript">
	function getImage(taskURL) {

		$
				.ajax({
					type : "GET",
					url : taskURL,

					headers : {
						'Accept' : 'text/html,application/xhtml+xml,application/xml,image/svg+xml;q=0.9,*/*;q=0.8',
					},
					crossDomain : true,
					beforeSend : function(xhr) {
						xhr.setRequestHeader("Authorization",
<%=sv.authStringForJBPM%>
	);
					},
					success : function(response) {
						console.log(response);
						var svg = $(response).find('svg');
						$("#main").html(svg);
  						 $("#close-process-image").show();//IBPTE-1239   
					},
					error : function(qXHR, textStatus, errorThrown) {
						console.log(textStatus);
	$("#service-error-message")
								.text(
										'Workflow service to fatch image is either down or not working properly');//IBPTE-1239
						$("#service-error").show();//IBPTE-1239
					}
				});

	}
</script>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy_Number")%></label>
					<div id="policyNumDiv"
						class='<%=(sv.policyNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
							if (!((sv.policyNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.policyNum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);

								}
							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.policyNum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Number")%></label>
					<div id="lifeAssuredNumDiv"
						class='<%=(sv.lifeAssuredNum.getFormData()).trim().length() ==0 ? "blank_cell":"output_cell"%>'>
						<%
           if (!((sv.lifeAssuredNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

             if (longValue == null || longValue.equalsIgnoreCase("")) {
               formatValue =formatValue((sv.lifeAssuredNum.getFormData()).toString()); 
             } else {
               formatValue = formatValue(longValue);


			 } 
             
           }else{

            if(longValue == null || longValue.equalsIgnoreCase("")){
				formatValue=formatValue((sv.lifeAssuredNum.getFormData()).toString());
			} else {
				formatValue =formatValue(longValue);
			}
			 }
			 %>
						<%=XSSFilter.escapeHtml(formatValue)%></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Name")%></label>
					<div id="lifeAssuredNameDiv"
						class='<%=(sv.lifeAssuredName.getFormData()).trim().length() ==0 ? "blank_cell":"output_cell"%>'>
						<%
           if (!((sv.lifeAssuredName.getFormData()).toString()).trim().equalsIgnoreCase("")) {

             if (longValue == null || longValue.equalsIgnoreCase("")) {
               formatValue =formatValue((sv.lifeAssuredName.getFormData()).toString()); 
             } else {
               formatValue = formatValue(longValue);


			 } 
             
           }else{

            if(longValue == null || longValue.equalsIgnoreCase("")){
				formatValue=formatValue((sv.lifeAssuredName.getFormData()).toString());
			} else {
				formatValue =formatValue(longValue);
			}
			 }
			 %>
						<%=XSSFilter.escapeHtml(formatValue)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Income")%></label>
					<%
                              qpsf =fw.getFieldXMLDef((sv.lifeAssuredIncome).getFieldName());
                              qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                              formatValue=smartHF.getPicFormatted(qpsf,sv.lifeAssuredIncome);
                              
					if (!((sv.lifeAssuredIncome.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);

								}
							} 

								if (!formatValue.trim().equalsIgnoreCase("")) {
									
						%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%} else{ 
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						
					<% } %>
					
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk_Indicator")%></label>
					<div id="riskIndicatorDiv"
						class='<%=(sv.riskIndicator.getFormData()).trim().length() ==0 ? "blank_cell":"output_cell"%>'>
						<%
           if (!((sv.riskIndicator.getFormData()).toString()).trim().equalsIgnoreCase("")) {

             if (longValue == null || longValue.equalsIgnoreCase("")) {
               formatValue =formatValue((sv.riskIndicator.getFormData()).toString()); 
             } else {
               formatValue = formatValue(longValue);
             }

			 } else{

            if(longValue == null || longValue.equalsIgnoreCase("")){
				formatValue=formatValue((sv.riskIndicator.getFormData()).toString());
			} else {
				formatValue =formatValue(longValue);
			}
			 }
			 %>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Is_US_Tax_Payor")%></label>
					<div id="isUsTaxIndicatorDiv"
						class='<%=(sv.isUSTaxPayor.getFormData()).trim().length() ==0 ? "blank_cell":"output_cell"%>'>
						<%
           if (!((sv.isUSTaxPayor.getFormData()).toString()).trim().equalsIgnoreCase("")) {

             if (longValue == null || longValue.equalsIgnoreCase("")) {
               formatValue =formatValue((sv.isUSTaxPayor.getFormData()).toString()); 
             } else {
               formatValue = formatValue(longValue);
             }

			 } else{

            if(longValue == null || longValue.equalsIgnoreCase("")){
				formatValue=formatValue((sv.isUSTaxPayor.getFormData()).toString());
			} else {
				formatValue =formatValue(longValue);
			}
			 }
			 %>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation_Class")%></label>
					<div id="occupClassDiv"
						class='<%=(sv.occupClass.getFormData()).trim().length() == 0 ? "blank_cell":"output_cell"%>'>
						<%
           if (!((sv.riskIndicator.getFormData()).toString()).trim().equalsIgnoreCase("")) {

             if (longValue == null || longValue.equalsIgnoreCase("")) {
               formatValue =formatValue((sv.occupClass.getFormData()).toString()); 
             } else {
               formatValue = formatValue(longValue);
             }

			 } else{

            if(longValue == null || longValue.equalsIgnoreCase("")){
				formatValue=formatValue((sv.occupClass.getFormData()).toString());
			} else {
				formatValue =formatValue(longValue);
			}
			 }
			 %>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Age")%></label>
					<%
                              qpsf =fw.getFieldXMLDef((sv.lifeAssuredAge).getFieldName());
                              qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                              formatValue=smartHF.getPicFormatted(qpsf,sv.lifeAssuredAge);
					if (!((sv.lifeAssuredAge.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);

								}
							} 

								if (!formatValue.trim().equalsIgnoreCase("")) {
									
						%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%} else{ 
						
					} %>
					
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_BMI")%></label>
					<%
                              qpsf =fw.getFieldXMLDef((sv.lifeAssuredBMI).getFieldName());
                              qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                              formatValue=smartHF.getPicFormatted(qpsf,sv.lifeAssuredBMI);
					if (!((sv.lifeAssuredBMI.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);

								}
							} 

								if (!formatValue.trim().equalsIgnoreCase("")) {
									
						%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%} else{ 
						
					} %>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total_Sum_at_Risk")%></label>
					<div id="totalSumDiv"
						class='<%=(sv.tsar.getFormData()).trim().length() == 0 ? "blank_cell":"output_cell" %>'>
						
						<%
                        qpsf =fw.getFieldXMLDef((sv.tsar).getFieldName());
                        qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                        formatValue=smartHF.getPicFormatted(qpsf,sv.tsar);
				if (!((sv.tsar.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);

							}
						} 

							if (!formatValue.trim().equalsIgnoreCase("")) {
								
					%>
				
					<%=XSSFilter.escapeHtml(formatValue)%>
				
				<%} else{ 
					
				} %>
					</div>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Underwriting_Decision")%></label>
					<div style="display: table;">
						<div class="input-group">
							<select name='uwDecision' id='uwDecision' style="width: 100px;">

								<option value="Approve">Approve</option>
								<option value="Decline">Reject</option>
								<option value="CounterOffer">CounterOffer</option>
								<option value="AdditionalUW">Set Additional UW
									Requirement</option>


							</select>

						</div>
					</div>
				</div>


			</div>


			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Underwriting_Decision_Date")%></label>
					<div class="input-group date form_date col-md-12" date-date=""
						data-date-format="dd/mm/yyyy" data-link-field="huwcdteDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.huwdcdteDisp,(sv.huwdcdteDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>
		</div>



		<%
			GeneralTable sfl = fw.getTable("sa651screensfl");
			int height;if(sfl.count()*27>210)
			{
				height = 210;
			}else
			{
				height = sfl.count() * 27;
			}
		%>



		<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sa561Table", {
				fixedCols : 0,					
				colWidths : [120,80,50,50,150,100,100,50,100,120,155],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
		
				addRemoveBtn:"N", 
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>
			/moreButton.gif",
											isReadOnlyFlag : true

										});

							});
		</script>
		
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sa561' width='100%'>
					
						<thead>

							<tr class='info'>
						
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Additional_Follow_Ups")%></center></th>
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Follow_Ups_Description")%></center></th>
								
							</tr>
						</thead>


						<tbody>


							<%
								String backgroundcolor = "#FFFFFF";Sa651screensfl.set1stScreenRow(sfl,appVars,sv);
								int count = 1;while(Sa651screensfl.hasMoreScreenRows(sfl))
								{
							%>
							<!-- indicators -->
							<%
								
							%>
							<tr style="background:<%=backgroundcolor%>;"
								id='<%="tablerow" + count%>'>



								<td
									style="color: #434343; width:100% ; position: relative; border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
									align="left" >
									<div class="input-group">
										<input name='<%="sa651screensfl.fupcdes" + "_R" + count%>'
											id='<%="sa651screensfl.fupcdes" + "_R" + count%>' type='text'
											value='<%=sv.fupcdes.getFormData()%>'
											style="width: 120px !important;"
											maxLength='<%=sv.fupcdes.getLength()%>'
											size='<%=sv.fupcdes.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(sa651screensfl.fupcdes)'
											onKeyUp='return checkMaxLength(this)'
											<%if ((new Byte((sv.fupcdes).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
											readonly="true" class="output_cell">
										<%
											} else if ((new Byte((sv.fupcdes).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
										%>
										class="bold_cell" > <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="doFocus(document.getElementById('<%="sa651screensfl.fupcdes" + "_R" + count%>')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>

										<%
											} else {
										%>
										class='<%=(sv.fupcdes).getColor() == null
					? "input_cell"
					: (sv.fupcdes).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
										> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="doFocus(document.getElementById('<%="sa651screensfl.fupcdes" + "_R" + count%>')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>

									</div> <%
 	}
 %>
								</td>

								<td
									style="color: #434343;  width:100% ;position: relative; left: expression(this.parentElement.offsetParent.offsetParent.scrollLeft); padding: 5px; width: 150px; font-weight: bold; font-size: 12px; font-family: Arial; border-top: 1px solid #dddddd;"
									align="left"><input type='text'
									maxLength='<%=sv.fupremk.getLength()%>'
									value='<%=sv.fupremk.getFormData()%>'
									size='<%=sv.fupremk.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sa651screensfl.fupremk)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sa651screensfl.fupremk_R" + count%>'
									id='<%="sa651screensfl.fupremk_R" + count%>' class="input_cell"
									style="width: 150px;"
									<%if (!(fw.getVariables().isScreenProtected())) {%>
									readonly="true" class="output_cell" <%}%>></td>
							</tr>

							<%
								count = count + 1;
									Sa651screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>



						</tbody>
					</table>

				</div>




			</div>
		</div>




		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<a class="btn btn-info" href="#"
						onClick=" JavaScript:doAction('PFKey91');"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
					<a class="btn btn-info" href="#"
						onClick=" JavaScript:doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>


				</div>
			</div>


			<div style='visibility: hidden;'>
				<table>
					<tr style='height: 22px;'>
						<td width='188'>&nbsp; &nbsp;<br /> <input name='select'
							type='text' <%formatValue=(sv.select.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if(formatValue!=null&&formatValue.trim().length()>0)
	{%>
							title='<%=formatValue%>' <%}%> size='<%=sv.select.getLength()%>'
							maxLength='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(sa651screensfl.select)'
							onKeyUp='return checkMaxLength(this)'
							<%if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED))==0)
	{%>
							readonly="true" class="output_cell"
							<%}else if((new Byte((sv.select).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD))==0)
	{%>
							class="bold_cell" <%}else
	{%>
							class='<%=(sv.select).getColor() == null
				? "input_cell"
				: (sv.select).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>></td>
					</tr>
				</table>
			</div>
		</div>

		<script>
			$(document).ready(function() {
				$('#dataTables-sa561').DataTable({
					ordering : false,
					searching : false,
					scrollX : true,
					scrollY : '300',
					scrollCollapse : true,
					paging : false,
					ordering : false,
					info : false,
					searching : false,
					orderable : false
				});

				$("#crtdateDisp").width(80)

			});
		</script>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
<!-- IBPTE-1239 START -->
<br />
<br />
<br />
<div id="service-error"
	style="display: none; padding-left: 25px; padding-right: 900px;">
	<div class="close-1" id="close-error-box">&times;</div>
	<div id="service-error-message" style="color: red; padding-top: 10px;"></div>
</div>
<div id="close-process-image" style="display: none;">
	<span class="close-1" id="close-image">&times;</span>
	<div id="main"></div>
</div>
<script>
	// When the user clicks on <span> (x), close the modal
	
	var spanProcessImage = document.getElementById("close-image");
	var closeProcessImage = document.getElementById("close-process-image");
	var errorBoxButton = document.getElementById("close-error-box");
	var errorBoxMessage = document.getElementById("service-error");
	errorBoxButton.onclick = function() {
		errorBoxMessage.style.display = "none";
	}
	spanProcessImage.onclick = function() {
		closeProcessImage.style.display = "none";
	}
</script>
<!-- IBPTE-1239 END -->



