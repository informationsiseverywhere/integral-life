<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
   String screenName = "Sa650"; 
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.workflow.screens.*"%>
<%
	Sa650ScreenVars sv = (Sa650ScreenVars) fw.getVariables();
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
	response.addHeader("X-Content-Type-Options", "NOSNIFF");

	response.addHeader("Cache-Control", "no-store");
	response.addHeader("Pragma", "no-cache");
%>
<!-- IBPTE-1239 START -->
<head>
<style>
/* The Close Button */
.close-1 {
	float: right;
	font-size: 20px;
	font-weight: bold;
}

.close-1:hover, .close-1:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>
</head>
<!-- IBPTE-1239 END -->
<script type="text/javascript">
	 	function getImage(taskURL){
	 		
	        $.ajax({
	            type: "GET",
	            url: taskURL,
	           
	            headers: {
	            	'Accept': 'text/html,application/xhtml+xml,application/xml,image/svg+xml;q=0.9,*/*;q=0.8',
                  },
                crossDomain: true,
	            beforeSend:function(xhr){
	                xhr.setRequestHeader ("Authorization", <%=sv.authStringForJBPM%>); //IJTI-1131
	            },
	            success:  function (response) {
	                console.log(response);
	                var svg = $(response).find('svg');  
	                $("#main").html(svg);
	                $("#close-process-image").show();//IBPTE-1239    
	            },
	            error: function(qXHR, textStatus, errorThrown) {
	                console.log(textStatus);
	               
	$("#service-error-message")
								.text(
										'Workflow service to fatch image is either down or not working properly');//IBPTE-1239
						$("#service-error").show();//IBPTE-1239
					}
				});
	}
</script>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract_Number")%></label>

					<div id="chdrnumDiv"
						class='<%=(sv.contractNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.contractNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.contractNum.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.contractNum.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%> 
					</div>

				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract_Type")%></label>

					<div id="contractTypeDiv"
						class='<%=(sv.contractType.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.contractType.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.contractType.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   }
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.contractType.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent_Number")%></label>

					<div id="agentNumDiv"
						class='<%=(sv.agentNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.agentNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.agentNum.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.agentNum.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
						
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Agent_Name")%></label>

					<div id="agentNameDiv"
						class='<%=(sv.agentName.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.agentName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.agentName.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   }
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.agentName.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract_Owner_Number")%></label>

					<div id="contractOwnerNumDiv"
						class='<%=(sv.contractOwnerNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.contractOwnerNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.contractOwnerNum.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.contractOwnerNum.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract_Owner_Name")%></label>

					<div id="contractOwnerNameDiv"
						class='<%=(sv.contractOwnerName.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.contractOwnerName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.contractOwnerName.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.contractOwnerName.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Number")%></label>

					<div id="lifeAssuredNumDiv"
						class='<%=(sv.lifeAssuredNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.lifeAssuredNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.lifeAssuredNum.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.lifeAssuredNum.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Name")%></label>

					<div id="lifeAssuredNameDiv"
						class='<%=(sv.lifeAssuredName.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.lifeAssuredName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.lifeAssuredName.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.lifeAssuredName.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_Age")%></label>
					<%
                              qpsf =fw.getFieldXMLDef((sv.lifeAssuredAge).getFieldName());
                              qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                              formatValue=smartHF.getPicFormatted(qpsf,sv.lifeAssuredAge);
					if (!((sv.lifeAssuredAge.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);

								}
							} 

								if (!formatValue.trim().equalsIgnoreCase("")) {
									
						%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%} else{ 
						
					} %>
					
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life_Assured_BMI")%></label>
					<%
                              qpsf =fw.getFieldXMLDef((sv.lifeAssuredBMI).getFieldName());
                              qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                              formatValue=smartHF.getPicFormatted(qpsf,sv.lifeAssuredBMI);
					if (!((sv.lifeAssuredBMI.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);

								}
							} 

								if (!formatValue.trim().equalsIgnoreCase("")) {
									
						%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%} else{ 
						
					} %>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Follow_Ups_in_Policy")%></label>

					<div id="followUpsInPolicyDiv"
						class='<%=(sv.followUpsInPolicy.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
						<%
				   if (!((sv.followUpsInPolicy.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				   
				   if (longValue == null || longValue.equalsIgnoreCase("")) {
      				   formatValue = formatValue((sv.followUpsInPolicy.getFormData()).toString());
				   } else {
        			   formatValue = formatValue(longValue); 
				   } 
				   }else {
				    
				      if (longValue == null || longValue.equalsIgnoreCase("")) {
    					  formatValue = formatValue((sv.followUpsInPolicy.getFormData()).toString());
					  } else {
					      formatValue = formatValue(longValue); 
					  } 

                  } 
			%>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total_Sum_at_Risk")%></label>
					<div id="totalSumDiv"
						class='<%=(sv.tsar.getFormData()).trim().length() == 0 ? "blank_cell":"output_cell" %>'>
						
						<%
                        qpsf =fw.getFieldXMLDef((sv.tsar).getFieldName());
                        qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                        formatValue=smartHF.getPicFormatted(qpsf,sv.tsar);
				if (!((sv.tsar.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);

							}
						} 

							if (!formatValue.trim().equalsIgnoreCase("")) {
								
					%>
				
					<%=XSSFilter.escapeHtml(formatValue)%>
				
				<%} else{ 
					
				} %>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Process_State")%></label>
						<%-- IJTI-1090 starts --%>
						<%
							String jbpmProcessInstanceUrl = IntegralWorkflowProperties.getWorkflowServiceRuntimeURL()
									+ sv.jbpmContainerId.trim() + "/images/processes/instances/" + sv.jbpmprocessinstanceID;
						%>
						<div>
							<button id="processImage" style="background: #398439;"
								class="btn btn-success btn-sm"
								onclick="return getImage('<%=jbpmProcessInstanceUrl%>')"><%=resourceBundleHandler.gettingValueFromBundle("Process_State")%></button>
						</div>
						<%-- IJTI-1090 ends --%>
					</div>
				</div>
			</div>

	</div>
</div>


<script>
			$(document).ready(function() {
				

				$("#followUpsInPolicyDiv").css("width", "50%"); 
				 


			});
		</script>

<%@ include file="/POLACommon2NEW.jsp"%>
<!-- IBPTE-1239 START -->
<br />
<br />
<br />
<div id="service-error"
	style="display: none; padding-left: 25px; padding-right: 900px;">
	<div class="close-1" id="close-error-box">&times;</div>
	<div id="service-error-message" style="color: red; padding-top: 10px;"></div>
</div>
<div id="close-process-image" style="display: none;">
	<span class="close-1" id="close-image">&times;</span>
	<div id="main"></div>
</div>
<script>
	// When the user clicks on <span> (x), close the modal

	var spanProcessImage = document.getElementById("close-image");
	var closeProcessImage = document.getElementById("close-process-image");
	var errorBoxButton = document.getElementById("close-error-box");
	var errorBoxMessage = document.getElementById("service-error");
	errorBoxButton.onclick = function() {
		errorBoxMessage.style.display = "none";
	}

	spanProcessImage.onclick = function() {
		closeProcessImage.style.display = "none";
	}
</script>
<!-- IBPTE-1239 END -->



