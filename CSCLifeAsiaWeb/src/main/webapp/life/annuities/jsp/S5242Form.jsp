

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5242";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	S5242ScreenVars sv = (S5242ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                                   ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bank Code       ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                   ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Account         ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "         ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"               ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                               ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"                                                   ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}
		//ILJ-86 starts
		if (appVars.ind02.isOn()) {
			sv.bankkey.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.bankCd.setReverse(BaseScreenData.REVERSED);
			sv.bankCd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.bankCd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.branchCd.setReverse(BaseScreenData.REVERSED);
			sv.branchCd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.branchCd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.bankCd.setInvisibility(BaseScreenData.INVISIBLE);
			sv.branchCd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-86 ends
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<%
				if ((new Byte((sv.bankCd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
					&& (new Byte((sv.branchCd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					
					sv.bankCd.set(sv.bankkey);
					sv.branchCd.set(sv.bankkey.sub1string(8, 10));
			%>
					<div class="col-md-5">
						<table>
							<tr>
								<td>
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
										<%
											if ((new Byte((sv.bankCd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| fw.getVariables().isScreenProtected()) {
										%>
												<%=smartHF.getHTMLVarExt(fw, sv.bankCd)%>
										<%
											} else {
										%>
											<div class="input-group" style="width: 100px;">
												<%=smartHF.getRichTextInputFieldLookup(fw, sv.bankCd)%>
												<span class="input-group-btn">
													<button class="btn btn-info" type="button"
														onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
														<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										<% } %>
									</div>
								</td>
								<td>
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Branch Code")%></label>
										<table>
											<tr>
												<td>
													<%
														if ((new Byte((sv.branchCd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| fw.getVariables().isScreenProtected()) {
													%>
														<%=smartHF.getHTMLVarExt(fw, sv.branchCd)%>
													<%
														} else {
													%>
														<div class="input-group" style="width: 100px;">
															<%=smartHF.getRichTextInputFieldLookup(fw, sv.branchCd)%>
															<span class="input-group-btn">
																<button class="btn btn-info" type="button"
																	onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
																	<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
																</button>
															</span>
														</div>
													<% } %>
												</td>
												<td>
													<%
														if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}							
														}
														else  {		
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
														}
													%>
													<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'
														style="min-width:120px;">
														<%=XSSFilter.escapeHtml(formatValue)%>
													</div>	
													<%
														longValue = null;
														formatValue = null;
													%>
												</td>
												<td>
													<%
														if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}							
														}
														else  {		
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
														}
													%>
													<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'
														style="min-width:120px;">
														<%=XSSFilter.escapeHtml(formatValue)%>
													</div>	
													<%
														longValue = null;
														formatValue = null;
													%>
												</td>
												<div style='visibility: hidden;'>
													<%=smartHF.getRichTextInputFieldLookup(fw, sv.bankkey, (sv.bankkey.getLength()))%>
												</div>
											</tr>
										</table>
									</div>
								</tr>
							</table>
						 </div>
				<%} %>
			<%
				if ((new Byte((sv.bankkey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
				<div class="col-md-4">
					<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>

					<table>
						<tr>
							<td style="min-width: 150px">
								<%
									if ((new Byte((sv.bankkey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.bankkey.getFormData();
 %> <%
 	if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div style="margin-right: 2px;"
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
								<div class="input-group">
								<!-- ILIFE-5801 -->
									<input name='bankkey' id='bankkey' type='text'
										value='<%=sv.bankkey.getFormData()%>'
										maxLength='<%=sv.bankkey.getLength()%>'
										size='<%=sv.bankkey.getLength() + 3%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(bankkey)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.bankkey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info" 
											type="button"
											onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>


									<%
										} else {
									%>

									class = '
									<%=(sv.bankkey).getColor() == null ? "input_cell"
								: (sv.bankkey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
										class="input-group-btn">
										<button class="btn btn-info" style="font-size: 20px;left: 3px;"
											type="button"
											onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>


									<%
										}
												longValue = null;
											}
										}
									%>
								
							</td>
							
							<td>
								<%
									if ((new Byte((sv.bankdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 120px;margin-left: -2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>


								</div>

							</td>
							
							<td>
								<%
									if ((new Byte((sv.branchdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.branchdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.branchdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 250px; min-width: 120px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<%
				}
			%>
		</div>

		<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5 : Task 1  -->

		<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5 : Task 1  -->


		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<%
						if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Account")%></label>

					<%
						}
					%>

					<table>
						<tr>
							<td style="min-width: 150px">
								<%
									if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	longValue = sv.bankacckey.getFormData();
 %> <%
 	if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div style="width: 200px;margin-right: 2px;"
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
								<div class="input-group">
								<!-- ILIFE-5801 -->
									<input name='bankacckey' id='bankacckey' type='text'
										value='<%=sv.bankacckey.getFormData()%>'
										maxLength='<%=sv.bankacckey.getLength()%>'
										size='<%=sv.bankacckey.getLength()%>' onFocus='doFocus(this)'
										onHelp='return fieldHelp(bankkey)'
										onKeyUp='return checkMaxLength(this)'
										<%if ((new Byte((sv.bankkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" class="output_cell">

									<%
										} else if ((new Byte((sv.bankacckey).getHighLight()))
														.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
									%>
									class="bold_cell" > <span class="input-group-btn">
										<button class="btn btn-info" style="font-size: 20px;left: 3px;"
											type="button"
											onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> <%
 	} else {
 %> class = ' <%=(sv.bankacckey).getColor() == null ? "input_cell"
								: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
								class="input-group-btn">
									<button class="btn btn-info" type="button"
										onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</span>
								</div> <%
 	}
 			longValue = null;
 		}
 	}
 %>
							</td>
							
							<td style="min-width: 120px">
								<%
									if ((new Byte((sv.bankaccdsc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.bankaccdsc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div style="margin-left: -2px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>

							</td>
						</tr>
					</table>
					<div style='visibility: hidden;'>
						<table>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
								<td width='188'>
									<%
										if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
								</td>
							</tr>
							<tr style='height: 22px;'>
								<td width='188'>
									<%
										if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<div class="label_txt">
										<%=resourceBundleHandler.gettingValueFromBundle("")%>
									</div> <%
 	}
 %>
								
							</tr>
						</table>
					</div>
					<br />
				</div>

			</div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

