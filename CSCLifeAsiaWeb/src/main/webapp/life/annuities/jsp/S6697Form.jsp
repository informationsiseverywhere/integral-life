


<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!-- ILIFE-2584 - Removed continue button -->
<!-- <%=((SMARTHTMLFormatter) AppVars.hf).getHTMLCBVar(19, 25)%> -->
<%
	String screenName = "S6697";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	S6697ScreenVars sv = (S6697ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6697screenWritten.gt(0)) {
%>
<%
	S6697screen.clearClassString(sv);
%>
<%
	StringData generatedText1 = new StringData("Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText2 = new StringData("Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText3 = new StringData("Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText17 = new StringData("SFO Approval No ");
%>
<%
	sv.sfonum.setClassString("");
%>
<%
	sv.sfonum.appendClassString("string_fld");
		sv.sfonum.appendClassString("input_txt");
		sv.sfonum.appendClassString("highlight");
%>
<%
	StringData generatedText20 = new StringData("Employee Contribution % ");
%>
<%
	sv.empconpc.setClassString("");
%>
<%
	sv.empconpc.appendClassString("num_fld");
		sv.empconpc.appendClassString("input_txt");
		sv.empconpc.appendClassString("highlight");
%>
<%
	StringData generatedText18 = new StringData("SFO Approval Date ");
%>
<%
	sv.sfodateDisp.setClassString("");
%>
<%
	sv.sfodateDisp.appendClassString("string_fld");
		sv.sfodateDisp.appendClassString("input_txt");
		sv.sfodateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText27 = new StringData("Employee Contribution ");
%>
<%
	sv.empcon.setClassString("");
%>
<%
	sv.empcon.appendClassString("string_fld");
		sv.empcon.appendClassString("input_txt");
		sv.empcon.appendClassString("highlight");
%>
<%
	StringData generatedText21 = new StringData("Issue");
%>
<%
	StringData generatedText22 = new StringData("Date ");
%>
<%
	sv.issdateDisp.setClassString("");
%>
<%
	sv.issdateDisp.appendClassString("string_fld");
		sv.issdateDisp.appendClassString("input_txt");
		sv.issdateDisp.appendClassString("highlight");
%>
<%
	StringData generatedText28 = new StringData("Pension Type ");
%>
<%
	sv.pentype.setClassString("");
%>
<%
	sv.pentype.appendClassString("string_fld");
		sv.pentype.appendClassString("input_txt");
		sv.pentype.appendClassString("highlight");
%>
<%
	StringData generatedText19 = new StringData("Administrators Ref ");
%>
<%
	sv.adminref.setClassString("");
%>
<%
	sv.adminref.appendClassString("string_fld");
		sv.adminref.appendClassString("input_txt");
		sv.adminref.appendClassString("highlight");
%>
<%
	StringData generatedText23 = new StringData("Coverage/Rider");
%>
<%
	generatedText23.appendClassString("label_txt");
		generatedText23.appendClassString("underline");
%>
<%
	StringData generatedText24 = new StringData("Contribution Type");
%>
<%
	generatedText24.appendClassString("label_txt");
		generatedText24.appendClassString("underline");
%>
<%
	StringData generatedText25 = new StringData("Max Funding Limit");
%>
<%
	generatedText25.appendClassString("label_txt");
		generatedText25.appendClassString("underline");
%>
<%
	StringData generatedText4 = new StringData("1");
%>
<%
	sv.crcode01.setClassString("");
%>
<%
	sv.crcode01.appendClassString("string_fld");
		sv.crcode01.appendClassString("input_txt");
		sv.crcode01.appendClassString("highlight");
%>
<%
	sv.contype01.setClassString("");
%>
<%
	sv.contype01.appendClassString("string_fld");
		sv.contype01.appendClassString("input_txt");
		sv.contype01.appendClassString("highlight");
%>
<%
	sv.maxfund01.setClassString("");
%>
<%
	sv.maxfund01.appendClassString("string_fld");
		sv.maxfund01.appendClassString("input_txt");
		sv.maxfund01.appendClassString("highlight");
%>
<%
	StringData generatedText5 = new StringData("2");
%>
<%
	sv.crcode02.setClassString("");
%>
<%
	sv.crcode02.appendClassString("string_fld");
		sv.crcode02.appendClassString("input_txt");
		sv.crcode02.appendClassString("highlight");
%>
<%
	sv.contype02.setClassString("");
%>
<%
	sv.contype02.appendClassString("string_fld");
		sv.contype02.appendClassString("input_txt");
		sv.contype02.appendClassString("highlight");
%>
<%
	sv.maxfund02.setClassString("");
%>
<%
	sv.maxfund02.appendClassString("string_fld");
		sv.maxfund02.appendClassString("input_txt");
		sv.maxfund02.appendClassString("highlight");
%>
<%
	StringData generatedText6 = new StringData("3");
%>
<%
	sv.crcode03.setClassString("");
%>
<%
	sv.crcode03.appendClassString("string_fld");
		sv.crcode03.appendClassString("input_txt");
		sv.crcode03.appendClassString("highlight");
%>
<%
	sv.contype03.setClassString("");
%>
<%
	sv.contype03.appendClassString("string_fld");
		sv.contype03.appendClassString("input_txt");
		sv.contype03.appendClassString("highlight");
%>
<%
	sv.maxfund03.setClassString("");
%>
<%
	sv.maxfund03.appendClassString("string_fld");
		sv.maxfund03.appendClassString("input_txt");
		sv.maxfund03.appendClassString("highlight");
%>
<%
	StringData generatedText7 = new StringData("4");
%>
<%
	sv.crcode04.setClassString("");
%>
<%
	sv.crcode04.appendClassString("string_fld");
		sv.crcode04.appendClassString("input_txt");
		sv.crcode04.appendClassString("highlight");
%>
<%
	sv.contype04.setClassString("");
%>
<%
	sv.contype04.appendClassString("string_fld");
		sv.contype04.appendClassString("input_txt");
		sv.contype04.appendClassString("highlight");
%>
<%
	sv.maxfund04.setClassString("");
%>
<%
	sv.maxfund04.appendClassString("string_fld");
		sv.maxfund04.appendClassString("input_txt");
		sv.maxfund04.appendClassString("highlight");
%>
<%
	StringData generatedText8 = new StringData("5");
%>
<%
	sv.crcode05.setClassString("");
%>
<%
	sv.crcode05.appendClassString("string_fld");
		sv.crcode05.appendClassString("input_txt");
		sv.crcode05.appendClassString("highlight");
%>
<%
	sv.contype05.setClassString("");
%>
<%
	sv.contype05.appendClassString("string_fld");
		sv.contype05.appendClassString("input_txt");
		sv.contype05.appendClassString("highlight");
%>
<%
	sv.maxfund05.setClassString("");
%>
<%
	sv.maxfund05.appendClassString("string_fld");
		sv.maxfund05.appendClassString("input_txt");
		sv.maxfund05.appendClassString("highlight");
%>
<%
	StringData generatedText9 = new StringData("6");
%>
<%
	sv.crcode06.setClassString("");
%>
<%
	sv.crcode06.appendClassString("string_fld");
		sv.crcode06.appendClassString("input_txt");
		sv.crcode06.appendClassString("highlight");
%>
<%
	sv.contype06.setClassString("");
%>
<%
	sv.contype06.appendClassString("string_fld");
		sv.contype06.appendClassString("input_txt");
		sv.contype06.appendClassString("highlight");
%>
<%
	sv.maxfund06.setClassString("");
%>
<%
	sv.maxfund06.appendClassString("string_fld");
		sv.maxfund06.appendClassString("input_txt");
		sv.maxfund06.appendClassString("highlight");
%>
<%
	StringData generatedText10 = new StringData("7");
%>
<%
	sv.crcode07.setClassString("");
%>
<%
	sv.crcode07.appendClassString("string_fld");
		sv.crcode07.appendClassString("input_txt");
		sv.crcode07.appendClassString("highlight");
%>
<%
	sv.contype07.setClassString("");
%>
<%
	sv.contype07.appendClassString("string_fld");
		sv.contype07.appendClassString("input_txt");
		sv.contype07.appendClassString("highlight");
%>
<%
	sv.maxfund07.setClassString("");
%>
<%
	sv.maxfund07.appendClassString("string_fld");
		sv.maxfund07.appendClassString("input_txt");
		sv.maxfund07.appendClassString("highlight");
%>
<%
	StringData generatedText11 = new StringData(" 8");
%>
<%
	sv.crcode08.setClassString("");
%>
<%
	sv.crcode08.appendClassString("string_fld");
		sv.crcode08.appendClassString("input_txt");
		sv.crcode08.appendClassString("highlight");
%>
<%
	sv.contype08.setClassString("");
%>
<%
	sv.contype08.appendClassString("string_fld");
		sv.contype08.appendClassString("input_txt");
		sv.contype08.appendClassString("highlight");
%>
<%
	sv.maxfund08.setClassString("");
%>
<%
	sv.maxfund08.appendClassString("string_fld");
		sv.maxfund08.appendClassString("input_txt");
		sv.maxfund08.appendClassString("highlight");
%>
<%
	StringData generatedText12 = new StringData(" 9");
%>
<%
	sv.crcode09.setClassString("");
%>
<%
	sv.crcode09.appendClassString("string_fld");
		sv.crcode09.appendClassString("input_txt");
		sv.crcode09.appendClassString("highlight");
%>
<%
	sv.contype09.setClassString("");
%>
<%
	sv.contype09.appendClassString("string_fld");
		sv.contype09.appendClassString("input_txt");
		sv.contype09.appendClassString("highlight");
%>
<%
	sv.maxfund09.setClassString("");
%>
<%
	sv.maxfund09.appendClassString("string_fld");
		sv.maxfund09.appendClassString("input_txt");
		sv.maxfund09.appendClassString("highlight");
%>
<%
	StringData generatedText13 = new StringData("10");
%>
<%
	sv.crcode10.setClassString("");
%>
<%
	sv.crcode10.appendClassString("string_fld");
		sv.crcode10.appendClassString("input_txt");
		sv.crcode10.appendClassString("highlight");
%>
<%
	sv.contype10.setClassString("");
%>
<%
	sv.contype10.appendClassString("string_fld");
		sv.contype10.appendClassString("input_txt");
		sv.contype10.appendClassString("highlight");
%>
<%
	sv.maxfund10.setClassString("");
%>
<%
	sv.maxfund10.appendClassString("string_fld");
		sv.maxfund10.appendClassString("input_txt");
		sv.maxfund10.appendClassString("highlight");
%>
<%
	StringData generatedText14 = new StringData("11");
%>
<%
	sv.crcode11.setClassString("");
%>
<%
	sv.crcode11.appendClassString("string_fld");
		sv.crcode11.appendClassString("input_txt");
		sv.crcode11.appendClassString("highlight");
%>
<%
	sv.contype11.setClassString("");
%>
<%
	sv.contype11.appendClassString("string_fld");
		sv.contype11.appendClassString("input_txt");
		sv.contype11.appendClassString("highlight");
%>
<%
	sv.maxfund11.setClassString("");
%>
<%
	sv.maxfund11.appendClassString("string_fld");
		sv.maxfund11.appendClassString("input_txt");
		sv.maxfund11.appendClassString("highlight");
%>
<%
	StringData generatedText15 = new StringData("12");
%>
<%
	sv.crcode12.setClassString("");
%>
<%
	sv.crcode12.appendClassString("string_fld");
		sv.crcode12.appendClassString("input_txt");
		sv.crcode12.appendClassString("highlight");
%>
<%
	sv.contype12.setClassString("");
%>
<%
	sv.contype12.appendClassString("string_fld");
		sv.contype12.appendClassString("input_txt");
		sv.contype12.appendClassString("highlight");
%>
<%
	sv.maxfund12.setClassString("");
%>
<%
	sv.maxfund12.appendClassString("string_fld");
		sv.maxfund12.appendClassString("input_txt");
		sv.maxfund12.appendClassString("highlight");
%>
<%
	StringData generatedText16 = new StringData("13");
%>
<%
	sv.crcode13.setClassString("");
%>
<%
	sv.crcode13.appendClassString("string_fld");
		sv.crcode13.appendClassString("input_txt");
		sv.crcode13.appendClassString("highlight");
%>
<%
	sv.contype13.setClassString("");
%>
<%
	sv.contype13.appendClassString("string_fld");
		sv.contype13.appendClassString("input_txt");
		sv.contype13.appendClassString("highlight");
%>
<%
	sv.maxfund13.setClassString("");
%>
<%
	sv.maxfund13.appendClassString("string_fld");
		sv.maxfund13.appendClassString("input_txt");
		sv.maxfund13.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			appVars.rolldown();
			appVars.rollup();
		}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
.panel{
height: 700px !important;
}

</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company ")%></label>

					<div style="width: 50px">

						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table ")%></label>
					<div style="width: 100px">

						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

					</div>
				</div>
			</div>





			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item ")%></label>
					<table>
						<tr>
							<td>
								<!-- 	<%=smartHF.getHTMLVar(1, 63, fw, sv.item)%> --> <!-- 	<%=smartHF.getHTMLVar(2, 63, fw, sv.longdesc)%> -->




								<%
									if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.item.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.item.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>

							</td>
							<td>
								<%
									if (((BaseScreenData) sv.longdesc) instanceof StringBase) {
								%> <%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.longdesc.getLength() + 1), null)
							.replace("absolute", "relative")%> <%
 	} else if (((BaseScreenData) sv.longdesc) instanceof DecimalData) {
 %> <%=smartHF.getHTMLVar(0, 0, fw, sv.longdesc, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
								<%
									}
								%>

							</td>
						</tr>
					</table>
				</div>
			</div>



		</div>






		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("SFO Approval No ")%></label>
					<div style="width: 150px">
						<%
							if (!((sv.sfonum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.sfonum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.sfonum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>


					</div>
				</div>
			</div>







			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Employee Contribution % ")%></label>
					<div style="width: 80px">
						<%
							if (((BaseScreenData) sv.empconpc) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.empconpc.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.empconpc) instanceof DecimalData) {
						%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.empconpc, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						<%
							}
						%>

					</div>
				</div>
			</div>






			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("SFO Approval Date ")%></label>
					<div style="width: 120px">
						<%
							if (!((sv.sfodateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.sfodateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.sfodateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Employee Contribution ")%></label>
					<div style="width: 50px">
						<%
							if (!((sv.empcon.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.empcon.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.empcon.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Issue Date")%></label>
					<div style="width: 150px">

						<%=smartHF.getHTMLCalNSVar(fw, sv.issdateDisp)%>
						<%-- 
           <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="sv.issdateDisp" data-link-format="dd/mm/yyyy">
           
            <%=smartHF.getRichTextDateInput(fw, sv.issdateDisp)%>
			 
			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	   </div> --%>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Pension Type ")%></label>
					<div style="width: 50px">
						<%
							if (!((sv.pentype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pentype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pentype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>



					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Administrators Ref ")%></label>
					<div style="width: 100px">


						<%
							if (!((sv.adminref.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.adminref.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.adminref.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6697' width='100%'>
						<thead>
							<tr>
								<th></th>
								<th>Coverage/Rider</th>
								<th>Contribution Type</th>
								<th>Max Funding Limit</th>

							</tr>
						</thead>


						<tbody>

							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									1</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode01.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype01.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund01.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									2</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode02.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype02.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund02.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									3</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode03.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype03.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund03.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									4</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode04.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype04.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund04.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									5</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode05.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype05.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund05.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									6</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode06.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype06.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund06.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									7</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode07.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype07.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund07.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									8</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode08.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype08.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund08.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									9</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode09.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype09.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund09.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									10</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode10.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype10.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund10.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									11</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode11.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype11.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund11.getFormData()%></td>
							</tr>


							<tr class="tableRowTag">
								<td class="tableDataTag" style="width: 20px;" align="center">
									12</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode12.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype12.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund12.getFormData()%></td>
							</tr>


							<tr class="tableRowTag" bgcolor="#FFFFFF">
								<td class="tableDataTag" style="width: 20px;" align="center">
									13</td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.crcode13.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.contype13.getFormData()%></td>
								<td class="tableDataTag" style="width: 150px;" align="center">
									<%=sv.maxfund13.getFormData()%></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<%
	}
%>

<%@ include file="/POLACommon2NEW.jsp"%>
<script>
	$(document).ready(function() {
		$('#dataTables-s6697').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true
		});
	});
</script>