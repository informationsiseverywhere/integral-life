<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl39";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sjl39ScreenVars sv = (Sjl39ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind13.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}

		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%></td>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
							</td>
							<td
								style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>
							</td>


						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Status")%></label>
					<div class="input-group"
						style="max-width: 145px; min-width: 100px; text-align: right;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.annpmntstatus, true)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Owner"))%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%></td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ownername, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%>
							</td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary / Payee")%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.payee, true)%>
							</td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.payeename, true)%>
							</td>
						</tr>
					</table>					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Option")%></label>
				<%	fieldItem=appVars.loadF4FieldsLong(new String[] {"annpmntopt"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("annpmntopt");
						longValue = (String) mappedItems.get((sv.annpmntopt.getFormData()).toString().trim());  
					%>
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 	"blank_cell" : "output_cell" %>'style="width:200px;">  
	   					<%if(longValue != null){%>
	   					<%=longValue%>
		   				<%}%>
	  				 </div>
   				<%
					longValue = null;
					formatValue = null;
				%>  
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Term")%></label>
						<div class="input-group" style="max-width: 120px; min-width: 100px; text-align: right;">
							
								<%
								qpsf = fw.getFieldXMLDef((sv.annupmnttrm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

					<input name='annupmnttrm' type='text'
						<%if ((sv.annupmnttrm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 70px" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.annupmnttrm)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.annupmnttrm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.annupmnttrm)%>' <%}%>
						size='<%=sv.annupmnttrm.getLength()%>'
						maxLength='<%=sv.annupmnttrm.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(annupmnttrm)'
						onKeyUp='return checkMaxLength(this)' style="width:80px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						readonly="true" class="output_cell" style="width:80px;"
						>
						</div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Deferred Term")%></label>
						<div class="input-group" style="max-width: 120px; min-width: 100px; text-align: right;">
						
							<%
								qpsf = fw.getFieldXMLDef((sv.dfrrdtrm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

					<input name='dfrrdtrm' type='text'
						<%if ((sv.dfrrdtrm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 70px" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.dfrrdtrm)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.dfrrdtrm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.dfrrdtrm)%>' <%}%>
						size='<%=sv.dfrrdtrm.getLength()%>'
						maxLength='<%=sv.dfrrdtrm.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(dfrrdtrm)'
						onKeyUp='return checkMaxLength(this)' style="width:80px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						readonly="true" class="output_cell" style="width:80px;"
						>
						</div>
						
						
				</div>
			</div>

				<div class="col-md-3">
		  <div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Frequency")%></label>
				<div class="input-group">							
					<%	fieldItem=appVars.loadF4FieldsLong(new String[] {"annpmntfreq"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("annpmntfreq");
						longValue = (String) mappedItems.get((sv.annpmntfreq.getFormData()).toString().trim());  
					%>
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 	"blank_cell" : "output_cell" %>'style="width:200px;">  
	   					<%if(longValue != null){%>
	   					<%=longValue%>
		   				<%}%>
	  				 </div>
   				<%
					longValue = null;
					formatValue = null;
				%>   																			
				</div>
			</div>
		</div>
	
		</div>
		<div class="row">
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total Annuity Fund")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.totannufund) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.totannufund,
							(sv.totannufund.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.totannufund) instanceof DecimalData) {
				%>
					<%
					if (sv.totannufund.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>
				</div>
			</div>
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Partial Payment Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.partpmntamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.partpmntamt,
							(sv.partpmntamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.partpmntamt) instanceof DecimalData) {
				%>
					<%
					if (sv.partpmntamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.partpmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.partpmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Deferred Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null,sv.dfrrdamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				</div>
			</div>
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.annupmntamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmntamt,
							(sv.annupmntamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.annupmntamt) instanceof DecimalData) {
				%>
					<%
					if (sv.annupmntamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
		</div>
	</div>
</div>

<%GeneralTable sfl = fw.getTable("sjl39screensfl");%>

<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover "
					id='dataTables-sjl39' width='100%'>
					<thead>
						<tr class='info'>

							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header9") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header10") %></th>
							<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header11") %></th>

						</tr>
					</thead>
					
				
					<tbody>
					<%
					
						String backgroundcolor="#FFFFFF";
						
						Sjl39screensfl
						.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (Sjl39screensfl
						.hasMoreScreenRows(sfl)) {
						
						%>

					<%
					{
							
							if (appVars.ind01.isOn()) {
								sv.select.setReverse(BaseScreenData.REVERSED);
							}
							if (appVars.ind02.isOn()) {
								sv.select.setEnabled(BaseScreenData.DISABLED);
							}
							
							if (appVars.ind13.isOn()) {
								sv.select.setInvisibility(BaseScreenData.INVISIBLE);
							}
							
							if (appVars.ind01.isOn()) {
								sv.select.setReverse(BaseScreenData.REVERSED);
								sv.select.setColor(BaseScreenData.RED);
							}
							if (!appVars.ind01.isOn()) {
								sv.select.setHighLight(BaseScreenData.BOLD);
							}
						}
					
						%>



					<tr style="background:<%= backgroundcolor%>;">



						<td
							<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							align="right" <% }else {%> align="center" <%}%>>
							<% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())
							||(sv.pmntstatus.equals("Terminate"))){//ILJ-567%>
							<input type="radio"
							value='<%= sv.select.getFormData() %>' onFocus='doFocus(this)'
							onHelp='return fieldHelp("sjl39screensfl" + "." +
						 "select")'
							onKeyUp='return checkMaxLength(this)'
							name='sjl39screensfl.select_R<%=count%>'
							id='sjl39screensfl.select_R<%=count%>'
							onClick="selectedRow('sjl39screensfl.select_R<%=count%>')"
							class="radio" disabled="disabled" /> <%}else{ %>
							<!-- ILIFE-8442 --> <input type="radio"
							value='<%= sv.select.getFormData() %>' onFocus='doFocus(this)'
							onHelp='return fieldHelp("sjl39screensfl" + "." +
						 "select")'
							onKeyUp='return checkMaxLength(this)'
							name='sjl39screensfl.select_R<%=count%>'
							id='sjl39screensfl.select_R<%=count%>'
							onClick="selectedRow('sjl39screensfl.select_R<%=count%>')"
							class="radio" /> <%}%>

						</td>

						<td style="width: 150px;"><%= sv.fillh.getFormData()%><%= sv.pmntno.getFormData()%><%= sv.filll.getFormData()%>



						</td>
						<td style="width: 130px;"><%= sv.pmntdateDisp.getFormData()%>



						</td>
						<td style="width: 120px;">
							<%
								if (((BaseScreenData) sv.annuityamt) instanceof StringBase) {
							%> <%=smartHF.getRichText(0, 0, fw, sv.annuityamt,
										(sv.annuityamt.getLength() + 1), null).replace(
										"absolute", "relative")%> <%
								} else if (((BaseScreenData) sv.annuityamt) instanceof DecimalData) {
							%> <%
								if (sv.annuityamt.equals(0)) {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.annuityamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
										<%
								} else {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.annuityamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'output_cell \' ",
													"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
								}
							%> <%
								} else {
							%> <%
								}
							%>
						</td>
						<td style="width: 120px;">
						    <%
								if (((BaseScreenData) sv.intrate) instanceof StringBase) {
							%> <%=smartHF.getRichText(0, 0, fw, sv.intrate,
										(sv.intrate.getLength() + 1), null).replace(
										"absolute", "relative")%> <%
								} else if (((BaseScreenData) sv.intrate) instanceof DecimalData) {
							%> <%
								if (sv.intrate.equals(0)) {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.intrate,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
										<%
								} else {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.intrate,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'output_cell \' ",
													"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
								}
							%> <%
								} else {
							%> <%
								}
							%>
						</td>
						<td style="width: 130px;">
						    <%
								if (((BaseScreenData) sv.annuitywint) instanceof StringBase) {
							%> <%=smartHF.getRichText(0, 0, fw, sv.annuitywint,
										(sv.annuitywint.getLength() + 1), null).replace(
										"absolute", "relative")%> <%
								} else if (((BaseScreenData) sv.annuitywint) instanceof DecimalData) {
							%> <%
								if (sv.annuitywint.equals(0)) {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.annuitywint,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
										<%
								} else {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.annuitywint,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'output_cell \' ",
													"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
								}
							%> <%
								} else {
							%> <%
								}
							%>
						</td>
						<td style="width: 130px;">
						    <%
								if (((BaseScreenData) sv.wholdtax) instanceof StringBase) {
							%> <%=smartHF.getRichText(0, 0, fw, sv.wholdtax,
										(sv.wholdtax.getLength() + 1), null).replace(
										"absolute", "relative")%> <%
								} else if (((BaseScreenData) sv.wholdtax) instanceof DecimalData) {
							%> <%
								if (sv.wholdtax.equals(0)) {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.wholdtax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
										<%
								} else {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.wholdtax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'output_cell \' ",
													"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
								}
							%> <%
								} else {
							%> <%
								}
							%>
						</td>
						<td style="width: 130px;">
						    <%
								if (((BaseScreenData) sv.paidamt) instanceof StringBase) {
							%> <%=smartHF.getRichText(0, 0, fw, sv.paidamt,
										(sv.paidamt.getLength() + 1), null).replace(
										"absolute", "relative")%> <%
								} else if (((BaseScreenData) sv.paidamt) instanceof DecimalData) {
							%> <%
								if (sv.paidamt.equals(0)) {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.paidamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
										<%
								} else {
							%> <%=smartHF.getHTMLVar(0,0,fw,sv.paidamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
											.replace("class=\'output_cell \' ",
													"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
								}
							%> <%
								} else {
							%> <%
								}
							%>
						</td>
						<td style="width: 130px;"><%= sv.pmntstatus.getFormData()%></td>



					</tr>


					<%
						if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
							backgroundcolor="#ededed";
							}else{
							backgroundcolor="#FFFFFF";
							}
						count = count + 1;
						Sjl39screensfl
						.setNextScreenRow(sfl, appVars, sv);
						}
						%>
						</tbody>
				</table>
			</div>
		</div>
		
			
	</div>
<%-- <input type="text" style="visibility: hidden" id="action1" value='<%= sv.action1 %>'>	 --%>

<%-- 	ILJ-286 --%>
   <input type="hidden" id="flagind" name="action1" value=""/>
        
	<table>
		<tr>
			<td>
				<div class="sectionbutton">
<%-- 					<a href="#" id="action1" onClick="JavaScript:checkSearchVal('2')" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Process Final Payment")%></a> --%>
				
					<a href="javascript:;" onClick='flag("1");' class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Process Final Payment")%></a>
				
				</div>
				
<%-- 			<input type="hidden" id="flagind"  value="<%=sv.action1.getFormData()%>"> --%>
				
			</td>
		</tr>
	</table>
</div>

<script>
$(document).ready(function() {
	$('#dataTables-sjl39').DataTable({
		ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollCollapse:true,
    	scrollY: "350px",
    	bautoWidth: false,
    	paging: false,
  	});
});


function checkSearchVal() {
	alert ( "BEFORE CLICK" );
	
	document.getElementById('action1').value = '1';
//	document.getElementById("action1").val("1");
	alert ( "AFTER CLICK" );
	doAction("PFKEY0");
}


function flag(flagind) {	
	 $("#flagind").val('1');
	doAction("PFKEY0");
}
	
	
	<%-- function checkSearchVal() {
		//	$.ClearActions();
	//	alert ( "ENTERED MTHOD" );
			//$("input[name='sjl39screensfl.action1_R1']").val("1");
			//$("input[name='sjl39screensfl.action1']").val("1");
			//$("input[name='action1']").val("1");
			
// 			var idt = "action1";

// 			$("input[name='" + idt + "']").val("1");

			var x = document.getElementById("action1");
			$(x).val("1")
			
			alert ( "AFTER CLICK  <%=x%>   " );
			doAction("PFKEY0");
		} --%>

</script>

<!-- <script>
$(document).ready(function() {
    var showval= document.getElementById('show_lbl').value;
 -->


<script language="javascript">


</script>

<script type="text/javascript">
window.onload = function()
{
setDisabledMoreBtn();
setDisabledMoreBtn('dataTables-sjl39');
}
</script>


<%@ include file="/POLACommon2NEW.jsp"%>

