

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S6698";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6698ScreenVars sv = (S6698ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Valid From ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"% Basic Tax Relief ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Inland Revenue No ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Earnings Cap ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "DSS");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To Age");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Maximum % of Gross Salary");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.itmfrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmfrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itmfrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.itmtoDisp.setReverse(BaseScreenData.REVERSED);
			sv.itmtoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.itmtoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ageIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ageIssageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.ageIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.ageIssageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.ageIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.ageIssageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.ageIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ageIssageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.ageIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ageIssageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.ageIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ageIssageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit06.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.ageIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.ageIssageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit07.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.ageIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.ageIssageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit08.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.ageIssageTo09.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.ageIssageTo09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit09.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.ageIssageTo10.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.ageIssageTo10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.pclimit10.setEnabled(BaseScreenData.DISABLED);
		}
	}
%>

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>

							<td>&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Inland Revenue No."))%></label>
					<div class="input-group">
						<%
							longValue = sv.inrevnum.getFormData();
						%>

						<%
							if ((new Byte((sv.inrevnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="width: 70px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='inrevnum' type='text'
							value='<%=sv.inrevnum.getFormData()%>'
							maxLength='<%=sv.inrevnum.getLength()%>'
							size='<%=sv.inrevnum.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(inrevnum)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.inrevnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.inrevnum).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">

							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('inrevnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.inrevnum).getColor() == null ? "input_cell"
							: (sv.inrevnum).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-btn">

							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('inrevnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>


			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group" style="width: 120px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("% Basic Tax Relief"))%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.taxrelpc).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='taxrelpc' type='text'
						<%if ((sv.taxrelpc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.taxrelpc)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.taxrelpc);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.taxrelpc)%>' <%}%>
						size='<%=sv.taxrelpc.getLength()%>'
						maxLength='<%=sv.taxrelpc.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(taxrelpc)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.taxrelpc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxrelpc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxrelpc).getColor() == null ? "input_cell"
						: (sv.taxrelpc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>


				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("DSS  No"))%></label>
					<div class="input-group">
						<%
							longValue = sv.dssnum.getFormData();
						%>

						<%
							if ((new Byte((sv.dssnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="width: 70px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>
						<%
							} else {
						%>
						<input name='dssnum' type='text'
							value='<%=sv.dssnum.getFormData()%>'
							maxLength='<%=sv.dssnum.getLength()%>'
							size='<%=sv.dssnum.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dssnum)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dssnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.dssnum).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">

							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('dssnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.dssnum).getColor() == null ? "input_cell"
							: (sv.dssnum).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
							class="input-group-btn">

							<button class="btn btn-info" style="font-size: 19px;"
								type="button"
								onClick="doFocus(document.getElementById('dssnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>


			<div class="col-md-4"></div>

			<div class="col-md-4">
				<div class="form-group" style="width: 120px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Earnings Cap"))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.earningCap).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='earningCap' type='text'
						<%if ((sv.earningCap).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.earningCap)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.earningCap);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.earningCap)%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.earningCap.getLength(), sv.earningCap.getScale(), 3)%>'
						maxLength='<%=sv.earningCap.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(earningCap)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.earningCap).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.earningCap).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.earningCap).getColor() == null ? "input_cell"
						: (sv.earningCap).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

		</div>
		<br/>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("To Age"))%></label>
				</div>
			</div>

			<div class="col-md-1"></div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum % of Gross Salary"))%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo01' type='text'
						<%if ((sv.ageIssageTo01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo01)%>' <%}%>
						size='<%=sv.ageIssageTo01.getLength()%>'
						maxLength='<%=sv.ageIssageTo01.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo01).getColor() == null ? "input_cell"
						: (sv.ageIssageTo01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit01' type='text'
						<%if ((sv.pclimit01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit01)%>' <%}%>
						size='<%=sv.pclimit01.getLength()%>'
						maxLength='<%=sv.pclimit01.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit01).getColor() == null ? "input_cell"
						: (sv.pclimit01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo02).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo02' type='text'
						<%if ((sv.ageIssageTo02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo02)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo02)%>' <%}%>
						size='<%=sv.ageIssageTo02.getLength()%>'
						maxLength='<%=sv.ageIssageTo02.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo02).getColor() == null ? "input_cell"
						: (sv.ageIssageTo02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit02).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit02' type='text'
						<%if ((sv.pclimit02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit02)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit02)%>' <%}%>
						size='<%=sv.pclimit02.getLength()%>'
						maxLength='<%=sv.pclimit02.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit02).getColor() == null ? "input_cell"
						: (sv.pclimit02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo03).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo03' type='text'
						<%if ((sv.ageIssageTo03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo03)%>' <%}%>
						size='<%=sv.ageIssageTo03.getLength()%>'
						maxLength='<%=sv.ageIssageTo03.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo03).getColor() == null ? "input_cell"
						: (sv.ageIssageTo03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit03).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit03' type='text'
						<%if ((sv.pclimit03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit03)%>' <%}%>
						size='<%=sv.pclimit03.getLength()%>'
						maxLength='<%=sv.pclimit03.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit03).getColor() == null ? "input_cell"
						: (sv.pclimit03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo04).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo04' type='text'
						<%if ((sv.ageIssageTo04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo04)%>' <%}%>
						size='<%=sv.ageIssageTo04.getLength()%>'
						maxLength='<%=sv.ageIssageTo04.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo04).getColor() == null ? "input_cell"
						: (sv.ageIssageTo04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit04).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit04' type='text'
						<%if ((sv.pclimit04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit04)%>' <%}%>
						size='<%=sv.pclimit04.getLength()%>'
						maxLength='<%=sv.pclimit04.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit04).getColor() == null ? "input_cell"
						: (sv.pclimit04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo05).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo05' type='text'
						<%if ((sv.ageIssageTo05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo05)%>' <%}%>
						size='<%=sv.ageIssageTo05.getLength()%>'
						maxLength='<%=sv.ageIssageTo05.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo05).getColor() == null ? "input_cell"
						: (sv.ageIssageTo05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit05).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit05' type='text'
						<%if ((sv.pclimit05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit05)%>' <%}%>
						size='<%=sv.pclimit05.getLength()%>'
						maxLength='<%=sv.pclimit05.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit05).getColor() == null ? "input_cell"
						: (sv.pclimit05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo06).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo06' type='text'
						<%if ((sv.ageIssageTo06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo06)%>' <%}%>
						size='<%=sv.ageIssageTo06.getLength()%>'
						maxLength='<%=sv.ageIssageTo06.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo06).getColor() == null ? "input_cell"
						: (sv.ageIssageTo06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit06).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit06' type='text'
						<%if ((sv.pclimit06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit06)%>' <%}%>
						size='<%=sv.pclimit06.getLength()%>'
						maxLength='<%=sv.pclimit06.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit06).getColor() == null ? "input_cell"
						: (sv.pclimit06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo07).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo07' type='text'
						<%if ((sv.ageIssageTo07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo07)%>' <%}%>
						size='<%=sv.ageIssageTo07.getLength()%>'
						maxLength='<%=sv.ageIssageTo07.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo07).getColor() == null ? "input_cell"
						: (sv.ageIssageTo07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit07).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit07' type='text'
						<%if ((sv.pclimit07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit07)%>' <%}%>
						size='<%=sv.pclimit07.getLength()%>'
						maxLength='<%=sv.pclimit07.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit07).getColor() == null ? "input_cell"
						: (sv.pclimit07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo08).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo08' type='text'
						<%if ((sv.ageIssageTo08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo08)%>' <%}%>
						size='<%=sv.ageIssageTo08.getLength()%>'
						maxLength='<%=sv.ageIssageTo08.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo08).getColor() == null ? "input_cell"
						: (sv.ageIssageTo08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit08).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit08' type='text'
						<%if ((sv.pclimit08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit08)%>' <%}%>
						size='<%=sv.pclimit08.getLength()%>'
						maxLength='<%=sv.pclimit08.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit08).getColor() == null ? "input_cell"
						: (sv.pclimit08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo09).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo09' type='text'
						<%if ((sv.ageIssageTo09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo09)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo09)%>' <%}%>
						size='<%=sv.ageIssageTo09.getLength()%>'
						maxLength='<%=sv.ageIssageTo09.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo09)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo09).getColor() == null ? "input_cell"
						: (sv.ageIssageTo09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit09).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit09' type='text'
						<%if ((sv.pclimit09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit09)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit09)%>' <%}%>
						size='<%=sv.pclimit09.getLength()%>'
						maxLength='<%=sv.pclimit09.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit09)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit09).getColor() == null ? "input_cell"
						: (sv.pclimit09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.ageIssageTo10).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='ageIssageTo10' type='text'
						<%if ((sv.ageIssageTo10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo10)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.ageIssageTo10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.ageIssageTo10)%>' <%}%>
						size='<%=sv.ageIssageTo10.getLength()%>'
						maxLength='<%=sv.ageIssageTo10.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo10)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.ageIssageTo10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.ageIssageTo10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.ageIssageTo10).getColor() == null ? "input_cell"
						: (sv.ageIssageTo10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>

			<div class="col-md-3"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.pclimit10).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>

					<input name='pclimit10' type='text'
						<%if ((sv.pclimit10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.pclimit10)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.pclimit10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.pclimit10)%>' <%}%>
						size='<%=sv.pclimit10.getLength()%>'
						maxLength='<%=sv.pclimit10.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(pclimit10)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.pclimit10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.pclimit10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.pclimit10).getColor() == null ? "input_cell"
						: (sv.pclimit10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
		</div>

	</div>
</div>















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Valid From")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




&nbsp;
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("To")%>
</div>



	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Inland Revenue No.")%>
</div>



<br/>

<%	
	longValue = sv.inrevnum.getFormData();  
%>

<% 
	if((new Byte((sv.inrevnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='inrevnum' 
type='text' 
value='<%=sv.inrevnum.getFormData()%>' 
maxLength='<%=sv.inrevnum.getLength()%>' 
size='<%=sv.inrevnum.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(inrevnum)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.inrevnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.inrevnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('inrevnum')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.inrevnum).getColor()== null  ? 
"input_cell" :  (sv.inrevnum).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('inrevnum')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}longValue = null;} %>


</td>

<td width='251'></td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("% Basic Tax Relief")%>
</div>



<br/>


	<%	
			qpsf = fw.getFieldXMLDef((sv.taxrelpc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='taxrelpc' 
type='text'

<%if((sv.taxrelpc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.taxrelpc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.taxrelpc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.taxrelpc) %>'
	 <%}%>

size='<%= sv.taxrelpc.getLength()%>'
maxLength='<%= sv.taxrelpc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(taxrelpc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.taxrelpc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxrelpc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxrelpc).getColor()== null  ? 
			"input_cell" :  (sv.taxrelpc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("DSS No")%>
</div>



<br/>

<%	
	longValue = sv.dssnum.getFormData();  
%>

<% 
	if((new Byte((sv.dssnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='dssnum' 
type='text' 
value='<%=sv.dssnum.getFormData()%>' 
maxLength='<%=sv.dssnum.getLength()%>' 
size='<%=sv.dssnum.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(dssnum)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.dssnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.dssnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('dssnum')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.dssnum).getColor()== null  ? 
"input_cell" :  (sv.dssnum).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('dssnum')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}longValue = null;} %>


</td>

<td width='251'></td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Earnings Cap")%>
</div>



<br/>


	<%	
			qpsf = fw.getFieldXMLDef((sv.earningCap).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='earningCap' 
type='text'

<%if((sv.earningCap).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.earningCap) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.earningCap);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.earningCap) %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.earningCap.getLength(), sv.earningCap.getScale(),3)%>'
maxLength='<%= sv.earningCap.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(earningCap)' onKeyUp='return checkMaxLength(this)'  

		onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'


<% 
	if((new Byte((sv.earningCap).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.earningCap).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.earningCap).getColor()== null  ? 
			"input_cell" :  (sv.earningCap).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("To Age")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maximum % of Gross Salary")%>
</div>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo01' 
type='text'

<%if((sv.ageIssageTo01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo01) %>'
	 <%}%>

size='<%= sv.ageIssageTo01.getLength()%>'
maxLength='<%= sv.ageIssageTo01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo01).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit01' 
type='text'

<%if((sv.pclimit01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit01) %>'
	 <%}%>

size='<%= sv.pclimit01.getLength()%>'
maxLength='<%= sv.pclimit01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit01).getColor()== null  ? 
			"input_cell" :  (sv.pclimit01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo02' 
type='text'

<%if((sv.ageIssageTo02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo02) %>'
	 <%}%>

size='<%= sv.ageIssageTo02.getLength()%>'
maxLength='<%= sv.ageIssageTo02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo02).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit02' 
type='text'

<%if((sv.pclimit02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit02) %>'
	 <%}%>

size='<%= sv.pclimit02.getLength()%>'
maxLength='<%= sv.pclimit02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit02).getColor()== null  ? 
			"input_cell" :  (sv.pclimit02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo03' 
type='text'

<%if((sv.ageIssageTo03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo03) %>'
	 <%}%>

size='<%= sv.ageIssageTo03.getLength()%>'
maxLength='<%= sv.ageIssageTo03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo03).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit03' 
type='text'

<%if((sv.pclimit03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit03) %>'
	 <%}%>

size='<%= sv.pclimit03.getLength()%>'
maxLength='<%= sv.pclimit03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit03).getColor()== null  ? 
			"input_cell" :  (sv.pclimit03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo04' 
type='text'

<%if((sv.ageIssageTo04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo04) %>'
	 <%}%>

size='<%= sv.ageIssageTo04.getLength()%>'
maxLength='<%= sv.ageIssageTo04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo04).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit04' 
type='text'

<%if((sv.pclimit04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit04) %>'
	 <%}%>

size='<%= sv.pclimit04.getLength()%>'
maxLength='<%= sv.pclimit04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit04).getColor()== null  ? 
			"input_cell" :  (sv.pclimit04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo05' 
type='text'

<%if((sv.ageIssageTo05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo05) %>'
	 <%}%>

size='<%= sv.ageIssageTo05.getLength()%>'
maxLength='<%= sv.ageIssageTo05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo05).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit05' 
type='text'

<%if((sv.pclimit05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit05) %>'
	 <%}%>

size='<%= sv.pclimit05.getLength()%>'
maxLength='<%= sv.pclimit05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit05).getColor()== null  ? 
			"input_cell" :  (sv.pclimit05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo06' 
type='text'

<%if((sv.ageIssageTo06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo06) %>'
	 <%}%>

size='<%= sv.ageIssageTo06.getLength()%>'
maxLength='<%= sv.ageIssageTo06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo06).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit06' 
type='text'

<%if((sv.pclimit06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit06) %>'
	 <%}%>

size='<%= sv.pclimit06.getLength()%>'
maxLength='<%= sv.pclimit06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit06).getColor()== null  ? 
			"input_cell" :  (sv.pclimit06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo07' 
type='text'

<%if((sv.ageIssageTo07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo07) %>'
	 <%}%>

size='<%= sv.ageIssageTo07.getLength()%>'
maxLength='<%= sv.ageIssageTo07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo07).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit07' 
type='text'

<%if((sv.pclimit07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit07) %>'
	 <%}%>

size='<%= sv.pclimit07.getLength()%>'
maxLength='<%= sv.pclimit07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit07).getColor()== null  ? 
			"input_cell" :  (sv.pclimit07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo08' 
type='text'

<%if((sv.ageIssageTo08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo08) %>'
	 <%}%>

size='<%= sv.ageIssageTo08.getLength()%>'
maxLength='<%= sv.ageIssageTo08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo08).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit08' 
type='text'

<%if((sv.pclimit08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit08) %>'
	 <%}%>

size='<%= sv.pclimit08.getLength()%>'
maxLength='<%= sv.pclimit08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit08).getColor()== null  ? 
			"input_cell" :  (sv.pclimit08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo09' 
type='text'

<%if((sv.ageIssageTo09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo09) %>'
	 <%}%>

size='<%= sv.ageIssageTo09.getLength()%>'
maxLength='<%= sv.ageIssageTo09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo09).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit09' 
type='text'

<%if((sv.pclimit09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit09) %>'
	 <%}%>

size='<%= sv.pclimit09.getLength()%>'
maxLength='<%= sv.pclimit09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit09).getColor()== null  ? 
			"input_cell" :  (sv.pclimit09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.ageIssageTo10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='ageIssageTo10' 
type='text'

<%if((sv.ageIssageTo10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.ageIssageTo10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.ageIssageTo10) %>'
	 <%}%>

size='<%= sv.ageIssageTo10.getLength()%>'
maxLength='<%= sv.ageIssageTo10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ageIssageTo10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.ageIssageTo10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ageIssageTo10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ageIssageTo10).getColor()== null  ? 
			"input_cell" :  (sv.ageIssageTo10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pclimit10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='pclimit10' 
type='text'

<%if((sv.pclimit10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pclimit10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pclimit10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pclimit10) %>'
	 <%}%>

size='<%= sv.pclimit10.getLength()%>'
maxLength='<%= sv.pclimit10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(pclimit10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pclimit10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pclimit10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pclimit10).getColor()== null  ? 
			"input_cell" :  (sv.pclimit10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</tr></table><br/></div> --%>

<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5: Task 1 -->
<style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5: Task 1 -->


<%@ include file="/POLACommon2NEW.jsp"%>

