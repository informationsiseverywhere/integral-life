

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5229";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	S5229ScreenVars sv = (S5229ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Vesting Registration");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Vesting Approval");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"I - Contract Enquiry");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number      ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date       ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Action               ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.efdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.efdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.efdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- <style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style> -->
<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 20px; left: 3px;"
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>

					<%
						if ((new Byte((sv.efdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp, (sv.efdateDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="efdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp, (sv.efdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Vesting Registration")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Vesting Approval")%>
				</b></label>
			</div>


			<div class="col-md-4">
				<label class="radio-inline"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "I")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%>
				</b></label>
			</div>
		</div>
		
	 <%if (sv.actionFlag.compareTo("Y") == 0) {%>
	 
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "D")%><%=resourceBundleHandler.gettingValueFromBundle("Annuity Details Change")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "E")%><%=resourceBundleHandler.gettingValueFromBundle("Annuity Withdrawal")%>
				</b></label>
			</div>
		</div>
		
		<% } %>
		
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

