<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl11";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sjl11ScreenVars sv = (Sjl11ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind05.isOn()) {
			sv.annuitype.setReverse(BaseScreenData.REVERSED);
			sv.annuitype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.annuitype.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.annuitype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.beneftype01.setReverse(BaseScreenData.REVERSED);
			sv.beneftype01.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.beneftype01.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.beneftype01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.beneftype02.setReverse(BaseScreenData.REVERSED);
			sv.beneftype02.setColor(BaseScreenData.RED);
		}
		if (appVars.ind47.isOn()) {
			sv.beneftype02.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.beneftype02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.beneftype03.setReverse(BaseScreenData.REVERSED);
			sv.beneftype03.setColor(BaseScreenData.RED);
		}
		if (appVars.ind48.isOn()) {
			sv.beneftype03.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.beneftype03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.beneftype04.setReverse(BaseScreenData.REVERSED);
			sv.beneftype04.setColor(BaseScreenData.RED);
		}
		if (appVars.ind49.isOn()) {
			sv.beneftype04.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.beneftype04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.beneftype05.setReverse(BaseScreenData.REVERSED);
			sv.beneftype05.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.beneftype05.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.beneftype05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.beneftype06.setReverse(BaseScreenData.REVERSED);
			sv.beneftype06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind51.isOn()) {
			sv.beneftype06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.beneftype06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.defannpayfreq.setReverse(BaseScreenData.REVERSED);
			sv.defannpayfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind52.isOn()) {
			sv.defannpayfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.defannpayfreq.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<%
	sv.itmfrmDisp.appendClassString("string_fld");
		sv.itmfrmDisp.appendClassString("output_txt");
		sv.itmfrmDisp.appendClassString("highlight");
%>
<%
	sv.annuitype.setClassString("");
%>
<%
	sv.beneftype01.setClassString("");
%>
<%
	sv.beneftype02.setClassString("");
%>
<%
	sv.beneftype03.setClassString("");
%>
<%
	sv.beneftype04.setClassString("");
%>
<%
	sv.beneftype05.setClassString("");
%>
<%
	sv.beneftype06.setClassString("");
%>
<%
	sv.defannpayfreq.setClassString("");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date Effective")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmfrmDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
							<td>&nbsp;&nbsp;</td>
							<td>
								<%
									if (!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.itmtoDisp.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style=width:90px;>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div><div class="col-md-4"></div><div class="col-md-4"></div>
		</div>		

		<div class="row" style="margin-top:30px;">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Type")%></label>
				</div>
			</div>
		</div>			
		<div class="row">
			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.annuitype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
								<td><%=smartHF.getHTMLVar(0, 0, fw, sv.annuitype, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style="width: 100px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.annuitype)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('annuitype')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.anntypdesc.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 180px;'>
								<%
								if(!((sv.anntypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.anntypdesc.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.anntypdesc.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>
						</td>
					</tr>
				</table>
			</div>				
		</div>	
       <div class="row" style="margin-top:30px;">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Type")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype01, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px';>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype01)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype01')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc01.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc01.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc01.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>								
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype02, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px';>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype02)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype02')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc02.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc02.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc02.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>								
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype03, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px';>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype03)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype03')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc03.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc03.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc03.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>								
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype04, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px';>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype04)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype04')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc04.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc04.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc04.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>								
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype05, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px';>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype05)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype05')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc05.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc05.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc05.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<%
						if ((new Byte((sv.beneftype06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>								
								<td style='min-width: 45px';><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype06, true)%></td>
								<%
						} else {
					%>
								<div class="input-group" style='min-width: 100px;'>
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.beneftype06)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('beneftype06')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								<%
						}
					%>
							</div>
						</td>
						<td>
							<%
								longValue = null;
								formatValue = null;
								%>
							<div
								class='<%= (sv.bentypdesc06.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
								style='margin-left: -1px; width: 170px;'>
								<%
								if(!((sv.bentypdesc06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc06.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.bentypdesc06.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
								longValue = null;
								formatValue = null;
								%>

						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row" style="margin-top: 30px;">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Annuity Payment Frequency")%></label>
				</div>
			</div>
		</div>
		<div class="row">		
		<div class="col-md-3">
				<div class="form-group" style="width:150px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "defannpayfreq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("defannpayfreq");
						optionValue = makeDropDownList(mappedItems, sv.defannpayfreq.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.defannpayfreq.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.defannpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width:150px;">
						<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.defannpayfreq).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='defannpayfreq' type='list'
							<%if ((new Byte((sv.defannpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.defannpayfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.defannpayfreq).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<%@ include file="/POLACommon2NEW.jsp"%>