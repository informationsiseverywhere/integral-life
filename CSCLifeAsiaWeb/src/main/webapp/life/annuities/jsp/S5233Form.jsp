

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5233";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	S5233ScreenVars sv = (S5233ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract No       ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status   ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status     ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency   ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured      ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life        ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1 - Select");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Regular");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Plan");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cover/");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Effective");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Vesting");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Vesting");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Payment");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Suff");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Date");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Amount");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Lump Sum");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Details");
%>
<%
	appVars.rollup(new int[]{93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<table><tr>
					<td>
						<%
							}
						%>



						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>


						</td>
						<td>



						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="margin-left: 1px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						</td>
						<td>

						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 350px;margin-left: 1px;min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</td>
					</tr></table>
				</div>
			</div>
		</div>

		<!-- ILIFE-2431 Coding and Unit testing - Life Cross Browser - Sprint 1 D4: Task 5 starts-->
		


		<!-- ILIFE-2431 Coding and Unit testing - Life Cross Browser - Sprint 1 D4: Task 5 ends-->


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

					<%
						}
					%>



					<%
						if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<%
						}
					%>
					<table><tr>
					<td>
						<%
							if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="min-width:71px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<%
							if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						</td>
						<td>

						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="margin-left: 1px;min-width: 100px;max-width: 350px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>

					<%
						}
					%>
					<table><tr>
					<td>
						<%
							if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="min-width: 71px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						
						</td>
						<td>

						<%
							if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
			

						<%
							if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="margin-left: 1px;min-width: 100px;max-width: 200px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Regular Payment Details")%></label>
			</div>
		</div>
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[8];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.select.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.plansfx.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
			} else {
				calculatedValue = (sv.plansfx.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.life.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.life.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.coverage.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.coverage.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.rider.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = (sv.rider.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.effdateDisp.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 12;
			} else {
				calculatedValue = (sv.effdateDisp.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.vstpay.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 12;
			} else {
				calculatedValue = (sv.vstpay.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.vstlump.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 12;
			} else {
				calculatedValue = (sv.vstlump.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("s5233screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
	
    
    <div class="row">
				<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id='dataTables-s5233' width='100%'>
								<thead>
									<tr class='info'>
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>									
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
										
										<!--  <td class="tableDataHeader" style="width:<%=tblColumnWidth[4 ]%>px;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></td> -->
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
										
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
						<!-- ILIFE-2431 Coding and Unit testing - Life Cross Browser - Sprint 1 D4: Task 5 ends-->	
									</tr>
								</thead>

								<tbody>
									<%
	S5233screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5233screensfl
	.hasMoreScreenRows(sfl)) {	
%>
<tr>
<%if((new Byte((sv.select).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
					<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	&nbsp;&nbsp;
																			
								
													
													
					
					 					 
					 <input type="radio" 
						 value='<%= (sv.select).getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5233screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5233screensfl.select_R<%=count%>'
						 id='s5233screensfl.select_R<%=count%>'
						 onClick="selectedRow('s5233screensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
									</td>
		<%}else{%>
												<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
					</td>														
										
					<%}%>
								<%if((new Byte((sv.plansfx).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px; " 
					<%if((sv.plansfx).getClass().getSimpleName().equals("ZonedDecimalData")) {%> style="text-align: left"<% }else {%> align="left" <%}%> >	&nbsp;								
								
											
									
											
						<%= sv.plansfx.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.life).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
					<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >		&nbsp;							
								
											
									
											
						<%= sv.life.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px+<%=tblColumnWidth[4 ]%>px;" "text-align: center"; 
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> style="text-align: center" <%}%> >								
								
											
									
											
						<%= sv.coverage.getFormData()%>
						
						<%} %>								 
						
						<%= "/"%>
				
								<%if((new Byte((sv.rider).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									
												
								
											
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									
		<%}%>
									
				    </td>
										
					
								<%if((new Byte((sv.effdateDisp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
					<%if((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >		&nbsp;							
								
											
									
											
						<%= sv.effdateDisp.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.vstpay).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" 
					<%if((sv.vstpay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.vstpay).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.vstpay,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.vstpay).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.vstlump).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag" style="width:<%=tblColumnWidth[7 ]%>px;" 
					<%if((sv.vstlump).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> style="text-align: right" <%}%> >									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.vstlump).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.vstlump,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.vstlump).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td class="tableDataTag" style="width:<%=tblColumnWidth[7 ]%>px;" >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	S5233screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
								</tbody>
							</table>
						</div>
				</div>
			</div>
    
	
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>
<script>
	$(document).ready(function() {
		$('#dataTables-s5233').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
			info:false,
			paging:false
		});
	});
</script>

