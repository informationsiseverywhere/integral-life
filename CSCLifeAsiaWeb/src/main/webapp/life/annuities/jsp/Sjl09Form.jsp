<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String screenName = "Sjl09";
%>

<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*" %>

<%
	Sjl09ScreenVars sv = (Sjl09ScreenVars) fw.getVariables();
%>

<%
	sv.wthldngflag.setClassString("");
%>
   
<%
	{
		if (appVars.ind03.isOn()) {
			sv.wthldngflag.setReverse(BaseScreenData.REVERSED);
			sv.wthldngflag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.wthldngflag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.wthldngflag.setEnabled(BaseScreenData.DISABLED);//IJL-568
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.company.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.tabl.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.item.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.longdesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
		 <!-- ILJ-568 -->
				 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Withholding Tax needed")%></label>
				    	
				    		<input type='checkbox' name='wthldngflag' value='Y' onFocus='doFocus(this)'onHelp='return fieldHelp(wthldngflag)' onKeyUp='return checkMaxLength(this)'
	<%
							if((sv.wthldngflag).getColor()!=null){
										 %>style='background-color:#FF0000;'
									<%}
									if((sv.wthldngflag).toString().trim().equalsIgnoreCase("Y")){
										%>checked
							      
							    <% } if((sv.wthldngflag).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
					    	   disabled 
							 <%}else{%>
							    	    enabled
							    	     <% }%>
							class ='UICheck' onclick="handleCheckBox('wthldngflag')"/><!-- ILJ-568 -->
							<input type='checkbox' name='wthldngflag' value=' '
							<% if(!(sv.wthldngflag).toString().trim().equalsIgnoreCase("Y")){
										%>checked
							      <% }%>
							style="visibility: hidden" onclick="handleCheckBox('wthldngflag')"/>
				    </div>
				 </div>
				
			</div>
				 <!-- ILJ-40 : End -->
			</div>
		
					
</div> 
</div>
					





<%if (sv.Sjl09protectWritten.gt(0)) {%>
	<%Sjl09protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>