

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl14";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sjl14ScreenVars sv = (Sjl14ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Annuity Registration Create");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Annuity Registration Modify");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"I - Annuity Registration Enquiry");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Number      ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Annuity Type       ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Type       ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Action               ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.annuitype.setReverse(BaseScreenData.REVERSED);
			sv.annuitype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.annuitype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.beneftype.setReverse(BaseScreenData.REVERSED);
			sv.beneftype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.beneftype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<!-- <style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style> -->
<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<div class="input-group">
						<%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 20px; left: 3px;"
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Type")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "annuitype" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("annuitype");
						optionValue = makeDropDownList(mappedItems, sv.annuitype.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.annuitype.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.annuitype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width: 100px;">
						<%
								if (longValue != null) {
							%>

						<%=longValue%>

						<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.annuitype).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='annuitype' type='list'
							<%if ((new Byte((sv.annuitype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.annuitype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<div class='output_cell' style="width: 100px;">
								<%=optionValue%>
							</div>
						</select>
						<%
							if ("red".equals((sv.annuitype).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Type")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "beneftype" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("beneftype");
						optionValue = makeDropDownList(mappedItems, sv.beneftype.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.beneftype.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.beneftype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width: 100px;">
						<%
								if (longValue != null) {
							%>

						<%=longValue%>

						<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.beneftype).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
						<%
							}
						%>
						<select name='beneftype' type='list'
							<%if ((new Byte((sv.beneftype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.beneftype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<div class='output_cell' style="width: 100px;">
								<%=optionValue%>
							</div>
						</select>
						<%
							if ("red".equals((sv.beneftype).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>						
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Annuity Registration Create")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Annuity Registration Modify")%>
				</b></label>
			</div>


			<div class="col-md-4">
				<label class="radio-inline"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "I")%><%=resourceBundleHandler.gettingValueFromBundle("Annuity Registration Enquiry")%>
				</b></label>
			</div>
		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

