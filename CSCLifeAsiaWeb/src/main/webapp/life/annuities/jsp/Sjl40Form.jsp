<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl40";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>
<%
	Sjl40ScreenVars sv = (Sjl40ScreenVars) fw.getVariables();
%>

<%
	Sjl40screen.clearClassString(sv);
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.annpmntopt.setReverse(BaseScreenData.REVERSED);
			sv.annpmntopt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind40.isOn()) {
			sv.annpmntopt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.annpmntopt.setHighLight(BaseScreenData.BOLD);
		}		
		if (appVars.ind02.isOn()) {
			sv.annpmntstatus.setReverse(BaseScreenData.REVERSED);
			sv.annpmntstatus.setColor(BaseScreenData.RED);
		}
		if (appVars.ind41.isOn()) {
			sv.annpmntstatus.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.annpmntstatus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.nextpmntdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.nextpmntdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind42.isOn()) {
			sv.nextpmntdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.nextpmntdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.annupmntamt.setReverse(BaseScreenData.REVERSED);
			sv.annupmntamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind43.isOn()) {
			sv.annupmntamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.annupmntamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.dfrrdamt.setReverse(BaseScreenData.REVERSED);
			sv.dfrrdamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind44.isOn()) {
			sv.dfrrdamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.dfrrdamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.totbalannamt.setReverse(BaseScreenData.REVERSED);
			sv.totbalannamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.totbalannamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.totbalannamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.newpmntdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.newpmntdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.newpmntdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.newpmntdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		
	/* 	if (appVars.ind07.isOn()) {
			sv.newpmntdt.setReverse(BaseScreenData.REVERSED);
			sv.newpmntdt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.newpmntdt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.newpmntdt.setHighLight(BaseScreenData.BOLD);
		} */
		if (appVars.ind08.isOn()) {
			sv.finalpayamt.setReverse(BaseScreenData.REVERSED);
			sv.finalpayamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind47.isOn()) {
			sv.finalpayamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.finalpayamt.setHighLight(BaseScreenData.BOLD);
		}
		
	}
%>

<div class="panel panel-default">	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%></td>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
							</td>
							<td style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>
							</td>


						</tr>
					</table>
				</div>
			</div>			
			<div class="col-md-6"></div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Status")%></label>
						<div class="input-group" style="max-width: 145px; min-width: 100px; text-align: right;">
							<%=smartHF.getHTMLVar(0, 0, fw, sv.annpmntstatus, true)%>
						</div>
				</div>
			</div>
		</div>
		
	<div class="row">
		<div class="col-md-4">
		  <div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Option")%></label>
				<div class="input-group">							
					<%	fieldItem=appVars.loadF4FieldsLong(new String[] {"annpmntopt"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("annpmntopt");
						longValue = (String) mappedItems.get((sv.annpmntopt.getFormData()).toString().trim());  
					%>
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 	"blank_cell" : "output_cell" %>'style="width:200px;">  
	   					<%if(longValue != null){%>
	   					<%=longValue%>
		   				<%}%>
	  				 </div>
   				<%
					longValue = null;
					formatValue = null;
				%>   																			
				</div>
			</div>
		</div>
	</div>
		

     <div class="row">
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Next Payment Date")%></label>
					<div class="form-group" style="max-width:100px;">				
								<%=smartHF.getRichText(0, 0, fw, sv.nextpmntdtDisp,(sv.nextpmntdtDisp.getLength()),null)%>
					</div>
			</div>
			
				<div class="col-md-3">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Amount")%></label>
						<div class="form-group" style="min-width:100px;max-width: 150px;text-align: right;">
							<%=smartHF.getHTMLVar(0, 0, fw, null,sv.annupmntamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
				</div>
				
				<div class="col-md-3">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Deferred Amount")%></label>
						<div class="form-group" style="min-width:100px;max-width: 150px;text-align: right;">
							<%=smartHF.getHTMLVar(0, 0, fw, null,sv.dfrrdamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
				</div>
				
				
				<div class="col-md-3">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Balance Annuity Amount")%></label>
					<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.totbalannamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.totbalannamt,
							(sv.totbalannamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.totbalannamt) instanceof DecimalData) {
				%>
					<%
					if (sv.totbalannamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totbalannamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totbalannamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>

		</div>
		
	<div class="row">
		 <div class="col-md-3">
			<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("New Payment Date")%></label>
			<%
				if ((new Byte((sv.newpmntdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {
			%>
			<%=smartHF.getRichTextDateInput(fw, sv.newpmntdtDisp,(sv.newpmntdtDisp.getLength()))%>
			<%}else{%>
			<div class="input-group date form_date col-md-12" data-date=""
				data-date-format="dd/mm/yyyy" data-link-field="newpmntdtDisp"
				data-link-format="dd/mm/yyyy">
				<%=smartHF.getRichTextDateInput(fw, sv.newpmntdtDisp,(sv.newpmntdtDisp.getLength()))%>
				<span class="input-group-addon"><span
					class="glyphicon glyphicon-calendar"></span></span>
			</div>
			
			
			<%}%>

			</div>
		</div>
	
			<div class="col-md-3">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Final Payment")%></label>
						<div class="form-group" style="min-width:100px;max-width: 150px;text-align: right;">
							<%=smartHF.getHTMLVar(0, 0, fw, null,sv.finalpayamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
			</div>
	</div>
		

</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

