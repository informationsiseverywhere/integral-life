

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6623";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6623ScreenVars sv = (S6623ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company  ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table  ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item  ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Freq");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "In Advance");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "In Arrears");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Guaranteed");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Commutation");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Period");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Factor");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Page Up/Page Down");
%>

<%
	{
		appVars.rolldown();
		appVars.rollup();
		if (appVars.ind01.isOn()) {
			sv.freqann01.setReverse(BaseScreenData.REVERSED);
			sv.freqann01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.freqann01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.freqann02.setReverse(BaseScreenData.REVERSED);
			sv.freqann02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.freqann02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.freqann03.setReverse(BaseScreenData.REVERSED);
			sv.freqann03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.freqann03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.freqann04.setReverse(BaseScreenData.REVERSED);
			sv.freqann04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.freqann04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.freqann05.setReverse(BaseScreenData.REVERSED);
			sv.freqann05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.freqann05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.freqann06.setReverse(BaseScreenData.REVERSED);
			sv.freqann06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.freqann06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.freqann07.setReverse(BaseScreenData.REVERSED);
			sv.freqann07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.freqann07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.freqann08.setReverse(BaseScreenData.REVERSED);
			sv.freqann08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.freqann08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.freqann09.setReverse(BaseScreenData.REVERSED);
			sv.freqann09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.freqann09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.freqann10.setReverse(BaseScreenData.REVERSED);
			sv.freqann10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.freqann10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.advance01.setReverse(BaseScreenData.REVERSED);
			sv.advance01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.advance01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.advance02.setReverse(BaseScreenData.REVERSED);
			sv.advance02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.advance02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.advance03.setReverse(BaseScreenData.REVERSED);
			sv.advance03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.advance03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.advance04.setReverse(BaseScreenData.REVERSED);
			sv.advance04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.advance04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.advance05.setReverse(BaseScreenData.REVERSED);
			sv.advance05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.advance05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.advance06.setReverse(BaseScreenData.REVERSED);
			sv.advance06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.advance06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.advance07.setReverse(BaseScreenData.REVERSED);
			sv.advance07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.advance07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.advance08.setReverse(BaseScreenData.REVERSED);
			sv.advance08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.advance08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.advance09.setReverse(BaseScreenData.REVERSED);
			sv.advance09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.advance09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.advance10.setReverse(BaseScreenData.REVERSED);
			sv.advance10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.advance10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.arrears01.setReverse(BaseScreenData.REVERSED);
			sv.arrears01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.arrears01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.arrears02.setReverse(BaseScreenData.REVERSED);
			sv.arrears02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.arrears02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.arrears03.setReverse(BaseScreenData.REVERSED);
			sv.arrears03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.arrears03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.arrears04.setReverse(BaseScreenData.REVERSED);
			sv.arrears04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.arrears04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.arrears05.setReverse(BaseScreenData.REVERSED);
			sv.arrears05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.arrears05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.arrears06.setReverse(BaseScreenData.REVERSED);
			sv.arrears06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.arrears06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.arrears07.setReverse(BaseScreenData.REVERSED);
			sv.arrears07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.arrears07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.arrears08.setReverse(BaseScreenData.REVERSED);
			sv.arrears08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.arrears08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.arrears09.setReverse(BaseScreenData.REVERSED);
			sv.arrears09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.arrears09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.arrears10.setReverse(BaseScreenData.REVERSED);
			sv.arrears10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.arrears10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.guarperd01.setReverse(BaseScreenData.REVERSED);
			sv.guarperd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.guarperd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.guarperd02.setReverse(BaseScreenData.REVERSED);
			sv.guarperd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.guarperd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.guarperd03.setReverse(BaseScreenData.REVERSED);
			sv.guarperd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.guarperd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.guarperd04.setReverse(BaseScreenData.REVERSED);
			sv.guarperd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.guarperd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.guarperd05.setReverse(BaseScreenData.REVERSED);
			sv.guarperd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.guarperd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.guarperd06.setReverse(BaseScreenData.REVERSED);
			sv.guarperd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.guarperd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.guarperd07.setReverse(BaseScreenData.REVERSED);
			sv.guarperd07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.guarperd07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.guarperd08.setReverse(BaseScreenData.REVERSED);
			sv.guarperd08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.guarperd08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.guarperd09.setReverse(BaseScreenData.REVERSED);
			sv.guarperd09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.guarperd09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.guarperd10.setReverse(BaseScreenData.REVERSED);
			sv.guarperd10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.guarperd10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.comtfact01.setReverse(BaseScreenData.REVERSED);
			sv.comtfact01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.comtfact01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.comtfact02.setReverse(BaseScreenData.REVERSED);
			sv.comtfact02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.comtfact02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.comtfact03.setReverse(BaseScreenData.REVERSED);
			sv.comtfact03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.comtfact03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.comtfact04.setReverse(BaseScreenData.REVERSED);
			sv.comtfact04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.comtfact04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.comtfact05.setReverse(BaseScreenData.REVERSED);
			sv.comtfact05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.comtfact05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.comtfact06.setReverse(BaseScreenData.REVERSED);
			sv.comtfact06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.comtfact06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.comtfact07.setReverse(BaseScreenData.REVERSED);
			sv.comtfact07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.comtfact07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.comtfact08.setReverse(BaseScreenData.REVERSED);
			sv.comtfact08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.comtfact08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.comtfact09.setReverse(BaseScreenData.REVERSED);
			sv.comtfact09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.comtfact09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.comtfact10.setReverse(BaseScreenData.REVERSED);
			sv.comtfact10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.comtfact10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 75px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		&nbsp;
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Freq")%></label>

				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("In Advance")%></label>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("In Arrears")%></label>

				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Guaranteed")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Period")%></label>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Commutation")%><%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann01");
						optionValue = makeDropDownList(mappedItems, sv.freqann01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann01.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann01).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann01' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann01).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance01' type='text'
						<%formatValue = (sv.advance01.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance01.getLength()%>'
						maxLength='<%=sv.advance01.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance01)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance01).getColor() == null ? "input_cell"
						: (sv.advance01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears01' type='text'
						<%formatValue = (sv.arrears01.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears01.getLength()%>'
						maxLength='<%=sv.arrears01.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears01)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears01).getColor() == null ? "input_cell"
						: (sv.arrears01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd01' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd01)%>' <%}%>
						size='<%=sv.guarperd01.getLength()%>'
						maxLength='<%=sv.guarperd01.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd01).getColor() == null ? "input_cell"
						: (sv.guarperd01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact01).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact01' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact01)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact01);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact01)%>' <%}%>
						size='<%=sv.comtfact01.getLength()%>'
						maxLength='<%=sv.comtfact01.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact01).getColor() == null ? "input_cell"
						: (sv.comtfact01).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>


		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann02");
						optionValue = makeDropDownList(mappedItems, sv.freqann02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann02.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann02).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann02' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann02).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance02' type='text'
						<%formatValue = (sv.advance02.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance02.getLength()%>'
						maxLength='<%=sv.advance02.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance02)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance02).getColor() == null ? "input_cell"
						: (sv.advance02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears02' type='text'
						<%formatValue = (sv.arrears02.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears02.getLength()%>'
						maxLength='<%=sv.arrears02.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears02)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears02).getColor() == null ? "input_cell"
						: (sv.arrears02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd02).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd02' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd02)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd02)%>' <%}%>
						size='<%=sv.guarperd02.getLength()%>'
						maxLength='<%=sv.guarperd02.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd02).getColor() == null ? "input_cell"
						: (sv.guarperd02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact02).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact02' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact02)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact02);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact02)%>' <%}%>
						size='<%=sv.comtfact02.getLength()%>'
						maxLength='<%=sv.comtfact02.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact02).getColor() == null ? "input_cell"
						: (sv.comtfact02).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann03" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann03");
						optionValue = makeDropDownList(mappedItems, sv.freqann03.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann03.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann03).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann03' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann03).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance03' type='text'
						<%formatValue = (sv.advance03.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance03.getLength()%>'
						maxLength='<%=sv.advance03.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance03)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance03).getColor() == null ? "input_cell"
						: (sv.advance03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears03' type='text'
						<%formatValue = (sv.arrears03.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears03.getLength()%>'
						maxLength='<%=sv.arrears03.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears03)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears03).getColor() == null ? "input_cell"
						: (sv.arrears03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd03).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd03' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd03)%>' <%}%>
						size='<%=sv.guarperd03.getLength()%>'
						maxLength='<%=sv.guarperd03.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd03).getColor() == null ? "input_cell"
						: (sv.guarperd03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact03).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact03' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact03)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact03);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact03)%>' <%}%>
						size='<%=sv.comtfact03.getLength()%>'
						maxLength='<%=sv.comtfact03.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact03).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact03).getColor() == null ? "input_cell"
						: (sv.comtfact03).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann04" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann04");
						optionValue = makeDropDownList(mappedItems, sv.freqann04.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann04.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann04).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann04' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann04).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance04' type='text'
						<%formatValue = (sv.advance04.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance04.getLength()%>'
						maxLength='<%=sv.advance04.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance04)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance04).getColor() == null ? "input_cell"
						: (sv.advance04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears04' type='text'
						<%formatValue = (sv.arrears04.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears04.getLength()%>'
						maxLength='<%=sv.arrears04.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears04)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears04).getColor() == null ? "input_cell"
						: (sv.arrears04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd04).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd04' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd04)%>' <%}%>
						size='<%=sv.guarperd04.getLength()%>'
						maxLength='<%=sv.guarperd04.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd04).getColor() == null ? "input_cell"
						: (sv.guarperd04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact04).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact04' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact04)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact04);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact04)%>' <%}%>
						size='<%=sv.comtfact04.getLength()%>'
						maxLength='<%=sv.comtfact04.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact04).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact04).getColor() == null ? "input_cell"
						: (sv.comtfact04).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>
		<!-- <---4 Ends--->
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann05" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann05");
						optionValue = makeDropDownList(mappedItems, sv.freqann05.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann05.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann05).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann05' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann05).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance05' type='text'
						<%formatValue = (sv.advance05.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance05.getLength()%>'
						maxLength='<%=sv.advance05.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance05)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance05).getColor() == null ? "input_cell"
						: (sv.advance05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears05' type='text'
						<%formatValue = (sv.arrears05.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears05.getLength()%>'
						maxLength='<%=sv.arrears05.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears05)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears05).getColor() == null ? "input_cell"
						: (sv.arrears05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd05).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd05' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd05)%>' <%}%>
						size='<%=sv.guarperd05.getLength()%>'
						maxLength='<%=sv.guarperd05.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd05).getColor() == null ? "input_cell"
						: (sv.guarperd05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact05).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact05' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact05)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact05);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact05)%>' <%}%>
						size='<%=sv.comtfact05.getLength()%>'
						maxLength='<%=sv.comtfact05.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact05).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact05).getColor() == null ? "input_cell"
						: (sv.comtfact05).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann06" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann06");
						optionValue = makeDropDownList(mappedItems, sv.freqann06.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann06.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann06).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann06' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann06).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance06' type='text'
						<%formatValue = (sv.advance06.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance06.getLength()%>'
						maxLength='<%=sv.advance06.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance06)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance06).getColor() == null ? "input_cell"
						: (sv.advance06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears06' type='text'
						<%formatValue = (sv.arrears06.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears06.getLength()%>'
						maxLength='<%=sv.arrears06.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears06)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears06).getColor() == null ? "input_cell"
						: (sv.arrears06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd06).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd06' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd06)%>' <%}%>
						size='<%=sv.guarperd06.getLength()%>'
						maxLength='<%=sv.guarperd06.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd06).getColor() == null ? "input_cell"
						: (sv.guarperd06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact06).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact06' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact06)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact06);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact06)%>' <%}%>
						size='<%=sv.comtfact06.getLength()%>'
						maxLength='<%=sv.comtfact06.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact06).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact06).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact06).getColor() == null ? "input_cell"
						: (sv.comtfact06).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann07" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann07");
						optionValue = makeDropDownList(mappedItems, sv.freqann07.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann07.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann07).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann07' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann07).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance07' type='text'
						<%formatValue = (sv.advance07.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance07.getLength()%>'
						maxLength='<%=sv.advance07.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance07)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance07).getColor() == null ? "input_cell"
						: (sv.advance07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears07' type='text'
						<%formatValue = (sv.arrears07.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears07.getLength()%>'
						maxLength='<%=sv.arrears07.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears07)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears07).getColor() == null ? "input_cell"
						: (sv.arrears07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd07).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd07' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd07)%>' <%}%>
						size='<%=sv.guarperd07.getLength()%>'
						maxLength='<%=sv.guarperd07.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd07).getColor() == null ? "input_cell"
						: (sv.guarperd07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact07).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact07' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact07)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact07);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact07)%>' <%}%>
						size='<%=sv.comtfact07.getLength()%>'
						maxLength='<%=sv.comtfact07.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact07).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact07).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact07).getColor() == null ? "input_cell"
						: (sv.comtfact07).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann08" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann08");
						optionValue = makeDropDownList(mappedItems, sv.freqann08.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann08.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann08).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann08' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann08).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance08' type='text'
						<%formatValue = (sv.advance08.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance08.getLength()%>'
						maxLength='<%=sv.advance08.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance08)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance08).getColor() == null ? "input_cell"
						: (sv.advance08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears08' type='text'
						<%formatValue = (sv.arrears08.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears08.getLength()%>'
						maxLength='<%=sv.arrears08.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears08)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears08).getColor() == null ? "input_cell"
						: (sv.arrears08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd08).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd08' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd08)%>' <%}%>
						size='<%=sv.guarperd08.getLength()%>'
						maxLength='<%=sv.guarperd08.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd08).getColor() == null ? "input_cell"
						: (sv.guarperd08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact08).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact08' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact08)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact08);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact08)%>' <%}%>
						size='<%=sv.comtfact08.getLength()%>'
						maxLength='<%=sv.comtfact08.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact08).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact08).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact08).getColor() == null ? "input_cell"
						: (sv.comtfact08).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann09" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann09");
						optionValue = makeDropDownList(mappedItems, sv.freqann09.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann09.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann09).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann09' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann09).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance09' type='text'
						<%formatValue = (sv.advance09.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance09.getLength()%>'
						maxLength='<%=sv.advance09.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance09)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance09).getColor() == null ? "input_cell"
						: (sv.advance09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears09' type='text'
						<%formatValue = (sv.arrears09.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears09.getLength()%>'
						maxLength='<%=sv.arrears09.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears09)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears09).getColor() == null ? "input_cell"
						: (sv.arrears09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd09).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd09' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd09)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd09)%>' <%}%>
						size='<%=sv.guarperd09.getLength()%>'
						maxLength='<%=sv.guarperd09.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd09)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd09).getColor() == null ? "input_cell"
						: (sv.guarperd09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact09).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact09' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact09)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact09);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact09)%>' <%}%>
						size='<%=sv.comtfact09.getLength()%>'
						maxLength='<%=sv.comtfact09.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact09)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact09).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact09).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact09).getColor() == null ? "input_cell"
						: (sv.comtfact09).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqann10" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqann10");
						optionValue = makeDropDownList(mappedItems, sv.freqann10.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqann10.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqann10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqann10).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%
							}
						%>

						<select name='freqann10' type='list' style="width: 140px;"
							<%if ((new Byte((sv.freqann10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqann10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqann10).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='advance10' type='text'
						<%formatValue = (sv.advance10.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.advance10.getLength()%>'
						maxLength='<%=sv.advance10.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(advance10)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.advance10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.advance10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.advance10).getColor() == null ? "input_cell"
						: (sv.advance10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<input name='arrears10' type='text'
						<%formatValue = (sv.arrears10.getFormData()).toString();%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>'
						<%if (formatValue != null && formatValue.trim().length() > 0) {%>
						title='<%=formatValue%>' <%}%>
						size='<%=sv.arrears10.getLength()%>'
						maxLength='<%=sv.arrears10.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(arrears10)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.arrears10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 70px;"
						<%} else if ((new Byte((sv.arrears10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:70px;" <%} else {%>
						class=' <%=(sv.arrears10).getColor() == null ? "input_cell"
						: (sv.arrears10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:70px;" <%}%>>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.guarperd10).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
					%>

					<input name='guarperd10' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.guarperd10)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.guarperd10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.guarperd10)%>' <%}%>
						size='<%=sv.guarperd10.getLength()%>'
						maxLength='<%=sv.guarperd10.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd10)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.guarperd10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.guarperd10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.guarperd10).getColor() == null ? "input_cell"
						: (sv.guarperd10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.comtfact10).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.S5VS3);
					%>

					<input name='comtfact10' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.comtfact10)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.comtfact10);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.comtfact10)%>' <%}%>
						size='<%=sv.comtfact10.getLength()%>'
						maxLength='<%=sv.comtfact10.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(comtfact10)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.comtfact10).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" class="output_cell" style="max-width: 100px;"
						<%} else if ((new Byte((sv.comtfact10).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="max-width:100px;" <%} else {%>
						class=' <%=(sv.comtfact10).getColor() == null ? "input_cell"
						: (sv.comtfact10).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						style="max-width:100px;" <%}%>>
				</div>
			</div>

		</div>
		&nbsp;

		<table>
			<tr>
				<td>

					<div class="sectionbutton">
						<a href="#" onClick="JavaScript:doAction('PFKey91');"
							class="btn btn-primary btn-xs"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
					</div>
				</td>
				<td>&nbsp;&nbsp;</td>
				<td>
					<div class="sectionbutton">
						<a href="#" onClick="JavaScript:doAction('PFKey90');"
							class="btn btn-primary btn-xs"><%=resourceBundleHandler.gettingValueFromBundle("Next")%></a>
					</div>
				</td>
			</tr>
		</table>



	</div>
</div>


<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5: Task 1 -->
<style>
@media \0screen\,screen\9 {
	.output_cell {
		margin-left: 1px
	}
}
</style>
<!-- ILIFE-2432 Life Cross Browser - Sprint 1 D5: Task 1 -->
<%@ include file="/POLACommon2NEW.jsp"%>

