<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl46";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sjl46ScreenVars sv = (Sjl46ScreenVars) fw.getVariables();
%>
<%
 		if (appVars.ind01.isOn()) {
            sv.annpmntfreq.setReverse(BaseScreenData.REVERSED);
            sv.annpmntfreq.setColor(BaseScreenData.RED);
        }
        if (appVars.ind02.isOn()) {
            sv.annpmntfreq.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind01.isOn()) {
            sv.annpmntfreq.setHighLight(BaseScreenData.BOLD);
        }
        %>        

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<table>
						<tr>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%></td>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
							</td>
							<td
								style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>
							</td>


						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment No. Status")%></label>
					<div class="input-group"
						style="width: 80px; text-align: left;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.annpmntstatus, true)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Owner"))%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%></td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ownername, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%>
							</td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary / Payee")%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.payee, true)%>
							</td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.payeename, true)%>
							</td>
						</tr>
					</table>					
				</div>
			</div>
		</div>
		<div class="row">
		
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Payment No.")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.pmntno) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.pmntno,
							(sv.pmntno.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.pmntno) instanceof DecimalData) {
				%>
					<%
					if (sv.pmntno.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.pmntno,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.pmntno,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 40px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
				%>
				</div>
			</div>
			
			<div class="col-md-3"> 
				    <div class="form-group"  style="width: 100px;">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Date")%></label>
								 <%=smartHF.getRichTextDateInput(fw, sv.pmntdateDisp,(sv.pmntdateDisp.getLength()))%>
							
				    </div>
				 </div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total Annuity Fund")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.totannufund) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.totannufund,
							(sv.totannufund.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.totannufund) instanceof DecimalData) {
				%>
					<%
					if (sv.totannufund.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>
				</div>
			</div>
		
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Already Paid")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.prmpaid) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.prmpaid,
							(sv.prmpaid.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.prmpaid) instanceof DecimalData) {
				%>
					<%
					if (sv.prmpaid.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.prmpaid,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.prmpaid,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
			
		</div>
		<div class="row">

			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Term")%></label>
				<div class="form-group"
						 style="width: 40px; text-align: right;">
						<%-- ILJ-624 --%>
						<%
					if (((BaseScreenData) sv.annupmnttrm) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmnttrm,
							(sv.annupmnttrm.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.annupmnttrm) instanceof DecimalData) {
				%>
					<%
					if (sv.annupmnttrm.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmnttrm,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmnttrm,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 40px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>
					</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Frequency")%></label>
				<div class="form-group" style="width: 100px; text-align: right;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "annpmntfreq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("annpmntfreq");
						optionValue = makeDropDownList(mappedItems, sv.annpmntfreq.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.annpmntfreq.getFormData()).toString().trim());
					%>
					<%
						if ((new Byte((sv.annpmntfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' style="min-width: 100px;">
						<%
								if (longValue != null) {
							%>

						<%=longValue%>

						<%
								}
							%>
					</div>
					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%
						if ("red".equals((sv.annpmntfreq).getColor())) {
					%>
					<div
						style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;text-align: right;width: 100px;">
						<%
							}
						%>
						<select name='annpmntfreq' type='list'
							<%if ((new Byte((sv.annpmntfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.annpmntfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.annpmntfreq).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
					<%
						longValue = null;
						optionValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annual Annuity Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null,sv.annualannamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				</div>
			</div>
			
			<div class="col-md-3">
				
					<label><%=resourceBundleHandler.gettingValueFromBundle("Withholding Tax Status")%></label>
					<div class="form-group"
						style="max-width: 145px; min-width: 100px; text-align: right;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.withholdtaxstus, true)%>
					</div>
				
			</div>
					
		</div>
		<div class="row">
	
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Required Expense Ratio")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.reqrexpenseratio) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.reqrexpenseratio,
							(sv.reqrexpenseratio.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.reqrexpenseratio) instanceof DecimalData) {
				%>
					<%
					if (sv.reqrexpenseratio.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.reqrexpenseratio,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 50px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.reqrexpenseratio,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 50px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Required Expense")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.reqrexpense) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.reqrexpense,
							(sv.reqrexpense.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.reqrexpense) instanceof DecimalData) {
				%>
					<%
					if (sv.reqrexpense.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.reqrexpense,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.reqrexpense,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
		
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Other Income")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.otherincome) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmntamt,
							(sv.otherincome.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.otherincome) instanceof DecimalData) {
				%>
					<%
					if (sv.otherincome.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.otherincome,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.otherincome,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Withholding Tax Rate")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.whhtr) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmntamt,
							(sv.whhtr.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.whhtr) instanceof DecimalData) {
				%>
					<%
					if (sv.whhtr.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.whhtr,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.whhtr,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
				%>

				</div>
			</div>
		
		</div>
		<div class="row">
		
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.annupmntamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmntamt,
							(sv.annupmntamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.annupmntamt) instanceof DecimalData) {
				%>
					<%
					if (sv.annupmntamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
					%>

				</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity w/interest")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.annuitywint) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annuitywint,
							(sv.annuitywint.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.annuitywint) instanceof DecimalData) {
				%>
					<%
					if (sv.annuitywint.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annuitywint,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annuitywint,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
					%>

				</div>
			</div>
		
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Withholding Tax Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.wholdtax) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annuitywint,
							(sv.wholdtax.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.wholdtax) instanceof DecimalData) {
				%>
					<%
					if (sv.wholdtax.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.wholdtax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.wholdtax,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
					%>

				</div>
			</div>
			
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Paid Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.paidamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.paidamt,
							(sv.paidamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.paidamt) instanceof DecimalData) {
				%>
					<%
					if (sv.paidamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.paidamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.paidamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} 
					%>

				</div>
			</div>
		
		</div>
		
	</div>
</div>

<%if (sv.Sjl46protectWritten.gt(0)) {%>
	<%Sjl46protect.clearClassString(sv);%>

	<%
	}
	%>


<%@ include file="/POLACommon2NEW.jsp"%>

