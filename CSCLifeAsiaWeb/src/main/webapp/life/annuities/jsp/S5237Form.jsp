

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5237";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S5237ScreenVars sv = (S5237ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract no      ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract owner   ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1 - Select,");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"2 - Add, 9 - Delete");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sel");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Payment No");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Payment Amount");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Payment Type");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%
	{
		if (appVars.ind01.isOn()) {
			generatedText7.setColor(BaseScreenData.BLUE);
		}
		if (appVars.ind03.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			generatedText8.setColor(BaseScreenData.BLUE);
		}
		if (appVars.ind03.isOn()) {
			sv.hselect.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}
%>
<!-- <style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style> -->
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<%
				if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no")%></label>
					<table>
					<tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>
						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>
						<%
							if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>

					</td>
					</tr></table>
				</div>
			</div>
			<%
				}
			%>


			<%
				if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>

			<div class="col-md-4 col-md-offset-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract owner")%></label>
					<table><tr>
					<td>
						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cownnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td>

						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ownername.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;max-width: 250px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
									formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr></table>
				</div>
			</div>
			<%
				}
			%>
		</div>
		<br />
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[5];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.select.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rgpynum.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
			} else {
				calculatedValue = (sv.rgpynum.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.pymt.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.pymt.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.rgpytype.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.rgpytype.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.rptldesc.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
			} else {
				calculatedValue = (sv.rptldesc.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;
		%>
		<%
			GeneralTable sfl = fw.getTable("s5237screensfl");
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<%
			S5237screensfl.set1stScreenRow(sfl, appVars, sv);
			int count = 1;
			while (S5237screensfl.hasMoreScreenRows(sfl)) {
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5237' width='100%' style="border-right: thin solid #dddddd !important;">
						<thead>
							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
								<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
							</tr>

						</thead>
						<tbody>
							<tr class="tableRowTag" id='<%="tablerow" + count%>'>
								<%
									if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag tableDataTagFixed"
									style="width:<%=tblColumnWidth[0]%>px;"
									<%if ((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><input
									type="radio" value='<%=(sv.select).getFormData()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp("s5237screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='s5237screensfl.select_R<%=count%>'
									id='s5237screensfl.select_R<%=count%>'
									onClick="selectedRow('s5237screensfl.select_R<%=count%>')"
									class="radio" /></td>
								<%
									} else {
								%>
								<td class="tableDataTag tableDataTagFixed"
									style="width:<%=tblColumnWidth[0]%>px;"></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"
									<%if ((sv.rgpynum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.rgpynum.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"
									<%if ((sv.pymt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.pymt).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.pymt,
 					COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 			if (!(sv.pymt).getFormData().toString().trim().equalsIgnoreCase("")) {
 				formatValue = formatValue(formatValue);
 			}
 %> <%=formatValue%> <%
 	longValue = null;
 			formatValue = null;
 %>




								</td>
								<%
									} else {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.rgpytype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[3]%>px;"
									<%if ((sv.rgpytype).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.rgpytype.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[3]%>px;"></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.rptldesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[4]%>px;"
									<%if ((sv.rptldesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.rptldesc.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[4]%>px;"></td>
								
								<%
									}
								%>

							</tr>
							<%
								count = count + 1;
									S5237screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<table>
			<tr>
				<td>
					<div class="sectionbutton">
						<a href="#" onClick="JavaScript:perFormOperation(1)"
							class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></a>
					</div>
				</td>
				<td>
					<div class="sectionbutton">
						<a href="#" onClick="JavaScript:perFormOperation(2)"
							class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
					</div>
				</td>
				<td>
					<div class="sectionbutton">
						<a href="#" onClick="JavaScript:perFormOperation(9)"
							class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-s5237').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			info:false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>
				<%
					if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("1 - Select,")%>
				</div> <%
 	}
 %> <br />&nbsp; &nbsp; &nbsp;
			</td>
			<td width='188'>
				<%
					if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("2 - Add, 9 - Delete")%>
				</div> <%
 	}
 %> <br /> <%
 	if ((new Byte((sv.hselect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (!((sv.hselect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.hselect.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.hselect.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>


			</td>
		</tr>
	</table>
</div>
