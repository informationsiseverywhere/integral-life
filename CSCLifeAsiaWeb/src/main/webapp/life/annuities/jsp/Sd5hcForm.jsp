

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5hc";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sd5hcScreenVars sv = (Sd5hcScreenVars) fw.getVariables();
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract No        ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner     ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured       ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life         ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Component Status   ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Commencement date  ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Paid to date    ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Plan Suffix        ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract currency  ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Original Benefit Amount Payable               ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Payable after Annuity Details Change  ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Early/Late Vesting Factor                     ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Percentage to Commute                         ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Lump Sum                                      ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Payable                               ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Use the Refresh key to calculate values");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bank Details          ");
%>
<%
	if (appVars.ind16.isOn()) {
		sv.ddind.setReverse(BaseScreenData.REVERSED);
		sv.ddind.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind16.isOn()) {
		sv.ddind.setHighLight(BaseScreenData.BOLD);
	}
	
	if (appVars.ind04.isOn()) {
		sv.rgpymop.setReverse(BaseScreenData.REVERSED);
		sv.rgpymop.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.rgpymop.setHighLight(BaseScreenData.BOLD);
	}
	
	if (appVars.ind23.isOn()) {
		sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
	} 
%>

<!-- <style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
	width: 80px !important;
}
</style> -->
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>

					<%
						}
					%>
					<table><tr>
					<td>


						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td>

						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div id="secondDiv" style="margin-left: 1px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td>

						<%
							if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="margin-left: 1px;max-width: 400px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td></tr>
					</table>
				</div>
			</div>
		</div>

		

		<!-- ILIFE-2431 Coding and Unit testing - Life Cross Browser - Sprint 1 D4: Task 5 ends-->


		<div class="row">
		
		<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<%
						formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 150px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%
						formatValue = formatValue((sv.premstatus.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("currency")%></label>
					<table><tr>
					<td>
						<%
							}
						%>
		<%
							if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.currcd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.currcd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
				
					</tr></table>
				</div>
			</div>
			
			<div class="col-md-2">
							<div class="col-md-2">
				<div class="form-group" >
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div class="input-group" >
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "register" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("register");
						optionValue = makeDropDownList(mappedItems, sv.register, 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.register.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 250px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div></div>
			</div>

			</div>
			
			
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>

					<%
						}
					%>

					<table><tr>
					<td>


						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td>

						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;max-width: 400px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>


					</td></tr>
					</table>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>

					<%
						}
					%>

					<table><tr>
					<td>


						<%
							if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="min-width: 78px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td>


						<%
							if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;min-width: 100px;max-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</td>
					</tr></table>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Commencement date")%></label>
					<div class="input-group">
						<%
							}
						%>




						<%
							if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 80px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to date")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div style="width: 80px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Bill to date")%></label>

					<%
						}
					%>
					<%
						if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div style="width: 80px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					<div class="input-group">
					<%
					qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
					qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS5);
				%>
			<input name='intRate' type='text'
					<%if ((sv.intRate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
					style="text-align: right" <%}%>
					value='<%=smartHF.getPicFormatted(qpsf, sv.intRate)%>'
					<%valueThis = smartHF.getPicFormatted(qpsf, sv.intRate);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
					title='<%=smartHF.getPicFormatted(qpsf, sv.intRate)%>' <%}%>
					size='<%=sv.intRate.getLength()%>'
					maxLength='<%=sv.intRate.getLength()%>' onFocus='doFocus(this)'
					onHelp='return fieldHelp(intrate)'
					onKeyUp='return checkMaxLength(this)'
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>'
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
					readonly="true" class="output_cell"><%-- 
					<%if ((new Byte((sv.intRate).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
					readonly="true" class="output_cell"
					<%} else if ((new Byte((sv.intRate).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%>
					class=' <%=(sv.intRate).getColor() == null ? "input_cell"
						: (sv.intRate).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
					<%}%>>
 --%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Calculate Frequency")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.intcalfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
					
					fieldItem = appVars.loadF4FieldsLong(new String[] { "intcalfreq" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("intcalfreq");
					optionValue = makeDropDownList(mappedItems, sv.intcalfreq.getFormData(), 2, resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.intcalfreq.getFormData()).toString().trim());
					if (longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue((sv.intcapfreq.getFormData()).toString());
					} else {
						formatValue = formatValue(longValue);
					}
					%>
					<div style="width: 80px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Capitalize frequency")%></label>

					<%
						}
					%>




					<%
						if ((new Byte((sv.intcapfreq).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
					fieldItem = appVars.loadF4FieldsLong(new String[] { "intcapfreq" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("intcapfreq");
					optionValue = makeDropDownList(mappedItems, sv.intcapfreq, 2, resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.intcapfreq.getFormData()).toString().trim());

					if (longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue((sv.intcapfreq.getFormData()).toString());
					} else {
						formatValue = formatValue(longValue);
					}
					%>
					<div style="width: 80px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Accumulated Amount ")%></label>
					<div class="input-group">
						<%
							}
						%>

						<%
							if ((new Byte((sv.accamnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.accamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.accamnt.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.accamnt.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div class="form-group" style="min-width:100px;max-width:167px;text-align: right;">	<%=smartHF.getHTMLVar(0, 0, fw, null, sv.accamnt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
					<%
						if ((new Byte((sv.rgpymop).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[] { "rgpymop" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("rgpymop");
							optionValue = makeDropDownList(mappedItems, sv.rgpymop.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
								/* Ticket #ILIFE-1802 start by akhan203  */
								formatValue = null;
								/*  Ticket #ILIFE-1802 ends  */
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.rgpymop).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 210px;">
						<%
							}
						%>

						<select name='rgpymop' type='list' style="width: 210px;"
							<%if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.rgpymop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.rgpymop).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						longValue = null;
								/* Ticket #ILIFE-1802 start by akhan203  */
								formatValue = null;
								/*  Ticket #ILIFE-1802 ends  */
					%>
					<%
						}
						}
					%>

				</div>
			</div>
		</div>


		


		<div style='visibility: hidden;'>
			<table>
				<tr style='height: 22px;'>
					<td width='251'></td>
				</tr>
			</table>

		</div>
	</div>
</div>



<div id="popups" class="panel panel-default pop" style="z-index: 1000;width: 710px;margin:50px 10%; display:none; position:absolute;display:none; top: 200px; border:1px solid lightgrey;">
	 <div class="panel-heading1">
	 <label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Withdrawal")%></label>
	 	</div>
	 	 <div class="panel-body"> 
	 		<div class="row">
	 		<div class="col-md-6">
	 		<label><%=resourceBundleHandler.gettingValueFromBundle("Confirm Annuity Withdrawal? (Y/N)")%></label>
	 		</div></div>
	 			<div class="row">
	 			<div class="col-md-4">
	 	 		 <input type="text" name="ansrepy2" id='ans' maxlength="1">  
	 			</div></div>
	

	  <div style="float:right;margin-right:5px;">
	 <button id="closebutton" style="background:#EA4742;"><%=resourceBundleHandler.gettingValueFromBundle("Close")%></button>
	 <button id="closecontbutton"style="background:#5CB85C;"><%=resourceBundleHandler.gettingValueFromBundle("Continue")%></button>
	 </div> 
	 <br><br>
	 </div></div>
	 
		<input type="hidden" name="ansrepy" id="mainans" maxlength="1">

	<style>
	 #ans{
	 height: 26px !important;
	 padding: 0 0 0 5px !important;
	 font-size: 13px !important;
	 border: 2px solid #ccc !important;
	 color: #000000 !important;
	 padding-right: 5px !important;
	 }
	 #closebutton,#closecontbutton, #closebutton2,#closecontbutton2{
	 color:white; padding:5px 10px; border:0px; border-radius:3px; font-weight:bold;
	 }
	 .panel-default > .panel-heading1 {
	     text-align: left;
	     font-weight: bold !important;
	  	 background-color: rgba(52, 77, 90, 0.78) !important;
	     color: #ffffff !important;
	     font-size: medium !important;
	     height:auto;
	 }
	 .panel-heading1 {
	     padding: 10px 15px;
	     border-bottom: 1px solid transparent;
	     border-top-left-radius: 3px;
	     border-top-right-radius: 3px;
	     
	 }
	 </style>

  <script type="text/javascript">
	// var flag= false;
	 var refcode1="<%=sv.refcode%>";
	 var msg = "<%=resourceBundleHandler.gettingValueFromBundle("Enter only Y/N")%>";
	 
	 function replicatevalueans(value)
	 {
		 $("#mainans").val(value);
	 }
 	$(document).ready(function() {	     
	 	 
	 	 	if (parent.frames["mainForm"].document.form1.action_key.value.toUpperCase() == "PFKEY0"){			
	 				
	 				if(refcode1 == "1" ){
	 					$('#popups').fadeIn(10);
	 				}
	 			} 	
	 	$('#closecontbutton').on('click',function(){
	 		if(refcode1 == "1"){	 			
	 			$('#popups').fadeOut(10);
	 			var here = $('#ans').val();
	 			
	 			if(here != "Y" && here !="N" && here!="")
	 				alert(msg);
	 			else{	
	 				
	 				if(here == "Y"){
	 					//flag=true;
	 				 	document.getElementById('ans').value = 'Y';  
	 					document.getElementById('mainans').value = 'Y';  
	 				 	doAction("PFKEY0");
	 				}
	 				else if(here == "N"){		 					
	 					document.getElementById('ans').value = 'N'; 					
	 					document.getElementById("mainans").value = 'N';	 					
	 					doAction("PFKEY0");
	 				}
	 				else  if(here == ""){
	 					document.getElementById('ans').value = 'X';  
	 					document.getElementById('mainans').value = 'X';  
	 					doAction("PFKEY0");
	 				}
	 			}
	 		/* 	$('#ans').val("") */
	 		}
	
	 		
	 	});

	 	$('#closebutton').on('click',function(){
	 		if(refcode1 == "1")
	 			$('#popups').fadeOut(10);
	 	});
 		
	 
	 });
	 </script>  
<script>
	$(document).ready(function() {
    	$('#dataTables-Sd5hc').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
			scrollX: true,
			
      	});
    });
</script>

<Div id='mainForm_OPTS' style='visibility:hidden;'>


								<li>
									<input name='ddind' id='ddind' type='hidden' value="<%=sv.ddind.getFormData()%>">
									<!-- text -->
									<%
									if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Bank Details")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Bank Details")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ddind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ddind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ddind'))"></i> 
			 						<%}%>
			 						</a>
								</li>

<%@ include file="/POLACommon2NEW.jsp"%>


