<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl15";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.annuities.screens.*"%>

<%
	Sjl15ScreenVars sv = (Sjl15ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.annpmntopt.setReverse(BaseScreenData.REVERSED);
			sv.annpmntopt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind40.isOn()) {
			sv.annpmntopt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.annpmntopt.setHighLight(BaseScreenData.BOLD);
		}		
		if (appVars.ind02.isOn()) {
			sv.annupmnttrm.setReverse(BaseScreenData.REVERSED);
			sv.annupmnttrm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind41.isOn()) {
			sv.annupmnttrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.annupmnttrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.partpmntamt.setReverse(BaseScreenData.REVERSED);
			sv.partpmntamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind42.isOn()) {
			sv.partpmntamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.partpmntamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.annpmntfreq.setReverse(BaseScreenData.REVERSED);
			sv.annpmntfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind43.isOn()) {
			sv.annpmntfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.annpmntfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.dfrrdtrm.setReverse(BaseScreenData.REVERSED);
			sv.dfrrdtrm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind44.isOn()) {
			sv.dfrrdtrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.dfrrdtrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.docaccptdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.docaccptdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind45.isOn()) {
			sv.docaccptdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.docaccptdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.frstpmntdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.frstpmntdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.frstpmntdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.frstpmntdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.payee.setReverse(BaseScreenData.REVERSED);
			sv.payee.setColor(BaseScreenData.RED);
		}
		if (appVars.ind47.isOn()) {
			sv.payee.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.payee.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.payeename.setReverse(BaseScreenData.REVERSED);
			sv.payeename.setColor(BaseScreenData.RED);
		}
		if (appVars.ind48.isOn()) {
			sv.payeename.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.payeename.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.pmntcurr.setReverse(BaseScreenData.REVERSED);
			sv.pmntcurr.setColor(BaseScreenData.RED);
		}
		if (appVars.ind49.isOn()) {
			sv.pmntcurr.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.pmntcurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.pmntcurrdesc.setReverse(BaseScreenData.REVERSED);
			sv.pmntcurrdesc.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.pmntcurrdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.pmntcurrdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.rgpymop.setReverse(BaseScreenData.REVERSED);
			sv.rgpymop.setColor(BaseScreenData.RED);
		}
		if (appVars.ind51.isOn()) {
			sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind12.isOn()) {
			sv.rgpymop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.payeeaccnum.setReverse(BaseScreenData.REVERSED);
			sv.payeeaccnum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind52.isOn()) {
			sv.payeeaccnum.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind13.isOn()) {
			sv.payeeaccnum.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%></td>
							<td style="max-width: 100px; min-width: 30px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
							</td>
							<td style="max-width: 105px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>   <!-- ILJ-770 -->
							</td>


						</tr>
					</table>
				</div>
			</div>			
			<div class="col-md-3">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Contract Date")%></label>					
					<div class="form-group" style="max-width: 100px; min-width: 100px; text-align: right;"><%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null)%>
			</div>				
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Branch Number")%></label>
					<div class="input-group"
						style="max-width: 100px; min-width: 60px; text-align: right;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.servbr, true)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Owner"))%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%></td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cownername, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td style="min-width: 80px"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%>
							</td>
							<td style="padding-left: 1px; min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Gender")%></label>
					<%-- <div class="input-group" style="width: 150px;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.cltsex, true)%>
					</div> --%>
					<div class="input-group">
						<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"cltsex"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("cltsex");
											longValue = (String) mappedItems.get((sv.cltsex.getFormData()).toString().trim());  
										%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style="width: 100px;">
							<%if(longValue != null){%>

							<%=longValue%>

							<%}%>
						</div>
						<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
		</div>
     <div class="row">
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Type")%></label>
					<%-- <table>
						<tr>
							<td style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.annuitype, true)%></td>
						</tr>
					</table> --%>
					<div class="input-group">
						<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"annuitype"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("annuitype");
											longValue = (String) mappedItems.get((sv.annuitype.getFormData()).toString().trim());  
										%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style="width: 140px;">
							<%if(longValue != null){%>

							<%=longValue%>

							<%}%>
						</div>
						<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Type")%></label>
					<%-- <table>
						<tr>
							<td style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, sv.beneftype, true)%></td>
						</tr>
					</table> --%>
					<div class="input-group">
						<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"beneftype"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("beneftype");
											longValue = (String) mappedItems.get((sv.beneftype.getFormData()).toString().trim());  
										%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style="width: 140px;">
							<%if(longValue != null){%>

							<%=longValue%>

							<%}%>
						</div>
						<%
		longValue = null;
		formatValue = null;
		%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Total Annuity Fund")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.totannufund) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.totannufund,
							(sv.totannufund.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.totannufund) instanceof DecimalData) {
				%>
					<%
					if (sv.totannufund.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.totannufund,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
		</div>
	<!-- </div>
</div> -->

<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Option")%></label>
			<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "annpmntopt" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("annpmntopt");
						optionValue = makeDropDownList(mappedItems, sv.annpmntopt.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.annpmntopt.getFormData()).toString().trim());
					%>
			<%
						if ((new Byte((sv.annpmntopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
			<div class='output_cell' style="min-width: 100px;">
				<%
								if (longValue != null) {
							%>

				<%=longValue%>

				<%
								}
							%>
			</div>
			<%
						longValue = null;
					%>
			<%
						} else {
					%>
			<%
						if ("red".equals((sv.annpmntopt).getColor())) {
					%>
			<div
				style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
				<%
							}
						%>
				<select name='annpmntopt' type='list'
					<%if ((new Byte((sv.annpmntopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.annpmntopt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<div class='output_cell' style="width: 100px;">
						<%=optionValue%>
					</div>
				</select>
				<%
							if ("red".equals((sv.annpmntopt).getColor())) {
						%>
			</div>
			<%
						}
					%>
			<%
						}
					%>
			<%
						longValue = null;
						optionValue = null;
					%>
		</div>
	</div>
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Term")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
								qpsf = fw.getFieldXMLDef((sv.annupmnttrm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

					<input name='annupmnttrm' type='text'
						<%if ((sv.annupmnttrm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 70px" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.annupmnttrm)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.annupmnttrm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.annupmnttrm)%>' <%}%>
						size='<%=sv.annupmnttrm.getLength()%>'
						maxLength='<%=sv.annupmnttrm.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(annupmnttrm)'
						onKeyUp='return checkMaxLength(this)' style="width:80px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.annupmnttrm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell" style="width:80px;"
						<%} else if ((new Byte((sv.annupmnttrm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="width:80px;" <%} else {%>
						class=' <%=(sv.annupmnttrm).getColor() == null ? "input_cell"
						: (sv.annupmnttrm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
						<%}%>>

				</div>
			</div>
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Partial Payment Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.partpmntamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.partpmntamt,
							(sv.partpmntamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.partpmntamt) instanceof DecimalData) {
				%>
					<%
					if (sv.partpmntamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.partpmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.partpmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
	<div class="col-md-3">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Frequency")%></label>
		<div class="form-group" style="width: 150px;">
			<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "annpmntfreq" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("annpmntfreq");
						optionValue = makeDropDownList(mappedItems, sv.annpmntfreq.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.annpmntfreq.getFormData()).toString().trim());
					%>
			<%
						if ((new Byte((sv.annpmntfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
			<div class='output_cell' style="min-width: 150px;">
				<%
								if (longValue != null) {
							%>

				<%=longValue%>

				<%
								}
							%>
			</div>
			<%
						longValue = null;
					%>
			<%
						} else {
					%>
			<%
						if ("red".equals((sv.annpmntfreq).getColor())) {
					%>
			<div
				style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
				<%
							}
						%>
				<select name='annpmntfreq' type='list'
					<%if ((new Byte((sv.annpmntfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.annpmntfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<%
							if ("red".equals((sv.annpmntfreq).getColor())) {
						%>
			</div>
			<%
						}
					%>
			<%
						}
					%>
			<%
						longValue = null;
						optionValue = null;
					%>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Deferred Term")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
								qpsf = fw.getFieldXMLDef((sv.dfrrdtrm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

					<input name='dfrrdtrm' type='text'
						<%if ((sv.dfrrdtrm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
						style="text-align: right; width: 70px" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf, sv.dfrrdtrm)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.dfrrdtrm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.dfrrdtrm)%>' <%}%>
						size='<%=sv.dfrrdtrm.getLength()%>'
						maxLength='<%=sv.dfrrdtrm.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(dfrrdtrm)'
						onKeyUp='return checkMaxLength(this)' style="width:80px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.dfrrdtrm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell" style="width:80px;"
						<%} else if ((new Byte((sv.dfrrdtrm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="width:80px;" <%} else {%>
						class=' <%=(sv.dfrrdtrm).getColor() == null ? "input_cell"
						: (sv.dfrrdtrm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
						<%}%>>

				</div>
			</div>
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Deferred Amount")%></label>
		<div class="form-group"
			style="max-width: 145px; min-width: 100px; text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null,sv.dfrrdamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
		</div>
	</div>
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Amount")%></label>
				<div class="form-group"
					style="max-width: 145px; min-width: 100px; text-align: right;">
					<%
					if (((BaseScreenData) sv.annupmntamt) instanceof StringBase) {
				%>
					<%=smartHF.getRichText(0, 0, fw, sv.annupmntamt,
							(sv.annupmntamt.getLength() + 1), null).replace(
							"absolute", "relative")%>
					<%
					} else if (((BaseScreenData) sv.annupmntamt) instanceof DecimalData) {
				%>
					<%
					if (sv.annupmntamt.equals(0)) {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
					} else {
				%>
					<%=smartHF.getHTMLVar(0,0,fw,sv.annupmntamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
					}
				%>
					<%
					} else {
				%>
					<%
					}
				%>

				</div>
			</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Fund Set Up Date")%></label>			
			<div class="form-group" style="max-width:100px;min-width: 100px; text-align: right;"><%=smartHF.getRichText(0, 0, fw, sv.fundsetupdtDisp,(sv.fundsetupdtDisp.getLength()),null)%>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Payment Start Date")%></label>			
			<div class="form-group" style="max-width: 100px; min-width: 100px; text-align: right;"><%=smartHF.getRichText(0, 0, fw, sv.pmntstartdtDisp,(sv.pmntstartdtDisp.getLength()),null)%>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Document Acceptance Date")%></label>
			<%
								if((new Byte((sv.docaccptdtDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
			<%=smartHF.getRichTextDateInput(fw, sv.docaccptdtDisp,(sv.docaccptdtDisp.getLength()))%>
			<%}else{%>
			<div class="input-group date form_date col-md-12" style="max-width: 140px; min-width: 100px; text-align: right;"  data-date=""
				data-date-format="dd/mm/yyyy" data-link-field="docaccptdtDisp"
				data-link-format="dd/mm/yyyy">
				<%=smartHF.getRichTextDateInput(fw, sv.docaccptdtDisp,(sv.docaccptdtDisp.getLength()))%>
				<span class="input-group-addon"><span
					class="glyphicon glyphicon-calendar"></span></span>
			</div>
			<%}%>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Annuity Payment Status")%></label>
			<div class="input-group" style="max-width: 145px; min-width: 100px; text-align: right;">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.annpmntstatus, true)%>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%></label>
			<%
				if ((new Byte((sv.frstpmntdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {
			%>	
			<%=smartHF.getRichTextDateInput(fw, sv.frstpmntdtDisp,(sv.frstpmntdtDisp.getLength()))%>
			<%}else{%>
			<div class="input-group date form_date col-md-12" data-date=""
				data-date-format="dd/mm/yyyy" data-link-field="frstpmntdtDisp"
				data-link-format="dd/mm/yyyy">
				<%=smartHF.getRichTextDateInput(fw, sv.frstpmntdtDisp,(sv.frstpmntdtDisp.getLength()))%>
				<span class="input-group-addon"><span
					class="glyphicon glyphicon-calendar"></span></span>
			</div>
			<%}%>		
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%></label>
			<div class="form-group" style="max-width: 100px; min-width: 100px; text-align: right;"><%=smartHF.getRichText(0, 0, fw, sv.anniversdtDisp,(sv.anniversdtDisp.getLength()),null)%>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%></label>
			<div class="form-group" style="max-width: 100px; min-width: 100px; text-align: right;"><%=smartHF.getRichText(0, 0, fw, sv.finalpmntdtDisp,(sv.finalpmntdtDisp.getLength()),null)%>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div>
			<label><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary / Payee")%></label>
			<table>
				<tr>
					<td>
						<div class="form-group">
							<%
				          if ((new Byte((sv.payee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>
							<div style="margin-right: 2px">
								<%=smartHF.getHTMLVarExt(fw, sv.payee)%></div>
							<%
							} else {
						%>
							<div class="input-group" style="width: 125px;">
								<%=smartHF.getRichTextInputFieldLookup(fw, sv.payee)%>
								<span class="input-group-btn">
									<button class="btn btn-info" type="button"
										onClick="doFocus(document.getElementById('payee')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</div>
							<%
				         }
				        %>
						</div>
					</td>
					<td>
						<%
								longValue = null;
								formatValue = null;
								%>
						<div
							class='<%= (sv.payeename.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
							style='margin-left: -1px; width: 133px;'>
							<%
								if(!((sv.payeename.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payeename.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payeename.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
								longValue = null;
								formatValue = null;
								%>
					</td>
				</tr>
			</table>
		</div>
	</div>
<div class="col-md-3">
		<div>
			<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%></label>
			<table>
				<tr>
					<td>
						<div class="form-group">
							<%
				          if ((new Byte((sv.pmntcurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>
							<div style="margin-right: 2px">
								<%=smartHF.getHTMLVarExt(fw, sv.pmntcurr)%></div>
							<%
							} else {
						%>
							<div class="input-group" style="width: 125px;">
								<%=smartHF.getRichTextInputFieldLookup(fw, sv.pmntcurr)%>
								<span class="input-group-btn">
									<button class="btn btn-info" type="button"
										onClick="doFocus(document.getElementById('pmntcurr')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
							</div>
							<%
				         }
				        %>
						</div>
					</td>
					<td>
						<%
								longValue = null;
								formatValue = null;
								%>
						<div
							class='<%= (sv.pmntcurrdesc.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>'
							style='margin-left: -1px; width: 133px;'>
							<%
								if(!((sv.pmntcurrdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pmntcurrdesc.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pmntcurrdesc.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
								longValue = null;
								formatValue = null;
								%>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
			<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpymop"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rgpymop");
						optionValue = makeDropDownList( mappedItems , sv.rgpymop.getFormData(),2,resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());
						%>
			<%
							if((new Byte((sv.rgpymop).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
						%>
			<div class='output_cell' style="width: 170px;">
				<%=longValue%>
			</div>
			<%
						longValue = null;
						%>
			<% }else {%>
			<% if("red".equals((sv.rgpymop).getColor())){
											%>
			<div
				style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572;">
				<%}%>
				<select name='rgpymop' type='list'
					<%
										if((new Byte((sv.rgpymop).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){%>
					readonly="true" disabled class="output_cell"
					<%}else if((new Byte((sv.rgpymop).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){	%>
					class="bold_cell" <%}else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<% if("red".equals((sv.rgpymop).getColor())){%>
			</div>
			<%}%>
			<%}%>
			<%
						longValue = null;
						optionValue = null;
						%>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Payee Account Number")%>
		</label>
		<table>
			<tr>
				<td style="width: 186px;">
					<div class="form-group">
								<%
				          if ((new Byte((sv.payeeaccnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>
								<div style="margin-right: 2px">
									<%=smartHF.getHTMLVarExt(fw, sv.payeeaccnum)%></div>
								<%
							} else {
						%>
								<div class="input-group" style="width: 125px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.payeeaccnum)%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('payeeaccnum')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
								</div>
								<%
				         }
				        %>
							</div>
						</td>
			<%-- 	<td style="width: 120px;"><%=smartHF.getHTMLVarReadOnly(fw, sv.bankaccdsc)%></td> --%>
			</tr>
		</table>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Code / Bank Kana Name"))%></label>
			<table>
				<tr>
					<td style="min-width: 30px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bankCd, true)%></td>
					<td style="padding-left: 1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.kanabank, true)%>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="col-md-4">			<!-- ILJ-770 -->
		<div class="form-group">
			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch Code / Branch Kana Name"))%></label>
			<table>
				<tr>
					<td style="min-width: 30px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.branchCd, true)%></td>
					<td style="padding-left: 1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.kanabranch, true)%>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Account Holder Name"))%></label>
			<table>
				<tr>
					<td style="min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bankaccdsc, true)%></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Account Type"))%></label>
			<table>
				<tr>
					<td style="min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkactyp, true)%></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div id='mainForm_OPTS' style='visibility:hidden;'>
<%=smartHF.getMenuLink(sv.annpayplan, resourceBundleHandler.gettingValueFromBundle("Annuity Payment Plan"),true)%>
</div>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>