

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6769";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6769ScreenVars sv = (S6769ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"BMI");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Class");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Underwriting");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rule");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factor");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<div class="panel panel-default">
        <div class="panel-body">
                    <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	                        <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	                      <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                       <div class="input-group">
	                    <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  							</div>
	                    </div>
	                  </div>
	             </div>
	             
	             
	  <div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
			</div>
			</br>
		           
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("BMI")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Class")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Underwriting")%></label>
	                    </div>
	                  </div>
	             </div>
	             
	               <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                      <%	
			qpsf = fw.getFieldXMLDef((sv.bmi01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi01' 
type='text'

<%if((sv.bmi01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi01) %>'
	 <%}%>

size='<%= sv.bmi01.getLength()%>'
maxLength='<%= sv.bmi01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi01).getColor()== null  ? 
			"input_cell" :  (sv.bmi01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass01' 
type='text'

<%if((sv.bmiclass01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:100px;"<% }%>

<%

		formatValue = (sv.bmiclass01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass01.getLength()%>'
maxLength='<%= sv.bmiclass01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass01).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule01");
	optionValue = makeDropDownList( mappedItems , sv.undwrule01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi02' 
type='text'

<%if((sv.bmi02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi02) %>'
	 <%}%>

size='<%= sv.bmi02.getLength()%>'
maxLength='<%= sv.bmi02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi02).getColor()== null  ? 
			"input_cell" :  (sv.bmi02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                       
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass02' 
type='text'

<%if((sv.bmiclass02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>

<%

		formatValue = (sv.bmiclass02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass02.getLength()%>'
maxLength='<%= sv.bmiclass02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass02).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule02");
	optionValue = makeDropDownList( mappedItems , sv.undwrule02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi03' 
type='text'

<%if((sv.bmi03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:left; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi03) %>'
	 <%}%>

size='<%= sv.bmi03.getLength()%>'
maxLength='<%= sv.bmi03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi03).getColor()== null  ? 
			"input_cell" :  (sv.bmi03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass03' 
type='text'

<%if((sv.bmiclass03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left"<% }%>

<%

		formatValue = (sv.bmiclass03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass03.getLength()%>'
maxLength='<%= sv.bmiclass03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass03).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule03");
	optionValue = makeDropDownList( mappedItems , sv.undwrule03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                    </div>
	                  </div>
	             </div>
	             
	             
	               <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                     <%	
			qpsf = fw.getFieldXMLDef((sv.bmi04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi04' 
type='text'

<%if((sv.bmi04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:left; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi04) %>'
	 <%}%>

size='<%= sv.bmi04.getLength()%>'
maxLength='<%= sv.bmi04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi04).getColor()== null  ? 
			"input_cell" :  (sv.bmi04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass04' 
type='text'

<%if((sv.bmiclass04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass04.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass04.getLength()%>'
maxLength='<%= sv.bmiclass04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass04)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass04).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule04");
	optionValue = makeDropDownList( mappedItems , sv.undwrule04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                    </div>
	                  </div>
	             </div>
	             
	             
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                      <%	
			qpsf = fw.getFieldXMLDef((sv.bmi05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi05' 
type='text'

<%if((sv.bmi05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi05) %>'
	 <%}%>

size='<%= sv.bmi05.getLength()%>'
maxLength='<%= sv.bmi05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi05).getColor()== null  ? 
			"input_cell" :  (sv.bmi05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass05' 
type='text'

<%if((sv.bmiclass05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass05.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass05.getLength()%>'
maxLength='<%= sv.bmiclass05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass05)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass05).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule05");
	optionValue = makeDropDownList( mappedItems , sv.undwrule05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi06' 
type='text'

<%if((sv.bmi06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi06) %>'
	 <%}%>

size='<%= sv.bmi06.getLength()%>'
maxLength='<%= sv.bmi06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi06).getColor()== null  ? 
			"input_cell" :  (sv.bmi06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass06' 
type='text'

<%if((sv.bmiclass06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass06.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass06.getLength()%>'
maxLength='<%= sv.bmiclass06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass06)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass06).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule06");
	optionValue = makeDropDownList( mappedItems , sv.undwrule06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	                    </div>
	                  </div>
	             </div>
	             
	             
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                      <input name='bmiclass07' 
type='text'

<%if((sv.bmiclass07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:100px;"<% }%>

<%

		formatValue = (sv.bmiclass07.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass07.getLength()%>'
maxLength='<%= sv.bmiclass07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass07)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:100px;"

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass07).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:100px;"
 
<%
	} 
%>
>
	                       
	                       
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass07' 
type='text'

<%if((sv.bmiclass07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass07.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass07.getLength()%>'
maxLength='<%= sv.bmiclass07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass07)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass07).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule07");
	optionValue = makeDropDownList( mappedItems , sv.undwrule07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	             
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi08' 
type='text'

<%if((sv.bmi08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi08) %>'
	 <%}%>

size='<%= sv.bmi08.getLength()%>'
maxLength='<%= sv.bmi08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi08).getColor()== null  ? 
			"input_cell" :  (sv.bmi08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                       
	                       
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass08' 
type='text'

<%if((sv.bmiclass08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass08.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass08.getLength()%>'
maxLength='<%= sv.bmiclass08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass08)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass08).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule08");
	optionValue = makeDropDownList( mappedItems , sv.undwrule08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi09' 
type='text'

<%if((sv.bmi09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi09) %>'
	 <%}%>

size='<%= sv.bmi09.getLength()%>'
maxLength='<%= sv.bmi09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi09).getColor()== null  ? 
			"input_cell" :  (sv.bmi09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass09' 
type='text'

<%if((sv.bmiclass09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass09.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass09.getLength()%>'
maxLength='<%= sv.bmiclass09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass09)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass09).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule09");
	optionValue = makeDropDownList( mappedItems , sv.undwrule09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

	                       
	                    </div>
	                  </div>
	             </div>
	             
	             
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.bmi10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
			
	%>

<input name='bmi10' 
type='text'

<%if((sv.bmi10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:100px;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.bmi10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.bmi10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.bmi10) %>'
	 <%}%>

size='<%= sv.bmi10.getLength()%>'
maxLength='<%= sv.bmi10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmi10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.bmi10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bmi10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmi10).getColor()== null  ? 
			"input_cell" :  (sv.bmi10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                       
	                       
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                     <input name='bmiclass10' 
type='text'

<%if((sv.bmiclass10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.bmiclass10.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmiclass10.getLength()%>'
maxLength='<%= sv.bmiclass10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmiclass10)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmiclass10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmiclass10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmiclass10).getColor()== null  ? 
			"input_cell" :  (sv.bmiclass10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule10");
	optionValue = makeDropDownList( mappedItems , sv.undwrule10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='undwrule10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.undwrule10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
     
	                    </div>
	                  </div>
	             </div>
	             
	           <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                      
	                    </div>
	                  </div>
	               </div>   
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>
	                    </div>
	                  </div>
	               </div>
	                  <div class="row">
	                   <div class="col-md-3">
	                     <div class="form-group">
	                      <input name='bmifact' 
type='text'

<%if((sv.bmifact).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:100px;"<% }%>

<%

		formatValue = (sv.bmifact.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.bmifact.getLength()%>'
maxLength='<%= sv.bmifact.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(bmifact)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.bmifact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:100px;"
<%
	}else if((new Byte((sv.bmifact).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bmifact).getColor()== null  ? 
			"input_cell" :  (sv.bmifact).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
	                     </div>
	                    </div>
	                   </div>
	                  
	            
	   </div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>

