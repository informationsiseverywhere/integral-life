<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6642";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6642ScreenVars sv = (S6642ScreenVars) fw.getVariables();%>

<%if (sv.S6642screenWritten.gt(0)) {%>
	<%S6642screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversionary Bonus");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Life Calculation Basis    ");%>
	<%sv.slsumass.setClassString("");%>
<%	sv.slsumass.appendClassString("string_fld");
	sv.slsumass.appendClassString("input_txt");
	sv.slsumass.appendClassString("highlight");
%>
	<%sv.slrevbns.setClassString("");%>
<%	sv.slrevbns.appendClassString("string_fld");
	sv.slrevbns.appendClassString("input_txt");
	sv.slrevbns.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life Calculation Basis     ");%>
	<%sv.jlsumass.setClassString("");%>
<%	sv.jlsumass.appendClassString("string_fld");
	sv.jlsumass.appendClassString("input_txt");
	sv.jlsumass.appendClassString("highlight");
%>
	<%sv.jlrevbns.setClassString("");%>
<%	sv.jlrevbns.appendClassString("string_fld");
	sv.jlrevbns.appendClassString("input_txt");
	sv.jlrevbns.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life Equal Age Basis       ");%>
	<%sv.jleqage.setClassString("");%>
<%	sv.jleqage.appendClassString("string_fld");
	sv.jleqage.appendClassString("input_txt");
	sv.jleqage.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Female Age Adjustment            ");%>
	<%sv.fmageadj.setClassString("");%>
<%	sv.fmageadj.appendClassString("num_fld");
	sv.fmageadj.appendClassString("input_txt");
	sv.fmageadj.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

<%}%>

<%if (sv.S6642protectWritten.gt(0)) {%>
	<%S6642protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	                        <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	                      <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                       <div class="input-group">
	                    <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  							</div>
	                    </div>
	                  </div>
	             </div>
	             	       <br><br>                    
	              <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-3">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Reversionary Bonus")%></label>
	                    </div>
	                  </div>
	             </div>
	             
	             
	             <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Single Life Calculation Basis")%></label>
	                    </div>
	                  </div>
	                  <div class="col-md-1"></div>
	                  <div class="col-md-3">
                        <div class="input-group" style="width:65px;">
							<%=smartHF.getHTMLVarExt(fw, sv.slsumass)%>
							<%=smartHF.getHTMLF4NSVarExt(fw, sv.slsumass)%>
							</div>
					  </div>
					  <div class="col-md-1"></div>
					   <div class="col-md-3">
                        <div class="input-group">
							<%=smartHF.getHTMLVarExt(fw, sv.slrevbns)%>
							<%=smartHF.getHTMLF4NSVarExt(fw, sv.slrevbns)%>
							</div>
					  </div>
	                 
	             </div>
	             </br>
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life Calculation Basis")%></label>
	                    </div>
	                  </div>
	                  <div class="col-md-1"></div>
	                      <div class="col-md-3">
                        <div class="input-group">
							<%=smartHF.getHTMLVarExt(fw, sv.jlsumass)%>
							<%=smartHF.getHTMLF4NSVarExt(fw, sv.jlsumass)%>
							</div>
					  </div>
	                  <div class="col-md-1"></div>
	                   <div class="col-md-3">
                        <div class="input-group">
							<%=smartHF.getHTMLVarExt(fw, sv.jlrevbns)%>
							<%=smartHF.getHTMLF4NSVarExt(fw, sv.slrevbns)%>
							</div>
					  </div>
	             </div>
	               
	               </br>
	               
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life Equal Age Basis")%></label>
	                    </div>
	                  </div>
	                  <div class="col-md-1"></div>
	                     <div class="col-md-3">
                        <div class="input-group">
							<%=smartHF.getHTMLVarExt(fw, sv.jleqage)%>
							<%=smartHF.getHTMLF4NSVarExt(fw, sv.jleqage)%>
							</div>
					  </div>
	                               
	             </div>
	              </br> 
	              <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Female Age Adjustment")%></label>
	                    </div>
	                  </div>
	                  <div class="col-md-1"></div>
	                   <div class="col-md-2">
	                    <div class="form-group">
	                <%if(((BaseScreenData)sv.fmageadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fmageadj,( sv.fmageadj.getLength()+1),null).replace("absolute","relative").replace("size='50'","size='30'")%>
<%}else if (((BaseScreenData)sv.fmageadj) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fmageadj, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>

<%}%>                  
	                    </div>
	                  </div>
	                               
	             </div>
	             
	             
	       </div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>
