<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5017";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S5017ScreenVars sv = (S5017ScreenVars) fw.getVariables();%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Please enter item to be calculated   ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Please enter long desc");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.itemToCreate.setReverse(BaseScreenData.REVERSED);
			sv.itemToCreate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.itemToCreate.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
	                       
	                    
	                       <%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	                   </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month / Year")%></label>
	                      <table><tr>
	                      <td>
	                      <%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</td>
<td>




<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="margin-left: 1px;max-width: 100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="margin-left: 1px;max-width: 100px;min-width: 100px" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 						</td>
 						</tr>
 						</table>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
	                        <%if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	                    </div>
	                  </div>
	             </div>	        
	                  	 <br>                        
	              <div class="row">
                      <div class="col-md-4">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Please enter item to be calculated")%></label>                       
                     </div>
                   </div>
                    <div class="row">
                      <div class="col-md-3">
                         				<%if ((new Byte((sv.itemToCreate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='itemToCreate' 
type='text'

<%if((sv.itemToCreate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:left; width:150px;"<% }%>

<%

		formatValue = (sv.itemToCreate.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.itemToCreate.getLength()%>'
maxLength='<%= sv.itemToCreate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(itemToCreate)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.itemToCreate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:150px;"
<%
	}else if((new Byte((sv.itemToCreate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:150px;"

<%
	}else { 
%>

	class = ' <%=(sv.itemToCreate).getColor()== null  ? 
			"input_cell" :  (sv.itemToCreate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:150px;"
 
<%
	} 
%>
>
<%}%>                           
                      </div>
                   </div>
                   <br>
                    <div class="row">
                      <div class="col-md-4">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Please enter long desc")%></label>                       
                     </div>
                   </div>
                    <div class="row">
                      <div class="col-md-3">
                            		<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='longdesc' 
type='text'

<%if((sv.longdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.longdesc.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.longdesc.getLength()%>'
maxLength='<%= sv.longdesc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(longdesc)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.longdesc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:150px;"
<%
	}else if((new Byte((sv.longdesc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:150px;"

<%
	}else { 
%>

	class = ' <%=(sv.longdesc).getColor()== null  ? 
			"input_cell" :  (sv.longdesc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:150px;"
 
<%
	} 
%>
>
<%}%>
                       
                     </div>
                   </div>
                </div>  
               <div style='visibility:hidden;'>
                  <div class="row">
                      <div class="col-md-3">
                              <label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year")%>
</div>
<%}%>
                              </label>                       
                     </div>
                   </div>


            </div>   
	     </div>
<%@ include file="/POLACommon2NEW.jsp"%>

