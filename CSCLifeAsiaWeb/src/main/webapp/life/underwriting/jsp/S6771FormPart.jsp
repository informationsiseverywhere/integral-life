<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6771FormPart";%>

<%@page import="com.csc.life.underwriting.screens.S6771ScreenVars"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.quipoz.framework.util.AppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>

<%-- Start preparing data --%>
<%
String longValue = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();


LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), lang);

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%S6771ScreenVars sv = (S6771ScreenVars) fw.getVariables();%>

 <div class="row">
 					<div class="col-md-2" style="max-width: 110px;"  >
   	                 <div class="input-group" style="max-width: 110px;">            
	                    
	                       <%	
	longValue = sv.questidf01.getFormData();  
%>

<% 
	if((new Byte((sv.questidf01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf01' 
id='questidf01'
type='text' 
value='<%=sv.questidf01.getFormData()%>' 
maxLength='<%=sv.questidf01.getLength()%>' 
size='<%=sv.questidf01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf01)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
                
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questidf01).getColor()== null  ? 
"input_cell" :  (sv.questidf01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
						
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                     <%	
	longValue = sv.questst01.getFormData();  
%>

<% 
	if((new Byte((sv.questst01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst01' 
id='questst01'
type='text' 
value='<%=sv.questst01.getFormData()%>' 
maxLength='<%=sv.questst01.getLength()%>' 
size='<%=sv.questst01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst01)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst01).getColor()== null  ? 
"input_cell" :  (sv.questst01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst01')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                     
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questidf02.getFormData();  
%>

<% 
	if((new Byte((sv.questidf02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf02' 
id='questidf02'
type='text' 
value='<%=sv.questidf02.getFormData()%>' 
maxLength='<%=sv.questidf02.getLength()%>' 
size='<%=sv.questidf02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf02).getColor()== null  ? 
"input_cell" :  (sv.questidf02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>  
	                    </div>
	                  
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questst02.getFormData();  
%>

<% 
	if((new Byte((sv.questst02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst02' 
id='questst02'
type='text' 
value='<%=sv.questst02.getFormData()%>' 
maxLength='<%=sv.questst02.getLength()%>' 
size='<%=sv.questst02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst02)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst02).getColor()== null  ? 
"input_cell" :  (sv.questst02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst02')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                     </div>  
	                    </div>
	                 
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questidf03.getFormData();  
%>

<% 
	if((new Byte((sv.questidf03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf03' 
id='questidf03'
type='text' 
value='<%=sv.questidf03.getFormData()%>' 
maxLength='<%=sv.questidf03.getLength()%>' 
size='<%=sv.questidf03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf03)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf03).getColor()== null  ? 
"input_cell" :  (sv.questidf03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       </div>
	                    </div>
	                
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questst03.getFormData();  
%>

<% 
	if((new Byte((sv.questst03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst03' 
id='questst03'
type='text' 
value='<%=sv.questst03.getFormData()%>' 
maxLength='<%=sv.questst03.getLength()%>' 
size='<%=sv.questst03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst03)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst03).getColor()== null  ? 
"input_cell" :  (sv.questst03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst03')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
						  </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questidf04.getFormData();  
%>

<% 
	if((new Byte((sv.questidf04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf04' 
id='questidf04'
type='text' 
value='<%=sv.questidf04.getFormData()%>' 
maxLength='<%=sv.questidf04.getLength()%>' 
size='<%=sv.questidf04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf04)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questidf04).getColor()== null  ? 
"input_cell" :  (sv.questidf04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div>  
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questst04.getFormData();  
%>

<% 
	if((new Byte((sv.questst04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst04' 
id='questst04'
type='text' 
value='<%=sv.questst04.getFormData()%>' 
maxLength='<%=sv.questst04.getLength()%>' 
size='<%=sv.questst04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst04)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst04).getColor()== null  ? 
"input_cell" :  (sv.questst04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst04')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                  
	       </div>     
	</br>       
<div class="row">
                      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                      <%	
	longValue = sv.questidf05.getFormData();  
%>

<% 
	if((new Byte((sv.questidf05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf05' 
id='questidf05'
type='text' 
value='<%=sv.questidf05.getFormData()%>' 
maxLength='<%=sv.questidf05.getLength()%>' 
size='<%=sv.questidf05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf05)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf05).getColor()== null  ? 
"input_cell" :  (sv.questidf05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div> 
	                    </div>
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                      <%	
	longValue = sv.questst05.getFormData();  
%>

<% 
	if((new Byte((sv.questst05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst05' 
id='questst05'
type='text' 
value='<%=sv.questst05.getFormData()%>' 
maxLength='<%=sv.questst05.getLength()%>' 
size='<%=sv.questst05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst05)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst05).getColor()== null  ? 
"input_cell" :  (sv.questst05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst05')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

							</div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                      <%	
	longValue = sv.questidf06.getFormData();  
%>

<% 
	if((new Byte((sv.questidf06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf06' 
id='questidf06'
type='text' 
value='<%=sv.questidf06.getFormData()%>' 
maxLength='<%=sv.questidf06.getLength()%>' 
size='<%=sv.questidf06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf06)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf06).getColor()== null  ? 
"input_cell" :  (sv.questidf06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	             
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                      <%	
	longValue = sv.questst06.getFormData();  
%>

<% 
	if((new Byte((sv.questst06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst06' 
id='questst06'
type='text' 
value='<%=sv.questst06.getFormData()%>' 
maxLength='<%=sv.questst06.getLength()%>' 
size='<%=sv.questst06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst06)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst06).getColor()== null  ? 
"input_cell" :  (sv.questst06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst06')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                 
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questidf07.getFormData();  
%>

<% 
	if((new Byte((sv.questidf07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf07' 
id='questidf07'
type='text' 
value='<%=sv.questidf07.getFormData()%>' 
maxLength='<%=sv.questidf07.getLength()%>' 
size='<%=sv.questidf07.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf07)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questidf07).getColor()== null  ? 
"input_cell" :  (sv.questidf07).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       
	                    </div>
	                  </div>
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questst07.getFormData();  
%>

<% 
	if((new Byte((sv.questst07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst07' 
id='questst07'
type='text' 
value='<%=sv.questst07.getFormData()%>' 
maxLength='<%=sv.questst07.getLength()%>' 
size='<%=sv.questst07.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst07)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst07).getColor()== null  ? 
"input_cell" :  (sv.questst07).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst07')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                        </div>
	                    </div>
	                 
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                       <%	
	longValue = sv.questidf08.getFormData();  
%>

<% 
	if((new Byte((sv.questidf08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf08'
id='questidf08' 
type='text' 
value='<%=sv.questidf08.getFormData()%>' 
maxLength='<%=sv.questidf08.getLength()%>' 
size='<%=sv.questidf08.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf08)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf08).getColor()== null  ? 
"input_cell" :  (sv.questidf08).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div> 
	                    </div>
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;"> 
	                <%	
	longValue = sv.questst08.getFormData();  
%>

<% 
	if((new Byte((sv.questst08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst08' 
id='questst08' 
type='text' 
value='<%=sv.questst08.getFormData()%>' 
maxLength='<%=sv.questst08.getLength()%>' 
size='<%=sv.questst08.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst08)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 



<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>


<%
	}else { 
%>

class = ' <%=(sv.questst08).getColor()== null  ? 
"input_cell" :  (sv.questst08).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst08')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>


<%}longValue = null;} %>
	                	</div>
	                  </div>          
	       </div>
	 </br>      
<div class="row">
                      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf09.getFormData();  
%>

<% 
	if((new Byte((sv.questidf09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf09' 
id='questidf09' 
type='text' 
value='<%=sv.questidf09.getFormData()%>' 
maxLength='<%=sv.questidf09.getLength()%>' 
size='<%=sv.questidf09.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf09)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf09).getColor()== null  ? 
"input_cell" :  (sv.questidf09).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

							</div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst09.getFormData();  
%>

<% 
	if((new Byte((sv.questst09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst09'
id='questst09'  
type='text' 
value='<%=sv.questst09.getFormData()%>' 
maxLength='<%=sv.questst09.getLength()%>' 
size='<%=sv.questst09.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst09)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst09).getColor()== null  ? 
"input_cell" :  (sv.questst09).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst09')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf10.getFormData();  
%>

<% 
	if((new Byte((sv.questidf10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf10' 
id='questidf10'  
type='text' 
value='<%=sv.questidf10.getFormData()%>' 
maxLength='<%=sv.questidf10.getLength()%>' 
size='<%=sv.questidf10.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf10)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questidf10).getColor()== null  ? 
"input_cell" :  (sv.questidf10).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       </div>
	                    </div>
	                 
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst10.getFormData();  
%>

<% 
	if((new Byte((sv.questst10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst10' 
id='questst10'  
type='text' 
value='<%=sv.questst10.getFormData()%>' 
maxLength='<%=sv.questst10.getLength()%>' 
size='<%=sv.questst10.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst10)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>


<%
	}else { 
%>

class = ' <%=(sv.questst10).getColor()== null  ? 
"input_cell" :  (sv.questst10).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst10')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>


	                       </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf11.getFormData();  
%>

<% 
	if((new Byte((sv.questidf11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf11' 
id='questidf11' 
type='text' 
value='<%=sv.questidf11.getFormData()%>' 
maxLength='<%=sv.questidf11.getLength()%>' 
size='<%=sv.questidf11.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf11)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf11')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf11).getColor()== null  ? 
"input_cell" :  (sv.questidf11).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf11')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                        </div>
	                    </div>
	                  
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst11.getFormData();  
%>

<% 
	if((new Byte((sv.questst11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst11' 
id='questst11' 
type='text' 
value='<%=sv.questst11.getFormData()%>' 
maxLength='<%=sv.questst11.getLength()%>' 
size='<%=sv.questst11.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst11)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst11')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst11).getColor()== null  ? 
"input_cell" :  (sv.questst11).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst11')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                    
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf12.getFormData();  
%>

<% 
	if((new Byte((sv.questidf12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf12' 
id='questidf12' 
type='text' 
value='<%=sv.questidf12.getFormData()%>' 
maxLength='<%=sv.questidf12.getLength()%>' 
size='<%=sv.questidf12.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf12)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf12')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>


<%
	}else { 
%>

class = ' <%=(sv.questidf12).getColor()== null  ? 
"input_cell" :  (sv.questidf12).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf12')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                 
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst12.getFormData();  
%>

<% 
	if((new Byte((sv.questst12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst12' 
id='questst12' 
type='text' 
value='<%=sv.questst12.getFormData()%>' 
maxLength='<%=sv.questst12.getLength()%>' 
size='<%=sv.questst12.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst12)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst12')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst12).getColor()== null  ? 
"input_cell" :  (sv.questst12).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst12')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                  
	       </div>
	      </br> 
<div class="row">
                       <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf13.getFormData();  
%>

<% 
	if((new Byte((sv.questidf13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf13' 
id='questidf13' 
type='text' 
value='<%=sv.questidf13.getFormData()%>' 
maxLength='<%=sv.questidf13.getLength()%>' 
size='<%=sv.questidf13.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf13)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf13')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span> 

<%
	}else { 
%>

class = ' <%=(sv.questidf13).getColor()== null  ? 
"input_cell" :  (sv.questidf13).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf13')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                        </div>
	                    </div>
	                  
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst13.getFormData();  
%>

<% 
	if((new Byte((sv.questst13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst13' 
id='questst13' 
type='text' 
value='<%=sv.questst13.getFormData()%>' 
maxLength='<%=sv.questst13.getLength()%>' 
size='<%=sv.questst13.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst13)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst13')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst13).getColor()== null  ? 
"input_cell" :  (sv.questst13).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst13')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>

	                      </div>	                    
	                  </div>
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf14.getFormData();  
%>

<% 
	if((new Byte((sv.questidf14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf14' 
id='questidf14' 
type='text' 
value='<%=sv.questidf14.getFormData()%>' 
maxLength='<%=sv.questidf14.getLength()%>' 
size='<%=sv.questidf14.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf14)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf14')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf14).getColor()== null  ? 
"input_cell" :  (sv.questidf14).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf14')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>

							</div>
	                       
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst14.getFormData();  
%>

<% 
	if((new Byte((sv.questst14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst14' 
id='questst14' 
type='text' 
value='<%=sv.questst14.getFormData()%>' 
maxLength='<%=sv.questst14.getLength()%>' 
size='<%=sv.questst14.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst14)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 



<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst14')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst14).getColor()== null  ? 
"input_cell" :  (sv.questst14).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst14')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       	 </div>
	                    </div>
	                  
	                  
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf15.getFormData();  
%>

<% 
	if((new Byte((sv.questidf15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf15' 
id='questidf15' 
type='text' 
value='<%=sv.questidf15.getFormData()%>' 
maxLength='<%=sv.questidf15.getLength()%>' 
size='<%=sv.questidf15.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf15)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf15')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf15).getColor()== null  ? 
"input_cell" :  (sv.questidf15).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf15')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst15.getFormData();  
%>

<% 
	if((new Byte((sv.questst15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst15' 
id='questst15'
type='text' 
value='<%=sv.questst15.getFormData()%>' 
maxLength='<%=sv.questst15.getLength()%>' 
size='<%=sv.questst15.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst15)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst15')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst15).getColor()== null  ? 
"input_cell" :  (sv.questst15).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst15')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf16.getFormData();  
%>

<% 
	if((new Byte((sv.questidf16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf16' 
type='text' 
value='<%=sv.questidf16.getFormData()%>' 
maxLength='<%=sv.questidf16.getLength()%>' 
size='<%=sv.questidf16.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf16)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf16')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf16).getColor()== null  ? 
"input_cell" :  (sv.questidf16).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf16')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
						 </div>
	                    </div>
	                  
	                  
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst16.getFormData();  
%>

<% 
	if((new Byte((sv.questst16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst16' 
id='questst16'
type='text' 
value='<%=sv.questst16.getFormData()%>' 
maxLength='<%=sv.questst16.getLength()%>' 
size='<%=sv.questst16.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst16)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst16')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst16).getColor()== null  ? 
"input_cell" :  (sv.questst16).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst16')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                        </div>
	                    </div>
	                  </div>
	       
	    </br>
 <div class="row">
                      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf17.getFormData();  
%>

<% 
	if((new Byte((sv.questidf17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf17' 
id='questidf17'
type='text' 
value='<%=sv.questidf17.getFormData()%>' 
maxLength='<%=sv.questidf17.getLength()%>' 
size='<%=sv.questidf17.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf17)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf17')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf17).getColor()== null  ? 
"input_cell" :  (sv.questidf17).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf17')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div>
	                    </div>
	                    
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst17.getFormData();  
%>

<% 
	if((new Byte((sv.questst17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst17' 
id='questst17'
type='text' 
value='<%=sv.questst17.getFormData()%>' 
maxLength='<%=sv.questst17.getLength()%>' 
size='<%=sv.questst17.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst17)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst17')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst17).getColor()== null  ? 
"input_cell" :  (sv.questst17).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst17')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div>
	                    </div>
	                  
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf18.getFormData();  
%>

<% 
	if((new Byte((sv.questidf18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf18' 
id='questidf18'
type='text' 
value='<%=sv.questidf18.getFormData()%>' 
maxLength='<%=sv.questidf18.getLength()%>' 
size='<%=sv.questidf18.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf18)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf18')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf18).getColor()== null  ? 
"input_cell" :  (sv.questidf18).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf18')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>


	                       </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst18.getFormData();  
%>

<% 
	if((new Byte((sv.questst18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst18' 
id='questst18'
type='text' 
value='<%=sv.questst18.getFormData()%>' 
maxLength='<%=sv.questst18.getLength()%>' 
size='<%=sv.questst18.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst18)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst18')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst18).getColor()== null  ? 
"input_cell" :  (sv.questst18).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst18')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>

	                      </div>
	                    </div>
	                    
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf19.getFormData();  
%>

<% 
	if((new Byte((sv.questidf19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf19' 
id='questidf19'
type='text' 
value='<%=sv.questidf19.getFormData()%>' 
maxLength='<%=sv.questidf19.getLength()%>' 
size='<%=sv.questidf19.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf19)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf19')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf19).getColor()== null  ? 
"input_cell" :  (sv.questidf19).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf19')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       </div>
	                    </div>
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst19.getFormData();  
%>

<% 
	if((new Byte((sv.questst19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst19' 
id='questst19'
type='text' 
value='<%=sv.questst19.getFormData()%>' 
maxLength='<%=sv.questst19.getLength()%>' 
size='<%=sv.questst19.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst19)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst19')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst19).getColor()== null  ? 
"input_cell" :  (sv.questst19).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst19')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf20.getFormData();  
%>

<% 
	if((new Byte((sv.questidf20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf20' 
id='questidf20'
type='text' 
value='<%=sv.questidf20.getFormData()%>' 
maxLength='<%=sv.questidf20.getLength()%>' 
size='<%=sv.questidf20.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf20)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf20')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf20).getColor()== null  ? 
"input_cell" :  (sv.questidf20).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf20')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst20.getFormData();  
%>

<% 
	if((new Byte((sv.questst20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst20' 
id='questst20'
type='text' 
value='<%=sv.questst20.getFormData()%>' 
maxLength='<%=sv.questst20.getLength()%>' 
size='<%=sv.questst20.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst20)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst20')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst20).getColor()== null  ? 
"input_cell" :  (sv.questst20).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst20')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                  
	       </div>
	       </br>