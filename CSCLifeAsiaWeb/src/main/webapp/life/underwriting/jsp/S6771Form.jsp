
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6771";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6771ScreenVars sv = (S6771ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Underwriting subroutine ");%>

<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	                        <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	                      <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                       <div class="input-group">
	                    <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  							</div>
	                    </div>
	                  </div>
	             </div>
  
	 <div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
			</div>

		
		 <div class="row">
                       <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Question")%></label>
	                    </div>
	                  </div>
	                   
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Statistic")%></label>
	                    </div>
	                  </div>
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Question")%></label>
	                    </div>
	                  </div>
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Statistic")%></label>
	                    </div>
	                  </div>
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Question")%></label>
	                    </div>
	                  </div>
	                  
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Statistic")%></label>
	                    </div>
	                  </div>
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Question")%></label>
	                    </div>
	                  </div>
	                    <div class="col-md-2" style="max-width: 110px;"  >
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Statistic")%></label>
	                    </div>
	                  </div>
	       </div>
	       <%-- split first 5 rows to another file --%>
       <jsp:include page="S6771FormPart.jsp"></jsp:include>
	       
	       <div class="row">
                     <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf21.getFormData();  
%>

<% 
	if((new Byte((sv.questidf21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf21' 
id='questidf21'
type='text' 
value='<%=sv.questidf21.getFormData()%>' 
maxLength='<%=sv.questidf21.getLength()%>' 
size='<%=sv.questidf21.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf21)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf21')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf21).getColor()== null  ? 
"input_cell" :  (sv.questidf21).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf21')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                        </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                     <%	
	longValue = sv.questst21.getFormData();  
%>

<% 
	if((new Byte((sv.questst21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst21' 
id='questst21'
type='text' 
value='<%=sv.questst21.getFormData()%>' 
maxLength='<%=sv.questst21.getLength()%>' 
size='<%=sv.questst21.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst21)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst21')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst21).getColor()== null  ? 
"input_cell" :  (sv.questst21).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst21')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                     </div>
	                    </div>
	                    
	                    
	                <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf22.getFormData();  
%>

<% 
	if((new Byte((sv.questidf22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf22' 
id='questidf22'
type='text' 
value='<%=sv.questidf22.getFormData()%>' 
maxLength='<%=sv.questidf22.getLength()%>' 
size='<%=sv.questidf22.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf22)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf22')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf22).getColor()== null  ? 
"input_cell" :  (sv.questidf22).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf22')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>


<%}longValue = null;} %>
	                       </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst22.getFormData();  
%>

<% 
	if((new Byte((sv.questst22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst22' 
id='questst22'
type='text' 
value='<%=sv.questst22.getFormData()%>' 
maxLength='<%=sv.questst22.getLength()%>' 
size='<%=sv.questst22.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst22)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst22')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst22).getColor()== null  ? 
"input_cell" :  (sv.questst22).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst22')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                
	                <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf23.getFormData();  
%>

<% 
	if((new Byte((sv.questidf23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf23' 
id='questidf23'
type='text' 
value='<%=sv.questidf23.getFormData()%>' 
maxLength='<%=sv.questidf23.getLength()%>' 
size='<%=sv.questidf23.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf23)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf23')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf23).getColor()== null  ? 
"input_cell" :  (sv.questidf23).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf23')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst23.getFormData();  
%>

<% 
	if((new Byte((sv.questst23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst23' 
id='questst23'
type='text' 
value='<%=sv.questst23.getFormData()%>' 
maxLength='<%=sv.questst23.getLength()%>' 
size='<%=sv.questst23.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst23)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst23')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst23).getColor()== null  ? 
"input_cell" :  (sv.questst23).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst23')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       </div>
	                    </div>
	                  
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf24.getFormData();  
%>

<% 
	if((new Byte((sv.questidf24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf24' 
id='questidf24'
type='text' 
value='<%=sv.questidf24.getFormData()%>' 
maxLength='<%=sv.questidf24.getLength()%>' 
size='<%=sv.questidf24.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf24)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf24')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf24).getColor()== null  ? 
"input_cell" :  (sv.questidf24).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf24')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                        </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst24.getFormData();  
%>

<% 
	if((new Byte((sv.questst24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst24' 
id='questst24'
type='text' 
value='<%=sv.questst24.getFormData()%>' 
maxLength='<%=sv.questst24.getLength()%>' 
size='<%=sv.questst24.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst24)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst24')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst24).getColor()== null  ? 
"input_cell" :  (sv.questst24).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst24')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                  
	       </div>
	   </br>    
	       
	        <div class="row">
                     <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                     <%	
	longValue = sv.questidf25.getFormData();  
%>

<% 
	if((new Byte((sv.questidf25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf25' 
id='questidf25'
type='text' 
value='<%=sv.questidf25.getFormData()%>' 
maxLength='<%=sv.questidf25.getLength()%>' 
size='<%=sv.questidf25.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf25)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf25).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf25')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf25).getColor()== null  ? 
"input_cell" :  (sv.questidf25).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf25')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst25.getFormData();  
%>

<% 
	if((new Byte((sv.questst25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst25'
id='questst25' 
type='text' 
value='<%=sv.questst25.getFormData()%>' 
maxLength='<%=sv.questst25.getLength()%>' 
size='<%=sv.questst25.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst25)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst25).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst25')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst25).getColor()== null  ? 
"input_cell" :  (sv.questst25).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst25')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf26.getFormData();  
%>

<% 
	if((new Byte((sv.questidf26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf26' 
id='questidf26' 
type='text' 
value='<%=sv.questidf26.getFormData()%>' 
maxLength='<%=sv.questidf26.getLength()%>' 
size='<%=sv.questidf26.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf26)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf26).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf26')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf26).getColor()== null  ? 
"input_cell" :  (sv.questidf26).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf26')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                  
	                  
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst26.getFormData();  
%>

<% 
	if((new Byte((sv.questst26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst26' 
id='questst26' 
type='text' 
value='<%=sv.questst26.getFormData()%>' 
maxLength='<%=sv.questst26.getLength()%>' 
size='<%=sv.questst26.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst26)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst26).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst26')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst26).getColor()== null  ? 
"input_cell" :  (sv.questst26).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst26')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf27.getFormData();  
%>

<% 
	if((new Byte((sv.questidf27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf27' 
id='questidf27'
type='text' 
value='<%=sv.questidf27.getFormData()%>' 
maxLength='<%=sv.questidf27.getLength()%>' 
size='<%=sv.questidf27.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf27)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf27).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf27')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf27).getColor()== null  ? 
"input_cell" :  (sv.questidf27).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf27')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                       </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst27.getFormData();  
%>

<% 
	if((new Byte((sv.questst27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst27' 
id='questst27'
type='text' 
value='<%=sv.questst27.getFormData()%>' 
maxLength='<%=sv.questst27.getLength()%>' 
size='<%=sv.questst27.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst27)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst27).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst27')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst27).getColor()== null  ? 
"input_cell" :  (sv.questst27).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst27')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf28.getFormData();  
%>

<% 
	if((new Byte((sv.questidf28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf28' 
id='questidf28'
type='text' 
value='<%=sv.questidf28.getFormData()%>' 
maxLength='<%=sv.questidf28.getLength()%>' 
size='<%=sv.questidf28.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf28)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf28).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf28')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf28).getColor()== null  ? 
"input_cell" :  (sv.questidf28).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf28')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst28.getFormData();  
%>

<% 
	if((new Byte((sv.questst28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst28' 
id='questst28'
type='text' 
value='<%=sv.questst28.getFormData()%>' 
maxLength='<%=sv.questst28.getLength()%>' 
size='<%=sv.questst28.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst28)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst28).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst28')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst28).getColor()== null  ? 
"input_cell" :  (sv.questst28).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst28')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                       </div>
	                    </div>
	                  
	       </div>
	       
	 </br>      
	       	 <div class="row">
                      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf29.getFormData();  
%>

<% 
	if((new Byte((sv.questidf29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf29' 
id='questidf29'
type='text' 
value='<%=sv.questidf29.getFormData()%>' 
maxLength='<%=sv.questidf29.getLength()%>' 
size='<%=sv.questidf29.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf29)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf29).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf29')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf29).getColor()== null  ? 
"input_cell" :  (sv.questidf29).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf29')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                       </div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst29.getFormData();  
%>

<% 
	if((new Byte((sv.questst29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst29' 
id='questst29'
type='text' 
value='<%=sv.questst29.getFormData()%>' 
maxLength='<%=sv.questst29.getLength()%>' 
size='<%=sv.questst29.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst29)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst29).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst29')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst29).getColor()== null  ? 
"input_cell" :  (sv.questst29).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst29')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>       
	                  </div>
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf30.getFormData();  
%>

<% 
	if((new Byte((sv.questidf30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf30' 
id='questidf30'
type='text' 
value='<%=sv.questidf30.getFormData()%>' 
maxLength='<%=sv.questidf30.getLength()%>' 
size='<%=sv.questidf30.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf30)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf30).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf30')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf30).getColor()== null  ? 
"input_cell" :  (sv.questidf30).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf30')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							 </div>                    
	                  </div>
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst30.getFormData();  
%>

<% 
	if((new Byte((sv.questst30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst30' 
id='questst30'
type='text' 
value='<%=sv.questst30.getFormData()%>' 
maxLength='<%=sv.questst30.getLength()%>' 
size='<%=sv.questst30.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst30)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst30).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst30')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst30).getColor()== null  ? 
"input_cell" :  (sv.questst30).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst30')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
								 </div>
	                    </div>
	                    
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf31.getFormData();  
%>

<% 
	if((new Byte((sv.questidf31).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf31' 
id='questidf31'
type='text' 
value='<%=sv.questidf31.getFormData()%>' 
maxLength='<%=sv.questidf31.getLength()%>' 
size='<%=sv.questidf31.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf31)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf31).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf31).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf31')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf31).getColor()== null  ? 
"input_cell" :  (sv.questidf31).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf31')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                    
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst31.getFormData();  
%>

<% 
	if((new Byte((sv.questst31).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst31' 
id='questst31'
type='text' 
value='<%=sv.questst31.getFormData()%>' 
maxLength='<%=sv.questst31.getLength()%>' 
size='<%=sv.questst31.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst31)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst31).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst31).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst31')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst31).getColor()== null  ? 
"input_cell" :  (sv.questst31).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst31')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                 
               <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf32.getFormData();  
%>

<% 
	if((new Byte((sv.questidf32).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf32' 
id='questidf32'
type='text' 
value='<%=sv.questidf32.getFormData()%>' 
maxLength='<%=sv.questidf32.getLength()%>' 
size='<%=sv.questidf32.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf32)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf32).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf32).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf32')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf32).getColor()== null  ? 
"input_cell" :  (sv.questidf32).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf32')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
						 </div>
	                    </div>
	                 
	                 
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst32.getFormData();  
%>

<% 
	if((new Byte((sv.questst32).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst32' 
id='questst32'
type='text' 
value='<%=sv.questst32.getFormData()%>' 
maxLength='<%=sv.questst32.getLength()%>' 
size='<%=sv.questst32.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst32)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst32).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst32).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst32')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst32).getColor()== null  ? 
"input_cell" :  (sv.questst32).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst32')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
						 </div>
	                    </div>                  
	       </div>
	</br>       
	         <div class="row">
                     <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                     <%	
	longValue = sv.questidf33.getFormData();  
%>

<% 
	if((new Byte((sv.questidf33).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf33' 
id='questidf33'
type='text' 
value='<%=sv.questidf33.getFormData()%>' 
maxLength='<%=sv.questidf33.getLength()%>' 
size='<%=sv.questidf33.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf33)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf33).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf33).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf33')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf33).getColor()== null  ? 
"input_cell" :  (sv.questidf33).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf33')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

	                      </div>
	                    </div>
	                  
	                  
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                     <%	
	longValue = sv.questst33.getFormData();  
%>

<% 
	if((new Byte((sv.questst33).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst33' 
id='questst33'
type='text' 
value='<%=sv.questst33.getFormData()%>' 
maxLength='<%=sv.questst33.getLength()%>' 
size='<%=sv.questst33.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst33)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst33).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst33).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst33')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst33).getColor()== null  ? 
"input_cell" :  (sv.questst33).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst33')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

							</div>
	                    </div>
	                  
	                  
	                   <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf34.getFormData();  
%>

<% 
	if((new Byte((sv.questidf34).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf34' 
id='questidf34'
type='text' 
value='<%=sv.questidf34.getFormData()%>' 
maxLength='<%=sv.questidf34.getLength()%>' 
size='<%=sv.questidf34.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf34)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf34).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf34).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf34')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf34).getColor()== null  ? 
"input_cell" :  (sv.questidf34).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf34')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst34.getFormData();  
%>

<% 
	if((new Byte((sv.questst34).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst34' 
id='questst34'
type='text' 
value='<%=sv.questst34.getFormData()%>' 
maxLength='<%=sv.questst34.getLength()%>' 
size='<%=sv.questst34.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst34)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst34).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst34).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst34')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst34).getColor()== null  ? 
"input_cell" :  (sv.questst34).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst34')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf35.getFormData();  
%>

<% 
	if((new Byte((sv.questidf35).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf35' 
id='questidf35'
type='text' 
value='<%=sv.questidf35.getFormData()%>' 
maxLength='<%=sv.questidf35.getLength()%>' 
size='<%=sv.questidf35.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf35)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf35).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf35).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf35')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf35).getColor()== null  ? 
"input_cell" :  (sv.questidf35).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf35')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>

							</div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       
<%	
	longValue = sv.questst35.getFormData();  
%>

<% 
	if((new Byte((sv.questst35).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst35' 
id='questst35'
type='text' 
value='<%=sv.questst35.getFormData()%>' 
maxLength='<%=sv.questst35.getLength()%>' 
size='<%=sv.questst35.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst35)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst35).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst35).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst35')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst35).getColor()== null  ? 
"input_cell" :  (sv.questst35).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst35')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                
	                <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questidf36.getFormData();  
%>

<% 
	if((new Byte((sv.questidf36).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf36' 
id='questidf36'
type='text' 
value='<%=sv.questidf36.getFormData()%>' 
maxLength='<%=sv.questidf36.getLength()%>' 
size='<%=sv.questidf36.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf36)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf36).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf36).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf36')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf36).getColor()== null  ? 
"input_cell" :  (sv.questidf36).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf36')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                    </div>
	                  </div>
	                 
	                 
	                  <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst36.getFormData();  
%>

<% 
	if((new Byte((sv.questst36).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst36' 
id='questst36'
type='text' 
value='<%=sv.questst36.getFormData()%>' 
maxLength='<%=sv.questst36.getLength()%>' 
size='<%=sv.questst36.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst36)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst36).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst36).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 


<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst36')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst36).getColor()== null  ? 
"input_cell" :  (sv.questst36).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst36')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                  
	       </div>
	       
	   </br>    
	       
	        <div class="row">
                      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf37.getFormData();  
%>

<% 
	if((new Byte((sv.questidf37).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf37' 
id='questidf37'
type='text' 
value='<%=sv.questidf37.getFormData()%>' 
maxLength='<%=sv.questidf37.getLength()%>' 
size='<%=sv.questidf37.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf37)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf37).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf37).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf37')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questidf37).getColor()== null  ? 
"input_cell" :  (sv.questidf37).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf37')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                        </div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst37.getFormData();  
%>

<% 
	if((new Byte((sv.questst37).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst37' 
id='questst37'
type='text' 
value='<%=sv.questst37.getFormData()%>' 
maxLength='<%=sv.questst37.getLength()%>' 
size='<%=sv.questst37.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst37)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst37).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst37).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst37')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%
	}else { 
%>

class = ' <%=(sv.questst37).getColor()== null  ? 
"input_cell" :  (sv.questst37).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst37')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
							</div>
	                    </div>
	                 
	                 <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf38.getFormData();  
%>

<% 
	if((new Byte((sv.questidf38).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf38' 
id='questidf38'
type='text' 
value='<%=sv.questidf38.getFormData()%>' 
maxLength='<%=sv.questidf38.getLength()%>' 
size='<%=sv.questidf38.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf38)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf38).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf38).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf38')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf38).getColor()== null  ? 
"input_cell" :  (sv.questidf38).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf38')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%}longValue = null;} %>
							</div>
	                    </div>
	                 

      <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                       <%	
	longValue = sv.questst38.getFormData();  
%>

<% 
	if((new Byte((sv.questst38).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst38' 
id='questst38'
type='text' 
value='<%=sv.questst38.getFormData()%>' 
maxLength='<%=sv.questst38.getLength()%>' 
size='<%=sv.questst38.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst38)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst38).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst38).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst38')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst38).getColor()== null  ? 
"input_cell" :  (sv.questst38).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst38')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                  
					<div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf39.getFormData();  
%>

<% 
	if((new Byte((sv.questidf39).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf39'
id='questidf39' 
type='text' 
value='<%=sv.questidf39.getFormData()%>' 
maxLength='<%=sv.questidf39.getLength()%>' 
size='<%=sv.questidf39.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf39)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf39).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf39).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf39')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf39).getColor()== null  ? 
"input_cell" :  (sv.questidf39).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf39')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                  
               <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                    	
	                       <%	
	longValue = sv.questst39.getFormData();  
%>

<% 
	if((new Byte((sv.questst39).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst39' 
id='questst39' 
type='text' 
value='<%=sv.questst39.getFormData()%>' 
maxLength='<%=sv.questst39.getLength()%>' 
size='<%=sv.questst39.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst39)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst39).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst39).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst39')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst39).getColor()== null  ? 
"input_cell" :  (sv.questst39).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst39')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                       </div>
	                    </div>
	                

                <div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questidf40.getFormData();  
%>

<% 
	if((new Byte((sv.questidf40).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questidf40'
id='questidf40'  
type='text' 
value='<%=sv.questidf40.getFormData()%>' 
maxLength='<%=sv.questidf40.getLength()%>' 
size='<%=sv.questidf40.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questidf40)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questidf40).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questidf40).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf40')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questidf40).getColor()== null  ? 
"input_cell" :  (sv.questidf40).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questidf40')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                 
<div class="col-md-2" style="max-width: 110px;"  >
   	                       <div class="input-group" style="max-width: 110px;">
	                      <%	
	longValue = sv.questst40.getFormData();  
%>

<% 
	if((new Byte((sv.questst40).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='questst40' 
id='questst40'
type='text' 
value='<%=sv.questst40.getFormData()%>' 
maxLength='<%=sv.questst40.getLength()%>' 
size='<%=sv.questst40.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(questst40)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.questst40).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.questst40).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst40')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>
<%
	}else { 
%>

class = ' <%=(sv.questst40).getColor()== null  ? 
"input_cell" :  (sv.questst40).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('questst40')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span>

<%}longValue = null;} %>
	                      </div>
	                    </div>
	                  
	       </div>
	</br>       
	           <div class="row">
                      <div class="col-md-3">
	                    <div class="form-group">
	                        <label><%=resourceBundleHandler.gettingValueFromBundle(" Underwriting subroutine")%></label>
	                         <input name='undwsubr' 
type='text'

<%

		formatValue = (sv.undwsubr.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.undwsubr.getLength()%>'
maxLength='<%= sv.undwsubr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(undwsubr)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.undwsubr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:65px;"
<%
	}else if((new Byte((sv.undwsubr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:65px;"

<%
	}else { 
%>

	class = ' <%=(sv.undwsubr).getColor()== null  ? 
			"input_cell" :  (sv.undwsubr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:80px;"
 
<%
	} 
%>
>
	                    </div>
	                  </div>
	             </div>
	             
	             
	             
	</div>
</div> 		
<%@ include file="/POLACommon2NEW.jsp"%>
