<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6775FormPart";%>

<%@page import="com.csc.life.underwriting.screens.S6775ScreenVars"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>

<%
String tit ="";
QPScreenField qpsf = null; 
DataModel sm = null;
String formatValue = null;
String valueThis = null;
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();


LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

LifeAsiaAppVars appVars = av;

String lan = av.getUserLanguage().toString().trim().toLowerCase();

if ("".equals(lan)){lan = "eng";}

String localeimageFolder = lan;

boolean[] savedInds = null;

if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), lang);
%> 

<%S6775ScreenVars sv = (S6775ScreenVars) fw.getVariables();%>



<%-- Prepare data--%>

<%!
public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}
%>

<tr height="10px"></tr>
<tr>
<td style="max-width:50px;">



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom01) %>'
	 <%}%>

size='<%= sv.intgfrom01.getLength()%>'
maxLength='<%= sv.intgfrom01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom01).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td style="max-width:50px;">



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto01' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto01) %>'
	 <%}%>

size='<%= sv.intgto01.getLength()%>'
maxLength='<%= sv.intgto01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto01).getColor()== null  ? 
			"input_cell" :  (sv.intgto01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule01");
	optionValue = makeDropDownList( mappedItems , sv.undwrule01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule01' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>


<td style="max-width:50px;">


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom13).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom13' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom13) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom13);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom13) %>'
	 <%}%>

size='<%= sv.intgfrom13.getLength()%>'
maxLength='<%= sv.intgfrom13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom13)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom13).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>

<td style="max-width:50px;">




	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto13).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto13' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto13) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto13);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto13) %>'
	 <%}%>

size='<%= sv.intgto13.getLength()%>'
maxLength='<%= sv.intgto13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto13)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto13).getColor()== null  ? 
			"input_cell" :  (sv.intgto13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule13"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule13");
	optionValue = makeDropDownList( mappedItems , sv.undwrule13.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule13.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule13).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule13' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule13).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>  </tr>



<tr height="10px"></tr>

<tr>
<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom02) %>'
	 <%}%>

size='<%= sv.intgfrom02.getLength()%>'
maxLength='<%= sv.intgfrom02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom02).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto02' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto02) %>'
	 <%}%>

size='<%= sv.intgto02.getLength()%>'
maxLength='<%= sv.intgto02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto02).getColor()== null  ? 
			"input_cell" :  (sv.intgto02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule02");
	optionValue = makeDropDownList( mappedItems , sv.undwrule02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule02' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom14).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom14' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom14) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom14);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom14) %>'
	 <%}%>

size='<%= sv.intgfrom14.getLength()%>'
maxLength='<%= sv.intgfrom14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom14)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom14).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto14).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto14' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto14) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto14);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto14) %>'
	 <%}%>

size='<%= sv.intgto14.getLength()%>'
maxLength='<%= sv.intgto14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto14)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto14).getColor()== null  ? 
			"input_cell" :  (sv.intgto14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule14"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule14");
	optionValue = makeDropDownList( mappedItems , sv.undwrule14.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule14.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule14).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule14' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule14).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>


<tr height="10px"></tr>

<tr >
<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom03) %>'
	 <%}%>

size='<%= sv.intgfrom03.getLength()%>'
maxLength='<%= sv.intgfrom03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom03).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto03' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto03) %>'
	 <%}%>

size='<%= sv.intgto03.getLength()%>'
maxLength='<%= sv.intgto03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto03).getColor()== null  ? 
			"input_cell" :  (sv.intgto03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule03");
	optionValue = makeDropDownList( mappedItems , sv.undwrule03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule03' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom15).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom15' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom15) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom15);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom15) %>'
	 <%}%>

size='<%= sv.intgfrom15.getLength()%>'
maxLength='<%= sv.intgfrom15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom15)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom15).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto15).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto15' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto15) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto15);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto15) %>'
	 <%}%>

size='<%= sv.intgto15.getLength()%>'
maxLength='<%= sv.intgto15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto15)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto15).getColor()== null  ? 
			"input_cell" :  (sv.intgto15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule15"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule15");
	optionValue = makeDropDownList( mappedItems , sv.undwrule15.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule15.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule15).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule15' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule15).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>
<tr height="10px"></tr>
<tr >
<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom04) %>'
	 <%}%>

size='<%= sv.intgfrom04.getLength()%>'
maxLength='<%= sv.intgfrom04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom04).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto04' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto04) %>'
	 <%}%>

size='<%= sv.intgto04.getLength()%>'
maxLength='<%= sv.intgto04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto04).getColor()== null  ? 
			"input_cell" :  (sv.intgto04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule04");
	optionValue = makeDropDownList( mappedItems , sv.undwrule04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule04' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom16).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom16' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom16) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom16);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom16) %>'
	 <%}%>

size='<%= sv.intgfrom16.getLength()%>'
maxLength='<%= sv.intgfrom16.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom16)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom16).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom16).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto16).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto16' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto16) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto16);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto16) %>'
	 <%}%>

size='<%= sv.intgto16.getLength()%>'
maxLength='<%= sv.intgto16.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto16)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto16).getColor()== null  ? 
			"input_cell" :  (sv.intgto16).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule16"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule16");
	optionValue = makeDropDownList( mappedItems , sv.undwrule16.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule16.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule16).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule16' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule16).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr >
<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom05) %>'
	 <%}%>

size='<%= sv.intgfrom05.getLength()%>'
maxLength='<%= sv.intgfrom05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom05).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto05' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto05) %>'
	 <%}%>

size='<%= sv.intgto05.getLength()%>'
maxLength='<%= sv.intgto05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto05).getColor()== null  ? 
			"input_cell" :  (sv.intgto05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule05");
	optionValue = makeDropDownList( mappedItems , sv.undwrule05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule05' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom17).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom17' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom17) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom17);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom17) %>'
	 <%}%>

size='<%= sv.intgfrom17.getLength()%>'
maxLength='<%= sv.intgfrom17.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom17)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom17).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom17).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>

<td width='85px'>




	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto17).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto17' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto17) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto17);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto17) %>'
	 <%}%>

size='<%= sv.intgto17.getLength()%>'
maxLength='<%= sv.intgto17.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto17)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto17).getColor()== null  ? 
			"input_cell" :  (sv.intgto17).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule17"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule17");
	optionValue = makeDropDownList( mappedItems , sv.undwrule17.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule17.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule17).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule17' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule17).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>
<tr height="10px"></tr>

