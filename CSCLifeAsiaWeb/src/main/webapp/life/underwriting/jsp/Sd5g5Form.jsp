


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SD5G5";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>

<%Sd5g5ScreenVars sv = (Sd5g5ScreenVars) fw.getVariables();%> 
	

	<%{
	}

	%>
	
<%appVars.rollup(new int[] {93});%>

	<div class="panel panel-default">

    	<div class="panel-body">
			
			<div class="row">        
				<div class="col-md-8">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						
					<table><tr><td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
			</td><td>
						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 150px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
			</td></tr></table>
					
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Total Accumulated Risk Amount")%></label>
						
		       		<table><tr><td style="text-align: right;">
		       					<%=smartHF.getHTMLVar(0, 0, fw, null,sv.totRiskamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>	
				  	</td></tr></table>
		       				
					</div>
				</div>	
			</div>
			<br>
			<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-sd5g5" width="100%"
						style="border-right: thin solid #dddddd !important;">
                         	<thead>
                            	<tr class='info'>
                                	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel") %></center></th>
                                    <th ><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Class ") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Class Description ") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Amount") %></center></th>
                               </tr>
                            </thead>
                            <tbody>
                            	<%
                            	GeneralTable sfl = fw.getTable("sd5g5screensfl");
                            	Sd5g5screensfl.set1stScreenRow(sfl, appVars, sv);
                            	int count = 1;
                            	while (Sd5g5screensfl.hasMoreScreenRows(sfl)) {
								%> 
								<tr>
						    		<td>
							 			<input type="radio"	value='<%=sv.select.getFormData()%>' 
									 			name='sd5g5screensfl.select_R<%=count%>'
									 			id='sd5g5screensfl.select_R<%=count%>'
									 		    onClick="selectedRow('sd5g5screensfl.select_R<%=count%>')"
				                       			<% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                               		||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
				                           			disabled="disabled"
			    	                   			<% } %>
		                         		/>						 			
									</td>
				    				<td><%=sv.riskClass.getFormData()%></td>
				    				<td><%=sv.riskClassDesc.getFormData()%></td>
				    				<td style="text-align: right;">		
																									
									<%	
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.riskAmt).getFieldName());						
									qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);									
									%>	
									
									<%
									formatValue = smartHF.getPicFormatted(qpsf,sv.riskAmt);
									if(!(sv.riskAmt).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}						
									%>
									<%= formatValue%>
									<%
									longValue = null;
									formatValue = null;								
									%>																																						
														
									</td>
								</tr>								
								<%
								count = count + 1;
								Sd5g5screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>								
                            </tbody>
                       </table>
                	</div>
					<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
					<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
				</div>
			</div>	
			
			
<script>
$(document).ready(function() {
    var showval= document.getElementById('show_lbl').value;
    var toval= document.getElementById('to_lbl').value;
    var ofval= document.getElementById('of_lbl').value;
    var entriesval= document.getElementById('entries_lbl').value;
    var nextval= document.getElementById('nxtbtn_lbl').value;
    var previousval= document.getElementById('prebtn_lbl').value;
    var dtmessage =  document.getElementById('msg_lbl').value;
	$('#dataTables-sd5g5').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '350',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {
                "next":       nextval,
                "previous":   previousval
            }
        }
  	});	
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

