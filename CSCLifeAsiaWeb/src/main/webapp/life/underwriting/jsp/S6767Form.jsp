
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6767";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6767ScreenVars sv = (S6767ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default" >
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
	                        <%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                 
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
	                      <%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life No")%></label>
	                        <%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	             </div>
	             
	             
	              <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
	                       <table>
	                       <tr>
	                       <td>
	                       
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		<td>
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					 if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </td>
	                    </tr>
	                    </table>
	                    </div>
	                  </div>
	             </div>
	             
	             
 <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id='dataTables-s6767' width="100%">
	                         	<thead>
	                            	<tr class='info'>								
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Questions")%></th>
										<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Answers")%></th>
	                               </tr>
	                            </thead>
	                            <tbody>
	                            <% 
	                            GeneralTable sfl = fw.getTable("s6767screensfl");
	                            S6767screensfl
	                        	.set1stScreenRow(sfl, appVars, sv);
	                            %>
	                            <% 
	                        	int count = 1;
	                        	while (S6767screensfl
	                        	.hasMoreScreenRows(sfl)) {
	                        		%>
							                        	
							                            
										  <tr>
																	
													 <td> <%= sv.question.getFormData()%></td>	
																							
													<td >
														  <%
															  longValue=sv.answer.getFormData();
															   if("".equals(longValue)){
															   longValue=resourceBundleHandler.gettingValueFromBundle("Select");
															   }else if("N".equals(longValue.trim())){
																longValue=resourceBundleHandler.gettingValueFromBundle("No");}
															   else if("Y".equals(longValue.trim())){
																longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
																}
															%>
														<%= longValue%>
													</td>
										</tr>
									<%
										count = count + 1;
	                                     S6767screensfl
										.setNextScreenRow(sfl, appVars, sv);
										}
										%>
	                            </tbody>

</table>
</div>
</div>
</div>
</div>
 
	             
	             
	              
     </div>
  </div>
  
  
  <script>
	var lineNo = 0;
	var contentArray = [];
	$("#dataTables-s6767 > tbody > tr").each(function() {
		if($('> td:nth-child(2)', this).html().trim() == ""){
			var question2 = $('> td:nth-child(1)', this).html().trim(); 
			var oriQuestion = $("#dataTables-s6767 > tbody > tr:eq("+(lineNo-1)+") > td:nth-child(1)").html().trim();
			$("#dataTables-s6767 > tbody > tr:eq("+(lineNo-1)+") > td:nth-child(1)").html(oriQuestion + " " + question2);
			$(this).remove();
			lineNo--;
		};
		lineNo++;
	});
$(document).ready(function() {
	$('#dataTables-s6767').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '375px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>
  
  
<%@ include file="/POLACommon2NEW.jsp"%>

