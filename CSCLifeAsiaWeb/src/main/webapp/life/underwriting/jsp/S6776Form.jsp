<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6776";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6776ScreenVars sv = (S6776ScreenVars) fw.getVariables();%>

<%if (sv.S6776screenWritten.gt(0)) {%>
	<%S6776screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Integer?");%>
	<%sv.intgflag.setClassString("");%>
<%	sv.intgflag.appendClassString("string_fld");
	sv.intgflag.appendClassString("input_txt");
	sv.intgflag.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Boolean?");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Apply");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Value");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rule?");%>
	<%sv.boolansw01.setClassString("");%>
<%	sv.boolansw01.appendClassString("string_fld");
	sv.boolansw01.appendClassString("input_txt");
	sv.boolansw01.appendClassString("highlight");
%>
	<%sv.rulechck01.setClassString("");%>
<%	sv.rulechck01.appendClassString("string_fld");
	sv.rulechck01.appendClassString("input_txt");
	sv.rulechck01.appendClassString("highlight");
%>
	<%sv.boolansw02.setClassString("");%>
<%	sv.boolansw02.appendClassString("string_fld");
	sv.boolansw02.appendClassString("input_txt");
	sv.boolansw02.appendClassString("highlight");
%>
	<%sv.rulechck02.setClassString("");%>
<%	sv.rulechck02.appendClassString("string_fld");
	sv.rulechck02.appendClassString("input_txt");
	sv.rulechck02.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>
	
	
<div class="panel panel-default">
<div class="panel-body">
<div class="row">
<div class="col-md-1">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</div>
</div>
<div class="col-md-3"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</div>

</div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<div class="input-group">

<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:400px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-2"></div>


<div class="col-md-5">
<div class="form-inline">
 
<label><%=resourceBundleHandler.gettingValueFromBundle("Integer")%></label>



<div id="intgflag" class="form-control" onhelp="return fieldHelp(&quot;intgflag&quot;)" style="background-color: rgb(238, 238, 238);min-width:80px;"> </div>

</div>
</div>
</div>



<div class="row">
<div class="col-md-2" style="max-width:220px"></div>
<div class="col-md-3">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Boolean?")%>"</label>
</div>
</div>

<div class="col-md-3"  style="max-width:80px">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Valid")%></label>
</div>
</div>

<div class="col-md-1">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Apply")%></label>
</div>
</div>
</div>

<div class="row">

<div class="col-md-5"></div>

<div class="col-md-3" style="max-width:80px">
<label><%=resourceBundleHandler.gettingValueFromBundle("Value")%></label>
</div>

<div class="col-md-3">
<label><%=resourceBundleHandler.gettingValueFromBundle("Rule?")%></label>
</div>
</div>


<div class="row">
<div class="col-md-5"></div>
<div class="col-md-1" style="min-width:50px">
<%=smartHF.getHTMLVar(fw, sv.boolansw01)%>
</div>

<div class="col-md-1" style="min-width:50px">
<%=smartHF.getHTMLVar(fw, sv.rulechck01)%>
</div>
</div>

<div class="row">

<div class="col-md-5"></div>

<div class="col-md-1" style="min-width:50px">
<%=smartHF.getHTMLVar(fw, sv.boolansw01)%>
</div>

<div class="col-md-1" style="min-width:50px">
<%=smartHF.getHTMLVar(fw, sv.boolansw01)%>
</div>
</div>

</div>
</div>	




<!-- ILIFE-2430 Life Cross Browser - Sprint 1 D4: Task 4   changed coordinates-->


<%}%>

<%-- <%if (sv.S6776protectWritten.gt(0)) {%>
	<%S6776protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%> --%>


<%@ include file="/POLACommon2NEW.jsp"%>
