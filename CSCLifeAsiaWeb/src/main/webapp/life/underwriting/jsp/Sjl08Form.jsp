

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl08";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*"%>
<%
	Sjl08ScreenVars sv = (Sjl08ScreenVars) fw.getVariables();
%>
<%
	{
			if (appVars.ind03.isOn()) {
				sv.sumAssuredFactor.setReverse(BaseScreenData.REVERSED);
				sv.sumAssuredFactor.setColor(BaseScreenData.RED);
			}
			if (!appVars.ind03.isOn()) {
				sv.sumAssuredFactor.setHighLight(BaseScreenData.BOLD);
			}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



</td><td>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td></tr></table>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Factor")%></label>
					<div class="input-group" >
					<%=smartHF.getHTMLVarExt(fw, sv.sumAssuredFactor)%>
					</div>
					
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%></label>

				</div>
			</div>
			</div>	
			<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.riskClass01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.riskClass01)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 84px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.riskClass01)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('riskClass01')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
					
			</div>
			<div class="col-md-2">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.riskClass02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.riskClass02)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 84px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.riskClass02)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('riskClass02')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.riskClass03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.riskClass03)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 84px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.riskClass03)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('riskClass03')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.riskClass04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.riskClass04)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 84px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.riskClass04)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('riskClass04')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					  <%
                                         if ((new Byte((sv.riskClass05).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 46px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.riskClass05)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                 <div class="input-group" style="min-width: 84px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.riskClass05)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('riskClass05')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<script>
/* $(document).ready(function() {
$('#subroutine').css("width","80");
$('#allowperiod').css("width","80");
$('#daycheck').css("width","80");

}); */
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

