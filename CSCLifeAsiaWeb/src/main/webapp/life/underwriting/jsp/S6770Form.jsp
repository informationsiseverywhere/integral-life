<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6770";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6770ScreenVars sv = (S6770ScreenVars) fw.getVariables();%>

<%if (sv.S6770screenWritten.gt(0)) {%>
	<%S6770screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"text");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.question01.setClassString("");%>
<%	sv.question01.appendClassString("string_fld");
	sv.question01.appendClassString("input_txt");
	sv.question01.appendClassString("highlight");
	sv.question01.appendClassString("highlight");
%>
	<%sv.question02.setClassString("");%>
<%	sv.question02.appendClassString("string_fld");
	sv.question02.appendClassString("input_txt");
	sv.question02.appendClassString("highlight");
	sv.question02.appendClassString("highlight");
%>
	<%sv.question03.setClassString("");%>
<%	sv.question03.appendClassString("string_fld");
	sv.question03.appendClassString("input_txt");
	sv.question03.appendClassString("highlight");
	sv.question03.appendClassString("highlight");
%>
	<%sv.question04.setClassString("");%>
<%	sv.question04.appendClassString("string_fld");
	sv.question04.appendClassString("input_txt");
	sv.question04.appendClassString("highlight");
	sv.question04.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Question");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.questtyp.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Underwriting Rule  ");%>
	<%sv.undwrule.setClassString("");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Answer Band  ");%>
	<%sv.answband.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.questtyp.setReverse(BaseScreenData.REVERSED);
			sv.questtyp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.questtyp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.undwrule.setReverse(BaseScreenData.REVERSED);
			sv.undwrule.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.undwrule.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.answband.setReverse(BaseScreenData.REVERSED);
			sv.answband.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.answband.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<script>
$(document).ready(function(){
	$("#item").attr("class","input-group-addon");
	$("#longdesc").attr("class","form-control");
	$("#tabl").css("width","100px");
	$("#company").css("width","100px");
	$("#question01").css("width","500px");
	$("#question02").css("width","500px");
	$("#question03").css("width","500px");
	$("#question04").css("width","500px");
	
	
})
</script>	
<div class="panel panel-default">
        <div class="panel-body">
                 <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	                        <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="company" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	                      <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="tabl" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                       <div class="input-group">
	                    <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div  id="item" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  							</div>
	                    </div>
	                  </div>
	             </div>
	             
	             
	<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
			</div>
	<br><br>
		       <div class="row">
                     
	                   <div class="col-md-6">
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Question Text")%></label>
	                      <div class="input-group"> 
	                      <%=smartHF.getHTMLVarExt(fw, sv.question01).replace("<pre></pre>","")%>
	                    
	                      	</div>
	                    </div>
	                  </div>
	            </div> 
	            <div class="row">
                     
	                   <div class="col-md-6">
	                    <div class="form-group">
	                   
	                      <div class="input-group"> 
	                      <%=smartHF.getHTMLVarExt(fw, sv.question02).replace("<pre></pre>","")%>
	                    
	                      	</div>
	                    </div>
	                  </div>
	            </div> 
	             <div class="row">
	             <div class="col-md-6">
	                    <div class="form-group">
	                   
	                      <div class="input-group"> 
	                      <%=smartHF.getHTMLVarExt(fw, sv.question03).replace("<pre></pre>","")%>
	                    
	                      	</div>
	                    </div>
	                  </div>
	            </div> 
	             <div class="row">
	             <div class="col-md-6">
	                    <div class="form-group">
	                   
	                      <div class="input-group"> 
	                      <%=smartHF.getHTMLVarExt(fw, sv.question04).replace("<pre></pre>","")%>
	                    
	                      	</div>
	                    </div>
	                  </div>
	            </div> 
	             <br><br>
	              <div class="row">
	             <div class="col-md-4">
	                    <div class="form-group">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("Question Type")%></label>
	                      <div class="input-group"> 
	                      <!-- ILIFE-6752 -->
	                      <%-- <%=smartHF.getHTMLSpaceVar(16, 3, fw, sv.questtyp)%>
	<%=smartHF.getHTMLF4NSVar(15.9, 3, fw, sv.questtyp)%> --%>
	                    <%
                                         if ((new Byte((sv.questtyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 100px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.questtyp)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 100px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.questtyp)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 19px; border-bottom-width: 0px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('questtyp')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
	                      	</div>
	                    </div>
	                  </div>
	          
	             
	         
	             <div class="col-md-4">
	                    <div class="form-group">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("Underwriter Rule")%></label>
	                      <div class="input-group" style="width:65px;"> 
	                     <%-- <%=smartHF.getHTMLSpaceVar(16, 35, fw, sv.undwrule)%>
	<%=smartHF.getHTMLF4NSVar(15.9, 34, fw, sv.undwrule)%> --%>
	<%
                                         if ((new Byte((sv.undwrule).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 100px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.undwrule)%>
                                                       <span class="input-group-addon"><span style="font-size: 19px;"><span class="glyphicon glyphicon-search"></span></span></span> 
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 100px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.undwrule)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 19px; border-bottom-width: 0px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('undwrule')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
	                    <!-- ILIFE-6752 ends -->
	                      	</div>
	                    </div>
	                  </div>
	            
          
          
	             <div class="col-md-4">
	                    <div class="form-group">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("Answer Band")%></label>
	                      <div class="input-group"> 
	                   <%=smartHF.getHTMLVar(16.1, 67, fw, sv.answband)%>
	                    
	                      	</div>
	                    </div>
	                  </div>
	            </div> 
          
	                
	</div>
</div> 		



<%}%>

<%if (sv.S6770protectWritten.gt(0)) {%>
	<%S6770protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<%@ include file="/POLACommon2NEW.jsp"%>