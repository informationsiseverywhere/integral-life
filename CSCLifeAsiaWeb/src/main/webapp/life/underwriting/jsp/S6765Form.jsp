


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6765";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6765ScreenVars sv = (S6765ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind03.isOn()) {
			sv.answer.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.answer.setInvisibility(BaseScreenData.INVISIBLE);
			sv.answer.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind03.isOn()) {
			sv.answer.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.answer.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
			<div class="form-group" style="max-width:100px;">
					<label  style="white-space: nowrap;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
					
					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
				</div>
			</div>
		
			<div class="col-md-4">
			<div class="form-group" style="max-width:50px;">
				
					<label  style="white-space: nowrap;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life No  "))%></label>
					
					<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
				</div>
			</div>
			
			<div class="col-md-4">
			<div class="form-group" style="max-width:50px;">
					<label  style="white-space: nowrap;" ><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint Life No  "))%></label>
					
					<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

				</div>
			</div>
		</div>
		<%-- Life Assured--%>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label style="white-space: nowrap;"><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life Assured"))%></label>
						
					<table><tr><td>
<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  </td><td>
  
<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td></tr></table>
 
					</div>
				</div>
</div>





	
		


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[2];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.question.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
			} else {		
				calculatedValue = (sv.question.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
														if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.answer.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
						} else {		
							calculatedValue = (sv.answer.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
		totalTblWidth = totalTblWidth -200;
		
			%>
		<%
		GeneralTable sfl = fw.getTable("s6765screensfl");
			savedInds = appVars.saveAllInds();
	S6765screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 13;
	int height;
	smartHF.setSflLineOffset(9);
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>


        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id='dataTables-s6765'   width='100%'>
							<thead>
								<tr class='info'>
									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>

									<th class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>

								</tr>
							</thead>
							<tbody>
							
<%
	S6765screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6765screensfl.hasMoreScreenRows(sfl)) {
%>
<%
{
		
		if (appVars.ind03.isOn()) {
			sv.answer.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind01.isOn()) {
			sv.answer.setInvisibility(BaseScreenData.INVISIBLE);
			sv.answer.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind03.isOn()) {
			sv.answer.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.answer.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	
	
	
		<%if((new Byte((sv.answer).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
						</td>
						
<td
										<%if((sv.answer).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>>
										<%if((new Byte((sv.answer).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <% if(sv.questtyp.getFormData().equalsIgnoreCase("B"))	{%>

										<% 
   longValue=sv.answer.getFormData();
   if("".equals(longValue)){
   longValue=resourceBundleHandler.gettingValueFromBundle("Select");
   }else if("N".equals(longValue)){
    longValue=resourceBundleHandler.gettingValueFromBundle("No");}
   else if("Y".equals(longValue)){
    longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
    }
	if((new Byte((sv.answer.getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected()))){ 
%>
										<div
											class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
											<%if(longValue != null){%>

											<%=XSSFilter.escapeHtml(longValue)%>

											<%}%>
										</div> <%
longValue = null;
%> <% }else {%> <% if("red".equalsIgnoreCase((sv.answer).getColor())){
					%>
										<div style="border:1px; border-style: solid; border-color: #B55050;  width:122px;"> 
											<%
					} 
					%>
											<select
												name='<%="s6765screensfl" + "." +
						 "answer" + "_R" + count %>'
												id='<%="s6765screensfl" + "." +
						 "answer" + "_R" + count %>'
												type='list' style="width: 120px;"
												alt='<%= sv.questidf.getFormData()%>'
												onFocus='doFocus(this)' onHelp='return fieldHelp(answer)'
												onKeyUp='return checkMaxLength(this)' name='answer'
												class="input_cell"
												<%
							if ((new Byte((sv.answer).getEnabled())).compareTo(new Byte(
				// ILIFE-1702 STARTS BY SLAKKALA 				
							BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
				// ILIFE-1702 ENDS   				
							%>
												readonly="true" class="output_cell"
												<%
							} else if ((new Byte((sv.answer).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
												class="bold_cell" <%
							} else {
							%>
												class='
							<%=(sv.answer).getColor() == null ? "input_cell"
							: (sv.answer).getColor().equalsIgnoreCase("red") ? "input_cell red reverse"
							: "input_cell"%>'
												<%
							}
							%>>
												<option value="">----<%=resourceBundleHandler.gettingValueFromBundle("Select")%>----
												</option>
												<option value="Y"
													<% if(sv.answer.getFormData().trim().equalsIgnoreCase("Y")) {%>
													Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
												<option value="N"
													<% if(sv.answer.getFormData().trim().equalsIgnoreCase("N")) {%>
													Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>


											</select>
											<%
					} 
					%>


											<%	}	else{%>



											<input type='text'
												<%if((sv.answer).getColor()!=null){
			 %>
												style='background-color: #FF0000;' <%}%>
												maxLength='<%=sv.answer.getLength()%>'
												value='<%= sv.answer.getFormData() %>'
												size='<%=sv.answer.getLength()%>' onFocus='doFocus(this)'
												onHelp='return fieldHelp(s6765screensfl.answer)'
												onKeyUp='return checkMaxLength(this)'
												name='<%="s6765screensfl" + "." +
						 "answer" + "_R" + count %>'
												id='<%="s6765screensfl" + "." +
						 "answer" + "_R" + count %>'
												class="input_cell"
												style="width: <%=sv.answer.getLength()*12%>px;"
												<%-- ILIFE-1702 STARTS BY SLAKKALA --%>
						  						  <%
							if ((new Byte((sv.answer).getEnabled())).compareTo(new Byte(
							BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())) {
							%>
												readonly="true" class="output_cell">
											<%
							} else if ((new Byte((sv.answer).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
											class="bold_cell"

											<%
							}
						 %>
											<%-- ILIFE-1702 ENDS --%>


											<%}%>


											<%}%>
										
									</td>
								</tr>
								<tr id='<%="tablerow"+count%>' style="height: 25px;font-weight: bold; ">
									<td
										<%if((sv.question).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>>
								
								<%} %>
						
								<%-- fix ILIFE-2409 by fwang3 --%>
								<%if(!"".equals(sv.question.getFormData().trim())){ %>
								
								
										<%if((new Byte((sv.question).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%= sv.question.getFormData()%>



										<%}%>
										
									
										
										
									
								
								
								
								
								
								<%}%>
								<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		count = count + 1;
		S6765screensfl.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	
	
	
	
	
	
	
	
							</tbody>
						</table>
						
	                 </div>
	             </div>
	         </div>
	     </div>
	     
	     
	     
	     
	<script>

	
	
	var feildarray = [];
	var count=0;
	$( "#dataTables-s6765 > tbody > tr" ).each(function() {
		 feildarray[count] = $('> td:nth-child(2)', this).html();
		count++;
		});
	

	
	  feildarray[0] = $("#dataTables-s6765 > tbody > tr:nth-child(1) > td").html();
	console.log(feildarray[0]);
	count=-1;
	$("#dataTables-s6765 > tbody > tr:nth-child(1) > td").html("");
	$("#dataTables-s6765 > tbody > tr:nth-child(1)").hide();
	
	$( "#dataTables-s6765 > tbody > tr" ).each(function() {
		if(count >=0)
		{
			if($('> td:nth-child(2)', this).length)
			{
		 	$('> td:nth-child(2)', this).html(feildarray[count]);
		 	console.log(this);
			}
			else
			{
				$(this).append("<td>"+feildarray[count]+"</td>");
			 	console.log("hello");
			}
		}
		count++;
			 
		});
	
	$(document).ready(function() {
		$('#dataTables-s6765').DataTable({
			ordering : false,
			searching : false,
			scrollY : "450px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false,
	        
		});
	});
	

	
	
	
	
</script>      

     
	     
	     
     </div>
</div>




	


<%@ include file="/POLACommon2NEW.jsp"%>
