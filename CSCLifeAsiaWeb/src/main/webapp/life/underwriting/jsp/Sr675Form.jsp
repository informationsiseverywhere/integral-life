<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR675";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%Sr675ScreenVars sv = (Sr675ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                              <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-2">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>
</div>
<div class="row">
	<div class="col-md-3">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
	</div>
<div class="row">
<div class="col-md-2">

<%StringData SR675_questionaier_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Questionaire Set");%>
<%=smartHF.getLit(0, 0, SR675_questionaier_LBL).replace("absolute","relative")%>

</div>
</div>
&nbsp;&nbsp;&nbsp;
<div class="row">
<div class="col-md-2" style="max-width:110px;text-align: center;">
<%StringData SR675_Set_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Set");%>
<%=smartHF.getLit(0, 0, SR675_Set_LBL).replace("absolute","relative")%>
</div>
<div class="col-md-1"> 
<b><%=resourceBundleHandler.gettingValueFromBundle("Age")%></b>
</div>	
<div class="col-md-1"></div>	
<div class="col-md-1"> 
<b><%=resourceBundleHandler.gettingValueFromBundle("Male")%></b>
</div>
<div class="col-md-3"></div>
<div class="col-md-1"> 
<b><%=resourceBundleHandler.gettingValueFromBundle("Female")%></b>
</div></div>
<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE01_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"1");%>
<%=smartHF.getLit(0, 0, AGE01_LBL).replace("absolute","relative")%>			 
		 		</div>
		 		</div>
		 		
		 		<div class="col-md-1"> 
				<div class="form-group">
		 		<%if(((BaseScreenData)sv.age01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age01,( sv.age01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset01");
	optionValue = makeDropDownList( mappedItems , sv.questset01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset01).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset01' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset01).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset01).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset01).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>
	</div>
	</div>
	
	<div class="col-md-2"> 
				<div class="form-group">
	<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset02");
	optionValue = makeDropDownList( mappedItems , sv.questset02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset02).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset02' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset02).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset02).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset02).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>
	</div>
	</div>	
		 		</div>
<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE02_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"2");%>
<%=smartHF.getLit(0, 0, AGE02_LBL).replace("absolute","relative")%>			 
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">
		 		<%if(((BaseScreenData)sv.age02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age02,( sv.age02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div></div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset03");
	optionValue = makeDropDownList( mappedItems , sv.questset03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset03).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset03' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset03).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset03).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset03).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>		
		</div></div>
		
<div class="col-md-2"> 
				<div class="form-group">
	<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset04");
	optionValue = makeDropDownList( mappedItems , sv.questset04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset04).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset04' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset04).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset04).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset04).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	</div>
	<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE03_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"3");%>
<%=smartHF.getLit(0, 0, AGE03_LBL).replace("absolute","relative")%>		 
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">	
		 		<%if(((BaseScreenData)sv.age03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age03,( sv.age03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset05");
	optionValue = makeDropDownList( mappedItems , sv.questset05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset05).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset05' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset05).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset05).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset05).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
		
<div class="col-md-1"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset06");
	optionValue = makeDropDownList( mappedItems , sv.questset06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset06).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset06' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset06).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset06).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset06).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	</div>
	<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE04_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"4");%>
<%=smartHF.getLit(0, 0, AGE04_LBL).replace("absolute","relative")%>	 
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">	
		 		<%if(((BaseScreenData)sv.age04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age04,( sv.age04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset07");
	optionValue = makeDropDownList( mappedItems , sv.questset07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset07).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset07' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset07).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset07).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset07).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>

	<div class="col-md-1"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset08");
	optionValue = makeDropDownList( mappedItems , sv.questset08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset08).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset08' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset08).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset08).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset08).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	</div>
	<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE05_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"5");%>
<%=smartHF.getLit(0, 0, AGE05_LBL).replace("absolute","relative")%>
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">	
		 		<%if(((BaseScreenData)sv.age05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age05,( sv.age05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset09");
	optionValue = makeDropDownList( mappedItems , sv.questset09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
  <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset09).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset09' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset09).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset09).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset09).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	
<div class="col-md-1"> 
				<div class="form-group">
			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset10");
	optionValue = makeDropDownList( mappedItems , sv.questset10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset10).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset10' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset10).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset10).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset10).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	</div>
<div class="row">	
	            	
		      	<div class="col-md-1" style="min-width:110px;text-align: center;"> 
				<div class="form-group">
						<%StringData AGE06_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"6");%>
<%=smartHF.getLit(0, 0, AGE06_LBL).replace("absolute","relative")%>
		 		</div>
		 		</div>
		 		<div class="col-md-1"> 
				<div class="form-group">	
		 		<%if(((BaseScreenData)sv.age06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.age06,( sv.age06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.age06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.age06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-4"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset11");
	optionValue = makeDropDownList( mappedItems , sv.questset11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset11).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset11' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset11).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset11).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset11).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>

<div class="col-md-1"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"questset12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("questset12");
	optionValue = makeDropDownList( mappedItems , sv.questset12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.questset12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.questset12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.questset12).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='questset12' type='list' style="width:270px;"  
					<% 
				if((new Byte((sv.questset12).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.questset12).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.questset12).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>
	</div>
<div class="row">

<div class="col-md-1"> 
				<div class="form-group">
				<%StringData BMIBASIS_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"BMI Basis");%>
<%=smartHF.getLit(0, 0, BMIBASIS_LBL).replace("absolute","relative").replace("size='58'","size='30'")%>
</div></div></div>
<div class="row">
<div class="col-md-2"> 
				<div class="form-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bmibasis"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bmibasis");
	String lanformap =fw.getAppVars().getLanguageCode();
	java.util.Set<String> set =  mappedItems.keySet();
	Map newmappedItems = new HashMap<String,String>();
	for(String key:set){
		if(key.indexOf(lanformap)==0){
			newmappedItems.put(key.substring(1), mappedItems.get(key));
		}
	}
	optionValue = makeDropDownList( newmappedItems , sv.bmibasis.getFormData(),2,resourceBundleHandler);  
	longValue = (String) newmappedItems.get((sv.bmibasis.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bmibasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
  <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.bmibasis).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='bmibasis' type='list' style="width:220px;"   
					<% 
				if((new Byte((sv.bmibasis).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.bmibasis).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.bmibasis).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
	</div>
	</div>	
	</div>					
	</div>
	</div>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
<!-- ILIFE-2430 Life Cross Browser - Sprint 1 D4: Task 4   starts-->
<script>
$(document).ready(function(){
	createDropdownNotInTable("questset01",15);
	createDropdownNotInTable("questset02",15);
	createDropdownNotInTable("questset03",15);
	createDropdownNotInTable("questset04",15);
	createDropdownNotInTable("questset05",15);
	createDropdownNotInTable("questset06",15);
	createDropdownNotInTable("questset07",15);
	createDropdownNotInTable("questset08",15);
	createDropdownNotInTable("questset09",15);
	createDropdownNotInTable("questset10",15);
	createDropdownNotInTable("questset11",15);
	createDropdownNotInTable("questset12",15);
	
});

</script>
<!-- ILIFE-2430 Life Cross Browser - Sprint 1 D4: Task 4   ends-->

