<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6775";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>

<%S6775ScreenVars sv = (S6775ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rule");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rule");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item  ");%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">

		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div></div>

  <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">


  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div></div></div>

  <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group"  style="max-width:100px;">

		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div></div></div></div>
	

<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from" )%></label>
					    		  	<table>
			    	   <tr>
			    	       
			    	     <td>

  			<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

  </td>

<td>&nbsp;</td>

                        
                        <td><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>

<td>&nbsp;</td>

                      
                        <td>



	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	  </td>
                      </tr>
                   </table>		</div></div></div>

<table>

<tr style='height:22px;'><td width='85px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("From")%>
</div>
</td>

<td width='85px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("To")%>
</div>
</td>

<td width='160px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rule")%>
</div>
</td>

<td width='50px'></td>

<td width='85px'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("From")%>
</div>
</td>



<td width='85px'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("To")%>
</div>
</td>

<td width='60px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rule")%>
</div>
</td></tr>

<%-- split 5 first rows to another file --%>
<jsp:include page="S6775FormPart.jsp"></jsp:include>

<tr style='height:22px;'>
<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom06) %>'
	 <%}%>

size='<%= sv.intgfrom06.getLength()%>'
maxLength='<%= sv.intgfrom06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom06).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto06' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto06) %>'
	 <%}%>

size='<%= sv.intgto06.getLength()%>'
maxLength='<%= sv.intgto06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto06).getColor()== null  ? 
			"input_cell" :  (sv.intgto06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule06");
	optionValue = makeDropDownList( mappedItems , sv.undwrule06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule06' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>

<td width='85px'>

	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom18).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom18' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom18) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom18);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom18) %>'
	 <%}%>

size='<%= sv.intgfrom18.getLength()%>'
maxLength='<%= sv.intgfrom18.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom18)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom18).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom18).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto18).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto18' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto18) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto18);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto18) %>'
	 <%}%>

size='<%= sv.intgto18.getLength()%>'
maxLength='<%= sv.intgto18.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto18)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto18).getColor()== null  ? 
			"input_cell" :  (sv.intgto18).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule18"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule18");
	optionValue = makeDropDownList( mappedItems , sv.undwrule18.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule18.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule18).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule18' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule18).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom07) %>'
	 <%}%>

size='<%= sv.intgfrom07.getLength()%>'
maxLength='<%= sv.intgfrom07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom07).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto07' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto07) %>'
	 <%}%>

size='<%= sv.intgto07.getLength()%>'
maxLength='<%= sv.intgto07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto07)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto07).getColor()== null  ? 
			"input_cell" :  (sv.intgto07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule07");
	optionValue = makeDropDownList( mappedItems , sv.undwrule07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule07' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom19).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom19' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom19) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom19);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom19) %>'
	 <%}%>

size='<%= sv.intgfrom19.getLength()%>'
maxLength='<%= sv.intgfrom19.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom19)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom19).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom19).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>

<td width='85px'>




	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto19).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto19' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto19) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto19);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto19) %>'
	 <%}%>

size='<%= sv.intgto19.getLength()%>'
maxLength='<%= sv.intgto19.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto19)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto19).getColor()== null  ? 
			"input_cell" :  (sv.intgto19).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule19"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule19");
	optionValue = makeDropDownList( mappedItems , sv.undwrule19.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule19.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule19).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule19' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule19).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom08) %>'
	 <%}%>

size='<%= sv.intgfrom08.getLength()%>'
maxLength='<%= sv.intgfrom08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom08).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto08' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto08) %>'
	 <%}%>

size='<%= sv.intgto08.getLength()%>'
maxLength='<%= sv.intgto08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto08)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto08).getColor()== null  ? 
			"input_cell" :  (sv.intgto08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule08");
	optionValue = makeDropDownList( mappedItems , sv.undwrule08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule08' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom20).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom20' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom20) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom20);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom20) %>'
	 <%}%>

size='<%= sv.intgfrom20.getLength()%>'
maxLength='<%= sv.intgfrom20.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom20)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom20).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom20).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>



<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto20).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto20' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto20) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto20);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto20) %>'
	 <%}%>

size='<%= sv.intgto20.getLength()%>'
maxLength='<%= sv.intgto20.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto20)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto20).getColor()== null  ? 
			"input_cell" :  (sv.intgto20).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule20"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule20");
	optionValue = makeDropDownList( mappedItems , sv.undwrule20.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule20.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule20).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule20' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule20).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom09) %>'
	 <%}%>

size='<%= sv.intgfrom09.getLength()%>'
maxLength='<%= sv.intgfrom09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom09).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto09' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto09) %>'
	 <%}%>

size='<%= sv.intgto09.getLength()%>'
maxLength='<%= sv.intgto09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto09)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto09).getColor()== null  ? 
			"input_cell" :  (sv.intgto09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule09");
	optionValue = makeDropDownList( mappedItems , sv.undwrule09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule09' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>

<td width='85px'>

	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom21).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom21' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom21) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom21);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom21) %>'
	 <%}%>

size='<%= sv.intgfrom21.getLength()%>'
maxLength='<%= sv.intgfrom21.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom21)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom21).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom21).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>



<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto21).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto21' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto21) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto21);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto21) %>'
	 <%}%>

size='<%= sv.intgto21.getLength()%>'
maxLength='<%= sv.intgto21.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto21)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto21).getColor()== null  ? 
			"input_cell" :  (sv.intgto21).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule21"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule21");
	optionValue = makeDropDownList( mappedItems , sv.undwrule21.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule21.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule21).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule21' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule21).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom10) %>'
	 <%}%>

size='<%= sv.intgfrom10.getLength()%>'
maxLength='<%= sv.intgfrom10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom10).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto10' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto10) %>'
	 <%}%>

size='<%= sv.intgto10.getLength()%>'
maxLength='<%= sv.intgto10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto10)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto10).getColor()== null  ? 
			"input_cell" :  (sv.intgto10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule10");
	optionValue = makeDropDownList( mappedItems , sv.undwrule10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule10' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>

<td width='85px'>

	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom22).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom22' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom22) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom22);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom22) %>'
	 <%}%>

size='<%= sv.intgfrom22.getLength()%>'
maxLength='<%= sv.intgfrom22.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom22)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom22).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom22).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto22).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto22' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto22) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto22);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto22) %>'
	 <%}%>

size='<%= sv.intgto22.getLength()%>'
maxLength='<%= sv.intgto22.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto22)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto22).getColor()== null  ? 
			"input_cell" :  (sv.intgto22).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule22"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule22");
	optionValue = makeDropDownList( mappedItems , sv.undwrule22.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule22.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule22).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule22' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule22).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr height="10px"></tr>
<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom11' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom11) %>'
	 <%}%>

size='<%= sv.intgfrom11.getLength()%>'
maxLength='<%= sv.intgfrom11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom11)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom11).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto11' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto11) %>'
	 <%}%>

size='<%= sv.intgto11.getLength()%>'
maxLength='<%= sv.intgto11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto11)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto11).getColor()== null  ? 
			"input_cell" :  (sv.intgto11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule11");
	optionValue = makeDropDownList( mappedItems , sv.undwrule11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule11' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>

<td width='85px'>

	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom23).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom23' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom23) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom23);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom23) %>'
	 <%}%>

size='<%= sv.intgfrom23.getLength()%>'
maxLength='<%= sv.intgfrom23.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom23)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom23).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom23).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>

<td width='85px'>




	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto23).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto23' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto23) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto23);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto23) %>'
	 <%}%>

size='<%= sv.intgto23.getLength()%>'
maxLength='<%= sv.intgto23.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto23)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto23).getColor()== null  ? 
			"input_cell" :  (sv.intgto23).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule23"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule23");
	optionValue = makeDropDownList( mappedItems , sv.undwrule23.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule23.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule23).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule23' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule23).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>
<tr height="10px"></tr>

<tr style='height:22px;'><td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom12' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom12) %>'
	 <%}%>

size='<%= sv.intgfrom12.getLength()%>'
maxLength='<%= sv.intgfrom12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom12)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom12).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='85px'>



	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto12' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto12) %>'
	 <%}%>

size='<%= sv.intgto12.getLength()%>'
maxLength='<%= sv.intgto12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto12)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto12).getColor()== null  ? 
			"input_cell" :  (sv.intgto12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule12");
	optionValue = makeDropDownList( mappedItems , sv.undwrule12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule12' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>


<td></td>
<td width='85px'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intgfrom24).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgfrom24' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom24) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgfrom24);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgfrom24) %>'
	 <%}%>

size='<%= sv.intgfrom24.getLength()%>'
maxLength='<%= sv.intgfrom24.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgfrom24)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgfrom24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgfrom24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgfrom24).getColor()== null  ? 
			"input_cell" :  (sv.intgfrom24).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>

<td width='85px'>




	<%	
			qpsf = fw.getFieldXMLDef((sv.intgto24).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='intgto24' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intgto24) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intgto24);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intgto24) %>'
	 <%}%>

size='<%= sv.intgto24.getLength()%>'
maxLength='<%= sv.intgto24.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intgto24)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intgto24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intgto24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intgto24).getColor()== null  ? 
			"input_cell" :  (sv.intgto24).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'style="max-width:70px;"
 
<%
	} 
%>
>
</td>


<td width='160px'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule24"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("undwrule24");
	optionValue = makeDropDownList( mappedItems , sv.undwrule24.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.undwrule24.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.undwrule24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:250px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.undwrule24).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:160px;"> 
<%
} 
%>

<select name='undwrule24' type='list' style="width:250px;"
<% 
	if((new Byte((sv.undwrule24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.undwrule24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.undwrule24).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr></table><br/><br/>
<table>
<tr style='height:22px;'><td></td><td></td><td width='140px'>


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%>
</div>
</td>
<td width='40px'></td>
<td width='140px'>

<input name='cont' 
type='text'

<%

		formatValue = (sv.cont.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.cont.getLength()%>'
maxLength='<%= sv.cont.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(cont)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.cont).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.cont).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.cont).getColor()== null  ? 
			"input_cell" :  (sv.cont).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table><br/></div></div>


<%@ include file="/POLACommon2NEW.jsp"%>