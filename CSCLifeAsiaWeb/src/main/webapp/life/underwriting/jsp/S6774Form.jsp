
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6774";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*" %>
<%S6774ScreenVars sv = (S6774ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                          ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Underwriting Complete? ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"    ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                          ");%>






<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					
					<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"lifcnum"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("lifcnum");
							longValue = (String) mappedItems.get((sv.lifcnum.getFormData()).toString().trim());  
						%>
						
						
						   <div class='<%= ((sv.lifcnum.getFormData() == null) || ("".equals((sv.lifcnum.getFormData()).trim()))) ? 
												"blank_cell" : "output_cell" %>'  style="max-width:100px;" > 
						   <%=	(sv.lifcnum.getFormData()).toString()%>
						   </div>
							 
						   <% /* start ILIFE-910 Screen S6774 is not showing the client’s name next to the client id*/ %>
						   <!--div class='<%--= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" --%>'-->  
								<%--if(longValue != null){--%>
								
								<%--=longValue--%>
								
								<%--}--%>
						   <!--/div-->
						   <% /* end ILIFE-910*/ %>
							<%
							longValue = null;
							formatValue = null;
							%>
					   
					  <%}%>
				</div></div>
					<div class="col-md-2"></div>
	<div class="col-md-4">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Underwriting Complete?")%></label>
<div class="input-group" style="width:100px">
<%	
	if ((new Byte((sv.undwflag).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.undwflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
	}
	if(((sv.undwflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("No");
	}
		 
%>

<% 
	if((new Byte((sv.undwflag).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<input class="form-control" type="text" placeholder=<%=longValue%> disabled>
<%
											longValue = null;
											%>
<% }else { %>
								<select name='undwflag' id="undwflag"	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(undwflag)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.undwflag).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="Y"<% if(((sv.undwflag.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(((sv.undwflag.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
</select>
							<%}%>	
							<%}%>
</div>
</div>			
				
				
			</div></div>
<!-- 		</div>
</div> -->

<div style='visibility:hidden;'>
<!-- <div class="panel panel-default">
<div class="panel-heading"</div>
<div class="body"> -->
	<div class="row">
		<div class="col-md-3">
			<%if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								
<input name='select' id='select' type='text'

<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.select.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.select.getLength()%>'
maxLength='<%= sv.select.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(select)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.select).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
<%
	}
%>
/>
<%}%>
		</div>
		</div>

<div class="col-md-3">
	<div class="form-group">
	<%if ((new Byte((generatedText0).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
	<div class="form-group">  
	<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">  
	<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
</div>
	</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">  
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
</div>
</div>
<div class="col-md-3">
<div class="form-group">  
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3">
<div class="form-group">  
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
</div>
</div>
<div class="col-md-3">
<div class="form-group">  
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label><%=resourceBundleHandler.gettingValueFromBundle("")%></label>
<%}%>
</div>
</div>
</div>

	<!-- </div> -->
</div>
</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- #ILIFE-2143 Cross Browser by fwang3-->	
<%-- <%
if(browerVersion.equals(Firefox) || browerVersion.equals(IE11)||browerVersion.equals(Chrome)){ 
%>
	<style>
		table{border-spacing:1px 4.3px !important}
		tr{height:38px !important}
	</style>
<%}%> --%>