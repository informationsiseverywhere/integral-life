	<%@ page language="java" pageEncoding="UTF-8" %>
	<%@ page contentType="text/html; charset=UTF-8" %>
	<%String screenName = "S6768FormPart";%>
	<%@ page import="com.csc.life.underwriting.screens.*"%>
	
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.csc.life.underwriting.screens.S6768ScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatterNew" %>
<%@page import="com.properties.PropertyLoader"%>

<%-- <%@page import="com.quipoz.framework.util.AppVars" %> --%>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@ page import="java.util.*" %>  
<%@page import="com.resource.ResourceBundleHandler"%>
<%@page import="com.properties.PropertyLoader"%>
<%@ page import="com.csc.fsu.general.screens.*" %>
<%@page import="com.quipoz.framework.util.*" %>
<%@page import="com.quipoz.framework.screenmodel.*" %>
<%@page import="com.quipoz.framework.datatype.*" %>
<%@page import="com.quipoz.COBOLFramework.util.*" %>
		
		
<%-- Start preparing data for ILIFE-8373 --%>
<%
QPScreenField qpsf = null; 
DataModel sm = null;
String formatValue = null;
String valueThis = null;
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;
BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();


LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();
LifeAsiaAppVars appVars = av;
av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatterNew smartHF = new SMARTHTMLFormatterNew(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(), lang);

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%S6768ScreenVars sv = (S6768ScreenVars) fw.getVariables();%>	

<%-- Prepare data--%>	

<%!
public String makeDropDownList(Map mp, Object val, int i) {

	
	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = "Select";
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
 	
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}
%>

		<%sv.undage01.setClassString("");%>
		<%sv.undsa01.setClassString("");%>
		<%sv.undsa02.setClassString("");%>
		<%sv.undsa03.setClassString("");%>
		<%sv.undsa04.setClassString("");%>
		<%sv.undsa05.setClassString("");%>
		<%sv.undage02.setClassString("");%>
		<%sv.undage03.setClassString("");%>
		<%sv.undage04.setClassString("");%>
		<%sv.undage05.setClassString("");%>
		<%sv.undage06.setClassString("");%>
		<%sv.undage07.setClassString("");%>
		<%sv.undage08.setClassString("");%>
		<%sv.undage09.setClassString("");%>
		<%sv.undage10.setClassString("");%>
			
		
		
		<% {	
			appVars.rolldown();
			appVars.rollup();
		if (appVars.ind12.isOn()) {
	sv.undage01.setReverse(BaseScreenData.REVERSED);
	sv.undage01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
	sv.undage01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
	sv.undsa01.setReverse(BaseScreenData.REVERSED);
	sv.undsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
	sv.undsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
	sv.undsa02.setReverse(BaseScreenData.REVERSED);
	sv.undsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
	sv.undsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
	sv.undsa03.setReverse(BaseScreenData.REVERSED);
	sv.undsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
	sv.undsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
	sv.undsa04.setReverse(BaseScreenData.REVERSED);
	sv.undsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
	sv.undsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
	sv.undsa05.setReverse(BaseScreenData.REVERSED);
	sv.undsa05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
	sv.undsa05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
	sv.undage02.setReverse(BaseScreenData.REVERSED);
	sv.undage02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
	sv.undage02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
	sv.undage03.setReverse(BaseScreenData.REVERSED);
	sv.undage03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
	sv.undage03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
	sv.undage04.setReverse(BaseScreenData.REVERSED);
	sv.undage04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
	sv.undage04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
	sv.undage05.setReverse(BaseScreenData.REVERSED);
	sv.undage05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
	sv.undage05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
	sv.undage06.setReverse(BaseScreenData.REVERSED);
	sv.undage06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
	sv.undage06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
	sv.undage07.setReverse(BaseScreenData.REVERSED);
	sv.undage07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
	sv.undage07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
	sv.undage08.setReverse(BaseScreenData.REVERSED);
	sv.undage08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
	sv.undage08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
	sv.undage09.setReverse(BaseScreenData.REVERSED);
	sv.undage09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
	sv.undage09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
	sv.undage10.setReverse(BaseScreenData.REVERSED);
	sv.undage10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
	sv.undage10.setHighLight(BaseScreenData.BOLD);
		}
	}
		
		%>
		
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%  
					    qpsf = fw.getFieldXMLDef((sv.undage09).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage09' type='text'
						<%if((sv.undage09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage09)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage09)%>' <%}%>
						size='<%=sv.undage09.getLength()%>'
						maxLength='<%=sv.undage09.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage09)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage09).getColor()== null  ? 
			"input_cell" :  (sv.undage09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule41"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule41");
						optionValue = makeDropDownList( mappedItems , sv.undwrule41.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule41.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule41).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule41).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule41' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule41).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule41).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule41).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule42"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule42");
						optionValue = makeDropDownList( mappedItems , sv.undwrule42.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule42.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule42).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule42).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule42' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule42).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule42).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule42).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule43"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule43");
						optionValue = makeDropDownList( mappedItems , sv.undwrule43.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule43.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule43).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule43).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule43' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule43).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule43).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule43).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule44"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule44");
						optionValue = makeDropDownList( mappedItems , sv.undwrule44.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule44.getFormData()).toString().trim());
					%>

					<%
						if((new Byte((sv.undwrule44).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule44).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule44' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule44).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule44).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule44).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule45"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule45");
						optionValue = makeDropDownList( mappedItems , sv.undwrule45.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule45.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule45).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule45).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule45' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule45).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule45).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule45).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div> 
				</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage10).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage10' type='text'
						<%if((sv.undage10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage10)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage10)%>' <%}%>
						size='<%=sv.undage10.getLength()%>'
						maxLength='<%=sv.undage10.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage10)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage10).getColor()== null  ? 
			"input_cell" :  (sv.undage10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule46"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule46");
						optionValue = makeDropDownList( mappedItems , sv.undwrule46.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule46.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule46).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule46).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule46' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule46).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule46).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule46).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule47"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule47");
						optionValue = makeDropDownList( mappedItems , sv.undwrule47.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule47.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule47).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule47).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule47' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule47).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule47).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule47).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule48"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule48");
						optionValue = makeDropDownList( mappedItems , sv.undwrule48.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule48.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule48).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule48).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule48' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule48).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule48).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule48).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule49"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule49");
						optionValue = makeDropDownList( mappedItems , sv.undwrule49.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule49.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule49).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule49).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule49' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule49).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule49).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule49).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	
				</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule50"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule50");
						optionValue = makeDropDownList( mappedItems , sv.undwrule50.getFormData(),2);  
						longValue = (String) mappedItems.get((sv.undwrule50.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule50).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=longValue%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule50).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule50' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule50).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule50).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule50).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div> 
				</div> 
				</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group"></div>
			</div> 
			</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label>Age Continuation</label>
				</div>	
				</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Sum Assured Continuation></label>
				</div>	
				</div>	
				</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<input name='agecont' type='text'
						<%if((sv.agecont).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						<%formatValue = (sv.agecont.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.agecont.getLength()%>'
						maxLength='<%=sv.agecont.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(agecont)'
						onKeyUp='return checkMaxLength(this)'
						<%if((new Byte((sv.agecont).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.agecont).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.agecont).getColor()== null  ? 
			"input_cell" :  (sv.agecont).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-3">
				<div class="form-group">
					<input name='sacont' type='text'
						<%if((sv.sacont).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						<%formatValue = (sv.sacont.getFormData()).toString();%>
						value='<%=formatValue%>'
						<%if(formatValue!=null && formatValue.trim().length()>0) {%>
						title='<%=formatValue%>' <%}%> size='<%=sv.sacont.getLength()%>'
						maxLength='<%=sv.sacont.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(sacont)'
						onKeyUp='return checkMaxLength(this)'
						<%if((new Byte((sv.sacont).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.sacont).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.sacont).getColor()== null  ? 
			"input_cell" :  (sv.sacont).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	
				</div>	
				</div>
			  