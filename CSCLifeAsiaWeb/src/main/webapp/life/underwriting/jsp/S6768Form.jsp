<%-- Start preparing data for ILIFE-8373 --%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "S6768";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.underwriting.screens.*"%>
<%S6768ScreenVars sv = (S6768ScreenVars) fw.getVariables();%>
<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," Up");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Up to Sum Assured Amount");%>
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age Continuation ");%>
<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured Continuation ");%>
<%
	{
		if (appVars.ind12.isOn()) {
	sv.undage01.setReverse(BaseScreenData.REVERSED);
	sv.undage01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
	sv.undage01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
	sv.undsa01.setReverse(BaseScreenData.REVERSED);
	sv.undsa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
	sv.undsa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
	sv.undsa02.setReverse(BaseScreenData.REVERSED);
	sv.undsa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
	sv.undsa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
	sv.undsa03.setReverse(BaseScreenData.REVERSED);
	sv.undsa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
	sv.undsa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
	sv.undsa04.setReverse(BaseScreenData.REVERSED);
	sv.undsa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
	sv.undsa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
	sv.undsa05.setReverse(BaseScreenData.REVERSED);
	sv.undsa05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
	sv.undsa05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
	sv.undage02.setReverse(BaseScreenData.REVERSED);
	sv.undage02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
	sv.undage02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
	sv.undage03.setReverse(BaseScreenData.REVERSED);
	sv.undage03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
	sv.undage03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
	sv.undage04.setReverse(BaseScreenData.REVERSED);
	sv.undage04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
	sv.undage04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
	sv.undage05.setReverse(BaseScreenData.REVERSED);
	sv.undage05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
	sv.undage05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
	sv.undage06.setReverse(BaseScreenData.REVERSED);
	sv.undage06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
	sv.undage06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
	sv.undage07.setReverse(BaseScreenData.REVERSED);
	sv.undage07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
	sv.undage07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
	sv.undage08.setReverse(BaseScreenData.REVERSED);
	sv.undage08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
	sv.undage08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
	sv.undage09.setReverse(BaseScreenData.REVERSED);
	sv.undage09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
	sv.undage09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
	sv.undage10.setReverse(BaseScreenData.REVERSED);
	sv.undage10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
	sv.undage10.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								} else  {
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.company.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								}
					%>
					<div
						class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								} else  {
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								}
					%>
					<div
						class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
				</div>	</div>
			<div class="col-md-3">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
						<%
							if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									} else  {
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.item.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									}
						%>
						<div
							class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									} else  {
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									}
						%>
						<div
							class='<%=((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>	</div>	</div>	</div>
<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	
					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
           </td>
           </tr>
		</table>
				 </div>	</div>	</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Up to Age")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Up to Sum Assured Amount")%></label>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group"></div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undsa01).getFieldName());
						valueThis=smartHF.getPicFormatted(qpsf,sv.undsa01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undsa01' type='text'
						<%if((sv.undsa01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.undsa01.getLength(), sv.undsa01.getScale(),3)%>'
						maxLength='<%=sv.undsa01.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(undsa01)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if((new Byte((sv.undsa01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undsa01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undsa01).getColor()== null  ? 
			"input_cell" :  (sv.undsa01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undsa02).getFieldName());
						valueThis=smartHF.getPicFormatted(qpsf,sv.undsa02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undsa02' type='text'
						<%if((sv.undsa02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.undsa02.getLength(), sv.undsa02.getScale(),3)%>'
						maxLength='<%=sv.undsa02.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(undsa02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if((new Byte((sv.undsa02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undsa02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undsa02).getColor()== null  ? 
			"input_cell" :  (sv.undsa02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undsa03).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis=smartHF.getPicFormatted(qpsf,sv.undsa03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undsa03' type='text'
						<%if((sv.undsa03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.undsa03.getLength(), sv.undsa03.getScale(),3)%>'
						maxLength='<%=sv.undsa03.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(undsa03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if((new Byte((sv.undsa03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undsa03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undsa03).getColor()== null  ? 
			"input_cell" :  (sv.undsa03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undsa04).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						valueThis=smartHF.getPicFormatted(qpsf,sv.undsa04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undsa04' type='text'
						<%if((sv.undsa04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.undsa04.getLength(), sv.undsa04.getScale(),3)%>'
						maxLength='<%=sv.undsa04.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(undsa04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if((new Byte((sv.undsa04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undsa04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undsa04).getColor()== null  ? 
			"input_cell" :  (sv.undsa04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undsa05).getFieldName());
						valueThis=smartHF.getPicFormatted(qpsf,sv.undsa05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undsa05' type='text'
						<%if((sv.undsa05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.undsa05.getLength(), sv.undsa05.getScale(),3)%>'
						maxLength='<%=sv.undsa05.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(undsa05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if((new Byte((sv.undsa05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undsa05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undsa05).getColor()== null  ? 
			"input_cell" :  (sv.undsa05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
		</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.undage01).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
						<input name='undage01' type='text'
							<%if((sv.undage01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf,sv.undage01)%>'
							<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							title='<%=smartHF.getPicFormatted(qpsf,sv.undage01)%>' <%}%>
							size='<%=sv.undage01.getLength()%>'
							maxLength='<%=sv.undage01.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(undage01)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if((new Byte((sv.undage01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
							readonly="true" class="output_cell"
							<%}else if((new Byte((sv.undage01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%>
							class=' <%=(sv.undage01).getColor()== null  ? 
			"input_cell" :  (sv.undage01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>	</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule01"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule01");
						optionValue = makeDropDownList( mappedItems , sv.undwrule01.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule01.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule01).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule01).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule01' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule01).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule02");
						optionValue = makeDropDownList( mappedItems , sv.undwrule02.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule02.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule02).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule02).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule02' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule02).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule03"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule03");
						optionValue = makeDropDownList( mappedItems , sv.undwrule03.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule03.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule03).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule03).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule03' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule03).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule04");
						optionValue = makeDropDownList( mappedItems , sv.undwrule04.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule04.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule04).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule04).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule04' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule04).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule05");
						optionValue = makeDropDownList( mappedItems , sv.undwrule05.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule05.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule05).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule05).getColor())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule05' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule05).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage02).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage02' type='text'
						<%if((sv.undage02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage02)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage02)%>' <%}%>
						size='<%=sv.undage02.getLength()%>'
						maxLength='<%=sv.undage02.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage02)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage02).getColor()== null  ? 
			"input_cell" :  (sv.undage02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule06"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule06");
						optionValue = makeDropDownList( mappedItems , sv.undwrule06.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule06.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule06).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule06).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule06' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule06).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule07");
						optionValue = makeDropDownList( mappedItems , sv.undwrule07.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule07.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule07).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule07).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule07' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule07).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule08");
						optionValue = makeDropDownList( mappedItems , sv.undwrule08.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule08.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule08).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule08).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule08' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule08).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule09"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule09");
						optionValue = makeDropDownList( mappedItems , sv.undwrule09.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule09.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule09).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule09).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule09' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule09).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule10"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule10");
						optionValue = makeDropDownList( mappedItems , sv.undwrule10.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule10.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule10).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule10).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule10' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule10).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage03).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage03' type='text'
						<%if((sv.undage03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage03)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage03)%>' <%}%>
						size='<%=sv.undage03.getLength()%>'
						maxLength='<%=sv.undage03.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage03)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage03).getColor()== null  ? 
			"input_cell" :  (sv.undage03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule11"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule11");
						optionValue = makeDropDownList( mappedItems , sv.undwrule11.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule11.getFormData()).toString().trim());
					%>

					<%
						if((new Byte((sv.undwrule11).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule11).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule11' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule11).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule12"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule12");
						optionValue = makeDropDownList( mappedItems , sv.undwrule12.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule12.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule12).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule12).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule12' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule12).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule13"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule13");
						optionValue = makeDropDownList( mappedItems , sv.undwrule13.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule13.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule13).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule13).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule13' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule13).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule14"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule14");
						optionValue = makeDropDownList( mappedItems , sv.undwrule14.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule14.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule14).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule14).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule14' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule14).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule15"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule15");
						optionValue = makeDropDownList( mappedItems , sv.undwrule15.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule15.getFormData()).toString().trim());
					%>

					<%
						if((new Byte((sv.undwrule15).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule15).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule15' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule15).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage04).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage04' type='text'
						<%if((sv.undage04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage04)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage04)%>' <%}%>
						size='<%=sv.undage04.getLength()%>'
						maxLength='<%=sv.undage04.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage04)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage04).getColor()== null  ? 
			"input_cell" :  (sv.undage04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule16"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule16");
						optionValue = makeDropDownList( mappedItems , sv.undwrule16.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule16.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule16).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule16).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule16' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule16).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule16).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule16).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule17"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule17");
						optionValue = makeDropDownList( mappedItems , sv.undwrule17.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule17.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule17).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule17).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule17' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule17).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule17).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule17).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule18"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule18");
						optionValue = makeDropDownList( mappedItems , sv.undwrule18.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule18.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule18).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule18).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule18' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule18).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule18).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule18).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule19"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule19");
						optionValue = makeDropDownList( mappedItems , sv.undwrule19.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule19.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule19).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule19).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule19' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule19).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule19).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule19).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule20"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule20");
						optionValue = makeDropDownList( mappedItems , sv.undwrule20.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule20.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule20).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule20).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule20' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule20).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule20).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule20).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage05).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage05' type='text'
						<%if((sv.undage05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage05)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage05)%>' <%}%>
						size='<%=sv.undage05.getLength()%>'
						maxLength='<%=sv.undage05.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage05)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage05).getColor()== null  ? 
			"input_cell" :  (sv.undage05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule21"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule21");
						optionValue = makeDropDownList( mappedItems , sv.undwrule21.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule21.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule21).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule21).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule21' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule21).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule21).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule21).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule22"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule22");
						optionValue = makeDropDownList( mappedItems , sv.undwrule22.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule22.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule22).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule22).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule22' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule22).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule22).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule22).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule23"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule23");
						optionValue = makeDropDownList( mappedItems , sv.undwrule23.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule23.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule23).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule23).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule23' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule23).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule23).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule23).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule24"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule24");
						optionValue = makeDropDownList( mappedItems , sv.undwrule24.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule24.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule24).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>

						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule24).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule24' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule24).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule24).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule24).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule25"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule25");
						optionValue = makeDropDownList( mappedItems , sv.undwrule25.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule25.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule25).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule25).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule25' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule25).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule25).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule25).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage06).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage06' type='text'
						<%if((sv.undage06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage06)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage06)%>' <%}%>
						size='<%=sv.undage06.getLength()%>'
						maxLength='<%=sv.undage06.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage06)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage06).getColor()== null  ? 
			"input_cell" :  (sv.undage06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule26"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule26");
						optionValue = makeDropDownList( mappedItems , sv.undwrule26.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule26.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule26).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule26).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule26' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule26).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule26).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule26).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
			</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule27"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule27");
						optionValue = makeDropDownList( mappedItems , sv.undwrule27.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule27.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule27).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule27).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule27' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule27).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule27).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule27).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule28"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule28");
						optionValue = makeDropDownList( mappedItems , sv.undwrule28.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule28.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule28).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule28).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule28' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule28).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule28).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule28).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule29"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule29");
						optionValue = makeDropDownList( mappedItems , sv.undwrule29.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule29.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule29).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule29).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule29' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule29).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule29).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule29).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule30"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule30");
						optionValue = makeDropDownList( mappedItems , sv.undwrule30.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule30.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule30).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule30).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule30' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule30).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule30).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule30).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage07).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage07' type='text'
						<%if((sv.undage07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage07)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage07)%>' <%}%>
						size='<%=sv.undage07.getLength()%>'
						maxLength='<%=sv.undage07.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage07)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage07).getColor()== null  ? 
			"input_cell" :  (sv.undage07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule31"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule31");
						optionValue = makeDropDownList( mappedItems , sv.undwrule31.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule31.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule31).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule31).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule31' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule31).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule31).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule31).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule32"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule32");
						optionValue = makeDropDownList( mappedItems , sv.undwrule32.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule32.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule32).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule32).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule32' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule32).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule32).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule32).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule33"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule33");
						optionValue = makeDropDownList( mappedItems , sv.undwrule33.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule33.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule33).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule33).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule33' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule33).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule33).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule33).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule34"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule34");
						optionValue = makeDropDownList( mappedItems , sv.undwrule34.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule34.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule34).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule34).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule34' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule34).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule34).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule34).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule35"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule35");
						optionValue = makeDropDownList( mappedItems , sv.undwrule35.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule35.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule35).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule35).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule35' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule35).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule35).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule35).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
		<div class="row">
			<div class="col-md-1">
				<div class="form-group">
					<%
						qpsf = fw.getFieldXMLDef((sv.undage08).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>
					<input name='undage08' type='text'
						<%if((sv.undage08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%>
						value='<%=smartHF.getPicFormatted(qpsf,sv.undage08)%>'
						<%valueThis=smartHF.getPicFormatted(qpsf,sv.undage08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						title='<%=smartHF.getPicFormatted(qpsf,sv.undage08)%>' <%}%>
						size='<%=sv.undage08.getLength()%>'
						maxLength='<%=sv.undage08.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(undage08)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if((new Byte((sv.undage08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){%>
						readonly="true" class="output_cell"
						<%}else if((new Byte((sv.undage08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
						class="bold_cell" <%}else {%>
						class=' <%=(sv.undage08).getColor()== null  ? 
			"input_cell" :  (sv.undage08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell"%>'
						<%}%>>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule36"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule36");
						optionValue = makeDropDownList( mappedItems , sv.undwrule36.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule36.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule36).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule36).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule36' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule36).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule36).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule36).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule37"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule37");
						optionValue = makeDropDownList( mappedItems , sv.undwrule37.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule37.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule37).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule37).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule37' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule37).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule37).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule37).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule38"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule38");
						optionValue = makeDropDownList( mappedItems , sv.undwrule38.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule38.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule38).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule38).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule38' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule38).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule38).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule38).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule39"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule39");
						optionValue = makeDropDownList( mappedItems , sv.undwrule39.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule39.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule39).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule39).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule39' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule39).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule39).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule39).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>
			<div class="col-md-2">
				<div class="form-group">
					<%
						fieldItem=appVars.loadF4FieldsLong(new String[] {"undwrule40"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("undwrule40");
						optionValue = makeDropDownList( mappedItems , sv.undwrule40.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.undwrule40.getFormData()).toString().trim());
					%>
					<%
						if((new Byte((sv.undwrule40).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
					%>
					<div title='<%=(longValue==null?"":longValue) %>' style='overflow:hidden; white-space:nowrap;' 
						class='<%=((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'>
						<%
							if(longValue != null){
						%>
						<%=XSSFilter.escapeHtml(longValue)%>
						<%	}%>
					</div>
					<%
						longValue = null;
					%>
					<%
						}else {
					%>
					<%
						if("red".equals((sv.undwrule40).getColor())){
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
						<%}%>
						<select name='undwrule40' type='list' style="padding-left:2px;padding-right:2px;"
							<%if((new Byte((sv.undwrule40).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
							readonly="true" disabled class="output_cell"
							<%}else if((new Byte((sv.undwrule40).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){%>
							class="bold_cell" <%}else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if("red".equals((sv.undwrule40).getColor())){
						%>
					</div>
					<%}%>
					<%}%>
				</div>	</div>	</div>
				
				
				<jsp:include page="S6768FormPart.jsp"></jsp:include>
				
		 </div> </div>
<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='100'>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("to")%>
				</div>
			</td>
			<td width='100'>
				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Age")%>
				</div>
		</tr>
	</table>
</div>
<br />
<script>
$(document).find("input[name^='undage']").each(function(){
	$(this).css("paddingRight","9px");
	
});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

