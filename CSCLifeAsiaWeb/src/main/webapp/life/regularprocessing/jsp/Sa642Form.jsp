

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sa642";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>
<%Sa642ScreenVars sv = (Sa642ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Option     ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"From Date       ");%>

<%{
	  if (appVars.ind07.isOn()) {
          sv.option.setReverse(BaseScreenData.REVERSED);
      }
      if (appVars.ind43.isOn()) {
          sv.option.setEnabled(BaseScreenData.DISABLED);
      }
      if (appVars.ind07.isOn()) {
          sv.option.setColor(BaseScreenData.RED);
      }
      if (!appVars.ind07.isOn()) {
          sv.option.setHighLight(BaseScreenData.BOLD);
      }
		if (appVars.ind02.isOn()) {
			sv.fromdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.fromdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.fromdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
  					<table><tr><td>
  <%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="schname">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
	<%					
		if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
 </td></tr></table>      
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
        <table><tr><td>
         <%					
		if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
  
	<%					
		if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
</td></tr></table>		
</div>	
</div>   

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label> 
       <div class="form-group" style="max-width:100px;">    <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>                                       
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
  <div class="input-group" style="max-width:100px;">                               <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>' style="max-width:120px;">
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
        </div>
        </div>
        </div>
        <%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label> 
        <div style="position: relative; margin-left:1px;max-width:100px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div>
	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
	
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label> 
             <div style="position: relative; margin-left:1px;max-width:120px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>                 	
	</div>                             
</div>
</div> 
 <div class="row">
 <div class="col-md-4">
	                   <div class="form-group" style="width: 170px;">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Option")%></label>  
                              
                              <% if ((new Byte((sv.option).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
                           	mappedItems = (Map) sv.getOptionMap();
							optionValue = makeDropDownList( mappedItems , sv.option.getFormData(),2,resourceBundleHandler); 
							longValue = (String) mappedItems.get((sv.option.getFormData()).toString().trim());	
							%>			        	  	
								<%=smartHF.getDropDownExt(sv.option, fw, longValue, "option", optionValue) %>
							<%} %>		
                
</div>
</div>

 <div class="col-md-4">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("From Date")%></label>  
	                   <div class="form-group" id="fromdateHide">
                 <%
								if((new Byte((sv.fromdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.fromdateDisp,(sv.fromdateDisp.getLength()))%>
							<%}else{%>
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="fromdateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.fromdateDisp,(sv.fromdateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%}%>
							
							
							</div>
							<div id="baseissurance"  class="blank_cell"  style="width:50px;display:none;">
			                </div>
</div>
</div>                                    
</div>
</div>

<script>
$(function () {	
	$('#option').change(function() {
		$("#fromdateHide").width(110);		
		if ( this.value === '0'){		 
		    $("#baseissurance").show();
		    $("#fromdateHide").hide();
		  }else{		  
			$("#fromdateHide").show();
		    $("#baseissurance").hide();
		  }
	});
});

$(document).ready(function(){
	$("#baseissurance").width(110);
	if(document.getElementById("option").value === '0'){		 
		$("#baseissurance").show();
		$("#fromdateHide").hide();
	}else{		  
		$("#fromdateHide").show();
	    $("#baseissurance").hide();
	  }
});

</script>
  
<%@ include file="/POLACommon2NEW.jsp"%>

