

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5023";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>

<%S5023ScreenVars sv = (S5023ScreenVars) fw.getVariables();%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Number ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Anniv. Bonus Date Range ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all Contracts From      ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all Contracts To        ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract number range ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all Contracts From      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To        ");%>

<%{
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5   starts  -->
	<style>
	@media \0screen\,screen\9
{
.iconPos{bottom:1px}
}
	
				 </style>
<!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5   ends -->


<div class="panel panel-default">
		
    	<div class="panel-body">
			<div class="row">  
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
<!-- <div class="input-group"> -->
<table><tr><td>



	
  		
		<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'id="schname">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
	<%					
		qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
        qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
        formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
        
        if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
                if(longValue == null || longValue.equalsIgnoreCase("")) {           
                formatValue = formatValue( formatValue );
                } else {
                formatValue = formatValue( longValue );
                }
        } else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</td></tr></table>

</div>

<!-- <br> -->
</div>


<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>



<!-- <div class="input-group"> -->
<table><tr><td>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</td><td style="padding-left:1px;">
 <%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>

</td></tr></table>
</div>
</div>
<!-- <br> -->
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>

 <div class="input-group"> 


	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div></div>
<!-- <br> -->
</div>
</div>

<div class="row">  
<div class="col-md-4">

<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>


<div class="input-group">


    
        
        <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>'>
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
        <!-- <br> -->
        </div>
        </div>
</div>

 <div class="col-md-4">
<div class="form-group">

	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>
	<!-- <div class="col-md-2"></div>
	<div class="col-md-2"> -->
	
		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label><!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5 -->
		
	<%--<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))--%>
	<div class="input-group">
<%-- 	<%=smartHF.getHTMLVarReadOnly(fw, sv.bcompany)%> --%>
<!-- 	<span style="width: 1px;"></span> -->
	<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div><!-- END TD FOR ROW 4,7 etc --> 
</div>
<div class="col-md-4">
<div class="form-group">

		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
	 
<!-- <div class="col-md-2"> -->
	<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label><!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5 -->
	<%--<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))--%>
	<div class="input-group">
<%-- 	<%=smartHF.getHTMLVarReadOnly(fw, sv.bbranch)%> --%>
<!-- 	<span style="width: 1px;"></span> -->
	<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div>
</div>
</div>









<div class="row">  
<div class="col-md-4"> 


<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Anniv. Bonus Date Range")%></label>

</div>
<div class="col-md-4"> </div>
<div class="col-md-4"> </div>
</div>
<br>
<div class="row">  
<div class="col-md-12">
 
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Extract all Contracts From")%></label>
<Table><Tr><Td>
<%	
	longValue = sv.datefrmDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datefrmDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
		%>
		<%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp, (sv.datefrmDisp.getLength()))%>
		<%
			}else {%> 

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datefrmDisp" data-link-format="dd/mm/yyyy">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
                                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                     </div> 

<%
	} %>




<%-- </Td>
<Td>
&nbsp;&nbsp;</Td>

<Td>

<label><%=resourceBundleHandler.gettingValueFromBundle("Extract all Contracts To")%></label>
<!-- <td> --><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	
</Td>

<Td>
&nbsp;&nbsp;</Td>

<Td> <!-- <td> --> --%>


</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

	<td>



<%	
	longValue = sv.datetoDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<%=smartHF.getRichTextDateInput(fw, sv.datetoDisp, (sv.datetoDisp.getLength()))%>

<%
						} else {
					%>
 
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy">
                                         <%=smartHF.getRichTextDateInput(fw, sv.datetoDisp,(sv.datetoDisp.getLength()))%>
                                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                       
                                                       
                                                       
                                                       
                                     </div> 

<%
}%>

</Td>
</Tr></Table>
</div>
</div>
</div>
<br>
<div class="row">  
<div class="col-md-4">
<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number range")%></label>
</div>
<div class="col-md-4"> </div>
<div class="col-md-4"> </div>
</div>
<br>
<div class="row">  
<div class="col-md-12">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts from")%></label>
               <table>
		<tr>
		  <td>
				<input name='chdrnum' type='text'
				
				<%if((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
				
				<%
				
						formatValue = (sv.chdrnum.getFormData()).toString();
				
				%>
					value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.chdrnum.getLength()%>'
				maxLength='<%= sv.chdrnum.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.chdrnum).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.chdrnum).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.chdrnum).getColor()== null  ? 
							"input_cell" :  (sv.chdrnum).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

	<td>
       <input name='chdrnum1' type='text'
				<%if((sv.chdrnum1).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
				
				<%
				
						formatValue = (sv.chdrnum1.getFormData()).toString();
				
				%>
					value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
				
				size='<%= sv.chdrnum1.getLength()%>'
				maxLength='<%= sv.chdrnum1.getLength()%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum1)' onKeyUp='return checkMaxLength(this)'  
				
				
				<% 
					if((new Byte((sv.chdrnum1).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.chdrnum1).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.chdrnum1).getColor()== null  ? 
							"input_cell" :  (sv.chdrnum1).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
	         </td>
           </tr>
		</table>
             </div>
	</div>
	<br>
</div></div></div>
<div style='visibility:hidden;'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Year")%>
</div>


</div>


<%@ include file="/POLACommon2NEW.jsp"%>

