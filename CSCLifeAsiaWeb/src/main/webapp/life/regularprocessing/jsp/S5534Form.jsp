<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5534";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*"%>
<%
	S5534ScreenVars sv = (S5534ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Premiums ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Calculation method ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"J/Life calc method ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Surrender method ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Frequency of unit deduction ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Processing");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "subroutine ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Administration Fees Method ");
%>

<%
	{
		if (appVars.ind06.isOn()) {
			sv.premmeth.setReverse(BaseScreenData.REVERSED);
			sv.premmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.premmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.jlPremMeth.setReverse(BaseScreenData.REVERSED);
			sv.jlPremMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.jlPremMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.svMethod.setReverse(BaseScreenData.REVERSED);
			sv.svMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.svMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.unitFreq.setReverse(BaseScreenData.REVERSED);
			sv.unitFreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.unitFreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.subprog.setReverse(BaseScreenData.REVERSED);
			sv.subprog.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.subprog.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premiums")%></label>
				</div>
			</div>
			<div class="col-md-4"></div><div class="col-md-4"></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Calculation method")%></label>
					<%
						if ((new Byte((sv.premmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getHTMLVarExt(fw, sv.premmeth, 0, 100)%>
					<%
						} else {
					%>
					<div class="input-group" style="width: 150px;">
						<input id='premmeth' name='premmeth' type='text'
							value='<%=sv.premmeth.getFormData()%>'
							maxLength='<%=sv.premmeth.getLength()%>'
							size='<%=sv.premmeth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(premmeth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.premmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.premmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('premmeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.premmeth).getColor() == null ? "input_cell"
							: (sv.premmeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('premmeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>

					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life calc method")%></label>
					<%
						if ((new Byte((sv.jlPremMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getHTMLVarExt(fw, sv.jlPremMeth, 0, 100)%>
					<%
						} else {
					%>
					<div class="input-group" style="width: 150px;">
						<input id='jlPremMeth' name='jlPremMeth' type='text'
							value='<%=sv.jlPremMeth.getFormData()%>'
							maxLength='<%=sv.jlPremMeth.getLength()%>'
							size='<%=sv.jlPremMeth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(jlPremMeth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.jlPremMeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.jlPremMeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('jlPremMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.jlPremMeth).getColor() == null ? "input_cell"
							: (sv.jlPremMeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('jlPremMeth')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>

					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Surrender method")%></label>
					<%
						if ((new Byte((sv.svMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getHTMLVarExt(fw, sv.svMethod, 0, 100)%>
					<%
						} else {
					%>
					<div class="input-group" style="width: 150px;">
						<input id='svMethod' name='svMethod' type='text'
							value='<%=sv.svMethod.getFormData()%>'
							maxLength='<%=sv.svMethod.getLength()%>'
							size='<%=sv.svMethod.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(svMethod)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.svMethod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.svMethod).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('svMethod')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.svMethod).getColor() == null ? "input_cell"
							: (sv.svMethod).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('svMethod')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>

					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency of unit deduction")%></label>
					<%
						if ((new Byte((sv.unitFreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getHTMLVarExt(fw, sv.unitFreq, 0, 100)%>
					<%
						} else {
					%>
					<div class="input-group" style="width: 150px;">
						<input id='unitFreq' name='unitFreq' type='text'
							value='<%=sv.unitFreq.getFormData()%>'
							maxLength='<%=sv.unitFreq.getLength()%>'
							size='<%=sv.unitFreq.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(unitFreq)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.unitFreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.unitFreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
								type="button"
								onClick="doFocus(document.getElementById('unitFreq')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.unitFreq).getColor() == null ? "input_cell"
							: (sv.unitFreq).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
								type="button"
								onClick="doFocus(document.getElementById('unitFreq')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>

					</div>
					<%
						}
					%>
				</div>
			</div><div class="col-md-6"></div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Administration Fees Method")%></label>
					<div style="width: 100px;">
						<input name='adfeemth' type='text'
							<%formatValue = (sv.adfeemth.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.adfeemth.getLength()%>'
							maxLength='<%=sv.adfeemth.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(adfeemth)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.adfeemth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.adfeemth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.adfeemth).getColor() == null ? "input_cell"
						: (sv.adfeemth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			<div class="col-md-6"></div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("subroutine")%></label>
					<div style="width: 100px;">
						<input name='subprog' type='text'
							<%formatValue = (sv.subprog.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.subprog.getLength()%>'
							maxLength='<%=sv.subprog.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(subprog)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.subprog).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.subprog).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.subprog).getColor() == null ? "input_cell"
						: (sv.subprog).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div><div class="col-md-6"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility: hidden;'>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Processing")%></label>
			</div>
		</div>
	</div>
</div>