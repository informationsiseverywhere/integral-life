

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5654";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>

<%S5654ScreenVars sv = (S5654ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Automatic Increase Indicator          ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Yes/No/Optional)");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CPI");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pre-Defined");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allowed");%>

<%{
		if (appVars.ind10.isOn()) {
			sv.indxflg.setReverse(BaseScreenData.REVERSED);
			sv.indxflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.indxflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.cpidef.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
			sv.cpidef.setReverse(BaseScreenData.REVERSED);
			sv.cpidef.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.cpidef.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.cpidef.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.cpiallwd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.cpiallwd.setReverse(BaseScreenData.REVERSED);
			sv.cpiallwd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind15.isOn()) {
			sv.cpiallwd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.cpiallwd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.predef.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind17.isOn()) {
			sv.predef.setReverse(BaseScreenData.REVERSED);
			sv.predef.setColor(BaseScreenData.RED);
		}
		if (appVars.ind18.isOn()) {
			sv.predef.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind17.isOn()) {
			sv.predef.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.predefallwd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.predefallwd.setReverse(BaseScreenData.REVERSED);
			sv.predefallwd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind21.isOn()) {
			sv.predefallwd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind20.isOn()) {
			sv.predefallwd.setHighLight(BaseScreenData.BOLD);
		}
		
	}

	%>


<div class="panel panel-default">
		
    	<div class="panel-body">
			<div class="row">   
			<div class="col-md-4">
<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>





	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



<br>
<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->	
	
	<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}


}
</style>
		<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->
</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>




	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<!-- <div class="input-group"> -->


<table><tr><td>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>

<!-- </div> --></div></div>
<div class="row">

<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=width:90px;>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
</div>
<br>
<div class="row">
<div class="col-md-4">



<%
ScreenDataHelper autoIndicator = new ScreenDataHelper(fw.getVariables(), sv.indxflg);
%>
<td style="visibility:<%=autoIndicator.visibility()%>" width='100'>

<label><%=resourceBundleHandler.gettingValueFromBundle("Automatic Increase Indicator")%></label>


<%if (autoIndicator.isReadonly()) {%>
<input name='indxflg' style=width:50px; type='text' value='<%=autoIndicator.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)' onHelp='return fieldHelp(indxflg)'
       onKeyUp='return checkMaxLength(this)'>
<%} else {%>
<select name="indxflg" style=width:100px; type="list" class="<%=autoIndicator.clazz()%>">
<option <%=autoIndicator.selected("O")%> value="O">O</option>
  <option <%=autoIndicator.selected("Y")%> value="Y">Y</option>
  <option <%=autoIndicator.selected("N")%> value="N">N</option>
</select>
<%}%>
</td>

</div></div>
<br>
<br>
 <div class="row">
<!-- <div class="col-md-3"></div>  -->

<%if ((new Byte((sv.cpidef).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>

<!-- <table>
<tr style='height:22px;'>
<td width='251'> --><div class="col-md-4"></div>
<div class="col-md-2">
	<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Default")%>
	</label></div>
	
<!-- </td> -->

<!-- <td  width='251'> -->
<div class="col-md-3">
	<label>
		<%=resourceBundleHandler.gettingValueFromBundle("Allowed")%>
</label>
	
	</div></div>
<!-- </td>
</tr>

<tr></tr>
<tr style='height:32px;'>
<td > -->
 <div class="row">
<!--  <div class="col-md-3"></div> -->  
<div class="col-md-4">
<label>
		<%=resourceBundleHandler.gettingValueFromBundle("CPI")%>
	</label>
	</div>
<!-- </td><td width='251'> -->
<div class="col-md-2">
	<input style='margin-right:255px;' type='checkbox' name='cpidef' value='Y'  onFocus='doFocus(this)'
		onHelp='return fieldHelp(cpidef)' onKeyUp='return checkMaxLength(this)'

	<%

		if((sv.cpidef).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.cpidef).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }if((sv.cpidef).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('cpidef')"/>
<input type='checkbox' name='otherscheallowed' value=' ' 

<% if(!(sv.cpidef).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('cpidef')"/>
</div>
<!-- </td>
<td  > -->
<div class="col-md-2">
<input style='margin-right:105px'; type='checkbox' name='cpiallwd' value='Y'  onFocus='doFocus(this)'
onHelp='return fieldHelp(cpiallwd)' onKeyUp='return checkMaxLength(this)'

<%

if((sv.cpiallwd).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.cpiallwd).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }if((sv.cpiallwd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('cpiallwd')"/>
<input type='checkbox' name='cpiallwd' value=' ' 

<% if(!(sv.cpiallwd).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('cpiallwd')"/>
</div>
</div>
<!-- </td>
</tr>

<tr style='height:px;'>
<td> -->
 <div class="row">
<!-- <div class="col-md-3"></div> --> <div class="col-md-4">
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Pre-Defined")%>
</label>
</div>
<!-- </td><td width='251'> -->
<div class="col-md-2">
<input style='margin-right:255px;' type='checkbox' name='predef' value='Y'  onFocus='doFocus(this)'
onHelp='return fieldHelp(predef)' onKeyUp='return checkMaxLength(this)'

<%

if((sv.predef).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.predef).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }if((sv.predef).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('predef')"/>
<input type='checkbox' name='predef' value=' ' 

<% if(!(sv.predef).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('predef')"/>
</div>
<!-- </td>
<td  width='251'> -->
<div class="col-md-3">
<input style='margin-right:105px'; type='checkbox' name='predefallwd' value='Y'  onFocus='doFocus(this)'
onHelp='return fieldHelp(predefallwd)' onKeyUp='return checkMaxLength(this)'

<%

if((sv.predefallwd).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.predefallwd).toString().trim().equalsIgnoreCase("Y")){
			%>checked

      <% }if((sv.predefallwd).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('predefallwd')"/>
<input type='checkbox' name='predefallwd' value=' ' 

<% if(!(sv.predefallwd).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('predefallwd')"/>

<!-- </td>
</tr>
</table> -->
<!-- <div class="col-md-3"></div> --></div>
<%}%></div>
<br/>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>