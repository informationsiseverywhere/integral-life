<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5399";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>

<%S5399ScreenVars sv = (S5399ScreenVars) fw.getVariables();%>

<%if (sv.S5399screenWritten.gt(0)) {%>
	<%S5399screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
<%	generatedText6.appendClassString("label_txt");
	generatedText6.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Set");%>
<%	generatedText7.appendClassString("label_txt");
	generatedText7.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage");%>
<%	generatedText11.appendClassString("label_txt");
	generatedText11.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Set");%>
<%	generatedText14.appendClassString("label_txt");
	generatedText14.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk");%>
<%	generatedText4.appendClassString("label_txt");
	generatedText4.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
<%	generatedText8.appendClassString("label_txt");
	generatedText8.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium");%>
<%	generatedText12.appendClassString("label_txt");
	generatedText12.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
<%	generatedText15.appendClassString("label_txt");
	generatedText15.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
<%	generatedText5.appendClassString("label_txt");
	generatedText5.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk");%>
<%	generatedText9.appendClassString("label_txt");
	generatedText9.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
<%	generatedText13.appendClassString("label_txt");
	generatedText13.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium");%>
<%	generatedText16.appendClassString("label_txt");
	generatedText16.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Hierarchy");%>
<%	generatedText18.appendClassString("label_txt");
	generatedText18.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
<%	generatedText10.appendClassString("label_txt");
	generatedText10.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Hierarchy");%>
<%	generatedText19.appendClassString("label_txt");
	generatedText19.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
<%	generatedText17.appendClassString("label_txt");
	generatedText17.appendClassString("highlight");
%>
	<%sv.covRiskStat01.setClassString("");%>
<%	sv.covRiskStat01.appendClassString("string_fld");
	sv.covRiskStat01.appendClassString("input_txt");
	sv.covRiskStat01.appendClassString("highlight");
%>
	<%sv.setCnRiskStat01.setClassString("");%>
<%	sv.setCnRiskStat01.appendClassString("string_fld");
	sv.setCnRiskStat01.appendClassString("input_txt");
	sv.setCnRiskStat01.appendClassString("highlight");
%>
	<%sv.covPremStat01.setClassString("");%>
<%	sv.covPremStat01.appendClassString("string_fld");
	sv.covPremStat01.appendClassString("input_txt");
	sv.covPremStat01.appendClassString("highlight");
%>
	<%sv.setCnPremStat01.setClassString("");%>
<%	sv.setCnPremStat01.appendClassString("string_fld");
	sv.setCnPremStat01.appendClassString("input_txt");
	sv.setCnPremStat01.appendClassString("highlight");
%>
	<%sv.covRiskStat02.setClassString("");%>
<%	sv.covRiskStat02.appendClassString("string_fld");
	sv.covRiskStat02.appendClassString("input_txt");
	sv.covRiskStat02.appendClassString("highlight");
%>
	<%sv.setCnRiskStat02.setClassString("");%>
<%	sv.setCnRiskStat02.appendClassString("string_fld");
	sv.setCnRiskStat02.appendClassString("input_txt");
	sv.setCnRiskStat02.appendClassString("highlight");
%>
	<%sv.covPremStat02.setClassString("");%>
<%	sv.covPremStat02.appendClassString("string_fld");
	sv.covPremStat02.appendClassString("input_txt");
	sv.covPremStat02.appendClassString("highlight");
%>
	<%sv.setCnPremStat02.setClassString("");%>
<%	sv.setCnPremStat02.appendClassString("string_fld");
	sv.setCnPremStat02.appendClassString("input_txt");
	sv.setCnPremStat02.appendClassString("highlight");
%>
	<%sv.covRiskStat03.setClassString("");%>
<%	sv.covRiskStat03.appendClassString("string_fld");
	sv.covRiskStat03.appendClassString("input_txt");
	sv.covRiskStat03.appendClassString("highlight");
%>
	<%sv.setCnRiskStat03.setClassString("");%>
<%	sv.setCnRiskStat03.appendClassString("string_fld");
	sv.setCnRiskStat03.appendClassString("input_txt");
	sv.setCnRiskStat03.appendClassString("highlight");
%>
	<%sv.covPremStat03.setClassString("");%>
<%	sv.covPremStat03.appendClassString("string_fld");
	sv.covPremStat03.appendClassString("input_txt");
	sv.covPremStat03.appendClassString("highlight");
%>
	<%sv.setCnPremStat03.setClassString("");%>
<%	sv.setCnPremStat03.appendClassString("string_fld");
	sv.setCnPremStat03.appendClassString("input_txt");
	sv.setCnPremStat03.appendClassString("highlight");
%>
	<%sv.covRiskStat04.setClassString("");%>
<%	sv.covRiskStat04.appendClassString("string_fld");
	sv.covRiskStat04.appendClassString("input_txt");
	sv.covRiskStat04.appendClassString("highlight");
%>
	<%sv.setCnRiskStat04.setClassString("");%>
<%	sv.setCnRiskStat04.appendClassString("string_fld");
	sv.setCnRiskStat04.appendClassString("input_txt");
	sv.setCnRiskStat04.appendClassString("highlight");
%>
	<%sv.covPremStat04.setClassString("");%>
<%	sv.covPremStat04.appendClassString("string_fld");
	sv.covPremStat04.appendClassString("input_txt");
	sv.covPremStat04.appendClassString("highlight");
%>
	<%sv.setCnPremStat04.setClassString("");%>
<%	sv.setCnPremStat04.appendClassString("string_fld");
	sv.setCnPremStat04.appendClassString("input_txt");
	sv.setCnPremStat04.appendClassString("highlight");
%>
	<%sv.covRiskStat05.setClassString("");%>
<%	sv.covRiskStat05.appendClassString("string_fld");
	sv.covRiskStat05.appendClassString("input_txt");
	sv.covRiskStat05.appendClassString("highlight");
%>
	<%sv.setCnRiskStat05.setClassString("");%>
<%	sv.setCnRiskStat05.appendClassString("string_fld");
	sv.setCnRiskStat05.appendClassString("input_txt");
	sv.setCnRiskStat05.appendClassString("highlight");
%>
	<%sv.covPremStat05.setClassString("");%>
<%	sv.covPremStat05.appendClassString("string_fld");
	sv.covPremStat05.appendClassString("input_txt");
	sv.covPremStat05.appendClassString("highlight");
%>
	<%sv.setCnPremStat05.setClassString("");%>
<%	sv.setCnPremStat05.appendClassString("string_fld");
	sv.setCnPremStat05.appendClassString("input_txt");
	sv.setCnPremStat05.appendClassString("highlight");
%>
	<%sv.covRiskStat06.setClassString("");%>
<%	sv.covRiskStat06.appendClassString("string_fld");
	sv.covRiskStat06.appendClassString("input_txt");
	sv.covRiskStat06.appendClassString("highlight");
%>
	<%sv.setCnRiskStat06.setClassString("");%>
<%	sv.setCnRiskStat06.appendClassString("string_fld");
	sv.setCnRiskStat06.appendClassString("input_txt");
	sv.setCnRiskStat06.appendClassString("highlight");
%>
	<%sv.covPremStat06.setClassString("");%>
<%	sv.covPremStat06.appendClassString("string_fld");
	sv.covPremStat06.appendClassString("input_txt");
	sv.covPremStat06.appendClassString("highlight");
%>
	<%sv.setCnPremStat06.setClassString("");%>
<%	sv.setCnPremStat06.appendClassString("string_fld");
	sv.setCnPremStat06.appendClassString("input_txt");
	sv.setCnPremStat06.appendClassString("highlight");
%>
	<%sv.covRiskStat07.setClassString("");%>
<%	sv.covRiskStat07.appendClassString("string_fld");
	sv.covRiskStat07.appendClassString("input_txt");
	sv.covRiskStat07.appendClassString("highlight");
%>
	<%sv.setCnRiskStat07.setClassString("");%>
<%	sv.setCnRiskStat07.appendClassString("string_fld");
	sv.setCnRiskStat07.appendClassString("input_txt");
	sv.setCnRiskStat07.appendClassString("highlight");
%>
	<%sv.covPremStat07.setClassString("");%>
<%	sv.covPremStat07.appendClassString("string_fld");
	sv.covPremStat07.appendClassString("input_txt");
	sv.covPremStat07.appendClassString("highlight");
%>
	<%sv.setCnPremStat07.setClassString("");%>
<%	sv.setCnPremStat07.appendClassString("string_fld");
	sv.setCnPremStat07.appendClassString("input_txt");
	sv.setCnPremStat07.appendClassString("highlight");
%>
	<%sv.covRiskStat08.setClassString("");%>
<%	sv.covRiskStat08.appendClassString("string_fld");
	sv.covRiskStat08.appendClassString("input_txt");
	sv.covRiskStat08.appendClassString("highlight");
%>
	<%sv.setCnRiskStat08.setClassString("");%>
<%	sv.setCnRiskStat08.appendClassString("string_fld");
	sv.setCnRiskStat08.appendClassString("input_txt");
	sv.setCnRiskStat08.appendClassString("highlight");
%>
	<%sv.covPremStat08.setClassString("");%>
<%	sv.covPremStat08.appendClassString("string_fld");
	sv.covPremStat08.appendClassString("input_txt");
	sv.covPremStat08.appendClassString("highlight");
%>
	<%sv.setCnPremStat08.setClassString("");%>
<%	sv.setCnPremStat08.appendClassString("string_fld");
	sv.setCnPremStat08.appendClassString("input_txt");
	sv.setCnPremStat08.appendClassString("highlight");
%>
	<%sv.covRiskStat09.setClassString("");%>
<%	sv.covRiskStat09.appendClassString("string_fld");
	sv.covRiskStat09.appendClassString("input_txt");
	sv.covRiskStat09.appendClassString("highlight");
%>
	<%sv.setCnRiskStat09.setClassString("");%>
<%	sv.setCnRiskStat09.appendClassString("string_fld");
	sv.setCnRiskStat09.appendClassString("input_txt");
	sv.setCnRiskStat09.appendClassString("highlight");
%>
	<%sv.covPremStat09.setClassString("");%>
<%	sv.covPremStat09.appendClassString("string_fld");
	sv.covPremStat09.appendClassString("input_txt");
	sv.covPremStat09.appendClassString("highlight");
%>
	<%sv.setCnPremStat09.setClassString("");%>
<%	sv.setCnPremStat09.appendClassString("string_fld");
	sv.setCnPremStat09.appendClassString("input_txt");
	sv.setCnPremStat09.appendClassString("highlight");
%>
	<%sv.covRiskStat10.setClassString("");%>
<%	sv.covRiskStat10.appendClassString("string_fld");
	sv.covRiskStat10.appendClassString("input_txt");
	sv.covRiskStat10.appendClassString("highlight");
%>
	<%sv.setCnRiskStat10.setClassString("");%>
<%	sv.setCnRiskStat10.appendClassString("string_fld");
	sv.setCnRiskStat10.appendClassString("input_txt");
	sv.setCnRiskStat10.appendClassString("highlight");
%>
	<%sv.covPremStat10.setClassString("");%>
<%	sv.covPremStat10.appendClassString("string_fld");
	sv.covPremStat10.appendClassString("input_txt");
	sv.covPremStat10.appendClassString("highlight");
%>
	<%sv.setCnPremStat10.setClassString("");%>
<%	sv.setCnPremStat10.appendClassString("string_fld");
	sv.setCnPremStat10.appendClassString("input_txt");
	sv.setCnPremStat10.appendClassString("highlight");
%>
	<%sv.covRiskStat11.setClassString("");%>
<%	sv.covRiskStat11.appendClassString("string_fld");
	sv.covRiskStat11.appendClassString("input_txt");
	sv.covRiskStat11.appendClassString("highlight");
%>
	<%sv.setCnRiskStat11.setClassString("");%>
<%	sv.setCnRiskStat11.appendClassString("string_fld");
	sv.setCnRiskStat11.appendClassString("input_txt");
	sv.setCnRiskStat11.appendClassString("highlight");
%>
	<%sv.covPremStat11.setClassString("");%>
<%	sv.covPremStat11.appendClassString("string_fld");
	sv.covPremStat11.appendClassString("input_txt");
	sv.covPremStat11.appendClassString("highlight");
%>
	<%sv.setCnPremStat11.setClassString("");%>
<%	sv.setCnPremStat11.appendClassString("string_fld");
	sv.setCnPremStat11.appendClassString("input_txt");
	sv.setCnPremStat11.appendClassString("highlight");
%>
	<%sv.covRiskStat12.setClassString("");%>
<%	sv.covRiskStat12.appendClassString("string_fld");
	sv.covRiskStat12.appendClassString("input_txt");
	sv.covRiskStat12.appendClassString("highlight");
%>
	<%sv.setCnRiskStat12.setClassString("");%>
<%	sv.setCnRiskStat12.appendClassString("string_fld");
	sv.setCnRiskStat12.appendClassString("input_txt");
	sv.setCnRiskStat12.appendClassString("highlight");
%>
	<%sv.covPremStat12.setClassString("");%>
<%	sv.covPremStat12.appendClassString("string_fld");
	sv.covPremStat12.appendClassString("input_txt");
	sv.covPremStat12.appendClassString("highlight");
%>
	<%sv.setCnPremStat12.setClassString("");%>
<%	sv.setCnPremStat12.appendClassString("string_fld");
	sv.setCnPremStat12.appendClassString("input_txt");
	sv.setCnPremStat12.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

<div class="panel panel-default">
		
    	<div class="panel-body">


			<div class="row">   
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>




	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>

</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>




  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">

<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<!-- <div class="input-group" style="width:46px;"> -->
	<table><tr><td>
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
	</div>

<!-- </div> --></div></div>
<br><br/>
<br><br/>
<!-- <br><br/>
<br><br/> -->

<div class="row">
<div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Set"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Set"))%></label>
      
      
      
      
      
      </div></div>
      </div>
      <div class="row">
<div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
      
      
      
      
      
      </div></div>
      </div>
         <div class="row">
<div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium"))%></label>
      
      
      
      
      
      </div></div>
      </div>
       <div class="row">
<div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Hierarchy"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Hierarchy"))%></label>
      
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
      <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Status"))%></label>
      
      
      
      
      
      </div></div>
      </div>
       <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat01)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat01)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat01)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat01)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
       <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat02)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat02)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat02)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat02)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat03)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat03)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat03)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat03)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat04)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat04)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat04)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat04)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat05)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat05)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat05)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat05)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat06)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat06)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat06)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat06)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat07)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat07)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat07)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat07)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat08)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat08)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat08)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat08)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat09)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat09)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat09)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat09)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat10)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat10)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat10)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat10)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat11)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat11)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat11)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat11)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
           <div class="row">
<div class="col-md-2">
<div class="form-group">
     
      <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covRiskStat12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covRiskStat12)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-4">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnRiskStat12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnRiskStat12)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
        <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.covPremStat12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.covPremStat12)%>
	 </div>
      
      
      
      
      </div></div>
      <div class="col-md-2">
<div class="form-group">
     
       <div class="input-group" style="width:80px;">
	<%=smartHF.getHTMLVarExt(fw, sv.setCnPremStat12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	<%=smartHF.getHTMLF4NSVarExt(fw, sv.setCnPremStat12)%>
	 </div>
      
      
      
      
      </div></div>
      </div>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
	





<%}%>

<%if (sv.S5399protectWritten.gt(0)) {%>
	<%S5399protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
</div></div>
<script>
$(document).ready(function() {
	
	$('#idesc').width('175');
	
	
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5   starts  -->
	<style>
	@media \0screen\,screen\9
	{img{margin-top:3px;}
}
	@media screen and (-webkit-min-device-pixel-ratio:0) {
img{margin-top:2px;}
}
@media all and (-ms-high-contrast:none) 
{img{margin-top:2px;}}
	
	.blank_cell{padding-right:0px;margin-left:1px;}
			 </style>
			 
<!-- ILIFE-2659 Life Cross Browser -Coding and UT- Sprint 3 D2: Task 5   ends -->

