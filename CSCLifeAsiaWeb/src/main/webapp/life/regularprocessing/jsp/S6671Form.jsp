

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6671";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>
<%S6671ScreenVars sv = (S6671ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts From      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To       ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
  					<table><tr><td>
  <%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="schname">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
	<%					
		if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleNumber.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
 </td></tr></table>      
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
        <table><tr><td>
         <%					
		if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctmonth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
  
	<%					
		if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acctyear.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
          <%  
            longValue = null;
            formatValue = null;
        %>
</td></tr></table>		
</div>	
</div>   

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label> 
       <div class="form-group" style="max-width:100px;">    <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>                                       
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
  <div class="input-group" style="max-width:100px;">                               <%                  
        if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) {     
                    
                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                            
                            
                    } else  {
                                
                    if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
                            } else {
                                formatValue = formatValue( longValue);
                            }
                    
                    }
                    %>          
                <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
                        "blank_cell" : "output_cell" %>' style="max-width:120px;">
                <%=XSSFilter.escapeHtml(formatValue)%>
            </div>  
        <%
        longValue = null;
        formatValue = null;
        %>
        </div>
        </div>
        </div>
        <%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bcompany");
		longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
	%>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label> 
        <div style="position: relative; margin-left:1px;max-width:100px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	</div>
	</div>
	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bbranch");
		longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
	%>
	
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label> 
             <div style="position: relative; margin-left:1px;max-width:120px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>                 	
	</div>                             
</div>
</div> 
 <div class="row">
 <div class="col-md-4">
	                   <div class="form-group" >
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts From")%></label>  
                  <table>
					<tr>
					<td>
					<input name='chdrnum' type='text' 

<%

		formatValue = (sv.chdrnum.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.chdrnum.getLength()%>'
maxLength='<%= sv.chdrnum.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.chdrnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.chdrnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.chdrnum).getColor()== null  ? 
			"input_cell" :  (sv.chdrnum).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>

>  
</td>
<td> &nbsp;&nbsp;  </td>
	<td><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>
	<td> &nbsp;&nbsp;  </td>
	<td>
<input name='chdrnum1' 
type='text'

<%

		formatValue = (sv.chdrnum1.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.chdrnum1.getLength()%>'
maxLength='<%= sv.chdrnum1.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum1)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.chdrnum1).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.chdrnum1).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.chdrnum1).getColor()== null  ? 
			"input_cell" :  (sv.chdrnum1).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
</tr>
</table>  
</div>
</div>
</div>                                    
</div>
</div>
  
<%@ include file="/POLACommon2NEW.jsp"%>

