
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S6635";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>
<%S6635ScreenVars sv = (S6635ScreenVars) fw.getVariables();%>

<%if (sv.S6635screenWritten.gt(0)) {%>
	<%S6635screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," Years");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Offset");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Unit");%>
	<%sv.riskunit01.setClassString("");%>
<%	sv.riskunit01.appendClassString("num_fld");
	sv.riskunit01.appendClassString("input_txt");
	sv.riskunit01.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Unit");%>
	<%sv.riskunit02.setClassString("");%>
<%	sv.riskunit02.appendClassString("num_fld");
	sv.riskunit02.appendClassString("input_txt");
	sv.riskunit02.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"in force");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"term");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Ass  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus  ");%>
	<%sv.yrsinf01.setClassString("");%>
<%	sv.yrsinf01.appendClassString("num_fld");
	sv.yrsinf01.appendClassString("input_txt");
	sv.yrsinf01.appendClassString("highlight");
%>
	<%sv.offset01.setClassString("");%>
<%	sv.offset01.appendClassString("num_fld");
	sv.offset01.appendClassString("input_txt");
	sv.offset01.appendClassString("highlight");
%>
	<%sv.sumass01.setClassString("");%>
<%	sv.sumass01.appendClassString("num_fld");
	sv.sumass01.appendClassString("input_txt");
	sv.sumass01.appendClassString("highlight");
%>
	<%sv.bonus01.setClassString("");%>
<%	sv.bonus01.appendClassString("num_fld");
	sv.bonus01.appendClassString("input_txt");
	sv.bonus01.appendClassString("highlight");
%>
	<%sv.yrsinf02.setClassString("");%>
<%	sv.yrsinf02.appendClassString("num_fld");
	sv.yrsinf02.appendClassString("input_txt");
	sv.yrsinf02.appendClassString("highlight");
%>
	<%sv.offset02.setClassString("");%>
<%	sv.offset02.appendClassString("num_fld");
	sv.offset02.appendClassString("input_txt");
	sv.offset02.appendClassString("highlight");
%>
	<%sv.sumass02.setClassString("");%>
<%	sv.sumass02.appendClassString("num_fld");
	sv.sumass02.appendClassString("input_txt");
	sv.sumass02.appendClassString("highlight");
%>
	<%sv.bonus02.setClassString("");%>
<%	sv.bonus02.appendClassString("num_fld");
	sv.bonus02.appendClassString("input_txt");
	sv.bonus02.appendClassString("highlight");
%>
	<%sv.yrsinf03.setClassString("");%>
<%	sv.yrsinf03.appendClassString("num_fld");
	sv.yrsinf03.appendClassString("input_txt");
	sv.yrsinf03.appendClassString("highlight");
%>
	<%sv.offset03.setClassString("");%>
<%	sv.offset03.appendClassString("num_fld");
	sv.offset03.appendClassString("input_txt");
	sv.offset03.appendClassString("highlight");
%>
	<%sv.sumass03.setClassString("");%>
<%	sv.sumass03.appendClassString("num_fld");
	sv.sumass03.appendClassString("input_txt");
	sv.sumass03.appendClassString("highlight");
%>
	<%sv.bonus03.setClassString("");%>
<%	sv.bonus03.appendClassString("num_fld");
	sv.bonus03.appendClassString("input_txt");
	sv.bonus03.appendClassString("highlight");
%>
	<%sv.yrsinf04.setClassString("");%>
<%	sv.yrsinf04.appendClassString("num_fld");
	sv.yrsinf04.appendClassString("input_txt");
	sv.yrsinf04.appendClassString("highlight");
%>
	<%sv.offset04.setClassString("");%>
<%	sv.offset04.appendClassString("num_fld");
	sv.offset04.appendClassString("input_txt");
	sv.offset04.appendClassString("highlight");
%>
	<%sv.sumass04.setClassString("");%>
<%	sv.sumass04.appendClassString("num_fld");
	sv.sumass04.appendClassString("input_txt");
	sv.sumass04.appendClassString("highlight");
%>
	<%sv.bonus04.setClassString("");%>
<%	sv.bonus04.appendClassString("num_fld");
	sv.bonus04.appendClassString("input_txt");
	sv.bonus04.appendClassString("highlight");
%>
	<%sv.yrsinf05.setClassString("");%>
<%	sv.yrsinf05.appendClassString("num_fld");
	sv.yrsinf05.appendClassString("input_txt");
	sv.yrsinf05.appendClassString("highlight");
%>
	<%sv.offset05.setClassString("");%>
<%	sv.offset05.appendClassString("num_fld");
	sv.offset05.appendClassString("input_txt");
	sv.offset05.appendClassString("highlight");
%>
	<%sv.sumass05.setClassString("");%>
<%	sv.sumass05.appendClassString("num_fld");
	sv.sumass05.appendClassString("input_txt");
	sv.sumass05.appendClassString("highlight");
%>
	<%sv.bonus05.setClassString("");%>
<%	sv.bonus05.appendClassString("num_fld");
	sv.bonus05.appendClassString("input_txt");
	sv.bonus05.appendClassString("highlight");
%>
	<%sv.yrsinf06.setClassString("");%>
<%	sv.yrsinf06.appendClassString("num_fld");
	sv.yrsinf06.appendClassString("input_txt");
	sv.yrsinf06.appendClassString("highlight");
%>
	<%sv.offset06.setClassString("");%>
<%	sv.offset06.appendClassString("num_fld");
	sv.offset06.appendClassString("input_txt");
	sv.offset06.appendClassString("highlight");
%>
	<%sv.sumass06.setClassString("");%>
<%	sv.sumass06.appendClassString("num_fld");
	sv.sumass06.appendClassString("input_txt");
	sv.sumass06.appendClassString("highlight");
%>
	<%sv.bonus06.setClassString("");%>
<%	sv.bonus06.appendClassString("num_fld");
	sv.bonus06.appendClassString("input_txt");
	sv.bonus06.appendClassString("highlight");
%>
	<%sv.yrsinf07.setClassString("");%>
<%	sv.yrsinf07.appendClassString("num_fld");
	sv.yrsinf07.appendClassString("input_txt");
	sv.yrsinf07.appendClassString("highlight");
%>
	<%sv.offset07.setClassString("");%>
<%	sv.offset07.appendClassString("num_fld");
	sv.offset07.appendClassString("input_txt");
	sv.offset07.appendClassString("highlight");
%>
	<%sv.sumass07.setClassString("");%>
<%	sv.sumass07.appendClassString("num_fld");
	sv.sumass07.appendClassString("input_txt");
	sv.sumass07.appendClassString("highlight");
%>
	<%sv.bonus07.setClassString("");%>
<%	sv.bonus07.appendClassString("num_fld");
	sv.bonus07.appendClassString("input_txt");
	sv.bonus07.appendClassString("highlight");
%>
	<%sv.yrsinf08.setClassString("");%>
<%	sv.yrsinf08.appendClassString("num_fld");
	sv.yrsinf08.appendClassString("input_txt");
	sv.yrsinf08.appendClassString("highlight");
%>
	<%sv.offset08.setClassString("");%>
<%	sv.offset08.appendClassString("num_fld");
	sv.offset08.appendClassString("input_txt");
	sv.offset08.appendClassString("highlight");
%>
	<%sv.sumass08.setClassString("");%>
<%	sv.sumass08.appendClassString("num_fld");
	sv.sumass08.appendClassString("input_txt");
	sv.sumass08.appendClassString("highlight");
%>
	<%sv.bonus08.setClassString("");%>
<%	sv.bonus08.appendClassString("num_fld");
	sv.bonus08.appendClassString("input_txt");
	sv.bonus08.appendClassString("highlight");
%>
	<%sv.yrsinf09.setClassString("");%>
<%	sv.yrsinf09.appendClassString("num_fld");
	sv.yrsinf09.appendClassString("input_txt");
	sv.yrsinf09.appendClassString("highlight");
%>
	<%sv.offset09.setClassString("");%>
<%	sv.offset09.appendClassString("num_fld");
	sv.offset09.appendClassString("input_txt");
	sv.offset09.appendClassString("highlight");
%>
	<%sv.sumass09.setClassString("");%>
<%	sv.sumass09.appendClassString("num_fld");
	sv.sumass09.appendClassString("input_txt");
	sv.sumass09.appendClassString("highlight");
%>
	<%sv.bonus09.setClassString("");%>
<%	sv.bonus09.appendClassString("num_fld");
	sv.bonus09.appendClassString("input_txt");
	sv.bonus09.appendClassString("highlight");
%>
	<%sv.yrsinf10.setClassString("");%>
<%	sv.yrsinf10.appendClassString("num_fld");
	sv.yrsinf10.appendClassString("input_txt");
	sv.yrsinf10.appendClassString("highlight");
%>
	<%sv.offset10.setClassString("");%>
<%	sv.offset10.appendClassString("num_fld");
	sv.offset10.appendClassString("input_txt");
	sv.offset10.appendClassString("highlight");
%>
	<%sv.sumass10.setClassString("");%>
<%	sv.sumass10.appendClassString("num_fld");
	sv.sumass10.appendClassString("input_txt");
	sv.sumass10.appendClassString("highlight");
%>
	<%sv.bonus10.setClassString("");%>
<%	sv.bonus10.appendClassString("num_fld");
	sv.bonus10.appendClassString("input_txt");
	sv.bonus10.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
<!--   <div class="col-md-3"></div> -->
<div class="col-md-4">
	                   <div class="form-group" style="min-width:70px;">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                              <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<!-- <div class="col-md-2"></div> -->
<div class="col-md-4">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
             <!--  <div class="input-group" style="padding-left:20px;">	 -->	
             <table><tr><td>
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    <!-- </div> -->
    </td></tr></table>
  </div>
 </div>
</div>
<div class="row">
	<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
               <table>
		<tr>
		  <td>
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style=width:90px;>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>	

	<td>
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
	<!-- <div class="col-md-1"></div> -->
<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Unit")%></label>
              <div class="input-group" style="min-width:71px;">
              	<%	
			qpsf = fw.getFieldXMLDef((sv.riskunit01).getFieldName());
			
			valueThis=smartHF.getPicFormatted(qpsf,sv.riskunit01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='riskunit01'
type='text'

	value='<%=valueThis %>'
			 <%
	
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.riskunit01.getLength(), sv.riskunit01.getScale(),3)%>'
 maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.riskunit01.getLength(), sv.riskunit01.getScale(),3)-1%>'  

	onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(riskunit01)' onKeyUp='return checkMaxLength(this)'  


	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.riskunit01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskunit01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskunit01).getColor()== null  ? 
			"input_cell" :  (sv.riskunit01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>>
</div>
</div>
</div>
<!-- <div class="col-md-1"></div> -->
<div class="col-md-4">
		   <div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Unit")%></label>
             <div class="input-group" style="min-width:71px;">
             <%	
			qpsf = fw.getFieldXMLDef((sv.riskunit02).getFieldName());
	
			valueThis=smartHF.getPicFormatted(qpsf,sv.riskunit02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			
			
	%>

<input name='riskunit02' 
type='text'

	value='<%=valueThis %>'
			 <%
	
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>
size='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.riskunit02.getLength(), sv.riskunit02.getScale(),3)%>'



maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.riskunit02.getLength(), sv.riskunit02.getScale(),3)-1%>' 

onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(riskunit02)' onKeyUp='return checkMaxLength(this)' 

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.riskunit02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskunit02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskunit02).getColor()== null  ? 
			"input_cell" :  (sv.riskunit02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="max-width:40px;"
 
<%
	} 
%>
>
</div>
</div>
</div>
</div>
<div class="row">
</div>

<div class="row">

<div class="col-md-2">
<label><%=smartHF.getLit(generatedText6)%></label>
<div class="list-group" style="width:46px;">
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf01)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf02)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf03)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf04)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf05)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf06)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf07)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf08)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf09)%>
<%=smartHF.getHTMLVarExt(fw, sv.yrsinf10)%>
</div>
</div>

<div class="col-md-2">
<label><%=smartHF.getLit(generatedText13)%></label>
<div class="list-group" style="width:46px;">
<%=smartHF.getHTMLVarExt(fw, sv.offset01)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset02)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset03)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset04)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset05)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset06)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset07)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset08)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset09)%>
<%=smartHF.getHTMLVarExt(fw, sv.offset10)%>
</div>
</div>

<div class="col-md-2">
<label><%=smartHF.getLit(generatedText7)%></label>
<div class="list-group" style="width:71px;">
<%=smartHF.getHTMLVarExt(fw, sv.sumass01)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass02)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass03)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass04)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass05)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass06)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass07)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass08)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass09)%>
<%=smartHF.getHTMLVarExt(fw, sv.sumass10)%>
</div>
</div>

<div class="col-md-2">
<label><%=smartHF.getLit(generatedText8)%></label>
<div class="list-group" style="width:71px;">
<%=smartHF.getHTMLVarExt(fw, sv.bonus01)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus02)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus03)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus04)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus05)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus06)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus07)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus08)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus09)%>
<%=smartHF.getHTMLVarExt(fw, sv.bonus10)%>
</div>
</div>
<div class="col-md-4"></div>
</div>

	


<%}%>

<%if (sv.S6635protectWritten.gt(0)) {%>
	<%S6635protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>


