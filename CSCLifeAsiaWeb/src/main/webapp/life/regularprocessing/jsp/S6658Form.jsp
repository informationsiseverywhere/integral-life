<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S6658";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>
<%S6658ScreenVars sv = (S6658ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid From ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary Processing Subroutine   ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Increase Processing -");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency Factor                    ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Simple              ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subroutine                          ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Compound            ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New component                       ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Optional            ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Add to Existing Component           ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandatory           ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversal subroutine                 ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Term to Cessation           ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Percentage  ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Age                         ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Percentage  ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed No. of Increases              ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission                          ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statistics          ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No Commission                       ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No Statistics       ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum No. of Refusals allowable   ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Permitted Refusal Period (Days)     ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.subprog.setReverse(BaseScreenData.REVERSED);
			sv.subprog.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.subprog.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.simpleInd.setReverse(BaseScreenData.REVERSED);
			sv.simpleInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.simpleInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.premsubr.setReverse(BaseScreenData.REVERSED);
			sv.premsubr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.premsubr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.compoundInd.setReverse(BaseScreenData.REVERSED);
			sv.compoundInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.compoundInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.addnew.setReverse(BaseScreenData.REVERSED);
			sv.addnew.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.addnew.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.optind.setReverse(BaseScreenData.REVERSED);
			sv.optind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.optind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.addexist.setReverse(BaseScreenData.REVERSED);
			sv.addexist.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.addexist.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.manopt.setReverse(BaseScreenData.REVERSED);
			sv.manopt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.manopt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.trevsub.setReverse(BaseScreenData.REVERSED);
			sv.trevsub.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.trevsub.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.minctrm.setReverse(BaseScreenData.REVERSED);
			sv.minctrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.minctrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.minpcnt.setReverse(BaseScreenData.REVERSED);
			sv.minpcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.minpcnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.agemax.setReverse(BaseScreenData.REVERSED);
			sv.agemax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.agemax.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.maxpcnt.setReverse(BaseScreenData.REVERSED);
			sv.maxpcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.maxpcnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.fixdtrm.setReverse(BaseScreenData.REVERSED);
			sv.fixdtrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.fixdtrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.comind.setReverse(BaseScreenData.REVERSED);
			sv.comind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.comind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.statind.setReverse(BaseScreenData.REVERSED);
			sv.statind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.statind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.nocommind.setReverse(BaseScreenData.REVERSED);
			sv.nocommind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.nocommind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.nostatin.setReverse(BaseScreenData.REVERSED);
			sv.nostatin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.nostatin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.maxRefusals.setReverse(BaseScreenData.REVERSED);
			sv.maxRefusals.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.maxRefusals.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.refusalPeriod.setReverse(BaseScreenData.REVERSED);
			sv.refusalPeriod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.refusalPeriod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.incrFlg.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <div class="input-group">
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
	
    </div>
  </div>
  
<div class="col-md-4">
	                   <div class="form-group" style="max-width:70px;">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                              <div class="input-group">
                              <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
</div>

<div class="col-md-4">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <table><tr><td>	
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr></table>
  </div>
 </div>
</div>
<div class="row">
	<div class="col-md-4">
		   <div class="form-group">
              <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
               <table>
		<tr>
		  <td style="width:90px;">
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td>
					<td><td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("to")%></td>	

	<td style="width:90px;">
        <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	         </td>
           </tr>
		</table>
             </div>
	</div>
<div class="col-md-4"></div>
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Anniversary Processing Subroutine")%></label>
  <div class="input-group">        <input name='subprog' 
type='text'

<%if((sv.subprog).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.subprog.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subprog.getLength()%>'
maxLength='<%= sv.subprog.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subprog)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.subprog).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.subprog).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.subprog).getColor()== null  ? 
			"input_cell" :  (sv.subprog).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>  
</div>                    
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Increase Processing -")%></label>
                        </div>
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Frequency Factor")%></label>
  <div class="input-group">      <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("billfreq");
	optionValue = makeDropDownList( mappedItems , sv.billfreq.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.billfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.billfreq).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='billfreq' type='list' style="width:140px;"
<% 
	if((new Byte((sv.billfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.billfreq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.billfreq).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Simple")%></label> 
  <div class="input-group" >       <input name='simpleInd' 
type='text'

<%if((sv.simpleInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.simpleInd.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.simpleInd.getLength()%>'
maxLength='<%= sv.simpleInd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(simpleInd)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.simpleInd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.simpleInd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.simpleInd).getColor()== null  ? 
			"input_cell" :  (sv.simpleInd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
   <label><%=resourceBundleHandler.gettingValueFromBundle("Compound")%></label> 
   <div class="input-group" >         <input name='compoundInd' 
type='text'

<%if((sv.compoundInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.compoundInd.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.compoundInd.getLength()%>'
maxLength='<%= sv.compoundInd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(compoundInd)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.compoundInd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.compoundInd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.compoundInd).getColor()== null  ? 
			"input_cell" :  (sv.compoundInd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>                    
</div>
</div> 
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%></label>
  <div class="input-group">          <input name='premsubr' 
type='text'

<%if((sv.premsubr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.premsubr.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.premsubr.getLength()%>'
maxLength='<%= sv.premsubr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premsubr)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.premsubr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premsubr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premsubr).getColor()== null  ? 
			"input_cell" :  (sv.premsubr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 
<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((sv.incrFlg).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Increase on Modified Value")%></label>
					<div>
						<input type='checkbox' name='incrFlg' value='Y'
							onFocus='doFocus(this)' onHelp='return fieldHelp(incrFlg)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((sv.incrFlg).getColor() != null) {%>
							style='background-color: #FF0000;'
							<%}
			if ((sv.incrFlg).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<%}
			if ((sv.incrFlg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()) {%>
							disabled <%}%> class='UICheck' onclick="handleCheckBox('incrFlg')" />

						<input type='checkbox' name='incrFlg' value=' '
							<%if (!(sv.incrFlg).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%}%> style="visibility: hidden"
							onclick="handleCheckBox('indic')" />
					</div>
					<%}%>
				</div>
			</div> 
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("New component")%></label>
 <div class="input-group">       <input name='addnew' 
type='text'

<%if((sv.addnew).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.addnew.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.addnew.getLength()%>'
maxLength='<%= sv.addnew.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(addnew)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.addnew).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.addnew).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.addnew).getColor()== null  ? 
			"input_cell" :  (sv.addnew).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Optional")%></label>
 <div class="input-group">      <input name='optind' 
type='text'

<%if((sv.optind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.optind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.optind.getLength()%>'
maxLength='<%= sv.optind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(optind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.optind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.optind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.optind).getColor()== null  ? 
			"input_cell" :  (sv.optind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Mandatory")%></label>
  <div class="input-group" >        <input name='manopt' 
type='text'

<%if((sv.manopt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.manopt.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.manopt.getLength()%>'
maxLength='<%= sv.manopt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(manopt)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.manopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.manopt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.manopt).getColor()== null  ? 
			"input_cell" :  (sv.manopt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>  
</div> 
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Add to Existing Component")%></label>
  <div class="input-group">    <input name='addexist' 
type='text'

<%if((sv.addexist).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.addexist.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.addexist.getLength()%>'
maxLength='<%= sv.addexist.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(addexist)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.addexist).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.addexist).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.addexist).getColor()== null  ? 
			"input_cell" :  (sv.addexist).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Reversal subroutine")%></label>  
    <div class="input-group" >         <input name='trevsub' 
type='text'

<%if((sv.trevsub).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.trevsub.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.trevsub.getLength()%>'
maxLength='<%= sv.trevsub.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(trevsub)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.trevsub).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.trevsub).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.trevsub).getColor()== null  ? 
			"input_cell" :  (sv.trevsub).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Term to Cessation")%></label>
      <div class="input-group">      <%	
			qpsf = fw.getFieldXMLDef((sv.minctrm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='minctrm' 
type='text'

<%if((sv.minctrm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.minctrm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.minctrm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.minctrm) %>'
	 <%}%>

size='<%= sv.minctrm.getLength()%>'
maxLength='<%= sv.minctrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(minctrm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.minctrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.minctrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.minctrm).getColor()== null  ? 
			"input_cell" :  (sv.minctrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Percentage")%></label>
       <div class="input-group" >     <%	
			qpsf = fw.getFieldXMLDef((sv.minpcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='minpcnt' 
type='text'

<%if((sv.minpcnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.minpcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.minpcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.minpcnt) %>'
	 <%}%>

size='<%= sv.minpcnt.getLength()%>'
maxLength='<%= sv.minpcnt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(minpcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.minpcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.minpcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.minpcnt).getColor()== null  ? 
			"input_cell" :  (sv.minpcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 

<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Percentage")%></label>  
    <div class="input-group">      <%	
			qpsf = fw.getFieldXMLDef((sv.maxpcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='maxpcnt' 
type='text'

<%if((sv.maxpcnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.maxpcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.maxpcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.maxpcnt) %>'
	 <%}%>

size='<%= sv.maxpcnt.getLength()%>'
maxLength='<%= sv.maxpcnt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(maxpcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.maxpcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.maxpcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.maxpcnt).getColor()== null  ? 
			"input_cell" :  (sv.maxpcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 
 
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Fixed No. of Increases")%></label>
  <div class="input-group" >       <%	
			qpsf = fw.getFieldXMLDef((sv.fixdtrm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='fixdtrm' 
type='text'

<%if((sv.fixdtrm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.fixdtrm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.fixdtrm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.fixdtrm) %>'
	 <%}%>

size='<%= sv.fixdtrm.getLength()%>'
maxLength='<%= sv.fixdtrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(fixdtrm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.fixdtrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.fixdtrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.fixdtrm).getColor()== null  ? 
			"input_cell" :  (sv.fixdtrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Age")%></label>
  <div class="input-group" >          <%	
			qpsf = fw.getFieldXMLDef((sv.agemax).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='agemax' 
type='text'

<%if((sv.agemax).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.agemax) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.agemax);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.agemax) %>'
	 <%}%>

size='<%= sv.agemax.getLength()%>'
maxLength='<%= sv.agemax.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(agemax)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.agemax).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.agemax).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.agemax).getColor()== null  ? 
			"input_cell" :  (sv.agemax).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div> 
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Commission")%></label>
     <div class="input-group" >       <input name='comind' 
type='text'

<%if((sv.comind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.comind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.comind.getLength()%>'
maxLength='<%= sv.comind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(comind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.comind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.comind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.comind).getColor()== null  ? 
			"input_cell" :  (sv.comind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
> 
</div>                  
</div>
</div> 
<div class="col-md-4"></div> 
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("No Commission")%></label>  
 <div class="input-group" >            <input name='nocommind' 
type='text'

<%if((sv.nocommind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.nocommind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.nocommind.getLength()%>'
maxLength='<%= sv.nocommind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(nocommind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.nocommind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.nocommind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.nocommind).getColor()== null  ? 
			"input_cell" :  (sv.nocommind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>  
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Statistics")%></label> 
 <div class="input-group" >      <input name='statind' 
type='text'

<%if((sv.statind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.statind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.statind.getLength()%>'
maxLength='<%= sv.statind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(statind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.statind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.statind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.statind).getColor()== null  ? 
			"input_cell" :  (sv.statind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
> 
</div>                       
</div>
</div> 
<div class="col-md-4"></div>     
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("No Statistics")%></label>
<div class="input-group" >            <input name='nostatin' 
type='text'

<%if((sv.nostatin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.nostatin.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.nostatin.getLength()%>'
maxLength='<%= sv.nostatin.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(nostatin)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.nostatin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.nostatin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.nostatin).getColor()== null  ? 
			"input_cell" :  (sv.nostatin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
</div> 
<div class="row">
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Maximum No. of Refusals allowable")%></label>
 <div class="input-group" >      
	<%	
			qpsf = fw.getFieldXMLDef((sv.maxRefusals).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='maxRefusals' 
type='text'

<%if((sv.maxRefusals).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.maxRefusals) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.maxRefusals);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.maxRefusals) %>'
	 <%}%>

size='<%= sv.maxRefusals.getLength()%>'
maxLength='<%= sv.maxRefusals.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(maxRefusals)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.maxRefusals).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.maxRefusals).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.maxRefusals).getColor()== null  ? 
			"input_cell" :  (sv.maxRefusals).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>
<div class="col-md-4"></div>   
<div class="col-md-4">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Permitted Refusal Period (Days)")%></label>
          
	<div class="input-group" > <%	
			qpsf = fw.getFieldXMLDef((sv.refusalPeriod).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
			
	%>

<input name='refusalPeriod' 
type='text'

<%if((sv.refusalPeriod).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.refusalPeriod) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.refusalPeriod);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.refusalPeriod) %>'
	 <%}%>

size='<%= sv.refusalPeriod.getLength()%>'
maxLength='<%= sv.refusalPeriod.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(refusalPeriod)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.refusalPeriod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.refusalPeriod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.refusalPeriod).getColor()== null  ? 
			"input_cell" :  (sv.refusalPeriod).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>  
>
</div>
</div>
</div>
</div>                                                                                                                                                                                                                                                                                                                                                                                                          
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
