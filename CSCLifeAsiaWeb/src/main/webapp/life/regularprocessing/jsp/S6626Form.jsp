

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6626";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.regularprocessing.screens.*" %>

<%S6626ScreenVars sv = (S6626ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing transaction Code      ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Collection");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Issue");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.transcd01.setReverse(BaseScreenData.REVERSED);
			sv.transcd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.transcd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.transcd06.setReverse(BaseScreenData.REVERSED);
			sv.transcd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.transcd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.transcd07.setReverse(BaseScreenData.REVERSED);
			sv.transcd07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.transcd07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.transcd08.setReverse(BaseScreenData.REVERSED);
			sv.transcd08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.transcd08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.transcd09.setReverse(BaseScreenData.REVERSED);
			sv.transcd09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.transcd09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.transcd10.setReverse(BaseScreenData.REVERSED);
			sv.transcd10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.transcd10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.transcd02.setReverse(BaseScreenData.REVERSED);
			sv.transcd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.transcd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.transcd03.setReverse(BaseScreenData.REVERSED);
			sv.transcd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.transcd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.transcd04.setReverse(BaseScreenData.REVERSED);
			sv.transcd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.transcd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.transcd05.setReverse(BaseScreenData.REVERSED);
			sv.transcd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.transcd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.trcode.setReverse(BaseScreenData.REVERSED);
			sv.trcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.trcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.trncd01.setReverse(BaseScreenData.REVERSED);
			sv.trncd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.trncd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.trncd02.setReverse(BaseScreenData.REVERSED);
			sv.trncd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.trncd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.trncd03.setReverse(BaseScreenData.REVERSED);
			sv.trncd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.trncd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.trncd04.setReverse(BaseScreenData.REVERSED);
			sv.trncd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.trncd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.trncd05.setReverse(BaseScreenData.REVERSED);
			sv.trncd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.trncd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.trncd06.setReverse(BaseScreenData.REVERSED);
			sv.trncd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.trncd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.trncd07.setReverse(BaseScreenData.REVERSED);
			sv.trncd07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.trncd07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.trncd08.setReverse(BaseScreenData.REVERSED);
			sv.trncd08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.trncd08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.trncd09.setReverse(BaseScreenData.REVERSED);
			sv.trncd09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.trncd09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.trncd10.setReverse(BaseScreenData.REVERSED);
			sv.trncd10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.trncd10.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
                      <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>




	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	<br>
</div>
</div>
<!--  <div class="col-md-2"></div> -->
 <div class="col-md-4">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>


	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</div>
</div>

<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->	
	
	<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}


}
</style>
		<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->



<!--  <div class="col-md-2"></div> -->
 <div class="col-md-4">
 <div class="form-group">
 <label> <%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
<!-- <div class="input-group"> -->


<table><tr><td>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'id="idesc" style="margin-left:1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
<!-- </div> --></div></div></div>

<div class="row">
 <div class="col-md-4">
<div class="form-group">

<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Transaction Code")%></label>





<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode");
	optionValue = makeDropDownList( mappedItems , sv.trcode.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode.getFormData()).toString().trim());  
%>
	<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->
<%-- <%=smartHF.getDropDownExt(sv.trcode, fw, longValue, "trcode", optionValue,0,150) %> --%> 
<% 
	if((new Byte((sv.trcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:165px;"> 
<%
} 
%>

<select name='trcode' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<br></div>
</div><div class="col-md-4"></div><div class="col-md-4"></div></div>

<div class="row">
<div class="col-md-2">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Collection")%></label>
</div></div>

<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Issue")%></label>
</div>
</div>
<div class="col-md-1"></div>

 <div class="col-md-2">
 <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Other")%></label>
</div>
</div>
<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Other")%></label>

</div>

</div><div class="col-md-2"></div>
</div>
<div class="row">
 <div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd01");
	optionValue = makeDropDownList( mappedItems , sv.transcd01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd01.getFormData()).toString().trim());  
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd01, fw, longValue, "transcd01", optionValue,0,150) %> --%>


<% 
	if((new Byte((sv.transcd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd01' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
</div>

<div class="col-md-1"></div>
<div class="col-md-2"> 
<div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd06");
	optionValue = makeDropDownList( mappedItems , sv.transcd06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd06.getFormData()).toString().trim());  
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd06, fw, longValue, "transcd06", optionValue,0,150) %> --%>

<% 
	if((new Byte((sv.transcd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd06' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>
</div>

<div class="col-md-1"></div>
 <div class="col-md-2"> 
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd01");
	optionValue = makeDropDownList( mappedItems , sv.trncd01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trncd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd01' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.trncd01, fw, longValue, "trncd01", optionValue,0,150) %> --%>
</div>
 </div>


<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd06");
	optionValue = makeDropDownList( mappedItems , sv.trncd06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd06.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd06' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

</div>
</div><div class="col-md-1"></div></div>

&nbsp;
<div class="row">
 <div class="col-md-2">
  <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd02");
	optionValue = makeDropDownList( mappedItems , sv.transcd02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd02.getFormData()).toString().trim());  
%>


<% 
	if((new Byte((sv.transcd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd02' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd02, fw, longValue, "transcd02", optionValue,0,150) %> --%>
</div>
</div>

<div class="col-md-1"></div>

<div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd07");
	optionValue = makeDropDownList( mappedItems , sv.transcd07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.transcd07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd07' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd07, fw, longValue, "transcd07", optionValue,0,150) %> --%>
</div>
</div>

<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd02");
	optionValue = makeDropDownList( mappedItems , sv.trncd02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trncd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd02' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.trncd02, fw, longValue, "trncd02", optionValue,0,150) %> --%>
</div>
</div>
<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd07");
	optionValue = makeDropDownList( mappedItems , sv.trncd07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd07.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd07' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.trncd07, fw, longValue, "trncd07", optionValue,0,150) %> --%>
</div>
</div><div class="col-md-1"></div>
</div>
&nbsp;

<div class="row">

 <div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd03");
	optionValue = makeDropDownList( mappedItems , sv.transcd03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd03.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd03' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd03, fw, longValue, "transcd03", optionValue,0,150) %> --%>
</div>
</div>


<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd08");
	optionValue = makeDropDownList( mappedItems , sv.transcd08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd08.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd08' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.transcd08, fw, longValue, "transcd08", optionValue,0,150) %> --%>
</div>
</div>


<div class="col-md-1"></div>
<div class="col-md-2">


 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd03");
	optionValue = makeDropDownList( mappedItems , sv.trncd03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd03.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd03' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.trncd03, fw, longValue, "trncd03", optionValue,0,150) %> --%>
</div>
</div>

<div class="col-md-1"></div>
 <div class="col-md-2">

 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd08");
	optionValue = makeDropDownList( mappedItems , sv.trncd08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd08.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd08' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.trncd08, fw, longValue, "trncd08", optionValue,0,150) %> --%>
</div>
</div></div>&nbsp;

 <div class="row">
 <div class="col-md-2">
 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd04");
	optionValue = makeDropDownList( mappedItems , sv.transcd04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd04.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd04' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled 
	class="output_cell"
<%
	}else if((new Byte((sv.transcd04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.transcd04, fw, longValue, "transcd04", optionValue,0,150) %> --%>
</div>
</div>

<div class="col-md-1"></div>
 <div class="col-md-2">

 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd09");
	optionValue = makeDropDownList( mappedItems , sv.transcd09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd09.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd09' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.transcd09, fw, longValue, "transcd09", optionValue,0,150) %> --%>
</div>
</div>

<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd04");
	optionValue = makeDropDownList( mappedItems , sv.trncd04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd04.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd04' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.trncd04, fw, longValue, "trncd04", optionValue,0,150) %> --%>
</div></div>


<div class="col-md-1"></div>
<div class="col-md-2">

 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd09");
	optionValue = makeDropDownList( mappedItems , sv.trncd09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd09.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd09' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.trncd09, fw, longValue, "trncd09", optionValue,0,150) %> --%>
</div></div><div class="col-md-1"></div></div>&nbsp;

<div class="row">
 <div class="col-md-2">
 <div class="form-group">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd05");
	optionValue = makeDropDownList( mappedItems , sv.transcd05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd05.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd05' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%-- <%=smartHF.getDropDownExt(sv.transcd05, fw, longValue, "transcd05", optionValue,0,150) %> --%>
</div></div>

<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"transcd10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("transcd10");
	optionValue = makeDropDownList( mappedItems , sv.transcd10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.transcd10.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.transcd10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.transcd10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='transcd10' type='list' style="width:220px;"
<% 
	if((new Byte((sv.transcd10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.transcd10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.transcd10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.transcd10, fw, longValue, "transcd10", optionValue,0,150) %> --%>
</div></div>

<div class="col-md-1"></div>
 <div class="col-md-2">
 <div class="form-group">


<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd05");
	optionValue = makeDropDownList( mappedItems , sv.trncd05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd05.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd05' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


<%-- <%=smartHF.getDropDownExt(sv.trncd05, fw, longValue, "trncd05", optionValue,0,150) %> --%>
</div></div>

<div class="col-md-1"></div>
 <div class="col-md-2">

 <div class="form-group">
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trncd10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trncd10");
	optionValue = makeDropDownList( mappedItems , sv.trncd10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trncd10.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.trncd10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:165px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trncd10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trncd10' type='list' style="width:220px;"
<% 
	if((new Byte((sv.trncd10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trncd10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trncd10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
<%-- <%=smartHF.getDropDownExt(sv.trncd10, fw, longValue, "trncd10", optionValue,0,150) %> --%>
</div></div><div class="col-md-1"></div></div>

	<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Codes")%>
</div>

</tr></table></div></div></div>


	<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->
<script>
$(document).ready(function(){

	$('#idesc').width('190');
	createDropdownNotInTable("trcode",15);
	createDropdownNotInTable("transcd01",15);
	createDropdownNotInTable("trncd01",15);
	createDropdownNotInTable("transcd02",15);
	createDropdownNotInTable("trncd02",15);
	createDropdownNotInTable("transcd03",15);
	createDropdownNotInTable("trncd03",15);
	createDropdownNotInTable("transcd04",15);
	createDropdownNotInTable("trncd04",15);
	createDropdownNotInTable("transcd05",15);
	createDropdownNotInTable("trncd05",15);
	createDropdownNotInTable("transcd06",15);
	createDropdownNotInTable("trncd06",15);
	createDropdownNotInTable("transcd07",15);
	createDropdownNotInTable("trncd07",15);
	createDropdownNotInTable("transcd08",15);
	createDropdownNotInTable("trncd08",15);
	createDropdownNotInTable("transcd09",15);
	createDropdownNotInTable("trncd09",15);
	createDropdownNotInTable("transcd10",15);
	createDropdownNotInTable("trncd10",15);
});
</script> 
	<!-- ILIFE-2660 Life Cross Browser - Sprint 3 D3 : Task 1  -->


<%@ include file="/POLACommon2NEW.jsp"%>

