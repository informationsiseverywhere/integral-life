

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR509";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.interestbearing.screens.*"%>
   <%
	Sr509ScreenVars sv = (Sr509ScreenVars) fw.getVariables();
%>
   <%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"A - Enter New Rate(s)");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"B - Delete New Rate(s)");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"C - Enquire on Rate(s)");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Interest Bearing Fund ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"(Leave blank for all");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"eligible funds)");
%>
  <%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From Date ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Action ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.fromdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.fromdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.fromdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Bearing Fund")%></label>
					<div class="input-group" style="width:180px;">
				    			<%if ((new Byte((sv.unitVirtualFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
								<%
									longValue = sv.unitVirtualFund.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.unitVirtualFund).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
								<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "output_cell" %>'>  
									   		<%if(longValue != null){%>
									   		
									   		<%=longValue%>
									   		
									   		<%}%>
									   </div>
								
								<%
								longValue = null;
								%>
								<% }else {%> 
								<input name='unitVirtualFund' id='unitVirtualFund'
								type='text' 
								value='<%=sv.unitVirtualFund.getFormData()%>' 
								maxLength='<%=sv.unitVirtualFund.getLength()%>' 
								size='<%=sv.unitVirtualFund.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(unitVirtualFund)' onKeyUp='return CheckMaxLengthAndChangeCase(unitVirtualFund, this)'  
								onblur = 'ChangeCase(unitVirtualFund)'
								<% 
									if((new Byte((sv.unitVirtualFund).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
								readonly="true"
								class="output_cell"	 >
								<%
								}else if((new Byte((sv.unitVirtualFund).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									
								%>	
								class="bold_cell" >
								<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"  type="button" onClick="doFocus(document.getElementById('unitVirtualFund'));doAction('PFKEY04')">
					        		<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        		</button>
					      		</span>
								
								<%
									}else { 
								%>
								
								class = ' <%=(sv.unitVirtualFund).getColor()== null  ? 
								"input_cell" :  (sv.unitVirtualFund).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>' >
								<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('unitVirtualFund'));doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
								
								<%}longValue = null;}} %>
								</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("From Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='fromdateDisp' type='text'
							value='<%=sv.fromdateDisp.getFormData()%>'
							maxLength='<%=sv.fromdateDisp.getLength()%>'
							size='<%=sv.fromdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(fromdateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.fromdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.fromdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							} else {
						%>

						class = '
						<%=(sv.fromdateDisp).getColor() == null ? "input_cell"
						: (sv.fromdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Enter New Rate(s)")%>
					</b></label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Delete New Rate(s)")%>
				</b>	</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "C")%>
						<%=resourceBundleHandler.gettingValueFromBundle("Enquire on Rate(s)")%>
					</b></label>
				</div>
			</div>
		</div>
		<div style="visibility: hidden;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("(Leave blank for all")%></label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("eligible funds)")%></label>
						<div>
							<input name='action' type='hidden'
								value='<%=sv.action.getFormData()%>'
								size='<%=sv.action.getLength()%>'
								maxLength='<%=sv.action.getLength()%>' class="input_cell"
								onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
								onKeyUp='return checkMaxLength(this)'>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>
