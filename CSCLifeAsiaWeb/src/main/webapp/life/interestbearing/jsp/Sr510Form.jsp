<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR510";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.interestbearing.screens.*"%>
<%
	Sr510ScreenVars sv = (Sr510ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.unitVirtualFund.setReverse(BaseScreenData.REVERSED);
			sv.unitVirtualFund.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.unitVirtualFund.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.shortdesc.setReverse(BaseScreenData.REVERSED);
			sv.shortdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.shortdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.interate01.setReverse(BaseScreenData.REVERSED);
			sv.interate01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.interate01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.interate02.setReverse(BaseScreenData.REVERSED);
			sv.interate02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.interate02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.fromdate01Disp.setReverse(BaseScreenData.REVERSED);
			sv.fromdate01Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.fromdate01Disp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.todateDisp.setReverse(BaseScreenData.REVERSED);
			sv.todateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.todateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.interate03.setReverse(BaseScreenData.REVERSED);
			sv.interate03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.interate03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.interate03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.interate04.setReverse(BaseScreenData.REVERSED);
			sv.interate04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.interate04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.interate04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.fromdate02Disp.setReverse(BaseScreenData.REVERSED);
			sv.fromdate02Disp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.fromdate02Disp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Fund");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Description");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Current Rate");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From Date");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "To Date");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "New Rate");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "From Date");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "New   Old");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "New   Old");
%>
<%
	appVars.rollup(new int[]{93});
%>


<div class="panel panel-default">
	<div class="panel-body">
		<%
			GeneralTable sfl = fw.getTable("sr510screensfl");
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr510' width='100%'>
						<thead>
							<tr class='info'>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Current Rate New")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("New Rate Old")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("From Date")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("To Date")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("New Rate New")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("New Rate Old")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("From Date")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								Sr510screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sr510screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<td><%=sv.unitVirtualFund.getFormData()%></td>
								<td><%=sv.shortdesc.getFormData()%></td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.interate01).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.interate01);
 		if (!sv.interate01.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>
								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.interate02).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.interate02);
 		if (!sv.interate02.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>
								</td>
								<td><%=sv.fromdate01Disp.getFormData()%></td>
								<td><%=sv.todateDisp.getFormData()%></td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.interate03).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.interate03);
									%> <%
 	if ((new Byte((sv.interate03).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| fw.getVariables().isScreenProtected()) {
 		} else {
 %> <input type='text'
									maxLength='<%=sv.interate03.getLength()%>'
									value='<%=formatValue%>' size='<%=sv.interate03.getLength()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(sr510screensfl.interate03)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sr510screensfl" + "." + "interate03" + "_R" + count%>'
									id='<%="sr510screensfl" + "." + "interate03" + "_R" + count%>'
									class="input_cell" style="width: 90 px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals() - 1%> ); "
									decimal='<%=qpsf.getDecimals() - 1%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>

									<%
										}
									%>

								</td>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.interate04).getFieldName());
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf, sv.interate04);
									%> <%
 	if ((new Byte((sv.interate04).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| fw.getVariables().isScreenProtected()) {
 		} else {
 %> <input type='text'
									maxLength='<%=sv.interate04.getLength()%>'
									value='<%=formatValue%>' size='<%=sv.interate04.getLength()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(sr510screensfl.interate04)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sr510screensfl" + "." + "interate04" + "_R" + count%>'
									id='<%="sr510screensfl" + "." + "interate04" + "_R" + count%>'
									class="input_cell" style="width: 90 px;"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event);'
									onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>
									<%
										}
									%>


								</td>
								<td><%=sv.fromdate02Disp.getFormData()%></td>
							</tr>

							<%
								count = count + 1;
									Sr510screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<script>
	$(document).ready(function() {
		$('#dataTables-sr510').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,

		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
