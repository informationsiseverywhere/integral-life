<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%= ((SMARTHTMLFormatter)AppVars.hf).getHTMLCBVar(19,25)%>
<%String screenName = "S5615";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>

<%S5615ScreenVars sv = (S5615ScreenVars) fw.getVariables();%>

<%if (sv.S5615screenWritten.gt(0)) {%>
	<%S5615screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("Contract       ");%>
	<%sv.chdrsel.setClassString("");%>
<%	sv.chdrsel.appendClassString("string_fld");
	sv.chdrsel.appendClassString("input_txt");
%>
	<%StringData generatedText3 = new StringData("Contract Type  ");%>
	<%sv.chdrtype.setClassString("");%>
	<%StringData generatedText4 = new StringData("Premium Status ");%>
	<%sv.pstatcode.setClassString("");%>
<%	sv.pstatcode.appendClassString("string_fld");
	sv.pstatcode.appendClassString("input_txt");
%>
	<%StringData generatedText5 = new StringData("Contract Curr  ");%>
	<%sv.currcd.setClassString("");%>
<%	sv.currcd.appendClassString("string_fld");
	sv.currcd.appendClassString("input_txt");
%>
	<%StringData generatedText6 = new StringData("Calc To Date   ");%>
	<%sv.ptdateDisp.setClassString("");%>
	<%StringData generatedText7 = new StringData("Billing Freq   ");%>
	<%sv.billfreq.setClassString("");%>
	<%StringData generatedText8 = new StringData("Billed Amount  ");%>
	<%sv.annamnt.setClassString("");%>
	<%StringData generatedText9 = new StringData("Sum Assured    ");%>
	<%sv.sumins.setClassString("");%>
	<%StringData generatedText10 = new StringData("Premium Term   ");%>
	<%sv.premterm.setClassString("");%>
	<%StringData generatedText11 = new StringData("Coverage       ");%>
	<%sv.coverage.setClassString("");%>
	<%StringData generatedText12 = new StringData("Risk Term      ");%>
	<%sv.riskCessTerm.setClassString("");%>
	<%StringData generatedText13 = new StringData("Rider          ");%>
	<%sv.rider.setClassString("");%>
	<%StringData generatedText14 = new StringData("Coverage Name  ");%>
	<%sv.crtable.setClassString("");%>
	<%StringData generatedText15 = new StringData("Joint Life     ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("input_txt");
%>
	<%StringData generatedText16 = new StringData("Cover Start Dt ");%>
	<%sv.crrcdDisp.setClassString("");%>
	<%StringData generatedText17 = new StringData("Joint Life Sex ");%>
	<%sv.jlsex.setClassString("");%>
	<%StringData generatedText18 = new StringData("Joint Age CCD  ");%>
	<%sv.ageprem.setClassString("");%>
	<%StringData generatedText19 = new StringData("Life           ");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("input_txt");
%>
	<%StringData generatedText20 = new StringData("Life Sex       ");%>
	<%sv.sex.setClassString("");%>
	<%StringData generatedText21 = new StringData("Benefit Term   ");%>
	<%sv.benterm.setClassString("");%>
	<%StringData generatedText22 = new StringData("Life Age CCD   ");%>
	<%sv.age.setClassString("");%>
	<%StringData generatedText23 = new StringData("Benefit Freq   ");%>
	<%sv.freqann.setClassString("");%>
	<%StringData generatedText24 = new StringData("Status         ");%>
<%	generatedText24.appendClassString("label_txt");
	generatedText24.appendClassString("highlight");
%>
	<%sv.statuz.setClassString("");%>
	<%StringData generatedText25 = new StringData("Actual Value   ");%>
<%	generatedText25.appendClassString("label_txt");
	generatedText25.appendClassString("highlight");
%>
	<%sv.resamt.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.trancde.setClassString("");%>
	<%sv.currcode.setClassString("");%>
	<%sv.effdateDisp.setClassString("");%>
	<%sv.language.setClassString("");%>
	<%sv.singp.setClassString("");%>

	<%
{
		if (appVars.ind32.isOn()) {
			generatedText2.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.chdrsel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.chdrtype.setReverse(BaseScreenData.REVERSED);
			sv.chdrtype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.chdrtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind23.isOn()) {
			sv.chdrtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind13.isOn()) {
			sv.pstatcode.setReverse(BaseScreenData.REVERSED);
			sv.pstatcode.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.pstatcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind13.isOn()) {
			sv.pstatcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText5.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.currcd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind12.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.billfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind14.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind30.isOn()) {
			sv.annamnt.setReverse(BaseScreenData.REVERSED);
			sv.annamnt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.annamnt.setInvisibility(BaseScreenData.INVISIBLE);
			sv.annamnt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.annamnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.sumins.setReverse(BaseScreenData.REVERSED);
			sv.sumins.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.sumins.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind27.isOn()) {
			sv.sumins.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.premterm.setReverse(BaseScreenData.REVERSED);
			sv.premterm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.premterm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.coverage.setReverse(BaseScreenData.REVERSED);
			sv.coverage.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.coverage.setEnabled(BaseScreenData.DISABLED);
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind04.isOn()) {
			sv.coverage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.rider.setReverse(BaseScreenData.REVERSED);
			sv.rider.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.rider.setEnabled(BaseScreenData.DISABLED);
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind05.isOn()) {
			sv.rider.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.crtable.setReverse(BaseScreenData.REVERSED);
			sv.crtable.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.crtable.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.jlife.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind03.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.crrcdDisp.setReverse(BaseScreenData.REVERSED);
			sv.crrcdDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.crrcdDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.crrcdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.jlsex.setReverse(BaseScreenData.REVERSED);
			sv.jlsex.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.jlsex.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind19.isOn()) {
			sv.jlsex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.ageprem.setReverse(BaseScreenData.REVERSED);
			sv.ageprem.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.ageprem.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind17.isOn()) {
			sv.ageprem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.life.setReverse(BaseScreenData.REVERSED);
			sv.life.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind02.isOn()) {
			sv.life.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.sex.setReverse(BaseScreenData.REVERSED);
			sv.sex.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.sex.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind18.isOn()) {
			sv.sex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.benterm.setReverse(BaseScreenData.REVERSED);
			sv.benterm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.benterm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.age.setReverse(BaseScreenData.REVERSED);
			sv.age.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.age.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind16.isOn()) {
			sv.age.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.freqann.setReverse(BaseScreenData.REVERSED);
			sv.freqann.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.freqann.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.statuz.setReverse(BaseScreenData.REVERSED);
			sv.statuz.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.statuz.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.resamt.setReverse(BaseScreenData.REVERSED);
			sv.resamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.resamt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(4, 4, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(4, 20, fw, sv.chdrsel)%>
	<%=smartHF.getHTMLF4NSVar(4, 20, fw, sv.chdrsel)%>

	<%=smartHF.getLit(5, 4, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(5, 20, fw, sv.chdrtype)%>
	<%=smartHF.getHTMLF4NSVar(5, 20, fw, sv.chdrtype)%>

	<%=smartHF.getLit(5, 39, generatedText4)%>

	<%=smartHF.getHTMLVar(5, 55, fw, sv.pstatcode)%>

	<%=smartHF.getLit(6, 4, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(6, 20, fw, sv.currcd)%>
	<%=smartHF.getHTMLF4NSVar(6, 20, fw, sv.currcd)%>

	<%=smartHF.getLit(6, 39, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(6, 56, fw, sv.ptdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(6, 56, fw, sv.ptdateDisp)%>

	<%=smartHF.getLit(7, 4, generatedText7)%>

	<%=smartHF.getHTMLSpaceVar(7, 20, fw, sv.billfreq)%>
	<%=smartHF.getHTMLF4NSVar(7, 20, fw, sv.billfreq)%>

	<%=smartHF.getLit(8, 4, generatedText8)%>

	<%=smartHF.getHTMLVar(8, 20, fw, sv.annamnt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(8, 39, generatedText9)%>

	<%=smartHF.getHTMLVar(8, 56, fw, sv.sumins, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(9, 39, generatedText10)%>

	<%=smartHF.getHTMLVar(9, 56, fw, sv.premterm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(10, 4, generatedText11)%>

	<%=smartHF.getHTMLVar(10, 20, fw, sv.coverage)%>

	<%=smartHF.getLit(10, 39, generatedText12)%>

	<%=smartHF.getHTMLVar(10, 56, fw, sv.riskCessTerm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(11, 4, generatedText13)%>

	<%=smartHF.getHTMLVar(11, 20, fw, sv.rider)%>

	<%=smartHF.getLit(12, 4, generatedText14)%>

	<%=smartHF.getHTMLSpaceVar(12, 20, fw, sv.crtable)%>
	<%=smartHF.getHTMLF4NSVar(12, 20, fw, sv.crtable)%>

	<%=smartHF.getLit(12, 39, generatedText15)%>

	<%=smartHF.getHTMLVar(12, 55, fw, sv.jlife)%>

	<%=smartHF.getLit(13, 4, generatedText16)%>

	<%=smartHF.getHTMLSpaceVar(13, 20, fw, sv.crrcdDisp)%>
	<%=smartHF.getHTMLCalNSVar(13, 20, fw, sv.crrcdDisp)%>

	<%=smartHF.getLit(13, 39, generatedText17)%>

	<%=smartHF.getHTMLSpaceVar(13, 56, fw, sv.jlsex)%>
	<%=smartHF.getHTMLF4NSVar(13, 56, fw, sv.jlsex)%>

	<%=smartHF.getLit(14, 39, generatedText18)%>

	<%=smartHF.getHTMLVar(14, 56, fw, sv.ageprem, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(15, 4, generatedText19)%>

	<%=smartHF.getHTMLVar(15, 20, fw, sv.life)%>

	<%=smartHF.getLit(16, 4, generatedText20)%>

	<%=smartHF.getHTMLSpaceVar(16, 20, fw, sv.sex)%>
	<%=smartHF.getHTMLF4NSVar(16, 20, fw, sv.sex)%>

	<%=smartHF.getLit(16, 39, generatedText21)%>

	<%=smartHF.getHTMLVar(16, 56, fw, sv.benterm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(17, 4, generatedText22)%>

	<%=smartHF.getHTMLVar(17, 20, fw, sv.age, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(17, 39, generatedText23)%>

	<%=smartHF.getHTMLSpaceVar(17, 56, fw, sv.freqann)%>
	<%=smartHF.getHTMLF4NSVar(17, 56, fw, sv.freqann)%>

	<%=smartHF.getLit(19, 39, generatedText24)%>

	<%=smartHF.getHTMLVar(19, 56, fw, sv.statuz)%>

	<%=smartHF.getLit(20, 39, generatedText25)%>

	<%=smartHF.getHTMLVar(20, 56, fw, sv.resamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>









<%}%>

<%if (sv.S5615protectWritten.gt(0)) {%>
	<%S5615protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
