<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5161";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>

<%S5161ScreenVars sv = (S5161ScreenVars) fw.getVariables();%>

<%if (sv.S5161screenWritten.gt(0)) {%>
	<%S5161screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Amount      Annual Ammount");%>
<%	generatedText4.appendClassString("label_txt");
	generatedText4.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency Loading             ");%>
	<%sv.instamnt01.setClassString("");%>
<%	sv.instamnt01.appendClassString("num_fld");
	sv.instamnt01.appendClassString("output_txt");
	sv.instamnt01.appendClassString("highlight");
%>
	<%sv.annamnt01.setClassString("");%>
<%	sv.annamnt01.appendClassString("num_fld");
	sv.annamnt01.appendClassString("output_txt");
	sv.annamnt01.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net Premium 1                 ");%>
	<%sv.instamnt02.setClassString("");%>
<%	sv.instamnt02.appendClassString("num_fld");
	sv.instamnt02.appendClassString("output_txt");
	sv.instamnt02.appendClassString("highlight");
%>
	<%sv.annamnt02.setClassString("");%>
<%	sv.annamnt02.appendClassString("num_fld");
	sv.annamnt02.appendClassString("output_txt");
	sv.annamnt02.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net Premium 2                 ");%>
	<%sv.instamnt03.setClassString("");%>
<%	sv.instamnt03.appendClassString("num_fld");
	sv.instamnt03.appendClassString("output_txt");
	sv.instamnt03.appendClassString("highlight");
%>
	<%sv.annamnt03.setClassString("");%>
<%	sv.annamnt03.appendClassString("num_fld");
	sv.annamnt03.appendClassString("output_txt");
	sv.annamnt03.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Alpha Loading 1               ");%>
	<%sv.instamnt05.setClassString("");%>
<%	sv.instamnt05.appendClassString("num_fld");
	sv.instamnt05.appendClassString("output_txt");
	sv.instamnt05.appendClassString("highlight");
%>
	<%sv.annamnt05.setClassString("");%>
<%	sv.annamnt05.appendClassString("num_fld");
	sv.annamnt05.appendClassString("output_txt");
	sv.annamnt05.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Alpha Loading 2               ");%>
	<%sv.instamnt06.setClassString("");%>
<%	sv.instamnt06.appendClassString("num_fld");
	sv.instamnt06.appendClassString("output_txt");
	sv.instamnt06.appendClassString("highlight");
%>
	<%sv.annamnt06.setClassString("");%>
<%	sv.annamnt06.appendClassString("num_fld");
	sv.annamnt06.appendClassString("output_txt");
	sv.annamnt06.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Beta  Loading 1               ");%>
	<%sv.instamnt08.setClassString("");%>
<%	sv.instamnt08.appendClassString("num_fld");
	sv.instamnt08.appendClassString("output_txt");
	sv.instamnt08.appendClassString("highlight");
%>
	<%sv.annamnt08.setClassString("");%>
<%	sv.annamnt08.appendClassString("num_fld");
	sv.annamnt08.appendClassString("output_txt");
	sv.annamnt08.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Beta  Loading 2               ");%>
	<%sv.instamnt09.setClassString("");%>
<%	sv.instamnt09.appendClassString("num_fld");
	sv.instamnt09.appendClassString("output_txt");
	sv.instamnt09.appendClassString("highlight");
%>
	<%sv.annamnt09.setClassString("");%>
<%	sv.annamnt09.appendClassString("num_fld");
	sv.annamnt09.appendClassString("output_txt");
	sv.annamnt09.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Gamma Loading 1               ");%>
	<%sv.instamnt11.setClassString("");%>
<%	sv.instamnt11.appendClassString("num_fld");
	sv.instamnt11.appendClassString("output_txt");
	sv.instamnt11.appendClassString("highlight");
%>
	<%sv.annamnt11.setClassString("");%>
<%	sv.annamnt11.appendClassString("num_fld");
	sv.annamnt11.appendClassString("output_txt");
	sv.annamnt11.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Gamma Loading 2               ");%>
	<%sv.instamnt12.setClassString("");%>
<%	sv.instamnt12.appendClassString("num_fld");
	sv.instamnt12.appendClassString("output_txt");
	sv.instamnt12.appendClassString("highlight");
%>
	<%sv.annamnt12.setClassString("");%>
<%	sv.annamnt12.appendClassString("num_fld");
	sv.annamnt12.appendClassString("output_txt");
	sv.annamnt12.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Standard Loading 1        ");%>
	<%sv.instamnt14.setClassString("");%>
<%	sv.instamnt14.appendClassString("num_fld");
	sv.instamnt14.appendClassString("output_txt");
	sv.instamnt14.appendClassString("highlight");
%>
	<%sv.annamnt14.setClassString("");%>
<%	sv.annamnt14.appendClassString("num_fld");
	sv.annamnt14.appendClassString("output_txt");
	sv.annamnt14.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Standard Loading 2        ");%>
	<%sv.instamnt15.setClassString("");%>
<%	sv.instamnt15.appendClassString("num_fld");
	sv.instamnt15.appendClassString("output_txt");
	sv.instamnt15.appendClassString("highlight");
%>
	<%sv.annamnt15.setClassString("");%>
<%	sv.annamnt15.appendClassString("num_fld");
	sv.annamnt15.appendClassString("output_txt");
	sv.annamnt15.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"High Sum Assured Rebate       ");%>
	<%sv.instamnt17.setClassString("");%>
<%	sv.instamnt17.appendClassString("num_fld");
	sv.instamnt17.appendClassString("output_txt");
	sv.instamnt17.appendClassString("highlight");
%>
	<%sv.annamnt17.setClassString("");%>
<%	sv.annamnt17.appendClassString("num_fld");
	sv.annamnt17.appendClassString("output_txt");
	sv.annamnt17.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reserve Injection Discount    ");%>
	<%sv.instamnt25.setClassString("");%>
<%	sv.instamnt25.appendClassString("num_fld");
	sv.instamnt25.appendClassString("output_txt");
	sv.instamnt25.appendClassString("highlight");
%>
	<%sv.annamnt25.setClassString("");%>
<%	sv.annamnt25.appendClassString("num_fld");
	sv.annamnt25.appendClassString("output_txt");
	sv.annamnt25.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Totals    ");%>
	<%sv.grossprem.setClassString("");%>
<%	sv.grossprem.appendClassString("num_fld");
	sv.grossprem.appendClassString("output_txt");
	sv.grossprem.appendClassString("highlight");
%>
	<%sv.agrossprem.setClassString("");%>
<%	sv.agrossprem.appendClassString("num_fld");
	sv.agrossprem.appendClassString("output_txt");
	sv.agrossprem.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.instamnt04.setClassString("");%>
	<%sv.instamnt07.setClassString("");%>
	<%sv.instamnt10.setClassString("");%>
	<%sv.instamnt13.setClassString("");%>
	<%sv.instamnt16.setClassString("");%>
	<%sv.instamnt18.setClassString("");%>
	<%sv.instamnt19.setClassString("");%>
	<%sv.instamnt20.setClassString("");%>
	<%sv.instamnt21.setClassString("");%>
	<%sv.instamnt22.setClassString("");%>
	<%sv.instamnt23.setClassString("");%>
	<%sv.instamnt24.setClassString("");%>
	<%sv.annamnt04.setClassString("");%>
	<%sv.annamnt07.setClassString("");%>
	<%sv.annamnt10.setClassString("");%>
	<%sv.annamnt13.setClassString("");%>
	<%sv.annamnt16.setClassString("");%>
	<%sv.annamnt18.setClassString("");%>
	<%sv.annamnt19.setClassString("");%>
	<%sv.annamnt20.setClassString("");%>
	<%sv.annamnt21.setClassString("");%>
	<%sv.annamnt22.setClassString("");%>
	<%sv.annamnt23.setClassString("");%>
	<%sv.annamnt24.setClassString("");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(3, 3, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 16, fw, sv.chdrnum)%>

	<%=smartHF.getLit(4, 3, generatedText3)%>

	<%=smartHF.getHTMLVar(4, 12, fw, sv.life)%>

	<%=smartHF.getLit(4, 19, generatedText15)%>

	<%=smartHF.getHTMLVar(4, 32, fw, sv.coverage)%>

	<%=smartHF.getLit(4, 39, generatedText16)%>

	<%=smartHF.getHTMLVar(4, 49, fw, sv.rider)%>

	<%=smartHF.getLit(6, 39, generatedText4)%>

	<%=smartHF.getLit(8, 5, generatedText5)%>

	<%=smartHF.getHTMLVar(8, 38, fw, sv.instamnt01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(8, 58, fw, sv.annamnt01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(9, 5, generatedText6)%>

	<%=smartHF.getHTMLVar(9, 38, fw, sv.instamnt02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 58, fw, sv.annamnt02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(10, 5, generatedText7)%>

	<%=smartHF.getHTMLVar(10, 38, fw, sv.instamnt03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(10, 58, fw, sv.annamnt03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(11, 5, generatedText8)%>

	<%=smartHF.getHTMLVar(11, 38, fw, sv.instamnt05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 58, fw, sv.annamnt05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(12, 5, generatedText17)%>

	<%=smartHF.getHTMLVar(12, 38, fw, sv.instamnt06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 58, fw, sv.annamnt06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(13, 5, generatedText9)%>

	<%=smartHF.getHTMLVar(13, 38, fw, sv.instamnt08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 58, fw, sv.annamnt08, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(14, 5, generatedText18)%>

	<%=smartHF.getHTMLVar(14, 38, fw, sv.instamnt09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 58, fw, sv.annamnt09, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(15, 5, generatedText10)%>

	<%=smartHF.getHTMLVar(15, 38, fw, sv.instamnt11, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 58, fw, sv.annamnt11, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(16, 5, generatedText11)%>

	<%=smartHF.getHTMLVar(16, 38, fw, sv.instamnt12, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 58, fw, sv.annamnt12, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(17, 5, generatedText12)%>

	<%=smartHF.getHTMLVar(17, 38, fw, sv.instamnt14,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 58, fw, sv.annamnt14, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(18, 5, generatedText13)%>

	<%=smartHF.getHTMLVar(18, 38, fw, sv.instamnt15, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 58, fw, sv.annamnt15, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(19, 5, generatedText14)%>

	<%=smartHF.getHTMLVar(19, 38, fw, sv.instamnt17, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 58, fw, sv.annamnt17, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(20, 5, generatedText20)%>

	<%=smartHF.getHTMLVar(20, 37, fw, sv.instamnt25, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 57, fw, sv.annamnt25, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(22, 25, generatedText19)%>

	<%=smartHF.getHTMLVar(22, 37, fw, sv.grossprem, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(22, 57, fw, sv.agrossprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>




























<%}%>

<%if (sv.S5161protectWritten.gt(0)) {%>
	<%S5161protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>



<%@ include file="/POLACommon2.jsp"%>
