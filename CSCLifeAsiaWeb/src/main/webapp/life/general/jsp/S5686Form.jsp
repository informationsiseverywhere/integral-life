<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5686";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>
<%S5686ScreenVars sv = (S5686ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company "))%>
					</label>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.company)%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table "))%>
					</label>
					<%=smartHF.getHTMLVarReadOnly(fw, sv.tabl)%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item "))%>
					</label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%=smartHF.getHTMLVarReadOnly(fw, sv.item)%>
						</td><td style="max-width: 205px;">
						<%=smartHF.getHTMLVarReadOnly(fw, sv.longdesc, 1)%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
				<%StringData SUN_Dim1_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"SUN Dimension");%>
					<label style="padding-top:5px;"><%=smartHF.getLit(0, 0, SUN_Dim1_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					</label>
					
					
					</div></div>
					
					<div class="col-md-2">
				<div class="form-group">
				<%StringData SUN_PRODTYP=resourceBundleHandler.gettingValueFromBundle(StringData.class,"1) Product Type :");%>
					<label style="padding-top: 4px;white-space: nowrap;">1) Product Type :
					</label>
					
					
					</div></div>
						<div class="col-md-2">
				<div class="form-group">
				<%=smartHF.getHTMLVarReadOnly(fw, sv.zdmsion)%>
				</div></div>
					</div>
		
		
		</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>

