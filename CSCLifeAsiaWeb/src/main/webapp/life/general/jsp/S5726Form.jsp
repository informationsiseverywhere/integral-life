<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5726";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*"%>
<%
	S5726ScreenVars sv = (S5726ScreenVars) fw.getVariables();
%>

<%
	if (sv.S5726screenWritten.gt(0)) {
%>
<%
	S5726screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Slots To Be Excluded");
%>
<%
	generatedText5.appendClassString("screen_header");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Frequency Loading   Slot  1  ");
%>
<%
	sv.indic01.setClassString("");
%>
<%
	sv.indic01.appendClassString("string_fld");
		sv.indic01.appendClassString("input_txt");
		sv.indic01.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subst. Loading 1    Slot 14  ");
%>
<%
	sv.indic14.setClassString("");
%>
<%
	sv.indic14.appendClassString("string_fld");
		sv.indic14.appendClassString("input_txt");
		sv.indic14.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Net Premium 1       Slot  2  ");
%>
<%
	sv.indic02.setClassString("");
%>
<%
	sv.indic02.appendClassString("string_fld");
		sv.indic02.appendClassString("input_txt");
		sv.indic02.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subst. Loading 2    Slot 15  ");
%>
<%
	sv.indic15.setClassString("");
%>
<%
	sv.indic15.appendClassString("string_fld");
		sv.indic15.appendClassString("input_txt");
		sv.indic15.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Net Premium 2       Slot  3  ");
%>
<%
	sv.indic03.setClassString("");
%>
<%
	sv.indic03.appendClassString("string_fld");
		sv.indic03.appendClassString("input_txt");
		sv.indic03.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Subst. Loading 3    Slot 16  ");
%>
<%
	sv.indic16.setClassString("");
%>
<%
	sv.indic16.appendClassString("string_fld");
		sv.indic16.appendClassString("input_txt");
		sv.indic16.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Net Premium 3       Slot  4  ");
%>
<%
	sv.indic04.setClassString("");
%>
<%
	sv.indic04.appendClassString("string_fld");
		sv.indic04.appendClassString("input_txt");
		sv.indic04.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"High Sum Rebate     Slot 17  ");
%>
<%
	sv.indic17.setClassString("");
%>
<%
	sv.indic17.appendClassString("string_fld");
		sv.indic17.appendClassString("input_txt");
		sv.indic17.appendClassString("highlight");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Alpha Loading 1     Slot  5  ");
%>
<%
	sv.indic05.setClassString("");
%>
<%
	sv.indic05.appendClassString("string_fld");
		sv.indic05.appendClassString("input_txt");
		sv.indic05.appendClassString("highlight");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Staff Discount      Slot 18  ");
%>
<%
	sv.indic18.setClassString("");
%>
<%
	sv.indic18.appendClassString("string_fld");
		sv.indic18.appendClassString("input_txt");
		sv.indic18.appendClassString("highlight");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Alpha Loading 2     Slot  6  ");
%>
<%
	sv.indic06.setClassString("");
%>
<%
	sv.indic06.appendClassString("string_fld");
		sv.indic06.appendClassString("input_txt");
		sv.indic06.appendClassString("highlight");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 19  ");
%>
<%
	sv.indic19.setClassString("");
%>
<%
	sv.indic19.appendClassString("string_fld");
		sv.indic19.appendClassString("input_txt");
		sv.indic19.appendClassString("highlight");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Alpha Loading 3     Slot  7  ");
%>
<%
	sv.indic07.setClassString("");
%>
<%
	sv.indic07.appendClassString("string_fld");
		sv.indic07.appendClassString("input_txt");
		sv.indic07.appendClassString("highlight");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 20  ");
%>
<%
	sv.indic20.setClassString("");
%>
<%
	sv.indic20.appendClassString("string_fld");
		sv.indic20.appendClassString("input_txt");
		sv.indic20.appendClassString("highlight");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Beta Loading  1     Slot  8  ");
%>
<%
	sv.indic08.setClassString("");
%>
<%
	sv.indic08.appendClassString("string_fld");
		sv.indic08.appendClassString("input_txt");
		sv.indic08.appendClassString("highlight");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 21  ");
%>
<%
	sv.indic21.setClassString("");
%>
<%
	sv.indic21.appendClassString("string_fld");
		sv.indic21.appendClassString("input_txt");
		sv.indic21.appendClassString("highlight");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Beta Loading  2     Slot  9  ");
%>
<%
	sv.indic09.setClassString("");
%>
<%
	sv.indic09.appendClassString("string_fld");
		sv.indic09.appendClassString("input_txt");
		sv.indic09.appendClassString("highlight");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 22  ");
%>
<%
	sv.indic22.setClassString("");
%>
<%
	sv.indic22.appendClassString("string_fld");
		sv.indic22.appendClassString("input_txt");
		sv.indic22.appendClassString("highlight");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Beta Loading  3     Slot 10  ");
%>
<%
	sv.indic10.setClassString("");
%>
<%
	sv.indic10.appendClassString("string_fld");
		sv.indic10.appendClassString("input_txt");
		sv.indic10.appendClassString("highlight");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 23  ");
%>
<%
	sv.indic23.setClassString("");
%>
<%
	sv.indic23.appendClassString("string_fld");
		sv.indic23.appendClassString("input_txt");
		sv.indic23.appendClassString("highlight");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Gamma Loading 1     Slot 11  ");
%>
<%
	sv.indic11.setClassString("");
%>
<%
	sv.indic11.appendClassString("string_fld");
		sv.indic11.appendClassString("input_txt");
		sv.indic11.appendClassString("highlight");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Spare               Slot 24  ");
%>
<%
	sv.indic24.setClassString("");
%>
<%
	sv.indic24.appendClassString("string_fld");
		sv.indic24.appendClassString("input_txt");
		sv.indic24.appendClassString("highlight");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Gamma Loading 2     Slot 12  ");
%>
<%
	sv.indic12.setClassString("");
%>
<%
	sv.indic12.appendClassString("string_fld");
		sv.indic12.appendClassString("input_txt");
		sv.indic12.appendClassString("highlight");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Reserve Injection   Slot 25  ");
%>
<%
	sv.indic25.setClassString("");
%>
<%
	sv.indic25.appendClassString("string_fld");
		sv.indic25.appendClassString("input_txt");
		sv.indic25.appendClassString("highlight");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Gamma Loading 3     Slot 13  ");
%>
<%
	sv.indic13.setClassString("");
%>
<%
	sv.indic13.appendClassString("string_fld");
		sv.indic13.appendClassString("input_txt");
		sv.indic13.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarExt(fw, sv.tabl)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.item)%>

						<%=smartHF.getHTMLVarExt(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText5)%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText6)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic01)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic14)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText8)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic02)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic15)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic03)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText11)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic16)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText12)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic04)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText13)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic17)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText14)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic05)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText15)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic18)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText16)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic06)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText17)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic19)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText18)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic07)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText19)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic20)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText20)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic08)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText21)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic21)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText22)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic09)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText23)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic22)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText24)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic10)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText25)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic23)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText26)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic11)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText27)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic24)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText28)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic12)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText29)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic25)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText30)%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.indic13)%></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S5726protectWritten.gt(0)) {
%>
<%
	S5726protect.clearClassString(sv);
%>
<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>