<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5616";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S5616ScreenVars sv = (S5616ScreenVars) fw.getVariables();%>

<%if (sv.S5616screenWritten.gt(0)) {%>
	<%S5616screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract       ");%>
	<%sv.chdrsel.setClassString("");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.pstatcode.setClassString("");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Curr  ");%>
	<%sv.currcd.setClassString("");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium ");%>
	<%sv.singp.setClassString("");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Freq   ");%>
	<%sv.billfreq.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cover Start Dt ");%>
	<%sv.crrcdDisp.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage       ");%>
	<%sv.coverage.setClassString("");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Calc To Date   ");%>
	<%sv.ptdateDisp.setClassString("");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.rider.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage Name  ");%>
	<%sv.crtable.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life       ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%sv.life.setClassString("");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status          ");%>
<%	generatedText16.appendClassString("label_txt");
	generatedText16.appendClassString("highlight");
%>
	<%sv.statuz.setClassString("");%>
<%	sv.statuz.appendClassString("string_fld");
	sv.statuz.appendClassString("output_txt");
	sv.statuz.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life     ");%>
	<%sv.jlife.setClassString("");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual Value    ");%>
<%	generatedText17.appendClassString("label_txt");
	generatedText17.appendClassString("highlight");
%>
	<%sv.resamt.setClassString("");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Adjust  ");%>
<%	generatedText18.appendClassString("label_txt");
	generatedText18.appendClassString("highlight");
%>
	<%sv.otheradjst.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-----------------------");%>
<%	generatedText20.appendClassString("label_txt");
	generatedText20.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Surrender Value ");%>
<%	generatedText19.appendClassString("label_txt");
	generatedText19.appendClassString("highlight");
%>
	<%sv.clamant.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.currcode.setClassString("");%>
	<%sv.effdateDisp.setClassString("");%>
	<%sv.language.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.pstatcode.setReverse(BaseScreenData.REVERSED);
			sv.pstatcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.pstatcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.singp.setReverse(BaseScreenData.REVERSED);
			sv.singp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.singp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.coverage.setReverse(BaseScreenData.REVERSED);
			sv.coverage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.coverage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.rider.setReverse(BaseScreenData.REVERSED);
			sv.rider.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rider.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.crtable.setReverse(BaseScreenData.REVERSED);
			sv.crtable.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.crrcdDisp.setReverse(BaseScreenData.REVERSED);
			sv.crrcdDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.crrcdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.life.setReverse(BaseScreenData.REVERSED);
			sv.life.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.life.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.clamant.setReverse(BaseScreenData.REVERSED);
			sv.clamant.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.clamant.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.resamt.setReverse(BaseScreenData.REVERSED);
			sv.resamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.resamt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(4, 4, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(4, 21, fw, sv.chdrsel)%>
	<%=smartHF.getHTMLF4NSVar(4, 21, fw, sv.chdrsel)%>

	<%=smartHF.getLit(5, 38, generatedText3)%>

	<%=smartHF.getHTMLVar(5, 55, fw, sv.pstatcode)%>

	<%=smartHF.getLit(6, 4, generatedText4)%>

	<%=smartHF.getHTMLSpaceVar(6, 21, fw, sv.currcd)%>
	<%=smartHF.getHTMLF4NSVar(6, 21, fw, sv.currcd)%>

	<%=smartHF.getLit(6, 38, generatedText7)%>

	<%=smartHF.getHTMLVar(6, 55, fw, sv.singp, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>

	<%=smartHF.getLit(7, 4, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(7, 21, fw, sv.billfreq)%>
	<%=smartHF.getHTMLF4NSVar(7, 21, fw, sv.billfreq)%>

	<%=smartHF.getLit(8, 38, generatedText13)%>

	<%=smartHF.getHTMLSpaceVar(8, 55, fw, sv.crrcdDisp)%>
	<%=smartHF.getHTMLCalNSVar(8, 55, fw, sv.crrcdDisp)%>

	<%=smartHF.getLit(9, 4, generatedText8)%>

	<%=smartHF.getHTMLVar(9, 21, fw, sv.coverage)%>

	<%=smartHF.getLit(9, 38, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(9, 55, fw, sv.ptdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(9, 55, fw, sv.ptdateDisp)%>

	<%=smartHF.getLit(10, 4, generatedText9)%>

	<%=smartHF.getLit(10, 18, generatedText10)%>

	<%=smartHF.getHTMLVar(10, 21, fw, sv.rider)%>

	<%=smartHF.getLit(11, 4, generatedText11)%>

	<%=smartHF.getHTMLSpaceVar(11, 21, fw, sv.crtable)%>
	<%=smartHF.getHTMLF4NSVar(11, 21, fw, sv.crtable)%>

	<%=smartHF.getLit(13, 4, generatedText14)%>

	<%=smartHF.getLit(13, 18, generatedText15)%>

	<%=smartHF.getHTMLVar(13, 21, fw, sv.life)%>

	<%=smartHF.getLit(13, 38, generatedText16)%>

	<%=smartHF.getHTMLVar(13, 55, fw, sv.statuz)%>

	<%=smartHF.getLit(14, 4, generatedText12)%>

	<%=smartHF.getHTMLVar(14, 21, fw, sv.jlife)%>

	<%=smartHF.getLit(14, 38, generatedText17)%>

	<%=smartHF.getHTMLVar(14, 55, fw, sv.resamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>

	<%=smartHF.getLit(15, 38, generatedText18)%>

	<%=smartHF.getHTMLVar(15, 55, fw, sv.otheradjst, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>

	<%=smartHF.getLit(16, 55, generatedText20)%>

	<%=smartHF.getLit(17, 38, generatedText19)%>

	<%=smartHF.getHTMLVar(17, 55, fw, sv.clamant, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>







<%}%>

<%if (sv.S5616protectWritten.gt(0)) {%>
	<%S5616protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<%@ include file="/POLACommon2.jsp"%>
