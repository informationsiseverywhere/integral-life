<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5656";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>

<%S5656ScreenVars sv = (S5656ScreenVars) fw.getVariables();%>

<%if (sv.S5656screenWritten.gt(0)) {%>
	<%S5656screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = new StringData("Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = new StringData("Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = new StringData("Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = new StringData("to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = new StringData("Bonus Multiplication Factors");%>
<%	generatedText7.appendClassString("label_txt");
	generatedText7.appendClassString("highlight");
%>
	<%StringData generatedText8 = new StringData("Bonus Wait Period ");%>
	<%sv.termMin.setClassString("");%>
<%	sv.termMin.appendClassString("num_fld");
	sv.termMin.appendClassString("input_txt");
	sv.termMin.appendClassString("highlight");
%>
	<%StringData generatedText9 = new StringData("Last Anniversary Reserve    ");%>
	<%sv.sumbfac01.setClassString("");%>
<%	sv.sumbfac01.appendClassString("num_fld");
	sv.sumbfac01.appendClassString("input_txt");
	sv.sumbfac01.appendClassString("highlight");
%>
	<%StringData generatedText10 = new StringData("This Anniversary Reserve    ");%>
	<%sv.sumbfac02.setClassString("");%>
<%	sv.sumbfac02.appendClassString("num_fld");
	sv.sumbfac02.appendClassString("input_txt");
	sv.sumbfac02.appendClassString("highlight");
%>
	<%StringData generatedText11 = new StringData("Next Anniversary Reserve    ");%>
	<%sv.sumbfac03.setClassString("");%>
<%	sv.sumbfac03.appendClassString("num_fld");
	sv.sumbfac03.appendClassString("input_txt");
	sv.sumbfac03.appendClassString("highlight");
%>
	<%StringData generatedText12 = new StringData("Net Annual Premium          ");%>
	<%sv.sumbfac04.setClassString("");%>
<%	sv.sumbfac04.appendClassString("num_fld");
	sv.sumbfac04.appendClassString("input_txt");
	sv.sumbfac04.appendClassString("highlight");
%>
	<%StringData generatedText13 = new StringData("Annualised policy Fee       ");%>
	<%sv.sumbfac05.setClassString("");%>
<%	sv.sumbfac05.appendClassString("num_fld");
	sv.sumbfac05.appendClassString("input_txt");
	sv.sumbfac05.appendClassString("highlight");
%>
	<%StringData generatedText14 = new StringData("Sum Assured/Annual Benefit  ");%>
	<%sv.sumbfac06.setClassString("");%>
<%	sv.sumbfac06.appendClassString("num_fld");
	sv.sumbfac06.appendClassString("input_txt");
	sv.sumbfac06.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(2.5, 2, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3.5, 2, fw, sv.company)%>
	<%=smartHF.getHTMLF4NSVar(3, 12, fw, sv.company)%>

	<%=smartHF.getLit(2.5, 33, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(3.5, 33, fw, sv.tabl)%>
	<%=smartHF.getHTMLF4NSVar(3, 24, fw, sv.tabl)%>

	<%=smartHF.getLit(2.5, 63, generatedText4)%>

	<%=smartHF.getHTMLVar(3.5, 63, fw, sv.item)%>
<!--  ILIFE 2560 starts-->
	<%=smartHF.getHTMLVar(3.5, 72, fw, sv.longdesc).replaceAll("240px","64px")%>
<!--  ILIFE 2560 ends-->
	<%=smartHF.getLit(5, 2, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(6, 2, fw, sv.itmfrmDisp)%>
	<%=smartHF.getHTMLCalNSVar(5, 24, fw, sv.itmfrmDisp)%>

	<%=smartHF.getLit(6, 14, generatedText6)%>
<!--  ILIFE 2560 starts-->
	<%=smartHF.getHTMLSpaceVar(6, 16, fw, sv.itmtoDisp).replace("80px","27px")%>
	<%=smartHF.getHTMLCalNSVar(5, 40, fw, sv.itmtoDisp)%>
<!--  ILIFE 2560 ends-->
	<%=smartHF.getLit(8, 2, generatedText7)%>

	<%=smartHF.getLit(10, 2, generatedText8)%>

	<%=smartHF.getHTMLVar(10, 33, fw, sv.termMin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN).replaceAll("24px","72px")%>

	<%=smartHF.getLit(11, 2, generatedText9)%>

	<%=smartHF.getHTMLVar(11, 33, fw, sv.sumbfac01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(12, 2, generatedText10)%>

	<%=smartHF.getHTMLVar(12, 33, fw, sv.sumbfac02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(13, 2, generatedText11)%>

	<%=smartHF.getHTMLVar(13, 33, fw, sv.sumbfac03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(14, 2, generatedText12)%>

	<%=smartHF.getHTMLVar(14, 33, fw, sv.sumbfac04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(15, 2, generatedText13)%>

	<%=smartHF.getHTMLVar(15, 33, fw, sv.sumbfac05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(16, 2, generatedText14)%>

	<%=smartHF.getHTMLVar(16, 33, fw, sv.sumbfac06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>




<%}%>

<%if (sv.S5656protectWritten.gt(0)) {%>
	<%S5656protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<style>
       #item{
              text-align:right;
       }
       #longdesc{
              text-align:right;
       }
</style>

<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>


<%@ include file="/POLACommon2.jsp"%>
