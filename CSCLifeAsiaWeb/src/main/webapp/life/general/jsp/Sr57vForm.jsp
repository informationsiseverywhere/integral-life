<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR57V";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*"%>
<%
	Sr57vScreenVars sv = (Sr57vScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<div class="panel panel-default">
	<div class="panel-body">


		<div class="row">

			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData COMPANY_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company");
						%>
						<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>

					<%=smartHF.getRichText(0, 0, fw, sv.company, (sv.company.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width").replace(" bold", "")
					.replace("input_cell", "bold_cell")%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData TABL_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table");
						%>
						<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>

					<%=smartHF.getRichText(0, 0, fw, sv.tabl, (sv.tabl.getLength() + 1), null).replace("absolute",
					"relative")%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData ITEM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item");
						%>
						<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<table>
						<tr>
							<td><%=smartHF.getRichText(0, 0, fw, sv.item, (sv.item.getLength() + 1), null).replace("absolute",
					"relative")%></td>
							<td style="padding-left: 1px;"><%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.longdesc.getLength() + 1), null)
					.replace("absolute", "relative")%></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
		<br>
		<div class="row">


			<div class="col-md-3">
				<div class="form-group">

					<label style="padding-top: 20px;">
						<%
							StringData Dimension_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Dimension");
						%>
						<%=smartHF.getLit(0, 0, Dimension_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<label style="padding-top: 20px;">
						<%
							StringData ExtrRoutine_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Extr Routine");
						%>
						<%=smartHF.getLit(0, 0, ExtrRoutine_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<label style="padding-top: 20px;">
						<%
							StringData DefaultValue_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"Default Value");
						%>
						<%=smartHF.getLit(0, 0, DefaultValue_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData ProductType_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"1) Product Type");
						%>
						<%=smartHF.getLit(0, 0, ProductType_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">

					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn01, (sv.subrtn01.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">

					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion01, (sv.zdmsion01.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

		</div>
		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData ProductClass_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"2) Product Class");
						%>
						<%=smartHF.getLit(0, 0, ProductClass_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn02, (sv.subrtn02.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion02, (sv.zdmsion02.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
		</div>

		<div class="row">


			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData ProductName_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"3) Product Name");
						%>
						<%=smartHF.getLit(0, 0, ProductName_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn03, (sv.subrtn03.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion03, (sv.zdmsion03.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData DistributionC_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"4) Distribution Channel");
						%>
						<%=smartHF.getLit(0, 0, DistributionC_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn04, (sv.subrtn04.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion04, (sv.zdmsion04.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData Bran_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "5) Work Area/Branch");
						%>
						<%=smartHF.getLit(0, 0, Bran_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn05, (sv.subrtn05.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion05, (sv.zdmsion05.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData Transaction_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"6) Transaction");
						%>
						<%=smartHF.getLit(0, 0, Transaction_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn06, (sv.subrtn06.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion06, (sv.zdmsion06.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>


		</div>

		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label style="padding-top: 20px;">
						<%
							StringData Tax_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "7) Tax");
						%>
						<%=smartHF.getLit(0, 0, Tax_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.subrtn07, (sv.subrtn07.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion07, (sv.zdmsion07.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#subrtn01").width(100);
		$("#subrtn02").width(100);
		$("#subrtn03").width(100);
		$("#subrtn04").width(100);
		$("#subrtn05").width(100);
		$("#subrtn06").width(100);
		$("#subrtn07").width(100);
		$("#zdmsion01").width(140);
		$("#zdmsion02").width(140);
		$("#zdmsion03").width(140);
		$("#zdmsion04").width(140);
		$("#zdmsion05").width(140);
		$("#zdmsion06").width(140);
		$("#zdmsion07").width(140);

	});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

