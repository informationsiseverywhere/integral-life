<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6005";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*"%>
<%
	S6005ScreenVars sv = (S6005ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6005screenWritten.gt(0)) {
%>
<%
	S6005screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	sv.company.setClassString("");
%>
<%
	sv.company.appendClassString("string_fld");
		sv.company.appendClassString("output_txt");
		sv.company.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	sv.tabl.setClassString("");
%>
<%
	sv.tabl.appendClassString("string_fld");
		sv.tabl.appendClassString("output_txt");
		sv.tabl.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	sv.item.setClassString("");
%>
<%
	sv.item.appendClassString("string_fld");
		sv.item.appendClassString("output_txt");
		sv.item.appendClassString("highlight");
%>
<%
	sv.longdesc.setClassString("");
%>
<%
	sv.longdesc.appendClassString("string_fld");
		sv.longdesc.appendClassString("output_txt");
		sv.longdesc.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Valid Methods  ");
%>
<%
	sv.bappmeth01.setClassString("");
%>
<%
	sv.bappmeth01.appendClassString("string_fld");
		sv.bappmeth01.appendClassString("input_txt");
		sv.bappmeth01.appendClassString("highlight");
%>
<%
	sv.bappmeth02.setClassString("");
%>
<%
	sv.bappmeth02.appendClassString("string_fld");
		sv.bappmeth02.appendClassString("input_txt");
		sv.bappmeth02.appendClassString("highlight");
%>
<%
	sv.bappmeth03.setClassString("");
%>
<%
	sv.bappmeth03.appendClassString("string_fld");
		sv.bappmeth03.appendClassString("input_txt");
		sv.bappmeth03.appendClassString("highlight");
%>
<%
	sv.bappmeth04.setClassString("");
%>
<%
	sv.bappmeth04.appendClassString("string_fld");
		sv.bappmeth04.appendClassString("input_txt");
		sv.bappmeth04.appendClassString("highlight");
%>
<%
	sv.bappmeth05.setClassString("");
%>
<%
	sv.bappmeth05.appendClassString("string_fld");
		sv.bappmeth05.appendClassString("input_txt");
		sv.bappmeth05.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Default Indic  ");
%>
<%
	sv.ind.setClassString("");
%>
<%
	sv.ind.appendClassString("string_fld");
		sv.ind.appendClassString("input_txt");
		sv.ind.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"End Of Term Bonus Defaults");
%>
<%
	generatedText7.appendClassString("label_txt");
		generatedText7.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Mid-Term Surrender Pay Client ");
%>
<%
	sv.indic01.setClassString("");
%>
<%
	sv.indic01.appendClassString("string_fld");
		sv.indic01.appendClassString("input_txt");
		sv.indic01.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Death Claim Pay Client        ");
%>
<%
	sv.indic02.setClassString("");
%>
<%
	sv.indic02.appendClassString("string_fld");
		sv.indic02.appendClassString("input_txt");
		sv.indic02.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Paid-Up Increase PUP Value    ");
%>
<%
	sv.indic03.setClassString("");
%>
<%
	sv.indic03.appendClassString("string_fld");
		sv.indic03.appendClassString("input_txt");
		sv.indic03.appendClassString("highlight");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText2)%></label>
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.company)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.company)%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText3)%></label>
					<div style="width: 100px;">
						<%=smartHF.getHTMLVarExt(fw, sv.tabl)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.tabl)%>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText4)%></label>
					<div class="input-group three-controller">
						<%=smartHF.getHTMLVarExt(fw, sv.item)%>

						<%=smartHF.getHTMLVarExt(fw, sv.longdesc)%>
					</div>
				</div>
			</div>
		</div>
		<br />

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText5)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth01, (sv.bappmeth01.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('bappmeth01')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth02, (sv.bappmeth02.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('bappmeth02')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth03, (sv.bappmeth03.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('bappmeth03')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth04, (sv.bappmeth04.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('bappmeth04')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="input-group">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.bappmeth05, (sv.bappmeth05.getLength()))%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; height: 34px; border-radius: 0px 0px 0px 0px;"
								type="button"
								onclick="doFocus(document.getElementById('bappmeth05')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label style="margin-top: 7px;"><%=smartHF.getLit(generatedText6)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;"><%=smartHF.getHTMLVarExt(fw, sv.ind)%></div>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText7)%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText8)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.indic01)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText9)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.indic02)%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLit(generatedText10)%></label>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div style="width: 70px;">
						<%=smartHF.getHTMLVarExt(fw, sv.indic03)%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%
	}
%>

<%
	if (sv.S6005protectWritten.gt(0)) {
%>
<%
	S6005protect.clearClassString(sv);
%>

<%
	}
%>


<%@ include file="/POLACommon2NEW.jsp"%>