<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR57W";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*"%>
<%
	Sr57wScreenVars sv = (Sr57wScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<div class="panel panel-default">
	<div class="panel-body">


		<div class="row">

			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData COMPANY_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company");
						%>
						<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>

					<%=smartHF.getRichText(0, 0, fw, sv.company, (sv.company.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width").replace(" bold", "")
					.replace("input_cell", "bold_cell")%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData TABL_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table");
						%>
						<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>

					<%=smartHF.getRichText(0, 0, fw, sv.tabl, (sv.tabl.getLength() + 1), null).replace("absolute",
					"relative")%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label>
						<%
							StringData ITEM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item");
						%>
						<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>
					<table>
						<tr>
							<td><%=smartHF.getRichText(0, 0, fw, sv.item, (sv.item.getLength() + 1), null).replace("absolute",
					"relative")%></td>
							<td style="padding-left: 1px;"><%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.longdesc.getLength() + 1), null)
					.replace("absolute", "relative")%></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
		<br>
		<div class="row">

			<div class="col-md-2">
				<div class="form-group">
					<label style="padding-top: 4px; white-space: nowrap;"> <%
 	StringData SUN_Dim1_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "SUN Dimension");
 %>
						<%=smartHF.getLit(0, 0, SUN_Dim1_LBL).replace("absolute;", "relative; font-weight: bold;")%></label>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label style="padding-top: 4px; white-space: nowrap;">
						<%
							StringData SUN_PRODTYP = resourceBundleHandler.gettingValueFromBundle(StringData.class,
									"1) Product Type :");
						%>
						<%=smartHF.getLit(0, 0, SUN_PRODTYP).replace("absolute;", "relative; font-weight: bold;")%>
					</label>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion01, (sv.zdmsion01.getLength() + 1), null)
					.replace("absolute", "relative")%>

				</div>
			</div>
		</div>
		<div class="row">

			<div class="col-md-2">
				<div class="form-group">
					<label style="padding-top: 4px; white-space: nowrap;"> <%
 	StringData SUN_Dim2_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "SUN Dimension");
 %>
						<%=smartHF.getLit(0, 0, SUN_Dim2_LBL).replace("absolute;", "relative; font-weight: bold;")%>
					</label>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label style="padding-top: 4px; white-space: nowrap;"> <%
 	StringData SUN_PRODCLS = resourceBundleHandler.gettingValueFromBundle(StringData.class,
 			"2) Product Class :");
 %>
						<%=smartHF.getLit(0, 0, SUN_PRODCLS).replace("absolute;", "relative; font-weight: bold;")%>

					</label>

				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<%=smartHF.getRichText(0, 0, fw, sv.zdmsion02, (sv.zdmsion02.getLength() + 1), null)
					.replace("absolute", "relative")%>

				</div>
			</div>
		</div>



	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

