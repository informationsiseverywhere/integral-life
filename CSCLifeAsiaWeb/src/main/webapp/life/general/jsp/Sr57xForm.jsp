<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR57X";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.general.screens.*"%>
<%
	Sr57xScreenVars sv = (Sr57xScreenVars) fw.getVariables();
%>
<%
	{
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
	
	    <div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%
							StringData COMPANY_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company");
						%>
						<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;", "relative; font-weight: bold;")%>
					</label>

					<%=smartHF.getRichText(0, 0, fw, sv.company, (sv.company.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width").replace(" bold", "")
					.replace("input_cell", "bold_cell")%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%
							StringData TABL_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table");
						%>
						<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;", "relative; font-weight: bold;")%>
					</label>

					<%=smartHF.getRichText(0, 0, fw, sv.tabl, (sv.tabl.getLength() + 1), null).replace("absolute",
					"relative")%>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%
							StringData ITEM_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item");
						%>
						<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;", "relative; font-weight: bold;")%>
					</label>
					<table>
						<tr>
							<td><%=smartHF.getRichText(0, 0, fw, sv.item, (sv.item.getLength() + 1), null).replace("absolute",
					"relative")%></td>
							<td style="padding-left: 1px;"><%=smartHF.getRichText(0, 0, fw, sv.longdesc, (sv.longdesc.getLength() + 1), null)
					.replace("absolute", "relative")%></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
		
		<div class="row">
		   <div class="col-md-3">
		        <div class="form-group">

					<label style="padding-top: 20px;">
						<%StringData SUB_ACCDE_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub A/C Code");%>
                        <%=smartHF.getLit(0, 0, SUB_ACCDE_LBL).replace("absolute;","relative; font-weight: bold;")%>
						
						
						
					</label>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">

					<label style="padding-top: 20px;">
						<%StringData DOC_TYP_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Tran Doc Type");%>
                         <%=smartHF.getLit(0, 0, DOC_TYP_LBL).replace("absolute;","relative; font-weight: bold;")%>
					</label>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">

					<label style="padding-top: 20px;">
						<%StringData SUB_ACTYP_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub A/C Type)");%>
                           <%=smartHF.getLit(0, 0, SUB_ACTYP_LBL).replace("absolute;","relative; font-weight: bold;")%>
					</label>
				</div>
			</div>

		</div>
		
		
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode01, (sv.sacscode01.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx01, (sv.rdocpfx01.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype01, (sv.sacstype01.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode02, (sv.sacscode02.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx02, (sv.rdocpfx02.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype02, (sv.sacstype02.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode03, (sv.sacscode03.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx03, (sv.rdocpfx03.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype03, (sv.sacstype03.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode04, (sv.sacscode04.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx04, (sv.rdocpfx04.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype04, (sv.sacstype04.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode05, (sv.sacscode05.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx05, (sv.rdocpfx05.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype05, (sv.sacstype05.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode06, (sv.sacscode06.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx06, (sv.rdocpfx06.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype06, (sv.sacstype06.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode07, (sv.sacscode07.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx07, (sv.rdocpfx07.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype07, (sv.sacstype07.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode08, (sv.sacscode08.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx08, (sv.rdocpfx08.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype08, (sv.sacstype08.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode09, (sv.sacscode09.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx09, (sv.rdocpfx09.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype09, (sv.sacstype09.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacscode10, (sv.sacscode10.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.rdocpfx10, (sv.rdocpfx10.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label></label>
					<%=smartHF.getRichText(0, 0, fw, sv.sacstype10, (sv.sacstype10.getLength() + 1), null)
					.replace("absolute", "relative")%>
				</div>
			</div>

	</div>	
	
	
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#sacscode01").width(100);
		$("#rdocpfx01").width(100);
		$("#sacstype01").width(100);
		$("#sacscode02").width(100);
		$("#rdocpfx02").width(100);
		$("#sacstype02").width(100);
		$("#sacscode03").width(100);
		$("#rdocpfx03").width(100);
		$("#sacstype03").width(100);
		$("#sacscode04").width(100);
		$("#rdocpfx04").width(100);
		$("#sacstype04").width(100);
		$("#sacscode05").width(100);
		$("#rdocpfx05").width(100);
		$("#sacstype05").width(100);
		$("#sacscode06").width(100);
		$("#rdocpfx06").width(100);
		$("#sacstype06").width(100);
		$("#sacscode07").width(100);
		$("#rdocpfx07").width(100);
		$("#sacstype07").width(100);
		$("#sacscode08").width(100);
		$("#rdocpfx08").width(100);
		$("#sacstype08").width(100);
		$("#sacscode09").width(100);
		$("#rdocpfx09").width(100);
		$("#sacstype09").width(100);
		$("#sacscode10").width(100);
		$("#rdocpfx10").width(100);
		$("#sacstype10").width(100);
	});
</script>
	
	
<%@ include file="/POLACommon2NEW.jsp"%>