<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%= ((SMARTHTMLFormatter)AppVars.hf).getHTMLCBVar(19,25)%>
<%String screenName = "S5614";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.general.screens.*" %>

<%S5614ScreenVars sv = (S5614ScreenVars) fw.getVariables();%>

<%if (sv.S5614screenWritten.gt(0)) {%>
	<%S5614screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("Effective Date ");%>
	<%sv.effdateDisp.setClassString("");%>
	<%StringData generatedText3 = new StringData("Contract       ");%>
	<%sv.chdrsel.setClassString("");%>
<%	sv.chdrsel.appendClassString("string_fld");
	sv.chdrsel.appendClassString("input_txt");
%>
	<%StringData generatedText4 = new StringData("Life           ");%>
	<%sv.life.setClassString("");%>
	<%StringData generatedText5 = new StringData("Contract Type  ");%>
	<%sv.chdrtype.setClassString("");%>
	<%StringData generatedText6 = new StringData("Life Sex       ");%>
	<%sv.sex.setClassString("");%>
	<%StringData generatedText7 = new StringData("Contract Curr  ");%>
	<%sv.currcd.setClassString("");%>
	<%StringData generatedText8 = new StringData("Life Age CCD   ");%>
	<%sv.age.setClassString("");%>
	<%StringData generatedText9 = new StringData("Billing Freq   ");%>
	<%sv.billfreq.setClassString("");%>
	<%StringData generatedText10 = new StringData("Joint Life     ");%>
	<%sv.jlife.setClassString("");%>
	<%StringData generatedText11 = new StringData("Coverage       ");%>
	<%sv.coverage.setClassString("");%>
	<%StringData generatedText12 = new StringData("Joint Life Sex ");%>
	<%sv.jlsex.setClassString("");%>
	<%StringData generatedText13 = new StringData("Rider          ");%>
	<%sv.rider.setClassString("");%>
	<%StringData generatedText14 = new StringData("Joint Age CCD  ");%>
	<%sv.ageprem.setClassString("");%>
	<%StringData generatedText15 = new StringData("Coverage Name  ");%>
	<%sv.crtable.setClassString("");%>
	<%StringData generatedText16 = new StringData("Premium Term   ");%>
	<%sv.premterm.setClassString("");%>
	<%StringData generatedText17 = new StringData("Risk Term      ");%>
	<%sv.riskCessTerm.setClassString("");%>
	<%StringData generatedText18 = new StringData("Sum Assured    ");%>
	<%sv.sumins.setClassString("");%>
	<%StringData generatedText19 = new StringData("Benefit Freq   ");%>
	<%sv.freqann.setClassString("");%>
	<%StringData generatedText20 = new StringData("Benefit Term   ");%>
	<%sv.benterm.setClassString("");%>
	<%StringData generatedText21 = new StringData("Premium        ");%>
	<%sv.annamnt.setClassString("");%>
	<%StringData generatedText22 = new StringData("Premium Breakdown ");%>
	<%sv.pbind.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.trancde.setClassString("");%>
	<%sv.currcode.setClassString("");%>
	<%sv.language.setClassString("");%>
	<%sv.singp.setClassString("");%>

	<%
{
		if (appVars.ind07.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			generatedText3.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind24.isOn()) {
			sv.chdrsel.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			generatedText4.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.life.setReverse(BaseScreenData.REVERSED);
			sv.life.setColor(BaseScreenData.RED);
		}
		if (appVars.ind27.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
			sv.life.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.life.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.chdrtype.setReverse(BaseScreenData.REVERSED);
			sv.chdrtype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.chdrtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind17.isOn()) {
			sv.chdrtype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.sex.setReverse(BaseScreenData.REVERSED);
			sv.sex.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.sex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.age.setReverse(BaseScreenData.REVERSED);
			sv.age.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.age.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.billfreq.setReverse(BaseScreenData.REVERSED);
			sv.billfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.billfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind09.isOn()) {
			sv.billfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (appVars.ind27.isOn()) {
			sv.jlife.setInvisibility(BaseScreenData.INVISIBLE);
			sv.jlife.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind26.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.coverage.setReverse(BaseScreenData.REVERSED);
			sv.coverage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
			sv.coverage.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.coverage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.jlsex.setReverse(BaseScreenData.REVERSED);
			sv.jlsex.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.jlsex.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind26.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.rider.setReverse(BaseScreenData.REVERSED);
			sv.rider.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
			sv.rider.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.rider.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.ageprem.setReverse(BaseScreenData.REVERSED);
			sv.ageprem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.ageprem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.crtable.setReverse(BaseScreenData.REVERSED);
			sv.crtable.setColor(BaseScreenData.RED);
		}
		if (appVars.ind26.isOn()) {
			sv.crtable.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.crtable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.premterm.setReverse(BaseScreenData.REVERSED);
			sv.premterm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.premterm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.sumins.setReverse(BaseScreenData.REVERSED);
			sv.sumins.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.sumins.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.freqann.setReverse(BaseScreenData.REVERSED);
			sv.freqann.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.freqann.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.benterm.setReverse(BaseScreenData.REVERSED);
			sv.benterm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.benterm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.annamnt.setReverse(BaseScreenData.REVERSED);
			sv.annamnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.annamnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind23.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 4, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 21, fw, sv.effdateDisp)%>
	<%=smartHF.getHTMLCalNSVar(3, 21, fw, sv.effdateDisp)%>

	<%=smartHF.getLit(5, 4, generatedText3)%>

	<%=smartHF.getHTMLSpaceVar(5, 21, fw, sv.chdrsel)%>
	<%=smartHF.getHTMLF4NSVar(5, 21, fw, sv.chdrsel)%>

	<%=smartHF.getLit(5, 42, generatedText4)%>

	<%=smartHF.getHTMLVar(5, 59, fw, sv.life)%>

	<%=smartHF.getLit(6, 4, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(6, 21, fw, sv.chdrtype)%>
	<%=smartHF.getHTMLF4NSVar(6, 21, fw, sv.chdrtype)%>

	<%=smartHF.getLit(6, 42, generatedText6)%>

	<%=smartHF.getHTMLSpaceVar(6, 59, fw, sv.sex)%>
	<%=smartHF.getHTMLF4NSVar(6, 59, fw, sv.sex)%>

	<%=smartHF.getLit(7, 4, generatedText7)%>

	<%=smartHF.getHTMLSpaceVar(7, 21, fw, sv.currcd)%>
	<%=smartHF.getHTMLF4NSVar(7, 21, fw, sv.currcd)%>

	<%=smartHF.getLit(7, 42, generatedText8)%>

	<%=smartHF.getHTMLVar(7, 59, fw, sv.age, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(8, 4, generatedText9)%>

	<%=smartHF.getHTMLSpaceVar(8, 21, fw, sv.billfreq)%>
	<%=smartHF.getHTMLF4NSVar(8, 21, fw, sv.billfreq)%>

	<%=smartHF.getLit(9, 42, generatedText10)%>

	<%=smartHF.getHTMLVar(9, 59, fw, sv.jlife)%>

	<%=smartHF.getLit(10, 4, generatedText11)%>

	<%=smartHF.getHTMLVar(10, 21, fw, sv.coverage)%>

	<%=smartHF.getLit(10, 42, generatedText12)%>

	<%=smartHF.getHTMLSpaceVar(10, 59, fw, sv.jlsex)%>
	<%=smartHF.getHTMLF4NSVar(10, 59, fw, sv.jlsex)%>

	<%=smartHF.getLit(11, 4, generatedText13)%>

	<%=smartHF.getHTMLVar(11, 21, fw, sv.rider)%>

	<%=smartHF.getLit(11, 42, generatedText14)%>

	<%=smartHF.getHTMLVar(11, 59, fw, sv.ageprem, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(12, 4, generatedText15)%>

	<%=smartHF.getHTMLSpaceVar(12, 21, fw, sv.crtable)%>
	<%=smartHF.getHTMLF4NSVar(12, 21, fw, sv.crtable)%>

	<%=smartHF.getLit(14, 4, generatedText16)%>

	<%=smartHF.getHTMLVar(14, 21, fw, sv.premterm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(15, 4, generatedText17)%>

	<%=smartHF.getHTMLVar(15, 21, fw, sv.riskCessTerm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(16, 42, generatedText18)%>

	<%=smartHF.getHTMLVar(16, 59, fw, sv.sumins, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(17, 4, generatedText19)%>

	<%=smartHF.getHTMLSpaceVar(17, 21, fw, sv.freqann)%>
	<%=smartHF.getHTMLF4NSVar(17, 21, fw, sv.freqann)%>

	<%=smartHF.getLit(18, 4, generatedText20)%>

	<%=smartHF.getHTMLVar(18, 21, fw, sv.benterm, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(18, 42, generatedText21)%>

	<%=smartHF.getHTMLVar(18, 59, fw, sv.annamnt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(21, 42, generatedText22)%>

	<%=smartHF.getHTMLVar(21, 61, fw, sv.pbind)%>








<%}%>

<%if (sv.S5614protectWritten.gt(0)) {%>
	<%S5614protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
