<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5696";%>
<%@ include file="/POLACommon3.jsp"%>
<%@ page import="com.csc.life.general.screens.S5696ScreenVars" %>

<%S5696ScreenVars sv = (S5696ScreenVars) fw.getVariables();%>
<%{
}%>

<div class='outerDiv'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.company,(sv.company.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.tabl,( sv.tabl.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute;","relative; font-weight: bold;")%>
<br/>
<%=smartHF.getRichText(0,0,fw,sv.item,( sv.item.getLength()+1),null).replace("absolute","relative")%>
<%=smartHF.getRichText(0,0,fw,sv.longdesc,( sv.longdesc.getLength()+1),null).replace("absolute","relative")%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
</table>

<table width='100%'>
<br/><br/>

<tr style='height:22px;'>

<td width='251'>
<%StringData PRCENT_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"SUN Dimension 5) Work area/Branch");%>
<%=smartHF.getLit(0, 0, PRCENT_LBL).replace("absolute;","relative; font-weight: bold;")%>


<br/>
<%=smartHF.getRichText(0, 0, fw, sv.zdmsion,(sv.zdmsion.getLength()+1),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
</td>
</tr>

<!-- TMLI Bancass -->
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flag Bancass");%>
<tr style='height:22px;'><td width='376'>
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Flag Bancass")%>
</div>
<%}%>



<br/>
<%if ((new Byte((sv.flagbancass).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='flagbancass' 
type='text'

<%if((sv.flagbancass).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.flagbancass.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.flagbancass.getLength()%>'
maxLength='<%= sv.flagbancass.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(flagbancass)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.flagbancass).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.flagbancass).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.flagbancass).getColor()== null  ? 
			"input_cell" :  (sv.flagbancass).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
<%}%>
</td>
<!-- TMLI Bancass -->


 </table>
<br/>
</div>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<div class="pagebutton">
<ul class='clear'>
<li><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/pagebtn_bg_01.png" width='84' height='22'></li>
<li><a id="continuebutton" name="continuebutton" href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY0')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_continue.png" width='84' height='22' alt='Continue'></a></li>
<li><a id='refreshbutton' name='refreshbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY05')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_refresh.png" width='84' height='22' alt='Refresh'></a></li>
<li><a id='previousbutton' name='previousbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY12')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_previous.png" width='84' height='22' alt='Previous'></a></li>
<li><a id='exitbutton' name='exitbutton' href='javascript:;' onClick="changeContinueImagePNG(this,'PFKEY03')" onMouseOver="changeMouseoverPNG(this)" onMouseOut="changeMouseoutPNG(this)"><img src="<%=ctx%>screenFiles/<%=localeimageFolder%>/btn_exit.png" width='84' height='22' alt='Exit'></a></li>
</ul>
</div>
<%@ include file="/POLACommon4.jsp"%>

