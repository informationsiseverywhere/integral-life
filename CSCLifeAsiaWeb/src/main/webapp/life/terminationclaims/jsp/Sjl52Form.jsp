<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl52";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sjl52ScreenVars sv = (Sjl52ScreenVars) fw.getVariables();
%>
<%{
		if (appVars.ind12.isOn()) {
			sv.conOwner.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.conOwner.setReverse(BaseScreenData.REVERSED);
			sv.conOwner.setColor(BaseScreenData.RED);
		}
}%>	
<div class="panel panel-default">
	<div class="panel-body">
	<% if (sv.cnclScreenFlag.compareTo("N") == 0) { //IBPLIFE-3582%>
		<div class="row">
			<div class="col-md-4">				
        			<div class="form-group">
        				<label>
        					<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Status"))%>
        				</label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.conStatus)%>
						</td></tr></table>
					</div>
				</div>
			</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Claim# From")%></label>
		        	<input name='claimFrom' type='text' size='9' 
		        		<%
			        		formatValue = (sv.claimFrom.getFormData()).toString();
			        	%>
			        		value='<%=formatValue%>'
			        	<%
			        		if (formatValue != null && formatValue.trim().length() > 0) {
			        	%>
			        		title='<%=formatValue%>' <%}%>
			        		size='<%=sv.claimFrom.getLength()%>'
			        		maxLength='<%=sv.claimFrom.getLength()%>' onFocus='doFocus(this)'
			        		onHelp='return fieldHelp(claimFrom)'
			        		onKeyUp='return checkMaxLength(this)'
			        	<%
			        		if ((new Byte((sv.claimFrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
			        	%>
			        		readonly="true" class="output_cell"
			        	<%
			        		} else if ((new Byte((sv.claimFrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
			        	%>
			        		class="bold_cell" 
			        	<%
			        		} else {
			        	%>
			        		class=' <%=(sv.claimFrom).getColor() == null ? "input_cell"
									: (sv.claimFrom).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			        	<%}%>
		        	/>
      		</div>
      	</div>
      	<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract# From")%></label>
        	
		        	<input name='conFrom' type='text' size='6' 
		        		<%
			        		formatValue = (sv.conFrom.getFormData()).toString();
			        	%>
			        		value='<%=formatValue%>'
			        	<%
			        		if (formatValue != null && formatValue.trim().length() > 0) {
			        	%>
			        		title='<%=formatValue%>' <%}%>
			        		size='<%=sv.conFrom.getLength()%>'
			        		maxLength='<%=sv.conFrom.getLength()%>' onFocus='doFocus(this)'
			        		onHelp='return fieldHelp(conFrom)'
			        		onKeyUp='return checkMaxLength(this)'
			        	<%
			        		if ((new Byte((sv.conFrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
			        	%>
			        		readonly="true" class="output_cell"
			        	<%
			        		} else if ((new Byte((sv.conFrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
			        	%>
			        		class="bold_cell" 
			        	<%
			        		} else {
			        	%>
			        		class=' <%=(sv.conFrom).getColor() == null ? "input_cell"
									: (sv.conFrom).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			        	<%}%>
		        	/>
      			</div>
      		</div>
      		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
        			<table><tr><td>
        			<div style="display: table;">
						<div class="input-group">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.clttwo, (sv.clttwo.getLength()))%>
						
						</div>
							<span class="input-group-btn" >
								<button class="btn btn-info"
									type="button"
									onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
					</div>
					</td><td>
		        	<input name='conOwner' type='text' size='30' 
		        		<%
		        		formatValue = (sv.conOwner.getFormData()).toString();
		        	%>
		        		value='<%=formatValue%>'
		        	<%
		        		if (formatValue != null && formatValue.trim().length() > 0) {
		        	%>
		        		title='<%=formatValue%>' <%}%>
		        		size='<%=sv.conOwner.getLength()%>'
		        		maxLength='<%=sv.conOwner.getLength()%>' onFocus='doFocus(this)'
		        		onHelp='return fieldHelp(conOwner)'
		        		onKeyUp='return checkMaxLength(this)'
		        	<%
		        		if ((new Byte((sv.conOwner).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
		        	%>
		        		readonly="true" class="output_cell"
		        	<%
		        		} else if ((new Byte((sv.conOwner).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
		        	%>
		        		class="bold_cell" 
		        	<%
		        		} else {
		        	%>
		        		class=' <%=(sv.conOwner).getColor() == null ? "input_cell"
								: (sv.conOwner).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
		        	<%}%>
		        	/>
		        	</td></tr></table>
      			</div>
      		</div>
      		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contact Date")%></label>
					<% if ((new Byte((sv.conDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                             <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.conDateDisp)%>                                       
                             </div>
               		<%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="conDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.conDateDisp, (sv.conDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
             		 <%}%>	
      			</div>
      		</div>
      		<div class="col-md-3">
				<div class="form-group">
      				<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "claimTypeList" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("claimTypeList");
							optionValue = makeDropDownList(mappedItems, sv.claimTypeList.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.claimTypeList.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.claimTypeList).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.claimTypeList.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							}else{
						%>	
						<%=smartHF.getDropDownExt(sv.claimTypeList, fw, longValue, "claimTypeList", optionValue, 0, 150)%>
						<%
								}
							%>			
				</div>
			</div>
		</div>	
	<%} 
	else{%>
	<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Claim# From")%></label>
		        	<input name='claimFrom' type='text' size='9' 
		        		<%
			        		formatValue = (sv.claimFrom.getFormData()).toString();
			        	%>
			        		value='<%=formatValue%>'
			        	<%
			        		if (formatValue != null && formatValue.trim().length() > 0) {
			        	%>
			        		title='<%=formatValue%>' <%}%>
			        		size='<%=sv.claimFrom.getLength()%>'
			        		maxLength='<%=sv.claimFrom.getLength()%>' onFocus='doFocus(this)'
			        		onHelp='return fieldHelp(claimFrom)'
			        		onKeyUp='return checkMaxLength(this)'
			        	<%
			        		if ((new Byte((sv.claimFrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
			        	%>
			        		readonly="true" class="output_cell"
			        	<%
			        		} else if ((new Byte((sv.claimFrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
			        	%>
			        		class="bold_cell" 
			        	<%
			        		} else {
			        	%>
			        		class=' <%=(sv.claimFrom).getColor() == null ? "input_cell"
									: (sv.claimFrom).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			        	<%}%>
		        	/>
      		</div>
      	</div>
      	<div class="col-md-1"></div>
      	<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract# From")%></label>
        	
		        	<input name='conFrom' type='text' size='6' 
		        		<%
			        		formatValue = (sv.conFrom.getFormData()).toString();
			        	%>
			        		value='<%=formatValue%>'
			        	<%
			        		if (formatValue != null && formatValue.trim().length() > 0) {
			        	%>
			        		title='<%=formatValue%>' <%}%>
			        		size='<%=sv.conFrom.getLength()%>'
			        		maxLength='<%=sv.conFrom.getLength()%>' onFocus='doFocus(this)'
			        		onHelp='return fieldHelp(conFrom)'
			        		onKeyUp='return checkMaxLength(this)'
			        	<%
			        		if ((new Byte((sv.conFrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
			        	%>
			        		readonly="true" class="output_cell"
			        	<%
			        		} else if ((new Byte((sv.conFrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
			        	%>
			        		class="bold_cell" 
			        	<%
			        		} else {
			        	%>
			        		class=' <%=(sv.conFrom).getColor() == null ? "input_cell"
									: (sv.conFrom).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			        	<%}%>
		        	/>
      			</div>
      		</div>
      		<div class="col-md-1"></div>
      		<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
        			<table><tr><td>
        			<div style="display: table;">
						<div class="input-group">
							<%=smartHF.getRichTextInputFieldLookup(fw, sv.clttwo, (sv.clttwo.getLength()))%>
						
						</div>
							<span class="input-group-btn" >
								<button class="btn btn-info"
									type="button"
									onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
					</div>
					</td><td>
		        	<input name='conOwner' type='text' size='30' 
		        		<%
		        		formatValue = (sv.conOwner.getFormData()).toString();
		        	%>
		        		value='<%=formatValue%>'
		        	<%
		        		if (formatValue != null && formatValue.trim().length() > 0) {
		        	%>
		        		title='<%=formatValue%>' <%}%>
		        		size='<%=sv.conOwner.getLength()%>'
		        		maxLength='<%=sv.conOwner.getLength()%>' onFocus='doFocus(this)'
		        		onHelp='return fieldHelp(conOwner)'
		        		onKeyUp='return checkMaxLength(this)'
		        	<%
		        		if ((new Byte((sv.conOwner).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
		        	%>
		        		readonly="true" class="output_cell"
		        	<%
		        		} else if ((new Byte((sv.conOwner).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
		        	%>
		        		class="bold_cell" 
		        	<%
		        		} else {
		        	%>
		        		class=' <%=(sv.conOwner).getColor() == null ? "input_cell"
								: (sv.conOwner).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
		        	<%}%>
		        	/>
		        	</td></tr></table>
      			</div>
      		</div>
      		
      		<div class="col-md-2">
				<div class="form-group">
      				<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "claimTypeList" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("claimTypeList");
							optionValue = makeDropDownList(mappedItems, sv.claimTypeList.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.claimTypeList.getFormData()).toString().trim());
						%>
						<%
							if ((new Byte((sv.claimTypeList).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div style="min-width: 90px; width: 90px;"
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=sv.claimTypeList.getFormData()%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							}else{
						%>	
						<%=smartHF.getDropDownExt(sv.claimTypeList, fw, longValue, "claimTypeList", optionValue, 0, 150)%>
						<%
								}
							%>			
				</div>
			</div>
		</div>	
	<%} %>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='Sjl52Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 50px !important"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Claim#")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract#")%></th>
									<th style="text-align: center; width: 300px !important""><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contact Date")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></th>
								</tr>
							</thead>
							<%
								GeneralTable sfl = fw.getTable("Sjl52screensfl");
								GeneralTable sfl1 = fw.getTable("Sjl52screensfl");
								
							%>
							<script language="javascript">
							        $(document).ready(function(){
										var rows = <%=sfl1.count() + 1%>;
										var isPageDown = 1;
										var pageSize = 1;
										var headerRowCount=1;
										var fields = new Array();
										fields[0] = "select";
								<%if (false) {%>	
									operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"Sjl52Table",null,headerRowCount);
								<%}%>	
							
							        });
							    </script>
							<tbody>
							<%
								Sjl52screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (Sjl52screensfl.hasMoreScreenRows(sfl)) {
								hyperLinkFlag = true;
							%>

							<tr id='tr<%=count%>'>
							<%
										if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>


									<%
										if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>

									<%
										} else {
									%>

									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.select.getLength()%>'
											value='<%=sv.select.getFormData()%>'
											size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(Sjl52screensfl.select)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="Sjl52screensfl" + "." + "select" + "_R" + count%>'
											id='<%="Sjl52screensfl" + "." + "select" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.select.getLength() * 12%> px;">

									</div>
									<%
										}
									%>
									<%
										}
									%>
								<td style="text-align: center; width: 50px !important" <%if(!(((BaseScreenData)sv.seqenum) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          					<%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.seqenum, "Sjl52screensfl", count, sfl, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
              					</td>
								<td style="width: 150px;"
										<%if (!(((BaseScreenData) sv.claimNum) instanceof StringBase)) {%>
										 <%} else {%> <%}%>>
										<%
											if ((new Byte((sv.claimNum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0
														|| (new Byte((sv.claimNum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
													hyperLinkFlag = false;
												}
										%> <%
 	if ((new Byte((sv.claimNum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (hyperLinkFlag) {
 %> <a href="javascript:;" class='tableLink'
	onClick='document.getElementById("<%="Sjl52screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.claimNum.getFormData()%></span></a>
										<%
											} else {
										%> <a href="javascript:;" class='tableLink'><span><%=sv.claimNum.getFormData()%></span></a>
										<%
											}
										%> <%
 	}
 %>
									</td>

									<td>
										<%if((new Byte((sv.chdrnum).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.chdrnum.getFormData();
												%>
										<div id="Sjl52screensfl.chdrnum_R<%=count%>"
											name="Sjl52screensfl.chdrnum_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
										<%if((new Byte((sv.clntId).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.clntId.getFormData();
												%>
										<div id="Sjl52screensfl.clntId_R<%=count%>"
											name="Sjl52screensfl.clntId_R<%=count%>"
											<%if (!(((BaseScreenData) sv.clntId) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
										<%= sv.contactDateDisp.getFormData()%>		
									</td>
									<td>
										<%if((new Byte((sv.claimType).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.claimType.getFormData();
												%>
										<div id="Sjl52screensfl.claimType_R<%=count%>"
											name="Sjl52screensfl.claimType_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>

								</tr>

								<%
									count = count + 1;
									Sjl52screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	
		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	     <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl52Table').DataTable({
		ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,     
            "EmptyTable":"No data available in table",	
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }	
          }    
	});
})
</script>	
<%@ include file="/POLACommon2NEW.jsp"%>	
		