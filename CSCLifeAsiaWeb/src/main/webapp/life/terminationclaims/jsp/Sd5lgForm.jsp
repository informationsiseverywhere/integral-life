
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SD5LG";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sd5lgScreenVars sv = (Sd5lgScreenVars) fw.getVariables();
%>


<%
	{
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
	</div>



	<div class="panel-body">
		<div class="row">
		
		
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
											
						<div class="input-group"><%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel, (sv.chdrsel.getLength()))%>
						
						
							<span class="input-group-btn" >
								<button class="btn btn-info"
									type="button"
									onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
							</div>
				
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>
		
		</div>
	</div>

</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
		</div>
		<div class="panel-body">
			<div class="row">			
			
				<div class="col-md-4">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "A")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Component Surrender Register")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Component Surrender Register Reversal")%></b>
					</label>
				</div>
				<div class="col-md-4">
					<label class="radio-inline"><b> <%=smartHF.buildRadioOption(sv.action, "action", "C")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Component Surrender Approval")%></b>
					</label>
				</div>
	
			</div>
		</div>
	</div>



<%@ include file="/POLACommon2NEW.jsp"%>

