<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5125";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	S5125ScreenVars sv = (S5125ScreenVars) fw.getVariables();
%>
<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. fund ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-sect ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit amount ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess Age / Term ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess date ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess Age / Term ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess date ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bene cess Age / Term ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bene cess date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality class ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl Method ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special terms ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurance   ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annuity details ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total policies in plan ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life (J/L) ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number available ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number applicable ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium with Tax  ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Detail ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stamp Duty");%>
<%{
		appVars.rolldown(new int[] {10});
		appVars.rollup(new int[] {10});
		if (appVars.ind42.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
			sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.frqdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.ratypind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind40.isOn()) {
			sv.ratypind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.ratypind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.ratypind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ratypind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind33.isOn()) {
			sv.anntind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.anntind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.anntind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numavail.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numapp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
			sv.numapp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind36.isOn()) {
			sv.numapp.setReverse(BaseScreenData.REVERSED);
			sv.numapp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.numapp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind45.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind44.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind44.isOn()) {
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-1223 STARTS
		if (appVars.ind46.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		//ILIFE-1223 ENDS
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind49.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		if (appVars.ind53.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.waitperiod.setReverse(BaseScreenData.REVERSED);
			sv.waitperiod.setColor(BaseScreenData.RED);
		}
		if (appVars.ind62.isOn()) {
			sv.bentrm.setReverse(BaseScreenData.REVERSED);
			sv.bentrm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind64.isOn()) {
			sv.poltyp.setReverse(BaseScreenData.REVERSED);
			sv.poltyp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind66.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
			sv.prmbasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.waitperiod.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind62.isOn()) {
			sv.bentrm.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind64.isOn()) {
			sv.poltyp.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind64.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.waitperiod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind63.isOn()) {
			sv.bentrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind65.isOn()) {
			sv.poltyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-009
		if (appVars.ind69.isOn()) {
			sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind68.isOn()) {
			sv.statcode.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-009
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		if (appVars.ind73.isOn()) {
			sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6941 start*/
		if (!appVars.ind121.isOn()) {
			sv.lnkgno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind122.isOn()) {
			sv.lnkgsubrefno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6941 end*/
		
		//ILJ-43
		if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}	
		//end
		/* IBPLIFE-2133 Start */
		if (sv.nbprp126lag.compareTo("N") != 0){
		if (appVars.ind80.isOn()) {
			sv.validflag.setReverse(BaseScreenData.REVERSED);
			sv.validflag.setColor(BaseScreenData.RED);
		}
		 if (appVars.ind83.isOn()) {
			sv.validflag.setEnabled(BaseScreenData.DISABLED);
		} 
		if (!appVars.ind80.isOn()) {
			sv.validflag.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind81.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		 if (appVars.ind84.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		} 
		if (!appVars.ind81.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind82.isOn()) {
			sv.covrprpse.setReverse(BaseScreenData.REVERSED);
			sv.covrprpse.setColor(BaseScreenData.RED);
		}
		 if (appVars.ind12.isOn()) {
			sv.covrprpse.setEnabled(BaseScreenData.DISABLED);
		} 
		if (!appVars.ind82.isOn()) {
			sv.covrprpse.setHighLight(BaseScreenData.BOLD);
		}
		}
		/* IBPLIFE-2133 End */
	}
%>


<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					
					<table><tr><td>
					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td><td>
  
						<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
  <%}%>
  
  </td><td>
						<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
  		<%}%>
  		</td></tr></table>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table><tr><td>
						<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 65px" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td><td>
	

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;min-width:65px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
  <%}%>
					</td></tr></table>
				</div>
			</div>
			<!-- <div class="col-md-4"></div> --><!-- ILIFE-6333 -->
			<%-- <div class="col-md-4">
			<div class="form-group">
			<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div id='zagelit' class='label_txt'class='label_txt'  
								onHelp='return fieldHelp("zagelit")'>
								<%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
			</div>
			</div> --%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
				<table>
				<tr>
				
<td>

<%if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:100px;"> 	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;margin-left: 1px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
			</td>
			
			</tr>
			</table>
			</div>
		</div>
	<!-- START OF ILIFE-6333 -->
		<!-- BRD-009-STARTS -->
		<div class="col-md-4">
			<div class="form-group">
			<%if ((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
			<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>
			<table>
				<tr>
					<td>
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("statcode");
						optionValue = makeDropDownList( mappedItems , sv.statcode.getFormData(),2,resourceBundleHandler);  
					longValue = (String) mappedItems.get((sv.statcode.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.statcode).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' 
												style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"width:82px;" : "width:140px;" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {
						%>
						
					<% if("red".equals((sv.statcode).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					
					<select name='statcode' type='list' style="width:145px;"
					<% 
						if((new Byte((sv.statcode).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.statcode).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.statcode).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					}} 
					%>
					<% longValue = null;%>	
					</td>
				</tr>
			</table>
			</div>	
		</div>
		<!-- BRD-009-ENDS -->
	<!-- END OF ILIFE-6333 -->
	</div>
	<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 					

		<table><tr><td>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width: 71px;">
<%					
if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
</td><td>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;min-width: 71px;max-width: 200px;">
<%					
if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>
    	 				</td></tr></table>	
    	 					</div></div>
    	 					
    	 					
    	 					</div>
    	 					
		<div class="row">
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
				<div style="max-width: 50px;">
					<%
						if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
				<div  style="max-width: 50px;">
					<%
						if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
				<div  style="max-width: 50px;">
					<%
						if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>
				<div style="max-width: 80px;">
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "statFund" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("statFund");
						longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
				<div  style="max-width: 50px;">

					<%
						if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
			<div class="col-md-2">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>
				<div style="max-width: 50px;">

					<%
						if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSubsect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.statSubsect.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 70px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
		</div>
		<!-- IBPLIFE-2133 Start -->
		<% if (sv.nbprp126lag.compareTo("Y") != 0){%>
		 <br>
		<hr>
		<br>
		<%} else{%>
			<div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assure / Premium")%></a>
                        </li>
                       
                        <li>
                        	<a href="#prem_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></a>
                        </li>
                    </ul>
                    </div>
                  </div>
                 <%} %>
              <% if (sv.nbprp126lag.compareTo("N") != 0){%>   
             <div class="tab-content">
               	<div class="tab-pane fade in active" id="basic_tab">
               	<%} %>
               		<div class="row">
			<div class="col-md-3">
			<div class="form-group">
				<div >
					<%
						if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Amount")%></label>
					<%
						}
					%>
					<table><tr><td>
					
					<%if ((new Byte((sv.sumin).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%	
								qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>
					
					<input name='sumin' 
					type='text'
					<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width:80px;"<% }%>
					
						value='<%=valueThis%>'
								 <%	 
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=valueThis%>'
						 <%}%>
					
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
					maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)-4%>' 
					onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
					
					<% 
						if((new Byte((sv.sumin).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.sumin).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.sumin).getColor()== null  ? 
								"input_cell" :  (sv.sumin).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					<%}%>
					
					</td>
					<td>
					
					
					<%if ((new Byte((sv.frqdesc).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					  </td></tr></table>
					</div>
					</div>
			</div>
			<%
				if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("mortcls");
							optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.mortcls).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #ec7572; width: 208px;">
						<%
							}
						%>

						<select name='mortcls' type='list' style="width: 208px;"
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.mortcls).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						formatValue = null;
								longValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>
			<%
				}
			%>
			<div class="col-md-3">
			<div class="form-group">
				<%
					longValue = null;
					formatValue = null;
				%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
				<%
					fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("currcd");
					longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
				%>

				<%
					if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.currcd.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}

					} else {

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.currcd.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}

					}
				%>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
			</div>
			<div class="col-md-3">
			<div class="form-group">

			<%
				if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
			<%
				}
			%>
			<%
				if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					fieldItem = appVars.loadF4FieldsLong(new String[] { "prmbasis" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("prmbasis");
					optionValue = makeDropDownList(mappedItems, sv.prmbasis.getFormData(), 2, resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());
			%>

			<%
				if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
			%>
			<div
				class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
				style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px" : "width:140px;"%>'>
				<%
					if (longValue != null) {
				%>

				<%=XSSFilter.escapeHtml(longValue)%>

				<%
					}
				%>
			</div>

			<%
				longValue = null;
			%>

			<%
				} else {
			%>

			<%
				if ("red".equals((sv.prmbasis).getColor())) {
			%>
			<div
				style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
				<%
					}
				%>

				<select name='prmbasis' type='list' style="width: 145px;"
					<%if ((new Byte((sv.prmbasis).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.prmbasis).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<%
					if ("red".equals((sv.prmbasis).getColor())) {
				%>
			</div>
			<%
				}
			%>

			<%
				}
				}
			%>
			<%
				longValue = null;
			%>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<!--  ILJ-43 -->
					<%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cess Age/Term")%></label><!-- ILJ-111 -->
					<% } else{%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
					<% } %>
					
					
					<table>
					<tr>
					<td>
							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessAge' type='text'
								<%if ((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessAge)%>' <%}%>
								size='<%=sv.riskCessAge.getLength()%>'
								maxLength='<%=sv.riskCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)'
								onKeyUp='return checkMaxLength(this)' style="width:75px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:75px;"
								<%} else if ((new Byte((sv.riskCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.riskCessAge).getColor() == null ? "input_cell"
						: (sv.riskCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								style="width:70px;" <%}%>>

					
						</td>
						<td>
						
					

							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)' style="width:70px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:70px;"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:70px;" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								style="width:70px;" <%}%>>
						
						</td>
						</tr>
						</table>
					
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<!--  ILJ-43 -->
					<%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cess Date")%></label><!-- ILJ-111 -->
					<% } else{%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
					<% } %>
					
					
					
					
                <% if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                            <div style="width: 80px"><%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp)%></div>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
					
					
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("liencd");
						optionValue = makeDropDownList(mappedItems, sv.liencd.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='liencd' type='list' style="width: 145px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.liencd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						formatValue = null;
							longValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">

			<%
				if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%><label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>
			<%
				}
			%>
			<%
				if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					fieldItem = appVars.loadF4FieldsLong(new String[] { "waitperiod" }, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("waitperiod");
					optionValue = makeDropDownList(mappedItems, sv.waitperiod.getFormData(), 2, resourceBundleHandler);
					longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());
			%>

			<%
				if ((new Byte((sv.waitperiod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
			%>
			<div
				class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
				style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px" : "width:140px"%>'>
				<%
					if (longValue != null) {
				%>

				<%=XSSFilter.escapeHtml(longValue)%>

				<%
					}
				%>
			</div>

			<%
				longValue = null;
			%>

			<%
				} else {
			%>

			<%
				if ("red".equals((sv.waitperiod).getColor())) {
			%>
			<div
				style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
				<%
					}
				%>

				<select name='waitperiod' type='list' style="width: 145px;"
					<%if ((new Byte((sv.waitperiod).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
					readonly="true" disabled class="output_cell"
					<%} else if ((new Byte((sv.waitperiod).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
					class="bold_cell" <%} else {%> class='input_cell' <%}%>>
					<%=optionValue%>
				</select>
				<%
					if ("red".equals((sv.waitperiod).getColor())) {
				%>
			</div>
			<%
				}
			%>

			<%
				}
				}
			%>
			<%
				longValue = null;
			%>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
					
					<table>
					<tr>
					<td>
					
					
							<%
								qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='premCessAge' type='text'
								<%if ((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessAge)%>' <%}%>
								size='<%=sv.premCessAge.getLength()%>'
								maxLength='<%=sv.premCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessAge).getColor() == null ? "input_cell"
						: (sv.premCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>






							<%
								qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
						
						</td>
						<td>
					
							<input name='premCessTerm' type='text'
								<%if ((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.premCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.premCessTerm)%>'
								<%}%> size='<%=sv.premCessTerm.getLength()%>'
								maxLength='<%=sv.premCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.premCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.premCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.premCessTerm).getColor() == null ? "input_cell"
						: (sv.premCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						
					</td>
					</tr>
					 </table>
				</div>
			</div>
			
			<div class="col-md-3">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
				
				
				
				
				
                <% if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                          <div style="width: 80px"><%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp)%></div>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
				
				
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group" >

				<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>
				<div style="max-width: 100px;">

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bappmeth" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bappmeth");
						optionValue = makeDropDownList(mappedItems, sv.bappmeth.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.bappmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.liencd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='liencd' type='list' style="width: 145px;"
							<%if ((new Byte((sv.bappmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.bappmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.bappmeth).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						formatValue = null;
							longValue = null;
					%>
					<%
						}
					%>


				</div>
			</div>
			</div>
			


			<div class="col-md-3">
			<div class="form-group">

				<%
					if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%><label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%>
					<%
						}
					%></label>
				<%
					if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						fieldItem = appVars.loadF4FieldsLong(new String[] { "bentrm" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("bentrm");
						optionValue = makeDropDownList(mappedItems, sv.bentrm.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());
				%>

				<%
					if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
				%>
				<div
					class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
					style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px" : "width:140px"%>'>
					<%
						if (longValue != null) {
					%>

					<%=XSSFilter.escapeHtml(longValue)%>

					<%
						}
					%>
				</div>

				<%
					longValue = null;
				%>

				<%
					} else {
				%>

				<%
					if ("red".equals((sv.bentrm).getColor())) {
				%>
				<div
					style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
					<%
						}
					%>

					<select name='bentrm' type='list' style="width: 145px;"
						<%if ((new Byte((sv.bentrm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
						readonly="true" disabled class="output_cell"
						<%} else if ((new Byte((sv.bentrm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%> class='input_cell' <%}%>>
						<%=optionValue%>
					</select>
					<%
						if ("red".equals((sv.bentrm).getColor())) {
					%>
				</div>
				<%
					}
				%>

				<%
					}
					}
				%>
				<%
					longValue = null;
				%>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age / Term")%></label>
					
					<table>
					<tr>
					<td>
					

							<%
								qpsf = fw.getFieldXMLDef((sv.benCessAge).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessAge' type='text'
								<%if ((sv.benCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessAge);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessAge)%>' <%}%>
								size='<%=sv.benCessAge.getLength()%>'
								maxLength='<%=sv.benCessAge.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessAge)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessAge).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessAge).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessAge).getColor() == null ? "input_cell"
						: (sv.benCessAge).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
					
						
						</td>
						<td>
						
						


							<%
								qpsf = fw.getFieldXMLDef((sv.benCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='benCessTerm' type='text'
								<%if ((sv.benCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right; width: 62px;margin-left: 1px;" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.benCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.benCessTerm)%>' <%}%>
								size='<%=sv.benCessTerm.getLength()%>'
								maxLength='<%=sv.benCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(benCessTerm)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.benCessTerm).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.benCessTerm).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.benCessTerm).getColor() == null ? "input_cell"
						: (sv.benCessTerm).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>>
						
					</td>
					</tr>
					</table>
					
				</div>
			</div>
			
			<div class="col-md-3">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess date")%></label>
				
				
				
				
                <% if ((new Byte((sv.benCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                           <div style="width: 80px"> <%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp)%></div>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="benCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp, (sv.benCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>
				
				
			</div>
			</div>
			
			<%
					if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
			<div class="col-md-3">
			<div class="form-group">
				
				<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
				<div  style="max-width: 100px;">
					

					<%
						if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[] { "dialdownoption" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("dialdownoption");
							optionValue = makeDropDownList(mappedItems, sv.dialdownoption.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.dialdownoption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px" : "width:140px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.dialdownoption).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='dialdownoption' type='list' style="width: 145px;"
							<%if ((new Byte((sv.dialdownoption).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.dialdownoption).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.dialdownoption).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
						}
					%>
					<%
						longValue = null;
					%>

				</div>
			</div>
			</div>
			
			
			<%
						}
					%>
			<!-- BRD-NBP-011 ends -->
			<%
					if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
			<div class="col-md-3">
			<div class="form-group">

				<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
				
				<div style="max-width: 150px;">

					<%
						if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[] { "poltyp" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("poltyp");
							optionValue = makeDropDownList(mappedItems, sv.poltyp.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px" : "width:140px"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.poltyp).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 145px;">
						<%
							}
						%>

						<select name='poltyp' type='list' style="width: 145px;"
							<%if ((new Byte((sv.poltyp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.poltyp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.poltyp).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
						}
					%>
					<%
						longValue = null;
					%>

				</div>
				</div>
			</div>
			
			<%
					}
				%>

		</div>
		<br>
		<hr>
		<br>

		<%
			if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="row">
			<div class="col-md-3">

				<%
					if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%><label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>


				<%
					if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
				%>
				<%
					if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
				%>
				<%=smartHF
									.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
									.replace("absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
				%>
				<%
					if (sv.adjustageamt.equals(0)) {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.adjustageamt,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'blank_cell\' ",
												"class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
				<%
					} else {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.adjustageamt,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'output_cell \' ",
												"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
				<%
					}
				%>
				<%
					if ((browerVersion.equals(IE8))) {
				%>
				<%
					if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
				%>
				<%=smartHF
									.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
									.replace("absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
				%>
				<%
					if (sv.adjustageamt.equals(0)) {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.adjustageamt,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'blank_cell\' ",
												"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
				<%
					} else {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.adjustageamt,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'output_cell \' ",
												"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
				<%
					}
				%>
			</div>
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText30).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>
				<%
					}
				%>

				<%
					if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
				%>
				<%
					if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
									.replace("absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
				%>
				<%
					if (sv.rateadj.equals(0)) {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.rateadj,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'blank_cell\' ",
												"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
				<%
					} else {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.rateadj,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'output_cell \' ",
												"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
				<%
					}
				%>
				<%
					if ((browerVersion.equals(IE8))) {
				%>
				<%
					if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
				%>
				<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
									.replace("absolute", "relative")%>
				<%
					} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
				%>
				<%
					if (sv.rateadj.equals(0)) {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.rateadj,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'blank_cell\' ",
												"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
				<%
					} else {
				%>
				<%=smartHF
										.getHTMLVar(0, 0, fw, sv.rateadj,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
										.replace("class=\'output_cell \' ",
												"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%
					}
				%>
				<%
					} else {
				%>
				<%
					}
				%>
				<%
					}
				%>
				<%
					}
				%>
			</div>
			<div class="col-md-3">
				<%
					if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
				<%
					}
				%>
				<div class="form-group" >
					<%
						if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
					%>
					<%
						if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
					%>
					<%
						if (sv.fltmort.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<%
						}
					%>
					<%
						if ((browerVersion.equals(IE8))) {
					%>
					<%
						if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
					%>
					<%
						if (sv.fltmort.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
			<div class="form-group">
				<%
					if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
				<%
					}
				%>
				<div  style="width: 145px !important;">
					<%
						if ((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {
					%>
					<%
						if (((BaseScreenData) sv.loadper) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
					%>
					<%
						if (sv.loadper.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<%
						}
					%>
					<%
						if ((browerVersion.equals(IE8))) {
					%>
					<%
						if (((BaseScreenData) sv.loadper) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
								.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
					%>
					<%
						if (sv.loadper.equals(0)) {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
					<%
						} else {
					%>
					<%=smartHF
									.getHTMLVar(0, 0, fw, sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<%
						}
					%>
				</div>
				</div>
			</div>

		</div>
		<%
			}
		%>
		<div class="row">
			<%
				if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%>
					</label>
					<%
						}
					%>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.premadj) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.premadj, (sv.premadj.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
						%>
						<%
							if (sv.premadj.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.premadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.premadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<div class="col-md-3">
			<div class="form-group">
					<%
						if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
						<%
							if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
						
						<%} else { %>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>						
						<%}%>
					<%
						}
					%>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.zlinstprem) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zlinstprem, (sv.zlinstprem.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zlinstprem) instanceof DecimalData) {
						%>
						<%
							if (sv.zlinstprem.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(0, 0, fw, sv.zlinstprem,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(0, 0, fw, sv.zlinstprem,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		
			<%
				if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%>
					</label>
					<%
						}
					%>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem, (sv.zbinstprem.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
						%>
						<%
							if (sv.zbinstprem.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<%
				if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
						%>
						<%
							if (sv.zstpduty01.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<!-- ILIFE-6941 start-->
			
			<%
				if ((new Byte((sv.lnkgno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%> 
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Number")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.lnkgno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgno, (sv.lnkgno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
	 			<%
				}
			%>
			<!--ILIFE-6941 end -->
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='instPrem' type='text'
							<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width:  145px !important;" <%}%>
							value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.instPrem.getLength(), sv.instPrem.getScale(), 3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.instPrem.getLength(), sv.instPrem.getScale(), 3) - 3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(instPrem)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							xtitle="onBlur='return doBlurNumberNew(event,true);'---- Fix cannot input xxx.xx --- remove Attr onBlur"
							<%if ((new Byte((sv.instPrem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.instPrem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.instPrem).getColor() == null ? "input_cell"
							: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<!-- ILIFE-1474 ENDS -->
						<%
							}
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(BaseScreenData.VISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax ")%>
					</label>
					<%
						}
					%>
					<div >
						<%
							if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='taxamt' type='text'
							<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; width:  145px !important;" <%}%>
							value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3) - 3%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(taxamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
							: (sv.taxamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<!-- ILIFE-1474 ENDS -->
						<%
							}
						%>
					</div>
				</div>
			</div>
			<!--ILIFE-6941 start-->
			<%
				if ((new Byte((sv.lnkgsubrefno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Sub Ref Number")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.lnkgsubrefno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgsubrefno, (sv.lnkgsubrefno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgsubrefno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgsubrefno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
				<%
				}
			%>
			<!--ILIFE-6941 end -->
		</div>
               	<% if (sv.nbprp126lag.compareTo("N") != 0){%>	
               	</div>
               
           <div class="tab-pane fade" id="prem_tab">
           		<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
								<table>
								<tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trancd, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trandesc, true)%></td>
							</tr>
							</table>
						</div>
				  </div>
				  
				  <div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
								<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class="input-group">
									<input name='dateeffDisp' id="dateeffDisp" type='text' value='<%=sv.effdateDisp.getFormData()%>' maxLength='<%=sv.effdateDisp.getLength()%>' 
										size='<%=sv.effdateDisp.getLength()%>' onFocus='doFocus(this)' onHelp='return fieldHelp(dateeffDisp)' onKeyUp='return checkMaxLength(this)'  
										readonly="true"	class="output_cell"	/>						
					
					</div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="effdateDisp"
						data-link-format="dd/mm/yyyy" >
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon" style="visibility: hidden;"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<%
						}
					%>
  </div>
  </div>
             </div>
             
             <div class="row">
             	<div class="col-md-9">
             	<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose")%></label>

					<table><tr>
							<td>
							
							<textarea name='covrprpse' style='width:400px;height:70px;background-color:white;resize:none;border: 2px solid !important;'
							type='text' 
							
							<%if((sv.covrprpse).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
							
							<%
							
									formatValue = (sv.covrprpse.getFormData()).toString();
							
							%>
							 <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=XSSFilter.escapeHtml(formatValue)%>' <%}%>
							
							size='<%= sv.covrprpse.getLength() %>'
							maxLength='<%= sv.covrprpse.getLength() %>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(dgptxt)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.covrprpse).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
								disabled
							<%
								}else if((new Byte((sv.covrprpse).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.covrprpse).getColor()== null  ? 
										"input_cell" :  (sv.covrprpse).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							><%=XSSFilter.escapeHtml(formatValue)%></textarea>
							
							</td></tr>
							
							</table>
					
              </div>
             </div>
             </div>
             
             
             </div>
             </div>
             <%} %>
		<!-- IBPLIFE-2133 End -->

	</div>
</div>


<!-- Cross Browser - ILIFE-2143 by fwang3 -->
<!-- ILIFE-912 Start -- Screen S5125 has UI issues - Field Alignment issue -->
<Div id='mainForm_OPTS' style="visibility: hidden;">
	<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
	<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"))%>
	<%	if(sv.exclind
.getInvisible()!= BaseScreenData.INVISIBLE){
%> 
	<%=smartHF.getMenuLink(sv.exclind, resourceBundleHandler.gettingValueFromBundle("Exclusions"))%>
	<%} %>
	<%=smartHF.getMenuLink(sv.ratypind, resourceBundleHandler.gettingValueFromBundle("Reassurance"))%>
	<%=smartHF.getMenuLink(sv.anntind, resourceBundleHandler.gettingValueFromBundle("Annuity Details"), true)%>
	<%=smartHF.getMenuLink(sv.pbind, resourceBundleHandler.gettingValueFromBundle("Premium Breakdown"))%>


</div>

<br />
<div style='visibility: hidden;'>
	<%
		if (!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

			if (longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
			} else {
				formatValue = formatValue(longValue);
			}

		} else {

			if (longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
			} else {
				formatValue = formatValue(longValue);
			}

		}
	%>
	<div
		class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>
	<%
		longValue = null;
		formatValue = null;
	%>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
<script type='text/javascript'>
    $(document).ready(function() {
        if($("textarea[name='covrprpse']").is(":disabled")){
            $("textarea[name='covrprpse']").css("background-color","")
        }
    });
</script>
