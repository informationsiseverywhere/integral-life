<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6672";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6672ScreenVars sv = (S6672ScreenVars) fw.getVariables();
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					<div class="input-group">
						<input name='chdrsel' type='text' id="chdrsel"
							value='<%=sv.chdrsel.getFormData()%>'
							maxLength='<%=sv.chdrsel.getLength()%>'
							size='<%=sv.chdrsel.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(chdrsel)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.chdrsel).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-btn">

							<button class="btn btn-info"
								style="bottom: 1px; font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.chdrsel).getColor() == null ? "input_cell"
						: (sv.chdrsel).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-btn">
							<button class="btn btn-info"
								style="bottom: 1px; font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"> <b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Lapse Processing")%>
				</b></label>
			</div>
		</div>
	</div>
	<td width='188'>&nbsp; &nbsp;<br /> <input name='action'
		type='hidden' value='<%=sv.action.getFormData()%>'
		size='<%=sv.action.getLength()%>'
		maxLength='<%=sv.action.getLength()%>' class="input_cell"
		onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
		onKeyUp='return checkMaxLength(this)'>
	</td>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
