<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SA509";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sa509ScreenVars sv = (Sa509ScreenVars) fw.getVariables();
%>

<%{
	/* if (appVars.ind01.isOn()) {
		sv.select.setReverse(BaseScreenData.REVERSED);
	}
	if (appVars.ind02.isOn()) {
		sv.select.setEnabled(BaseScreenData.DISABLED);
	}
	
	if (appVars.ind13.isOn()) {
		sv.select.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	if (appVars.ind01.isOn()) {
		sv.select.setReverse(BaseScreenData.REVERSED);
		sv.select.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.select.setHighLight(BaseScreenData.BOLD);
	} */
	/* if (appVars.ind03.isOn()) {
		sv.indic.setInvisibility(BaseScreenData.INVISIBLE);
	} */
}%>
<div class="panel panel-default">
	<div class="panel-body">	
		<br>
		<hr>
		<%-- <div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Investigation Results")%></label>
				</div>
			</div>
		</div> --%>

		<div class="row">
		<!-- Claim Notification Number -->
		<div class="col-md-2">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.notifinum)%></div>
				</div>
			</div>
			</div>
			<!-- Life Assured -->
			<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px; margin-left: 2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			</div>
			<!-- Claimant -->
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Claimant")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.claimant.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.claimant.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.claimant.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td>
								<%
									if (!((sv.clamnme.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.clamnme.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.clamnme.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px; margin-left: 2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<!-- Claim Notification Number -->
			<div class="col-md-4">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship to Life Assured")%></label>
				<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "relation" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("relation");
						longValue = (String) mappedItems.get((sv.relation.getFormData()).toString().trim());
						if (longValue == null || longValue.equals("")) {
							formatValue = formatValue((sv.relation.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>
					<div class='output_cell' style="width: 220px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>		
					<%-- <div  style="width: 100px;"><%=smartHF.getHTMLVarExt(fw, sv.relation)%></div> --%>
				</div>
			</div>
		</div>
		
		<%
			GeneralTable sfl = fw.getTable("sa509screensfl");
			%>
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sa509' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%> </th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Investigation Result")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Date and Time")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					     	 <%
					     		String backgroundcolor="#FFFFFF";
								Sa509screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sa509screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							<%
							{
								
								/* if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
								}
								if (appVars.ind02.isOn()) {
									sv.select.setEnabled(BaseScreenData.DISABLED);
								}
								
								if (appVars.ind13.isOn()) {
									sv.select.setInvisibility(BaseScreenData.INVISIBLE);
								}
								
								if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
									sv.select.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind01.isOn()) {
									sv.select.setHighLight(BaseScreenData.BOLD);
								} */
							}
						
							%>

				<tr class="tableRowTag" id='<%="tablerow"+count%>' >
					<td align="center" <%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="center" <%}%> >			 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sa509screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sa509screensfl.select_R<%=count%>'
						 id='sa509screensfl.select_R<%=count%>'
						 onClick="selectedRow('sa509screensfl.select_R<%=count%>')"
						 class="radio"
					 />
		      	</td>
					<td align="right" <%if(!(((BaseScreenData)sv.seqnoen) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          			<%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.seqnoen, "sa509screensfl", count, sfl, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
              			</td>
					    <td  align="left"
						 <%if((sv.accdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%>><%= sv.accdesc.getFormData()%></td>
									
				    	<td align="right" <%if(!(((BaseScreenData)sv.effdatesDisp) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
						<%-- <%if((sv.effdatesDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
							<%= sv.effdatesDisp.getFormData()%> --%>
							  <table><tr class='edited'><td id='styled'><%= sv.effdatesDisp.getFormData() %></td><td id='styled'><%= sv.acctime.getFormData().toString() %></td></tr></table>
             
						 </td>
						    <td align="left" <%if(!(((BaseScreenData)sv.userid) instanceof StringBase)) {%> align="left"<% }else {%> align="right" <%}%>>
	          			<%=smartHF.getHTMLOutputFieldSFL(sv.userid, "userid", "sa509screensfl", count)%>
             			 </td>
		  			
					</tr>
				
					<%
					count = count + 1;
					Sa509screensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
			 <input type='text' style='display:none;' id='indxflgid' name='indxflg' onFocus='doFocus(this)' value='N' onKeyUp='return checkMaxLength(this)' />
 
	<table>
		<tr>		
		<td>
		    <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperationSa509(2)" class="btn btn-info" id='test1'><%=resourceBundleHandler.gettingValueFromBundle("Create")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(3)" class="btn btn-info" id='test2'><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(4)" class="btn btn-info" id='test3'><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(5)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></a>
			</div>
		</td>
		</tr>
	</table>
	</div>
	</div>
	</div>
	<style>
#mainareaDiv>div>div.panel.panel-default>div>div:nth-child(18)>div:nth-child(2)>div>div>div
	{
	width: 165px !important;
}
.edited{
   background-color: inherit !important;
}
#styled{
	font-size: 13px !important;
	font-weight: 600;
  	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
}
</style>
<script>
	$(document).ready(function() {
    	$('#dataTables-sa509').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollX: true,
        	scrollCollapse:true,
        	moreBtn: true,
        	
      	});
    	fixedColumns: true
    	<%if (appVars.ind26.isOn()) {%>
    	var elems = document.querySelectorAll('[id^="test"]');
    	for (var i = 0; i < elems.length; i++) {
    		document.getElementById(elems.item(i).id).setAttribute("disabled","disabled");
    		document.getElementById(elems.item(i).id).setAttribute("onClick","");
    	}
    	<%}%>
    });
	
	function perFormOperationSa509(act) {
		$("input:radio").each(function(){
		    var $this = $(this);

		    if($this.is(":checked")){
		    	document.getElementById($this.attr('id')).value = act; 
		    }
		});
		document.getElementById('indxflgid').value = 'Y';  
		doAction('PFKEY0'); 
	}
	
</script> 

	<%@ include file="/POLACommon2NEW.jsp"%>
