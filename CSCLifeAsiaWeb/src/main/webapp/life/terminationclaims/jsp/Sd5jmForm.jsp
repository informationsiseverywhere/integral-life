<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SD5JM";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sd5jmScreenVars sv = (Sd5jmScreenVars) fw.getVariables();%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-5" style="padding-right:0px ;width:350px; margin-top: 10px;">
				<div class="form-group">
					<strong>
						<%=resourceBundleHandler.gettingValueFromBundle("Confirm Claim Payees Payout Details? (Y/N)")%>
					</strong>
				</div>
			</div>
            <div class="col-md-2" style="padding-left:0px">
                <div class="form-group">
                    <div class="input-group" style="min-width: 70px;">
                        <%=smartHF.getHTMLVarExt(fw, sv.confirm)%>
                    </div>
                </div>
            </div>
			<%--<div class="col-md-2" style="padding-left:0px">
				<div class="form-group">
					<select name='confirm' style="width:100px; margin-top: 3px;"
							onFocus='doFocus(this)'
							onHelp='return fieldHelp(confirm)'
							onKeyUp='return checkMaxLength(this)'
							class = 'input_cell'>
						<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
						<option value="Y"<% if(((sv.confirm.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
						<option value="N"<% if(((sv.confirm.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
					</select>
				</div>
			</div>--%>

		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
