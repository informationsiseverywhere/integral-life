<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sa621";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sa621ScreenVars sv = (Sa621ScreenVars) fw.getVariables();%>
<%{
	
	
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<%if ((new Byte((sv.notifinum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
					<label>	
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%>
					</label>
				<div class="input-group">
								 <%					
						if(!((sv.notifinum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.notifinum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.notifinum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
    	   	<div class="col-md-4" style='width:100% !important;'> 
	        	<div class="form-group">
	        	<label>	
					<%=resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes")%>
				</label>
	        		<div class="tabblock" id="navbar">
						<div class="tabcontent">				
							<div id="content1">						
							<%-- <textarea id='textarea1'
									style='width: 100%; height: 335px; z-index: 2;'
									onFocus='doFocus(this)' onHelp='return fieldHelp("textarea1")'
									onkeyup="" id='savenotesbtn' class='textarea bold'
									<%if ((new Byte((sv.notes).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
									readonly="true" disabled class="output_cell"
									<%} else if ((new Byte((sv.notes).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								 </textarea>	 --%>
								 <textarea id='notes'  name="notes"  value=''style='width: 100%; height: 335px; z-index: 2;'
									onFocus='doFocus(this)' onHelp='return fieldHelp("notes")'
									onkeyup="" class='textarea bold'
									<%if ((new Byte((sv.notes).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
									readonly="true" disabled class="output_cell"
									<%} else if ((new Byte((sv.notes).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%> class='input_cell' <%}%>><%=sv.notes.getFormData().toString().trim() %></textarea>	
								</div>				
							</div>
						</div>
	 		</div>
		</div>
	</div>
	<input type="hidden" name="datime" id="maintime">
</div>
</div>
<script>
var value = new Date();
$("#maintime").val(value);

</script>
<%@ include file="/POLACommon2NEW.jsp"%>
