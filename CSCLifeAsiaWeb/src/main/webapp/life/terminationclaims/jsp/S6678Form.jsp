<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "S6678";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6678ScreenVars sv = (S6678ScreenVars) fw.getVariables();
%>

<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"1 - Exclusion clause, 2 - Follow up extended text");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Act?");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Type");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "St");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reminder Date");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Created Date");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cat");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Received date");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Expiry Date");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Remarks");
%>
<%
	appVars.rollup(new int[]{93});
%>
<!-- ILIFE-2836 starts-->
<!-- <style>
@media \0screen\,screen\9
{
.iconPos{margin-bottom:1px}
img{margin-top:1px;}
}
</style> -->

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 1px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<%
			GeneralTable sfl = fw.getTable("s6678screensfl");
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6678' width='100%'>
						<thead>
							<tr class='info'>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Act?")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Status")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Reminder Date")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Created Date")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Category")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Received Date")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Expiry Date")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Remarks")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								S6678screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								Map<String, Map<String, String>> fupStatMap = appVars.loadF4FieldsLong(new String[]{"fupstat"}, sv, "E",
										baseModel);

								while (S6678screensfl.hasMoreScreenRows(sfl)) {

									{
										if (appVars.ind30.isOn()) {
											sv.fupcdes.setReverse(BaseScreenData.REVERSED);
											sv.fupcdes.setColor(BaseScreenData.RED);
										}
										if (appVars.ind38.isOn()) {
											sv.fupcdes.setEnabled(BaseScreenData.DISABLED);
										}
										if (!appVars.ind30.isOn()) {
											sv.fupcdes.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind02.isOn()) {
											sv.fupstat.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind32.isOn()) {
											sv.fupstat.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind02.isOn()) {
											sv.fupstat.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind02.isOn()) {
											sv.fupstat.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind03.isOn()) {
											sv.fupremdtDisp.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind32.isOn()) {
											sv.fupremdtDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind03.isOn()) {
											sv.fupremdtDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind03.isOn()) {
											sv.fupremdtDisp.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind04.isOn()) {
											sv.fupremk.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind33.isOn()) {
											sv.fupremk.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind04.isOn()) {
											sv.fupremk.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind04.isOn()) {
											sv.fupremk.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind05.isOn()) {
											sv.sfflg.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind34.isOn()) {
											sv.sfflg.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind05.isOn()) {
											sv.sfflg.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind05.isOn()) {
											sv.sfflg.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind40.isOn()) {
											sv.fuptype.setInvisibility(BaseScreenData.INVISIBLE);
										}
										if (appVars.ind41.isOn()) {
											sv.fuprcvdDisp.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind43.isOn()) {
											sv.fuprcvdDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind41.isOn()) {
											sv.fuprcvdDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind41.isOn()) {
											sv.fuprcvdDisp.setHighLight(BaseScreenData.BOLD);
										}
										if (appVars.ind42.isOn()) {
											sv.exprdateDisp.setReverse(BaseScreenData.REVERSED);
										}
										if (appVars.ind44.isOn()) {
											sv.exprdateDisp.setEnabled(BaseScreenData.DISABLED);
										}
										if (appVars.ind42.isOn()) {
											sv.exprdateDisp.setColor(BaseScreenData.RED);
										}
										if (!appVars.ind42.isOn()) {
											sv.exprdateDisp.setHighLight(BaseScreenData.BOLD);
										}
									}
							%>
							<tr>
								<td class="tableDataTag tableDataTagFixed"
									style="width:<%if (imageFolder.contains("eng")) {%><%=10%><%} else {%><%=10%><%}%>px;"
									<%if ((sv.sfflg).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>><input
									type="radio" value='<%=sv.sfflg.getFormData()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp("s6678screensfl" + "." +
						 "sfflg")'
									onKeyUp='return checkMaxLength(this)'
									name='s6678screensfl.sfflg_R<%=count%>'
									id='s6678screensfl.sfflg_R<%=count%>'
									onClick="selectedRow('s6678screensfl.sfflg_R<%=count%>')"
									class="radio" /></td>
								<td class="tableDataTag" style="width:<%=200%>px;"
									<%if ((sv.fupcdes).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<!--dropdown convert into pop-up atiwari23--> <!-- Ticket ILIFE-990 starts -->
									<%
										longValue = sv.fupremdtDisp.getFormData();
									%> <%
 	if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<div style="min-width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <%
 	longValue = null;
 %> <%
 	} else {
 %><!-- Ticket ILIFE-990 ends -->
									<div class="input-group" style="width: 100px;">
										<input
											name='<%="s6678screensfl" + "." + "fupcdes" + "_R" + count%>'
											id='<%="s6678screensfl" + "." + "fupcdes" + "_R" + count%>'
											type='text' value='<%=sv.fupcdes.getFormData()%>'
											class=" <%=(sv.fupcdes).getColor() == null
							? "input_cell"
							: (sv.fupcdes).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
											maxLength='<%=sv.fupcdes.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6678screensfl.fupcdes)'
											onKeyUp='return checkMaxLength(this)'
											style="width:<%=sv.fupcdes.getLength() * 12%>px;"> <span
											class="input-group-btn">
											<button class="btn btn-info"
												style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
												;
								type="button"
												onClick="doFocus(document.getElementById('<%="s6678screensfl" + "." + "fupcdes" + "_R" + count%>')); doAction('PFKEY04')">
												<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
											</button>
										</span>
										<%
											}
										%>
									</div>


								</td>
								<td class="tableDataTag" style="width: 150px;"
									<%if ((sv.fupstat).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<%
										mappedItems = (Map) fupStatMap.get("fupstat");
											optionValue = makeDropDownList(mappedItems, sv.fupstat, 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.fupstat.getFormData()).toString().trim());
									%> <%
 	if ((new Byte((sv.fupstat).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| fw.getVariables().isScreenProtected()) {
 %> <!-- ILIFE-2073-STARTS -->
									<div style="width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <!--<div class='output_cell'>  --> <%--<%=longValue%> --%> <!--</div> -->
									<!--ILIFE-2073-ENDS --> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.fupstat).getColor())) {
 %>
									<div
										style="border: 1px; border-style: solid; border-color: #B55050; width: 198px;">
										<%
											}
										%>
										<select name='<%="s6678screensfl.fupstat_R" + count%>'
											id='<%="s6678screensfl.fupstat_R" + count%>' type='list'
											style="width: 198px;" class='input_cell'>
											<%=optionValue%>
										</select>
										<%
											if ("red".equals((sv.fupstat).getColor())) {
										%>
									</div> <%
 	}
 %> <%
 	}
 %>


								</td>
								<td class="tableDataTag" style="width: 170px;"
									<%if ((sv.fupremdtDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<%
										longValue = sv.fupremdtDisp.getFormData();
									%> <%
 	if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<div style="width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
									<div class="input-group date form_date col-md-12" data-date=""
										data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
										data-link-format="dd/mm/yyyy" style="width: 150px;">
										<input
											name='<%="s6678screensfl" + "." + "fupremdtDisp" + "_R" + count%>'
											id='<%="s6678screensfl" + "." + "fupremdtDisp" + "_R" + count%>'
											type='text' value='<%=sv.fupremdtDisp.getFormData()%>'
											class=" <%=(sv.fupremdtDisp).getColor() == null
							? "input_cell"
							: (sv.fupremdtDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
											maxLength='<%=sv.fupremdtDisp.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6678screensfl.fupremdtDisp)'
											onKeyUp='return checkMaxLength(this)' class="input_cell"
											style="width: <%=sv.fupremdtDisp.getLength() * 12%> px;">

										<a href="javascript:;"
											onClick="showCalendar(this, document.getElementById(
					'<%="s6678screensfl" + "." + "fupremdtDisp" + "_R" + count%>'),'<%=av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">


										</a> <span class="input-group-addon"> <span
											class="glyphicon glyphicon-calendar"></span>
										</span>

										<%
											}
										%>

									</div>
								</td>
								<td class="tableDataTag" style="width: 500px;"
									<%if ((sv.crtdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>><%=sv.crtdateDisp.getFormData()%>
								</td>
								<td class="tableDataTag" style="width: 200px;"
									<%if ((sv.fuptype).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>><%=sv.fuptype.getFormData()%>
								</td>
								<td class="tableDataTag" style="width: 200px;"
									<%if ((sv.fuprcvdDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<%
										longValue = sv.fuprcvdDisp.getFormData();
									%> <%
 	if ((new Byte((sv.fuprcvdDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<div style="min-width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
									<div class="input-group date form_date col-md-12" data-date=""
										data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
										data-link-format="dd/mm/yyyy" style="width: 150px;">
										<input
											name='<%="s6678screensfl" + "." + "fuprcvdDisp" + "_R" + count%>'
											id='<%="s6678screensfl" + "." + "fuprcvdDisp" + "_R" + count%>'
											type='text' value='<%=sv.fuprcvdDisp.getFormData()%>'
											class=" <%=(sv.fuprcvdDisp).getColor() == null
							? "input_cell"
							: (sv.fuprcvdDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
											maxLength='<%=sv.fuprcvdDisp.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6678screensfl.fuprcvdDisp)'
											onKeyUp='return checkMaxLength(this)' class="input_cell"
											style="width: <%=sv.fuprcvdDisp.getLength() * 12%> px;">

										<a href="javascript:;"
											onClick="showCalendar(this, document.getElementById(
					'<%="s6678screensfl" + "." + "fuprcvdDisp" + "_R" + count%>'),'<%=av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">


										</a><span class="input-group-addon"> <span
											class="glyphicon glyphicon-calendar"></span>
										</span>
										<%
											}
										%>
									</div>

								</td>
								<td class="tableDataTag" style="width: 200px;"
									<%if ((sv.exprdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<%
										longValue = sv.exprdateDisp.getFormData();
									%> <%
 	if ((new Byte((sv.exprdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<div style="width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <%
 	longValue = null;
 %> <%
 	} else {
 %>
									<div class="input-group date form_date col-md-12" data-date=""
										data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
										data-link-format="dd/mm/yyyy" style="width: 150px;">
										<input
											name='<%="s6678screensfl" + "." + "exprdateDisp" + "_R" + count%>'
											id='<%="s6678screensfl" + "." + "exprdateDisp" + "_R" + count%>'
											type='text' value='<%=sv.exprdateDisp.getFormData()%>'
											class=" <%=(sv.exprdateDisp).getColor() == null
							? "input_cell"
							: (sv.exprdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
											maxLength='<%=sv.exprdateDisp.getLength()%>'
											onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6678screensfl.exprdateDisp)'
											onKeyUp='return checkMaxLength(this)' class="input_cell"
											style="width: <%=sv.exprdateDisp.getLength() * 12%> px;">

										<a href="javascript:;"
											onClick="showCalendar(this, document.getElementById(
					'<%="s6678screensfl" + "." + "exprdateDisp" + "_R" + count%>'),'<%=av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)">
										</a><span class="input-group-addon"> <span
											class="glyphicon glyphicon-calendar"></span>
										</span>
										<%
											}
										%>

									</div>
								</td>
								<td class="tableDataTag" style="width: 100px;"
									<%if ((sv.fupremk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="center" <%}%>>
									<!-- Ticket ILIFE-990 starts --> <%
 	longValue = sv.fupremdtDisp.getFormData();
 %> <%
 	if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 				|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
									<div style="width: 100px;"
										class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "input_cell"%>'>
										<%
											if (longValue != null) {
										%>

										<%=longValue%>

										<%
											}
										%>
									</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <input type='text' maxLength='<%=sv.fupremk.getLength()%>'
									value='<%=sv.fupremk.getFormData()%>'
									class=" <%=(sv.fupremdtDisp).getColor() == null
							? "input_cell"
							: (sv.fupremdtDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
									size='<%=sv.fupremk.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(s6678screensfl.fupremk)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="s6678screensfl" + "." + "fupremk" + "_R" + count%>'
									id='<%="s6678screensfl" + "." + "fupremk" + "_R" + count%>'
									class="input_cell"
									style="width: <%=sv.fupremk.getLength() * 12%> px;"> <%
 	}
 %> <!-- Ticket ILIFE-990 ends -->
								</td>

							</tr>

							<%
								count = count + 1;
									S6678screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<table>
					<tr>
						<td>
							<div class="sectionbutton">
								<a href="#" onClick="JavaScript:perFormOperation(1)"
									class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Clause")%></a>
							</div>
						</td>
						<td>&nbsp;&nbsp;</td>
						<td>
							<div class="sectionbutton">
								<a href="#" onClick="JavaScript:perFormOperation(2)"
									class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Follow Up Extended Text")%></a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-s6678').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			info : true,
			paging : true
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script type="text/javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>

<%
	}
%>
