<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5016";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5016ScreenVars sv = (S5016ScreenVars) fw.getVariables();%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind30.isOn()) {
			sv.fupcdes.setReverse(BaseScreenData.REVERSED);
			sv.fupcdes.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.fupcdes.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind30.isOn()) {
			sv.fupcdes.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.jlife.setReverse(BaseScreenData.REVERSED);
			sv.jlife.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.jlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.fupstat.setReverse(BaseScreenData.REVERSED);
			sv.fupstat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.fupstat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.fupremdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.fupremdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.fupremdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.fupremk.setReverse(BaseScreenData.REVERSED);
			sv.fupremk.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.fupremk.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.sfflg.setReverse(BaseScreenData.REVERSED);
			sv.sfflg.setColor(BaseScreenData.RED);
		}
		if (appVars.ind47.isOn()) {
			sv.sfflg.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind44.isOn()) {
			sv.sfflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.fuptype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind41.isOn()) {
			sv.fuprcvdDisp.setReverse(BaseScreenData.REVERSED);
			sv.fuprcvdDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind39.isOn()) {
			sv.fuprcvdDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind41.isOn()) {
			sv.fuprcvdDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.exprdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.exprdateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind39.isOn()) {
			sv.exprdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind42.isOn()) {
			sv.exprdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Exclusion clause, 2 - Follow up extended text");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act?");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/L");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reminder Date");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Created Date");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cat");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Received Date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Expiry Date");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Remarks");%>
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		       		<table><tr><td>
		       			<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td style="padding-left:1px;">
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
	
					  		</td><td style="padding-left:1px;min-width:65px;">
							<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
								<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"ctypedes"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("ctypedes");
								longValue = (String) mappedItems.get((sv.ctypedes.getFormData()).toString().trim());  
							%>
						
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
			       	<div class="col-md-4">
			       	</div>
			     
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td><td style="padding-left:1px;">
					
						<%					
						if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 70px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <%
		 	GeneralTable sfl = fw.getTable("s5016screensfl");
			%>
			<script language="javascript">
					        $(document).ready(function(){
					        	var rows = <%=sfl.count() + 1%>;
								var isPageDown = 1;
								var pageSize = 1;
								
							<%-- 	<%if (!appVars.ind54.isOn()) {%> /*BSIBF-119 START*/
								var headerRowCount= 1;
								<%} else {%>
								var headerRowCount= rows;
								 <%}%>
								 --%>
							
								
								var headerRowCount= rows; 
								 					 	/*BSIBF-119 END*/
								 //IFSU-1092 Start
								var fields = new Array();	//required fields
								fields[0] = "fuptype";
								//IFSU-1092 End
	
					        	operateTableForSuperTable(rows,isPageDown,pageSize,fields,"dataTables-s5016",null,headerRowCount);
	
	
        }); 
					    </script>
			<div class="row">		
		 		<div class="col-md-12">
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5016' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      	
							<%
							S5016screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							Map<String,Map<String,String>> fupStatMap = appVars.loadF4FieldsLong(new String[] {"fupstat"},sv,"E",baseModel);
							
							while (S5016screensfl.hasMoreScreenRows(sfl)) {
						
							%>
							
					<%
						// ILIFE-8368 Start 
		        	{
						if (appVars.ind31.isOn()) {
							sv.lifeno.setReverse(BaseScreenData.REVERSED);
						}
						if (appVars.ind31.isOn()) {
							sv.lifeno.setColor(BaseScreenData.RED);
						}
						if (!appVars.ind31.isOn()) {
							sv.lifeno.setHighLight(BaseScreenData.BOLD);
						}
						// ILIFE-8368 End 
		  	      }
					%>
		<tr class="tableRowTag"  id='<%="tablerow"+count%>' >
							
							
							<td>
											<input type="checkbox" 
												value='<%= sv.sfflg.getFormData() %>' 
								 			 	onFocus='doFocus(this)' onHelp='return fieldHelp("s5016screensfl" + "." +"select")' onKeyUp='return checkMaxLength(this)' 
								 			 	name='s5016screensfl.sfflg_R<%=count%>'
								 				id='s5016screensfl.sfflg_R<%=count%>'
								 				onClick="selectedRow('s5016screensfl.sfflg_R<%=count%>')"
							 				/>
							</td>	
							
									
											<td>	
												
												<%= sv.fuptype.getFormData()%>
												</td>
												<TD>
													<%
							fieldItem=appVars.loadF4FieldsShort(new String[] {"fupcdes"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("fupcdes");
							optionValue = makeDropDownList( mappedItems , sv.fupcdes.getFormData(),2,resourceBundleHandler);
				// 	<!-- MIBT-194 START -->
				if(mappedItems.containsKey((sv.fupcdes.getFormData()).toString().trim())){
						    longValue = (String) mappedItems.get((sv.fupcdes.getFormData()).toString().trim());
					}
				else{
			    	longValue=(sv.fupcdes.getFormData()).toString().trim();
			    }
				// 			<!--    MIBT-194 END -->
						    if(longValue==null)
						    {
						      longValue="";
						    }
						%>
						<%if((new Byte((sv.fupcdes).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){%>
						<div class='output_cell' style="width: 60px;">
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>
							<%longValue = null;%>
						<%
						}else {
						%>					
								
						<div class="input-group" style="width: 150px;">
				           <%=smartHF.getRichTextInputFieldLookup(fw, sv.fupcdes).replaceAll(">"," onblur = 'return changeCase(this)'  >")%>
				             <span class="input-group-btn">
				             <button class="btn btn-info"
				                style="font-size: 19px; border-bottom-width: 2px !important;"
				                type="button"
				                onClick="doFocus(document.getElementById('<%="s5016screensfl" + "." + "fupcdes" + "_R" + count%>')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
						
						<%
					 	 }
					 	%>
												</TD>
												
										<%-- 	<td >
												<div class="input-group" style="min-width: 160px;">
												<input name='<%="s5016screensfl" + "." +
												 "fupcdes" + "_R" + count %>'
												type='text' id='fupcdes'
												value='<%= sv.fupcdes.getFormData() %>'
												maxLength='<%=sv.fupcdes.getLength()%>' 
												onFocus='doFocus(this)' onHelp='return fieldHelp(s5016screensfl.fupcdes)' onKeyUp='return checkMaxLength(this)' 
												 style = "width: 80px;"
												<% if((new Byte((sv.fupcdes).getEnabled()))
															.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ) {  %>
												 
													 	readonly="true"
														class="output_cell" >
														<%}else {%> 
												class = ' <%=(sv.fupcdes).getColor()== null  ? 
															"input_cell" :  (sv.fupcdes).getColor().equals("red") ? 
															"input_cell red reverse" : "input_cell" %>'
												
												>	
												
												<span class="input-group-btn">
													<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('fupcdes')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
													</button>
												</span>	
															
												<a href="javascript:;" 
												onClick="doFocus(document.getElementById('<%="s5016screensfl" + "." +
												 "fupcdes" + "_R" + count %>')); changeF4Image(this); doAction('PFKEY04');"> 
												
												<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0">
												</a>
											
													<%} %>		</div>	
															</td> --%>
							<!--<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
								-->

									<td>
									<!-- ILIFE-8368 Start -->
										<div class="form-group">
											<%
												sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.lifeno).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.lifeno);
											%>


											<input type='text' maxLength='<%=sv.lifeno.getLength()%>'
												value='<%=formatValue%>'
												size='<%=sv.lifeno.getLength()%>' onFocus='doFocus(this)'
												onHelp='return fieldHelp(s5016screensfl.lifeno)'
												onKeyUp='return checkMaxLength(this)'
												name='<%="s5016screensfl" + "." + "lifeno" + "_R" + count%>'
												id='<%="s5016screensfl" + "." + "lifeno" + "_R" + count%>'
												style="width: 100px;"
												<%if ((new Byte((sv.lifeno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
												readonly="true" class="output_cell"
												<%} else if ((new Byte((sv.lifeno).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
												class="bold_cell form-control" <%} else {%>
												class=' <%=(sv.lifeno).getColor() == null ? "input_cell"
							: (sv.lifeno).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
												<%}%>>

										</div>
												<!--ILIFE-8368 End -->
									</td>


									<!--<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																							
																			
											
											 						 
												 
												 						 -->
												 						<td > 
												 						 <input type='text' 
												maxLength='<%=sv.jlife.getLength()%>'
												 value='<%= sv.jlife.getFormData() %>' 
												 size='<%=sv.jlife.getLength()%>'
												 onFocus='doFocus(this)' onHelp='return fieldHelp(s5016screensfl.jlife)' onKeyUp='return checkMaxLength(this)' 
												 name='<%="s5016screensfl" + "." +
												 "jlife" + "_R" + count %>'
												 style = "width:100px;"
												 <% if((new Byte((sv.jlife).getEnabled()))
															.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ) {  %>
												 
													 	readonly="true"
														class="output_cell" >
														<%}else {%> 
												 
												 class = "input_cell"
												  
												  
												  >
												  
												  						 
															<%} %>	
											
																	
															</td>
										    									<!--<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																							
																		
										      				     -->
										      				     <td >
										      				     <%	
												mappedItems = (Map) fupStatMap.get("fupstat");
												optionValue = makeDropDownList( mappedItems , sv.fupstat,2,resourceBundleHandler);  
												longValue = (String) mappedItems.get((sv.fupstat.getFormData()).toString().trim());  
											%>
											<% if((new Byte((sv.fupstat).getEnabled()))
												.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
											%>  
											<div class='output_cell' style="width:140px;"> 
											<%if(longValue!=null) { %>
											   <%=XSSFilter.escapeHtml(longValue)%>
											<%}else {%>
											   <%=""%>
											<%} %>
											</div>
											
											<%
											longValue = null;
											%>
						
											<% }else {%>
											<% if("red".equals((sv.fupstat).getColor())){
											%>
											<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
											<%
											} 
											%>
											<select name='<%="s5016screensfl.fupstat_R"+count%>' type='list' style="min-width:210px;"
											
											class = 'input_cell'
											>
											<%=optionValue%>
											</select>
											<% if("red".equals((sv.fupstat).getColor())){
											%>
											</div>
											<%
											} 
											%>
										    <%
												} 
											%>
											</td>
										    									
											<td style="min-width: 160px;">	
											<% if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="fupremdtDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp, (sv.fupremdtDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 			
	
											</td>
										    	
										    	
											<td style="min-width:160px;">
											<% if ((new Byte((sv.fupremdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="fupremdtDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.fupremdtDisp, (sv.fupremdtDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 			
											
	
											</td>
										    	
				<td style="min-width:160px;">	
											
											<% if ((new Byte((sv.fuprcvdDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                   <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.fuprcvdDisp)%>
                                       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="fuprcvdDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.fuprcvdDisp, (sv.fuprcvdDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%> 			
			</td>
										    									
				<td style="min-width:160px;">
						
					<% if ((new Byte((sv.exprdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
                           <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.exprdateDisp)%>
                                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                           </div>
                	<%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                data-date-format="dd/mm/yyyy" data-link-field="exprdateDisp"
                                data-link-format="dd/mm/yyyy" style="width: 150px;">
                                <%=smartHF.getRichTextDateInput(fw, sv.exprdateDisp, (sv.exprdateDisp.getLength()))%>
                              <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                             </span>
                           </div>
                                  
             			<%}%>
             	</td>
										   
				<td > 						 
				<input type='text' 
					   maxLength='<%=sv.fupremk.getLength()%>'
					   value='<%= sv.fupremk.getFormData() %>' 
					   size='<%=sv.fupremk.getLength()%>'
   			      	   style = "width: 100px;"
					   onFocus='doFocus(this)' onHelp='return fieldHelp(s5016screensfl.fupremk)' onKeyUp='return checkMaxLength(this)' 
												name='<%="s5016screensfl" + "." + "fupremk" + "_R" + count %>'
					 <% if((new Byte((sv.fupremk).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ) {  %>
					 	readonly="true"
						class="output_cell" >
						<%}else {%>
						<td class = "input_cell"/>  
								<%} %>
				</td>
					
								
								
	     </tr>


							<%
						
							count = count + 1;
							S5016screensfl.setNextScreenRow(sfl, appVars, sv);
							}
							%>
					      </tbody>
					</table>
					</div>
			</div>
		</div>
		  <div class="sectionbutton" >
	                    <table><tr><td> 
			<a href="#" onClick="JavaScript:perFormOperation(1)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Exclusion Clause")%></a>
			</td> <!-- <td> &nbsp;&nbsp;</td> -->
			<td> 
			<a href="#" onClick="JavaScript:perFormOperation(2)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Follow Up Extended Text")%></a>
			</td> <!-- <td> &nbsp;&nbsp;</td> -->
			<td> 
			<a id="subfile_add" class="btn btn-info" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
			</td> 
			<td> 
			<a id="subfile_remove" class="btn btn-info"	href='javascript:;' disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
			</td> 
					
			</tr></table></div>
		    
	
	 </div>
</div>		
<!-- <script>
$(document).ready(function() {
	$('#dataTables-s5016').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
  	});
})
</script> -->
<script>
       $(document).ready(function() {
              $('#dataTables-s5016').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : '300',
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
              
     $("#subfile_remove").click(function () {
              	
          		$('input:checkbox[type=checkbox]').prop('checked',false);
          	    	
          	   	$("#subfile_remove").attr('disabled', 'disabled'); 

              });  
              
              $('input[type="checkbox"]'). click(function(){
              	if($(this). prop("checked") == true){
              		 $('#subfile_remove').removeAttr('disabled');
              	}else{
              		 $('#subfile_remove').attr("disabled","disabled");   
              	}
              
              });
              
       });
       
       function changeCase(elem)
       {
           elem.value = elem.value.toUpperCase();
       }
</script> 


<%@ include file="/POLACommon2NEW.jsp"%>

