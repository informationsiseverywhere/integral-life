

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5231";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5231ScreenVars sv = (S5231ScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Schedule Name         ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month      ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number       ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year       ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date        ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company               ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue             ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch                ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Vesting Date Range ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts from      ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date From)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to      ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date To)");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number Range ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts from      ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to      ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.datefrmDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefrmDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datefrmDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>














<div class="panel panel-default">
      <div class="panel-body">     

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%></label>
 
<div class="input-group">
<%					
		if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		<%	
			qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber).replace(",","");
			
			if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
</div></div></div>




<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%></label>
                       
                       <div class="input-group" style="max-width:50px;">
                       <%	
			qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
			
			if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	<%	
			qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
			
			if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>

</div></div></div>




<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
	<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  </div></div>


</div>





	 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%></label>
 
<%					
		if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>


</div></div>





<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>' style="width:100px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	
	
	</div>
	
	</div>




<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
                       
         <div style="position: relative; margin-left:1px;width:100px" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
	<%
	   longValue = null;
	   formatValue = null;
	%>
	
	</div></div>
	</div>
	<br><br>
	<div class="row">	
	<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Vesting Date Range")%></label>
                       
                       
                       
            </div></div></div>           
	
	
	<br>
                       
                       
                       


<div class="row">	
 
			    	<div class="col-md-5"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts from")%></label>


<table><tr><td>
<%
								if((new Byte((sv.datefrmDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
								 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
							<%
								}else{
							%>
							<div class="input-group date form_date col-md-12"  style="max-width:220px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>
							<%
								}
							%>
				    </div>
				 </div>
</td><td>

<div class="label_txt">
&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle( "to")%>&nbsp;&nbsp;
</div>

</td><td>


<%	

if((new Byte((sv.datefrmDisp).getEnabled()))
.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
%>
 <%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
<%
}else{
%>
<div class="input-group date form_date col-md-12"  style="max-width:220px;"data-date="" data-date-format="dd/mm/yyyy" data-link-field="startDateDisp" data-link-format="dd/mm/yyyy">
<%=smartHF.getRichTextDateInput(fw, sv.datefrmDisp,(sv.datefrmDisp.getLength()))%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
<%
}
%>


</td></tr></table>

</div></div>


 
</div>

<br>

<div class="row">
	<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number Range")%></label>

</div></div>
</div>


<br>


<div class="row">
	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts from")%></label>
                       
                       
                <table><tr><td>       
                       
<input name='chdrnum' 
type='text'

<%if((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.chdrnum.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.chdrnum.getLength()%>'
maxLength='<%= sv.chdrnum.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.chdrnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.chdrnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.chdrnum).getColor()== null  ? 
			"input_cell" :  (sv.chdrnum).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</td><td>

<div class="label_txt">
&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("to")%>&nbsp;&nbsp;
</div>



</td><td>


<input name='chdrnum1' 
type='text'

<%if((sv.chdrnum1).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.chdrnum1.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.chdrnum1.getLength()%>'
maxLength='<%= sv.chdrnum1.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum1)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.chdrnum1).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.chdrnum1).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.chdrnum1).getColor()== null  ? 
			"input_cell" :  (sv.chdrnum1).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</td></tr></table>

</div></div>
</div>



































</div></div>




<%@ include file="/POLACommon2NEW.jsp"%>

