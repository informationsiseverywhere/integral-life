<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5228";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>

<%
	appVars.rollup();
	appVars.rolldown();
%>
<%S5228ScreenVars sv = (S5228ScreenVars) fw.getVariables();%>


<%if (sv.S5228screenWritten.gt(0)) {%>
	<%S5228screen.clearClassString(sv);}%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number         ");%>
	
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No     ");%>
	
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life No   ");%>
	
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life    ");%>
	
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex                     ");%>
	
	<%sv.sex.setClassString("");%>
<%	sv.sex.appendClassString("string_fld");
	sv.sex.appendClassString("output_txt");
	sv.sex.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date of Birth");%>

	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>

	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Birth");%>
	
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%sv.dobDisp.setClassString("");%>
<%	sv.dobDisp.appendClassString("string_fld");
	sv.dobDisp.appendClassString("output_txt");
	sv.dobDisp.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date of Death" );%>
	
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>
	
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Death");%>
	
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%sv.dtofdeathDisp.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason for Death        ");%>
	
	<%sv.reasoncd.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason Description      ");%>
	
	<%sv.resndesc.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setReverse(BaseScreenData.REVERSED);
			sv.dtofdeathDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
					<%					
					if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="min-width: 50px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life No")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.life.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.life.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Joint Life No")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				</td>
				<td>
				
				
				
				
				<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 150px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       	</td>
		       	</tr>
		       	</table>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Sex")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.sex).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.sex.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.sex.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.sex.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       		<div class="col-md-4"></div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Date of Birth")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.dobDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.dobDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.dobDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.dobDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%>
						<%}%></label>
		       		<div class="input-group" style="min-width: 150px;">
		       		<%	
						if ((new Byte((sv.dtofdeathDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.dtofdeathDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.dtofdeathDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
				 
					
					<% 
						if((new Byte((sv.dtofdeathDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					
					<%
						}else if((new Byte((sv.dtofdeathDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtofdeathDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.dtofdeathDisp,(sv.dtofdeathDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
										
					<%
						}else { 
					%>
					
					
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtofdeathDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.dtofdeathDisp,(sv.dtofdeathDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
					
					
					<%} }}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Reason for Death")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:230px;"> 
					<%
					} 
					%>
					<script type="text/javascript">
					
					function change()
					{
						document.getElementsByName("resndesc")[0].value="";
						doAction('PFKEY05');
						
					}
					</script>
					<select name='reasoncd' type='list' style="width:230px;"
					<% 
						if((new Byte((sv.reasoncd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.reasoncd).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					  onchange="change()" 
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.reasoncd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Reason Description")%>
					<%}%></label>
		       		<div class="input-group" style="min-width: 250px;">
		       		<%if ((new Byte((sv.resndesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

						<%
								longValue = null;
								formatValue = null;
								%>
						
						
						
						<input name='resndesc' 
						type='text'
						
						
						<%
						
						fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("reasoncd");
						optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
						formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
						
						%>
						
						<%if(formatValue==null) {
						formatValue="";
						}
						%>
						
							<% 		String str=(sv.resndesc.getFormData()).toString().trim(); %>
							<% if(str.equals("") || str==null) {
								str=formatValue;
							}
						
						%>
						value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
						size='50'
						maxLength='50' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(resndesc)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.resndesc).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){  
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.resndesc).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.resndesc).getColor()== null  ? 
									"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%
								longValue = null;
								formatValue = null;
								%>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Birth")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Death")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>


</td><td width='188'>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>
<%}%>
</td>
</tr></table></div>