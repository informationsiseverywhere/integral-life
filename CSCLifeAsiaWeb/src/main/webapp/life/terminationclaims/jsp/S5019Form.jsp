<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5019";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5019ScreenVars sv = (S5019ScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"--------------------------");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy loans ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other Adjustments ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Debt ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjustment reason ");%>

<%{
		if (appVars.ind60.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.otheradjst.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.currcd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind08.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.estimateTotalValue.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.clamant.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.reasoncd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind60.isOn()) {
			sv.resndesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (appVars.ind60.isOn()) {
			sv.resndesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind06.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.policyloan.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.tdbtamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Suffix No ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov/Rid");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ccy");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Estimated");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Value");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Value");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind10.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.btdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.btdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.btdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<%
fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd","reasoncd"},sv,"E",baseModel);
%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					<%}%></label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</td><td>
					
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 20px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		       	
			       	
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
					<%}%></label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						</td><td>
					
						<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		       	
		       		
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%}%>
		       		<div class="input-group">
		       		<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       		</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
						<%}%>
		       		</label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						</td><td>
					
						<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		       	
			    
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					<%}%></label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				  </td><td>
				
				
					<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		       	
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("J/Life")%>
						<%}%></label>
		       		<table><tr><td>
		       		<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					
					</td><td>
						<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       		</div>
		       	
			    
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       		</div>
		       	
		       		
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       		</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></label>
		      		</div>
		       	</div>
		    </div>
		    
		    <%
			GeneralTable sfl = fw.getTable("s5019screensfl");
			%>
		
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='s5019Table'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
								S5019screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5019screensfl.hasMoreScreenRows(sfl)) {	
							%>

				<tr id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td class="tableDataTag tableDataTagFixed" style="width:100px;" 
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
								
											
													
															
										<%= sv.coverage.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td  >
									</td>														
														
									<%}%>
												<%if((new Byte((sv.rider).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:60px;" 
									<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
													
															
										<%= sv.rider.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.fund).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:60px;" 
									<%if((sv.fund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
													
															
										<%= sv.fund.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.fieldType).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:60px;" 
									<%if((sv.fieldType).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
													
															
										<%= sv.fieldType.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.shortds).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:100px;" 
									<%if((sv.shortds).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
													
															
										<%= sv.shortds.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.cnstcur).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:80px;" 
									<%if((sv.cnstcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
													
															
										<%= sv.cnstcur.getFormData()%>
										
																		 
								
													</td>
						<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.estMatValue).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:100px;" 
									<%if((sv.estMatValue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
																											
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());						
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									
														
										<%
											formatValue = smartHF.getPicFormatted(qpsf,sv.estMatValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
											if(!(sv.estMatValue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
												formatValue = formatValue( formatValue );
											}
										%>
										<%= formatValue%>
										<%
												longValue = null;
												formatValue = null;
										%>
									 			 		
							 		
							    				 
								
													</td>
						<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
												<%if((new Byte((sv.actvalue).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						    									<td class="tableDataTag" style="width:100px;" 
									<%if((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
												
															
																											
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());						
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									
														
										<%
											formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
											if(!(sv.actvalue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
												formatValue = formatValue( formatValue );
											}
										%>
										<%= formatValue%>
										<%
												longValue = null;
												formatValue = null;
										%>
									 			 		
							 		
							    				 
								
													</td>
							<%}else{%>
																<td  >
																		
								    </td>
														
									<%}%>
													
							</tr>
						
							<%
							count = count + 1;
							S5019screensfl.setNextScreenRow(sfl, appVars, sv);
							}
							%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity Details")%></label>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Policy loans")%>
					<%}%></label>
		       		<div class="input-group"  style="min-width: 80px;">
		       		<%if ((new Byte((sv.policyloan).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.policyloan,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       		</div>
		       	
			    
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Other Adjustments")%>
					<%}%>
		       		</label>
		       		<div class="input-group"  style="min-width: 80px;">
		       		<%if ((new Byte((sv.otheradjst).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


							<%	
									qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									valueThis=smartHF.getPicFormatted(qpsf,sv.otheradjst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							%>
						
						<input name='otheradjst' 
						type='text'
						
						<%if((sv.otheradjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
							value='<%=valueThis%>'
									 <%	 
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=valueThis%>'
							 <%}%>
						
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.otheradjst.getLength(), sv.otheradjst.getScale(),3)%>'
						maxLength='<%= sv.otheradjst.getLength()%>' 
						onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
						
						
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						
						<% 
							if((new Byte((sv.otheradjst).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.otheradjst).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.otheradjst).getColor()== null  ? 
									"input_cell" :  (sv.otheradjst).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>
		       		</div>
		       		</div>
		       		</div>
		       	
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Policy Debt")%>
					<%}%>
		       		</label>
		       		<div class="input-group"  style="min-width: 80px;">
		       		<%if ((new Byte((sv.tdbtamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.tdbtamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.tdbtamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       		</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
					<%}%></label>
		       		<div class="input-group" style="min-width: 180px;">
		       		<%	
						/* if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { */
						
						mappedItems = (Map) fieldItem.get("currcd");
						optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
					%>
					
					<%=smartHF.getDropDownExt(sv.currcd, fw, longValue, "currcd", optionValue) %>
					<!--ILIFE-5482 -->
					<%
						longValue = null;
						formatValue = null;
					%>
					<!--ILIFE-5482 -->
		       		</div>
		       		</div>
		       		</div>
		       	
			       	
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Estimated Total")%>
					<%}%></label>
		       		<div class="input-group" style="width: 180px;">
		       		<%if ((new Byte((sv.estimateTotalValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
				  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%> 
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       		</div>
		       	
		       		
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle(" Actual Total")%>
						<%}%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%if ((new Byte((sv.clamant).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%> 
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       		</div>
		    </div>
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Adjustment reason")%>
						<%}%></label>
		       		<div class="input-group">
		       		<table>
		       		<tr>
		       		<td style="min-width: 180px;">
		       		<%	
					mappedItems = (Map) fieldItem.get("reasoncd");
					optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
					longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
					%>
				
					<%=smartHF.getDropDownExt(sv.reasoncd, fw, longValue, "reasoncd", optionValue) %>
					</td>
					
					<td style="min-width: 140px;">
					<%if ((new Byte((sv.resndesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


					<input name='resndesc' 
					type='text' ;
					
					<%if((sv.resndesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
					<%
							
							mappedItems = (Map) fieldItem.get("reasoncd");
							optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
							formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
							
					
					%>
					
					<%if(formatValue==null) {
						formatValue="";
					}
					
					 %>
					  		
								<% String str=(sv.resndesc.getFormData()).toString().trim(); %>
								<% if(str.equals("") || str==null) {
									str=formatValue;
								}
								
							%>
					
						value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
					
					size='50'
					maxLength='50' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(resndesc)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.resndesc).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.resndesc).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.resndesc).getColor()== null  ? 
								"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					<%}%>
					</td>
					</tr>
		       		</table>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		    </div>
	 </div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

<script type="text/javascript">

function change()
{
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}
</script>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("--------------------------")%>
</div>
<%}%>

</td>
<td width='188'>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maturity")%>
</div>
<%}%>
<br/>

<%if ((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  </td>
</tr>
</table></div>