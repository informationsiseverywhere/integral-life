<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH5C2";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.text.NumberFormat"%>

<%
	Sh5c2ScreenVars sv = (Sh5c2ScreenVars) fw.getVariables();
%>
<%
	String[][] items = {{"refundOption"}, {}, {}};
	Map longDesc = appVars.getLongDesc(items, "E", baseModel.getCompany().toString().trim(), baseModel, sv);
%>
<%{
	if (appVars.ind03.isOn()) {
		sv.refundOption.setReverse(BaseScreenData.REVERSED);
		sv.refundOption.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.refundOption.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind05.isOn()) {
		sv.rsncde.setReverse(BaseScreenData.REVERSED);
		sv.rsncde.setColor(BaseScreenData.RED);
	}
	if (appVars.ind10.isOn()) {
		sv.rsncde.setEnabled(BaseScreenData.DISABLED);
	}
	if (!appVars.ind05.isOn()) {
		sv.rsncde.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind06.isOn()) {
		sv.resndesc.setReverse(BaseScreenData.REVERSED);
		sv.resndesc.setColor(BaseScreenData.RED);
	}
	if (appVars.ind12.isOn()) {
		sv.resndesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (!appVars.ind06.isOn()) {
		sv.resndesc.setHighLight(BaseScreenData.BOLD);
	}	
	if (appVars.ind07.isOn()) {
		sv.tPolicyReserve.setReverse(BaseScreenData.REVERSED);
		sv.tPolicyReserve.setColor(BaseScreenData.RED);
	}
	if (appVars.ind11.isOn()) {
		sv.tPolicyReserve.setEnabled(BaseScreenData.DISABLED);
	}
	if (!appVars.ind07.isOn()) {
		sv.tPolicyReserve.setHighLight(BaseScreenData.BOLD);
	}
	
}%>
<%
	appVars.rollup(new int[]{93});
%>
<div class="panel panel-default">
	<div class="panel-body">
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					</label>
					<table>
						<tr>
							<td>
								 <%
 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> 
							</td>
							<td style="padding-left: 1px;">
							<%
 	if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> 
							</td>
							<td>
								<%
 	if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div id="cntdesc" style="margin-left: 1px; max-width: 320px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%>
					</label>
					
					<table>
					<tr>
					<td>
				
						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

							</td>
							<td style="padding-left:1px;">
						

						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px;margin-left: 1px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-1"></div>			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Date")%></label>
					
					<div style="width: 80px;">
						
						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						
					</div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
	        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
					<div style="width: 80px;">
						
						<%
							if (!((sv.riskcommdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%>
					</label>
					
					<table>
					<tr>
					<td>
						<%
						System.out.println("owner--------------"+sv.cownnum);
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						
						</td>
						<td style="padding-left:1px;">
						
						<%
							
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:120px;max-width:195px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						
					</td>
					</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					<table>
						<tr>
							
							<td style="padding-left:1px;">
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:120px;max-width:160px;;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-1"></div>	
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Number")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>			
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
					<div style="width: 30px;">
						<%
							if (!((sv.claimTyp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div> 	
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage / Rider Details")%></label>
				</div>
			</div>
		</div>
		
		<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-sh52c" width="100%"
						style="border-right: thin solid #dddddd !important;">
                         	<thead>
                            	<tr class='info'>
                                	<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider") %></center></th>
                                    <th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Component ") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium Aready Paid ") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Cash Value") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Policy Reserve") %></center></th> 
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Value") %></center></th> 
                               </tr>
                            </thead>
                            <tbody>
                            	<%
                            	GeneralTable sfl = fw.getTable("sh5c2screensfl");
                            	Sh5c2screensfl.set1stScreenRow(sfl, appVars, sv);
                            	int count = 1;
                            	BigDecimal totalPrem=BigDecimal.ZERO;
                            	BigDecimal totalCash=BigDecimal.ZERO;
                            	BigDecimal totalPolReserve=BigDecimal.ZERO;
                            	BigDecimal totalFund=BigDecimal.ZERO;
                            	while (Sh5c2screensfl.hasMoreScreenRows(sfl)) {
                            	
                            	%>
								<tr>
						    		<td style="width:50px;"
									<%if ((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.coverage.getFormData()%>
									<% System.out.println("sv.coverage.getFormData()"+ sv.coverage.getFormData());%>
								</td>
								<td style="width:50px;"
									<%if ((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.rider.getFormData()%>
								</td>
				    			<td><b><%=sv.component.getFormData()%></b></td>
				    			<td><b><%=sv.compdesc.getFormData()%></b></td>
			    				<td  align="right" style="vertical-align:middle;padding: 3px !important;">		
			    				
			    												
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.instPrem).getFieldName());										
										formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.instPrem).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
								    	
									%>
								</td>
								
				    			<td  align="right" style="vertical-align:middle;padding: 3px !important;">									
									
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.cashvalare).getFieldName());										
										formatValue = smartHF.getPicFormatted(qpsf,sv.cashvalare,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.cashvalare).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
									
									%>
								</td>
								
								<td  align="right" style="vertical-align:middle;padding: 3px !important;">									
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.polReserve).getFieldName());						
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
										formatValue = smartHF.getPicFormatted(qpsf,sv.polReserve,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.polReserve).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
							
									%>
								</td>
								<td  align="right" style="vertical-align:middle;padding: 3px !important;">									
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.fundVal).getFieldName());										
										formatValue = smartHF.getPicFormatted(qpsf,sv.fundVal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.fundVal).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
									%>
								</td>
							
								</tr>								
								<%
								count = count + 1;
								Sh5c2screensfl.setNextScreenRow(sfl, appVars, sv);
								}
                            	
								%>								
                            </tbody>
                       </table>
                	</div>			
				</div>
			</div>	
			
		 <div class="row">
		 	 <div class="col-md-2">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
			       		<div class="input-group">
			       				<%					
								if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
			       		</div>
			       		</div>
			       	</div>
			       	
					
				
				<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Option")%></label>					
						<%
							mappedItems = (Map) longDesc.get("refundOption");
							optionValue = makeDropDownList(mappedItems, sv.refundOption.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.refundOption.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.refundOption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div class='output_cell' style="max-width: 190px;min-width: 190px;">
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<select name='refundOption' type='list' style="width: 190px;"
								<%if ((new Byte((sv.refundOption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.refundOption).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class = ' <%=(sv.refundOption).getColor()== null  ? 
									"input_cell" :  (sv.refundOption).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'  <%}%>
								onchange="change()">
								<!-- BRD-140 --> 
								<%=optionValue%>
							</select>
					
						<%
							}
						%>
					<% 
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>			
		 	
		 	<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium Paid")%></label>
					
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.totalPrem).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.totalPrem,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.totalPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>	       	
			
				<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Cash Value")%></label>
					
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.tCashval).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.tCashval,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.tCashval.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
			
				</div>
			</div>	       	
		 			
		 		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Policy Reserve")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.tPolicyReserve).getFieldName());
							valueThis = sv.tPolicyReserve.getFormDataRaw();
						%>

						<input name='tPolicyReserve' type='text'
							<%if ((sv.tPolicyReserve).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tPolicyReserve.getLength(), sv.tPolicyReserve.getScale(), 3)%>'
							maxLength='<%=sv.tPolicyReserve.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(tPolicyReserve)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.tPolicyReserve).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.tPolicyReserve).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.tPolicyReserve).getColor() == null
						? "input_cell"
						: (sv.tPolicyReserve).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>	       	
		 			
		 		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Fund Value")%></label>
					
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.tFundVal).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.tFundVal,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.tFundVal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
			
				</div>
			</div>	       		 			
		 
		 </div>
		 
		 <div class="row">
		 	<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Policy Loan")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.apl).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.apl,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.apl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Policy Loan Interest")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.aplInterest).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.aplInterest,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.aplInterest.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loan")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.policyloan,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loan Interest")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.policyloanInterest).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.policyloanInterest,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.policyloanInterest.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
		 </div>
		 
		 		 <div class="row">
		 <div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.susamt).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.susamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			 <div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Advance Premium")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.advPrm).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.advPrm,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.advPrm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Unexpired Premium")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.unexpiredprm).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.unexpiredprm,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.unexpiredprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			
		 
		 </div>
		 
		 
		 
		 <div class="row">
			 <div class="col-md-6">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Reason Code")%></label>
		       		
		       		
		       		<table><tr>
		       		<td>

		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"rsncde"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("rsncde");
							optionValue = makeDropDownList( mappedItems , sv.rsncde.getFormData(),1,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.rsncde.getFormData()).toString().trim());  
						%>
						
						<% 
							if((new Byte((sv.rsncde).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(sv.rsncde.getFormData().toString().trim())%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else{%>
							
						
						<select name='rsncde' type='list' style="width:140px; "
						<% 
							if((new Byte((sv.rsncde).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							disabled
							class="output_cell"
						<%
							}else if((new Byte((sv.rsncde).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						<%
							}else { 
						%>
							class = ' <%=(sv.rsncde).getColor()== null  ? 
					"input_cell" :  (sv.rsncde).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>'
						<%
							} 
						%>
						onchange="change()"
						>
						<%=optionValue%>
						</select>
						
						
						<%
						} 
						%>
						
						
								<%
								longValue = null;
								formatValue = null;
								%>
						
						</td>
						
						<td style="width: 500px;padding-left: 1px;">
						<input name='resndesc' 
						type='text'
						
						<%
						
								fieldItem=appVars.loadF4FieldsLong(new String[] {"rsncde"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("rsncde");
								optionValue = makeDropDownList( mappedItems , sv.rsncde.getFormData(),2,resourceBundleHandler);  
								formatValue = (String) mappedItems.get((sv.rsncde.getFormData()).toString().trim()); 
								
						%>
						<%if(formatValue==null) {
							formatValue="";
						}
						
						 %>
						 
						 <% String str=(sv.resndesc.getFormData()).toString().trim(); %>
									<% if(str.equals("") || str==null) {
										str=formatValue;
									}
									
								%>
						 
							value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
						
						size='50'
						maxLength='50' 
						
						
						<% 
							if((new Byte((sv.resndesc).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.resndesc).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.resndesc).getColor()== null  ? 
									"output_cell" :  (sv.resndesc).getColor().equals("red") ? 
									"output_cell red reverse" : "output_cell" %>'
						 
						<%
							} 
						%>
						>
						
						
								<%
								longValue = null;
								formatValue = null;
								%>

							</td>	
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	<div class="col-md-4"></div>
		 		
		 		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
					<div align="right" style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.total).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf, sv.total,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.total.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		 
		 </div>
		

			
	</div>
</div>
<script type="text/javascript">

function change()
{
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}

</script>


<%@ include file="/POLACommon2NEW.jsp"%>