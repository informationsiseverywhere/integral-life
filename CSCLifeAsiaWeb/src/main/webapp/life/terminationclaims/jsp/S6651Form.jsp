<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6651";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6651ScreenVars sv = (S6651ScreenVars) fw.getVariables();%>

<%if (sv.S6651screenWritten.gt(0)) {%>
	<%S6651screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                 Paid Up Fees                              ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq                          Flat                              %                           Minimum                                    Maximum");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"----------------------------------------------------------------------------------------------------------------------------------       ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"00 |");%>
	<%sv.puffamt02.setClassString("");%>
	<%sv.feepc02.setClassString("");%>
	<%sv.pufeemin02.setClassString("");%>
	<%sv.pufeemax02.setClassString("");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"01 |");%>
	<%sv.puffamt03.setClassString("");%>
	<%sv.feepc03.setClassString("");%>
	<%sv.pufeemin03.setClassString("");%>
	<%sv.pufeemax03.setClassString("");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"02 |");%>
	<%sv.puffamt04.setClassString("");%>
	<%sv.feepc04.setClassString("");%>
	<%sv.pufeemin04.setClassString("");%>
	<%sv.pufeemax04.setClassString("");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"04 |");%>
	<%sv.puffamt05.setClassString("");%>
	<%sv.feepc05.setClassString("");%>
	<%sv.pufeemin05.setClassString("");%>
	<%sv.pufeemax05.setClassString("");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"12 |");%>
	<%sv.puffamt06.setClassString("");%>
	<%sv.feepc06.setClassString("");%>
	<%sv.pufeemin06.setClassString("");%>
	<%sv.pufeemax06.setClassString("");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjust Initial Units (Y/N)");%>
	<%sv.adjustiu.setClassString("");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Use Bid or Offer Price (B/O)");%>
	<%sv.bidoffer.setClassString("");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allow override of Sum Assured (Y/N)");%>
	<%sv.ovrsuma.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind04.isOn()) {
			sv.puffamt02.setReverse(BaseScreenData.REVERSED);
			sv.puffamt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.puffamt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc02.setReverse(BaseScreenData.REVERSED);
			sv.feepc02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pufeemin02.setReverse(BaseScreenData.REVERSED);
			sv.pufeemin02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pufeemin02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pufeemax02.setReverse(BaseScreenData.REVERSED);
			sv.pufeemax02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pufeemax02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.puffamt03.setReverse(BaseScreenData.REVERSED);
			sv.puffamt03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.puffamt03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc03.setReverse(BaseScreenData.REVERSED);
			sv.feepc03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pufeemin03.setReverse(BaseScreenData.REVERSED);
			sv.pufeemin03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pufeemin03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pufeemax03.setReverse(BaseScreenData.REVERSED);
			sv.pufeemax03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pufeemax03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.puffamt04.setReverse(BaseScreenData.REVERSED);
			sv.puffamt04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.puffamt04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc04.setReverse(BaseScreenData.REVERSED);
			sv.feepc04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pufeemin04.setReverse(BaseScreenData.REVERSED);
			sv.pufeemin04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pufeemin04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pufeemax04.setReverse(BaseScreenData.REVERSED);
			sv.pufeemax04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pufeemax04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.puffamt05.setReverse(BaseScreenData.REVERSED);
			sv.puffamt05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.puffamt05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc05.setReverse(BaseScreenData.REVERSED);
			sv.feepc05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pufeemin05.setReverse(BaseScreenData.REVERSED);
			sv.pufeemin05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pufeemin05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pufeemax05.setReverse(BaseScreenData.REVERSED);
			sv.pufeemax05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pufeemax05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.puffamt06.setReverse(BaseScreenData.REVERSED);
			sv.puffamt06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.puffamt06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.feepc06.setReverse(BaseScreenData.REVERSED);
			sv.feepc06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.feepc06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pufeemin06.setReverse(BaseScreenData.REVERSED);
			sv.pufeemin06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pufeemin06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.pufeemax06.setReverse(BaseScreenData.REVERSED);
			sv.pufeemax06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.pufeemax06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.adjustiu.setReverse(BaseScreenData.REVERSED);
			sv.adjustiu.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.adjustiu.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.bidoffer.setReverse(BaseScreenData.REVERSED);
			sv.bidoffer.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.bidoffer.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.ovrsuma.setReverse(BaseScreenData.REVERSED);
			sv.ovrsuma.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.ovrsuma.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		   <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div>
				</div>
			
			
			
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				    </div>
				 </div>   	
				 
				
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		<td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
		</td>
		</tr>
		</table>
				</div>
			</div>
		</div>
		
		 <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div>
        </div>
        
        <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<center><label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paid Up Fees"))%></label></center>
        				
        		</div>
        	</div>	
        </div>
        
         <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Freq"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Flat"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("%"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum"))%></label>
                
                </div>
               </div>
            </div>    
            
             <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------------------------------------------------------------------------------------------------------------"))%></label>
                
                </div>
               </div>
               </div>	
               
               <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("00"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.puffamt02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group" style="max-width:75px;">
               <%=smartHF.getHTMLVar(fw, sv.feepc02, COBOLHTMLFormatter.S3VS2)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <%=smartHF.getHTMLVar(fw, sv.pufeemin02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.pufeemax02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
            </div>
            
            <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("01"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.puffamt03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group" style="max-width:75px;">
               <%=smartHF.getHTMLVar(fw, sv.feepc03, COBOLHTMLFormatter.S3VS2)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <%=smartHF.getHTMLVar(fw, sv.pufeemin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.pufeemax03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
            </div>
            
                        <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("02"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.puffamt04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group" style="max-width:75px;">
               <%=smartHF.getHTMLVar(fw, sv.feepc04, COBOLHTMLFormatter.S3VS2)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <%=smartHF.getHTMLVar(fw, sv.pufeemin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.pufeemax04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
            </div>
            
            <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("04"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.puffamt05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group" style="max-width:75px;">
               <%=smartHF.getHTMLVar(fw, sv.feepc05, COBOLHTMLFormatter.S3VS2)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <%=smartHF.getHTMLVar(fw, sv.pufeemin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.pufeemax05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
            </div>
            
            <div class="row">
              <div class="col-md-1">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("12"))%></label>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.puffamt06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-2">
                <div class="form-group" style="max-width:75px;">
               <%=smartHF.getHTMLVar(fw, sv.feepc06, COBOLHTMLFormatter.S3VS2)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
                <%=smartHF.getHTMLVar(fw, sv.pufeemin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
               
               <div class="col-md-3">
                <div class="form-group">
               <%=smartHF.getHTMLVar(fw, sv.pufeemax06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                
                </div>
               </div>
            </div>
            &nbsp;
             <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Adjust Initial Units (Y/N)"))%></label>
                <input type='checkbox' name='adjustiu' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(adjustiu)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.adjustiu).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.adjustiu).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.adjustiu).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('adjustiu')"/>

<input type='checkbox' name='adjustiu' value='N' 

<% if(!(sv.adjustiu).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('adjustiu')"/>
                </div>
               </div>
               
               <div class="col-md-5"></div>
               
               <div class="col-md-3">
                <div class="form-group">
              <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Use Bid or Offer Price (B/O)"))%></label>
                <%	
	if ((new Byte((sv.bidoffer).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Bid");
	}
	if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("O")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Offer");
	}
	 
%>

<%	
	if((new Byte((sv.bidoffer).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
   				
   			<%} %>
	   </div>

<%
longValue = null;
%>

	<% }else { %>
	
<% if("red".equals((sv.bidoffer).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='bidoffer' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(bidoffer)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.bidoffer).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.bidoffer).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="B"<% if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></option>
<option value="O"<% if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("O")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></option>
		
		
</select>
<% if("red".equals((sv.bidoffer).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>
                </div>
               </div>
               </div>
               
               <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Allow override of Sum Assured (Y/N)"))%></label>
                <input type='checkbox' name='ovrsuma' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(ovrsuma)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.ovrsuma).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.ovrsuma).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.ovrsuma).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('ovrsuma')"/>

<input type='checkbox' name='ovrsuma' value='N' 

<% if(!(sv.ovrsuma).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('ovrsuma')"/>
                
                </div>
               </div>
              </div>  
            
    </div>
    </div>        		


<%-- 	<div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<style>
@media \0screen\,screen\9
{
.output_cell{margin-right:2px}
}
</style>
<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


&nbsp;


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table></div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:174px; left:257px; width:472px;'>Paid Up Fees</div>


<div class='label_txt'class='label_txt' style='position: absolute; top:194px; left:24px; '>Freq</div>

<div class='label_txt'class='label_txt' style='position: absolute; top:194px; left:63px;'>Flat</div>
<div class='label_txt'class='label_txt' style='position: absolute; top:194px; left:257px; '>%</div>
<div class='label_txt'class='label_txt' style='position: absolute; top:194px; left:337px;'>Minimum</div>
<div class='label_txt'class='label_txt' style='position: absolute; top:194px; left:528px; '>Maximum</div>
<div class='label_txt'class='label_txt' style='position: absolute; top:204px; left:60px; '>---------------------------------------------------------------------------------------------------------------------------------------------------------------</div>

<!-- ILIFE-1528 START  by nnazeer -->

<div class='label_txt'class='label_txt' style='position: absolute; top:222px; left:25px; width:24px;text-align=center'>00</div>


	<%=smartHF.getHTMLVar(11, 8, fw, sv.puffamt02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 32, fw, sv.feepc02, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(11, 42, fw, sv.pufeemin02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 66, fw, sv.pufeemax02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>


<div class='label_txt'class='label_txt' style='position: absolute; top:242px; left:26px; width:24px;text-align=center'>01&nbsp;</div>
	<%=smartHF.getHTMLVar(12, 8, fw, sv.puffamt03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 32, fw, sv.feepc03, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(12, 42, fw, sv.pufeemin03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 66, fw, sv.pufeemax03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

<div class='label_txt'class='label_txt' style='position: absolute; top:262px; left:24px; width:24px;text-align=center'>02</div>

	<%=smartHF.getHTMLVar(13, 8, fw, sv.puffamt04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 32, fw, sv.feepc04, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(13, 42, fw, sv.pufeemin04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 66, fw, sv.pufeemax04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

<div class='label_txt'class='label_txt' style='position: absolute; top:283px; left:24px; width:24px;text-align=center'>04</div>

	<%=smartHF.getHTMLVar(14, 8, fw, sv.puffamt05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 32, fw, sv.feepc05, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(14, 42, fw, sv.pufeemin05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 66, fw, sv.pufeemax05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

<div class='label_txt'class='label_txt' style='position: absolute; top:302px; left:24px; width:24px;text-align=center'>12</div>

	<%=smartHF.getHTMLVar(15, 8, fw, sv.puffamt06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 32, fw, sv.feepc06, COBOLHTMLFormatter.S3VS2)%>

	<%=smartHF.getHTMLVar(15, 42, fw, sv.pufeemin06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 66, fw, sv.pufeemax06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%> --%>


<!-- ILIFE-1528 END -->

<%-- <div style='position:absolute;
	top:350px;
	left:22px;'>
	<table>


<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Adjust Initial Units (Y/N)")%>
</div>



<br/>



<input type='checkbox' name='adjustiu' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(adjustiu)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.adjustiu).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.adjustiu).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.adjustiu).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('adjustiu')"/>

<input type='checkbox' name='adjustiu' value='N' 

<% if(!(sv.adjustiu).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('adjustiu')"/>


</td>

<td width='251'></td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Use Bid or Offer Price (B/O)")%>
</div>



<br/>

<%	
	if ((new Byte((sv.bidoffer).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("B")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Bid");
	}
	if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("O")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Offer");
	}
	 
%>

<%	
	if((new Byte((sv.bidoffer).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
   				
   			<%} %>
	   </div>

<%
longValue = null;
%>

	<% }else { %>
	
<% if("red".equals((sv.bidoffer).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='bidoffer' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(bidoffer)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.bidoffer).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.bidoffer).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="B"<% if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("B")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Bid")%></option>
<option value="O"<% if(((sv.bidoffer.getFormData()).toString()).trim().equalsIgnoreCase("O")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Offer")%></option>
		
		
</select>
<% if("red".equals((sv.bidoffer).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>
</td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Allow override of Sum Assured (Y/N)")%>
</div>



<br/>






<input type='checkbox' name='ovrsuma' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(ovrsuma)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.ovrsuma).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.ovrsuma).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.ovrsuma).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('ovrsuma')"/>

<input type='checkbox' name='ovrsuma' value='N' 

<% if(!(sv.ovrsuma).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('ovrsuma')"/>





</td></tr></table></div>
 --%>



<% } %>

<%if (sv.S6651protectWritten.gt(0)) {%>
	<%S6651protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<style>
div[id*='puffamt']{padding-right:2px !important} 
div[id*='feepc']{padding-right:2px !important}
div[id*='pufeemin']{padding-right:2px !important}
div[id*='pufeemax']{padding-right:2px !important}
</style>  
<%@ include file="/POLACommon2NEW.jsp"%>