

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl55"; 
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sjl55ScreenVars sv = (Sjl55ScreenVars) fw.getVariables();
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment No"))%></label>
				<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.payNum, longValue)%></div>
				</div>
			</div>
			<div class="col-md-3">				
        			<div class="form-group">
        				<label>	<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Claim Number"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.claimNum)%>
						</td></tr></table>
					</div>
				</div>
				
				<div class="col-md-6">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Number"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.contractNum)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.cnttype)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.ctypedes)%>
						</td></tr></table>
					</div>
				</div>
		</div>

		<div class="row">
		
		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Date"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.payDateDisp, longValue)%></div>
				</div>
			</div>
			
			<div class="col-md-3 ">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Method"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.payMethod, longValue)%></div>
				</div>
			</div>
			
			<div class="col-md-3 ">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Payment Amount"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLVarExt(fw, sv.totalPayAmt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%></div>
				</div>
			</div>
			
			<div class="col-md-3">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Insured"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.clientnum)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.clntName)%>
						</td></tr></table>
					</div>
				</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Created By"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.createdUser, longValue)%></div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Modified By"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.modifiedUser, longValue)%></div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Approved By"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.approvedUser, longValue)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Authorized By"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.authorizedUser, longValue)%></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Created Date"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.createdDateDisp, longValue)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Modified Date"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.modifiedDateDisp, longValue)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
 					<label>  <%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Approved Date"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.approvedDateDisp, longValue)%></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Authorized Date"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.authorizedDateDisp, longValue)%></div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3 ">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Status"))%></label>
					<div class="input-group" style="min-width:71px;"><%=smartHF.getHTMLFieldEnquiry(sv.payStatus, longValue)%></div>
				</div>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				</div><tr><td><hr /></td></tr></div></div>
			<div class="row">
				<div class="col-md-12">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee"))%></label>
        			</div>
        		</div>
        	</div>
		<%
			GeneralTable sfl = fw.getTable("sjl55screensfl");
			GeneralTable sfl1 = fw.getTable("sjl55screensfl");
			Sjl55screensfl.set1stScreenRow(sfl, appVars, sv);
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else if (sfl.count() * 27 > 118) {
				height = sfl.count() * 27;
			} else {
				height = 118;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				
					<div >
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='sjl55Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 45px"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
									<th style="text-align: center; width: 105px"><%=resourceBundleHandler.gettingValueFromBundle("Client No.")%></th>
									<th style="text-align: center; width: 280px"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th style="text-align: center; width: 300px"><%=resourceBundleHandler.gettingValueFromBundle("Bank")%></th>
									<th style="text-align: center; width: 200px"><%=resourceBundleHandler.gettingValueFromBundle("Account type")%></th>
									<th style="text-align: center; width: 160px"><%=resourceBundleHandler.gettingValueFromBundle("Bank Account #")%></th>
									<th style="text-align: center; width: 280px"><%=resourceBundleHandler.gettingValueFromBundle("Account Name")%></th>
									<th style="text-align: center; width: 85px"><%=resourceBundleHandler.gettingValueFromBundle("Receiving %")%></th>
									<th style="text-align: center; width: 137px"><%=resourceBundleHandler.gettingValueFromBundle("Payment Amount")%></th>
								</tr>
							</thead>
							<tbody>
							<%
								Sjl55screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (Sjl55screensfl.hasMoreScreenRows(sfl)) {
								hyperLinkFlag = true;
								
								if (appVars.ind19.isOn()) {
									sv.prcent.setReverse(BaseScreenData.REVERSED);
									sv.prcent.setColor(BaseScreenData.RED);
								}
								if (appVars.ind21.isOn()) {
									sv.prcent.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind20.isOn()) {
									sv.pymt.setReverse(BaseScreenData.REVERSED);
									sv.pymt.setColor(BaseScreenData.RED);
								}
								if (appVars.ind22.isOn()) {
									sv.pymt.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind13.isOn()) {
									sv.clttwo.setReverse(BaseScreenData.REVERSED);
									sv.clttwo.setColor(BaseScreenData.RED);
								}
								if (appVars.ind23.isOn()) {
									sv.clttwo.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind17.isOn()) {
									sv.bankacckey.setReverse(BaseScreenData.REVERSED);
									sv.bankacckey.setColor(BaseScreenData.RED);
								}
								if (appVars.ind27.isOn()) {
									sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
								}
							%> 	
							<tr id='<%="tablerow" + count%>'>
								<td>
										<%if((new Byte((sv.seqenum).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.seqenum.getFormData();
												%>
										<div id="sjl55screensfl.seqenum_R<%=count%>"
											name="sjl55screensfl.seqenum_R<%=count%>" align="center">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									
									<td
				style="color: #434343;position: relative;   border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left">
				<div class="input-group">
				<input name='<%="sjl55screensfl.clttwo" + "_R" +count %>' id='<%="sjl55screensfl.clttwo" + "_R"+count %>' type='text'
				value='<%=sv.clttwo.getFormData()%>' style="width: 95px !important;"
				maxLength='<%=sv.clttwo.getLength()%>'
				size='<%=sv.clttwo.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(sjl55screensfl.clttwo)'
				onKeyUp='return checkMaxLength(this)'
		 		<%if ((new Byte((sv.clttwo).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0) {%>
				readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.clttwo).getHighLight()))
 				.compareTo(new Byte(BaseScreenData.BOLD)) == 0) { %>
 class="bold_cell" > 
				                                    
                   
                
			<%
 	} else {
 %> class='<%=(sv.clttwo).getColor() == null ? "input_cell"
									: (sv.clttwo).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >
				                                     
                </div>
				
				<%
 	}
 %></td>
									
									<td>
										<%if((new Byte((sv.clntID).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.clntID.getFormData();
												%>
										<div id="sjl55screensfl.clntID_R<%=count%>"
											name="sjl55screensfl.clntID_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
										<%if((new Byte((sv.babrdc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.babrdc.getFormData();
												%>
										<div id="sjl55screensfl.babrdc_R<%=count%>"
											name="sjl55screensfl.babrdc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.babrdc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
										<%if((new Byte((sv.accdesc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.accdesc.getFormData();
												%>
										<div id="sjl55screensfl.accdesc_R<%=count%>"
											name="sjl55screensfl.accdesc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.accdesc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.bankacckey.getFormData();
										%>
										<div id="sjl55screensfl.bankacckey_R<%=count%>"
											name="sjl55screensfl.bankacckey_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>
											<%
												}
											%>
										</div> <%
									 	longValue = null;
									 %> <%
									 	} else {
									 %>

										<div class="input-group" style="width: 150px;">
											<input
												name='<%="sjl55screensfl" + "." + "bankacckey" + "_R" + count%>'
												id='<%="sjl55screensfl" + "." + "bankacckey" + "_R" + count%>'
												type='text' value='<%=sv.bankacckey.getFormData()%>'
												class=" <%=(sv.bankacckey).getColor() == null
								? "input_cell"
								: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.bankacckey.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sjl55screensfl.bankacckey)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.bankacckey.getLength() * 12%> px;">

										</div> <%
										 	}
										 %> <%
										 	}
										 %></div>
									</td>
									<td>
										<%if((new Byte((sv.bankaccdsc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.bankaccdsc.getFormData();
												%>
										<div id="sjl55screensfl.bankaccdsc_R<%=count%>"
											name="sjl55screensfl.bankaccdsc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.bankaccdsc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									
									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.prcent).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.prcent).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.prcent);
										%> <%
 	if ((new Byte((sv.prcent).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="sjl55screensfl.prcent_R<%=count%>"
											name="sjl55screensfl.prcent_R<%=count%>"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
											<%
												if (formatValue != null) {
											%>

											<%=formatValue%>


											<%
												}
											%>
										</div> <%
 	}  }
										%>								
 </div>
									</td>
									
									<td>
									<div class="form-group" style="text-align: right;">
										<%
											if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.pymt).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.pymt);
										%> <%
 	if ((new Byte((sv.pymt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="sjl55screensfl.pymt_R<%=count%>"
											name="sjl55screensfl.pymt_R<%=count%>"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
											<%
												if (formatValue != null) {
											%>

											<%=formatValue%>


											<%
												}
											%>
										</div> 
										<%
												}}
											%>
 </div>
									</td>
								</tr>

								<%
									count = count + 1;
								Sjl55screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
				</div>
				</div>
				
	

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<script>
$(document).ready(function() {
	$('#dataTables-sjl55').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
paging:   true,
        scrollCollapse: true,
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
