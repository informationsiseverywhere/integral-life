
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5238";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>

<%{
	appVars.rollup();
	appVars.rolldown();
%>
<%S5238ScreenVars sv = (S5238ScreenVars) fw.getVariables();%>


<%if (sv.S5238screenWritten.gt(0)) {%>
	<%S5238screen.clearClassString(sv);%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number         ");%>
	
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No     ");%>
	
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life No   ");%>
	
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reverse Trans No        ");%>
	
<%	generatedText26.appendClassString("label_txt");
	generatedText26.appendClassString("highlight");
	generatedText26.appendClassString("reverse_txt");
	generatedText26.appendClassString("highlight");
%>
	<%sv.trandes.setClassString("");%>
<%	sv.trandes.appendClassString("string_fld");
	sv.trandes.appendClassString("output_txt");
	sv.trandes.appendClassString("highlight");
	sv.trandes.appendClassString("reverse_txt");
	sv.trandes.appendClassString("highlight");
%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Press Enter to Confirm");%>
	
<%	generatedText25.appendClassString("label_txt");
	generatedText25.appendClassString("highlight");
%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life    ");%>
	
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex                     ");%>
	
	<%sv.sex.setClassString("");%>
<%	sv.sex.appendClassString("string_fld");
	sv.sex.appendClassString("output_txt");
	sv.sex.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>
	
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Birth");%>
	
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%sv.dobDisp.setClassString("");%>
<%	sv.dobDisp.appendClassString("string_fld");
	sv.dobDisp.appendClassString("output_txt");
	sv.dobDisp.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"of");%>
	
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Death");%>
	
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	
	<%sv.dtofdeathDisp.setClassString("");%>
<%	sv.dtofdeathDisp.appendClassString("string_fld");
	sv.dtofdeathDisp.appendClassString("output_txt");
	sv.dtofdeathDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason for Death        ");%>
	
	<%sv.reasoncd.setClassString("");%>
<%	sv.reasoncd.appendClassString("string_fld");
	sv.reasoncd.appendClassString("output_txt");
	sv.reasoncd.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason Description      ");%>
	
	<%sv.resndesc.setClassString("");%>
<%	sv.resndesc.appendClassString("string_fld");
	sv.resndesc.appendClassString("output_txt");
	sv.resndesc.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind04.isOn()) {
			sv.trandes.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>








<div class="panel panel-default">
<div class="panel-body">    

 
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>


<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  </div></div>
  







<div class="col-md-4"> 
			    	     <div class="form-group" >
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
                        <div style="width:80px">
                        
                        	<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
	</div></div></div>
	
	
	
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-md-4"> 
			    	     <div class="form-group" >
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life No")%></label>
<div style="width:80px">

	<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  </div></div></div>
  
  
  </div>









 <div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group"  >
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>

<table>
<tr>
<td>

<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
	
<td>
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 200;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  
</td>
</tr>
</table>
  </div></div>
  
  
  
  </div>





<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group" style="width:60px">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>



		<%					
		if(!((sv.sex.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.sex.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  </div></div>


<div class="col-md-4"> </div>

<div class="col-md-4"> 
			    	     <div class="form-group" style="width:85px">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Date of Birth")%></label>

<%					
		if(!((sv.dobDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dobDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div>
		
		
		
		</div>






<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group" style="width:80px">
                        <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%></label>

		<%					
		if(!((sv.dtofdeathDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div>
		
		





<div class="col-md-4"></div>






<div class="col-md-4"> 
			    	     <div class="form-group" style="width:180px">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Reason for Death")%></label>


	
		<%					
		if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  
  
  </div></div>
  
  
  
  </div>






<div class="row">	
			    	<div class="col-md-5"> 
			    	     <div class="form-group" >
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Reason Description")%></label>



<%					
		if(!((sv.resndesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div>
		
		
		</div>
		
		
		
		
		
		
		
		<div class="row">
		
		<div style='visibility:hidden;'> 
 <div class="input-group">
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Reverse Trans No")%>
</div>

 
  		
		<%					
		if(!((sv.trandes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	 
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Press Enter to Confirm")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Birth")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("of")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Death")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>

 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>


 

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>

 </div>
		</div>
		</div>
		
		
		
		</div></div>


  
<%}} %>
<%@ include file="/POLACommon2NEW.jsp"%>

