<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR687";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr687ScreenVars sv = (Sr687ScreenVars) fw.getVariables();%>

<%if (sv.Sr687screenWritten.gt(0)) {%>
	<%Sr687screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------Per-Claim------------------------");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amt/Life Time");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amt/Annual");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max Ben");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deductibles");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CoPay%");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1");%>
	<%sv.hosben01.setClassString("");%>
	<%sv.amtlife01.setClassString("");%>
<%	sv.amtlife01.appendClassString("num_fld");
	sv.amtlife01.appendClassString("input_txt");
	sv.amtlife01.appendClassString("highlight");
%>
	<%sv.amtyear01.setClassString("");%>
<%	sv.amtyear01.appendClassString("num_fld");
	sv.amtyear01.appendClassString("input_txt");
	sv.amtyear01.appendClassString("highlight");
%>
	<%sv.benfamt01.setClassString("");%>
<%	sv.benfamt01.appendClassString("num_fld");
	sv.benfamt01.appendClassString("input_txt");
	sv.benfamt01.appendClassString("highlight");
%>
	<%sv.nofday01.setClassString("");%>
<%	sv.nofday01.appendClassString("num_fld");
	sv.nofday01.appendClassString("input_txt");
	sv.nofday01.appendClassString("highlight");
%>
	<%sv.gdeduct01.setClassString("");%>
<%	sv.gdeduct01.appendClassString("num_fld");
	sv.gdeduct01.appendClassString("input_txt");
	sv.gdeduct01.appendClassString("highlight");
%>
	<%sv.copay01.setClassString("");%>
<%	sv.copay01.appendClassString("num_fld");
	sv.copay01.appendClassString("input_txt");
	sv.copay01.appendClassString("highlight");
%>
	<%sv.benefits01.setClassString("");%>
<%	sv.benefits01.appendClassString("string_fld");
	sv.benefits01.appendClassString("output_txt");
	sv.benefits01.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2");%>
	<%sv.hosben02.setClassString("");%>
	<%sv.amtlife02.setClassString("");%>
<%	sv.amtlife02.appendClassString("num_fld");
	sv.amtlife02.appendClassString("input_txt");
	sv.amtlife02.appendClassString("highlight");
%>
	<%sv.amtyear02.setClassString("");%>
<%	sv.amtyear02.appendClassString("num_fld");
	sv.amtyear02.appendClassString("input_txt");
	sv.amtyear02.appendClassString("highlight");
%>
	<%sv.benfamt02.setClassString("");%>
<%	sv.benfamt02.appendClassString("num_fld");
	sv.benfamt02.appendClassString("input_txt");
	sv.benfamt02.appendClassString("highlight");
%>
	<%sv.nofday02.setClassString("");%>
<%	sv.nofday02.appendClassString("num_fld");
	sv.nofday02.appendClassString("input_txt");
	sv.nofday02.appendClassString("highlight");
%>
	<%sv.gdeduct02.setClassString("");%>
<%	sv.gdeduct02.appendClassString("num_fld");
	sv.gdeduct02.appendClassString("input_txt");
	sv.gdeduct02.appendClassString("highlight");
%>
	<%sv.copay02.setClassString("");%>
<%	sv.copay02.appendClassString("num_fld");
	sv.copay02.appendClassString("input_txt");
	sv.copay02.appendClassString("highlight");
%>
	<%sv.benefits02.setClassString("");%>
<%	sv.benefits02.appendClassString("string_fld");
	sv.benefits02.appendClassString("output_txt");
	sv.benefits02.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3");%>
	<%sv.hosben03.setClassString("");%>
	<%sv.amtlife03.setClassString("");%>
<%	sv.amtlife03.appendClassString("num_fld");
	sv.amtlife03.appendClassString("input_txt");
	sv.amtlife03.appendClassString("highlight");
%>
	<%sv.amtyear03.setClassString("");%>
<%	sv.amtyear03.appendClassString("num_fld");
	sv.amtyear03.appendClassString("input_txt");
	sv.amtyear03.appendClassString("highlight");
%>
	<%sv.benfamt03.setClassString("");%>
<%	sv.benfamt03.appendClassString("num_fld");
	sv.benfamt03.appendClassString("input_txt");
	sv.benfamt03.appendClassString("highlight");
%>
	<%sv.nofday03.setClassString("");%>
<%	sv.nofday03.appendClassString("num_fld");
	sv.nofday03.appendClassString("input_txt");
	sv.nofday03.appendClassString("highlight");
%>
	<%sv.gdeduct03.setClassString("");%>
<%	sv.gdeduct03.appendClassString("num_fld");
	sv.gdeduct03.appendClassString("input_txt");
	sv.gdeduct03.appendClassString("highlight");
%>
	<%sv.copay03.setClassString("");%>
<%	sv.copay03.appendClassString("num_fld");
	sv.copay03.appendClassString("input_txt");
	sv.copay03.appendClassString("highlight");
%>
	<%sv.benefits03.setClassString("");%>
<%	sv.benefits03.appendClassString("string_fld");
	sv.benefits03.appendClassString("output_txt");
	sv.benefits03.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4");%>
	<%sv.hosben04.setClassString("");%>
	<%sv.amtlife04.setClassString("");%>
<%	sv.amtlife04.appendClassString("num_fld");
	sv.amtlife04.appendClassString("input_txt");
	sv.amtlife04.appendClassString("highlight");
%>
	<%sv.amtyear04.setClassString("");%>
<%	sv.amtyear04.appendClassString("num_fld");
	sv.amtyear04.appendClassString("input_txt");
	sv.amtyear04.appendClassString("highlight");
%>
	<%sv.benfamt04.setClassString("");%>
<%	sv.benfamt04.appendClassString("num_fld");
	sv.benfamt04.appendClassString("input_txt");
	sv.benfamt04.appendClassString("highlight");
%>
	<%sv.nofday04.setClassString("");%>
<%	sv.nofday04.appendClassString("num_fld");
	sv.nofday04.appendClassString("input_txt");
	sv.nofday04.appendClassString("highlight");
%>
	<%sv.gdeduct04.setClassString("");%>
<%	sv.gdeduct04.appendClassString("num_fld");
	sv.gdeduct04.appendClassString("input_txt");
	sv.gdeduct04.appendClassString("highlight");
%>
	<%sv.copay04.setClassString("");%>
<%	sv.copay04.appendClassString("num_fld");
	sv.copay04.appendClassString("input_txt");
	sv.copay04.appendClassString("highlight");
%>
	<%sv.benefits04.setClassString("");%>
<%	sv.benefits04.appendClassString("string_fld");
	sv.benefits04.appendClassString("output_txt");
	sv.benefits04.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"5");%>
	<%sv.hosben05.setClassString("");%>
	<%sv.amtlife05.setClassString("");%>
<%	sv.amtlife05.appendClassString("num_fld");
	sv.amtlife05.appendClassString("input_txt");
	sv.amtlife05.appendClassString("highlight");
%>
	<%sv.amtyear05.setClassString("");%>
<%	sv.amtyear05.appendClassString("num_fld");
	sv.amtyear05.appendClassString("input_txt");
	sv.amtyear05.appendClassString("highlight");
%>
	<%sv.benfamt05.setClassString("");%>
<%	sv.benfamt05.appendClassString("num_fld");
	sv.benfamt05.appendClassString("input_txt");
	sv.benfamt05.appendClassString("highlight");
%>
	<%sv.nofday05.setClassString("");%>
<%	sv.nofday05.appendClassString("num_fld");
	sv.nofday05.appendClassString("input_txt");
	sv.nofday05.appendClassString("highlight");
%>
	<%sv.gdeduct05.setClassString("");%>
<%	sv.gdeduct05.appendClassString("num_fld");
	sv.gdeduct05.appendClassString("input_txt");
	sv.gdeduct05.appendClassString("highlight");
%>
	<%sv.copay05.setClassString("");%>
<%	sv.copay05.appendClassString("num_fld");
	sv.copay05.appendClassString("input_txt");
	sv.copay05.appendClassString("highlight");
%>
	<%sv.benefits05.setClassString("");%>
<%	sv.benefits05.appendClassString("string_fld");
	sv.benefits05.appendClassString("output_txt");
	sv.benefits05.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"6");%>
	<%sv.hosben06.setClassString("");%>
	<%sv.amtlife06.setClassString("");%>
<%	sv.amtlife06.appendClassString("num_fld");
	sv.amtlife06.appendClassString("input_txt");
	sv.amtlife06.appendClassString("highlight");
%>
	<%sv.amtyear06.setClassString("");%>
<%	sv.amtyear06.appendClassString("num_fld");
	sv.amtyear06.appendClassString("input_txt");
	sv.amtyear06.appendClassString("highlight");
%>
	<%sv.benfamt06.setClassString("");%>
<%	sv.benfamt06.appendClassString("num_fld");
	sv.benfamt06.appendClassString("input_txt");
	sv.benfamt06.appendClassString("highlight");
%>
	<%sv.nofday06.setClassString("");%>
<%	sv.nofday06.appendClassString("num_fld");
	sv.nofday06.appendClassString("input_txt");
	sv.nofday06.appendClassString("highlight");
%>
	<%sv.gdeduct06.setClassString("");%>
<%	sv.gdeduct06.appendClassString("num_fld");
	sv.gdeduct06.appendClassString("input_txt");
	sv.gdeduct06.appendClassString("highlight");
%>
	<%sv.copay06.setClassString("");%>
<%	sv.copay06.appendClassString("num_fld");
	sv.copay06.appendClassString("input_txt");
	sv.copay06.appendClassString("highlight");
%>
	<%sv.benefits06.setClassString("");%>
<%	sv.benefits06.appendClassString("string_fld");
	sv.benefits06.appendClassString("output_txt");
	sv.benefits06.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Aggregated Deductible ");%>
	<%sv.aad.setClassString("");%>
<%	sv.aad.appendClassString("num_fld");
	sv.aad.appendClassString("input_txt");
	sv.aad.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Min Sum Assured");%>
	<%sv.zssi.setClassString("");%>
<%	sv.zssi.appendClassString("num_fld");
	sv.zssi.appendClassString("input_txt");
	sv.zssi.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium & Coverage Timing Factor");%>
	<%sv.premunit.setClassString("");%>
<%	sv.premunit.appendClassString("num_fld");
	sv.premunit.appendClassString("input_txt");
	sv.premunit.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Coverage Timing Factor ");%>
	<%sv.factor.setClassString("");%>
<%	sv.factor.appendClassString("num_fld");
	sv.factor.appendClassString("input_txt");
	sv.factor.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item ");%>
	<%sv.contitem.setClassString("");%>
<%	sv.contitem.appendClassString("string_fld");
	sv.contitem.appendClassString("input_txt");
	sv.contitem.appendClassString("highlight");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind11.isOn()) {
			sv.hosben01.setReverse(BaseScreenData.REVERSED);
			sv.hosben01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.hosben01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.hosben02.setReverse(BaseScreenData.REVERSED);
			sv.hosben02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.hosben02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.hosben03.setReverse(BaseScreenData.REVERSED);
			sv.hosben03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.hosben03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.hosben04.setReverse(BaseScreenData.REVERSED);
			sv.hosben04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.hosben04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.hosben05.setReverse(BaseScreenData.REVERSED);
			sv.hosben05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.hosben05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.hosben06.setReverse(BaseScreenData.REVERSED);
			sv.hosben06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.hosben06.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">        	
   <div class="panel-body">
   
	<div class="row">        
		<div class="col-md-4">	
		<div class="form-group" style="max-width:50px;">				
		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
	</div>

      
	<div class="col-md-4">
	<div class="form-group" >
	<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</div>
	</div>

		<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
		<table>
		<tr>
		<td>		
				<%					
				if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.item.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.item.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  
			</td>
			
			<td>		
		  		
				<%					
				if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="max-width: 150px;margin-left: 1px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
		  
		</td>
		</tr>
		</table>
		</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%></label>
             <table>
				<tr>
		 		 <td>
					<%					
					if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width:80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>

				</td>
				
				<td style="padding-left:10px;padding-right:10px">
				<%=resourceBundleHandler.gettingValueFromBundle("to")%>
				</td>	

				<td>
		        <%					
				if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width:80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
	         </td>
           </tr>
		</table>
       </div>
	</div>
  </div>	
<br>
<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"></div>
<div class="col-md-1" style="text-align: center;">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit")%></label>
	</div>
<div class="col-md-3"></div>
	<div class="col-md-5">
	<label><%=resourceBundleHandler.gettingValueFromBundle("-----------------------------Per-Claim-----------------------------")%>"</label>
	</div>
</div>
<div class="row">
<div class="col-md-1" style="max-width:100px;"></div>
<div class="col-md-1">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Code")%></label>
</div>
<div class="col-md-1" style="min-width:160px;"></div>
<div class="col-md-1" style="min-width:100px;">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Amt/Life")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Time")%></label>
</div>

<div class="col-md-1" style="min-width:100px;padding-left:25px;">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Amt/Annual")%></label>
</div>
<div class="col-md-1" style="min-width:100px;">
	<label><%=resourceBundleHandler.gettingValueFromBundle("Max")%>&nbsp;<%=resourceBundleHandler.gettingValueFromBundle("Benefit")%></label>
</div>

<div class="col-md-1">
	<label><%=resourceBundleHandler.gettingValueFromBundle("No")%></label>
</div>
<div class="col-md-1">
	<label style="margin-left: -15px;"><%=resourceBundleHandler.gettingValueFromBundle("Deductibles")%></label>
</div>
<div class="col-md-1"></div>
<div class="col-md-1">
	<label style="margin-left:6px;"><%=resourceBundleHandler.gettingValueFromBundle("CoPay%")%></label>
</div>
</div>

<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
				<div class="form-group">
						<b>1</b>			 
		 		</div>
		 		</div> 
<div class="col-md-1" style="min-width:85px;text-align: center;">
<div class="form-group">
	<div class="input-group">
	<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben01)%></div>
	<%=smartHF.getHTMLF4NSVar(fw, sv.hosben01)%>
	<% if(!((sv.benefits01.getFormData()).toString()).trim().equalsIgnoreCase("")) { %> 
	
	<%					
		if(!((sv.benefits01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
	<%-- <%=smartHF.getHTMLVar(8.9, 8.5, fw, sv.benefits01, 141)%>  --%>
	<% } %>
	</div>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-1" style="min-width:85px;text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.amtlife01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div class="col-md-1" style="min-width:85px;text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.amtyear01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
<div class="col-md-1" style="min-width:85px;text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.benfamt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
<div class="col-md-1" style="min-width:85px;text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.nofday01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
<div class="col-md-1" style="text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct01, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
<div class="col-md-1"></div>
<div class="col-md-1" style="min-width:85px;text-align: center;">
	<%=smartHF.getHTMLVar(fw, sv.copay01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>	
</div>
<%-- 	<% if(!((sv.benefits01.getFormData()).toString()).trim().equalsIgnoreCase("")) { %> <%=smartHF.getHTMLVar(fw, sv.benefits01)%> <% } %> --%>

	
<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
<div class="form-group">
						<b>2</b>			 
		 		</div>
		 		</div> 
<div  class="col-md-1" style="min-width:85px;text-align: center;">
<div class="form-group">
	<div class="input-group">
		<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben02)%></div>
		<%=smartHF.getHTMLF4NSVar(fw, sv.hosben02)%>
		
	
		<%					
		if(!((sv.benefits02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			
	</div>
</div>
</div>
<div class="col-md-2"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtlife02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtyear02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.benfamt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.nofday02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct02, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.copay02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
</div>



<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
<div class="form-group">
						<b>3</b>			 
		 		</div>
		 		</div> 
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
		<div class="input-group">
		<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben03)%></div>
		<%=smartHF.getHTMLF4NSVar(fw, sv.hosben03)%>
		<% if(!((sv.benefits03.getFormData()).toString()).trim().equalsIgnoreCase("")) { %>
		
		
		<%					
		if(!((sv.benefits03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%--  <%=smartHF.getHTMLVar(13.5, 8.5, fw, sv.benefits03, 141)%>  --%>
		 
		 
		 <% } %>
		</div>
	</div>
</div>
<div class="col-md-2"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtlife03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtyear03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.benfamt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.nofday03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct03, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.copay03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
</div>	
	

<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
<div class="form-group">
						<b>4</b>			 
		 		</div>
		 		</div> 
<div  class="col-md-1" style="min-width:85px;text-align: center;">
<div class="form-group">
	<div class="input-group">
	<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben04)%></div>
	<%=smartHF.getHTMLF4NSVar(fw, sv.hosben04)%>
	<% if(!((sv.benefits04.getFormData()).toString()).trim().equalsIgnoreCase("")) { %>
	
	<%					
		if(!((sv.benefits04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
	<%--  <%=smartHF.getHTMLVar(fw, sv.benefits04)%>  --%>
	 
	 
	 <% } %>
	</div>
</div>
</div>
<div class="col-md-2"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtlife04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtyear04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.benfamt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.nofday04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct04, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.copay04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>

</div>	
	


<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
<div class="form-group">
						<b>5</b>			 
		 		</div>
		 		</div> 
<div  class="col-md-1" style="min-width:85px;text-align: center;">
<div class="form-group">
	<div class="input-group">
	<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben05)%></div>
	<%=smartHF.getHTMLF4NSVar(fw, sv.hosben05)%>
	<% if(!((sv.benefits05.getFormData()).toString()).trim().equalsIgnoreCase("")) { %>
	
	
	<%					
		if(!((sv.benefits05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
	<%--  <%=smartHF.getHTMLVar(15.8, 8.5, fw, sv.benefits05, 141)%>  --%>
	 
	 
	 <% } %>
	</div>
</div>
</div>
<div class="col-md-2"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtlife05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtyear05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.benfamt05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.nofday05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct05, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.copay05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div></div>	
	

<div class="row">
<div class="col-md-1" style="max-width:110px;text-align: center;"> 
<div class="form-group">
						<b>6</b>			 
		 		</div>
		 		</div> 
<div  class="col-md-1" style="min-width:85px;text-align: center;">
<div class="form-group">
	<div class="input-group">
	<div style="width: 50px !important;"><%=smartHF.getHTMLSpaceVar(fw, sv.hosben06)%></div>
	<%=smartHF.getHTMLF4NSVar(fw, sv.hosben06)%>
	<% if(!((sv.benefits06.getFormData()).toString()).trim().equalsIgnoreCase("")) { %> 
	
	<%-- <%=smartHF.getHTMLVar(15.8, 8.5, fw, sv.benefits06, 141)%>  --%>
	<%					
		if(!((sv.benefits06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style=" max-width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
	
	<% } %>
	</div>
</div>
</div>
<div class="col-md-2"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtlife06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1" style="max-width:20px;"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.amtyear06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.benfamt06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.nofday06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div  class="col-md-1" style="text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.gdeduct06, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>
<div class="col-md-1"></div>
<div  class="col-md-1" style="min-width:85px;text-align: center;">
	<div class="form-group">
	<%=smartHF.getHTMLVar(fw, sv.copay06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	</div>
</div>

</div>
	
	<!-- ILIFE-498 END -- SR687 has UI issues -->
	<!-- ILIFE-2679 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 5 ends  -->
<!--  ILIFE-2087 -->
	


<div class="row">
<div class="col-md-4">
<div class="form-group">
<label style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Annual Aggregated Deductible")%>
</label>


<div class="form-group" style="max-width:100px;">
	<%	
			qpsf = fw.getFieldXMLDef((sv.aad).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.aad,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='aad' 
type='text'

<%if((sv.aad).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.aad.getLength(), sv.aad.getScale(),3)%>'
maxLength='<%= sv.aad.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(aad)' onKeyUp='return checkMaxLength(this)'  

		onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
		decimal='<%=qpsf.getDecimals()%>' 
		onPaste='return doPasteNumber(event,true);'
		onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.aad).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.aad).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.aad).getColor()== null  ? 
			"input_cell" :  (sv.aad).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="col-md-4"></div>
<div class="col-md-4">
<div class="form-group">

<label style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Min Sum Assured")%>
</label>

<div class="form-group" style="width:150px;">

<input name='zssi' 
type='text'

<%if((sv.zssi).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%
		qpsf = fw.getFieldXMLDef((sv.zssi).getFieldName());
		formatValue = smartHF.getPicFormatted(qpsf,sv.zssi,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
;
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zssi.getLength(), sv.zssi.getScale(),3)%>'
maxLength='<%= sv.zssi.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zssi)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
		decimal='<%=qpsf.getDecimals()%>' 
		onPaste='return doPasteNumber(event,true);'
		onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.zssi).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zssi).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zssi).getColor()== null  ? 
			"input_cell" :  (sv.zssi).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

</div>


<!-- ILIFE-2679 by pmujavadiya -->

<div class="row">
<!-- ILIFE-2679 by pmujavadiya -->
<div class="col-md-4">
<label style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Premium & Coverage Timing Factor")%>
</label>



<div class="form-group" style="width:100px;">

<input name='premunit' 
type='text'

<%if((sv.premunit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.premunit.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.premunit.getLength()%>'
maxLength='<%= sv.premunit.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premunit)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.premunit).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premunit).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premunit).getColor()== null  ? 
			"input_cell" :  (sv.premunit).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>



<div class="col-md-4"></div>


<div class="col-md-4">

<label style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Life Coverage Timing Factor")%>
</label>


<div class="form-group" style="width:100px;">

<input name='factor' 
type='text'

<%if((sv.factor).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.factor.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.factor.getLength()%>'
maxLength='<%= sv.factor.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(factor)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.factor).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.factor).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.factor).getColor()== null  ? 
			"input_cell" :  (sv.factor).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
</div>

<div class="row">
<div class="col-md-3">

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%>
</label>

<div class="form-group" style="max-width:100px;">

<input name='contitem' 
type='text'

<%if((sv.contitem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.contitem.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>




<%}%>

<%if (sv.Sr687protectWritten.gt(0)) {%>
	<%Sr687protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2679 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 5 starts  -->

<!-- ILIFE-2679 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 5  ends -->

