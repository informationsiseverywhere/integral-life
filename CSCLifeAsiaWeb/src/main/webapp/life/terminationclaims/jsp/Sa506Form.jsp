<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SA506";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sa506ScreenVars sv = (Sa506ScreenVars) fw.getVariables();%>
<%{
	     if (appVars.ind04.isOn()) {
		    sv.clttwo.setReverse(BaseScreenData.REVERSED);
		    sv.clttwo.setColor(BaseScreenData.RED);
	      }
	     if (!appVars.ind04.isOn()) {
		    sv.clttwo.setHighLight(BaseScreenData.BOLD);
	     }
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.efdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.efdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.efdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}
	%>
	
<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
	</div>
	
	<div class="panel-body" >	     		     
		<div class="row" >
			<div class="col-md-3" >
			<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured Client No.")%></label> 
				<div class="form-group" >
					<div class="input-group" style="max-width: 200px;">
						<input name='clttwo' id='clttwo' type='text'
							value='<%=sv.clttwo.getFormData()%>'
							maxLength='<%=sv.clttwo.getLength()%>'
							size='<%=sv.clttwo.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(clttwo)'
							onKeyUp='return checkMaxLength(this)'
							<% 
	if((new Byte((sv.clttwo).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
							readonly="true" class="output_cell">

						<%
	}else if((new Byte((sv.clttwo).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
						class="bold_cell" > <span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
								; type="button"
								onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('clttwo')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

						<%
	}else { 
%>

						class = '
						<%=(sv.clttwo).getColor()== null  ? 
"input_cell" :  (sv.clttwo).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' > <span
							class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
								;  type="button"
								onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('clttwo')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>
 --%>
						<%} %>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Notification Date")%></label>
					<div class="input-group">
						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/MM/yyyy" data-link-field="efdateDisp"
							data-link-format="dd/mm/yyyy">

							<input name='efdateDisp' type='text'
								value='<%=sv.efdateDisp.getFormData()%>'
								maxLength='<%=sv.efdateDisp.getLength()%>'
								size='<%=sv.efdateDisp.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(efdateDisp)'
								onKeyUp='return checkMaxLength(this)'
								<% 
	if((new Byte((sv.efdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>
								readonly="true" class="output_cell">

							<%
	}else if((new Byte((sv.efdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>
							class="bold_cell" >

							<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('efdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>

							<%
	}else { 
%>

							class = '
							<%=(sv.efdateDisp).getColor()== null  ? 
"input_cell" :  (sv.efdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

							<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('efdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>

							<%} %>




						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<%=resourceBundleHandler.gettingValueFromBundle("Action")%>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="radioButtonSubmenuUIG">
						<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
							<%=resourceBundleHandler.gettingValueFromBundle("Create New Notification")%></label>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<div class="radioButtonSubmenuUIG">
						<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
							<%=resourceBundleHandler.gettingValueFromBundle("Modify Notification")%></label>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<div class="radioButtonSubmenuUIG">
						<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%>
							<%=resourceBundleHandler.gettingValueFromBundle("Close Notification")%></label>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div class="radioButtonSubmenuUIG">
						<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%>
							<%=resourceBundleHandler.gettingValueFromBundle("Notification Enquiry")%></label>
					</div>
				</div>
			</div>
			<input name='action' type='hidden'
				value='<%=sv.action.getFormData()%>'
				size='<%=sv.action.getLength()%>'
				maxLength='<%=sv.action.getLength()%>' class="input_cell"
				onFocus='doFocus(this)' onHelp='return fieldHelp(action)'
				onKeyUp='return checkMaxLength(this)'>
		</div>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
