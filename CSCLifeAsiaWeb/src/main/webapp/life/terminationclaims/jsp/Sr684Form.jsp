
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR684";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr684ScreenVars sv = (Sr684ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No    ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cmpnt  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life           ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner          ");%>	
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD            ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status   ");%>	
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date   ");%>	
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date     ");%>	
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CCY ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-----------------------------------------------------------------------------");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Seq. No        ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Type           ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Status        ");%>	
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason               ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Life           ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Evidence            ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method       ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency           ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee                ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Destination Key     ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Amount   ");%>
	
	<%-- generatedText for Claim Amount --%>
<%
    StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Amount  ");
%>
<%-- generatedText for Actual Amount --%>
<%
    StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Net Claim Amount  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
    StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Reason  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
    StringData generatedText46 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Reason  Description  ");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Paid to Date  ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Currency     ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Approval Date       ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date       ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Review Date         ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"First Payment Date   ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Paid Date      ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Payment Date    ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary Date    ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Final Payment Date   ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Recieve Date  ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Incur Date    ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow Ups           ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account        ");%>

<%{
		if (appVars.ind40.isOn()) {
			sv.aprvdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.aprvdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.aprvdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		//=======================claim amount adjustment=========================
				if (appVars.ind44.isOn()) {
					sv.pymtAdj.setReverse(BaseScreenData.REVERSED);
					sv.pymtAdj.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind44.isOn()) {
					sv.pymtAdj.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind67.isOn()) {
					sv.pymtAdj.setEnabled(BaseScreenData.DISABLED);
				}
				//=======================adjustamt amount=========================
				if (appVars.ind07.isOn()) {
					sv.adjustamt.setReverse(BaseScreenData.REVERSED);
					sv.adjustamt.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind07.isOn()) {
					sv.adjustamt.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind69.isOn()) {
					sv.adjustamt.setEnabled(BaseScreenData.DISABLED);
				}

				//=======================Net Claim Amount=========================
				if (appVars.ind09.isOn()) {
					sv.netclaimamt.setReverse(BaseScreenData.REVERSED);
					sv.netclaimamt.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind09.isOn()) {
					sv.netclaimamt.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind50.isOn()) {
					sv.netclaimamt.setEnabled(BaseScreenData.DISABLED);
				}

				//=======================adjustamt Reason=========================
				if (appVars.ind08.isOn()) {
					sv.reasoncd.setReverse(BaseScreenData.REVERSED);
					sv.reasoncd.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind08.isOn()) {
					sv.reasoncd.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind49.isOn()) {
					sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
				}

				//=======================adjustamt Reason Description=========================
				if (appVars.ind10.isOn()) {
					sv.resndesc.setReverse(BaseScreenData.REVERSED);
					sv.resndesc.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind10.isOn()) {
					sv.resndesc.setHighLight(BaseScreenData.BOLD);
				}
				if (appVars.ind70.isOn()) {
					sv.resndesc.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind18.isOn()) {
					sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
				}
	}

	%>


<div class="panel panel-default">
		
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
					<div class="form-group">
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
	<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
<%}%>
<table><tr><td>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	


</td><td>


<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td><td>



<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td></tr></table>
</div></div>
        		      

	<div class="col-md-4">
	 <% if (sv.cmoth008flag.compareTo("N") != 0) { %> 
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<%
						if (!((sv.aacct.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.aacct.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.aacct.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				<%}%>	
			</div>
		
<div class="col-md-2">

<%if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Cmpnt")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div>

<div class="row">        
					<div class="col-md-2">
					<div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
</label>
<%}%>
<div class="input-group">

<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>
</div>

<div class="col-md-6"></div>
 <% if (sv.cmoth008flag.compareTo("N") != 0) { %> 
<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim No.")%></label>
					<%
						if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.claimnumber.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.claimnumber.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
<%}%>
</div>

<div class="row">        
					<div class="col-md-2">
					<div class="form-group">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
</label>
<%}%>

<div class="input-group">
<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-4">
					
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->		
<%}%>
<div class="form-group" style="max-width:100px;">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>

<div class="col-md-4">
					<div class="form-group">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
</label>
<%}%>
<div class="input-group">
<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:400px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
</div></div>

<div class="row">        
					<div class="col-md-2">
					
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
					<div class="form-group">
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
</label>
<%}%>

<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
					<div class="form-group">
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("CCY")%>
</label>
<%}%>

<div class="input-group">
<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.currds).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.currds.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currds.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currds.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
</div></div>

<div class="row">        
					<div class="col-md-4">
					
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Seq. No")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%>
</label>
<%}%>


<%if ((new Byte((sv.rgpytypesd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpytypesd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Status")%>
</label>
<%}%>
<div class="input-group">
<%if ((new Byte((sv.rgpystat).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpystat.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpystat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpystat.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.statdsc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.statdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div></div>

<div class="row">        
					<div class="col-md-2">
<%if ((new Byte((generatedText36).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Life")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.clamparty).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clamparty.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clamparty.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clamparty.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
<div class="form-group">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Reason")%>
</label>
<%}%>

<div class="input-group">

<%if ((new Byte((sv.cltype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cltype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cltype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.clmdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clmdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Evidence")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.claimevd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.claimevd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.claimevd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.claimevd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div>

<div class="row">        
					<div class="col-md-2"><div class="form-group">
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%>
</label>
<%}%>

<div class="input-group">
<%if ((new Byte((sv.rgpymop).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpymop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpymop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpymop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.rgpyshort).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpyshort.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpyshort.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpyshort.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2"><div class="form-group">
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
</label>
<%}%>
<div class="input-group">
<%if ((new Byte((sv.regpayfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.regpayfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.regpayfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.regpayfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.frqdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div></div>
<div class="col-md-2"></div>
<div class="col-md-2">
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Destination Key")%>
</label>
<%}%>
<div class="form-group">
<%if ((new Byte((sv.destkey).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.destkey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.destkey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.destkey.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

</div>
</div>
</div>

<div class="row">        
					<div class="col-md-2"><div class="form-group">
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Payee")%>
</label>
<%}%>
<div class="input-group">
<%if ((new Byte((sv.payclt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.payclt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payclt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payclt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.payenme).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:60px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div></div>
</div></div>

<div class="row">        
					<div class="col-md-4">
<%if ((new Byte((sv.pymtAdj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Original Claim Amount")%></label>
                    <%}else{ %>
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></label>
                    <%}%>

<div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.pymt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.pymt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.pymt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Total Paid to Date")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totamnt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.totamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
</div>
</div>

<div class="col-md-2"><div class="form-group">
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
</label>
<%}%>

<div class="input-group">

<%if ((new Byte((sv.claimcur).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.claimcur.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.claimcur.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.claimcur.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.clmcurdsc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clmcurdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div></div>

<div class="row">        
					<div class="col-md-4">
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Approval Date")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">

<%	
	if ((new Byte((sv.aprvdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.aprvdateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.aprvdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 


<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="aprvdateDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.aprvdateDisp,(sv.aprvdateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>
</div>
</div>

<div class="col-md-2">
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.crtdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Review Date")%>
</label>
<%}%>

<div class="form-group">
<%if ((new Byte((sv.revdteDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.revdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.revdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.revdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
</div>

<div class="row">        
					<div class="col-md-4">
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.firstPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.firstPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.firstPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.firstPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Last Paid Date")%>
</label>
<%}%>


<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.lastPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lastPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Next Payment Date")%>
</label>
<%}%>


<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.nextPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.nextPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.nextPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.nextPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</div>
</div>
</div>

<div class="row">
<div class="col-md-4">
<%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.anvdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.anvdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.anvdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.anvdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.finalPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.finalPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.finalPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.finalPaydateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Recieve Date")%>
</label>
<%}%>


<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.recvdDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.recvdDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.recvdDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.recvdDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
</div>
<div class="row">
<div class="col-md-4">
<%if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Claim Incur Date")%>
</label>
<%}%>

<div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.incurdtDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.incurdtDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div></div>
</div>
<!-- CLM004 start -->
 <% if (sv.cmoth002flag.compareTo("Y") == 0) { %> 
<div class="row">
				 <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("No. of Days for Interest"))%></label>
        				<div class="input-group">
        				<%	
			qpsf = fw.getFieldXMLDef((sv.itstdays).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name=itstdays 
type='text'
<%if((sv.itstdays).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.itstdays) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.itstdays);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.itstdays) %>'
	 <%}%>

size='<%= sv.itstdays.getLength()%>'
maxLength='<%= sv.itstdays.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(itstdays)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
	class="input_cell">
        			</div>
        			</div>
        		</div>

              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Rate"))%></label>
        				<div class="input-group">
        				<%	
			qpsf = fw.getFieldXMLDef((sv.itstrate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
	%>

<input name='itstrate' 
type='text'

<%if((sv.itstrate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.itstrate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.itstrate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.itstrate) %>'
	 <%}%>

size='<%= sv.itstrate.getLength()%>'
maxLength='<%= sv.itstrate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(itstrate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
	class="input_cell">
        			</div>
        			</div>
        		</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Amount")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.itstamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.itstamt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='itstamt' type='text'
							<%if ((sv.itstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.itstamt.getLength(), sv.itstamt.getScale(), 3)%>'
							maxLength='<%=sv.itstamt.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(itstamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							readonly="true" class="output_cell">
					</div>
				</div>
			</div>
</div>
<% } %>
<!-- CLM004 end -->
<div class="row">
         <!-- CML-009 -->
         
    <%if ((new Byte((sv.adjustamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
            <div class="col-md-4" style="min-width:120px;">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Amount")%></label>
                    <div style="width: 150px;">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.adjustamt).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.adjustamt,
                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
                        %>

                        <input name='adjustamt' type='text'
                            <%if ((sv.adjustamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%> value='<%=valueThis%>'
                            <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                               title='<%=valueThis%>' <%}%>
                               size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.adjustamt.getLength(), sv.adjustamt.getScale(), 3)%>'
                               maxLength='<%=sv.pymt.getLength()%>'
                               onFocus='doFocus(this),onFocusRemoveCommas(this)'
                               onHelp='return fieldHelp(pymt)'
                               onKeyUp='return checkMaxLength(this)'
                               onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                               decimal='<%=qpsf.getDecimals()%>'
                               onPaste='return doPasteNumber(event,true);'
                               onBlur='return doBlurNumberNew(event,true);'
                            <%if ((new Byte((sv.adjustamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.adjustamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.adjustamt).getColor() == null ? "input_cell"
						: (sv.adjustamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>
			<%} %>
          <%-- adjustment reason --%>            
             <%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
			 <div class="col-md-4"> 
			    	  <div class="form-group">
                       
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment reason")%></label>
                      <table>
                      <tr>
                      <td>
		<%					
		if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:70px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
</td>
<td>

		<%					
		if(!((sv.resndesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:200px;margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                       
                       
                </td>
                </tr>
                </table>
                       </div></div>	

			<%} %>
     <%if ((new Byte((sv.netclaimamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
            <%-- net claim amount --%>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Net Claim Amount")%></label>
                   <div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.netclaimamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.netclaimamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.netclaimamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
                </div>
            </div>
			<%} %>         
    </div>


<Div id='mainForm_OPTS' style='visibility:hidden'>
    <%=smartHF.getMenuLink(sv.fupflg, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>
    <%=smartHF.getMenuLink(sv.ddind, resourceBundleHandler.gettingValueFromBundle("Bank Account"))%>
    <% if (sv.cmoth008flag.compareTo("N") != 0) { %> 
   	<%=smartHF.getMenuLink(sv.investres, resourceBundleHandler.gettingValueFromBundle("Investigation Results"))%>
    <%=smartHF.getMenuLink(sv.claimnotes, resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes"))%>
    <%} %>
<%-- 
<%

if(!(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' 
class="hyperLink">

<p class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%></p>     

</a>
</div>	
</div>
<%} %>

<div>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg
.getFormData()%>">
</div>


<%

if(!(sv.ddind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
.getEnabled()==BaseScreenData.DISABLED)){
%>

<div style="height: 15 px">
<div id='null'><a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' 
class="hyperLink">

<p class="text-center"><%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%></p>          

</a>
</div>	
</div>
<%} %>

<div>
<input name='ddind' id='ddind' type='hidden'  value="<%=sv.ddind
.getFormData()%>">
</div>
	 --%>
</Div>



<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("-----------------------------------------------------------------------------")%>
</div>
<%}%>

</tr></table></div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>


