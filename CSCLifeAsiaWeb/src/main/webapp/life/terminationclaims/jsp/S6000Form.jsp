

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6000";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6000ScreenVars sv = (S6000ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Paid Up Processing");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - ETI Processing");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Q - Paid Up Quotation");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"I - ETI Quotation");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">

<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
         
         
         
<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
    	 					<div class="input-group">

<input name='chdrsel' 
type='text' 
id='chdrsel'
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>


<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>

</div></div></div>


<div class="col-md-3"> </div>
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	 					<div class="input-group">
    	 			<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy">		
    	 					
<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>


<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>


<%} %>

</div></div></div>
</div>



</div></div>
</div>




<div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
<div class="panel-body"> 



<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<div class="radioButtonSubmenuUIG">

<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Paid Up Processing")%></label> 
</div>
 
 </div></div>
 
 
 
 <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
<div class="radioButtonSubmenuUIG">

<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
<%=resourceBundleHandler.gettingValueFromBundle("ETI Processing")%></label> 
</div>
 
 </div></div>
 
 
 
 
  <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
<div class="radioButtonSubmenuUIG">

<label for="Q"><%= smartHF.buildRadioOption(sv.action, "action", "Q")%>
<%=resourceBundleHandler.gettingValueFromBundle("Paid Up Quotation")%></label> 
</div>
 
 </div></div>
 
 
 	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
    	 					<div class="radioButtonSubmenuUIG">

<label for="I"><%= smartHF.buildRadioOption(sv.action, "action", "I")%>
<%=resourceBundleHandler.gettingValueFromBundle("ETI Quotation")%></label> 
</div>
 
 </div></div>
 
 
 </div>
 
 

</div>
</div>

 


<%@ include file="/POLACommon2NEW.jsp"%>

