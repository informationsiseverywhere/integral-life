
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SD5LH";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sd5lhScreenVars sv = (Sd5lhScreenVars) fw.getVariables();
%>


<%
	{
		
		if (appVars.ind09.isOn()) {
			sv.payeeno.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
	        sv.payeeno.setReverse(BaseScreenData.REVERSED);
	        sv.payeeno.setColor(BaseScreenData.RED);
	 	}
	    if (!appVars.ind02.isOn()) {
	        sv.payeeno.setHighLight(BaseScreenData.BOLD);
	    }
	
		if (appVars.ind04.isOn()) {
			sv.mop.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
	        sv.mop.setReverse(BaseScreenData.REVERSED);
	        sv.mop.setColor(BaseScreenData.RED);
	 	}
	    if (!appVars.ind01.isOn()) {
	        sv.mop.setHighLight(BaseScreenData.BOLD);
	    }
	    	    
	    if (appVars.ind14.isOn()) {
			sv.drctdbtno.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind15.isOn()) {
			sv.drctdbtno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind13.isOn()) {
	      	sv.drctdbtno.setReverse(BaseScreenData.REVERSED);
	        sv.drctdbtno.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind13.isOn()) {
	  		sv.drctdbtno.setHighLight(BaseScreenData.BOLD);
	    }
	    
		if (appVars.ind11.isOn()) {
			sv.crdtcrdno.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.crdtcrdno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
	  		sv.crdtcrdno.setReverse(BaseScreenData.REVERSED);
			sv.crdtcrdno.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind03.isOn()) {
	 		sv.crdtcrdno.setHighLight(BaseScreenData.BOLD);
	    }
	    
	    if (appVars.ind07.isOn()) {
			sv.refadjust.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.refadjust.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
	  		sv.refadjust.setReverse(BaseScreenData.REVERSED);
			sv.refadjust.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind06.isOn()) {
	 		sv.refadjust.setHighLight(BaseScreenData.BOLD);
	    }
	    	    
	    if (appVars.ind17.isOn()) {
			sv.refamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.refamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind16.isOn()) {
	  		sv.refamt.setReverse(BaseScreenData.REVERSED);
			sv.refamt.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind16.isOn()) {
	 		sv.refamt.setHighLight(BaseScreenData.BOLD);
	    }
	    
	}
%>




<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td><%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%></td>
							<td style="padding-left: 1px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
							</td>
							<td style="padding-left: 1px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>
							</td>


						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.chdrstatus, true)%>

				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div class="input-group" style="width: 120px;">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.premstatus, true)%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group" style="max-width: 140px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[]{"cntcurr"}, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="form-group" style="max-width: 140px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Owner"))%></label>
					<table>
						<tr>
							<td><%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%></td>
							<td style="padding-left: 1px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.ownername, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td><%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%></td>
							<td style="padding-left: 1px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label>
					<div class="input-group">
						<%
							if (!((sv.currfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.currfromDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					<div class="input-group">
						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>

				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
					<div class="input-group">
						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[]{"payfreq"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("payfreq");
							longValue = (String) mappedItems.get((sv.payfreq.getFormData()).toString().trim());
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 100px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component No.&Code")%></label>
					<table>
						<tr>
							<td style="min-width: 46px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.compno, true)%></td>
							<td style="padding-left: 1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.compcode, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Description")%></label>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.compdesc, true)%>						
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Risk Status")%></label>
					<table>
						<tr>
							<td style="min-width: 46px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.comprskcode, true)%></td>
							<td style="padding-left: 1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.comprskdesc, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Premium Status")%></label>
					<table>
						<tr>
							<td style="min-width: 46px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.comppremcode, true)%></td>
							<td style="padding-left: 1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.comppremdesc, true)%>
							</td>
						</tr>
					</table>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Sum Assured")%></label>
					
					
					
					<div  style="min-width:100px;max-width:167px;text-align: right;">
					<%=smartHF.getHTMLVar(0, 0, fw, null, sv.compsumss, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
				
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Billing Premium")%></label>
					
					<div  style="min-width:100px;max-width:167px;text-align: right;">
					<%=smartHF.getHTMLVar(0, 0, fw, null, sv.compbilprm, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>
					
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Premium")%></label>
					
					<div  style="min-width:100px;max-width:167px;text-align: right;">
					<%=smartHF.getHTMLVar(0, 0, fw, null, sv.refprm, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Adjustment")%></label>					
					
					<%-- <%
						if ((new Byte((sv.refadjust).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%> --%>
					
					<%
                            qpsf = fw.getFieldXMLDef((sv.refadjust).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.refadjust,
                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
                        %>

                        <input name='refadjust' type='text'
                            <%if ((sv.refadjust).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%> value='<%=valueThis%>'
                            <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                               title='<%=valueThis%>' <%}%>
                               size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.refadjust.getLength(), sv.refadjust.getScale(), 3)%>'
                               maxLength='<%=sv.refadjust.getLength()%>'
                               onFocus='doFocus(this),onFocusRemoveCommas(this)'
                               onHelp='return fieldHelp(refadjust)'
                               onKeyUp='return checkMaxLength(this)'
                               onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                               decimal='<%=qpsf.getDecimals()%>'
                               onPaste='return doPasteNumber(event,true);'
                               onBlur='return doBlurNumberNew(event,true);'
                            <%if ((new Byte((sv.refadjust).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.refadjust).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.refadjust).getColor() == null ? "input_cell"
						: (sv.refadjust).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
					
					
				
					<%
						longValue = null;
						formatValue = null;
					%>
					
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Refund Amount")%></label>
					
					
					<div  style="min-width:100px;max-width:167px;text-align: right;">
					<%=smartHF.getHTMLVar(0, 0, fw, null, sv.refamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
					</div>		
					
						
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
					<%
					String shotMop =  sv.mop.getFormData();
						fieldItem = appVars.loadF4FieldsLong(new String[]{"mop"}, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("mop");
						optionValue = makeDropDownList(mappedItems, sv.mop.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());

						if (longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
					%>

					<%
						if ((new Byte((sv.mop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' id="mop">
						<%=XSSFilter.escapeHtml(longValue)%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%=smartHF.getDropDownExt(sv.mop, fw, longValue, "mop", optionValue, 0)%>
					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
					<input type="text" id="mop1" value="<%=shotMop %>" style="visibility: hidden;margin-top: -27px;">
				</div>
			</div>
			<div class="col-md-3">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.payeeno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>

								<div class="input-group" style="width: 72px;">
									<%=smartHF.getHTMLVarExt(fw, sv.payeeno)%>
								</div> <%
								 	} else {
								 %>
								<div class="input-group" style="width: 120px;">
									<div class="form-group">
										<%=smartHF.getRichTextInputFieldLookup(fw, sv.payeeno)%></div>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('payeeno')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>

								</div> <%
								 	}
								 %>

							</td>
							<td>
								<%
									if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
								 	if (!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								
								 			if (longValue == null || longValue.equalsIgnoreCase("")) {
								 				formatValue = formatValue((sv.payeenme.getFormData()).toString());
								 			} else {
								 				formatValue = formatValue(longValue);
								 			}
								
								 		} else {
								
								 			if (longValue == null || longValue.equalsIgnoreCase("")) {
								 				formatValue = formatValue((sv.payeenme.getFormData()).toString());
								 			} else {
								 				formatValue = formatValue(longValue);
								 			}
								
								 		}
								 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px; margin-left: 1px; max-width: 167px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>

							</td>


						</tr>
					</table>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-md-3" id="bankactkeydiv3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>


					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 100px;"></div>

				</div>
			</div>

			<div class="col-md-3" id="bankactkeydiv2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No")%></label>

					<%
						if ((new Byte((sv.drctdbtno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>

					<div class="input-group" style="min-width: 200px;">
						<%=smartHF.getHTMLVarExt(fw, sv.drctdbtno)%>
					</div>
					<%
						} else {
					%>
					<div class="input-group" style="width: 225px;">
						<%
							if ("red".equals((sv.drctdbtno).getColor())) {
						%>
						<div
							style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572; height: 30px;">
							<%
								}
							%>
							<input name='drctdbtno' id='drctdbtno' type='text'
								value='<%=sv.drctdbtno.getFormData()%>'
								maxLength='<%=sv.drctdbtno.getLength()%>'
								size='<%=sv.drctdbtno.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(drctdbtno)'
								onKeyUp='return checkMaxLength(this)' disabled>
							<%
								if ("red".equals((sv.drctdbtno).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onClick="doFocus(document.getElementById('drctdbtno')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>

					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-3" id="bankactkeydiv1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No")%></label>

					<%
						if ((new Byte((sv.crdtcrdno).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>

					<div class="input-group" style="min-width: 200px;">
						<%=smartHF.getHTMLVarExt(fw, sv.crdtcrdno)%>
					</div>
					<%
						} else {
					%>
					<div class="input-group" style="width: 225px;">
						<%
							if ("red".equals((sv.crdtcrdno).getColor())) {
						%>
						<div
							style="border-style: solid; border: 2px; border-style: solid; border-color: #ec7572; height: 30px;">
							<%
								}
							%>
							<input name='crdtcrdno' id='crdtcrdno' type='text'
								value='<%=sv.crdtcrdno.getFormData()%>'
								maxLength='<%=sv.crdtcrdno.getLength()%>'
								size='<%=sv.crdtcrdno.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(crdtcrdno)'
								onKeyUp='return checkMaxLength(this)' disabled>
							<%
								if ("red".equals((sv.crdtcrdno).getColor())) {
							%>
						</div>
						<%
							}
						%>
						<%-- <%=smartHF.getRichTextInputFieldLookup(fw, sv.crdtcrdno)%> --%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onClick="doFocus(document.getElementById('crdtcrdno')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>


					<%
						}
					%>
					</div>
					</div>
					
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
							<%=smartHF.getHTMLVarExt(fw, sv.bnkcode)%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Name")%></label>
							<%=smartHF.getHTMLVarExt(fw, sv.bnkcdedsc)%>
						</div>
					</div>
		</div>

	</div>
</div>

<script>
	$(function() {
		$('#mop').change(function() {
			if (this.value === 'C') {
				// alert("C")
				$("#bankactkeydiv1").show();
				$("#bankactkeydiv2").hide();
				$("#bankactkeydiv3").hide();

				$("input[name*='bankactkey']").val(' ');
			} else if (this.value === '4') {
				// alert("4=")
				$("#bankactkeydiv2").show();
				$("#bankactkeydiv3").hide();
				$("#bankactkeydiv1").hide();

				$("input[name*='crdtcrd']").val(' ');

			} else {
				//  alert("Other")
				$("#bankactkeydiv3").show();
				$("#bankactkeydiv1").hide();
				$("#bankactkeydiv2").hide();

				$("input[name*='bankactkey']").val(' ');
				$("input[name*='crdtcrd']").val(' ');
			}
		});
	});

	$(document).ready(function() {
		if (document.getElementById("mop1").value === 'C') {			
			$("#bankactkeydiv1").show();
			$("#bankactkeydiv2").hide();
			$("#bankactkeydiv3").hide();
		} else if (document.getElementById("mop1").value === '4') {			
			$("#bankactkeydiv2").show();
			$("#bankactkeydiv3").hide();
			$("#bankactkeydiv1").hide();
		} else {			
			$("#bankactkeydiv3").show();
			$("#bankactkeydiv1").hide();
			$("#bankactkeydiv2").hide();
		}

	});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>