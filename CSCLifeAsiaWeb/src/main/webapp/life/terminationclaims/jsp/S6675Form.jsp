<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6675";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6675ScreenVars sv = (S6675ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------- Surrender Details ----------------------------------");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy loans ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Net SV Debt ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cash Deposits ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Ccy ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Print Letter (Y/N) ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Debt  ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tax");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Imposed ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Other Adjustments ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Press Enter for Calculation");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Total ");
%>

<%
	{
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.clamant.setReverse(BaseScreenData.REVERSED);
			sv.clamant.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.clamant.setColor(BaseScreenData.WHITE);
		}
		if (appVars.ind12.isOn() && !appVars.ind15.isOn()) {
			sv.clamant.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind12.isOn()) {
			generatedText20.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind12.isOn()) {
			generatedText20.setColor(BaseScreenData.BLUE);
		}
		if (appVars.ind13.isOn()) {
			sv.letterPrintFlag.setReverse(BaseScreenData.REVERSED);
			sv.letterPrintFlag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.letterPrintFlag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
		if (appVars.ind19.isOn()) {
			sv.unexpiredprm.setInvisibility(BaseScreenData.INVISIBLE);
		}	
		if (appVars.ind20.isOn()) {
			sv.suspenseamt.setInvisibility(BaseScreenData.INVISIBLE);
		}	
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/Life");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to-date ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to-date ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Plan Suffix No ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------- Coverage/Rider Details------------------------------");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cov/Rider Fund  Typ  Descriptn. R/I Ccy   Estimated Value        Actual Value");
%>
<%
	appVars.rollup(new int[] { 93 });
%>
<%
	{
		if (appVars.ind10.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.btdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.btdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.btdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			

			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					<%
						if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%
						if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.pstate.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cownnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>

						<%
							if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.linsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td style="min-width:100px;">
						<%
							if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td style="min-width:100px;">

						<%
							if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlinsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlinsname.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 100px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4"></div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
					<%
						if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div style="width: 100px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage / Rider Details")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6675' style="width: 100%;">
						<thead>
							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Cov/Rider")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("R/ICurrency")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></center></th>
							</tr>

							<%
								/* This block of jsp code is to calculate the variable width of the table at runtime.*/
								int[] tblColumnWidth = new int[8];
								int totalTblWidth = 0;
								int calculatedValue = 0;

								if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.coverage.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
								} else {
									calculatedValue = (sv.coverage.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[0] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rider.getFormData()).length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
								} else {
									calculatedValue = (sv.rider.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[1] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.fund.getFormData()).length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
								} else {
									calculatedValue = (sv.fund.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[2] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fieldType.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
								} else {
									calculatedValue = (sv.fieldType.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[3] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.shortds.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
								} else {
									calculatedValue = (sv.shortds.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[4] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.cnstcur.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 12;
								} else {
									calculatedValue = (sv.cnstcur.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[5] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.estMatValue.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 12;
								} else {
									calculatedValue = (sv.estMatValue.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[6] = calculatedValue;

								if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.actvalue.getFormData())
										.length()) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 12;
								} else {
									calculatedValue = (sv.actvalue.getFormData()).length() * 12;
								}
								totalTblWidth += calculatedValue;
								tblColumnWidth[7] = calculatedValue;
							%>
							<%
								GeneralTable sfl = fw.getTable("s6675screensfl");
								int height;
								if (sfl.count() * 27 > 210) {
									height = 210;
								} else {
									height = sfl.count() * 27;
								}
							%>
						
						<tbody>
							<%
								String backgroundcolor = "#FFFFFF";

								S6675screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S6675screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr id='tr<%=count%>' height="30">

								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[0] + tblColumnWidth[1]%>px;z-index:8;position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;"
									align="right"><%=sv.coverage.getFormData()%>&nbsp; <!-- 	</td>
				    									<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[1]%>px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
															 --> <%=sv.rider.getFormData()%></td>

								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[2]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="left"><%=sv.fund.getFormData()%></td>
								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[3]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="left"><%=sv.fieldType.getFormData()%></td>
								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[4]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="left"><%=sv.shortds.getFormData()%></td>

								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[5]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="left"><%=sv.cnstcur.getFormData()%></td>

								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[6]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.estMatValue,
 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 		if (!sv.estMatValue.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>




								</td>

								<td
									style="color:#434343; padding: 5px; width:<%=tblColumnWidth[7]%>px;font-weight: bold;font-size: 12px; font-family: Arial"
									align="right">
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.actvalue,
 				COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
 		if (!sv.actvalue.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
 %> <%=formatValue%> <%
 	longValue = null;
 		formatValue = null;
 %>




								</td>

							</tr>

							<%
								if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
										backgroundcolor = "#ededed";
									} else {
										backgroundcolor = "#FFFFFF";
									}
									count = count + 1;
									S6675screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						
					</table>

				</div>
			</div>

		</div>

		

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<div class="input-group">
					<%
						if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
				</div>
			</div>

			

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Print Letter")%></label>
					<input type='checkbox' name='letterPrintFlag' value='Y'
						onFocus='doFocus(this)' onHelp='return fieldHelp(letterPrintFlag)'
						onKeyUp='return checkMaxLength(this)'
						<%if ((sv.letterPrintFlag).getColor() != null) {%>
						style='background-color: #FF0000;'
						<%}
			if ((sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")) {%>
						checked
						<%}
			if ((sv.letterPrintFlag).getEnabled() == BaseScreenData.DISABLED) {%>
						disabled <%}%> class='UICheck'
						onclick="handleCheckBox('letterPrintFlag')" /> <input
						type='checkbox' name='letterPrintFlag' value=' '
						<%if (!(sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")) {%>
						checked <%}%> style="visibility: hidden;"
						onclick="handleCheckBox('letterPrintFlag')" />
				</div>
			</div>

			

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div class="input-group">
		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("currcd");
							optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),1,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
						%>
						
						<% 
							if((new Byte((sv.currcd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
							
						<% if("red".equals((sv.currcd).getColor())){
						%>
						<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
						<%
						} 
						%>
						
						<select name='currcd' type='list' style="width:145px;"
						<% 
							if((new Byte((sv.currcd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							disabled
							class="output_cell"
						<%
							}else if((new Byte((sv.currcd).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						<%
							}else { 
						%>
							class = 'input_cell' 
						<%
							} 
						%>
						>
						<%=optionValue%>
						</select>
						<% if("red".equals((sv.currcd).getColor())){
						%>
						</div>
						<%
						} 
						%>
						
						<%
						} 
						%>
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Deposits")%></label>
					<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.zrcshamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>

					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
				</div>
			</div>

			<div class="col-md-1"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy loans")%></label>
					<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.policyloan,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>

					<%
						longValue = null;
						formatValue = null;
					%>
					</div>
				</div>
			</div>


			<div class="col-md-2"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Net SV Debt")%></label>
					<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.netOfSvDebt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.netOfSvDebt,
								COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.netOfSvDebt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>

					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Debt")%></label>
<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.tdbtamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					%>

					<input name='tdbtamt' readonly="true" type='text'
						<%if ((sv.tdbtamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.tdbtamt.getLength(), sv.tdbtamt.getScale(), 3)%>'
						maxLength='<%=sv.tdbtamt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(tdbtamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.tdbtamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.tdbtamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.tdbtamt).getColor() == null ? "input_cell"
						: (sv.tdbtamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%> <%longValue = null;
			formatValue = null;%>>
				</div>
				</div>
			</div>

			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Imposed")%></label>
<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt,
								COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					%>

					<input name='taxamt' readonly="true" type='text'
						<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.taxamt.getLength(), sv.taxamt.getScale(), 3)%>'
						maxLength='<%=sv.taxamt.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(taxamt)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
						: (sv.tdbtamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%> <%longValue = null;
			formatValue = null;%>>
				</div>
				</div>
			</div>

			<div class="col-md-2"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Other Adjustments")%></label>
					<div style="width: 150px;">
					<%
						qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						valueThis = smartHF.getPicFormatted(qpsf, sv.otheradjst,
								COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					%>

					<input name='otheradjst' readonly="true" type='text'
						<%if ((sv.otheradjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
						style="text-align: right" <%}%> value='<%=valueThis%>'
						<%if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=valueThis%>' <%}%>
						size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.otheradjst.getLength(), sv.otheradjst.getScale(), 3)%>'
						maxLength='<%=sv.otheradjst.getLength()%>'
						onFocus='doFocus(this),onFocusRemoveCommas(this)'
						onHelp='return fieldHelp(otheradjst)'
						onKeyUp='return checkMaxLength(this)'
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
						<%if ((new Byte((sv.otheradjst).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell"
						<%} else if ((new Byte((sv.otheradjst).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" <%} else {%>
						class=' <%=(sv.otheradjst).getColor() == null ? "input_cell"
						: (sv.otheradjst).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%> <%longValue = null;
			formatValue = null;%>>
				</div>
				</div>
			</div>

		</div>

		<div class="row">
		<%
			if ((new Byte((sv.unexpiredprm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Unexpired Premium")%></label>
		       
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.unexpiredprm).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unexpiredprm,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.unexpiredprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 150px; text-align: right" >	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 150px;" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		
		       	</div>
		       	<div class="col-md-2"></div>
		       	<%} %>
		       	
		       	<%
			if ((new Byte((sv.unexpiredprm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		 <div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%></label>
		       
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.suspenseamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.suspenseamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.suspenseamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 150px; text-align: right" >	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 150px;" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		
		       	</div>
		       	<div class="col-md-2"></div>
		       	<%} %>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.estimateTotalValue,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 100px;left: -1px; text-align: right">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div style="min-width: 100px;left: -1px;" class="blank_cell">&nbsp;</div>

						<%
							}
						%>

						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>

						<%
							qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.clamant,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 100px;margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div style="min-width: 100px;margin-left:1px;" class="blank_cell">&nbsp;</div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-s6675').DataTable({
			ordering : false,
			searching : false,
			scrollY: "300px",
			scrollCollapse: true,
			scrollX:true,
			info: false,
			paging: false
		});
	});
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

