<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5024";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5024ScreenVars sv = (S5024ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accounting Month ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Name ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Run Id ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Job Queue ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Library ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maturity/Expiry Date Range");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts from      ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date From)");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to      ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Date To)");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number Range");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extract all contracts from      ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to      ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.datecfrom.setReverse(BaseScreenData.REVERSED);
			sv.datecfrom.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.datecfrom.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.datecto.setReverse(BaseScreenData.REVERSED);
			sv.datecto.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.datecto.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.chdrnum1.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.chdrnum1.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Schedule Name/Number")%>
					<%}%></label>
		       		
		       		<table><tr><td >
		       		<%if ((new Byte((sv.scheduleName).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.scheduleName.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.scheduleName.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							</td>
							<td>
							<%if ((new Byte((sv.scheduleNumber).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%	
									qpsf = fw.getFieldXMLDef((sv.scheduleNumber).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.scheduleNumber);
									
									if(!((sv.scheduleNumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell">	
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" > &nbsp; </div>
								
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
							
						 <%}%>
						 </td>
						 </tr> </table>
		       		
		       		</div>
		       	</div>
		       	
		       		
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Accounting Month/Year")%>
					<%}%></label>
		       		<table><tr><td >
		       		<%if ((new Byte((sv.acctmonth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.acctmonth).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.acctmonth);
							
							if(!((sv.acctmonth.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
					
				</td>
				<td >
				
				
				
				
				<%if ((new Byte((sv.acctyear).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.acctyear).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.acctyear);
							
							if(!((sv.acctyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
				 </td>
		       		</tr></table>
		       		</div>
		       	</div>
		       	
		       	
		       		
		       		
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Job Queue")%>
					<%}%>
		       		</label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.jobq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.jobq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jobq.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<!-- <div class="col-md-2">
		       		</div> -->
		       	<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"bcompany"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("bcompany");
				longValue = (String) mappedItems.get((sv.bcompany.getFormData()).toString().trim());  
				%>
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
		       		<div class="input-group">
		       		<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		<%=XSSFilter.escapeHtml(longValue)%>
					   		<%}%>
					   </div>
					<%
					   longValue = null;
					   formatValue = null;
					%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<!-- <div class="col-md-2">
		       		</div> -->
		       		
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"bbranch"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("bbranch");
						longValue = (String) mappedItems.get((sv.bbranch.getFormData()).toString().trim());  
					%>
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Branch"))%></label>
		       		<div class="input-group">
		       		<div style="position: relative; margin-left:1px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? "blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		<%}%>
						   </div>
						<%
						   longValue = null;
						   formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    <br/><br/>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity/Expiry Date Range")%></label>
		       	    		</div>
		       	</div>

		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts from")%></label>
		       		<table><tr> 
		       		<td style="min-width: 170px;">
		       		<%	
						if ((new Byte((sv.datecfrom).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.datecfrom.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.datecfrom).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					
					<% 
						if((new Byte((sv.datecfrom).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					
					<%
						}else if((new Byte((sv.datecfrom).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datecfrom" data-link-format="dd/mm/yyyy">
	                   <%=smartHF.getRichTextDateInput(fw, sv.datecfrom,(sv.datecfrom.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	       			</div>
	       			
								
					<%
						}else { 
					%>
					
					
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datecfrom" data-link-format="dd/mm/yyyy">
	                   <%=smartHF.getRichTextDateInput(fw, sv.datecfrom,(sv.datecfrom.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	       			</div>
	       			
	       		
					<%} }longValue = null;}%>
					</td>
					
					<td style="min-width: 40px; text-align: left;">
					
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("to")%>
					</div>
					
					</td>
					
					<td style="min-width: 170px;">
					
					<%	
						if ((new Byte((sv.datecto).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.datecto.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.datecto).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
				  
					
					<% 
						if((new Byte((sv.datecto).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
				
					
					<%
						}else if((new Byte((sv.datecto).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datecto" data-link-format="dd/mm/yyyy">
	                   <%=smartHF.getRichTextDateInput(fw, sv.datecto,(sv.datecto.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	       			</div>
					
					<%
						}else { 
					%>
					
										
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="datecto" data-link-format="dd/mm/yyyy">
	                   <%=smartHF.getRichTextDateInput(fw, sv.datecto,(sv.datecto.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	       			</div>
					
					
					<%} }longValue = null;}%>
		       		</td>
		       		</tr> </table>
		       		</div>
		       	</div>
		       	
		      </div>
		    
		   <br/><br/>
		    
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number Range")%></label>
		       		</div>
		       	</div>
		      </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Extract all contracts from")%></label>
		       		<table><tr>
		       		<td style="min-width: 100px;">
		       		<input name='chdrnum' 
						type='text'
						
						<%if((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.chdrnum.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.chdrnum.getLength()%>'
						maxLength='<%= sv.chdrnum.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.chdrnum).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.chdrnum).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.chdrnum).getColor()== null  ? 
									"input_cell" :  (sv.chdrnum).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</td>
						
						<td style="min-width: 40px; text-align: center;">
						
						<div class="label_txt">
						<%=resourceBundleHandler.gettingValueFromBundle("to")%>
						</div>
						
						</td>
						
						
						<td style="min-width: 100px;">
						<input name='chdrnum1' 
						type='text'
						
						<%if((sv.chdrnum1).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.chdrnum1.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.chdrnum1.getLength()%>'
						maxLength='<%= sv.chdrnum1.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(chdrnum1)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.chdrnum1).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.chdrnum1).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.chdrnum1).getColor()== null  ? 
									"input_cell" :  (sv.chdrnum1).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						</td>
		       		</tr></table>
		       		</div>
		       	</div>
		       	
		     
		    </div>
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maturity/Expiry Date Range")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Date From)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Date To)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number Range")%>
</div>

</tr></table></div>