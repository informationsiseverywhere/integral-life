
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr57p";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr57pScreenVars sv = (Sr57pScreenVars) fw.getVariables();%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Claim Details -----------------------------------------------");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dth. Date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pol Lns ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Poldebt ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Intrest ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff. Date ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Oth Adj ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Off Crg ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cause of Death");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net Tot ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Est Tot ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currncy ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Deposit ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Claim Amt ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adj Reason    ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow ups       ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.dtofdeathDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.causeofdth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.causeofdth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
/* 		if (appVars.ind04.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}*/
		if (appVars.ind09.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.longdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.longdesc.setReverse(BaseScreenData.REVERSED);
			sv.longdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.longdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
	
		if (appVars.ind31.isOn()) {
		    sv.claimnumber.setReverse(BaseScreenData.REVERSED);
		    sv.claimnumber.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind31.isOn()) {
		    sv.claimnumber.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind24.isOn()) {
		  	sv.claimnumber.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Coverage/Rider Review ---------------------------------------");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                          Lien                 Estimated              Actual");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov/Rd Fund Typ Descriptn.Code     Ccy             Value               Value");%>
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					<%}%></label>
		       		<table><tr><td style="min-width:65px">
					<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</td><td style="min-width:1px">
					</td><td style="min-width:40px">
					
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:50px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</td><td style="min-width:1px">
					</td><td>
					
					<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div id="cntdesc" style="margin-left: 1px;max-width: 160px;min-width:120px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	

		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%>
					<%} else { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Risk Commentcement Date")%>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%}%></label>
		       		<table><tr><td  style="min-width:70px">
					
					<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					  
					  
					  	</td><td style="min-width:1px">
					</td>
					<!-- ILIFE-8700--Start -->
					<%-- <td style="min-width:70px">
					
					
					  <%if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=formatValue%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td> --%>
		       		<!-- ILIFE-8700--End -->
		       		</tr></table>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Risk/Premium Status")%>
					<%}%></label>
		       		<table><tr><td>
						
						<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width:100px">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						  
						  
						  </td><td style="min-width:1px">
					</td><td style="min-width:65px">
					
					
						  <%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:100px;margin-left: 1px;max-width: 150px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					<%}%></label>
		       		<table><tr><td>
					<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="max-width:100px" >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					</td><td style="min-width:1px">
					</td><td style="max-width:160px;">
					
					
					<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:120px;max-width:195px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	

		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					<%}%></label>
		       		<table><tr><td style="min-width:40px">
					<%if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:50px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					
					
					</td><td style="min-width:1px">
					</td><td style="min-width:70px">
					
					<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:70px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					
					
						</td><td style="min-width:1px">
					</td><td style="max-width:160px;">
					
					<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:120px;max-width:195px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	<%if ((new Byte((sv.claimnumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
		       	<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim No")%></label>
						<div style="width: 80px;">
						<%
							if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {
		       	
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
		    </div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
				<%}%>
 </div>
		    
		</div>    
		     <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
					<%}%></label>
		       		<div class="input-group">
					<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
					<%}%></label>
		       		<div class="input-group">
					
					<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
					<%}%></label>
		       		<div class="input-group">
					
					<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.dtofdeathDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.dtofdeathDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Cause of Death")%></label>
		       		<div class="input-group">
					<%if ((new Byte((sv.causeofdthdsc).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.causeofdthdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.causeofdthdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.causeofdthdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total Claim Amount")%></label>
		       		<div class="input-group" style="min-width: 80px;">
						<%if ((new Byte((sv.totclaim).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%	
									qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.totclaim);
									
									if(!((sv.totclaim.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell">	
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" > &nbsp; </div>
								
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
							
						 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total Adjustment")%></label>
		       		<div class="input-group" style="min-width: 80px;">
						<%if ((new Byte((sv.otheradjst).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%	
									qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.otheradjst);
									
									if(!((sv.otheradjst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell">	
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" > &nbsp; </div>
								
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
							
						 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Reason")%></label>
		       		<table><tr><td>
						<%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:71px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						  
						  
						  </td><td style="min-width:1px">
						  </td><td style="max-width:160px;">
						  
						<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:120px">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>	
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[8];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.coverage.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*5;								
			} else {		
				calculatedValue = (sv.coverage.getFormData()).length()*5;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rider.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*5;								
			} else {		
				calculatedValue = (sv.rider.getFormData()).length()*5;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.crtable.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*6;								
			} else {		
				calculatedValue = (sv.crtable.getFormData()).length()*6;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.lcendesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*10;								
			} else {		
				calculatedValue = (sv.lcendesc.getFormData()).length()*10;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.actvalue.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*6;								
			} else {		
				calculatedValue = (sv.actvalue.getFormData()).length()*6;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.zclmadjst.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*7;								
			} else {		
				calculatedValue = (sv.zclmadjst.getFormData()).length()*7;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.zhldclmv.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*7;								
			} else {		
				calculatedValue = (sv.zhldclmv.getFormData()).length()*7;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.zhldclma.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*7;								
			} else {		
				calculatedValue = (sv.zhldclma.getFormData()).length()*7;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[7]= calculatedValue;
			
						
			%>
		<%
		GeneralTable sfl = fw.getTable("sr57pscreensfl");
		int height;
		if(sfl.count()*27 > 80) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
		<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sr57p' style="border-right: thin solid #dddddd !important;" width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider")%></center> </th>
						<th  colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage Description")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Hold Claim Amount")%></center></th>
						<th style="border-right-width: 1px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Hold Adjustment")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
								String backgroundcolor = "#FFFFFF";
							Sr57pscreensfl
							.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (Sr57pscreensfl
							.hasMoreScreenRows(sfl)) {	
							{
								if (appVars.ind08.isOn()) {
									sv.zhldclmv.setReverse(BaseScreenData.REVERSED);
								}		
								if (appVars.ind08.isOn()) {
									sv.zhldclmv.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind08.isOn()) {
									sv.zhldclmv.setHighLight(BaseScreenData.BOLD);
								}
								if (appVars.ind11.isOn()) {
									sv.zhldclma.setReverse(BaseScreenData.REVERSED);
								}
								
								if (appVars.ind11.isOn()) {
									sv.zhldclma.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind11.isOn()) {
									sv.zhldclma.setHighLight(BaseScreenData.BOLD);
								}
							}
						%>
						
							<tr>
												    									<td  
											<%if((sv.coverage).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align="right" <%}%> style="width:50px;">
																									
																<%if((new Byte((sv.coverage).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%= sv.coverage.getFormData()%>
												
																				 
										
															<%}%>
									</td>
										    									<td  
											<%if((sv.rider).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align=right <%}%> style="width:50px;">									
																<%if((new Byte((sv.rider).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%= sv.rider.getFormData()%>
												
																				 
										
															<%}%>
									</td>
										    									<td  
											<%if((sv.crtable).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align="right" <%}%> style="min-wdith:50px;">									
																<%if((new Byte((sv.crtable).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%= sv.crtable.getFormData()%>
												
																				 
										
															<%}%>
									</td>
										    									<td  
											<%if((sv.lcendesc).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align="right" <%}%> style="min-width:180px;max-width:280px;">									
																<%if((new Byte((sv.lcendesc).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%= sv.lcendesc.getFormData()%>
												
																				 
										
															<%}%>
									</td>
										    									<td  style="min-width: 160px;"
											<%if((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																<%if((new Byte((sv.actvalue).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());						
												qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue);
													if(!(sv.actvalue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
											 			 		
									 		
									    				 
										
															<%}%>
									</td>
										    									<td  
											<%if((sv.zclmadjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																<%if((new Byte((sv.zclmadjst).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
															
																	
												<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.zclmadjst).getFieldName());						
												qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.zclmadjst);
													if(!(sv.zclmadjst).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
											 			 		
									 		
									    				 
										
															<%}%>
									</td>
										    									<td  
											<%if((sv.zhldclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="right" <%}%> >
											<%if ((new Byte((sv.zhldclmv).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						
							<%	
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.zhldclmv).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									valueThis=smartHF.getPicFormatted(qpsf,sv.zhldclmv,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									
							%>
						
						<input 
						name='<%="sr57pscreensfl" + "." +
											 "zhldclmv" + "_R" + count %>'
											 id='<%="sr57pscreensfl" + "." +
											 "zhldclmv" + "_R" + count %>'
						type='text'
						
						<%if((sv.zhldclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
							value='<%=valueThis%>'
									 <%	 
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=valueThis %>'
							 <%}%>
						
						size='<%= sv.zhldclmv.getLength()%>'
						maxLength='<%= sv.zhldclmv.getLength()%>' 
						onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						
						<% 
							if((new Byte((sv.zhldclmv).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.zhldclmv).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.zhldclmv).getColor()== null  ? 
									"input_cell" :  (sv.zhldclmv).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>											
																
									</td>
										    									<td  style="border-right-width: 1px;"
											<%if((sv.zhldclma).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="right" <%}%> >	
																<%if ((new Byte((sv.zhldclma).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						
							<%	
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.zhldclma).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									 valueThis=smartHF.getPicFormatted(qpsf,sv.zhldclma,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									
							%>
						
						<input  
						name='<%="sr57pscreensfl" + "." +
											 "zhldclma" + "_R" + count %>'
											 id='<%="sr57pscreensfl" + "." +
											 "zhldclma" + "_R" + count %>'
						type='text'
						
						<%if((sv.zhldclma).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
							value='<%=valueThis %>'
									 <%
							
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%= valueThis %>'
							 <%}%>
						
						size='<%= sv.zhldclma.getLength()%>'
						maxLength='<%= sv.zhldclma.getLength()%>' 
						onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						
						<% 
							if((new Byte((sv.zhldclma).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.zhldclma).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.zhldclma).getColor()== null  ? 
									"input_cell" :  (sv.zhldclma).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>		
						</td>
											
							</tr>
						
							<%
								if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
										backgroundcolor = "#ededed";
									} else {
										backgroundcolor = "#FFFFFF";
									}
							count = count + 1;
							Sr57pscreensfl
							.setNextScreenRow(sfl, appVars, sv);
							}
							%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
		
	 </div>
</div>
<script>
$(document).ready(function() {
	if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','230px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','190px')
	} 
	
})
</script>

<!-- <script>
	$(document).ready(function() {
    	$('#dataTables-sr57p').DataTable({
    		paging: false,
        	ordering: false,
        	searching:false
           	});
    });
</script>  -->
<%@ include file="/POLACommon2NEW.jsp"%>

