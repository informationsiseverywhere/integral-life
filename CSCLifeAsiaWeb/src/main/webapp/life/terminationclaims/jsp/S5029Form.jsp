<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5029";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5029ScreenVars sv = (S5029ScreenVars) fw.getVariables();%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind60.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill-to-date ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------------------- List of Policies --------------------------------");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status Codes");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suffix");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Covr");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					
					
					
					
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					
					
					
					
					<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 600px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				
				
				
				
				<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%>
					<%} else { %>
					<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("RCD"))%>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%}%></label>
								
					
		       		<div class="input-group">
		       		<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		      <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				
				
				<%if ((new Byte((sv.ownernum).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.ownernum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownernum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownernum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		    </div>
		    
		    
		      <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					
					
					
					
					<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("J/Life")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
									<%					
									if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							
							
							
							
							
							<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>

		    </div>
		    
		      <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Bill-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>

		    </div>
		    
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("List of Policies")%></label>
		       		 </div>
		       	</div>
		    </div>
		    
		    
		    <%
				/* This block of jsp code is to calculate the variable width of the table at runtime.*/
				int[] tblColumnWidth = new int[5];
				int totalTblWidth = 0;
				int calculatedValue =0;
				
																		if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
										} else {		
											calculatedValue = (sv.select.getFormData()).length()*12;								
										}					
																		totalTblWidth += calculatedValue;
						tblColumnWidth[0]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.planSuffix.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
							} else {		
								calculatedValue = (sv.planSuffix.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[1]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.stycvr.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
							} else {		
								calculatedValue = (sv.stycvr.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[2]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.pstatcode.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
							} else {		
								calculatedValue = (sv.pstatcode.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[3]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.statdesc.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
							} else {		
								calculatedValue = (sv.statdesc.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[4]= calculatedValue;
							%>
						<%
						GeneralTable sfl = fw.getTable("s5029screensfl");
						int height;
						if(sfl.count()*27 > 210) {
						height = 210 ;
						} else {
						height = sfl.count()*27;
						}	
						%>
						
						<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5029'>	
			    	 	<thead>
			    	 	<tr class='info'>									
							<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Select")%></center></th>
							<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix")%></center></th>
							<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Status Codes")%></center></th>
							<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Status Description")%></center></th>
							</tr>
							<tr class='info'>     							
									<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></center></th>
									<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></center></th>
						</tr>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
								S5029screensfl
								.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5029screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							
								<tr class="tableRowTag" id='<%="tablerow"+count%>' >
													<%if((new Byte((sv.select).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
												<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																										
															
																				
																				
												
												 					 
												 <input type="checkbox" 
													 value='<%= sv.select.getFormData() %>' 
													 onFocus='doFocus(this)' onHelp='return fieldHelp("s5029screensfl" + "." +
													 "select")' onKeyUp='return checkMaxLength(this)' 
													 name='s5029screensfl.select_R<%=count%>'
													 id='s5029screensfl.select_R<%=count%>'
													 onClick="selectedRow('s5029screensfl.select_R<%=count%>')"
													 class="UICheck"
												 />
												 
												 					
												
																		
																</td>
									<%}else{%>
																			<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
												</td>														
																	
												<%}%>
															<%if((new Byte((sv.planSuffix).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" 
												<%if((sv.planSuffix).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.planSuffix.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															<%if((new Byte((sv.stycvr).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
												<%if((sv.stycvr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.stycvr.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															<%if((new Byte((sv.pstatcode).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
												<%if((sv.pstatcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.pstatcode.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															<%if((new Byte((sv.statdesc).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
												<%if((sv.statdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.statdesc.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
																					
											    </td>
																	
												<%}%>
																
								</tr>
							
								<%
								count = count + 1;
								S5029screensfl
								.setNextScreenRow(sfl, appVars, sv);
								}
								%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	 </div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

