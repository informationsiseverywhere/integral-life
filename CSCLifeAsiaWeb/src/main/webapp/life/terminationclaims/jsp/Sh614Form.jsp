

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH614";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh614ScreenVars sv = (Sh614ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Details ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Capitalisation Details ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.intRate.setReverse(BaseScreenData.REVERSED);
			sv.intRate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.intRate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.interestFrequency.setReverse(BaseScreenData.REVERSED);
			sv.interestFrequency.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.interestFrequency.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.compfreq.setReverse(BaseScreenData.REVERSED);
			sv.compfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.compfreq.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    		
			<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    		
		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

  

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group"style="max-width:100px;">
						    		
				
			
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
						    		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    		
	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
			

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				    		</div>
				    		
				    		<br>
				    		<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Details")%></label>
					    		</div></div></div>
				    	
				    	
				    	<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Rate")%></label>
					    		     <div class="input-group" style="min-width:150px">
						    		
	<%	
			qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS5);
			
	%>

<input name='intRate' 
type='text'

<%if((sv.intRate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intRate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
	 <%}%>

size='<%= sv.intRate.getLength()%>'
maxLength='<%= sv.intRate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intRate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intRate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intRate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intRate).getColor()== null  ? 
			"input_cell" :  (sv.intRate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				  	
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
							<div class="input-group">
						    <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"interestFrequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("interestFrequency");
	optionValue = makeDropDownList( mappedItems , sv.interestFrequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.interestFrequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.interestFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.interestFrequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:272px;"> 
<%
} 
%>

<select name='interestFrequency' type='list' style="width:270px;"
<% 
	if((new Byte((sv.interestFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.interestFrequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.interestFrequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				    	<br>
				    	
				    			    		<div class="row">	
			    	<div class="col-md-6"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Capitalisation Details")%></label>
					    		</div></div></div>
				    	
				    	
				    	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
					    		     <div class="input-group">
						    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"compfreq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("compfreq");
	optionValue = makeDropDownList( mappedItems , sv.compfreq.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.compfreq.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.compfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.compfreq).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:272px;"> 
<%
} 
%>

<select name='compfreq' type='list' style="width:270px;"
<% 
	if((new Byte((sv.compfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.compfreq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.compfreq).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

				      			     </div>
				    		</div>
					</div>
				    		
				  
			
			    		
		    </div>	
		    
		    </div></div>
				    		


<%@ include file="/POLACommon2NEW.jsp"%>

