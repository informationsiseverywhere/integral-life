

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6694";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6694ScreenVars sv = (S6694ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Account Code             ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Account Type             ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"G/L Account Key              ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"DB/CR (+/-)                  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Details Required        ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee Details Required       ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Details Required    ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method               ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.sacscode.setReverse(BaseScreenData.REVERSED);
			sv.sacscode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.sacscode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.sacstype.setReverse(BaseScreenData.REVERSED);
			sv.sacstype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.sacstype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.glact.setReverse(BaseScreenData.REVERSED);
			sv.glact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.glact.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.debcred.setReverse(BaseScreenData.REVERSED);
			sv.debcred.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.debcred.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.bankreq.setReverse(BaseScreenData.REVERSED);
			sv.bankreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.bankreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.payeereq.setReverse(BaseScreenData.REVERSED);
			sv.payeereq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.payeereq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.contreq.setReverse(BaseScreenData.REVERSED);
			sv.contreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.contreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.reqntype.setReverse(BaseScreenData.REVERSED);
			sv.reqntype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.reqntype.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<!-- ILIFE-2674 Life Cross Browser -Coding and UT- Sprint 3 D5: Task 5 starts  -->
	<style>
	@media \0screen\,screen\9
{
.iconPos{margin-bottom:1px}
}
					 </style>
					 
					 
					 
	<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-3"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-2"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			 </div>      
			  
			    <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div>
        </div>   
        
          <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sub Account Code"))%></label>
        				
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacscode");
	optionValue = makeDropDownList( mappedItems , sv.sacscode.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.sacscode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.sacscode).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='sacscode' type='list' style="width:240px;"
<% 
	if((new Byte((sv.sacscode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell"  style="min-width:240px;"
<%
	}else if((new Byte((sv.sacscode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
		class="bold_cell"  style="min-width:240px;"
<%
	}else { 
%>
	class = 'input_cell'  style="min-width:240px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.sacscode).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	</div>
      
        	
        	<div class="col-md-1"></div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sub Account Type"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacstype");
	optionValue = makeDropDownList( mappedItems , sv.sacstype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacstype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.sacstype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.sacstype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='sacstype' type='list' style="width:240px;"
<% 
	if((new Byte((sv.sacstype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell" style="min-width:240px;"
<%
	}else if((new Byte((sv.sacstype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  style="min-width:240px;"
<%
	}else { 
%>
	class = 'input_cell'  style="min-width:240px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.sacstype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	</div>
        	<div class="col-md-1"></div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("G/L Account Key"))%></label>
        				<div class="input-group" style="min-width:100px">
        				<%	
	longValue = sv.glact.getFormData();  
%>

<% 
	if((new Byte((sv.glact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='glact' id='glact'
type='text' 
value='<%=sv.glact.getFormData()%>' 
maxLength='<%=sv.glact.getLength()%>' 
size='<%=sv.glact.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(glact)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.glact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.glact).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; href="javascript:;" type="button" onClick="doFocus(document.getElementById('glact')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true" ></i>
                          </button>
                          </span>

<%
	}else { 
%>

class = ' <%=(sv.glact).getColor()== null  ? 
"input_cell" :  (sv.glact).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-btn">
                           
                           <button class="btn btn-info" sstyle="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('glact')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true" ></i>
                          </button>
                          </span>

<%}longValue = null;} %>
        		</div>
        	</div>
        </div>			
      </div>  		
      
      <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("DB/CR (+/-)"))%></label>
        				<%	
	if ((new Byte((sv.debcred).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("-")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Credit");
	}
	if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("+")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Debit");
	}
	 
%>
<!-- ILIFe-2555 ended by vjain60 -->

<% 
				if((new Byte((sv.debcred).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%> 
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
				"blank_cell" : "output_cell" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	
				<%} %>
				</div>
				
				<%
				longValue = null;
				%>

				<% }else { %>

<% if("red".equals((sv.debcred).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='debcred' style="width:140px;" 	
					onFocus='doFocus(this)'
					onHelp='return fieldHelp(debcred)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.debcred).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.debcred).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
<!-- ILIFe-2555 started by vjain60 -->
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="-"<% if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("-")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Credit")%></option>
<option value="+"<% if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("+")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Debit")%></option>
<!-- ILIFe-2555 ended by vjain60 -->		

</select>
<% if("red".equals((sv.debcred).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>
        		</div>
        	</div>
        </div>			
        &nbsp;
         <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Details Required"))%></label>
        				<input type='checkbox' name='bankreq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(bankreq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.bankreq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.bankreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.bankreq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('bankreq')"/>

<input type='checkbox' name='bankreq' value=' ' 

<% if(!(sv.bankreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('bankreq')"/>
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee Details Required"))%></label>
        				<input type='checkbox' name='payeereq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(payeereq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.payeereq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.payeereq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.payeereq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('payeereq')"/>

<input type='checkbox' name='payeereq' value=' ' 

<% if(!(sv.payeereq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('payeereq')"/>
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Details Required"))%></label>
        				<input type='checkbox' name='contreq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(contreq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.contreq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.contreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.contreq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('contreq')"/>

<input type='checkbox' name='contreq' value=' ' 

<% if(!(sv.contreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('contreq')"/>
        				
        		</div>
        	</div>
        </div>	
        
        <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Method"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("reqntype");
	optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.reqntype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.reqntype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='reqntype' type='list' style="width:200px;"
<% 
	if((new Byte((sv.reqntype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell" style="min-width:200px;"
<%
	}else if((new Byte((sv.reqntype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
class="bold_cell" style="min-width:200px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:200px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.reqntype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	</div>
        </div>											 
	</div>
	
	<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>

</tr></table></div>
	</div>				 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
<!-- ILIFE-2674 Life Cross Browser -Coding and UT- Sprint 3 D5: Task 5 ends -->

<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



&nbsp;

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Sub Account Code")%>
</div>



<br/>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacscode");
	optionValue = makeDropDownList( mappedItems , sv.sacscode.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.sacscode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.sacscode).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='sacscode' type='list' style="width:140px;"
<% 
	if((new Byte((sv.sacscode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.sacscode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.sacscode).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Sub Account Type")%>
</div>



<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacstype");
	optionValue = makeDropDownList( mappedItems , sv.sacstype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacstype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.sacstype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.sacstype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='sacstype' type='list' style="width:140px;"
<% 
	if((new Byte((sv.sacstype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.sacstype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.sacstype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("G/L Account Key")%>
</div>



<br/>

<%	
	longValue = sv.glact.getFormData();  
%>

<% 
	if((new Byte((sv.glact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='glact' 
type='text' 
value='<%=sv.glact.getFormData()%>' 
maxLength='<%=sv.glact.getLength()%>' 
size='<%=sv.glact.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(glact)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.glact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.glact).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('glact')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.glact).getColor()== null  ? 
"input_cell" :  (sv.glact).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('glact')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}longValue = null;} %>


</td></tr>

<tr style='height:22px;'><td width='251'>
<br>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("DB/CR (+/-)")%>
</div>



<br/>
<!-- ILIFe-2555 started by vjain60 -->
<%	
	if ((new Byte((sv.debcred).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("-")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Credit");
	}
	if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("+")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Debit");
	}
	 
%>
<!-- ILIFe-2555 ended by vjain60 -->

<% 
				if((new Byte((sv.debcred).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
				%> 
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
				"blank_cell" : "output_cell" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	
				<%} %>
				</div>
				
				<%
				longValue = null;
				%>

				<% }else { %>

<% if("red".equals((sv.debcred).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='debcred' style="width:140px;" 	
					onFocus='doFocus(this)'
					onHelp='return fieldHelp(debcred)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.debcred).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.debcred).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
<!-- ILIFe-2555 started by vjain60 -->
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="-"<% if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("-")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Credit")%></option>
<option value="+"<% if(((sv.debcred.getFormData()).toString()).trim().equalsIgnoreCase("+")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Debit")%></option>
<!-- ILIFe-2555 ended by vjain60 -->		

</select>
<% if("red".equals((sv.debcred).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>

</td>

<td width='251'></td><td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<br>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Details Required")%>
</div>



<br/>

<input type='checkbox' name='bankreq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(bankreq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.bankreq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.bankreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.bankreq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('bankreq')"/>

<input type='checkbox' name='bankreq' value=' ' 

<% if(!(sv.bankreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('bankreq')"/>

</td>

<td width='251'>
<br>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payee Details Required")%>
</div>



<br/>

<input type='checkbox' name='payeereq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(payeereq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.payeereq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.payeereq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.payeereq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('payeereq')"/>

<input type='checkbox' name='payeereq' value=' ' 

<% if(!(sv.payeereq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('payeereq')"/>

</td>

<td width='251'>
<br>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Details Required")%>
</div>



<br/>

<input type='checkbox' name='contreq' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(contreq)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.contreq).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.contreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.contreq).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('contreq')"/>

<input type='checkbox' name='contreq' value=' ' 

<% if(!(sv.contreq).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('contreq')"/>

</td></tr>

<tr style='height:22px;'><td width='251'>
<br>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%>
</div>



<br/>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("reqntype");
	optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.reqntype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

<% }else {%> 

<% if("red".equals((sv.reqntype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='reqntype' type='list' style="width:140px;"
<% 
	if((new Byte((sv.reqntype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.reqntype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.reqntype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>

</tr></table></div><br/></div>
 --%>

<%@ include file="/POLACommon2NEW.jsp"%>

