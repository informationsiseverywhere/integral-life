<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SR691";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr691ScreenVars sv = (Sr691ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = new StringData("Company ");%>
	<%StringData generatedText3 = new StringData("Table ");%>
	<%StringData generatedText4 = new StringData("Item ");%>
	<%StringData generatedText5 = new StringData("Period for Tax to be Imposed  ");%>
	<%StringData generatedText6 = new StringData("years since Policy Commencement");%>
	<%StringData generatedText7 = new StringData("Tax");%>
	<%StringData generatedText8 = new StringData("-");%>
	<%StringData generatedText9 = new StringData("Percentage  ");%>
	<%StringData generatedText10 = new StringData("% of Gross");%>
	<%StringData generatedText11 = new StringData("-");%>
	<%StringData generatedText12 = new StringData("Flat Rate   ");%>

<%{
		if (appVars.ind20.isOn()) {
			sv.tyearno.setReverse(BaseScreenData.REVERSED);
			sv.tyearno.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.tyearno.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.pcnt.setReverse(BaseScreenData.REVERSED);
			sv.pcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.pcnt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.flatrate.setReverse(BaseScreenData.REVERSED);
			sv.flatrate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.flatrate.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                 
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
   <div class="form-group" style="max-width:80px;">                             <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-2">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>
 </div>
<tr style='height:47px;'>
<td>
&nbsp;
</td>
</tr>
<div class="row">
<div class="col-md-6">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Period for Tax to be Imposed since Policy Commencement")%>
</label>
<%}%>
<div class="form-group" style="max-width:100px;">
<table><tr><td><%if ((new Byte((sv.tyearno).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.tyearno).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROS1);
			
	%>

<input name='tyearno' 
type='text'

<%if((sv.tyearno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.tyearno) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.tyearno);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.tyearno) %>'
	 <%}%>

size='<%= sv.tyearno.getLength()%>'
maxLength='<%= sv.tyearno.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(tyearno)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.tyearno).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.tyearno).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.tyearno).getColor()== null  ? 
			"input_cell" :  (sv.tyearno).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%></td><td><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Years")%>
</label>
<%}%></td></tr></table></div>


</div>
</div>
<tr style='height:22px;'>
<td>
&nbsp;
</td>
</tr>
<div class="row">
<div class="col-md-2">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Tax")%>
</label>
<%}%>
</div>
</div>
<div class="row">
<div class="col-md-3">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Percentage of Gross")%>
</label>
<%}%><div class="form-group" style="max-width:100px;"><table><tr><td>
<%if ((new Byte((sv.pcnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.pcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROS1VS2);
			
	%>

<input name='pcnt' 
type='text'

<%if((sv.pcnt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.pcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.pcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.pcnt) %>'
	 <%}%>
<%/*bug #ILIFE-1020 start*/%>
size='<%= sv.pcnt.getLength()+1%>'
maxLength='<%= sv.pcnt.getLength()+1%>' 
<%/*bug #ILIFE-1020 end*/%>
onFocus='doFocus(this)' onHelp='return fieldHelp(pcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.pcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.pcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.pcnt).getColor()== null  ? 
			"input_cell" :  (sv.pcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td><td> % 
<%}%></td></tr></table>
</div>
</div>
<div class="col-md-3">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Flat Rate")%>
</label>
<%}%><div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.flatrate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.flatrate).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S13ZEROS1VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.flatrate,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE_ONE);
	%>

<input name='flatrate' 
type='text'

<%if((sv.flatrate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.flatrate.getLength(), sv.flatrate.getScale(),3)%>'
maxLength='<%= sv.flatrate.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(flatrate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.flatrate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.flatrate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.flatrate).getColor()== null  ? 
			"input_cell" :  (sv.flatrate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%></div></div></div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
