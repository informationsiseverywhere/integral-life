

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sd5ho";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sd5hoScreenVars sv = (Sd5hoScreenVars) fw.getVariables();
%>

<%if(fw.getVariables().isScreenProtected()){%>
<style>
#dataTables-sd5ho td div div{
 border:none !important;
 background-color:transparent !important;
}
</style>
<% }%>

<div class="panel panel-default">
		<div class="panel-body">
		
		       <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-4"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-1"></div>
			     <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group" style="max-width:100px">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:60px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			     
			  </div>   
			  

<div class="row"></div>
   
<div class="row">
			<div class="col-md-9">
				<div class="">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " 
							id='dataTables-sd5ho' width='100%'>
							<thead>
							
								<tr class='info'>
								
					<th style="min-width:50px; text-align:center;">&nbsp;</th>
				
		            <th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Duration (Months)")%></th>
		         								
					<th style="min-width:100px; text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Refund Rate (%)")%></th>
					
					

	</tr> </thead><tbody>
<!-- ILIFE-2719 Life Cross Browser - Sprint 4 D2 : Task 2  ends-->		


				<tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon16, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate16, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon17, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate17, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon18, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate18, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon19, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate19, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>
                  
                  <tr class="tableRowTag">	
				<td class="tableDataTag" style="min-width:50px;" align="center">
				   <div class="form-group"><label>>=</label></div>
				</td>
				<td class="tableDataTag" style="min-width:100px;" align="center">
					<div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.remamon20, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                  <td class="tableDataTag" style="min-width:100px;" align="center">
                   <div class="form-group">
					  <%=smartHF.getHTMLVarExt(fw, sv.rate20, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
				    </div>
                  </td>
                 
                  </tr>

			
</tbody>
</table>			
                  
			   
			   </div>
			   </div> 
			   </div>
			   <div class="col-md-2"></div>
			   
			   </div>    
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
<script>

$(document).ready(function() {
	$('#dataTables-sd5ho').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:false,
        info:false
  	});
	

})


</script>
