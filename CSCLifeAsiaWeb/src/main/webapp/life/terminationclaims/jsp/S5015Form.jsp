<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5015";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5015ScreenVars sv = (S5015ScreenVars) fw.getVariables();%>

<%if (sv.S5015screenWritten.gt(0)) {%>
	<%S5015screen.clearClassString(sv);%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText19)%>


<%}%>


<%if (sv.S5015protectWritten.gt(0)) {%>
	<%S5015protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (appVars.ind18.isOn()) {
	sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
}
%>

<%if (sv.S5015screenctlWritten.gt(0)) {%>
	<%S5015screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5015screensfl");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%sv.rstate.setClassString("");%>
<%	sv.rstate.appendClassString("string_fld");
	sv.rstate.appendClassString("output_txt");
	sv.rstate.appendClassString("highlight");
%>
	<%sv.pstate.setClassString("");%>
<%	sv.pstate.appendClassString("string_fld");
	sv.pstate.appendClassString("output_txt");
	sv.pstate.appendClassString("highlight");
%>
<%-- 	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%> --%>
	<!-- ILJ-49 start -->
					<%
					StringData generatedText4 = null;
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Date ");%>
					<%} else { %>
					<%generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
						<%} %>
                   <!-- ILJ-49 ends -->	
	<%sv.occdateDisp.setClassString("");%>
<%	sv.occdateDisp.appendClassString("string_fld");
	sv.occdateDisp.appendClassString("output_txt");
	sv.occdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.ownernum.setClassString("");%>
<%	sv.ownernum.appendClassString("string_fld");
	sv.ownernum.appendClassString("output_txt");
	sv.ownernum.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%sv.lifcnum.setClassString("");%>
<%	sv.lifcnum.appendClassString("string_fld");
	sv.lifcnum.appendClassString("output_txt");
	sv.lifcnum.appendClassString("highlight");
%>
	<%sv.linsname.setClassString("");%>
<%	sv.linsname.appendClassString("string_fld");
	sv.linsname.appendClassString("output_txt");
	sv.linsname.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%sv.jlifcnum.setClassString("");%>
<%	sv.jlifcnum.appendClassString("string_fld");
	sv.jlifcnum.appendClassString("output_txt");
	sv.jlifcnum.appendClassString("highlight");
%>
	<%sv.jlinsname.setClassString("");%>
<%	sv.jlinsname.appendClassString("string_fld");
	sv.jlinsname.appendClassString("output_txt");
	sv.jlinsname.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Suffix");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Covr");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

	
<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText2)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-6">
		       		<div class="form-group">
		       		<div class="input-group" style="max-width: 420px;">
					<%=smartHF.getHTMLVarExt(fw, sv.chdrnum,0 ,100)%>
					<%=smartHF.getHTMLSpaceVar(fw, sv.cnttype)%>
					<%=smartHF.getHTMLF4NSVarExt(fw, sv.cnttype)%>
					<%=smartHF.getHTMLVarExt(fw, sv.ctypedes,0,400)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText3)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLVarExt(fw, sv.rstate,0,100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.pstate,0,300)%>
		       		</div>
		       		</div>
		       	</div>
		       	<div class="col-md-1">
		       	</div>
	
				<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText4)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLSpaceVar(fw, sv.occdateDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.occdateDisp)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText5)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-8">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 500px;">
					<%=smartHF.getHTMLVarExt(fw, sv.cownnum,0,100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.ownernum,0,500)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText6)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-8">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 500px;">
					<%=smartHF.getHTMLVarExt(fw, sv.lifcnum,0,100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.linsname,0,500)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText7)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-8">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 500px;">
					<%=smartHF.getHTMLVarExt(fw, sv.jlifcnum,0,100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.jlinsname,0,500)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText8)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLSpaceVar(fw, sv.ptdateDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.ptdateDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText9)%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLSpaceVar(fw, sv.btdateDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.btdateDisp)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>

		    
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5015'>	
			    	 	<thead>
			    	 	<tr class='info'>									
							<%=smartHF.getLit(0, 12, generatedText10)%>
							<%=smartHF.getLit(0, 20, generatedText17)%>
							<%=smartHF.getLit(0, 28, generatedText18)%>
							<%=smartHF.getLit(0, 40, generatedText11)%>
							<%=smartHF.getLit(1, 3, generatedText12)%>
							<%=smartHF.getLit(1, 11, generatedText13)%>
							<%=smartHF.getLit(1, 21, generatedText14)%>
							<%=smartHF.getLit(1, 28, generatedText15)%>
							<%=smartHF.getLit(1, 40, generatedText16)%>
						</tr>	
			         	</thead>
					      <tbody>
					      <%if (sv.S5015screensflWritten.gt(0)) {%>
								<%GeneralTable sflNew = fw.getTable("s5015screensfl");
								savedInds = appVars.saveAllInds();
								S5015screensfl.set1stScreenRow(sflNew, appVars, sv);
								double sflLine = 0.0;
								int doingLine = 0;
								int sflcols = 1;
								int linesPerCol = 9;
								String height = smartHF.fmty(9);
								smartHF.setSflLineOffset(13);
								%>
								<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(2)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
								<%while (S5015screensfl.hasMoreScreenRows(sflNew)) {%>
								<%sv.select.setClassString("");%>
							<%	sv.select.appendClassString("string_fld");
								sv.select.appendClassString("input_txt");
								sv.select.appendClassString("highlight");
							%>
								<%sv.planSuffix.setClassString("");%>
							<%	sv.planSuffix.appendClassString("num_fld");
								sv.planSuffix.appendClassString("output_txt");
								sv.planSuffix.appendClassString("highlight");
							%>
								<%sv.stycvr.setClassString("");%>
							<%	sv.stycvr.appendClassString("string_fld");
								sv.stycvr.appendClassString("output_txt");
								sv.stycvr.appendClassString("highlight");
							%>
								<%sv.pstatcode.setClassString("");%>
							<%	sv.pstatcode.appendClassString("string_fld");
								sv.pstatcode.appendClassString("output_txt");
								sv.pstatcode.appendClassString("highlight");
							%>
								<%sv.statdesc.setClassString("");%>
							<%	sv.statdesc.appendClassString("string_fld");
								sv.statdesc.appendClassString("output_txt");
								sv.statdesc.appendClassString("highlight");
							%>
								<%sv.screenIndicArea.setClassString("");%>
							
								<%
							{
									if (appVars.ind01.isOn()) {
										sv.select.setEnabled(BaseScreenData.DISABLED);
									}
								}
							
								%>
							
									<%=smartHF.getTableHTMLVarQual(sflLine, 3, sflNew, sv.select)%>
									<%=smartHF.getTableHTMLVarQual(sflLine, 12, sflNew, sv.planSuffix)%>
									<%=smartHF.getTableHTMLVarQual(sflLine, 22, sflNew, sv.stycvr)%>
									<%=smartHF.getTableHTMLVarQual(sflLine, 29, sflNew, sv.pstatcode)%>
									<%=smartHF.getTableHTMLVarQual(sflLine, 40, sflNew, sv.statdesc)%>
							
									<%sflLine += 1;
									doingLine++;
									if (doingLine % linesPerCol == 0 && sflcols > 1) {
										sflLine = 0.0;
									}
									S5015screensfl.setNextScreenRow(sflNew, appVars, sv);
								}%>
								</div>
								<%appVars.restoreAllInds(savedInds);%>
							
							
							<%}%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
<br><br>
<%}%>
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
