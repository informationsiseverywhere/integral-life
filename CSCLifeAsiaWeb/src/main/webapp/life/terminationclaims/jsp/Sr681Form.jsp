

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR681";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr681ScreenVars sv = (Sr681ScreenVars) fw.getVariables();%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind05.isOn()) {
			sv.slt.setReverse(BaseScreenData.REVERSED);
			sv.slt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.slt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Relation");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>

<%{
		if (appVars.ind13.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
						<%}%></label>
		       		<!-- <div class="input-group"> -->
		       		<table><tr><td style="padding-left: 1px;" >
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</td><td style="padding-left: 1px;" >
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					</td><td style="padding-left: 1px;" >
					<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 800px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					  </td></tr>
					  </table>
		       		<!-- </div> -->
		       		</div>
		       	</div>
		    </div>
		    
		    <%
			GeneralTable sfl = fw.getTable("sr681screensfl");
			%>
		
		
		
		<style> /* ILIFE-8090 */
		.text-center
		{
		position:relative !important;
		top:-17px !important;
		}
		</style>
		<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sr681'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center> </th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client")%></center> </th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Relation")%></center> </th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Name")%></center> </th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
							Sr681screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (Sr681screensfl.hasMoreScreenRows(sfl)) {	
						 %>
						
							<tr class="tableRowTag"   id='<%="tablerow"+count%>' >
												
												
												<%if((new Byte((sv.slt).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
								    									<td class="tableDataTag tableDataTagFixed" style="max-width:40px;" 
											<%if((sv.slt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																									
														
																			
																			
											
											 					 
											 <input type="checkbox" 
												 value='<%= sv.slt.getFormData() %>' 
												 onFocus='doFocus(this)' onHelp='return fieldHelp("sr681screensfl" + "." +
												 "slt")' onKeyUp='return checkMaxLength(this)' 
												 name='sr681screensfl.slt_R<%=count%>'
												 id='sr681screensfl.slt_R<%=count%>'
												 onClick="selectedRow('sr681screensfl.slt_R<%=count%>')"
												 class="UICheck"
											 />
											 
											 					
											
																	
															</td>
								<%}else{%>
																		<td class="tableDataTag tableDataTagFixed" style="width:40px;" >
											</td>														
																
											<%}%>
														<%if((new Byte((sv.clntnum).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
								    									<td class="tableDataTag" style="width:80px;" 
											<%if((sv.clntnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
														
																	
															
																	
												<%= sv.clntnum.getFormData()%>
												
																				 
										
															</td>
								<%}else{%>
																		<td class="tableDataTag" style="width:80px;" >
																				
										    </td>
																
											<%}%>
														<%if((new Byte((sv.relation).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
								    									<td class="tableDataTag" style="width:80px;" 
											<%if((sv.relation).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
														
																	
															
																	
												<%= sv.relation.getFormData()%>
												
																				 
										
															</td>
								<%}else{%>
																		<td class="tableDataTag" style="width:300px;" >
																				
										    </td>
																
											<%}%>
														<%if((new Byte((sv.clntname).getInvisible()))
												.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
								    									<td class="tableDataTag" style="width:300px;" 
											<%if((sv.clntname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
														
																	
															
																	
												<%= sv.clntname.getFormData()%>
												
																				 
										
															</td>
								<%}else{%>
																		<td class="tableDataTag" style="width:300px;" >
																				
										    </td>
																
											<%}%>
															
							</tr>
						<!-- ILIFE-2678 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 4 End by snayeni --> 
							<%
							count = count + 1;
							Sr681screensfl
							.setNextScreenRow(sfl, appVars, sv);
							}
							%>
					      </tbody>
					</table>
					 <div id="load-more" class="col-md-offset-10 pull-right">
						    <a class="btn btn-info" href="#" style='width: 74px;'
						       onclick="doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("More")%>
						    </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

