

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6692";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6692ScreenVars sv = (S6692ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Associated Regular Payment Type      ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.rgpytype.setReverse(BaseScreenData.REVERSED);
			sv.rgpytype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.rgpytype.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		
		       <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-3"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-2"></div>
			     <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group" style="max-width:100px">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:60px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			     
			  </div>      
			  
			   <div class="row">	
                <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Associated Regular Payment Type")%></label>
			           <div class="input-group">  <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpytype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpytype");
	optionValue = makeDropDownList( mappedItems , sv.rgpytype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpytype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpytype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:300px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpytype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:202px;"> 
<%
} 
%>

<select name='rgpytype' type='list' style="width:200px;"
<% 
	if((new Byte((sv.rgpytype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="max-width:300px;"
<%
	}else if((new Byte((sv.rgpytype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="max-width:300px;"
<%
	}else { 
%>
	class = 'input_cell' style="max-width:300px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpytype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
			        </div></div>
			     </div>
			   </div>   
			   
			   </div>
			   </div>     


















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Associated Regular Payment Type")%>
</div>



<br/>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpytype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpytype");
	optionValue = makeDropDownList( mappedItems , sv.rgpytype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpytype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpytype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpytype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpytype' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpytype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpytype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpytype).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td></tr></table><br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>

