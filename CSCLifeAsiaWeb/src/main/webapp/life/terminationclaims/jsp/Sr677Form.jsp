<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR677";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr677ScreenVars sv = (Sr677ScreenVars) fw.getVariables();%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.relation.setReverse(BaseScreenData.REVERSED);
			sv.relation.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.relation.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.clntnum.setReverse(BaseScreenData.REVERSED);
			sv.clntnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.clntnum.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Relationship");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>

<%{
		if (appVars.ind13.isOn()) {
			sv.chdrnum.setReverse(BaseScreenData.REVERSED);
			sv.chdrnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.chdrnum.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	
				    		
				    		
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
</label>
<%}%>

<table>
<tr>
<td>


<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td>
<td>




<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:80px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td>
<td>




<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td>
</tr>
</table>
		
</div></div>
</div>

<br>

<div class="row">
			<div class="col-md-10">
               <div class="table-responsive">
	         <table  id='dataTables-sr677' class="table table-striped table-bordered table-hover" width="100%">
               <thead>
		
			        <tr class="info">
			        <th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Client No ")%></th>									
										
					<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Relationship")%></th>
										
					<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Name")%></th>
			     	 
		 	        </tr>
			 </thead>
			 

<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[3];
int totalTblWidth = 0;
int calculatedValue =0;

			//ILIFE-1527 STARTS by smahalaxmi															
if((resourceBundleHandler.gettingValueFromBundle("Client No ").length()*12) >= (sv.clntnum.getFormData()).length()*12+32+16 ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Client No ").length())*10;								
						} else {		
							calculatedValue = ((sv.clntnum.getFormData()).length()*11)+60+16;								
						}		
									totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
																			if((resourceBundleHandler.gettingValueFromBundle("Relationship").length()*12) >= (sv.relation.getFormData()).length()*12+32+16 ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Relationship").length())*11;								
						} else {		
							calculatedValue = ((sv.relation.getFormData()).length()*11)+32+16;								
						}		
									totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Name").length() >= (sv.clntname.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Name").length())*9;								
			} else {		
				calculatedValue = (sv.clntname.getFormData()).length()*9;								
			}
	//ILIFE-1527 ENDS					
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("sr677screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<tbody>
  <%

  
  
  
	Sr677screensfl.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	Map<String,Map<String,String>> relationMap = appVars.loadF4FieldsLong(new String[] {"relation"},sv,"E",baseModel);
	
	while (Sr677screensfl.hasMoreScreenRows(sfl)) {	


	%>

		<tr id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.clntnum).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    	<td 
					<%if((sv.clntnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> style="width:145px;">
					<div class="input-group" style="max-width:125px">
							<input name='<%="sr677screensfl" + "." +
			 "clntnum" + "_R" + count %>' id='<%="sr677screensfl" + "." +
			 "clntnum" + "_R" + count %>' 
			type='text' 
			value='<%= sv.clntnum.getFormData() %>'
			maxLength='<%=sv.clntnum.getLength()%>' 
			onFocus='doFocus(this)' onHelp='return fieldHelp(sr677screensfl.clntnum)' onKeyUp='return checkMaxLength(this)' 
			 style = "width: <%=sv.clntnum.getLength()*12%> px;"
			<% 
	if((new Byte((sv.clntnum).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0|| fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell">	

<%
	}else if((new Byte((sv.clntnum).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){

%>	
class="bold_cell" >	
					 <span class="input-group-btn">
<button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('<%="sr677screensfl" + "." +
"clntnum" + "_R" + count %>'));doAction('PFKEY04');">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	

<%
}else { 
%> 
			class = " <%=(sv.clntnum).getColor()== null  ? 
			"input_cell" :  
			(sv.clntnum).getColor().equals("red") ? 
			"input_cell red reverse" : 
			"input_cell" %>">
			
		 <span class="input-group-btn">
<button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('<%="sr677screensfl" + "." +
"clntnum" + "_R" + count %>'));doAction('PFKEY04');">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
	
<%}%>			 




		</div></td>													
								
						
		<%}else{%>
												<td >
					</td>														
										
					<%}%>
								<%if((new Byte((sv.relation).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td 
					<%if((sv.relation).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> style="width:150px;">									
								
													
																					
				      				     <%	
						mappedItems = (Map) relationMap.get("relation");
						optionValue = makeDropDownList( mappedItems , sv.relation,2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.relation.getFormData()).toString().trim());  
					%>
					<% if((new Byte((sv.relation).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:140px;">
					<%if(longValue!=null){ %> 
					   <%=XSSFilter.escapeHtml(longValue)%>
					 <%} %>
					</div>
					
					<%
					longValue = null;
					%>

					<% }else {%>
					<% if("red".equals((sv.relation).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>
					<select name='<%="sr677screensfl.relation_R"+count%>' id='<%="sr677screensfl.relation_R"+count%>' type='list' style="max-width:180px;"
					
					class = 'input_cell'
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.relation).getColor())){
					%>
					</div>
					<%
					} 
					%>
				    <%
						} 
					%>
				    
											
									</td>
		<%}else{%>
												<td >
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.clntname).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td 
					<%if((sv.clntname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> style="width:600px;" >									
								
											
									
											
						<%= sv.clntname.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td >
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr677screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%> </tbody>
		</table>
		</div></div></div>





</div></div> 


<script>
$(document).ready(function() {
	$('#dataTables-sr677').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '420px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>


<%@ include file="/POLACommon2NEW.jsp"%>
