

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SA511";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sa511ScreenVars sv = (Sa511ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind01.isOn()) {
		sv.select.setReverse(BaseScreenData.REVERSED);
	}
	if (appVars.ind02.isOn()) {
		sv.select.setEnabled(BaseScreenData.DISABLED);
	}
	
	if (appVars.ind13.isOn()) {
		sv.select.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	if (appVars.ind01.isOn()) {
		sv.select.setReverse(BaseScreenData.REVERSED);
		sv.select.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.select.setHighLight(BaseScreenData.BOLD);
	}
		if (appVars.ind05.isOn()) {
			sv.notifnum.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.longdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.notifcdate.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.notiflupdate.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.ntfstat.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.userid.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assurd")%></label>
					<table>
						<tr>
							<td>
								<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
							<td>
								<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 40px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>
			
			<!-- Client Status -->
			<div class="col-md-2">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client Status")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.cltstat)%></div>
				</div>
			</div>
			
		</div>

			<%
			GeneralTable sfl = fw.getTable("sa511screensfl");
			%>
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sa511' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%> </th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Notification Number")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Incident Type")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Notification Creation Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Notification Latest Update Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Notification Status")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					     	 <%
			String backgroundcolor="#FFFFFF";
								Sa511screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sa511screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							<%
							{
									if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
								}
								if (appVars.ind02.isOn()) {
									sv.select.setEnabled(BaseScreenData.DISABLED);
								}
								
								if (appVars.ind13.isOn()) {
									sv.select.setInvisibility(BaseScreenData.INVISIBLE);
								}
								
								if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
									sv.select.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind01.isOn()) {
									sv.select.setHighLight(BaseScreenData.BOLD);
								}
									if (appVars.ind05.isOn()) {
										sv.notifnum.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind06.isOn()) {
										sv.longdesc.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind07.isOn()) {
										sv.notifcdate.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind08.isOn()) {
										sv.notiflupdate.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind09.isOn()) {
										sv.ntfstat.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind10.isOn()) {
										sv.userid.setInvisibility(BaseScreenData.INVISIBLE);
									}
								}
					
							%>

				<tr style="background:<%= backgroundcolor%>;" class="tableRowTag" id='<%="tablerow"+count%>' >
					<td  

						<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="center" <%}%> >
																			
											
										
												
						
					 <% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){%>	 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sa511screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sa511screensfl.select_R<%=count%>'
						 id='sa511screensfl.select_R<%=count%>'
						 onClick="selectedRow('sa511screensfl.select_R<%=count%>')"
						 class="UICheck"
						 
						 
				disabled="disabled"
						
						
					 />
					 <%}else{ %>
					 		<input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sa511screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sa511screensfl.select_R<%=count%>'
						 id='sa511screensfl.select_R<%=count%>'
						 onClick="selectedRow('sa511screensfl.select_R<%=count%>')"
						 class="UICheck"
						 />		
					
											
									<%}%>

						</td>

					    <td style="min-width: 90px;" 
						<%if((sv.notifnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
					<%-- <a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sa511screensfl" + "." +
						      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'>	 --%>							
							<%= sv.notifnum.getFormData()%>
						 </td>
						 
					    <td  style="min-width: 70px;" 
						<%if((sv.longdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.longdesc.getFormData()%>
						</td>
						
					   <td  style="min-width: 90px;" 
						<%if((sv.notifcdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%> >									
						<%= sv.notifcdateDisp.getFormData()%>
						</td>
									
				    	<td  style="min-width: 90px;" 
						<%if((sv.notiflupdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%> >									
							<%= sv.notiflupdateDisp.getFormData()%>
						 </td>
						 
		  				<td 
						<%if((sv.ntfstat).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.ntfstat.getFormData()%>
						</td>
						
						<td  style="min-width: 90px;" 
						<%if((sv.userid).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.userid.getFormData()%>
							<!-- IF -->
						</td>
					
					</tr>
				
					<%
	if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
							backgroundcolor="#ededed";
							}else{
							backgroundcolor="#FFFFFF";
							}
					count = count + 1;
					Sa511screensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
<script>

	$(document).ready(function() {
    	$('#dataTables-sa511').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollX: true,
        	scrollCollapse:true,
        	moreBtn: true,
        	
      	});
    	fixedColumns: true

    });
</script>   


<%@ include file="/POLACommon2NEW.jsp"%>
