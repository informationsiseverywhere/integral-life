

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR686";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr686ScreenVars sv = (Sr686ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Waiver Code      ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.waivercode.setReverse(BaseScreenData.REVERSED);
			sv.waivercode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.waivercode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
<div class="col-md-1">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                              <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
    </div>
  </div>
  <div class="col-md-3"></div>
<div class="col-md-2">
	                 
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
   <div class="form-group" style="max-width:80px;">                             <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
</div>
</div>
<div class="col-md-2"></div>
<div class="col-md-2">
	<div class="form-group">
             <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
              <div class="input-group">		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			   </div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    </div>
  </div>
 </div>
 </div>

<div class="row">
<div class="col-md-2">
	                   <div class="form-group">
                              <label><%=resourceBundleHandler.gettingValueFromBundle("Waiver code")%></label>


<input name='waivercode' 
type='text'

<%

		formatValue = (sv.waivercode.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.waivercode.getLength()%>'
maxLength='<%= sv.waivercode.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(waivercode)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.waivercode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.waivercode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.waivercode).getColor()== null  ? 
			"input_cell" :  (sv.waivercode).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></div>
</div>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

