

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5698";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	S5698ScreenVars sv = (S5698ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		if (appVars.ind50.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	
		
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract no ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status ");
%>
<%
	 StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD "); 
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to-date ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Bill-to-date ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"---------------------------- List of Lives -----------------------------------");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/L");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Name");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Status");
%>
<%
	appVars.rollup(new int[]{93});
%>

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no")%></label>
					<div class="input-group">

						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	

					<%-- <%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%> --%>


					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%-- <%
						}
					%> --%>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<div class="input-group">

						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						<%
							if ((new Byte((sv.ownernum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ownernum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownernum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownernum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div style="width: 150px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 150px">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>

					<%
						if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group" style="width: 150px">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bill-to-date")%></label>
					<%
						if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div style="width: 150px;"
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("List of Lives")%></label>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5698'>
						<%
							GeneralTable sfl = fw.getTable("s5698screensfl");
						%>
						<thead align="center">

							<tr class='info'>

								<th><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Joint Life No")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Name")%></th>
								<th><%=resourceBundleHandler.gettingValueFromBundle("Status")%></th>

							</tr>
						</thead>


						<tbody>
							<%
								S5698screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5698screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr class="tableRowTag" id='<%="tablerow" + count%>'>
								<%
									if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td><input type="checkbox"
									value='<%=sv.select.getFormData()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp("s5698screensfl" + "." +
						 "select")'
									onKeyUp='return checkMaxLength(this)'
									name='s5698screensfl.select_R<%=count%>'
									id='s5698screensfl.select_R<%=count%>'
									onClick="selectedRow('s5698screensfl.select_R<%=count%>')"
									class="UICheck" /></td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width: 100px;"
									<%if ((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.life.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width: 100px;"
									<%if ((sv.jlife).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.jlife.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width: 100px;"
									<%if ((sv.lifcnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.lifcnum.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width: 200px;"
									<%if ((sv.lifename).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.lifename.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.stdescsh).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td class="tableDataTag" style="width: 100px;"
									<%if ((sv.stdescsh).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.stdescsh.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td></td>

								<%
									}
								%>

							</tr>
							<%
								count = count + 1;
									S5698screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-s5698').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			info : false,
			paging : true
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

