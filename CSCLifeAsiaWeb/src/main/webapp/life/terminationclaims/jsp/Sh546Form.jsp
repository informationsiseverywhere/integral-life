<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SH546";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh546ScreenVars sv = (Sh546ScreenVars) fw.getVariables();%>

<%if (sv.Sh546screenWritten.gt(0)) {%>
	<%Sh546screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Unit ");%>
	<%sv.unit.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"5");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"6");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"7");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"8");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"9");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"10");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"0+");%>
	<%sv.insprm01.setClassString("");%>
	<%sv.insprm02.setClassString("");%>
	<%sv.insprm03.setClassString("");%>
	<%sv.insprm04.setClassString("");%>
	<%sv.insprm05.setClassString("");%>
	<%sv.insprm06.setClassString("");%>
	<%sv.insprm07.setClassString("");%>
	<%sv.insprm08.setClassString("");%>
	<%sv.insprm09.setClassString("");%>
	<%sv.insprm10.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"10+");%>
	<%sv.insprm11.setClassString("");%>
	<%sv.insprm12.setClassString("");%>
	<%sv.insprm13.setClassString("");%>
	<%sv.insprm14.setClassString("");%>
	<%sv.insprm15.setClassString("");%>
	<%sv.insprm16.setClassString("");%>
	<%sv.insprm17.setClassString("");%>
	<%sv.insprm18.setClassString("");%>
	<%sv.insprm19.setClassString("");%>
	<%sv.insprm20.setClassString("");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"20+");%>
	<%sv.insprm21.setClassString("");%>
	<%sv.insprm22.setClassString("");%>
	<%sv.insprm23.setClassString("");%>
	<%sv.insprm24.setClassString("");%>
	<%sv.insprm25.setClassString("");%>
	<%sv.insprm26.setClassString("");%>
	<%sv.insprm27.setClassString("");%>
	<%sv.insprm28.setClassString("");%>
	<%sv.insprm29.setClassString("");%>
	<%sv.insprm30.setClassString("");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"30+");%>
	<%sv.insprm31.setClassString("");%>
	<%sv.insprm32.setClassString("");%>
	<%sv.insprm33.setClassString("");%>
	<%sv.insprm34.setClassString("");%>
	<%sv.insprm35.setClassString("");%>
	<%sv.insprm36.setClassString("");%>
	<%sv.insprm37.setClassString("");%>
	<%sv.insprm38.setClassString("");%>
	<%sv.insprm39.setClassString("");%>
	<%sv.insprm40.setClassString("");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"40+");%>
	<%sv.insprm41.setClassString("");%>
	<%sv.insprm42.setClassString("");%>
	<%sv.insprm43.setClassString("");%>
	<%sv.insprm44.setClassString("");%>
	<%sv.insprm45.setClassString("");%>
	<%sv.insprm46.setClassString("");%>
	<%sv.insprm47.setClassString("");%>
	<%sv.insprm48.setClassString("");%>
	<%sv.insprm49.setClassString("");%>
	<%sv.insprm50.setClassString("");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"50+");%>
	<%sv.insprm51.setClassString("");%>
	<%sv.insprm52.setClassString("");%>
	<%sv.insprm53.setClassString("");%>
	<%sv.insprm54.setClassString("");%>
	<%sv.insprm55.setClassString("");%>
	<%sv.insprm56.setClassString("");%>
	<%sv.insprm57.setClassString("");%>
	<%sv.insprm58.setClassString("");%>
	<%sv.insprm59.setClassString("");%>
	<%sv.insprm60.setClassString("");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"60+");%>
	<%sv.insprm61.setClassString("");%>
	<%sv.insprm62.setClassString("");%>
	<%sv.insprm63.setClassString("");%>
	<%sv.insprm64.setClassString("");%>
	<%sv.insprm65.setClassString("");%>
	<%sv.insprm66.setClassString("");%>
	<%sv.insprm67.setClassString("");%>
	<%sv.insprm68.setClassString("");%>
	<%sv.insprm69.setClassString("");%>
	<%sv.insprm70.setClassString("");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"70+");%>
	<%sv.insprm71.setClassString("");%>
	<%sv.insprm72.setClassString("");%>
	<%sv.insprm73.setClassString("");%>
	<%sv.insprm74.setClassString("");%>
	<%sv.insprm75.setClassString("");%>
	<%sv.insprm76.setClassString("");%>
	<%sv.insprm77.setClassString("");%>
	<%sv.insprm78.setClassString("");%>
	<%sv.insprm79.setClassString("");%>
	<%sv.insprm80.setClassString("");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"80+");%>
	<%sv.insprm81.setClassString("");%>
	<%sv.insprm82.setClassString("");%>
	<%sv.insprm83.setClassString("");%>
	<%sv.insprm84.setClassString("");%>
	<%sv.insprm85.setClassString("");%>
	<%sv.insprm86.setClassString("");%>
	<%sv.insprm87.setClassString("");%>
	<%sv.insprm88.setClassString("");%>
	<%sv.insprm89.setClassString("");%>
	<%sv.insprm90.setClassString("");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"90+");%>
	<%sv.insprm91.setClassString("");%>
	<%sv.insprm92.setClassString("");%>
	<%sv.insprm93.setClassString("");%>
	<%sv.insprm94.setClassString("");%>
	<%sv.insprm95.setClassString("");%>
	<%sv.insprm96.setClassString("");%>
	<%sv.insprm97.setClassString("");%>
	<%sv.insprm98.setClassString("");%>
	<%sv.insprm99.setClassString("");%>
	<%sv.instpr01.setClassString("");%>
	<%StringData generatedText31 = new StringData("100+");%>
	<%sv.instpr02.setClassString("");%>
	<%sv.instpr03.setClassString("");%>
	<%sv.instpr04.setClassString("");%>
	<%sv.instpr05.setClassString("");%>
	<%sv.instpr06.setClassString("");%>
	<%sv.instpr07.setClassString("");%>
	<%sv.instpr08.setClassString("");%>
	<%sv.instpr09.setClassString("");%>
	<%sv.instpr10.setClassString("");%>
	<%sv.instpr11.setClassString("");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age 0 ");%>
	<%sv.insprem.setClassString("");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem Unit ");%>
	<%sv.premUnit.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind03.isOn()) {
			sv.unit.setReverse(BaseScreenData.REVERSED);
			sv.unit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.unit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.insprm01.setReverse(BaseScreenData.REVERSED);
			sv.insprm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.insprm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.insprm02.setReverse(BaseScreenData.REVERSED);
			sv.insprm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.insprm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.insprm03.setReverse(BaseScreenData.REVERSED);
			sv.insprm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.insprm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.insprm04.setReverse(BaseScreenData.REVERSED);
			sv.insprm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.insprm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.insprm05.setReverse(BaseScreenData.REVERSED);
			sv.insprm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.insprm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.insprm06.setReverse(BaseScreenData.REVERSED);
			sv.insprm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.insprm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.insprm07.setReverse(BaseScreenData.REVERSED);
			sv.insprm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.insprm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.insprm08.setReverse(BaseScreenData.REVERSED);
			sv.insprm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.insprm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.insprm09.setReverse(BaseScreenData.REVERSED);
			sv.insprm09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.insprm09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.insprm10.setReverse(BaseScreenData.REVERSED);
			sv.insprm10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.insprm10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.insprm11.setReverse(BaseScreenData.REVERSED);
			sv.insprm11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.insprm11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.insprm12.setReverse(BaseScreenData.REVERSED);
			sv.insprm12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.insprm12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.insprm13.setReverse(BaseScreenData.REVERSED);
			sv.insprm13.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.insprm13.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.insprm14.setReverse(BaseScreenData.REVERSED);
			sv.insprm14.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.insprm14.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.insprm15.setReverse(BaseScreenData.REVERSED);
			sv.insprm15.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.insprm15.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.insprm16.setReverse(BaseScreenData.REVERSED);
			sv.insprm16.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.insprm16.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.insprm17.setReverse(BaseScreenData.REVERSED);
			sv.insprm17.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.insprm17.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.insprm18.setReverse(BaseScreenData.REVERSED);
			sv.insprm18.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.insprm18.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.insprm19.setReverse(BaseScreenData.REVERSED);
			sv.insprm19.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.insprm19.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.insprm20.setReverse(BaseScreenData.REVERSED);
			sv.insprm20.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.insprm20.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.insprm21.setReverse(BaseScreenData.REVERSED);
			sv.insprm21.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.insprm21.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.insprm22.setReverse(BaseScreenData.REVERSED);
			sv.insprm22.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.insprm22.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.insprm23.setReverse(BaseScreenData.REVERSED);
			sv.insprm23.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.insprm23.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.insprm24.setReverse(BaseScreenData.REVERSED);
			sv.insprm24.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.insprm24.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.insprm25.setReverse(BaseScreenData.REVERSED);
			sv.insprm25.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.insprm25.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind49.isOn()) {
			sv.insprm26.setReverse(BaseScreenData.REVERSED);
			sv.insprm26.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind49.isOn()) {
			sv.insprm26.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.insprm27.setReverse(BaseScreenData.REVERSED);
			sv.insprm27.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.insprm27.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.insprm28.setReverse(BaseScreenData.REVERSED);
			sv.insprm28.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.insprm28.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.insprm29.setReverse(BaseScreenData.REVERSED);
			sv.insprm29.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.insprm29.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.insprm30.setReverse(BaseScreenData.REVERSED);
			sv.insprm30.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.insprm30.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.insprm31.setReverse(BaseScreenData.REVERSED);
			sv.insprm31.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.insprm31.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.insprm32.setReverse(BaseScreenData.REVERSED);
			sv.insprm32.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.insprm32.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.insprm33.setReverse(BaseScreenData.REVERSED);
			sv.insprm33.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.insprm33.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.insprm34.setReverse(BaseScreenData.REVERSED);
			sv.insprm34.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.insprm34.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind58.isOn()) {
			sv.insprm35.setReverse(BaseScreenData.REVERSED);
			sv.insprm35.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind58.isOn()) {
			sv.insprm35.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.insprm36.setReverse(BaseScreenData.REVERSED);
			sv.insprm36.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.insprm36.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.insprm37.setReverse(BaseScreenData.REVERSED);
			sv.insprm37.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.insprm37.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.insprm38.setReverse(BaseScreenData.REVERSED);
			sv.insprm38.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.insprm38.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.insprm39.setReverse(BaseScreenData.REVERSED);
			sv.insprm39.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.insprm39.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.insprm40.setReverse(BaseScreenData.REVERSED);
			sv.insprm40.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.insprm40.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.insprm41.setReverse(BaseScreenData.REVERSED);
			sv.insprm41.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.insprm41.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.insprm42.setReverse(BaseScreenData.REVERSED);
			sv.insprm42.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.insprm42.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.insprm43.setReverse(BaseScreenData.REVERSED);
			sv.insprm43.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.insprm43.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind67.isOn()) {
			sv.insprm44.setReverse(BaseScreenData.REVERSED);
			sv.insprm44.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind67.isOn()) {
			sv.insprm44.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind68.isOn()) {
			sv.insprm45.setReverse(BaseScreenData.REVERSED);
			sv.insprm45.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.insprm45.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.insprm46.setReverse(BaseScreenData.REVERSED);
			sv.insprm46.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.insprm46.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.insprm47.setReverse(BaseScreenData.REVERSED);
			sv.insprm47.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.insprm47.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.insprm48.setReverse(BaseScreenData.REVERSED);
			sv.insprm48.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.insprm48.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind72.isOn()) {
			sv.insprm49.setReverse(BaseScreenData.REVERSED);
			sv.insprm49.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.insprm49.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.insprm50.setReverse(BaseScreenData.REVERSED);
			sv.insprm50.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.insprm50.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind74.isOn()) {
			sv.insprm51.setReverse(BaseScreenData.REVERSED);
			sv.insprm51.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind74.isOn()) {
			sv.insprm51.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			sv.insprm52.setReverse(BaseScreenData.REVERSED);
			sv.insprm52.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.insprm52.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind76.isOn()) {
			sv.insprm53.setReverse(BaseScreenData.REVERSED);
			sv.insprm53.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind76.isOn()) {
			sv.insprm53.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			sv.insprm54.setReverse(BaseScreenData.REVERSED);
			sv.insprm54.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.insprm54.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind78.isOn()) {
			sv.insprm55.setReverse(BaseScreenData.REVERSED);
			sv.insprm55.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.insprm55.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind79.isOn()) {
			sv.insprm56.setReverse(BaseScreenData.REVERSED);
			sv.insprm56.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.insprm56.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind80.isOn()) {
			sv.insprm57.setReverse(BaseScreenData.REVERSED);
			sv.insprm57.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.insprm57.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.insprm58.setReverse(BaseScreenData.REVERSED);
			sv.insprm58.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.insprm58.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind82.isOn()) {
			sv.insprm59.setReverse(BaseScreenData.REVERSED);
			sv.insprm59.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.insprm59.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind83.isOn()) {
			sv.insprm60.setReverse(BaseScreenData.REVERSED);
			sv.insprm60.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind83.isOn()) {
			sv.insprm60.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm61.setReverse(BaseScreenData.REVERSED);
			sv.insprm61.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm61.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm62.setReverse(BaseScreenData.REVERSED);
			sv.insprm62.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm62.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm63.setReverse(BaseScreenData.REVERSED);
			sv.insprm63.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm63.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm64.setReverse(BaseScreenData.REVERSED);
			sv.insprm64.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm64.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm65.setReverse(BaseScreenData.REVERSED);
			sv.insprm65.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm65.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm66.setReverse(BaseScreenData.REVERSED);
			sv.insprm66.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm66.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm67.setReverse(BaseScreenData.REVERSED);
			sv.insprm67.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm67.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm68.setReverse(BaseScreenData.REVERSED);
			sv.insprm68.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm68.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind84.isOn()) {
			sv.insprm69.setReverse(BaseScreenData.REVERSED);
			sv.insprm69.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind84.isOn()) {
			sv.insprm69.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm70.setReverse(BaseScreenData.REVERSED);
			sv.insprm70.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm70.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm71.setReverse(BaseScreenData.REVERSED);
			sv.insprm71.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm71.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm72.setReverse(BaseScreenData.REVERSED);
			sv.insprm72.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm72.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm73.setReverse(BaseScreenData.REVERSED);
			sv.insprm73.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm73.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm74.setReverse(BaseScreenData.REVERSED);
			sv.insprm74.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm74.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm75.setReverse(BaseScreenData.REVERSED);
			sv.insprm75.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm75.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm76.setReverse(BaseScreenData.REVERSED);
			sv.insprm76.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm76.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm77.setReverse(BaseScreenData.REVERSED);
			sv.insprm77.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm77.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm78.setReverse(BaseScreenData.REVERSED);
			sv.insprm78.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm78.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm79.setReverse(BaseScreenData.REVERSED);
			sv.insprm79.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm79.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind85.isOn()) {
			sv.insprm80.setReverse(BaseScreenData.REVERSED);
			sv.insprm80.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind85.isOn()) {
			sv.insprm80.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm81.setReverse(BaseScreenData.REVERSED);
			sv.insprm81.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm81.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm82.setReverse(BaseScreenData.REVERSED);
			sv.insprm82.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm82.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm83.setReverse(BaseScreenData.REVERSED);
			sv.insprm83.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm83.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm84.setReverse(BaseScreenData.REVERSED);
			sv.insprm84.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm84.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm85.setReverse(BaseScreenData.REVERSED);
			sv.insprm85.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm85.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm86.setReverse(BaseScreenData.REVERSED);
			sv.insprm86.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm86.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm87.setReverse(BaseScreenData.REVERSED);
			sv.insprm87.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm87.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm88.setReverse(BaseScreenData.REVERSED);
			sv.insprm88.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm88.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm89.setReverse(BaseScreenData.REVERSED);
			sv.insprm89.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm89.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind86.isOn()) {
			sv.insprm90.setReverse(BaseScreenData.REVERSED);
			sv.insprm90.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind86.isOn()) {
			sv.insprm90.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm91.setReverse(BaseScreenData.REVERSED);
			sv.insprm91.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm91.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm92.setReverse(BaseScreenData.REVERSED);
			sv.insprm92.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm92.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm93.setReverse(BaseScreenData.REVERSED);
			sv.insprm93.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm93.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm94.setReverse(BaseScreenData.REVERSED);
			sv.insprm94.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm94.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm95.setReverse(BaseScreenData.REVERSED);
			sv.insprm95.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm95.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm96.setReverse(BaseScreenData.REVERSED);
			sv.insprm96.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm96.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm97.setReverse(BaseScreenData.REVERSED);
			sv.insprm97.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm97.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm98.setReverse(BaseScreenData.REVERSED);
			sv.insprm98.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm98.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprm99.setReverse(BaseScreenData.REVERSED);
			sv.insprm99.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprm99.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr01.setReverse(BaseScreenData.REVERSED);
			sv.instpr01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.insprem.setReverse(BaseScreenData.REVERSED);
			sv.insprem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.insprem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind88.isOn()) {
			sv.premUnit.setReverse(BaseScreenData.REVERSED);
			sv.premUnit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind88.isOn()) {
			sv.premUnit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr02.setReverse(BaseScreenData.REVERSED);
			sv.instpr02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr03.setReverse(BaseScreenData.REVERSED);
			sv.instpr03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr04.setReverse(BaseScreenData.REVERSED);
			sv.instpr04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr05.setReverse(BaseScreenData.REVERSED);
			sv.instpr05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr06.setReverse(BaseScreenData.REVERSED);
			sv.instpr06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr07.setReverse(BaseScreenData.REVERSED);
			sv.instpr07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr08.setReverse(BaseScreenData.REVERSED);
			sv.instpr08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr09.setReverse(BaseScreenData.REVERSED);
			sv.instpr09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr10.setReverse(BaseScreenData.REVERSED);
			sv.instpr10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind87.isOn()) {
			sv.instpr11.setReverse(BaseScreenData.REVERSED);
			sv.instpr11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind87.isOn()) {
			sv.instpr11.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	
	



	
	
	

	

	
	
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1 changed coordinates  -->
<%}%>

<%if (sv.Sh546protectWritten.gt(0)) {%>
	

	<%
{
	}

	%>

<% } %>


<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1   -->	 
<style>
		
		div[id*='instpr']{padding-right:1px !important} 
		
		div[id*='insprem']{padding-right:1px !important} 
		</style>
		
		
		
		<div class="panel panel-default">
        <div class="panel-body">
               <div class="row">
                      <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
	                        <div class="input-group">
	                       <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
			</div>
				</div>
				<div class="col-md-2">
				</div>
				
				<div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
	                       <div class="input-group">
	                       <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
				</div>
				</div>
				<div class="col-md-1">
				</div>
				<div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
	                       	<div class="input-group">
	                       	<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:600px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	                       	
	                       	</div>
	                       </div>
	                       </div>
					</div> <!--  end of row -->
					
					
				
					<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
				<div class="col-md-2">
				</div>
				<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Unit ")%></label>
						<div class="input-group">
					
	<%	
			qpsf = fw.getFieldXMLDef((sv.unit).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
	<input name='unit' 
type='text'

<%if((sv.unit).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.unit) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.unit);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.unit) %>'
	 <%}%>

size='<%= sv.unit.getLength()%>'
maxLength='<%= sv.unit.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(unit)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.unit).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.unit).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.unit).getColor()== null  ? 
			"input_cell" :  (sv.unit).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>


>
</div>
					</div>									
				</div>
				
			</div>   <!--  end of row -->
					
					<br/>
					 <div class="row">
                      <div class="col-md-1" >
	                    <div class="form-group">
	                      <label><%=resourceBundleHandler.gettingValueFromBundle("Age")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("1")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("2")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("3")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("4")%></label>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("5")%></label>
	                    </div>
	                  </div>
	                  <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("6")%></label>
	                    </div>
	                  </div>
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("7")%></label>
	                    </div>
	                   </div>
	                    <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("8")%></label>
	                    </div>
	                  </div>
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("9")%></label>
	                    </div>
	                  </div>
	                   <div class="col-md-1" style="max-width:100px;text-align: center;">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("10")%></label>
	                    </div>
	                  </div>
	                  
	                </div>  <!--  end of row -->
					
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("0+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1">
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("10+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm14, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm15, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm16, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm17, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm18, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm19, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm20, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                           <label><%=resourceBundleHandler.gettingValueFromBundle("20+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                     <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm21, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm22, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm23, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm24, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm25, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm26, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm27, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm28, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm29, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm30, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                          <div class="input-group" style="min-width: 70px;">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("30+")%></label>
                            </div>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm31, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm32, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm33, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm34, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm35, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm36, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm37, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm38, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm39, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm40, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("40+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm41, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm42, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm43, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm44, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm45, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm46, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm47, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm48, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm49, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm50, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("50+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm51, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm52, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm53, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm54, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm55, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm56, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm57, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm58, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm59, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm60, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("60+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm61, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm62, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm63, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm64, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm65, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm66, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm67, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm68, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm69, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm70, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("70+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm71, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm72, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm73, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm74, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm75, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm76, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm77, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm78, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm79, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm80, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("80+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm81, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm82, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm83, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm84, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm85, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm86, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm87, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm88, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm89, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm90, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("90+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprm91, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.insprm92, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm93, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.insprm94, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm95, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm96, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm97, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm98, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.insprm99, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
					
					<div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("100+")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.instpr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                        <%=smartHF.getHTMLVar(fw, sv.instpr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                        </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                     <%=smartHF.getHTMLVar(fw, sv.instpr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                     </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
	                  <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                       <%=smartHF.getHTMLVar(fw, sv.instpr11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
	                    </div>
	                  </div>
                 
	                </div>  <!-- end of row -->
	                
	                
					 <div class="row">
                      
	                  
	                   <div class="col-md-1">
                          <div class="form-group">
                            <label><%=resourceBundleHandler.gettingValueFromBundle("Age 0")%></label>
                         </div>
                       </div>
	                  
	                   <div class="col-md-1" >
	                    <div class="form-group">
	                    <div class="input-group" style="min-width: 70px;">
	                      <%=smartHF.getHTMLVar(fw, sv.insprem, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                      </div>
	                    </div>
	                  </div>
	                  </div>  <!-- end of row -->
					
					<div class="row">
                      <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Prem Unit")%></label>
	                       <div class="input-group">
	                       <%=smartHF.getHTMLVar(fw, sv.premUnit, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
	                       </div>
					 </div>
					  </div>
	                  </div>  <!-- end of row -->
	                  
						</div>
							</div>
		
		
		
		<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1   -->	 
<%@ include file="/POLACommon2NEW.jsp"%>
