

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6617";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6617ScreenVars sv = (S6617ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Calc. Method ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Accrual Method ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Cap.  Method ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Cap. Frequency ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate ");%>

<%{
		if (appVars.ind38.isOn()) {
			sv.intRate.setReverse(BaseScreenData.REVERSED);
			sv.intRate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.intRate.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.intCalcMethod.setReverse(BaseScreenData.REVERSED);
			sv.intCalcMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.intCalcMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.intCapMethod.setReverse(BaseScreenData.REVERSED);
			sv.intCapMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.intCapMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.intAccrualMethod.setReverse(BaseScreenData.REVERSED);
			sv.intAccrualMethod.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.intAccrualMethod.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.intCapFrequency.setReverse(BaseScreenData.REVERSED);
			sv.intCapFrequency.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.intCapFrequency.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div>
				</div>
			
			
		<!-- 	<div class="col-md-3"></div> -->
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				    </div>
				 </div>   	
				 
				 <!-- <div class="col-md-1"></div> -->
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group">	 -->
					<table><tr><td>
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>
		
				 <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div> <div class="col-md-4"></div> <div class="col-md-4"></div>
        </div>
        
        <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Calc. Method"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCalcMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCalcMethod");
	optionValue = makeDropDownList( mappedItems , sv.intCalcMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCalcMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCalcMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCalcMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCalcMethod' type='list' style="width:200px;"
<% 
	if((new Byte((sv.intCalcMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCalcMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCalcMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	</div>	
        	
        	<div class="col-md-4"></div>
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Accrual Method"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intAccrualMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intAccrualMethod");
	optionValue = makeDropDownList( mappedItems , sv.intAccrualMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intAccrualMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intAccrualMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intAccrualMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intAccrualMethod' type='list' style="width:200px;"
<% 
	if((new Byte((sv.intAccrualMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intAccrualMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intAccrualMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	</div>	
        </div>
        
        <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Cap.  Method"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCapMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCapMethod");
	optionValue = makeDropDownList( mappedItems , sv.intCapMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCapMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCapMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCapMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCapMethod' type='list' style="width:200px;"
<% 
	if((new Byte((sv.intCapMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCapMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCapMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        		</div>
        		
        		<div class="col-md-4"></div>
        		
        		<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Cap. Frequency"))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCapFrequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCapFrequency");
	optionValue = makeDropDownList( mappedItems , sv.intCapFrequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCapFrequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCapFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCapFrequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCapFrequency' type='list' style="width:200px;"
<% 
	if((new Byte((sv.intCapFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCapFrequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCapFrequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        		</div>
        	</div>								

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Rate"))%></label>
        				<div class="input-group">
        				<%	
			qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
	%>

<input name='intRate' 
type='text'

<%if((sv.intRate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intRate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
	 <%}%>

size='<%= sv.intRate.getLength()%>'
maxLength='<%= sv.intRate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intRate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intRate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intRate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intRate).getColor()== null  ? 
			"input_cell" :  (sv.intRate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        			</div>
        			</div>
        		</div>
        		 <div class="col-md-4"></div>
        		 
        		      
 <% if (sv.cmoth002flag.compareTo("N") != 0) { %> 

				 <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Deferred Interest Days"))%></label>
        				<div class="input-group">
        				<%	
			qpsf = fw.getFieldXMLDef((sv.defInterestDay).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name=defInterestDay 
type='text'
<%if((sv.defInterestDay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.defInterestDay) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.defInterestDay);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.defInterestDay) %>'
	 <%}%>

size='<%= sv.defInterestDay.getLength()%>'
maxLength='<%= sv.defInterestDay.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(defInterestDay)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.defInterestDay).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0  ||(((ScreenModel) fw).getVariables().isScreenProtected()))
	{ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.defInterestDay).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.defInterestDay).getColor()== null  ? 
			"input_cell" :  (sv.defInterestDay).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        			</div>
        			</div>
        		</div>
        		<%}%>	
        	</div>	
        	
        </div>
        </div>			




















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<!--  ILIFE 2671 starts-->
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>
<!--  ILIFE 2671 ends-->
<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



&nbsp;

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Interest Calc. Method")%>
</div>



<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCalcMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCalcMethod");
	optionValue = makeDropDownList( mappedItems , sv.intCalcMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCalcMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCalcMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCalcMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCalcMethod' type='list' style="width:140px;"
<% 
	if((new Byte((sv.intCalcMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCalcMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCalcMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td width='251'></td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Interest Accrual Method")%>
</div>



<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intAccrualMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intAccrualMethod");
	optionValue = makeDropDownList( mappedItems , sv.intAccrualMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intAccrualMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intAccrualMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intAccrualMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intAccrualMethod' type='list' style="width:140px;"
<% 
	if((new Byte((sv.intAccrualMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intAccrualMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intAccrualMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Interest Cap.  Method")%>
</div>



<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCapMethod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCapMethod");
	optionValue = makeDropDownList( mappedItems , sv.intCapMethod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCapMethod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCapMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCapMethod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCapMethod' type='list' style="width:140px;"
<% 
	if((new Byte((sv.intCapMethod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCapMethod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCapMethod).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td>

<td width='251'></td><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Interest Cap. Frequency")%>
</div>



<br/>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"intCapFrequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("intCapFrequency");
	optionValue = makeDropDownList( mappedItems , sv.intCapFrequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.intCapFrequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.intCapFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.intCapFrequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='intCapFrequency' type='list' style="width:140px;"
<% 
	if((new Byte((sv.intCapFrequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.intCapFrequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.intCapFrequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%>
</div>



<br/>


	<%	
			qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			
	%>

<input name='intRate' 
type='text'

<%if((sv.intRate).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intRate);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
	 <%}%>

size='<%= sv.intRate.getLength()%>'
maxLength='<%= sv.intRate.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intRate)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intRate).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intRate).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intRate).getColor()== null  ? 
			"input_cell" :  (sv.intRate).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table><br/></div> --%>

<%@ include file="/POLACommon2NEW.jsp"%>

