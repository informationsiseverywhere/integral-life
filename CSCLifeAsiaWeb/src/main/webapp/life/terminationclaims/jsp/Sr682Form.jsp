

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR682";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr682ScreenVars sv = (Sr682ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No    ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cmpnt  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life           ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner          ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD            ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status   ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date   ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date     ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CCY ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-----------------------------------------------------------------------------");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Seq No ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Receive Date ");%>
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Incur Date ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Type        ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Status        ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason            ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Life        ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(F4)");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Evidence            ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency           ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee             ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Destination Key     ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Amount  ");%>
	
<%-- generatedText for Claim Amount --%>
<%
	StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Adjustment Amount  ");
%>
<%-- generatedText for Actual Amount --%>
<%
	StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Net Claim Amount  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
	StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Adjustment Reason  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
	StringData generatedText46 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Adjustment Reason  Description  ");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Paid to Date  ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Currency  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Approval Date       ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date    ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Review Date         ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"First Payment Date");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Paid Date      ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Payment Date ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary Date    ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Final Payment Date");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cancellation Date   ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow Ups ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annuity Details ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Health Claim ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.cltype.setReverse(BaseScreenData.REVERSED);
			sv.cltype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.cltype.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind02.isOn()) {
			sv.cltype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.claimevd.setReverse(BaseScreenData.REVERSED);
			sv.claimevd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind33.isOn()) {
			sv.claimevd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.claimevd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.rgpymop.setReverse(BaseScreenData.REVERSED);
			sv.rgpymop.setColor(BaseScreenData.RED);
		}
		if (appVars.ind34.isOn()) {
			sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.rgpymop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.regpayfreq.setReverse(BaseScreenData.REVERSED);
			sv.regpayfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind35.isOn()) {
			sv.regpayfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.regpayfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.payclt.setReverse(BaseScreenData.REVERSED);
			sv.payclt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind36.isOn()) {
			sv.payclt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.payclt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.destkey.setReverse(BaseScreenData.REVERSED);
			sv.destkey.setColor(BaseScreenData.RED);
		}
		if (appVars.ind37.isOn()) {
			sv.destkey.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind11.isOn()) {
			sv.destkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.claimcur.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind75.isOn()) {
			sv.claimcur.setReverse(BaseScreenData.REVERSED);
			sv.claimcur.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind75.isOn()) {
			sv.claimcur.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.crtdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.crtdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.crtdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.crtdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.revdteDisp.setReverse(BaseScreenData.REVERSED);
			sv.revdteDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind41.isOn()) {
			sv.revdteDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind16.isOn()) {
			sv.revdteDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.firstPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.firstPaydateDisp.setReverse(BaseScreenData.REVERSED);
			sv.firstPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.firstPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.nextPaydateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind76.isOn()) {
			sv.nextPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind40.isOn()) {
			sv.nextPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind40.isOn()) {
			sv.nextPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.anvdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.anvdateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind42.isOn()) {
			sv.anvdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind18.isOn()) {
			sv.anvdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.finalPaydateDisp.setReverse(BaseScreenData.REVERSED);
			sv.finalPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind43.isOn()) {
			sv.finalPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind17.isOn()) {
			sv.finalPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind31.isOn()) {
			sv.cancelDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind70.isOn()) {
			sv.totalamt.setReverse(BaseScreenData.REVERSED);
			sv.totalamt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.totalamt.setInvisibility(BaseScreenData.INVISIBLE);
			sv.totalamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.msgclaim.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.anntind.setReverse(BaseScreenData.REVERSED);
			sv.anntind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind46.isOn()) {
			sv.anntind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind45.isOn()) {
			sv.anntind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.clamparty.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind51.isOn()) {
			sv.clamparty.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind50.isOn()) {
			sv.clamparty.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.clamparty.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.activeInd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind58.isOn()) {
			sv.activeInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind55.isOn()) {
			sv.activeInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.activeInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.recvdDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.recvdDateDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.recvdDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.recvdDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.incurdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.incurdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.incurdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.incurdtDisp.setHighLight(BaseScreenData.BOLD);
		}
//	if (appVars.ind12.isOn()) {
//		sv.pymt.setReverse(BaseScreenData.REVERSED);
//		sv.pymt.setColor(BaseScreenData.RED);
//	}
//	if (appVars.ind38.isOn()) {
//		sv.pymt.setEnabled(BaseScreenData.DISABLED);
//	}
	//=======================claim amount adjustment=========================
	if (appVars.ind44.isOn()) {
		sv.pymtAdj.setReverse(BaseScreenData.REVERSED);
		sv.pymtAdj.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind44.isOn()) {
		sv.pymtAdj.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind67.isOn()) {
		sv.pymtAdj.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind67.isOn()) {
		sv.pymtAdj.setInvisibility(BaseScreenData.INVISIBLE);
	}

	//=======================adjustamt amount=========================
	if (appVars.ind07.isOn()) {
		sv.adjustamt.setReverse(BaseScreenData.REVERSED);
		sv.adjustamt.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind07.isOn()) {
		sv.adjustamt.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind69.isOn()) {
		sv.adjustamt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind79.isOn()) {
		sv.adjustamt.setInvisibility(BaseScreenData.INVISIBLE);
	}

	//=======================Net Claim Amount=========================
	if (appVars.ind12.isOn()) {
		sv.netclaimamt.setReverse(BaseScreenData.REVERSED);
		sv.netclaimamt.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind12.isOn()) {
		sv.netclaimamt.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind68.isOn()) {
		sv.netclaimamt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind78.isOn()) {
		sv.netclaimamt.setInvisibility(BaseScreenData.INVISIBLE);
	}

	//=======================adjustamt Reason=========================
	if (appVars.ind08.isOn()) {
		sv.reasoncd.setReverse(BaseScreenData.REVERSED);
		sv.reasoncd.setColor(BaseScreenData.RED);
	}

	if (!appVars.ind08.isOn()) {
		sv.reasoncd.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind49.isOn()) {
		sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind77.isOn()) {
		sv.reasoncd.setInvisibility(BaseScreenData.INVISIBLE);
	}

	//=======================adjustamt Reason Description=========================
	if (appVars.ind10.isOn()) {
		sv.resndesc.setReverse(BaseScreenData.REVERSED);
		sv.resndesc.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind10.isOn()) {
		sv.resndesc.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind66.isOn()) {
		sv.resndesc.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind80.isOn()) {
		sv.resndesc.setInvisibility(BaseScreenData.INVISIBLE);
	}
	
	
	if (appVars.ind90.isOn()) {
		sv.itstdays.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind91.isOn()) {
		sv.itstdays.setInvisibility(BaseScreenData.INVISIBLE);
	}

	
	if (appVars.ind92.isOn()) {
		sv.itstrate.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind93.isOn()) {
		sv.itstrate.setInvisibility(BaseScreenData.INVISIBLE);
	}

	
	if (appVars.ind94.isOn()) {
		sv.itstamt.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind95.isOn()) {
		sv.itstamt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	//FWANG3         
    if (appVars.ind22.isOn()) {
        sv.investres.setReverse(BaseScreenData.REVERSED);
        sv.investres.setColor(BaseScreenData.RED);
    }
    if (!appVars.ind22.isOn()) {
        sv.investres.setHighLight(BaseScreenData.BOLD);
    }
    if (appVars.ind24.isOn()) {
        sv.claimnotes.setReverse(BaseScreenData.REVERSED);
        sv.claimnotes.setColor(BaseScreenData.RED);
    }
    if (!appVars.ind24.isOn()) {
        sv.claimnotes.setHighLight(BaseScreenData.BOLD);
    }
    if (appVars.ind52.isOn()) {
        sv.notifino.setReverse(BaseScreenData.REVERSED);
        sv.notifino.setColor(BaseScreenData.RED);
    }
    if (appVars.ind71.isOn()) {
        sv.notifino.setEnabled(BaseScreenData.DISABLED);
    }
    if (!appVars.ind52.isOn()) {
        sv.notifino.setHighLight(BaseScreenData.BOLD);
    }	
    if (appVars.ind81.isOn()) {
		sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
	}	
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					<%}%></label>
					<table>
					<tr>
					<td>
		       		
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					</td>
				<td style="padding-left:1px;">
				
				
				
				
				<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					</td>
					<td style="padding-left:1px;">
				
				
				
				
				
				<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 800px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				  </td>
				  </tr>
				  </table>
		       		
		       		</div>
		       	</div>
		       	
	           
	    	    <div class="col-md-4"> 
	    	     <% if (sv.showFlag.compareTo("Y") == 0) { %> 
		    		<div class="form-group">  	  
			    		<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<%
	                      if ((new Byte((sv.notifino).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
	                                      || fw.getVariables().isScreenProtected()) {
	                   %>
					<div class="input-group" style="width: 170px;">
						<%=smartHF.getHTMLVarExt(fw, sv.notifino)%>
					</div>
	                   <%
	                   } else {
	                   %>
					<div class="input-group" style="width: 170px;">
				    		<%=smartHF.getRichTextInputFieldLookup(fw, sv.notifino)%>
				    		<span class="input-group-btn">
			        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('notifino')); doAction('PFKEY04');">
			        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
			        			</button>
			      			</span>
		      		</div>
					<%
						}
					%>
					</div> <% } %> 
	    	   	</div>         
	    	   
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Cmpnt")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					
					
					
					
					<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 <!-- ILJ-48 Starts -->
			<% if (sv.iljCntDteFlag.compareTo("Y") == 0){ %>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>

                    <%
                        if (!((sv.riskcommdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 85px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
            <%} %>
			<!-- ILJ-48 End -->
		       	
		       	<div class="col-md-4"></div>
	            <% if (sv.showFlag.compareTo("Y") == 0) { %> 
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim No")%></label>
	                     <%
	                        if (!((sv.claimno.getFormData()).toString()).trim().equalsIgnoreCase("")) {
	
	                            if (longValue == null || longValue.equalsIgnoreCase("")) {
	                                formatValue = formatValue((sv.claimno.getFormData()).toString());
	                            } else {
	                                formatValue = formatValue(longValue);
	                            }
	
	                        } else {
	
	                            if (longValue == null || longValue.equalsIgnoreCase("")) {
	                                formatValue = formatValue((sv.claimno.getFormData()).toString());
	                            } else {
	                                formatValue = formatValue(longValue);
	                            }
	
	                        }
	                    %>
	                    <div style="width: 150px;"
	                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
	                        <%=XSSFilter.escapeHtml(formatValue)%>
	                    </div>
	                    <%
	                        longValue = null;
	                        formatValue = null;
	                    %>
	                </div>
	            </div>
	        	<% } %>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					<%}%>
		       		</label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				
				
				
				
				<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		      </div>
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%> </label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				
				
				
				
				<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("CCY")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				
				
				
				
				<%if ((new Byte((sv.currds).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.currds.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currds.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currds.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    <br/>
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText34).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Seq No")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText38).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Receive Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.recvdDateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.recvdDateDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.recvdDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					
					<% 
						if((new Byte((sv.recvdDateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					
					<%
						}else if((new Byte((sv.recvdDateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="recvdDateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp,(sv.recvdDateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
							
					<%
						}else { 
					%>
					
										
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="recvdDateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp,(sv.recvdDateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
										
					<%}longValue = null;}}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText39).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Incur Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.incurdtDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.incurdtDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.incurdtDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					  
					
					<% 
						if((new Byte((sv.incurdtDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					
					<%
						}else if((new Byte((sv.incurdtDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
				
					 
					  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="incurdtDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp,(sv.incurdtDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
						
					<%
						}else { 
					%>
					
					
					
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="incurdtDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp,(sv.incurdtDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
										
					<%} longValue = null;}}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rgpytypesd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.rgpytypesd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	       	
		       		<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Status")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rgpystat).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rgpystat.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpystat.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpystat.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				
				
				
				
				<%if ((new Byte((sv.statdsc).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.statdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.statdsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Reason")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.cltype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cltype"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cltype");
						optionValue = makeDropDownList( mappedItems , sv.cltype.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.cltype.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.cltype).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.cltype).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:200px;"> 
					<%
					} 
					%>
					
					<select name='cltype' type='list' style="min-width:200px;"
					<% 
						if((new Byte((sv.cltype).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.cltype).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.cltype).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Claim Life")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.clamparty).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

						<%	
							
							longValue = sv.clamparty.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.clamparty).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						<input name='clamparty' id='clamparty'
						type='text' 
						value='<%=sv.clamparty.getFormData()%>' 
						maxLength='<%=sv.clamparty.getLength()%>' 
						size='<%=sv.clamparty.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(clamparty)' onKeyUp='return checkMaxLength(this)'  
						
						<% 
							if((new Byte((sv.clamparty).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.clamparty).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						class="bold_cell" >
						 
						 <span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('clamparty')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						
												
						<%
							}else { 
						%>
						
						class = ' <%=(sv.clamparty).getColor()== null  ? 
						"input_cell" :  (sv.clamparty).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('clamparty')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						
												
						<%}longValue = null;}} %>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-1">
		       		</div>
		       		
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Evidence")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.claimevd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


					<input name='claimevd' 
					type='text'
					
					<%if((sv.claimevd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
					<%
					
							formatValue = (sv.claimevd.getFormData()).toString();
					
					%>
						value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.claimevd.getLength()%>'
					maxLength='<%= sv.claimevd.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(claimevd)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.claimevd).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.claimevd).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.claimevd).getColor()== null  ? 
								"input_cell" :  (sv.claimevd).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.rgpymop).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpymop"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("rgpymop");
						optionValue = makeDropDownList( mappedItems , sv.rgpymop.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.rgpymop).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.rgpymop).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:200px;"> 
					<%
					} 
					%>
					
					<select name='rgpymop' type='list' style="min-width:200px;"
					<% 
						if((new Byte((sv.rgpymop).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.rgpymop).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.rgpymop).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.regpayfreq).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayfreq"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("regpayfreq");
						optionValue = makeDropDownList( mappedItems , sv.regpayfreq.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.regpayfreq.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.regpayfreq).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.regpayfreq).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:150px;"> 
					<%
					} 
					%>
					
					<select name='regpayfreq' type='list' style="min-width:150px;"
					<% 
						if((new Byte((sv.regpayfreq).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.regpayfreq).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.regpayfreq).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Payee")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.payclt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

						<%	
							
							longValue = sv.payclt.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.payclt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						<input name='payclt' id='payclt'
						type='text' 
						value='<%=sv.payclt.getFormData()%>' 
						maxLength='<%=sv.payclt.getLength()%>' 
						size='<%=sv.payclt.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(payclt)' onKeyUp='return checkMaxLength(this)'  
						
						<% 
							if((new Byte((sv.payclt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>  
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.payclt).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						class="bold_cell" >
						
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
						 
												
						<%
							}else { 
						%>
						
						class = ' <%=(sv.payclt).getColor()== null  ? 
						"input_cell" :  (sv.payclt).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
												
											
						<%}longValue = null;}} %>
						
						
						
						
						
						
						<%if ((new Byte((sv.payenme).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Destination Key")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.destkey).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<input name='destkey' 
						type='text'
						
						<%if((sv.destkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.destkey.getFormData()).toString();
						
						%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.destkey.getLength()%>'
						maxLength='<%= sv.destkey.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(destkey)' onKeyUp='return checkMaxLength(this)'  
						
						
						<% 
							if((new Byte((sv.destkey).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.destkey).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.destkey).getColor()== null  ? 
									"input_cell" :  (sv.destkey).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((sv.pymtAdj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Original Claim Amount")%></label>
							<%}else{ %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></label>
							<%}%>
		       		<div class="input-group" style="min-width: 100px;">
		       		<%if ((new Byte((sv.clmamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%	
								qpsf = fw.getFieldXMLDef((sv.clmamt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.clmamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								
								if(!((sv.clmamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell">	
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" > &nbsp; </div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
						
					 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Total Paid to Date")%>
					<%}%></label>
		       		<div class="input-group" style="min-width: 100px;">
		       		<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.totamnt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.totamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.claimcur).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"claimcur"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("claimcur");
						optionValue = makeDropDownList( mappedItems , sv.claimcur.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.claimcur.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.claimcur).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.claimcur).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:200px;"> 
					<%
					} 
					%>
					
					<select name='claimcur' type='list' style="min-width:200px;"
					<% 
						if((new Byte((sv.claimcur).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.claimcur).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.claimcur).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Approval Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.aprvdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.aprvdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.aprvdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.aprvdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
							if ((new Byte((sv.crtdateDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							longValue = sv.crtdateDisp.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.crtdateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						  
						
						<% 
							if((new Byte((sv.crtdateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						
						
						<%
							}else if((new Byte((sv.crtdateDisp).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						
						  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="crtdateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp,(sv.crtdateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
											
						<%
							}else { 
						%>
						
												
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="crtdateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp,(sv.crtdateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
												
						<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Review Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
							if ((new Byte((sv.revdteDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							longValue = sv.revdteDisp.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.revdteDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						
						<% 
							if((new Byte((sv.revdteDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						
						<%
							}else if((new Byte((sv.revdteDisp).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						
						 
						  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="revdteDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp,(sv.revdteDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
												
						<%
							}else { 
						%>
						
						
						
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="revdteDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp,(sv.revdteDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
											
						<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
							if ((new Byte((sv.firstPaydateDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							longValue = sv.firstPaydateDisp.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.firstPaydateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
						
						
						<% 
							if((new Byte((sv.firstPaydateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						
						
						<%
							}else if((new Byte((sv.firstPaydateDisp).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						
						 
						  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="firstPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp,(sv.firstPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
												
						<%
							}else { 
						%>
						
					
						
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="firstPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp,(sv.firstPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
												
						<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Last Paid Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.lastPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.lastPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Next Payment Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.nextPaydateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.nextPaydateDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.nextPaydateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 100px;">  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					
					<% 
						if((new Byte((sv.nextPaydateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					<%
						}else if((new Byte((sv.nextPaydateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 
					  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="nextPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.nextPaydateDisp,(sv.nextPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
									
					<%
						}else { 
					%>
					
					
					
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="nextPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.nextPaydateDisp,(sv.nextPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
									
					<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%>
					<%}%>
		       		</label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.anvdateDisp).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						longValue = sv.anvdateDisp.getFormData();  
					%>
					
					<% 
						if((new Byte((sv.anvdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					<% }else {%> 
					
					
					<% 
						if((new Byte((sv.anvdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
					
					
					<%
						}else if((new Byte((sv.anvdateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
				
					 
					  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp,(sv.anvdateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
										
					<%
						}else { 
					%>
					
					
					
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp,(sv.anvdateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
          			
									
					<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 <div class="col-md-4">
		       	 	</div>
		       	 	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
							if ((new Byte((sv.finalPaydateDisp).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							longValue = sv.finalPaydateDisp.getFormData();  
						%>
						
						<% 
							if((new Byte((sv.finalPaydateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
						%>  
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=XSSFilter.escapeHtml(longValue)%>
							   		
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%> 
												
						<% 
							if((new Byte((sv.finalPaydateDisp).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
					
						
						<%
							}else if((new Byte((sv.finalPaydateDisp).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>	
						
						 
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp,(sv.finalPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          				</div>
												
						<%
							}else { 
						%>
						
												
						 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp" data-link-format="dd/mm/yyyy">
		                    <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp,(sv.finalPaydateDisp.getLength()))%>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          			</div>
											
						<%}longValue = null; }}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		
		    </div>
		    
		     <div class="row">
		     <div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Cancellation Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.cancelDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.cancelDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cancelDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cancelDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 <div class="col-md-4">
		       	 	</div>
		       	 	
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label>
		       		<%if ((new Byte((sv.msgclaim).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.msgclaim.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.msgclaim.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.msgclaim.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%></label>
				  
		       		<div class="input-group">
		       		
						<%if ((new Byte((sv.totalamt).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%	
									qpsf = fw.getFieldXMLDef((sv.totalamt).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.totalamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									
									if(!((sv.totalamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell">	
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" > &nbsp; </div>
								
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
							
						 <%}%>
		       		</div>
		       		</div>
		       	</div>
	
		    </div>
		    
		    
		     <div class="row">
			
			 <%if ((new Byte((sv.itstdays).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
			 <div class="col-md-4" >
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("No. of Days for Interest")%></label>
					 <div class="input-group" style="min-width:80px">
						<%
						
							qpsf = fw.getFieldXMLDef((sv.itstdays).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.itstdays,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.itstdays.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					 </div>
				 </div>
			 </div>
			 <%} %>
		
			  <%if ((new Byte((sv.itstrate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>		
			 <div class="col-md-4">
				 <div class="form-group" >
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					 <div class="input-group" style="min-width:80px">
					<%
						
							qpsf = fw.getFieldXMLDef((sv.itstrate).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.itstrate,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.itstrate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>	 </div>
			</div> </div> <%} %>
			 
			 		<%if ((new Byte((sv.itstamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
					<div class="col-md-4">
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Interest Amount")%></label>
					 <div class="input-group" style="min-width: 80px;">
							 
					<%
						
							qpsf = fw.getFieldXMLDef((sv.itstamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.itstamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.itstamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					 </div>
				 </div>
			 </div>
			 <%} %>
		 </div>


		 <div class="row">
			 <!-- CML-009 -->
			 <%if ((new Byte((sv.adjustamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
			 <div class="col-md-4" >
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Amount")%></label>
					 <div class="input-group">
						 <%
							 qpsf = fw.getFieldXMLDef((sv.adjustamt).getFieldName());
							 //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							 valueThis = smartHF.getPicFormatted(qpsf, sv.adjustamt,
									 COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
						 %>

						 <input name='adjustamt' type='text'
							 <%if ((sv.adjustamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
								style="text-align: right" <%}%> value='<%=valueThis%>'
							 <%if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=valueThis%>' <%}%>
								size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.adjustamt.getLength(), sv.adjustamt.getScale(), 3)%>'
								maxLength='<%=sv.adjustamt.getLength()%>'
								onFocus='doFocus(this),onFocusRemoveCommas(this)'
								onHelp='return fieldHelp(pymt)'
								onKeyUp='return checkMaxLength(this)'
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event,true);'
								onBlur='return doBlurNumberNew(event,true);'
							 <%if ((new Byte((sv.adjustamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell"
							 <%} else if ((new Byte((sv.adjustamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.adjustamt).getColor() == null ? "input_cell"
						: (sv.adjustamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							 <%}%>>
					 </div>
				 </div>
			 </div>
			 <%} %>
			 <%-- adjustment reason code --%>
			 <div class="col-md-4">
				 <div class="form-group" >
					  <%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Reason")%></label>
					 
					 <table><tr><td>
					 <%
						 fieldItem = appVars.loadF4FieldsLong(new String[] { "reasoncd" }, sv, "E", baseModel);
						 mappedItems = (Map) fieldItem.get("reasoncd");
						 optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 1, resourceBundleHandler);
						 longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
					 %>

					 <%
						 if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								 || (((ScreenModel) fw).getVariables().isScreenProtected())) {
					 %>
					 <div style="width: 150px;"
						  class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						 <%
							 if (longValue != null) {
						 %>

						 <%=sv.reasoncd%>

						 <%
							 }
						 %>
					 </div>

					 <%
						 longValue = null;
					 %>

					 <%
					 } else {
					 %>

					 <%
						 if ("red".equals((sv.reasoncd).getColor())) {
					 %>
					 <div
							 style="border: 1px; border-style: solid; border-color: #B55050; width: 215px;">
						 <%
							 }
						 %>

						 <select name='reasoncd' type='list' style="width: 220px;"
								 <%if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								 readonly="true" disabled class="output_cell" style="width:150px;"
								 <%} else if ((new Byte((sv.reasoncd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								 class="bold_cell" style="width:215px;" <%} else {%>
								 class='input_cell' style="width:215px;" <%}%> onchange="change()">
							 <%=optionValue%>
						 </select>
						 <%
							 if ("red".equals((sv.reasoncd).getColor())) {
						 %>
					 </div>

					 <%
						 }
					 %>

					 <%
						 }
					 %>
				 </td>
			 <td>
					 <%-- Adjustment Reason Description --%>
					 <input name='resndesc'
							type='text'

						 <%

								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasoncd");
								optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);
								formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());

						%>
						 <%if(formatValue==null) {
							formatValue="";
						}
						 %>



							value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

							size='50'
							maxLength='50'
							readonly="true"
							class="output_cell">


					 <%
						 longValue = null;
						 formatValue = null;
					 %>

			</td>  </tr></table>	<%} %> </div>
			 </div>
			 <%if ((new Byte((sv.netclaimamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
			 <%-- net claim amount --%>
			 <div class="col-md-4">
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Net Claim Amount")%></label>
					 <div class="input-group" style="min-width: 80px;">
						 <%
							 qpsf = fw.getFieldXMLDef((sv.netclaimamt).getFieldName());
							 //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							 formatValue = smartHF.getPicFormatted(qpsf,sv.netclaimamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							 if(!((sv.netclaimamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								 if(longValue == null || longValue.equalsIgnoreCase("")) {
									 formatValue = formatValue( formatValue );
								 } else {
									 formatValue = formatValue( longValue );
								 }
							 }

							 if(!formatValue.trim().equalsIgnoreCase("")) {
						 %>
						 <div class="output_cell">
							 <%=XSSFilter.escapeHtml(formatValue)%>
						 </div>
						 <%
						 } else {
						 %>

						 <div class="blank_cell" > </div>

						 <%
							 }
						 %>


						 <%
							 longValue = null;
							 formatValue = null;
						 %>
					 </div>
				 </div>
			 </div>
			 <%} %>
		 </div>
	 </div>

</div>


<Div id='mainForm_OPTS' style='visibility:hidden'>
     <%=smartHF.getMenuLink(sv.fupflg, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>
     <%=smartHF.getMenuLink(sv.anntind, resourceBundleHandler.gettingValueFromBundle("Annuity Details"))%>
     <%=smartHF.getMenuLink(sv.ddind, resourceBundleHandler.gettingValueFromBundle("Bank Account"))%>
         <%=smartHF.getMenuLink(sv.activeInd, resourceBundleHandler.gettingValueFromBundle("Health Claim"))%>
     <% if (sv.showFlag.compareTo("Y") == 0) { %> 
     <%=smartHF.getMenuLink(sv.investres, resourceBundleHandler.gettingValueFromBundle("Investigation Results"))%>
     <%=smartHF.getMenuLink(sv.claimnotes, resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes"))%>
     <% } %>
 <%--        
<table>
<tr>
<td>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg.getFormData()%>">
<%
if (sv.fupflg
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_tick_01.gif" border="0">

<%}
if (sv.fupflg.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('fupflg'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 28px;">
<a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>

</a>
</div>
<%} %>
</td></tr>
<tr>
<td>
<input name='anntind' id='anntind' type='hidden'  value="<%=sv.anntind.getFormData()%>">
<%
if (sv.anntind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_tick_01.gif" border="0">

<%}
	if (sv.anntind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('anntind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_greent_01.gif" border="0">
<%							            
}
%>
</td><td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.anntind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.anntind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 28px;">
<a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("anntind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Annuity Details")%>

</a>
</div>
<%} %>
</td></tr>
<tr><td>
<input name='ddind' id='ddind' type='hidden'  value="<%=sv.ddind.getFormData()%>">
<%
if (sv.ddind
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_tick_01.gif" border="0">

<%}
	if (sv.ddind
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ddind'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.ddind
.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 28px;">
<a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%>
   


</a>	
</div>
<%} %>
</td>
</tr>
<tr><td>
<input name='activeInd' id='activeInd' type='hidden'  value="<%=sv.activeInd
.getFormData()%>">
<%
if (sv.activeInd
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_tick_01.gif" border="0">

<%}
	if (sv.activeInd
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('activeInd'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.activeInd
.getInvisible()== BaseScreenData.INVISIBLE|| sv.activeInd
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 28px;">
<a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("activeInd"))' 
class="hyperLink">
<%=resourceBundleHandler.gettingValueFromBundle("Health Claim")%>

</a>	
</div>
<%} %>
</td></tr></table> --%>
</Div>

<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.descrip).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='188'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("-----------------------------------------------------------------------------")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.clmdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clmdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText37).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(F4)")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.rgpyshort).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rgpyshort.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpyshort.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rgpyshort.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.frqdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.clmcurdsc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.clmcurdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clmcurdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>
</tr></table></div>
