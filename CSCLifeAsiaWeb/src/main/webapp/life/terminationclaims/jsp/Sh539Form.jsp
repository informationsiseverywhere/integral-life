<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "SH539";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh539ScreenVars sv = (Sh539ScreenVars) fw.getVariables();%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Press ENTER to");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date            ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill to Date    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing Frequency       ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Commencement Date  ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"<<<");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"ESTIMATION  ONLY");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,">>>");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-------------------------------");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"ETI");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Components");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-------------------------------");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Existing SI/");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Surrender Value/");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Term");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Old Cess Date/");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Cov Ridr");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New SI");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"SV Refund");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Year Day");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New Cess Date");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind10.isOn()) {
			sv.flag.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText15.setBlink(BaseScreenData.BLINKING);
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
               <div class="row">
                      <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
	                       	<div class="input-group">
	                       <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 65px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							
							
							
							
							
							<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 50px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							
							
							
							
							
							<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'style="max-width:800px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								</div>                       
							</div>
							</div>
							
				<div class="col-md-3">
				</div>
					
					<div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                       	<div class="input-group">
	                       <%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</div>
					</div>
					</div>
					
					
				
					
					
					<div class="col-md-4">
						                    <div class="form-group">
						                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						                       	<div class="input-group">
						                       <%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
	                       
	                       </div>
	                       </div>
	                       </div>
			</div>   <!-- end of row -->

				<div class="row">
				                      <div class="col-md-4">
					                    <div class="form-group">
					                       <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					                       	<div class="input-group">
					                       <%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				  </div>
				</div>
				</div>
				
			<div class="col-md-1">
				</div>
				
					<div class="col-md-3">
						                    <div class="form-group">
						                       <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
						                       	<div class="input-group">
						                       <%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.register.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.register.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
  
                      </div>
                      </div>
                      </div>
                    
	                       
	             
						
						<div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
	                       <div class="input-group">
	                       <%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
									<%					
									if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							
							
							
							
							
							<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width: 200px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
	                       
	                        </div>
	                       </div>
	                       </div>
  
</div>  <!-- end of row -->



				<div class="row">
                      <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
                            <div class="input-group">
                            <%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							
						
						
						
						
						
						<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						                            
                            </div>
						</div>
						</div>
	                   <div class="col-md-3">
						</div> 
						
						<div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
	                         <div class="input-group">
						                       <%					
							if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</div>
	                       </div>
	                       </div>
	                       
          	                 
	                       <div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Bill to Date")%></label>
	                         <div class="input-group">
	                       <%					
							if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</div>
	                       </div>
	                       </div>
					</div>  <!-- end of row -->
					
					
					<div class="row">
                      <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
	                        <div class="input-group">
	                       <%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
									<%					
									if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width: 60px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							
							
							
							
							
							<%if ((new Byte((sv.bilfrqdesc).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.bilfrqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.bilfrqdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.bilfrqdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								                       
							                           
							 </div>
							</div>
							</div>
							
							<div class="col-md-3">
				</div>
						
						<div class="col-md-3">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label>
	                         <div class="input-group">
	                       <%if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							        </div>               
						</div>
						</div>
						
						
						
						
                 <div class="col-md-2">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
                           <table><tr><td>
                            <%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.mop.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.mop.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 30px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							
						</td><td>




						<%if ((new Byte((sv.flag).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.flag.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.flag.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.flag.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 30px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						      </td>                      
						 </tr></table>
						</div>
						</div>
						
						</div>  <!--  end of row -->

					<%-- 	<div class="row">
				        	<div class="col-md-2">
					       		<div class="form-group">
					       		<label><%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("ETI")%>
								<%}%></label>
					       		<div class="input-group">
					       		</div>
					       		</div>
					       	</div>
					    </div> --%>
					    
					    	<%
							GeneralTable sfl = fw.getTable("sh539screensfl");
							%>
							
							 <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sh539'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
						<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
						<th rowspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
						<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Header12")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></center></th>
						<th rowspan="2" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></center></th>
						</tr>
						<tr class='info'>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></center></th>
						</tr>	
			         	</thead>
			         	
					    <tbody><%
						Sh539screensfl.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (Sh539screensfl.hasMoreScreenRows(sfl)) {	
						%>
					
						<tr class="tableRowTag" id='<%="tablerow"+count%>' >
											<%if((new Byte((sv.life).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag tableDataTagFixed" style="width:60x;" 
										<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																								
													
																
														
																
											<%= sv.life.getFormData()%>
											
																			 
									
														</td>
							<%}else{%>
																	<td  >
										</td>														
															
										<%}%>
													<%if((new Byte((sv.coverage).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td  
										<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
														
																
											<%= sv.coverage.getFormData()%>
											
																			 
									
														</td>
							<%}else{%>
																	<td class="tableDataTag" style="width:100px;" >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.rider).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td  
										<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
														
																
											<%= sv.rider.getFormData()%>
											
																			 
									
														</td>
							<%}else{%>
																	<td class="tableDataTag" style="width:100px;" >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.hetiosi).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td  style="min-width:130px;"
										<%if((sv.hetiosi).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hetiosi).getFieldName());						
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.hetiosi,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												if(!(sv.hetiosi).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td class="tableDataTag" style="width:100px;" >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.hetinsi).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td  style="min-width:100px;" 
										<%if((sv.hetinsi).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hetinsi).getFieldName());						
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.hetinsi,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												if(!(sv.hetinsi).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td class="tableDataTag" style="width:100px;" >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.hsurval).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="min-width:150px;" 
										<%if((sv.hsurval).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hsurval).getFieldName());						
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.hsurval,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												if(!(sv.hsurval).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td  >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.hrefval).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="min-width:150px;" 
										<%if((sv.hrefval).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hrefval).getFieldName());						
											//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.hrefval,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												if(!(sv.hrefval).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td  >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.etiyear).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="width:100px;" 
										<%if((sv.etiyear).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.etiyear).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.etiyear);
												if(!(sv.etiyear).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td  >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.etidays).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="min-width:100px;" 
										<%if((sv.etidays).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
																												
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.etidays).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.etidays);
												if(!(sv.etidays).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
										 			 		
								 		
								    				 
									
														</td>
							<%}else{%>
																	<td  >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.riskCessDateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="min-width:150px;" 
										<%if((sv.riskCessDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
														
																
											<%= sv.riskCessDateDisp.getFormData()%>
											
																			 
									
														</td>
							<%}else{%>
																	<td  >
																			
									    </td>
															
										<%}%>
													<%if((new Byte((sv.zrdateDisp).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							    									<td class="tableDataTag" style="min-width:150px;" 
										<%if((sv.zrdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
													
																
														
																
											<%= sv.zrdateDisp.getFormData()%>
											
																			 
									
														</td>
							<%}else{%>
																	<td class="tableDataTag" style="width:100px;" >
																			
									    </td>
															
										<%}%>
														
						</tr>
					
						<%
						count = count + 1;
						Sh539screensfl
						.setNextScreenRow(sfl, appVars, sv);
						}
						%>
					      </tbody>
					</table>
						
					</div>
					 <div id="load-more" class="col-md-offset-10 pull-right">
						    <a class="btn btn-info" href="#" style='width: 74px;'
						       onclick="doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("More")%>
						    </a>
						</div>
				</div>
			</div>
		</div>
		<br/><br/><br/><br/>
		 <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Press ENTER to")%>
					<%}%></label>
		       		<div class="input-group">
		       		
					<%if ((new Byte((sv.trandsc).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.trandsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.trandsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.trandsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div id='trandsc' class='label_txt'class='label_txt'  onHelp='return fieldHelp("trandsc")'><%=sv.trandsc.getFormData() %></div>
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
			<div style='visibility:hidden;'><table>
			<tr style='height:22px;'><td width='188'>
			<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("------------------------------------------------------------------------------")%>
			</label>
			<%}%>
			
			</tr></table></div>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>