<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6762";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>

<%S6762ScreenVars sv = (S6762ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Soft Locked");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Invalid Contract Status");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Invalid Component Status");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Invalid Payment Status");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment in Review");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Terminated");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Insufficient Fund Amount");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Withdrawal < Min Amount");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No Price Available for Fund");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item not found on T5679");%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group">
					<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</div>
				</div>
			</div>
			
			<div class="col-md-1">
</div>

<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div class="input-group">
					<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	</div>
			</div>
	</div>
	
<div class="col-md-1">
</div>

<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width: 800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</div>
	</div>
	</div>
	
	
			
		</div>   <!-- end row -->


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Soft Locked")%></label>
				<div class="input-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpCsfl"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpCsfl");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpCsfl.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpCsfl.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpCsfl).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpCsfl.getFormData() == null)||("".equals(sv.regpayExcpCsfl.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpCsfl.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpCsfl.getFormData()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpCsfl).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpCsfl' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpCsfl).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpCsfl).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpCsfl).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
	</div>
				</div>
			</div>
		</div>   <!-- end row -->

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Invalid Contract Status")%></label>
				<div class="input-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpIchs"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpIchs");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpIchs.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpIchs.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpIchs).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpIchs.getFormData() == null)||("".equals(sv.regpayExcpIchs.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpIchs.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpIchs.getFormData()%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpIchs).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpIchs' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpIchs).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpIchs).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpIchs).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
	</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Invalid Component Status")%></label>
				<div class="input-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpIcos"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpIcos");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpIcos.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpIcos.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpIcos).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpIcos.getFormData() == null)||("".equals(sv.regpayExcpIcos.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpIcos.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpIcos.getFormData()%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpIcos).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpIcos' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpIcos).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpIcos).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpIcos).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
				</div>
				</div>
	
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Invalid Payment Status")%></label>
				<div class="input-group">
				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpIpst"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpIpst");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpIpst.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpIpst.getFormData()).toString().trim());  
	
%>

<% 
	if((new Byte((sv.regpayExcpIpst).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpIpst.getFormData() == null)||("".equals(sv.regpayExcpIpst.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpIpst.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpIpst.getFormData()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else { %>
	
<% if("red".equals((sv.regpayExcpIpst).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:250px;"> 
<%
} 
%>

<select name='regpayExcpIpst' type='list' style="min-width:250px;"
<% 
	if((new Byte((sv.regpayExcpIpst).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpIpst).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpIpst).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
				</div>
				</div>
		</div>   <!-- end of row -->
<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment in Review")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpInrv"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpInrv");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpInrv.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpInrv.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpInrv).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpInrv.getFormData() == null)||("".equals(sv.regpayExcpInrv.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpInrv.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpInrv.getFormData()%>
	   		
	   		<%}%>
	   </div>


<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpInrv).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpInrv' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpInrv).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpInrv).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpInrv).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
	</div>
</div>
 
<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Terminated")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpExpd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpExpd");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpExpd.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpExpd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpExpd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpExpd.getFormData() == null)||("".equals(sv.regpayExcpExpd.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpExpd.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpExpd.getFormData()%>
	   		
	   		<%}%>
	   </div>


<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpExpd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpExpd' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpExpd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpExpd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpExpd).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
	</div>
</div>
	</div>  <!-- end of row -->

<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Insufficient Fund Amount")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpInsf"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpInsf");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpInsf.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpInsf.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpInsf).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpInsf.getFormData() == null)||("".equals(sv.regpayExcpInsf.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpInsf.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpInsf.getFormData()%>
	   		
	   		<%}%>
	   </div>


<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpInsf).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpInsf' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpInsf).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpInsf).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpInsf).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
	</div>
</div>

<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Withdrawal < Min Amount")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpLtma"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpLtma");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpLtma.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpLtma.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpLtma).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpLtma.getFormData() == null)||("".equals(sv.regpayExcpLtma.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpLtma.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpLtma.getFormData()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpLtma).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpLtma' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpLtma).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpLtma).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpLtma).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
</div> 
</div> 
	</div> <!-- end of row -->


<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("No Price Available for Fund")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpNopr"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpNopr");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpNopr.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpNopr.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpNopr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpNopr.getFormData() == null)||("".equals(sv.regpayExcpNopr.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpNopr.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpNopr.getFormData()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpNopr).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpNopr' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpNopr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpNopr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpNopr).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
	</div>
</div>

<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item not found on T5679")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayExcpTinf"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("regpayExcpTinf");
	optionValue = makeDropDownList( mappedItems , sv.regpayExcpTinf.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.regpayExcpTinf.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.regpayExcpTinf).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((sv.regpayExcpTinf.getFormData() == null)||("".equals(sv.regpayExcpTinf.getFormData().trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(sv.regpayExcpTinf.getFormData() != null){%>
	   		
	   		<%=sv.regpayExcpTinf.getFormData()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.regpayExcpTinf).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='regpayExcpTinf' type='list' style="width:180px;"
<% 
	if((new Byte((sv.regpayExcpTinf).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.regpayExcpTinf).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.regpayExcpTinf).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
					</div>
					</div>
	</div> <!-- end of row -->



	</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

