

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6691";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6691ScreenVars sv = (S6691ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Selected Component ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Value ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Declaration Date ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Details");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------------------------");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Value Amount To Surrender ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Currency ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Reserve Value ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Print Letter (Y/N) ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>

<%{
		if (!appVars.ind01.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.plansfx.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.plansfx.setColor(BaseScreenData.WHITE);
			sv.plansfx.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.bonusValueSurrender.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.bonusValueSurrender.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.bonusValueSurrender.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.bonusValueSurrender.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.paycurr.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.paycurr.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.paycurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.paycurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.letterPrintFlag.setReverse(BaseScreenData.REVERSED);
			sv.letterPrintFlag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.letterPrintFlag.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<!-- ILIFE-2674 Life Cross Browser -Coding and UT- Sprint 3 D5: Task 5 starts  -->
<!-- <style>
@media \0screen\,screen\9
{
.blank_cell{margin-right:1px}
}
</style> --><!-- ILIFE-2674 Life Cross Browser -Coding and UT- Sprint 3 D5: Task 5 ends -->

<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
		<div class="panel-body">
		       <div class="row">	
                <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <div class="input-group"> 
			            <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

<%if ((new Byte((sv.cntdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           </div>
			        </div>
			     </div>

			     <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
			           <div class="input-group"> 
			            <%if ((new Byte((sv.rstatdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rstatdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstatdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstatdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.premStatDesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premStatDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           </div>
			         </div>
			      </div>     

			      <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
			           <%if ((new Byte((sv.rcdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rcdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			         </div>
			      </div>
			   </div>        
			   
			   <div class="row">	
                <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
			           <div class="input-group"> 
			           <%if ((new Byte((sv.cownum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cownum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           </div>
			        </div>
			      </div>
			    </div>       
			    
			    <div class="row">	
                <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
			           <div class="input-group"> 
			           <%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           </div>
			        </div>
			      </div>
			      
			      <div class="col-md-4"></div>
			       <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
			           <div class="input-group"> 
			           <%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           </div>
			         </div>
			       </div>
			    </div>  
			    
			     <div class="row">	
                <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
			           <%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			        </div>
			      </div>
			      
			      <div class="col-md-4 col-md-offset-6">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
			           <%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			        </div>
			      </div>
			     </div>      
			     
		  <div class="row">	
		         
                <div class="col-md-4">
		           <div class="form-group"> 
		            <%if ((new Byte((sv.plansfx).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
			          
	
  		
		<%					
		if(!((sv.plansfx.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.plansfx.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.plansfx.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			       </div>
			   </div> 
			
			 
			 <div class="col-md-4"></div>
			 
			 <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
			           <%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			           
			        </div>
			  </div>    
			</div>     
			
			<div class="row">	
		         
                <div class="col-md-4">
		           <div class="form-group"> 
		              <label><%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%></label>
		              
		              </div>
		            </div>    
		         </div>   
		         
		         <div class="row">	
		              <div class="col-md-4">
		                 <div class="form-group"> 
		                  <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
		                   
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		                 </div>
		            </div>    
		         
		          <div class="col-md-4">
		                 <div class="form-group"> 
		                  <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label> 
			              
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
			              </div>  
			          </div>      

		          <div class="col-md-4">
		                 <div class="form-group"> 
		                 <%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label> 
		                   
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		                   </div>
		                </div>   
                  </div>
                  
                  <div class="row">	
		              <div class="col-md-4">
		                 <div class="form-group"> 
		                 <%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%></label>
		                 
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		                   </div>
		                 </div>
		                 <div class="col-md-4">
		                 <div class="form-group"> 
		               
<%if ((new Byte((sv.bonusValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Value")%></label>
		                   	<%	
			qpsf = fw.getFieldXMLDef((sv.bonusValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S13ZEROS1VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.bonusValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE_ONE);
			
			if(!((sv.bonusValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div style="width: 150px;" class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div style="width: 150px;" class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
		                   </div>
		                 </div>
		                 <div class="col-md-3">
		                 <div class="form-group"> 
		               <%if ((new Byte((sv.bonusDecDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                   <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Declaration Date")%></label>
		                   
	
  		
		<%					
		if(!((sv.bonusDecDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bonusDecDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bonusDecDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		                   </div>
		                 </div>
		              </div>     
		              
		               <div class="row">	
		              <div class="col-md-4">
		                 <div class="form-group"> 
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Details")%></label>
		                   </div>
		                </div>
		             </div>  
		             
		              <div class="row">	
		              <div class="col-md-4">
		                 <div class="form-group"> 
		                 
                             <%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		                     <%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div style="width: 150px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
		                   </div>
		                </div>
		                
		                  <div class="col-md-4"></div>
		                <div class="col-md-4">
		                 <div class="form-group"> 
		                 <%if ((new Byte((sv.bonusValueSurrender).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Value Amount To Surrender")%></label>
		                     <div style="width: 150px;">
		                     <%	
			qpsf = fw.getFieldXMLDef((sv.bonusValueSurrender).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.bonusValueSurrender,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='bonusValueSurrender' 
type='text'

<%if((sv.bonusValueSurrender).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.bonusValueSurrender.getLength(), sv.bonusValueSurrender.getScale(),3)%>'
maxLength='<%= sv.bonusValueSurrender.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(bonusValueSurrender)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.bonusValueSurrender).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bonusValueSurrender).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bonusValueSurrender).getColor()== null  ? 
			"input_cell" :  (sv.bonusValueSurrender).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
</div>
		                   </div>
		                </div>
		             </div>  

                     <div class="row">	
		              <div class="col-md-4">
		                 <div class="form-group"> 
		                 
                            <%if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%></label>
		                     <%	
	
	longValue = sv.paycurr.getFormData();  
%>

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div style="width: 150px;" class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='paycurr' 
type='text' 
value='<%=sv.paycurr.getFormData()%>' 
maxLength='<%=sv.paycurr.getLength()%>' 
size='<%=sv.paycurr.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(paycurr)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.paycurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('paycurr')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
							<button class="btn btn-info" 
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
								type="button"
								onClick="doFocus(document.getElementById('paycurr')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

<%
	}else { 
%>

class = ' <%=(sv.paycurr).getColor()== null  ? 
"input_cell" :  (sv.paycurr).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('paycurr')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
							<button class="btn btn-info" 
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important";
								type="button"
								onClick="doFocus(document.getElementById('paycurr')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
<%}longValue = null;}} %>
		                  </div>
		               </div> 
		               <div class="col-md-4">
		                 <div class="form-group"> 
		                 
                              <%if ((new Byte((sv.bonusReserveValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Reserve Value")%></label>
		                     <div style="width: 150px;">
		                 	<%	
			qpsf = fw.getFieldXMLDef((sv.bonusReserveValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S13ZEROS1VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.bonusReserveValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE_ONE);
			
			if(!((sv.bonusReserveValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div style="width: 150px;" class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 </div>
		                  </div>
		               </div>    
		               
		                <div class="col-md-4">
		                 <div class="form-group"> 
		                 
                            <%if ((new Byte((sv.letterPrintFlag).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		                     <label><%=resourceBundleHandler.gettingValueFromBundle("Print Letter (Y/N)")%></label>
		                     <input type='checkbox' name='letterPrintFlag' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(letterPrintFlag)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.letterPrintFlag).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.letterPrintFlag).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('letterPrintFlag')"/>

<input type='checkbox' name='letterPrintFlag' value='N' 

<% if(!(sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('letterPrintFlag')"/>
<%}%>
		                     
		                  </div>
		               </div>      
		            </div>   
		            </div>
  <div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("------------------------------------------------------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("---------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Details")%>
</div>
<%}%>

</tr></table></div>



</div>
















<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.cntdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.rstatdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rstatdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstatdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstatdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.premStatDesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premStatDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premStatDesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("RCD")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.rcdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rcdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.cownum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cownum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("J/Life")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	





<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.plansfx).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.plansfx.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.plansfx.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.plansfx.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr></table><br/>
  
 	
	<table>

<tr style='height:2px;'><td width='251'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
</div>
<%}%>





<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Life No")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rider No")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>

<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Value")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.bonusValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.bonusValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S13ZEROS1VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.bonusValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE_ONE);
			
			if(!((sv.bonusValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Declaration Date")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.bonusDecDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.bonusDecDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bonusDecDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bonusDecDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr></table><br/><table>

<tr style='height:2px;'><td width='251'>
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Details")%>
</div>
<%}%>



<tr style='height:22px;'><td width='251'>
<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='251'></td><td width='251'>
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Value Amount To Surrender")%>
</div>
<%}%>



<br/>
<%if ((new Byte((sv.bonusValueSurrender).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.bonusValueSurrender).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			valueThis=smartHF.getPicFormatted(qpsf,sv.bonusValueSurrender,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='bonusValueSurrender' 
type='text'

<%if((sv.bonusValueSurrender).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.bonusValueSurrender.getLength(), sv.bonusValueSurrender.getScale(),3)%>'
maxLength='<%= sv.bonusValueSurrender.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(bonusValueSurrender)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.bonusValueSurrender).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.bonusValueSurrender).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.bonusValueSurrender).getColor()== null  ? 
			"input_cell" :  (sv.bonusValueSurrender).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
</td></tr>

<tr style='height:22px;'>

<td width='251'>
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
</div>
<%}%>



<br/>
<%if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.paycurr.getFormData();  
%>

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='paycurr' 
type='text' 
value='<%=sv.paycurr.getFormData()%>' 
maxLength='<%=sv.paycurr.getLength()%>' 
size='<%=sv.paycurr.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(paycurr)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.paycurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('paycurr')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.paycurr).getColor()== null  ? 
"input_cell" :  (sv.paycurr).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('paycurr')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%}longValue = null;}} %>


</td>


<!--<td width='251'>
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
</div>
<%}%>



<br/>
<%	
	if ((new Byte((sv.paycurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paycurr");
	optionValue = makeDropDownList( mappedItems , sv.paycurr.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.paycurr).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='paycurr' type='list' style="width:140px;"
<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.paycurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.paycurr).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
</td>

--><td width='251'>
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Reserve Value")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.bonusReserveValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.bonusReserveValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S13ZEROS1VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.bonusReserveValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE_ONE);
			
			if(!((sv.bonusReserveValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</td>

<td width='251'>
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Print Letter (Y/N)")%>
</div>
<%}%>



<br/>
<%if ((new Byte((sv.letterPrintFlag).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input type='checkbox' name='letterPrintFlag' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(letterPrintFlag)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.letterPrintFlag).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.letterPrintFlag).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('letterPrintFlag')"/>

<input type='checkbox' name='letterPrintFlag' value='N' 

<% if(!(sv.letterPrintFlag).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('letterPrintFlag')"/>
<%}%>
</td></tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("------------------------------------------------------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("---------------------------------")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%>
</div>
<%}%>


<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Details")%>
</div>
<%}%>

</tr></table></div><br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>

