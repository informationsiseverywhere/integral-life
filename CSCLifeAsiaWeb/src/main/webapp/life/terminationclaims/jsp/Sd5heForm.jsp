

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sd5he";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sd5heScreenVars sv = (Sd5heScreenVars) fw.getVariables();%>

<div class="panel panel-default">
		<div class="panel-body">
		
		       <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-4"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-1"></div>
			     <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group" style="max-width:100px">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:60px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			     
			  </div>      
			  
			   <div class="row">	
                <div class="col-md-5">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Amount Limit for AutoPay (Minimum)")%></label> 
<% 
qpsf = fw.getFieldXMLDef((sv.minpayamnt).getFieldName());
formatValue = smartHF.getPicFormatted(qpsf, sv.minpayamnt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN);
			        		   
	if((new Byte((sv.minpayamnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(formatValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(formatValue)%>
	   		
	   		<%}%>
	   </div>


	<% }else {%>
	
<input style="text-align: right; width:100px;" maxLength='<%=sv.minpayamnt.getLength() + 1%>'  value='<%=formatValue%>'	size='<%=sv.minpayamnt.getLength()%>' 
 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(minpayamnt)' onKeyUp='return checkMaxLength(this)' onHelp='return fieldHelp(minpayamnt)' 
 onKeyUp='return checkMaxLength(this)' name='minpayamnt'  id='minpayamnt' onchange="updateTextBox(this.value,name)"  class="form-control"
 onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); " decimal='<%=qpsf.getDecimals()%>' onBlur='return doBlurNumberNew(event,true);'>


<%
} 
%>
			        </div>
			     </div>
			     
			                     <div class="col-md-5">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Amount Limit for AutoPay (Maximum)")%></label>
<% 
qpsf = fw.getFieldXMLDef((sv.maxpayamnt).getFieldName());
formatValue = smartHF.getPicFormatted(qpsf, sv.maxpayamnt,
		COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN);
			        		   
	if((new Byte((sv.maxpayamnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="max-width:100px;">  
	   		<%if(formatValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(formatValue)%>
	   		
	   		<%}%>
	   </div>


	<% }else {%>
	
<input style="text-align: right; width:100px;" maxLength='<%=sv.maxpayamnt.getLength() + 1%>'
	   value='<%=formatValue%>'	size='<%=sv.maxpayamnt.getLength()%>' onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(maxpayamnt)'
		onKeyUp='return checkMaxLength(this)' onHelp='return fieldHelp(maxpayamnt)' onKeyUp='return checkMaxLength(this)' name='maxpayamnt' class="form-control" 
	   id='maxpayamnt' onchange="updateTextBox(this.value,name)"  
		onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); " decimal='<%=qpsf.getDecimals()%>' 
		onBlur='return doBlurNumberNew(event,true);'>


<%
} 
%>
			        </div>
			     </div>
			     
			   </div>   
			   
			   </div>
			   </div>     


<%@ include file="/POLACommon2NEW.jsp"%>

