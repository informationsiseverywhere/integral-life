<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5020";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5020ScreenVars sv = (S5020ScreenVars) fw.getVariables();%>

<div class="panel panel-default">
	 <div class="panel-body">
		 <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
		       	<table><tr><td>
		       		<div 	
						class='<%= (sv.chdrnum.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>'>
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					  </td><td style="min-width:1px"></td><td>
				  		<div 	
						class='<%= (sv.cnttype.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>'>
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
							  </td><td style="min-width:1px"></td><td>
						<div 	
						class='<%= (sv.ctypedes.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>' style="max-width: 800px;">
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-1">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
		       		<table><tr><td>
		       		<div 	
						class='<%= (sv.rstate.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
						<%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					  </td><td style="min-width:1px"></td><td>
					<div 	
						class='<%= (sv.pstate.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
						<%					
						if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		</div>
		       		
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("RCD ")%></label>
		       		<div class="input-group">
		       				<%					
								if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	 
	 <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
		       		<table><tr><td>
		       		<div 	
						class='<%= (sv.cownnum.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
						<%					
						if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					  </td><td style="min-width:1px"></td><td>
				
				  		<div 	
						class='<%= (sv.ownername.getFormData()).trim().length() == 0 ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
						<%					
						if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
				
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
				
									} else  {
				
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
				
									}
									%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-3">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
		       	<table><tr><td>
		       		<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  	  </td><td style="min-width:1px"></td><td>
				  		
						<%					
						if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  	  </td><td style="min-width:1px"></td><td>
				  		
						<%					
						if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
		       		<div class="input-group">
		       				<%					
								if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-3">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Billed to date ")%></label>
		       		<div class="input-group">
		       				<%					
								if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	        <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix")%></label>
		       		<div class="input-group">
		       				<%					
								if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		     <%
				/* This block of jsp code is to calculate the variable width of the table at runtime.*/
				int[] tblColumnWidth = new int[9];
				int totalTblWidth = 0;
				int calculatedValue =0;
				
																		if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.life.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
										} else {		
											calculatedValue = (sv.life.getFormData()).length()*12;								
										}					
																		totalTblWidth += calculatedValue;
						tblColumnWidth[0]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.coverage.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
							} else {		
								calculatedValue = (sv.coverage.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[1]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.rider.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
							} else {		
								calculatedValue = (sv.rider.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[2]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fund.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
							} else {		
								calculatedValue = (sv.fund.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[3]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.fundtype.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
							} else {		
								calculatedValue = (sv.fundtype.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[4]= calculatedValue;
							
										if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.shortds.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
							} else {		
								calculatedValue = (sv.shortds.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[5]= calculatedValue;
						
						if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.cnstcur.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
							} else {		
								calculatedValue = (sv.cnstcur.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[6]= calculatedValue;
						
						if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.estMatValue.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
							} else {		
								calculatedValue = (sv.estMatValue.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[7]= calculatedValue;
						
						if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.actvalue.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
							} else {		
								calculatedValue = (sv.actvalue.getFormData()).length()*12;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[8]= calculatedValue;
						
							%>
						<%
						GeneralTable sfl = fw.getTable("s5020screensfl");
						int height;
						if(sfl.count()*27 > 210) {
						height = 210 ;
						} else {
						height = sfl.count()*27;
						}
						
						%>
						
						<div class="row">
	        		<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></label>
		       		
				       		</div>
				       	</div>
				    </div>
				    
				<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5020'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Life")%></center></th>									
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></center></th>									
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Type")%></center></th>									
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("CCY")%></center></th>									
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Estimated value")%></center></th>
						<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Actual value")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
							S5020screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (S5020screensfl.hasMoreScreenRows(sfl)) {
							
							%>
						
						
							<tr>
												    									
								<td align="right">									
																
									<%= sv.life.getFormData()%>
								</td>
								<td  align="left">									
																
									<%= sv.coverage.getFormData()%>
								</td>
								<td  align="right">									
																
									<%= sv.rider.getFormData()%>
								</td>
								<td  align="left">									
																
									<%= sv.fund.getFormData()%>
								</td>
								<td  align="left">									
																
									<%= sv.fundtype.getFormData()%>
								</td>
								<td  align="left">									
																
									<%= sv.shortds.getFormData()%>
								</td>
								<td align="left">									
																
									<%= sv.cnstcur.getFormData()%>
								</td>
								<td align="right">									
										<%
											formatValue = null;
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf,sv.estMatValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>								
									<%=formatValue%>
								</td>
								<td align="right">									
										<%
											formatValue = null;
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										%>								
									<%=formatValue%>
								</td>
						</tr>
							
							
								<%
								
								count = count + 1;
								S5020screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Maturity Details")%></label>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	   
		       	
		       	<div class="col-md-4">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Other Adjustments ")%></label>
		       		<div class="input-group">
		       					<%
					formatValue = null;
					qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.otheradjst,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>		
					<input style="padding-top: 4px; text-align: right;" type='text'
					maxLength='<%=sv.otheradjst.getLength() + 1%>'
					size='<%=sv.otheradjst.getLength() + 1%>' name='otheradjst'
					readonly="true" class="output_cell"
					style="width: <%=sv.otheradjst.getLength() * 7%> px;"
					value="<%=formatValue%>">	
					<%
					longValue = null;
					formatValue = null;
					%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy debt       ")%></label>
		       		<div class="input-group">
		       					<%
					formatValue = null;
					qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.tdbtamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>				
					<input style="padding-top: 4px; text-align: right;" type='text'
					maxLength='<%=sv.tdbtamt.getLength() + 1%>'
					size='<%=sv.tdbtamt.getLength() + 1%>' name='tdbtamt'
					readonly="true" class="output_cell"
					style="width: <%=sv.tdbtamt.getLength() * 7%> px;"
					value="<%=formatValue%>">			
					<%
					longValue = null;
					formatValue = null;
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency ")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Estimated Total ")%></label>
		       		<div class="input-group">
		       		<%
					formatValue = null;
					qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>				
					<input style="padding-top: 4px; text-align: right;" type='text'
					maxLength='<%=sv.estimateTotalValue.getLength() + 1%>'
					size='<%=sv.estimateTotalValue.getLength() + 1%>' name='estimateTotalValue'
					readonly="true" class="output_cell"
					style="width: <%=sv.estimateTotalValue.getLength() * 7%> px;"
					value="<%=formatValue%>">
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-3">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Actual Total ")%></label>
		       		<div class="input-group">
		       				<%
					formatValue = null;
					qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
					formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
					%>				
					<input style="padding-top: 4px; text-align: right;" type='text'
					maxLength='<%=sv.clamant.getLength() + 1%>'
					size='<%=sv.clamant.getLength() + 1%>' name='clamant'
					readonly="true" class="output_cell"
					style="width: <%=sv.clamant.getLength() * 7%> px;"
					value="<%=formatValue%>">
					<%
					longValue = null;
					formatValue = null;
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment reason ")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td style="min-width:1px"></td><td>
						<%					
						if(!((sv.resndesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.resndesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>

<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
			
		    </div>
		    
		    
	 </div>
</div>

<script>
	$(document).ready(function() {
		var showval= document.getElementById('show_lbl').value;
		var toval= document.getElementById('to_lbl').value;
		var ofval= document.getElementById('of_lbl').value;
		var entriesval= document.getElementById('entries_lbl').value;	
		var nextval= document.getElementById('nxtbtn_lbl').value;
		var previousval= document.getElementById('prebtn_lbl').value;
		$('#dataTables-s5020').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
        	 language: {
                 "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,            
                 "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
                 "paginate": {                
                     "next":       nextval,
                     "previous":   previousval
                 }
               }     
      	});
    

    });
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
