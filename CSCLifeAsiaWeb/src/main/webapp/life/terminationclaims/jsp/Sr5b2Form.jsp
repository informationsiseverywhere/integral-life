<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5b2";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>

<%Sr5b2ScreenVars sv = (Sr5b2ScreenVars) fw.getVariables();%>
<%{
	if (appVars.ind02.isOn()) {
		sv.nlg.setReverse(BaseScreenData.REVERSED);
		sv.nlg.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind02.isOn()) {
		sv.nlg.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind03.isOn()) {
		sv.nlgczag.setReverse(BaseScreenData.REVERSED);
		sv.nlgczag.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.nlgczag.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.maxnlgyrs.setReverse(BaseScreenData.REVERSED);
		sv.maxnlgyrs.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.maxnlgyrs.setHighLight(BaseScreenData.BOLD);
	}
} %>

<div class="panel panel-default">
<div class="panel-body">

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<!--
				<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
				<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
				-->
				<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
				
				<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
					<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}						
						}else{
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.company.getFormData()).toString()); 
							}else{
								formatValue = formatValue( longValue);
							}
						}
					%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
				<%}%>
		
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<!--
			<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
			<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
			-->
			<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			
			<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<!--
			<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
			<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
			-->
			<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			<table>
			<tr>
			<td>
			<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.item.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.item.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>
			</td>
			<td>
			<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
				<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						if(longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
						} else {
							formatValue = formatValue( longValue);
						}						
					}else{
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
						}else{
							formatValue = formatValue( longValue);
						}
					}
				%>
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
			<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			<%}%>
			</td>
			</tr>
			</table>

			</div>
		</div>

	</div>
	
	<div class="row">

	<div class="col-md-3">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
		
		<table>
		<tr>
		<td>
			<%
				if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
				</td>
		<td style="padding-left: 5px;"></td>
		<td>
		<label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label>
		
		</td>
		<td style="padding-left: 5px;"></td>
		<td>
				<%					
				if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							} else  {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 80px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			</td>
			</tr>
			</table>
			</div>
		</div>

	</div>

	<div class="row">
			
		<div class="col-md-3">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("No Lapse Guarantee")%></label>
			
			<%	
				if ((new Byte((sv.nlg).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {
									
				if(((sv.nlg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
					longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
				}
				if(((sv.nlg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
				longValue=resourceBundleHandler.gettingValueFromBundle("No");
				}
					 
			%>
			<% 
				if((new Byte((sv.nlg).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
			%>  
			  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 70px;">  
				   		<%if(longValue != null){%>
				   		
				   		<%=XSSFilter.escapeHtml(longValue)%>
				   		
				   		<%}%>
				   </div>
			<%
			longValue = null;
			%>
				<% }else {%>
				
			<% if("red".equals((sv.nlg).getColor())){
			%>
			<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
			<%
			} 
			%>
			<select name='nlg' style="width:70px; " 	
				onFocus='doFocus(this)'
				onHelp='return fieldHelp(nlg)'
				onKeyUp='return checkMaxLength(this)'
			<% 
				if((new Byte((sv.nlg).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.nlg).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
				class = 'input_cell' 
			<%
				} 
			%>
			>
			<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
			<option value="Y"<% if(((sv.nlg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
			<option value="N"<% if(((sv.nlg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
			</select>
			<% if("red".equals((sv.nlg).getColor())){
			%>
			</div>
			<%
			} 
			%>
			<%
			}longValue = null;} 
			%>
			</div>
		</div>
			
		
	</div>
	
	<div class="row">

		
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("NLG Cease Age")%></label>		
				<%=smartHF.getHTMLVar(0, 0, fw, sv.nlgczag, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="width: 100px;">
			<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Max Number of Years for NLG")%></label>
		
			<%=smartHF.getHTMLVar(0, 0, fw, sv.maxnlgyrs, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
			</div>
		</div>		
		
	</div>


</div>
</div>

<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<%@ include file="/POLACommon2NEW.jsp"%>
