<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl54";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%@ page import="com.csc.smart.recordstructures.Wsspwindow" %>
<%@ page import="com.csc.smart.recordstructures.Wsspcomn" %>

<%
	Sjl54ScreenVars sv = (Sjl54ScreenVars) fw.getVariables();
	Wsspwindow wsspwindow = new Wsspwindow();
%>
<%{
		if (appVars.ind29.isOn()) {
			sv.reqntype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.reqntype.setReverse(BaseScreenData.REVERSED);
			sv.reqntype.setColor(BaseScreenData.RED);
		}
}%>
<p id="wsspcomn.chdrCownnum"></p>	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Number"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.payNum)%>
						</td></tr></table>
					</div>
				</div>
				
				<div class="col-md-3">				
        			<div class="form-group">
        				<label>	<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Claim Number"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.claimNum)%>
						</td></tr></table>
					</div>
				</div>
				
				<div class="col-md-6">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Number"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.contractNum)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.cnttype)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.ctypedes)%>
						</td></tr></table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">				
				    	<div class="form-group" style="width: 80px;">
				    	<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Payment Date")%></label>
						<table><tr><td>
						<%=smartHF.getRichTextDateInput(fw, sv.payDateDisp)%>
						</td></tr></table> 
				    </div>
				 </div>	
					
				<div class="col-md-3">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Method"))%></label>
        				<div class="input-group" >
					<%
						if ((new Byte((sv.reqntype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							fieldItem = appVars.loadF4FieldsLong(new String[]{"reqntype"}, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("reqntype");
							optionValue = makeDropDownList(mappedItems, sv.reqntype.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());
					%>
					<%=smartHF.getDropDownExt(sv.reqntype, fw, longValue, "reqntype", optionValue)%>
					<%
						}
					%>
				</div>
					</div>
				</div>
					
				<div class="col-md-3">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Payment Amount"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.totalPayAmt)%>
						</td></tr></table>
					</div>
				</div>	
				
				<div class="col-md-3">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Insured"))%></label>
        				<table><tr><td>
								<%=smartHF.getHTMLVarExt(fw, sv.clientnum)%>
								</td><td style=" padding-left: 1px">
								<%=smartHF.getHTMLVarExt(fw, sv.clntName)%>
						</td></tr></table>
					</div>
				</div>	
			</div>	
			<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				</div></div></div>
			<div class="row">
				<div class="col-md-12">				
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee"))%></label>
        			</div>
        		</div>
        	</div>
        	<%
					GeneralTable sfl = fw.getTable("sjl54screensfl");
					GeneralTable sfl1 = fw.getTable("sjl54screensfl");
			%>
			<script language="javascript">
							$(document).ready(
									function() {
										var rows =
						<%=sfl1.count() + 1%>
							;
										var isPageDown = 1;
										var pageSize = 1;
										var headerRowCount = 1;
										var fields = new Array();
										fields[0] = "clttwo";
										fields[1] = "bankacckey";

										operateTableForSuperTableNEW(rows,
												isPageDown, pageSize, fields,
												"dataTables-sjl54", null,
												headerRowCount);

									});
						</script>
							
        <div class="row">
			<div class="col-md-12">
				<div >
					<div class="table-responsive">
						<table id='dataTables-sjl54' 
						style="table-layout: fixed;"
						class="table table-striped table-bordered table-hover" width="100%" >
							<thead>
								 <tr class='info'>
									<th  style="text-align: center; width: 45px !important"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
									<th  style="text-align: center; width: 130px !important"><%=resourceBundleHandler.gettingValueFromBundle("Client No.")%></th>
									<th  style="text-align: center; width: 280px !important"><%=resourceBundleHandler.gettingValueFromBundle("Client Name")%></th>
									<th  style="text-align: center; width: 280px !important"><%=resourceBundleHandler.gettingValueFromBundle("Bank")%></th>
									<th  style="text-align: center; width: 200px !important"><%=resourceBundleHandler.gettingValueFromBundle("Account type")%></th>
									<th  style="text-align: center; width: 230px !important"><%=resourceBundleHandler.gettingValueFromBundle("Bank Account #")%></th>
									<th  style="text-align: center; width: 280px !important"><%=resourceBundleHandler.gettingValueFromBundle("Account Name")%></th>
									<th  style="text-align: center; width: 85px !important"><%=resourceBundleHandler.gettingValueFromBundle("Receiving %")%></th>
									<th  style="text-align: center; width: 175px !important"><%=resourceBundleHandler.gettingValueFromBundle("Payment Amount")%></th>
								</tr>  
							</thead>
							<tbody>
							<%
								Sjl54screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (Sjl54screensfl.hasMoreScreenRows(sfl)) {
								hyperLinkFlag = true;
								
								if (appVars.ind19.isOn()) {
									sv.prcent.setReverse(BaseScreenData.REVERSED);
									sv.prcent.setColor(BaseScreenData.RED);
								}
								if (appVars.ind21.isOn()) {
									sv.prcent.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind20.isOn()) {
									sv.pymt.setReverse(BaseScreenData.REVERSED);
									sv.pymt.setColor(BaseScreenData.RED);
								}
								if (appVars.ind22.isOn()) {
									sv.pymt.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind13.isOn()) {
									sv.clttwo.setReverse(BaseScreenData.REVERSED);
									sv.clttwo.setColor(BaseScreenData.RED);
								}
								if (appVars.ind23.isOn()) {
									sv.clttwo.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind17.isOn()) {
									sv.bankacckey.setReverse(BaseScreenData.REVERSED);
									sv.bankacckey.setColor(BaseScreenData.RED);
								}
								if (appVars.ind27.isOn()) {
									sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind14.isOn()) {
									sv.clntID.setReverse(BaseScreenData.REVERSED);
									sv.clntID.setColor(BaseScreenData.RED);
								}
								if (appVars.ind24.isOn()) {
									sv.clntID.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind15.isOn()) {
									sv.babrdc.setReverse(BaseScreenData.REVERSED);
									sv.babrdc.setColor(BaseScreenData.RED);
								}
								if (appVars.ind25.isOn()) {
									sv.babrdc.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind16.isOn()) {
									sv.accdesc.setReverse(BaseScreenData.REVERSED);
									sv.accdesc.setColor(BaseScreenData.RED);
								}
								if (appVars.ind26.isOn()) {
									sv.accdesc.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind18.isOn()) {
									sv.bankaccdsc.setReverse(BaseScreenData.REVERSED);
									sv.bankaccdsc.setColor(BaseScreenData.RED);
								}
								if (appVars.ind28.isOn()) {
									sv.bankaccdsc.setEnabled(BaseScreenData.DISABLED);
								}
								
							%> 	
							<tr id='<%="tablerow" + count%>'>
								<td align="center" style="width:30px;">		
									<%=smartHF.getCheckBox(0, 0, sfl, sv.select,"1", " ", null)%>
								</td>
									<td
				style="color: #434343;position: relative;   border-right: 1px solid #dddddd; border-top: 1px solid #dddddd;"
				align="left">
				<div class="input-group" readonly>
				<input name='<%="sjl54screensfl.clttwo" + "_R" +count %>' id='<%="sjl54screensfl.clttwo" + "_R"+count %>' type='text'
				value='<%=sv.clttwo.getFormData()%>' style="width: 95px !important;"
				maxLength='<%=sv.clttwo.getLength()%>'
				size='<%=sv.clttwo.getLength()%>' onFocus='doFocus(this)'
				onHelp='return fieldHelp(sjl54screensfl.clttwo)'
				onKeyUp='return checkMaxLength(this)'
		 		<%if ((new Byte((sv.clttwo).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()) {%>
				readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.clttwo).getHighLight()))
 				.compareTo(new Byte(BaseScreenData.BOLD)) == 0) { %>
 class="bold_cell" > 
				                                    
                     <span class="input-group-btn">
                        <button class="btn btn-info"
                              type="button" onClick="doFocus(document.getElementById('<%="sjl54screensfl.clttwo" + "_R" +count %>')); doAction('PFKEY04')">
                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                         </button>
                     </span>
                
			<%
 	} else {
 %> class='<%=(sv.clttwo).getColor() == null ? "input_cell"
									: (sv.clttwo).getColor().equals("red") ? "input_cell red reverse"
											: "input_cell"%>' >
				                                     
                     <span class="input-group-btn">
                        <button class="btn btn-info"
                              type="button" onClick="doFocus(document.getElementById('<%="sjl54screensfl.clttwo" + "_R" +count %>')); doAction('PFKEY04')">
                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                         </button>
                     </span>
               
                </div>
				
				<%
 	}
 %></td>
									
									 <td>
										<%if((new Byte((sv.clntID).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.clntID.getFormData();
												%>
										<div id="sjl54screensfl.clntID_R<%=count%>"
											name="sjl54screensfl.clntID_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td> 
									
									
									 <td>
										<%if((new Byte((sv.babrdc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.babrdc.getFormData();
												%>
										<div id="sjl54screensfl.babrdc_R<%=count%>"
											name="sjl54screensfl.babrdc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.babrdc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td> 
									
								
									 <td>
										<%if((new Byte((sv.accdesc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.accdesc.getFormData();
												%>
										<div id="sjl54screensfl.accdesc_R<%=count%>"
											name="sjl54screensfl.accdesc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.accdesc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td> 
									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<%
											if ((new Byte((sv.bankacckey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
															|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
														longValue = sv.bankacckey.getFormData();
										%>
										<div id="sjl54screensfl.bankacckey_R<%=count%>"
											name="sjl54screensfl.bankacckey_R<%=count%>"
											class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%
												if (longValue != null) {
											%>
											<%=longValue%>
											<%
												}
											%>
										</div> <%
									 	longValue = null;
									 %> <%
									 	} else {
									 %>

										<div class="input-group" style="width: 150px;">
											<input
												name='<%="sjl54screensfl" + "." + "bankacckey" + "_R" + count%>'
												id='<%="sjl54screensfl" + "." + "bankacckey" + "_R" + count%>'
												type='text' value='<%=sv.bankacckey.getFormData()%>'
												class=" <%=(sv.bankacckey).getColor() == null
								? "input_cell"
								: (sv.bankacckey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
												maxLength='<%=sv.bankacckey.getLength()%>'
												onFocus='doFocus(this)'
												onHelp='return fieldHelp(sjl54screensfl.bankacckey)'
												onKeyUp='return checkMaxLength(this)'
												style="width: <%=sv.bankacckey.getLength() * 12%> px;">
											<span class="input-group-btn">
												<button class="btn btn-info"  
													style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"
													;
                                 type="button"
													onClick="doFocus(document.getElementById('<%="sjl54screensfl" + "." + "bankacckey" + "_R" + count%>')); doAction('PFKEY04')">
													<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
												</button>
											</span>
										</div> <%
										 	}
										 %> <%
										 	}
										 %></div>
									</td>
					
									 <td>
										<%if((new Byte((sv.bankaccdsc).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.bankaccdsc.getFormData();
												%>
										<div id="sjl54screensfl.bankaccdsc_R<%=count%>"
											name="sjl54screensfl.bankaccdsc_R<%=count%>"
											<%if (!(((BaseScreenData) sv.bankaccdsc) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									
									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.prcent).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.prcent).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.prcent);
										%> <%
 	if ((new Byte((sv.prcent).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="sjl54screensfl.prcent_R<%=count%>"
											name="sjl54screensfl.prcent_R<%=count%>"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
											<%
												if (formatValue != null) {
											%>

											<%=formatValue%>


											<%
												}
											%>
										</div> <%
 	} else {
 %> <input type='text' value='<%=formatValue%>'
										<%if (qpsf.getDecimals() > 0) {%>
										size='<%=sv.prcent.getLength() + 1%>'
										maxLength='<%=sv.prcent.getLength() + 1%>' <%} else {%>
										size='<%=sv.prcent.getLength()%>'
										maxLength='<%=sv.prcent.getLength()%>' <%}%>
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sjl54screensfl.prcent)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sjl54screensfl" + "." + "prcent" + "_R" + count%>'
										id='<%="sjl54screensfl" + "." + "prcent" + "_R" + count%>'
										class=" <%=(sv.prcent).getColor() == null
								? "input_cell"
								: (sv.prcent).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
										style="width: <%=sv.prcent.getLength() * 12%> px;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>
										<%
											}
										%> <%
 	}
 %>
 </div>
									</td>
									
									<td>
									<div class="form-group">
										<%
											if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.pymt).getFieldName());
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													formatValue = smartHF.getPicFormatted(qpsf, sv.pymt);
										%> <%
 	if ((new Byte((sv.pymt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
										<div id="sjl54screensfl.pymt_R<%=count%>"
											name="sjl54screensfl.pymt_R<%=count%>"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>' align="right">
											<%
												if (formatValue != null) {
											%>

											<%=formatValue%>


											<%
												}
											%>
										</div> <%
 	} else {
 %> <input type='text' value='<%=formatValue%>'
										<%if (qpsf.getDecimals() > 0) {%>
										size='<%=sv.pymt.getLength() + 1%>'
										maxLength='<%=sv.pymt.getLength() + 1%>' <%} else {%>
										size='<%=sv.pymt.getLength()%>'
										maxLength='<%=sv.pymt.getLength()%>' <%}%>
										onFocus='doFocus(this)'
										onHelp='return fieldHelp(sjl54screensfl.pymt)'
										onKeyUp='return checkMaxLength(this)'
										name='<%="sjl54screensfl" + "." + "pymt" + "_R" + count%>'
										id='<%="sjl54screensfl" + "." + "pymt" + "_R" + count%>'
										class=" <%=(sv.pymt).getColor() == null
								? "input_cell"
								: (sv.pymt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
										style="width: <%=sv.pymt.getLength() * 12%> px;text-align: right;"
										onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
										decimal='<%=qpsf.getDecimals()%>'
										onPaste='return doPasteNumber(event);'
										onBlur='return doBlurNumber(event);' title='<%=formatValue%>'>
										<%
											}
										%> <%
 	}
 %>
 </div>
									</td>
								</tr>

								<%
									count = count + 1;
								Sjl54screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
							<%
								appVars.restoreAllInds(savedInds);
							%>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<div class="btn-group">
								<div class="sectionbutton">
									<p style="font-size: 12px; font-weight: bold;">
									<a id="subfile_add" class="btn btn-success" href='javascript:;'><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
									<a id="subfile_remove" class="btn btn-danger" href='javascript:;' disabled><%=resourceBundleHandler.gettingValueFromBundle("Remove")%></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div> 
			</div>		
		</div>
<script>
 $(document).ready(function(){
	 var x = $("select[id^='sjl54screensfl.clttwo_R']");
		for (var i = 0; i < x.length; i++) {
			x[i].onchange = function() {
				loadSelect(this);
			}
		}
		var x = $("select[id^='sjl54screensfl.bankacckey_R']");
		for (var i = 0; i < x.length; i++) {
			x[i].onchange = function() {
				loadSelect(this);
			}
		}	

    $("#subfile_remove").click(function () {
    	
		$('input:checkbox[type=checkbox]').prop('checked',false);
	    	
	   	$("#subfile_remove").attr('disabled', 'disabled'); 

    });  
    
    $('input[type="checkbox"]'). click(function(){
    	if($(this). prop("checked") == true){
    		 $('#subfile_remove').removeAttr('disabled');
    	}else{
    		 $('#subfile_remove').attr("disabled","disabled");   
    	}
    
    });
    
  });	 
</script>

<script>
$(document).ready(function() {
	$('#dataTables-sjl54').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: true,
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
  	});
})
</script>
</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>	
