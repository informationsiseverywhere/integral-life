<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SJL51";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sjl51ScreenVars sv = (Sjl51ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind30.isOn()) {
			sv.reqntype.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind30.isOn()) {
			sv.reqntype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.reqntype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind33.isOn()) {
			sv.claimnmber.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind33.isOn()) {
			sv.claimnmber.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.claimnmber.setColor(BaseScreenData.RED);
		}
		if (appVars.ind31.isOn()) {
			sv.paymentNum.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind31.isOn()) {
			sv.paymentNum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.paymentNum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind35.isOn()) {
			sv.bankcode.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind35.isOn()) {
			sv.bankcode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.bankcode.setColor(BaseScreenData.RED);
		}
		if (appVars.ind32.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind32.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.action.setColor(BaseScreenData.RED);
		}
	}
%>


<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
		
		<div class="col-md-4">
		     <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Number")%></label>
				
				<div class="input-group" style="max-width:120px">				
					<%=smartHF.getHTMLVarExt(fw, sv.claimnmber)%> 
					<span class="input-group-btn">
						<button class="btn btn-info" type="button"
							onclick="doFocus(document.getElementById('claimnmber')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
					
			</div> 
		</div>
		</div>

		<div class="col-md-4 ">
			<div class="form-group">
				<label><div class='label_txt' style='position:relative; bottom:3px;'><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></div></label>
					<div class="input-group" >
						<%
							if ((new Byte((sv.bankcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"bankcode"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("bankcode");
								optionValue = makeDropDownList(mappedItems, sv.bankcode.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.bankcode.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.bankcode, fw, longValue, "bankcode", optionValue)%>
						<%
							}
						%>
					</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Number")%></label>
				<div class="input-group" style="max-width:120px">				
					<%=smartHF.getHTMLVarExt(fw, sv.paymentNum)%> 
					<span class="input-group-btn">
						<button class="btn btn-info"
							 type="button"
							onclick="doFocus(document.getElementById('paymentNum')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>	
			</div>
		</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<style>
.radio-inline>div>label{

font-weight: normal !important;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"A",new StringData(resourceBundleHandler.gettingValueFromBundle("Create Claim Payment"))).replace("absolute","relative")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"B",new StringData(resourceBundleHandler.gettingValueFromBundle("Modify Claim Payment"))).replace("absolute","relative")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"C",new StringData(resourceBundleHandler.gettingValueFromBundle("Approve Claim Payment"))).replace("absolute","relative")%>
				</b></label>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"D",new StringData(resourceBundleHandler.gettingValueFromBundle("Authorise Claim Payment"))).replace("absolute","relative")%>
				</b></label>
			</div>
			<div class="col-md-4">
			<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"E",new StringData(resourceBundleHandler.gettingValueFromBundle("Remove Payment Request"))).replace("absolute","relative")%>
				</b></label>
			</div>
			<div class="col-md-4">
				<label class="radio-inline"><b><%=smartHF.getRadio(0, 0, fw,sv.action,"F",new StringData(resourceBundleHandler.gettingValueFromBundle("Enquire Claim Payment"))).replace("absolute","relative")%>
				</b></label>
			</div>
		</div>
			
		<div style='visibility: hidden;' class="row">
			<div class="col-md-4 col-md-offset-8">
				<input name='action' type='hidden'
					value='<%=sv.action.getFormData()%>'
					size='<%=sv.action.getLength()%>' onHelp='return fieldHelp(action)'
					maxLength='<%=sv.action.getLength()%>' class="input_cell"
					onFocus='doFocus(this)' onKeyUp='return checkMaxLength(this)'>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>

<script type="text/javascript">

	$(document).ready(function() {
    	exchangeAddon(".exchange_addon");
      	});

</script>  

<%@ include file="/POLACommon2NEW.jsp"%>
<script language="javascript">
$(document).ready(function(){
	//IFSU-551 begin
	
	<%if(browerVersion.equals("IE10")){%>	
		$("#payeeselImg").css("margin-left","-3px");
	<%}%>
});
</script>

