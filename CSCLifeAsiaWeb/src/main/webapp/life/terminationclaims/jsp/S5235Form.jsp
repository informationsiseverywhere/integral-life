


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5235";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5235ScreenVars sv = (S5235ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency of annuity payments                          ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payments in Advance                                    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payments in Arrears                                    ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Guaranteed payment period                              ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"years");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest used in Annuity calculation                   ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(optional)");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Capital content                                        ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"With proportion                                        ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Without proportion                                     ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Return of balance of Purchase Price                    ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Nominated Life                                         ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage on Death of Nominated Life                  ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage on Death of Other Life                      ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"N.B. A non-blank answer denotes a YES");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.advance.setReverse(BaseScreenData.REVERSED);
			sv.advance.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.advance.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.arrears.setReverse(BaseScreenData.REVERSED);
			sv.arrears.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.arrears.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.intanny.setReverse(BaseScreenData.REVERSED);
			sv.intanny.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.intanny.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.capcont.setReverse(BaseScreenData.REVERSED);
			sv.capcont.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.capcont.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.withprop.setReverse(BaseScreenData.REVERSED);
			sv.withprop.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.withprop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.withoprop.setReverse(BaseScreenData.REVERSED);
			sv.withoprop.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.withoprop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ppind.setReverse(BaseScreenData.REVERSED);
			sv.ppind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ppind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.dthpercn.setReverse(BaseScreenData.REVERSED);
			sv.dthpercn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.dthpercn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.dthperco.setReverse(BaseScreenData.REVERSED);
			sv.dthperco.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.dthperco.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.guarperd.setReverse(BaseScreenData.REVERSED);
			sv.guarperd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.guarperd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.nomlife.setReverse(BaseScreenData.REVERSED);
			sv.nomlife.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.nomlife.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.freqann.setReverse(BaseScreenData.REVERSED);
			sv.freqann.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.freqann.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>




<div class="panel panel-default">
      <div class="panel-body">     

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
<div style="width:150px">
	<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div></div>
		
		
		
		
		<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
		<div style="width:50px">
			<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div></div>
		
		
		
		
		<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
                       
                <div style="width:50px">       
                       	
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                       
                       
                     </div>  </div></div>
			
		</div>
		
		
		
		
		
		
		<div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		<table>
		<tr>
		<td>
			<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  
	<td>
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
  
  
  </div></div>
  
  
  
    <div class="col-md-4"> </div>
  
  <div class="col-md-4"> 
			    	  <div class="form-group">
                  
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
    <div style="width:50px"> 
  <%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div></div>
		
 
  </div>
		
		
		
		
			<div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
			
			<div class="input-group">
			<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:200px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		
		</div></div></div>
		
		</div>





<div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Frequency of annuity payments")%></label>
  <div class="input-group"> 

<input name='freqann' 
type='text' 
value='<%=sv.freqann.getFormData()%>' 
maxLength='<%=sv.freqann.getLength()%>' 
size='<%=sv.freqann.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(freqann)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.freqann).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.freqann).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('freqann')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('freqann')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.freqann).getColor()== null  ? 
"input_cell" :  (sv.freqann).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('freqann')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('freqann')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>


</div></div></div>

 



<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Payments in Advance")%></label>
                    <div style="width:45px">      
                       <input name='advance' 
type='text'

<%

		formatValue = (sv.advance.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.advance.getLength()%>'
maxLength='<%= sv.advance.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(advance)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.advance).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.advance).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.advance).getColor()== null  ? 
			"input_cell" :  (sv.advance).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>

                       
                       
                       
                       
                       
                       
                       <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Payments in Arrears")%></label>
  <div style="width:80px"> 
<input name='arrears' 
type='text'

<%

		formatValue = (sv.arrears.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.arrears.getLength()%>'
maxLength='<%= sv.arrears.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(arrears)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.arrears).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.arrears).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.arrears).getColor()== null  ? 
			"input_cell" :  (sv.arrears).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>


</div>



<div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Guaranteed payment period")%></label>
  <table><tr><td>
  <div style="width:45px"> 

<%	
			qpsf = fw.getFieldXMLDef((sv.guarperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='guarperd' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.guarperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
	 <%}%>

size='<%= sv.guarperd.getLength()%>'
maxLength='<%= sv.guarperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.guarperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.guarperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.guarperd).getColor()== null  ? 
			"input_cell" :  (sv.guarperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</td><td>
&nbsp;&nbsp;
 <label style="font-size: 13px;"><%=resourceBundleHandler.gettingValueFromBundle("years")%> </label> 
</td></tr></table>
</div></div>





<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Interest used in Annuity calculation")%></label>
                       
      <table><tr><td>
  <div style="width:80px">                  
                       
                       
                     <%	
			qpsf = fw.getFieldXMLDef((sv.intanny).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='intanny' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.intanny) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.intanny);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.intanny) %>'
	 <%}%>

size='<%= sv.intanny.getLength()%>'
maxLength='<%= sv.intanny.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intanny)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.intanny).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intanny).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intanny).getColor()== null  ? 
			"input_cell" :  (sv.intanny).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div></td><td>
&nbsp;&nbsp;
<label style="font-size: 13px;"><%=resourceBundleHandler.gettingValueFromBundle("(optional)")%></label>  
</td></tr></table>

</div></div>  
                       
                       
   <div class="col-md-4"> 
			    	  <div class="form-group" style="width: 155px;">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Capital content")%></label>
                       
	<%	
			qpsf = fw.getFieldXMLDef((sv.capcont).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			
	%>

<input name='capcont' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.capcont) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.capcont);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.capcont) %>'
	 <%}%>

size='<%= sv.capcont.getLength()%>'
maxLength='<%= sv.capcont.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(capcont)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.capcont).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.capcont).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.capcont).getColor()== null  ? 
			"input_cell" :  (sv.capcont).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</div></div>
   </div>
   
   
   
   
   <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("With proportion")%></label>
                 <div style="width:45px">        
                       <input name='withprop' 
type='text'

<%

		formatValue = (sv.withprop.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.withprop.getLength()%>'
maxLength='<%= sv.withprop.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(withprop)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.withprop).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.withprop).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.withprop).getColor()== null  ? 
			"input_cell" :  (sv.withprop).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</div></div></div>
                       

<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Without proportion")%></label>
                    <div style="width:45px">          
                       <input name='withoprop' 
type='text'

<%

		formatValue = (sv.withoprop.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.withoprop.getLength()%>'
maxLength='<%= sv.withoprop.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(withoprop)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.withoprop).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.withoprop).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.withoprop).getColor()== null  ? 
			"input_cell" :  (sv.withoprop).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>


                       
                       
                       
                       
                       
                       
                       <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Return of balance of Purchase Price")%></label>
  <div style="width:45px">     
<input name='ppind' 
type='text'

<%

		formatValue = (sv.ppind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ppind.getLength()%>'
maxLength='<%= sv.ppind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ppind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ppind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ppind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ppind).getColor()== null  ? 
			"input_cell" :  (sv.ppind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>

</div>







<div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Nominated Life")%></label>
  <div  class="input-group" style="width:100px">     
<input name='nomlife' 
type='text' 
value='<%=sv.nomlife.getFormData()%>' 
maxLength='<%=sv.nomlife.getLength()%>' 
size='<%=sv.nomlife.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(nomlife)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.nomlife).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.nomlife).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('nomlife')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('nomlife')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
<%
	}else { 
%>

class = ' <%=(sv.nomlife).getColor()== null  ? 
"input_cell" :  (sv.nomlife).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('nomlife')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('nomlife')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>

</div></div></div>




<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage on Death of Nominated Life")%></label>
  <div style="width:80px">     

<%	
			qpsf = fw.getFieldXMLDef((sv.dthpercn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='dthpercn' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthpercn);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
	 <%}%>

size='<%= sv.dthpercn.getLength()%>'
maxLength='<%= sv.dthpercn.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthpercn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthpercn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthpercn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthpercn).getColor()== null  ? 
			"input_cell" :  (sv.dthpercn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</div></div></div>


<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Percentage on Death of Other Life")%></label>

  <div style="width:80px">     
<%	
			qpsf = fw.getFieldXMLDef((sv.dthperco).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='dthperco' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthperco);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
	 <%}%>

size='<%= sv.dthperco.getLength()%>'
maxLength='<%= sv.dthperco.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthperco)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthperco).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthperco).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthperco).getColor()== null  ? 
			"input_cell" :  (sv.dthperco).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>





</div></div></div>


</div>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("N.B. A non-blank answer denotes a YES")%>
</div>

</div></div>






























































































<%@ include file="/POLACommon2NEW.jsp"%>

