<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6695";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6695ScreenVars sv = (S6695ScreenVars) fw.getVariables();%>
<%{
	if (appVars.ind01.isOn()) {
		sv.contitem.setInvisibility(BaseScreenData.INVISIBLE);
	}
}%>
	<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-3"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-2"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			 </div>      
			  
			    <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective/To"))%></label>
                        <div class="input-group">  <%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%>

        		</div></div>
        	</div>
        </div>   
        
        &nbsp;
         <div class="row">
              <div class="col-md-12">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Increase benefit by the following parameters"))%></label>
        				
        			</div>
        	</div>
        </div>				
  
           &nbsp;
           
           <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Term Up to year"))%></label>
        				
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Percentage Increase"))%></label>
        				
        			</div>
        	</div>
        </div>	
        <!-- 1 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr01,( sv.tupyr01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc01,( sv.pctinc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 2 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr02,( sv.tupyr02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc02,( sv.pctinc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
        <!-- 3 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr03,( sv.tupyr03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc03,( sv.pctinc03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 4 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr04,( sv.tupyr04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc04,( sv.pctinc04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 5 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr05,( sv.tupyr05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc05,( sv.pctinc05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 6 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr06,( sv.tupyr06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc06,( sv.pctinc06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 7 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr07,( sv.tupyr07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc07,( sv.pctinc07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 8 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr08,( sv.tupyr08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc08,( sv.pctinc08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 9 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr09,( sv.tupyr09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc09,( sv.pctinc09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 10 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr10,( sv.tupyr10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc10,( sv.pctinc10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 11 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr11,( sv.tupyr11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc11,( sv.pctinc11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
         <!-- 12 -->
        <div class="row">
              <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.tupyr12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr12,( sv.tupyr12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        	
        	<div class="col-md-3"></div>
        	
        	 <div class="col-md-3">
                <div class="form-group" style="max-width:75px;">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%if(((BaseScreenData)sv.pctinc12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc12,( sv.pctinc12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
        			</div>
        	</div>
        </div>	
        
    <%if (!appVars.ind01.isOn()) { %>
  <div class="row">	
			    	<div class="col-md-4">          
		<div class="form-group">  
		<label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
	
		<%=smartHF.getHTMLVarExt(fw, sv.contitem)%>
		
		</div></div><div class="col-md-4"> </div><div class="col-md-4"> </div></div>
		<%} %>
        
</div>
</div>

<script>
$(document).ready(function() {
$('#contitem').css("width","80");
});
</script>
































<%-- <div class='outerDiv' style='width:750;height:500;'><!-- overflow removed by pmujavadiya -->
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<br/>
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>
<br/>
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>
<br/>
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData ITMFRM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective/To");%>
<%=smartHF.getLit(0, 0, ITMFRM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective/To")%>
</div>
<br/>
<%=smartHF.getRichText(0, 0, fw, sv.itmfrmDisp,(sv.itmfrmDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmfrmDisp).replace("absolute","relative")%></td>
<!-- ILIFE-2674 commented by pmujavadiya -->
<%=smartHF.getRichText(0, 0, fw, sv.itmtoDisp,(sv.itmtoDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.itmtoDisp).replace("absolute","relative")%></td>
</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData S6695_1_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Increase benefit by the following parameters");%>
<%=smartHF.getLit(0, 0, S6695_1_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Increase benefit by the following parameters")%>
</div>
</td>
<td></td>
<td></td>
</tr> </table>

<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData S6695_2_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Term Up to year");%>
<%=smartHF.getLit(0, 0, S6695_2_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Term Up to year")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData TUPYR01_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage Increase");%>
<%=smartHF.getLit(0, 0, TUPYR01_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Percentage Increase")%>
</div>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr01,( sv.tupyr01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc01,( sv.pctinc01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr02,( sv.tupyr02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc02,( sv.pctinc02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr03,( sv.tupyr03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc03,( sv.pctinc03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr04,( sv.tupyr04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc04,( sv.pctinc04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr05,( sv.tupyr05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc05) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc05,( sv.pctinc05.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc05) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr06,( sv.tupyr06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc06) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc06,( sv.pctinc06.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc06) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr07,( sv.tupyr07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc07) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc07,( sv.pctinc07.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc07) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr08,( sv.tupyr08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc08) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc08,( sv.pctinc08.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc08) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr09,( sv.tupyr09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc09) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc09,( sv.pctinc09.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc09) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr10,( sv.tupyr10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc10) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc10,( sv.pctinc10.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc10) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr11,( sv.tupyr11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc11) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc11,( sv.pctinc11.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc11) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.tupyr12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.tupyr12,( sv.tupyr12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.tupyr12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.tupyr12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.pctinc12) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.pctinc12,( sv.pctinc12.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.pctinc12) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.pctinc12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr> </table>
<br/>
</div> --%>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
