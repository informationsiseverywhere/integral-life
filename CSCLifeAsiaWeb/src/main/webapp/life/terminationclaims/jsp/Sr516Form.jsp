
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR516";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr516ScreenVars sv = (Sr516ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. fund ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-sect ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit amount ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess Age / Term ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cess date ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess Age / Term ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem cess date ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bene cess Age / Term ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bene cess date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality class ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl Method ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded Premium ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special terms ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reassurance   ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annuity details ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total policies in plan ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life (J/L) ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number available ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number applicable ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium with Tax  ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Details ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stamp Duty");%>
<%{
		appVars.rolldown(new int[] {10});
		appVars.rollup(new int[] {10});
		if (appVars.ind42.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText10.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.zrsumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.zrsumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.zrsumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.zrsumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
			sv.zrsumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.frqdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind53.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
			generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText19.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			generatedText20.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind39.isOn()) {
			sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			generatedText21.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText22.setInvisibility(BaseScreenData.INVISIBLE);
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.ratypind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind40.isOn()) {
			sv.ratypind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.ratypind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.ratypind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ratypind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind33.isOn()) {
			sv.anntind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.anntind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind34.isOn()) {
			sv.anntind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.anntind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText27.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numavail.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numapp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
			sv.numapp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind36.isOn()) {
			sv.numapp.setReverse(BaseScreenData.REVERSED);
			sv.numapp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.numapp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind38.isOn()) {
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind37.isOn()) {
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind42.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-3188--started*/
		if (appVars.ind60.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		/*ILIFE-3188--ended*/
		if (appVars.ind42.isOn()) {
			generatedText32.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind42.isOn()) {
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		/*BRD-306 END */
		if (appVars.ind54.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind60.isOn()) {
			sv.waitperiod.setReverse(BaseScreenData.REVERSED);
			sv.waitperiod.setColor(BaseScreenData.RED);
		}
		if (appVars.ind62.isOn()) {
			sv.bentrm.setReverse(BaseScreenData.REVERSED);
			sv.bentrm.setColor(BaseScreenData.RED);
		}
		if (appVars.ind64.isOn()) {
			sv.poltyp.setReverse(BaseScreenData.REVERSED);
			sv.poltyp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind66.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
			sv.prmbasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.waitperiod.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind62.isOn()) {
			sv.bentrm.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind64.isOn()) {
			sv.poltyp.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind64.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.waitperiod.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind63.isOn()) {
			sv.bentrm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind65.isOn()) {
			sv.poltyp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		
		//ILJ-43
		if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}	
		//end
	}

	%>


<div class="panel panel-default">
        	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr>
								<td>
								
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' >
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
								</td>
								<td >
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
															
																	if(longValue == null || longValue.equalsIgnoreCase("")) {
																		formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
																	} else {
																		formatValue = formatValue( longValue);
																	}
																	
																	
															} else  {
																		
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																		formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
																	} else {
																		formatValue = formatValue( longValue);
																	}
															
															}
															%>			
														<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
																"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
														<%=XSSFilter.escapeHtml(formatValue)%>
													</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
								</td>
								<td >
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width:400px;min-width: 100px;margin-left: 1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td>
								</tr></table>
						
							
						</div>											
					</div>
				</div>
	<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						 
						<table><tr><td>
						<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:65px;max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td >
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:65px;max-width:200px;margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
		 
		</div>
		</div>
		<div class="col-md-4"></div>
			<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
	
<div class="input-group" style="max-width:40px;"> <%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
				
		<%
			} else {
		%>
		
		<div class="blank_cell">&nbsp;</div>
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	</div>
		</div></div></div>
		<div class="row">
		<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table><tr><td>
						<%					
if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="min-width:71px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="min-width:71px;"></div>
		
		<% 
			} 
		%>
		
<%
longValue = null;
formatValue = null;
%>
</td>
<td>
<div 	
	class='<%= (sv.jlinsname.getFormData()).trim().length() == 0 ? 
					"blank_cell" : "output_cell" %>' style="min-width:71px;max-width:200px;margin-left:1px">
<%					
if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>
		<%=XSSFilter.escapeHtml(formatValue)%>
	</div>	
<%
longValue = null;
formatValue = null;
%>					</td></tr></table>
						 				
					</div>
				</div>			
				</div>
<div class="row">
		<div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
						<div class="form-group" style="max-width:50px;"><%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		</div>
<div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
						<div class="form-group" style="max-width:50px;">
						<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			
		</div></div>
<div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
						<div class="form-group" style="max-width:50px;">
							<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</div></div>
<div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. fund")%></label>
						<div class="form-group" style="max-width:80px;">
						<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>
		<%
		longValue = null;
		formatValue = null;
		%>
 </div></div> 
<div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
						<div class="form-group" style="max-width:50px;">
						  		
		<%					
		if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	 </div></div>
 <div class="col-md-2">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-sect")%></label>
						<div class="form-group" style="max-width:50px;"> 
							
		<%					
		if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:73px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div></div>
		</div>
		<br>
		<hr/>
		<br>
<div class="row">
		<div class="col-md-3">
		<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Amount")%></label>
				<table>
				<tr>
				<td>
				<%	
			qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.zrsumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='zrsumin' 
type='text'
<%if((sv.zrsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 139px"<% }%>
	value='<%=valueThis%>'
			 <%
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.zrsumin.getLength(), sv.zrsumin.getScale(),3)%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.zrsumin.getLength(), sv.zrsumin.getScale(),3)%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(zrsumin)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.zrsumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.zrsumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.zrsumin).getColor()== null  ? 
			"input_cell" :  (sv.zrsumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
 <td style="padding-left:2px;">	
		<%					
		if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %> ' style="width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

		</td> </tr></table>				
			</div>
			</div>		
			
			
			
			<%if((new Byte((generatedText18).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>
		<div class="col-md-3">
					<div class="form-group" style="min-width:200px;">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>
		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
   <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:200px;" : "width:200px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mortcls).getColor())){
%>
<div style="border:2px; border-style: solid; border-color: #ec7572;  width:203px;"> 
<%
} 
%>

<select name='mortcls' type='list' style="width:200px;"
<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mortcls).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mortcls).getColor())){
%>
</div>
<%
} 
%>
<%
formatValue=null;
longValue = null;
%>
<%
} 
%>
</div></div><%} %>
<div class="col-md-3">
<%
longValue = null;
formatValue=null;
%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:82px;" %>'> 
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</div>
		
					<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>	<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label><%}%>
		<%	
	if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("prmbasis");
	optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.prmbasis).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='prmbasis' type='list' style="width:145px;"
<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.prmbasis).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.prmbasis).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	
</div>
<div class="row">
		<div class="col-md-3">
					<div class="form-group">
						<!--  ILJ-43 -->
						<%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term")%></label>
						<% } else{%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
						<% } %>
						<!--  END  -->	
						
					<!-- <div class="row">
						<div class="col-md-3"> -->
						<table>
						<tr>
						<td>
							<%	
			qpsf = fw.getFieldXMLDef((sv.riskCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='riskCessAge' 
type='text'
<%if((sv.riskCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width:  75px "<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.riskCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.riskCessAge) %>'
	 <%}%>

size='<%= sv.riskCessAge.getLength()%>'
maxLength='<%= sv.riskCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.riskCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.riskCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.riskCessAge).getColor()== null  ? 
			"input_cell" :  (sv.riskCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

						<!-- </div> -->
						</td>
						<td>
						<!-- <div class="col-md-2"> -->

							<%
								qpsf = fw.getFieldXMLDef((sv.riskCessTerm).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>

							<input name='riskCessTerm' type='text'
								<%if ((sv.riskCessTerm).getClass().getSimpleName()
					.equals("ZonedDecimalData")) {%>
								style="text-align: right; width:  75px" <%}%>
								value='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%valueThis = smartHF.getPicFormatted(qpsf, sv.riskCessTerm);
			if (valueThis != null && valueThis.trim().length() > 0) {%>
								title='<%=smartHF.getPicFormatted(qpsf, sv.riskCessTerm)%>'
								<%}%> size='<%=sv.riskCessTerm.getLength()%>'
								maxLength='<%=sv.riskCessTerm.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessTerm)'
								onKeyUp='return checkMaxLength(this)' style="width:73px;"
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>'
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
								<%if ((new Byte((sv.riskCessTerm).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
								readonly="true" class="output_cell" style="width:73px;"
								<%} else if ((new Byte((sv.riskCessTerm).getHighLight()))
					.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" style="width:73px;" <%} else {%>
								class=' <%=(sv.riskCessTerm).getColor() == null ? "input_cell"
						: (sv.riskCessTerm).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
								style="width:73px;" <%}%>>
						<!-- </div> -->
					<!-- </div> -->
					</td>
					</tr>
					</table>
					</div>
				</div>
<div class="col-md-3">
					<div class="form-group">
						<!--  ILJ-43 -->
						<%
						if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
						<% } else{%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess date")%></label>
						<% } %>
						<!--  END  -->	
						
						<%	
	longValue = sv.riskCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
 <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='riskCessDateDisp' 
type='text' 
value='<%=sv.riskCessDateDisp.getFormData()%>' 
maxLength='<%=sv.riskCessDateDisp.getLength()%>' 
size='<%=sv.riskCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(riskCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.riskCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	style="width:140px;"

<%
	}else if((new Byte((sv.riskCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:145px;"
 
<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp,(sv.riskCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%
	}else { 
%>

class = ' <%=(sv.riskCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.riskCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:145px;">

<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp,(sv.riskCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>

</div></div>
<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
						<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
   <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.liencd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='liencd' type='list' style="width:145px;"
<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.liencd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.liencd).getColor())){
%>
</div>
<%
} 
%>
<%
formatValue=null;
longValue = null;
%>
<%
} 
%>
						
</div></div>
		
						<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %><label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label><%}%>
						<%	
	if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"waitperiod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("waitperiod");
	optionValue = makeDropDownList( mappedItems , sv.waitperiod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.waitperiod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.waitperiod).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='waitperiod' type='list' style="width:145px;"
<% 
	if((new Byte((sv.waitperiod).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.waitperiod).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.waitperiod).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>
</div>
<div class="row">
		<div class="col-md-3">
			<div class="form-group">		
						<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
			<table><tr>
						<td>			
						<%	
			qpsf = fw.getFieldXMLDef((sv.premCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='premCessAge' 
type='text'
<%if((sv.premCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width: 75px" <% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessAge) %>'
	 <%}%>

size='<%= sv.premCessAge.getLength()%>'
maxLength='<%= sv.premCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessAge).getColor()== null  ? 
			"input_cell" :  (sv.premCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>






	<%	
			qpsf = fw.getFieldXMLDef((sv.premCessTerm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
</td>
						<td>
<input name='premCessTerm' 
type='text'
<%if((sv.premCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; margin-left:1px;width: 75px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.premCessTerm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.premCessTerm) %>'
	 <%}%>

size='<%= sv.premCessTerm.getLength()%>'
maxLength='<%= sv.premCessTerm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessTerm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.premCessTerm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.premCessTerm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.premCessTerm).getColor()== null  ? 
			"input_cell" :  (sv.premCessTerm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table></div></div>
<div class="col-md-3">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess date")%></label>
						<div class="form-group">
			<%	
	longValue = sv.premCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
 <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='premCessDateDisp' 
type='text' 
value='<%=sv.premCessDateDisp.getFormData()%>' 
maxLength='<%=sv.premCessDateDisp.getLength()%>' 
size='<%=sv.premCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(premCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.premCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	style="width:140px;"

<%
	}else if((new Byte((sv.premCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:145px;"
 
<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp,(sv.premCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%
	}else { 
%>

class = ' <%=(sv.premCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.premCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:145px;">

<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp,(sv.premCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>			
						</div></div>
<div class="col-md-3">
					<div class="form-group">
						 <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%></label>
						
		<% 
	if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("J")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("J");
	}
	if(((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("L")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("L");
    }
	 
%>

<% 
	if((new Byte((sv.select).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
   <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.select).getColor())){
					%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
					<%
					} 
					%>

<select name='select' style="width:80px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(select)'
	onKeyUp='return checkMaxLength(this)'
					<% 
				if((new Byte((sv.select).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.select).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
	class = 'input_cell' 
			<%
				} 
			%>
			>
					
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="J"<% if(((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("J")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("J")%></option>
<option value="L"<% if(((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("L")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("L")%></option>


					</select>
					<% if("red".equals((sv.select).getColor())){
					%>
					</div>
					<%
					} 
					%>

					<%
}longValue = null;} 
					%>
		</div></div>
		
		<div class="col-md-3">
					
						<%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {%><label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%><%}%></label>	
			<%	
	if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bentrm"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bentrm");
	optionValue = makeDropDownList( mappedItems , sv.bentrm.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bentrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
                       style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.bentrm).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='bentrm' type='list' style="width:145px;"
<% 
	if((new Byte((sv.bentrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.bentrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.bentrm).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>
</div></div>
<div class="row">
		<div class="col-md-3">
						<div class="form-group">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age / Term")%></label>
						<table><tr>
						<td>
					

	<%	
			qpsf = fw.getFieldXMLDef((sv.benCessAge).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='benCessAge' 
type='text'
<%if((sv.benCessAge).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: left; width: 75px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.benCessAge) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.benCessAge);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.benCessAge) %>'
	 <%}%>

size='<%= sv.benCessAge.getLength()%>'
maxLength='<%= sv.benCessAge.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(benCessAge)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.benCessAge).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.benCessAge).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.benCessAge).getColor()== null  ? 
			"input_cell" :  (sv.benCessAge).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
<td>



	<%	
			qpsf = fw.getFieldXMLDef((sv.benCessTerm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='benCessTerm' 
type='text'
<%if((sv.benCessTerm).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; margin-left: 1px; width: 75px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.benCessTerm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.benCessTerm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.benCessTerm) %>'
	 <%}%>

size='<%= sv.benCessTerm.getLength()%>'
maxLength='<%= sv.benCessTerm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(benCessTerm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.benCessTerm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.benCessTerm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.benCessTerm).getColor()== null  ? 
			"input_cell" :  (sv.benCessTerm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td></tr></table></div></div>

<div class="col-md-3">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess date")%></label>
						<div class="form-group" style="max-width:100px;">

<%	
	longValue = sv.benCessDateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.benCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
 <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='benCessDateDisp' 
type='text' 
value='<%=sv.benCessDateDisp.getFormData()%>' 
maxLength='<%=sv.benCessDateDisp.getLength()%>' 
size='<%=sv.benCessDateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(benCessDateDisp)' onKeyUp='return checkMaxLength(this)'  
	
<% 
	if((new Byte((sv.benCessDateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	style="width:140px;"

<%
	}else if((new Byte((sv.benCessDateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" style="width:145px;"
 
<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp,(sv.benCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%
	}else { 
%>

class = ' <%=(sv.benCessDateDisp).getColor()== null  ? 
"input_cell" :  (sv.benCessDateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' style="width:145px;">

<div class="input-group" id="dateRangePicker">
 <%=smartHF.getRichTextDateInput(fw, sv.benCessDateDisp,(sv.benCessDateDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>
</div></div>
<div class="col-md-3">
		<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>			
						<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
						<%}%>
						
						<div class="form-group" style="max-width:100px;">

<%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.dialdownoption).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='dialdownoption' type='list' style="width:130px;"
<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dialdownoption).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dialdownoption).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	

</div></div>
<!-- BRD-NBP-011 ends -->
<div class="col-md-3">
					
						<%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %><label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label><%}%>
						<div class="form-group" style="max-width:100px;">

<%	
	if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"poltyp"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("poltyp");
	optionValue = makeDropDownList( mappedItems , sv.poltyp.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.poltyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px" : "width:140px" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.poltyp).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='poltyp' type='list' style="width:145px;"
<% 
	if((new Byte((sv.poltyp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.poltyp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.poltyp).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	

</div></div>

<%if((new Byte((generatedText20).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>
<div class="col-md-3">
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>
						<div class="form-group" style="max-width:100px;">

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bappmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bappmeth");
	optionValue = makeDropDownList( mappedItems , sv.bappmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
   <div  class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
		style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.liencd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='liencd' type='list' style="width:145px;"
<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.bappmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.bappmeth).getColor())){
%>
</div>
<%
} 
%>
<%
formatValue=null;
longValue = null;
%>
<%
} 
%>
</div>
</div>


<%} %>
</div>

<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
<div class="row">
		<div class="col-md-3">
					
				<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%><label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label><%} %>

						
<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
 <% }%> 
 <% if((browerVersion.equals(IE8)) ) {%>
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
 <% }%>
 </div> 
 	<div class="col-md-3">
<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label><%} %>
						
<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8))) {%>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
</div>
<div class="col-md-3">
	<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>				
						<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label><%} %>
						<div class="form-group" style="max-width:100px;">
		<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8)) ) {%>
<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
</div></div>
<div class="col-md-3">
	<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>				
						<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label><%} %>
						<div class="form-group" style="max-width:100px;">
						<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8)) ) {%>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
</div></div>										
</div>									
<%
} 
%>
	
<div class="row">
<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
<div class="col-md-3">
<div class="form-group">
<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>

<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>				
						<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label><%} %>
						
<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8))) {%>
<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%><%} %>
</div>
</div>
<%}%>
<div class="col-md-3">
<div class="form-group">
		
					<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
						<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
						<%} else { %>
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label><%} %>
						<%}%>
						<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
<%if(sv.zlinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8)) ) {%>
<%if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
<%if(sv.zlinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
</div>
</div>

<div class="col-md-3">
<div class="form-group">		
<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>					
							
						<%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label><%}%>
								
						
<% if((browerVersion.equals(IE11)) || (browerVersion.equals(Chrome))) {%>
<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'min-width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'min-width: 145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%>
<% if((browerVersion.equals(IE8)) ) {%>
<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'min-width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'min-width: 145px !important; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
<%}%><%} %>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
<%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>				
						<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>	<%} %>
						
						<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
<%if(sv.zstpduty01.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>	
<%} %>
<%}else {%>
<%}%>
</div>
</div>


</div>

<div class="row">
		<div class="col-md-3">
		<div class="form-group">
	<%if((new Byte((generatedText22).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>				
						<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>	
						
						
	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<!--ILIFE-1474 STARTS-->
<input  name='instPrem' 
type='text'
<%if((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px"<% }%>
	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)+3%>'
maxLength='<%= COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)-3%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" 
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
<%} %>


<div class="col-md-3">
					<div class="form-group">
					<%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>

<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='taxamt' 
type='text'

<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align:right;width:145px"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.taxamt.getLength(), sv.taxamt.getScale(),3)%>'
maxLength='<%= sv.taxamt.getLength()%>' 
onFocus='doFocus(this) ,onFocusRemoveCommas(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  
	style="width:140px;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell" style="width:140px;"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  style="width:140px;"

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>

<%}%>

 <!-- ILIFE-912 End -- Screen Sr516 has UI issues - Field Alignment issue -->


<!-- ILIFE-961 STARTS -skumar498 -->


</div></div>

</div>



<div class="row">
<%if((new Byte((generatedText25).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>

<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Total policies in plan")%></label>

<%if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.polinc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='polinc' 
type='text'

<%if((sv.polinc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.polinc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.polinc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.polinc) %>'
	 <%}%>

size='<%= sv.polinc.getLength()+13%>'
maxLength='<%= sv.polinc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(polinc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.polinc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.polinc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.polinc).getColor()== null  ? 
			"input_cell" :  (sv.polinc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
	
 
	

</div>
</div>
<%} %>


<%if((new Byte((generatedText27).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>
<div class="col-md-4">
<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Number available")%></label>


<%if ((new Byte((sv.numavail).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.numavail).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='numavail' 
type='text'

<%if((sv.numavail).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.numavail) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.numavail);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.numavail) %>'
	 <%}%>

size='<%= sv.numavail.getLength()+13%>'
maxLength='<%= sv.numavail.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(numavail)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.numavail).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.numavail).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.numavail).getColor()== null  ? 
			"input_cell" :  (sv.numavail).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
	
 
	

</div>
</div>
<%} %>
</div>	

<div class="row">

<%if((new Byte((generatedText28).getInvisible()))
	.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){  %>
<div class="col-md-4">
<div class="form-group">

<label>
<%=resourceBundleHandler.gettingValueFromBundle("Number applicable")%>
</label>


	<%if ((new Byte((sv.numapp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.numapp).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='numapp' 
type='text'

<%if((sv.numapp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.numapp) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.numapp);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.numapp) %>'
	 <%}%>

size='<%= sv.numapp.getLength()+13%>'
maxLength='<%= sv.numapp.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(numapp)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.numapp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.numapp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.numapp).getColor()== null  ? 
			"input_cell" :  (sv.numapp).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
	
 
	
</div>
</div>
<%} %>

</div>

 <!-- ILIFE-912 Start -- Screen Sr516 has UI issues - Field Alignment issue -->
	<Div id='mainForm_OPTS' style='visibility:hidden'>
<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
<%=smartHF.getMenuLink(sv.taxind, resourceBundleHandler.gettingValueFromBundle("Tax Detail"))%>
<%=smartHF.getMenuLink(sv.ratypind, resourceBundleHandler.gettingValueFromBundle("Reassurance"))%>
<%=smartHF.getMenuLink(sv.anntind, resourceBundleHandler.gettingValueFromBundle("Annuity Details"))%>
<%=smartHF.getMenuLink(sv.exclind, resourceBundleHandler.gettingValueFromBundle("Exclusions"))%>
<%=smartHF.getMenuLink(sv.pbind, resourceBundleHandler.gettingValueFromBundle("Premium Breakdown"))%>
</div>	


<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.crtabtitl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabtitl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabtitl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
</tr></table></div>


</div>
</div>

		
			

<%@ include file="/POLACommon2NEW.jsp"%>
<!-- Cross Browser - ILIFE-2143 by fwang3 -->	
