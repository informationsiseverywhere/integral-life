

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S6307";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>

<%S6307ScreenVars sv = (S6307ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind07.isOn()) {
			sv.percreqd.setReverse(BaseScreenData.REVERSED);
			sv.percreqd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.percreqd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.percreqd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.actvalue.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind20.isOn()) {
			sv.actvalue.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.actvalue.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.actvalue.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind31.isOn()) {
			sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
				}
				
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind32.isOn()) {
			sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
				}
				
				 if(appVars.ind69.isOn()){
					sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
					sv.reserveUnitsDate.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind57.isOn()) {
						sv.bankacckey.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind62.isOn()) {
						sv.crdtcrd.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind56.isOn()) {
						sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
					}
				 if (appVars.ind61.isOn()) {
						sv.crdtcrd.setEnabled(BaseScreenData.DISABLED);
					}
				 if (appVars.ind55.isOn()) {
						sv.bankacckey.setReverse(BaseScreenData.REVERSED);
						sv.bankacckey.setColor(BaseScreenData.RED);
					}
				 if (appVars.ind60.isOn()) {
						sv.crdtcrd.setReverse(BaseScreenData.REVERSED);
						sv.crdtcrd.setColor(BaseScreenData.RED);
					}
				 if (!appVars.ind55.isOn()) {
						sv.bankacckey.setHighLight(BaseScreenData.BOLD);
					}
				 if (!appVars.ind60.isOn()) {
						sv.crdtcrd.setHighLight(BaseScreenData.BOLD);
					}
				 
				 if (appVars.ind80.isOn()) {
						sv.reqntype.setReverse(BaseScreenData.REVERSED);
						sv.reqntype.setColor(BaseScreenData.RED);
					}
				 if (appVars.ind88.isOn()) {
						 sv.payee.setInvisibility(BaseScreenData.INVISIBLE);
						 sv.payee.setEnabled(BaseScreenData.DISABLED);
					}
				 if (appVars.ind18.isOn()) {
						sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
					}	
	}

	%>

	<%StringData generatedText14 = new StringData("------------------------- Surrender Details ----------------------------------");%>
	<%StringData generatedText17 = new StringData("Effective Date ");%>
	<%StringData generatedText16 = new StringData("Total     ");%>
	<%StringData generatedText18 = new StringData("Currency       ");%>
	<%StringData generatedText15 = new StringData("Total Fee ");%>
	<%StringData generatedText19 = new StringData("Total Amount   ");%>
	<%StringData generatedText20 = new StringData("Total %        ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind20.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.totalfee.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind20.isOn()) {
			sv.totalamt.setEnabled(BaseScreenData.DISABLED);
			
		}
		if (appVars.ind03.isOn()) {
			sv.totalamt.setReverse(BaseScreenData.REVERSED);
			sv.totalamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.totalamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.prcnt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.prcnt.setReverse(BaseScreenData.REVERSED);
			sv.prcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.prcnt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number   ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life   ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date      ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date    ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------- Coverage/Rider Details------------------------------");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Suffix No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                           Actual           Estimated");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Co/Rd Fund Typ Descriptn     Ccy    %                Value               Value");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind01.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText5.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>






 <div class="panel panel-default">
      <div class="panel-body">     

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
                       <table>
                       <tr>
                       <td>
<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	</td>
  	<td>	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  <td>	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 160px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</td>
		</tr>
		</table>
		</div></div>
		
		
	
		<div class="col-md-4"> 	
			    	  <div class="form-group" >
                    
                       <!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
                       <div class="input-group" >
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div> </div>
		
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
                         <table>
                         <tr>
                         <td>
		
		<%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>	
  		
		<%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
	</td>
	</tr>
	</table>
	
	</div></div>
		
		</div>
		
		
		
		
		 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
                         <table>
                         <tr>
                         <td>
		
			<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  
  <td>
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
	</td>
	</tr>
	</table>
	
		
		</div></div>
		
		</div>
		
		
		
		 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
                      <table>
                      <tr>
                      <td>
                         
                         	
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                         
        </td>
        </tr>
        </table>                 
                         </div></div>
                         </div>
                         
                         
                         
                         
                          <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
                        <table>
                        <tr>
                        <td>
                         
                         <%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>
	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                         
     </td>
     </tr>
     </table>
                         
                         </div></div>
                         </div>
		
		
		
		
		
		
		
		 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
                       <div class="input-group">   
                       <%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
		
		
		</div></div> </div>
		
		
		<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:75px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div>
		
		
		
		<div class="col-md-4"> 
			    	  <div class="form-group" >
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix No")%></label>
		
			<%					
		if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div>
		</div>
		
		
		
		
		
		
		
		
		
		<div class="row">
				<div class="col-md-12">
				
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Details")%>	</label></a>
						</li>
						<li><a href="#tab2" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></label></a>
						</li>
						<%if((new Byte((sv.payee).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
						<li><a href="#third_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Payout Details")%></label></a>
						<%}%>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1">
								
								
								
								
								<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		<div class="input-group">
		<%	
	longValue = sv.effdateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="premcessDisp" data-link-format="dd/mm/yyyy"> 
<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>
 --%>
 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
 
<%} %>
</div>
<%longValue = null;} %>




		</div></div></div>
		<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>


					<%
						longValue = sv.reserveUnitsInd.getFormData();
						if ("".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Select");
						} else if ("Y".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Y");
						} else if ("N".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("N");
						}
						if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>

						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.reserveUnitsInd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
						<%
							}
						%>
						<select name='reserveUnitsInd' type='list' style="width: 100px;"
							<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>

							<option value=""
								<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
							<option value="Y"
								<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
							<option value="N"
								<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
						</select>
						<%
							if ("red".equals((sv.reserveUnitsInd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>
		
		
		<%-- <div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
		<table>
		<tr>
		<td>
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="width:80px">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:80px">  </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 		</td><td style="padding-left: 1px;">	

  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:80px">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="width:80px"> </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</td>
		</tr>
		</table>
		 </div>
		</div>
		 --%>
		
		
		<div class="col-md-4">
				<div class="form-group" style="max-width: 150px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>

					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> --%>
					<%--  <div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
						data-link-format="dd/mm/yyyy" style="min-width:162px;">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
							
					</div> --%>
					
					<% 
								if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
							%>
								<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
							<% }else {%>
			                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>	
			                <%} %>	
					
				</div>
			</div>
		
		
		</div>
			<div class="row">	
		<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		
		
		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("currcd");
	optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.currcd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='currcd' type='list' style="width:170px;"
<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.currcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.currcd).getColor())){
%>
</div>
<%
} longValue = null;
%>

<%
} 
%>


</div></div>
		
		
		
		
			    	<div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total Fee")%></label>
                       
                       
                       		<%	
			qpsf = fw.getFieldXMLDef((sv.totalfee).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalfee,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.totalfee.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:80px;">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:80px;"> </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div>
		
		<div class="col-md-2"> </div>
		<div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Tax on Fee")%></label>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
<!-- ILIFE-6709 name='prcnt' removed from taxamt field. -->
<input 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
	 <%}%>

size='<%= sv.taxamt.getLength()%>'
maxLength='<%= sv.taxamt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event);'

<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell" style="width:80px;"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:80px;"

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div>



		 


</div>
		
 <div class="row">	
			    	
 <div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total Amount")%></label>
                       
                       
                    <%	
			qpsf = fw.getFieldXMLDef((sv.totalamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.totalamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='totalamt' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.totalamt) %>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.totalamt) %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.totalamt.getLength(), sv.totalamt.getScale(),3)%>'
maxLength='<%= sv.totalamt.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(totalamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.totalamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell" style="width:150px;"
<%
	}else if((new Byte((sv.totalamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:150px;"

<%
	}else { 
%>

	class = ' <%=(sv.totalamt).getColor()== null  ? 
			"input_cell" :  (sv.totalamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:150px;"
 
<%
	} 
%>
>

</div></div>


<div class="col-md-1"></div> 
<div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total %")%></label>
                       <%	
			qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='prcnt' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.prcnt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
	 <%}%>

size='<%= sv.prcnt.getLength()%>'
maxLength='<%= sv.prcnt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(prcnt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event);'

<% 
	if((new Byte((sv.prcnt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell" style="width:80px;"
<%
	}else if((new Byte((sv.prcnt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="width:80px;"

<%
	}else { 
%>

	class = ' <%=(sv.prcnt).getColor()== null  ? 
			"input_cell" :  (sv.prcnt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' style="width:80px;"
 
<%
	} 
%>
>

</div></div>
	
	<div class="col-md-2"></div> 
	 <div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
		<div class="input-group">
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="width:80px">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:80px">  </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:80px">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="width:80px"> </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		 </div>
		 </div>
		</div>
		
</div>		
	
		
		
		
		
		
		
		
		
	 
                    
                    
                    
            </div>      
                    
                    
	         
					    		   
					    		   
					    		   
		<%			    		   
					    		   int[] tblColumnWidth = new int[9];
int totalTblWidth = 0;
int calculatedValue =0;
			if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.coverage.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*8;								
		} else {		
			calculatedValue = (sv.coverage.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[0]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rider.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
		} else {		
			calculatedValue = (sv.rider.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[1]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.fund.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
		} else {		
			calculatedValue = (sv.fund.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[2]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fieldType.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
		} else {		
			calculatedValue = (sv.fieldType.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[3]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.shortds.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
		} else {		
			calculatedValue = (sv.shortds.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[4]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.cnstcur.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*8;								
		} else {		
			calculatedValue = (sv.cnstcur.getFormData()).length()*8;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[5]= calculatedValue;
											if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.percreqd.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
					} else {		
						calculatedValue = (sv.percreqd.getFormData()).length()*8;								
					}					
										totalTblWidth += calculatedValue;
	tblColumnWidth[6]= calculatedValue;
											if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.actvalue.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
					} else {		
						calculatedValue = (sv.actvalue.getFormData()).length()*12;								
					}					
										totalTblWidth += calculatedValue;
	tblColumnWidth[7]= calculatedValue;
				if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.estMatValue.getFormData()).length() ) {
			calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
		} else {		
			calculatedValue = (sv.estMatValue.getFormData()).length()*12;								
		}		
		totalTblWidth += calculatedValue;
	tblColumnWidth[8]= calculatedValue;
	%>
<%
		GeneralTable sfl = fw.getTable("s6307screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
		  
                       
                       
                       
                       
                       <div class="tab-pane fade" id="tab2" >
						
						<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    <div class="table-responsive">
                       
                       
                       
              <table class="table table-striped table-bordered table-hover" id='dataTables-s6307' width="100%">	
              <thead>
             
             
             <tr class="info">
                    <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>		         								
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("%")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></th>
             
             </tr>
             
             </thead>
             <% S6307screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6307screensfl
	.hasMoreScreenRows(sfl)) {	
%>




             
             <tbody>
             <tr >
		<td 
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
										<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.rider).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.fund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.fund).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.fund.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.fieldType).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.fieldType).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.fieldType.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.shortds).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.shortds).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.shortds.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.cnstcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.cnstcur).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.cnstcur.getFormData()%>
						
														 
				
									<%}%>
			</td>	
				<td 
					<%if((sv.percreqd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="center" <%}%> >		
						<%if((new Byte((sv.percreqd).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.percreqd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.percreqd);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.percreqd.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=sv.percreqd.getLength()%>'
						 onFocus='doFocus(this)' onHelp='return fieldHelp(s6307screensfl.percreqd)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6307screensfl" + "." +
						 "percreqd" + "_R" + count %>'
						 id='<%="s6307screensfl" + "." +
						 "percreqd" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: <%=sv.percreqd.getLength()*8%> px;"
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return getdoBlurNumber(event);'
						  title='<%=formatValue %>'
						 >
										
					
											
									<%}%>
			</td>
				    									<td  
					<%if((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="center" <%}%> >									
										<%if((new Byte((sv.actvalue).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										
													
					
					 	
						<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
						%>					
						
						
						
						<input type='text' 
						maxLength='<%=sv.actvalue.getLength()%>'
						 value='<%=formatValue %>' 
						 size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.actvalue.getLength(),sv.actvalue.getScale(),3)%>'
						 onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(s6307screensfl.actvalue)' onKeyUp='return checkMaxLength(this)' 
						 name='<%="s6307screensfl" + "." +
						 "actvalue" + "_R" + count %>'
						 id='<%="s6307screensfl" + "." +
						 "actvalue" + "_R" + count %>'
						 class = "input_cell"
						  style = "width: 130px;text-align:right" <%-- ILIFE-2847 changed by pmujavadiya --%>
						  
						  	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
						  title='<%=formatValue %>'
						 >
										
					
											
									<%}%>
			</td>
				    									<td  
					<%if((sv.estMatValue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.estMatValue).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.estMatValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!(sv.estMatValue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									<%}%>
			</td>	
						

		
		
	</tr>
	
	
	
	
	<%
	count = count + 1;
	S6307screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
             
             
             
             
             
             
             
             
             </tbody>
             
             
             </table>          
                       
                       
                       
                       
                       
                       
                       
                       </div></div></div></div></div>
                       
                       
             
 		<!-- ICIL-254 start  -->
 		<%if((new Byte((sv.payee).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		<div class="tab-pane fade" id="third_tab"  class="table-responsive">
		
		<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
                            <div class="input-group three-controller">	
                        
                              <input name='payee' id='payee' type='text' value='<%= sv.payee.getFormData() %>' 	class = " <%=(sv.payee).getColor()== null  ? 
						"input_cell" :  (sv.payee).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>" 
						maxLength='<%=sv.payee.getLength()%>' 	onFocus='doFocus(this)' onHelp='return fieldHelp(payee)' onKeyUp='return checkMaxLength(this)' 
						 style = "width: 120px;">		
									
						<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('payee')); changeF4Image(this); doAction('PFKEY04');">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
                       <div name ="payeename" id="payeename" style=" margin-left: 1px; background-color: rgb(238, 238, 238); color: rgb(153, 153, 153); font-weight: bold; width: 150px;"
                        class="form-control ellipsis"><%= sv.payeename.getFormData()==null?"":sv.payeename.getFormData() %></div>
             </div>
             </div>
            </div>
            
           
 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
				    	 <div class="input-group">
				    	<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("reqntype");
							optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
						%>
						
						<% 
							if((new Byte((sv.reqntype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>`
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.reqntype, fw, longValue, "reqntype", optionValue, 0) %>
						<%
							} 
						%>  
				    </div></div>
				 </div>

</div>
          	<div class="row">
					
			 <%	 if (appVars.ind57.isOn() && appVars.ind62.isOn()) { %>		
			 <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

		
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 100px;">
								
							</div>	
							
						
						
				<!-- 	 <div class="input-group" style="min-width:100px">
					 
					 </div> -->
                                  
</div></div>
					
					 <%
                                         }
                                  %>
					
					
 <%	 if (!(appVars.ind57.isOn())) { %>
              <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

	<table><tr><td class="form-group">	  
                                  <div class="input-group" style="width: 210px;">
                                  
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankacckey)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                 
								 
					  </td></tr></table>	

	</div></div>  <%} %>
	
	 <%	 if (!(appVars.ind62.isOn())) { %>          
	
	 <div class="col-md-4">
		 <div class="form-group">
			 <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>
					<table><tr><td>	 
                                  <div class="input-group" style="width: 210px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.crdtcrd)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('crdtcrd')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                
					  </td></tr></table>	
			 </div> 
		</div>
					    		 <%} %>
	
		<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Code"))%></label>
					<table>
					<tr>
					<td style="min-width: 100px;"> <%=smartHF.getHTMLVarExt(fw, sv.bankkey)%>
				<%-- 	<%=smartHF.getHTMLVarExt(fw, sv.bsortcde)%> --%>
                     </td>
                     </tr>
					</table>
					</div></div>
                     
                     
                     <div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Name"))%></label>
					<table>
					<tr> <td style="min-width:100px;">
					
					<%=smartHF.getHTMLVarExt(fw, sv.bankdesc)%>
						<%-- <%					
				if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell" %>' style="min-width: 100px;">
						<%=formatValue%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%> --%>
					</td>
					</tr>
					
					
					</table>
					
					
					</div></div>
	
	
	
	</div>
	
            
       
		
		
		</div>

		<%}%>
		<!-- ICIL-254 end -->





					</div>
                       
                       
                       
                       </div>
		
		
		
		</div> 
		
		
		
		</div></div>







<script>
	$(document).ready(function() {
		 $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
		        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		    } );
		$('#dataTables-s6307').DataTable({
			ordering : false,
			searching : false,
			scrollY : "150px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false
		});
	});
</script> 
<script>
	$(document).ready(function() {
		if($("#paymthbf option:selected").val() == '4' || $("#paymthbf option:selected").val()=='C'){
			$("#bankbtn").removeClass("disabled");
		}else{
			$("#bankbtn").addClass("disabled");
		}
		$("#paymthbf").change(function(){
			if($("#paymthbf option:selected").val() == '4' || $("#paymthbf option:selected").val()=='C'){
				$("#bankbtn").removeClass("disabled");
			}else{
				$("#bankbtn").addClass("disabled");
			}
		});
	});
</script> 



<%@ include file="/POLACommon2NEW.jsp"%>


