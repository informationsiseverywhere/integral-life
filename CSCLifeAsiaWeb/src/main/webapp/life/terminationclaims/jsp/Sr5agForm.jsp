<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR5AG";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sr5agScreenVars sv = (Sr5agScreenVars) fw.getVariables();
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div style="width: 70px;">
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div style="width: 100px;">
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group" style="padding-right: 318px;">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label> <%=resourceBundleHandler.gettingValueFromBundle("Exclusion Text")%></label>
			</div>
			<textArea name="excltxt" cols="120" maxlength="1500" rows="10"
				wrap="on" style="resize:none" onhelp="return fieldHelp('excltxt')" class="textarea bold"
				style="z-index: 2; line-height: 15px; position: relative; width: 700px; height: 200px; left: 10px; font-size: 14;"
				<%if ((new Byte((sv.excltxt).getEnabled())).compareTo(new Byte(BaseScreenData.ENABLED)) != 0) {%>
				disabled <% }%>><%=sv.excltxt.getFormData().toString().trim() %></textArea>

		</div>
	</div>
</div>
<%@ include file="/POLACommon2NEW.jsp"%>
