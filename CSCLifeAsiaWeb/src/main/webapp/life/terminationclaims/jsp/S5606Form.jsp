<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S5606";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5606ScreenVars sv = (S5606ScreenVars) fw.getVariables();%>

<%if (sv.S5606screenWritten.gt(0)) {%>
	<%S5606screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = new StringData("Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = new StringData("Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = new StringData("Valid from ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = new StringData("To ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = new StringData("Benefit Min ");%>
	<%sv.sumInsMin.setClassString("");%>
	<%StringData generatedText8 = new StringData("Max ");%>
	<%sv.sumInsMax.setClassString("");%>
	<%StringData generatedText9 = new StringData("Freq ");%>
	<%sv.benfreq.setClassString("");%>
	<%StringData generatedText10 = new StringData("Deferment ");%>
	<%sv.dfrprd.setClassString("");%>
<%	sv.dfrprd.appendClassString("num_fld");
	sv.dfrprd.appendClassString("input_txt");
	sv.dfrprd.appendClassString("highlight");
%>
	<%StringData generatedText11 = new StringData("F---T");%>
	<%StringData generatedText12 = new StringData("F---T");%>
	<%StringData generatedText13 = new StringData("F---T");%>
	<%StringData generatedText14 = new StringData("F---T");%>
	<%StringData generatedText15 = new StringData("F---T");%>
	<%StringData generatedText16 = new StringData("F---T");%>
	<%StringData generatedText17 = new StringData("F---T");%>
	<%StringData generatedText18 = new StringData("F---T");%>
	<%StringData generatedText19 = new StringData("Issue Age ");%>
	<%sv.ageIssageFrm01.setClassString("");%>
	<%sv.ageIssageTo01.setClassString("");%>
	<%sv.ageIssageFrm02.setClassString("");%>
	<%sv.ageIssageTo02.setClassString("");%>
	<%sv.ageIssageFrm03.setClassString("");%>
	<%sv.ageIssageTo03.setClassString("");%>
	<%sv.ageIssageFrm04.setClassString("");%>
	<%sv.ageIssageTo04.setClassString("");%>
	<%sv.ageIssageFrm05.setClassString("");%>
	<%sv.ageIssageTo05.setClassString("");%>
	<%sv.ageIssageFrm06.setClassString("");%>
	<%sv.ageIssageTo06.setClassString("");%>
	<%sv.ageIssageFrm07.setClassString("");%>
	<%sv.ageIssageTo07.setClassString("");%>
	<%sv.ageIssageFrm08.setClassString("");%>
	<%sv.ageIssageTo08.setClassString("");%>
	<%StringData generatedText20 = new StringData("Cessation Age ");%>
	<%sv.riskCessageFrom01.setClassString("");%>
	<%sv.riskCessageTo01.setClassString("");%>
	<%sv.riskCessageFrom02.setClassString("");%>
	<%sv.riskCessageTo02.setClassString("");%>
	<%sv.riskCessageFrom03.setClassString("");%>
	<%sv.riskCessageTo03.setClassString("");%>
	<%sv.riskCessageFrom04.setClassString("");%>
	<%sv.riskCessageTo04.setClassString("");%>
	<%sv.riskCessageFrom05.setClassString("");%>
	<%sv.riskCessageTo05.setClassString("");%>
	<%sv.riskCessageFrom06.setClassString("");%>
	<%sv.riskCessageTo06.setClassString("");%>
	<%sv.riskCessageFrom07.setClassString("");%>
	<%sv.riskCessageTo07.setClassString("");%>
	<%sv.riskCessageFrom08.setClassString("");%>
	<%sv.riskCessageTo08.setClassString("");%>
	<%StringData generatedText21 = new StringData("Prem Cess Age ");%>
	<%sv.premCessageFrom01.setClassString("");%>
	<%sv.premCessageTo01.setClassString("");%>
	<%sv.premCessageFrom02.setClassString("");%>
	<%sv.premCessageTo02.setClassString("");%>
	<%sv.premCessageFrom03.setClassString("");%>
	<%sv.premCessageTo03.setClassString("");%>
	<%sv.premCessageFrom04.setClassString("");%>
	<%sv.premCessageTo04.setClassString("");%>
	<%sv.premCessageFrom05.setClassString("");%>
	<%sv.premCessageTo05.setClassString("");%>
	<%sv.premCessageFrom06.setClassString("");%>
	<%sv.premCessageTo06.setClassString("");%>
	<%sv.premCessageFrom07.setClassString("");%>
	<%sv.premCessageTo07.setClassString("");%>
	<%sv.premCessageFrom08.setClassString("");%>
	<%sv.premCessageTo08.setClassString("");%>
	<%StringData generatedText30 = new StringData("Bene Cess Age ");%>
	<%sv.benCessageFrom01.setClassString("");%>
	<%sv.benCessageTo01.setClassString("");%>
	<%sv.benCessageFrom02.setClassString("");%>
	<%sv.benCessageTo02.setClassString("");%>
	<%sv.benCessageFrom03.setClassString("");%>
	<%sv.benCessageTo03.setClassString("");%>
	<%sv.benCessageFrom04.setClassString("");%>
	<%sv.benCessageTo04.setClassString("");%>
	<%sv.benCessageFrom05.setClassString("");%>
	<%sv.benCessageTo05.setClassString("");%>
	<%sv.benCessageFrom06.setClassString("");%>
	<%sv.benCessageTo06.setClassString("");%>
	<%sv.benCessageFrom07.setClassString("");%>
	<%sv.benCessageTo07.setClassString("");%>
	<%sv.benCessageFrom08.setClassString("");%>
	<%sv.benCessageTo08.setClassString("");%>
	<%StringData generatedText29 = new StringData("Cess date, Anniversary or Exact ");%>
	<%sv.eaage.setClassString("");%>
	<%StringData generatedText22 = new StringData("(A/E)");%>
	<%StringData generatedText23 = new StringData("Issue Age ");%>
	<%sv.termIssageFrm01.setClassString("");%>
	<%sv.termIssageTo01.setClassString("");%>
	<%sv.termIssageFrm02.setClassString("");%>
	<%sv.termIssageTo02.setClassString("");%>
	<%sv.termIssageFrm03.setClassString("");%>
	<%sv.termIssageTo03.setClassString("");%>
	<%sv.termIssageFrm04.setClassString("");%>
	<%sv.termIssageTo04.setClassString("");%>
	<%sv.termIssageFrm05.setClassString("");%>
	<%sv.termIssageTo05.setClassString("");%>
	<%sv.termIssageFrm06.setClassString("");%>
	<%sv.termIssageTo06.setClassString("");%>
	<%sv.termIssageFrm07.setClassString("");%>
	<%sv.termIssageTo07.setClassString("");%>
	<%sv.termIssageFrm08.setClassString("");%>
	<%sv.termIssageTo08.setClassString("");%>
	<%StringData generatedText24 = new StringData("Coverage Term ");%>
	<%sv.riskCesstermFrom01.setClassString("");%>
	<%sv.riskCesstermTo01.setClassString("");%>
	<%sv.riskCesstermFrom02.setClassString("");%>
	<%sv.riskCesstermTo02.setClassString("");%>
	<%sv.riskCesstermFrom03.setClassString("");%>
	<%sv.riskCesstermTo03.setClassString("");%>
	<%sv.riskCesstermFrom04.setClassString("");%>
	<%sv.riskCesstermTo04.setClassString("");%>
	<%sv.riskCesstermFrom05.setClassString("");%>
	<%sv.riskCesstermTo05.setClassString("");%>
	<%sv.riskCesstermFrom06.setClassString("");%>
	<%sv.riskCesstermTo06.setClassString("");%>
	<%sv.riskCesstermFrom07.setClassString("");%>
	<%sv.riskCesstermTo07.setClassString("");%>
	<%sv.riskCesstermFrom08.setClassString("");%>
	<%sv.riskCesstermTo08.setClassString("");%>
	<%StringData generatedText25 = new StringData("Prem Cess Term ");%>
	<%sv.premCesstermFrom01.setClassString("");%>
	<%sv.premCesstermTo01.setClassString("");%>
	<%sv.premCesstermFrom02.setClassString("");%>
	<%sv.premCesstermTo02.setClassString("");%>
	<%sv.premCesstermFrom03.setClassString("");%>
	<%sv.premCesstermTo03.setClassString("");%>
	<%sv.premCesstermFrom04.setClassString("");%>
	<%sv.premCesstermTo04.setClassString("");%>
	<%sv.premCesstermFrom05.setClassString("");%>
	<%sv.premCesstermTo05.setClassString("");%>
	<%sv.premCesstermFrom06.setClassString("");%>
	<%sv.premCesstermTo06.setClassString("");%>
	<%sv.premCesstermFrom07.setClassString("");%>
	<%sv.premCesstermTo07.setClassString("");%>
	<%sv.premCesstermFrom08.setClassString("");%>
	<%sv.premCesstermTo08.setClassString("");%>
	<%StringData generatedText31 = new StringData("Bene Cess Term ");%>
	<%sv.benCesstermFrm01.setClassString("");%>
	<%sv.benCesstermTo01.setClassString("");%>
	<%sv.benCesstermFrm02.setClassString("");%>
	<%sv.benCesstermTo02.setClassString("");%>
	<%sv.benCesstermFrm03.setClassString("");%>
	<%sv.benCesstermTo03.setClassString("");%>
	<%sv.benCesstermFrm04.setClassString("");%>
	<%sv.benCesstermTo04.setClassString("");%>
	<%sv.benCesstermFrm05.setClassString("");%>
	<%sv.benCesstermTo05.setClassString("");%>
	<%sv.benCesstermFrm06.setClassString("");%>
	<%sv.benCesstermTo06.setClassString("");%>
	<%sv.benCesstermFrm07.setClassString("");%>
	<%sv.benCesstermTo07.setClassString("");%>
	<%sv.benCesstermFrm08.setClassString("");%>
	<%sv.benCesstermTo08.setClassString("");%>
	<%StringData generatedText26 = new StringData("Special Terms ");%>
	<%sv.specind.setClassString("");%>
<%	sv.specind.appendClassString("string_fld");
	sv.specind.appendClassString("input_txt");
	sv.specind.appendClassString("highlight");
%>
	<%StringData generatedText27 = new StringData("Mortality Class ");%>
	<%sv.mortcls01.setClassString("");%>
	<%sv.mortcls02.setClassString("");%>
	<%sv.mortcls03.setClassString("");%>
	<%sv.mortcls04.setClassString("");%>
	<%sv.mortcls05.setClassString("");%>
	<%sv.mortcls06.setClassString("");%>
	<%StringData generatedText28 = new StringData("Lien Codes ");%>
	<%sv.liencd01.setClassString("");%>
	<%sv.liencd02.setClassString("");%>
	<%sv.liencd03.setClassString("");%>
	<%sv.liencd04.setClassString("");%>
	<%sv.liencd05.setClassString("");%>
	<%sv.liencd06.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind03.isOn()) {
			sv.sumInsMin.setReverse(BaseScreenData.REVERSED);
			sv.sumInsMin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.sumInsMin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.sumInsMax.setReverse(BaseScreenData.REVERSED);
			sv.sumInsMax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.sumInsMax.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.benfreq.setReverse(BaseScreenData.REVERSED);
			sv.benfreq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.benfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ageIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ageIssageFrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.ageIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.ageIssageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.ageIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.ageIssageFrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.ageIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.ageIssageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.ageIssageFrm03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.ageIssageFrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.ageIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.ageIssageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ageIssageFrm04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ageIssageFrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.ageIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.ageIssageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ageIssageFrm05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ageIssageFrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.ageIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.ageIssageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.ageIssageFrm06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ageIssageFrm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.ageIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.ageIssageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.ageIssageFrm07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.ageIssageFrm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ageIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.ageIssageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.ageIssageFrm08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.ageIssageFrm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ageIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ageIssageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.riskCessageFrom01.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.riskCessageFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.riskCessageTo01.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.riskCessageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.riskCessageFrom02.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.riskCessageFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.riskCessageTo02.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.riskCessageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.riskCessageFrom03.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.riskCessageFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.riskCessageTo03.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.riskCessageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.riskCessageFrom04.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.riskCessageFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.riskCessageTo04.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.riskCessageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.riskCessageFrom05.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.riskCessageFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.riskCessageTo05.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.riskCessageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessageFrom06.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.riskCessageFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.riskCessageTo06.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.riskCessageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessageFrom07.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessageFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.riskCessageTo07.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.riskCessageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.riskCessageFrom08.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.riskCessageFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.riskCessageTo08.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.riskCessageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.premCessageFrom01.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.premCessageFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.premCessageTo01.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.premCessageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessageFrom02.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.premCessageFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.premCessageTo02.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.premCessageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessageFrom03.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.premCessageFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.premCessageTo03.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.premCessageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.premCessageFrom04.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.premCessageFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.premCessageTo04.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.premCessageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.premCessageFrom05.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.premCessageFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.premCessageTo05.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.premCessageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessageFrom06.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.premCessageFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.premCessageTo06.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.premCessageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.premCessageFrom07.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.premCessageFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.premCessageTo07.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.premCessageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessageFrom08.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessageFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.premCessageTo08.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.premCessageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.eaage.setReverse(BaseScreenData.REVERSED);
			sv.eaage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.eaage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.termIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.termIssageFrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.termIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.termIssageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.termIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.termIssageFrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.termIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.termIssageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.termIssageFrm03.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.termIssageFrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.termIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.termIssageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.termIssageFrm04.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.termIssageFrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.termIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.termIssageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.termIssageFrm05.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.termIssageFrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.termIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.termIssageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.termIssageFrm06.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.termIssageFrm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.termIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.termIssageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.termIssageFrm07.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.termIssageFrm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.termIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.termIssageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.termIssageFrm08.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.termIssageFrm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.termIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.termIssageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.riskCesstermFrom01.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.riskCesstermFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.riskCesstermTo01.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.riskCesstermTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.riskCesstermFrom02.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.riskCesstermFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.riskCesstermTo02.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.riskCesstermTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.riskCesstermFrom03.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.riskCesstermFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.riskCesstermTo03.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.riskCesstermTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.riskCesstermFrom04.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.riskCesstermFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.riskCesstermTo04.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.riskCesstermTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.riskCesstermFrom05.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.riskCesstermFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.riskCesstermTo05.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.riskCesstermTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.riskCesstermFrom06.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.riskCesstermFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.riskCesstermTo06.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.riskCesstermTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.riskCesstermFrom07.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.riskCesstermFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.riskCesstermTo07.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.riskCesstermTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.riskCesstermFrom08.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.riskCesstermFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.riskCesstermTo08.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.riskCesstermTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.premCesstermFrom01.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.premCesstermFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.premCesstermTo01.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.premCesstermTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.premCesstermFrom02.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.premCesstermFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.premCesstermTo02.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.premCesstermTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.premCesstermFrom03.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.premCesstermFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.premCesstermTo03.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.premCesstermTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.premCesstermFrom04.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.premCesstermFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.premCesstermTo04.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.premCesstermTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.premCesstermFrom05.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.premCesstermFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.premCesstermTo05.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.premCesstermTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.premCesstermFrom06.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.premCesstermFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.premCesstermTo06.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.premCesstermTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.premCesstermFrom07.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.premCesstermFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.premCesstermTo07.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.premCesstermTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.premCesstermFrom08.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.premCesstermFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.premCesstermTo08.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.premCesstermTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind68.isOn()) {
			sv.mortcls01.setReverse(BaseScreenData.REVERSED);
			sv.mortcls01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.mortcls01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.mortcls02.setReverse(BaseScreenData.REVERSED);
			sv.mortcls02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.mortcls02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.mortcls03.setReverse(BaseScreenData.REVERSED);
			sv.mortcls03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.mortcls03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.mortcls04.setReverse(BaseScreenData.REVERSED);
			sv.mortcls04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.mortcls04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind72.isOn()) {
			sv.mortcls05.setReverse(BaseScreenData.REVERSED);
			sv.mortcls05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.mortcls05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.mortcls06.setReverse(BaseScreenData.REVERSED);
			sv.mortcls06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.mortcls06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			sv.liencd01.setReverse(BaseScreenData.REVERSED);
			sv.liencd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.liencd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind78.isOn()) {
			sv.liencd02.setReverse(BaseScreenData.REVERSED);
			sv.liencd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.liencd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind79.isOn()) {
			sv.liencd03.setReverse(BaseScreenData.REVERSED);
			sv.liencd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.liencd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind80.isOn()) {
			sv.liencd04.setReverse(BaseScreenData.REVERSED);
			sv.liencd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.liencd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.liencd05.setReverse(BaseScreenData.REVERSED);
			sv.liencd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.liencd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind82.isOn()) {
			sv.liencd06.setReverse(BaseScreenData.REVERSED);
			sv.liencd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.liencd06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.benCessageFrom01.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.benCessageFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.benCessageTo01.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.benCessageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessageFrom02.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.benCessageFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.benCessageTo02.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.benCessageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessageFrom03.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.benCessageFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.benCessageTo03.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.benCessageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.benCessageFrom04.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.benCessageFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.benCessageTo04.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.benCessageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.benCessageFrom05.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.benCessageFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.benCessageTo05.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.benCessageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessageFrom06.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.benCessageFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.benCessageTo06.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.benCessageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.benCessageFrom07.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.benCessageFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.benCessageTo07.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.benCessageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessageFrom08.setReverse(BaseScreenData.REVERSED);
			sv.benCessageFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessageFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.benCessageTo08.setReverse(BaseScreenData.REVERSED);
			sv.benCessageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.benCessageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.benCesstermFrm01.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.benCesstermFrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.benCesstermTo01.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.benCesstermTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.benCesstermFrm02.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.benCesstermFrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.benCesstermTo02.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.benCesstermTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.benCesstermFrm03.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.benCesstermFrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.benCesstermTo03.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.benCesstermTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.benCesstermFrm04.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.benCesstermFrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.benCesstermTo04.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.benCesstermTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.benCesstermFrm05.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.benCesstermFrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.benCesstermTo05.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.benCesstermTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.benCesstermFrm06.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.benCesstermFrm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.benCesstermTo06.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.benCesstermTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.benCesstermFrm07.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.benCesstermFrm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.benCesstermTo07.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.benCesstermTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.benCesstermFrm08.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermFrm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.benCesstermFrm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.benCesstermTo08.setReverse(BaseScreenData.REVERSED);
			sv.benCesstermTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.benCesstermTo08.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
 
 
 
 
 <div class="panel panel-default">
      <div class="panel-body">     

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
             <div style="width:35px">             
 <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 </div></div></div>
 
 
 
 
 <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                            <div style="width:80px">    
                       <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
                       
                       
                       </div></div></div>
                       
                       
                       <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                      <table>
                      <tr>
                      <td>
                       	
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  id="item">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>

	<td>

  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px;" id="longdesc">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  </td>
  </tr>
  </table>
  </div></div>
                       
                       
 
 
 </div>
 
 
 
 
 
 
 <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
 
 <table><tr><td>
 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td>

<td>

&nbsp;To&nbsp;

</td>

<td>	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
  
  </td></tr></table>
  </div></div>
  </div>
 
 
 
 
 
 
 
 <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Min")%></label>
                       <div style="width:120px">
 <input name='sumInsMin' 
        type='text'
        text-allign='right'

<%

	qpsf = fw.getFieldXMLDef((sv.sumInsMin).getFieldName());		
	formatValue = smartHF.getPicFormatted(qpsf,sv.sumInsMin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumInsMin.getLength(), sv.sumInsMin.getScale(),3)%>'
maxLength='<%= sv.sumInsMin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumInsMin)' onKeyUp='return checkMaxLength(this)'  
onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
decimal='<%=qpsf.getDecimals()%>' 
onPaste='return doPasteNumber(event,true);'
onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumInsMin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumInsMin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumInsMin).getColor()== null  ? 
			"input_cell" :  (sv.sumInsMin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>
 
 
 
 <div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Max")%></label>
 <div style="width:160px">
 <input name='sumInsMax' 
type='text'

<%

        qpsf = fw.getFieldXMLDef((sv.sumInsMax).getFieldName());		
        formatValue = smartHF.getPicFormatted(qpsf,sv.sumInsMax,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumInsMax.getLength(), sv.sumInsMax.getScale(),3)%>'
maxLength='<%= sv.sumInsMax.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumInsMax)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
		decimal='<%=qpsf.getDecimals()%>' 
		onPaste='return doPasteNumber(event,true);'
		onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumInsMax).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumInsMax).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumInsMax).getColor()== null  ? 
			"input_cell" :  (sv.sumInsMax).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>
 
 
 
 
 <div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Freq")%></label>
 <div class="input-group"   >
 
<input name='benfreq' 
type='text'
id='alfnds'
<%

		formatValue = (sv.benfreq.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.benfreq.getLength()%>'
maxLength='<%= sv.benfreq.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(specind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.benfreq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.benfreq).getColor()== null  ? 
			"input_cell" :  (sv.benfreq).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('alfnds')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>


<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('alfnds')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

</div></div>
</div>



 <div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Deferment")%></label>
 <div style="width:80px">
<input name='dfrprd' 
type='text'

<%

		formatValue = (sv.dfrprd.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.dfrprd.getLength()%>'
maxLength='<%= sv.dfrprd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(specind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.dfrprd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfrprd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfrprd).getColor()== null  ? 
			"input_cell" :  (sv.dfrprd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>


 
 
 <div class="col-md-2"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%></label>
 <div style="width:60px">
 <input name='specind' 
type='text'

<%

		formatValue = (sv.specind.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.specind.getLength()%>'
maxLength='<%= sv.specind.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(specind)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.specind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.specind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.specind).getColor()== null  ? 
			"input_cell" :  (sv.specind).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

</div></div></div>


 
 </div>
 
 
 
 <br><br>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	                  
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                     
                      <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                   
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                  
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                   
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                   
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                    
                      <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 
                    
                     			    	<div class="col-md-1" > 
			    	                     
                       <label ><%=resourceBundleHandler.gettingValueFromBundle("F-----T")%></label>
 
 
                    </div> 

 
 
 </div>
 
 
 
 
 
 <div class="row">	
			    	<div class="col-md-2" "> 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Issue Age")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1"> 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px;"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm01)%>
                       </div>
                     <div class="col-md-2 ddddd" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1"> 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.ageIssageFrm08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.ageIssageTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 
 
 
 
  <div class="row">	
			    	<div class="col-md-2"> 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Cessation Age")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCessageFrom08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCessageTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 <div class="row">	
			    	<div class="col-md-2"> 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Prem Cess Age")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar( fw, sv.premCessageFrom07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                      <%=smartHF.getHTMLVar( fw, sv.premCessageFrom08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar( fw, sv.premCessageTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 
  <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Bene Cess Age")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                      <%=smartHF.getHTMLVar (fw, sv.benCessageFrom06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar (fw, sv.benCessageFrom07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                      <%=smartHF.getHTMLVar (fw, sv.benCessageFrom08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar (fw, sv.benCessageTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
  
 <div class="row">	
			    	<div class="col-md-12" > 
			    	  <div class="form-group">  
			    	  <table><tr><td style="width:240px">                
                       <label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("Cess date, Anniversary os Exact")%></label>
                      </td>
                      <td>
                      <%=smartHF.getHTMLVar(fw, sv.eaage)%>
                      </td>
                      <td style="width:40px"></td>
                      <td>
                      <label style="font-size: 14px;"><%=resourceBundleHandler.gettingValueFromBundle("(A/E)")%></label>
                      </td></tr></table>
                      </div></div>
             </div>
 
 
 
 
 
 
 
 
  <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Issue Age")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.termIssageFrm08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.termIssageTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 
 
 
 
    <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Term")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo05)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo07)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.riskCesstermTo08)%>
                     
                    </div></div>
                    </div></div> 
        
 
 
 </div>
 
 
 
 
 
 
 
 
   <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Prem Cess Term")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.premCesstermFrom08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.premCesstermTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 
 
 
 
   <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Bene Cess Term")%></label>
 
 
                    </div></div>
                    
                    
 			    	<div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm01)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo01)%>
                     
                    </div></div>
                    </div></div> 
                     
                     
                     
                     <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm02)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo02)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm03)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo03)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm04)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo04)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm05)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo05)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm06)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo06)%>
                     
                    </div></div>
                    </div></div> 
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm07)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo07)%>
                     
                    </div></div>
                    </div></div>  
                    
                    
                    
                    <div class="col-md-1" > 
			    	  <div class="form-group"  >
			    	  
			    	  <div class="row">	
			    	<div class="col-md-2" style="width:35px; padding:0px"> 
			    	 
                       <%=smartHF.getHTMLVar(fw, sv.benCesstermFrm08)%>
                       </div>
                     <div class="col-md-2" style="width:35px; padding:0px"> 
	                    <%=smartHF.getHTMLVar(fw, sv.benCesstermTo08)%>
                     
                    </div></div>
                    </div></div>  
        
 
 
 </div>
 
 
 
 
 
 
 <!-- ----------------------------------------------------------------------------------------------------- -->
 
 <br>
 
 <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls01)%>
	</div></div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls02)%>
	</div></div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls03)%>
</div></div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls04)%>
</div></div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls05)%>
</div></div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.mortcls06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.mortcls06)%>
	</div></div>
	
	</div>
 
 
 
 
 
 
  <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd01)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd02)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd03)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd04)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd05)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.liencd06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.liencd06)%>
	</div></div>
	</div>
	
	</div>
 
 
 
   <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis01)%>
	</div></div>
	</div>
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis02)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis03)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis04)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis05)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.prmbasis06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.prmbasis06)%>
	</div></div>
	</div>
	</div>
 
 
 
 
 
    <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod01)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod02)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod03)%>
</div></div>
</div>
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod04)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod05)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod06)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
	 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod07)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod07)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width: 91px; padding-left: 6px;padding-right: 3px;">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod08)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod08)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
	 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.waitperiod09)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.waitperiod09)%>
	</div></div>
	</div>
	
	</div>
	
	
	
	
	
	
	
	
	   <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm01)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm02)%>
	</div></div>
	</div>
	
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm03)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm04)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm05)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm06)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
	<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm07)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm07)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width:91px; padding-left:6px; padding-right:3px">
	<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm08)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm08)%>
	</div></div>
	</div>
	
	<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
	<div class="form-group">
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.bentrm09)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.bentrm09)%>
	</div></div>
	</div>
	</div>
 
 
 
 
 
 	   <div class="row">	
			    	<div class="col-md-2" > 
			    	  <div class="form-group">                  
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>

</div></div>


<div class="col-md-1" style="width:90px; padding-left:0px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp01)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp01)%>
	</div></div>
	</div>
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp02)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp02)%>
	</div></div>
	</div>
<div class="col-md-1" style="width:95px; padding-left:6px;; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp03)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp03)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp04)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp04)%>
</div></div>
</div>
<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp05)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp05)%>
</div></div>
</div>

<div class="col-md-1" style="width:95px; padding-left:6px; padding-right:3px">
 <div class="form-group">  
<div class="input-group" style="min-width: 40px;">
	<%=smartHF.getHTMLSpaceVar(fw, sv.poltyp06)%>
	<%=smartHF.getHTMLF4NSVar(fw, sv.poltyp06)%>
	</div></div>
	</div>
	
	 
	
	</div>
 
 
 
 
 
 
 </div></div>
 
 
 <%}%>
 
<script type="text/javascript">
 $(document).ready(function(){
	 
	 
	 if (screen.height == 1024) {
			
			$('#longdesc').css('max-width','165px')
		}
	 if (screen.height == 900) {
			
			$('#longdesc').css('max-width','250px')
		}
	 if (screen.height == 768) {
			
			$('#longdesc').css('max-width','195px')
		}
 
 });
</script>
<!--  
<style>

@media screen and (-webkit-min-device-pixel-ratio: 1) and (max-device-width: 1280px) and (min-device-width: 900px){
#longdesc {
    max-width: 173px !important;
}
}
</style> -->

<%@ include file="/POLACommon2NEW.jsp"%>
