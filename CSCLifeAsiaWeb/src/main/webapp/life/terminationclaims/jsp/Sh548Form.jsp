

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH548";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh548ScreenVars sv = (Sh548ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Participating ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage Code ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dividend Option ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.divdParticipant.setReverse(BaseScreenData.REVERSED);
			sv.divdParticipant.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.divdParticipant.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.puCvCode.setReverse(BaseScreenData.REVERSED);
			sv.puCvCode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.puCvCode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zdivopt.setReverse(BaseScreenData.REVERSED);
			sv.zdivopt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zdivopt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		     <div class="input-group">
						    		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
							<div class="input-group">
						    		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group"style="max-width:100px;">
						    		
				
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px;">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Dates Effective")%></label>
					    		
					    		<TABLE><TR><TD>
					    		     
						    		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</TD>
				      			     
				      			     <TD> &nbsp; </TD>
				      			     
				      			     <TD> <div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>
				      			     </TD>
				      			     
				      			      <TD> &nbsp; </TD>
				      			      
				      			      <TD>
						    		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
			

				      			      </TD>
				      			      </TR></TABLE>
				    		</div>
					</div>
				    		</div>
				    		
				    		<%
ScreenDataHelper participating = new ScreenDataHelper(fw.getVariables(), sv.divdParticipant);
%>
				    		
				    	<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
				
<LABEL>
<%=resourceBundleHandler.gettingValueFromBundle("Participating")%>
</LABEL>
<div class="input-group" style="max-width:80px">  	  
				
<%if (participating.isReadonly()) {%>
<input name='divdParticipant' type='text' value='<%=participating.value()%>'
       class="blank_cell" readonly
       onFocus='doFocus(this)' onHelp='return fieldHelp(divdParticipant)'
       onKeyUp='return checkMaxLength(this)'>
<%} else {%>
<select name="divdParticipant" type="list" class="<%=participating.clazz()%>">
  <option <%=participating.selected("Y")%> value="Y">Y</option>
  <option <%=participating.selected("N")%> value="N">N</option>
</select>
<%}%>

				    		</div></div>
					</div>
				    		
				    		
				    <div class="col-md-3">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Code")%></label>
							<div class="input-group">
						    	
<input name='puCvCode' 
type='text'

<%if((sv.puCvCode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		formatValue = (sv.puCvCode.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.puCvCode.getLength()%>'
maxLength='<%= sv.puCvCode.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(puCvCode)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.puCvCode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.puCvCode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.puCvCode).getColor()== null  ? 
			"input_cell" :  (sv.puCvCode).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-3">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Dividend Option")%></label>
							<div class="input-group">
						    		
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"zdivopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("zdivopt");
	optionValue = makeDropDownList( mappedItems , sv.zdivopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.zdivopt.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.zdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.zdivopt).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:222px;"> 
<%
} 
%>

<select name='zdivopt' type='list' style="width:220px;"
<% 
	if((new Byte((sv.zdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zdivopt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.zdivopt).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

						    		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				 	
				    		
				    		
				    		
				    		
				    		



<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>
</div>

</tr></table></div><br/>

</div></div>  <!--  panel-->
<%@ include file="/POLACommon2NEW.jsp"%>

<%!
private static class ScreenDataHelper {

    VarModel screen;
    BaseScreenData data;

    ScreenDataHelper(VarModel screen, BaseScreenData data) {
        super();
        if (screen == null) throw new AssertionError("Null VarModel");
        if (data == null) throw new AssertionError("Null BaseScreenData");
        this.screen = screen;
        this.data = data;
    }
    
    String value() {
        return data.getFormData().trim();
    }

    boolean isReadonly() {
        return (isProtected() || isDisabled());
    }
    
    boolean isProtected() {
        return screen.isScreenProtected();
    }

    boolean isDisabled() {
        return data.getEnabled() == BaseScreenData.DISABLED;
    }
    
    String readonly() {
        if (isProtected()) return "readonly";
        else return "";
    }

    String visibility() {
        if (data.getInvisible() == BaseScreenData.INVISIBLE) return "hidden";
        else return "visible";
    }
    
    int length() {
        return value().length();
    }
    
    String clazz() {
        String clazz = "input_cell";
        if (isProtected()) clazz = "blank_cell";
        else if (isDisabled()) clazz = "output_cell";
        if (data.getHighLight() == BaseScreenData.BOLD) clazz += " bold_cell";
        if (BaseScreenData.RED.equals(data.getColor())) clazz += " red reverse";
        return clazz;
    }
    
    String selected(String value) {
        if (value().equalsIgnoreCase(value)) return  "selected='selected'";
        else return "";
    }

}
%>