

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR583";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr583ScreenVars sv = (Sr583ScreenVars) fw.getVariables();%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Details");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Code     ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type of Unit     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Double Indemnity ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date From        ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date  To         ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount Incurred  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual Units     ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Benefit  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max. No of Units ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Covered Amount   ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Covered Units    ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual Paid Amount  ");%>

<%{
		if (appVars.ind20.isOn()) {
			sv.datefromDisp.setReverse(BaseScreenData.REVERSED);
			sv.datefromDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.datefromDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind20.isOn()) {
			sv.datefromDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.datetoDisp.setReverse(BaseScreenData.REVERSED);
			sv.datetoDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.datetoDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind21.isOn()) {
			sv.datetoDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.gincurr.setReverse(BaseScreenData.REVERSED);
			sv.gincurr.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.gincurr.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind22.isOn()) {
			sv.gincurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.acbenunt.setReverse(BaseScreenData.REVERSED);
			sv.acbenunt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind71.isOn()) {
			sv.acbenunt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind23.isOn()) {
			sv.acbenunt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.gcnetpy.setReverse(BaseScreenData.REVERSED);
			sv.gcnetpy.setColor(BaseScreenData.RED);
		}
		if (appVars.ind70.isOn()) {
			sv.gcnetpy.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind24.isOn()) {
			sv.gcnetpy.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind75.isOn()) {
			sv.comt.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>



<div class="panel panel-default">
	<div class="panel-body">
	
		<div class="row">
			<div class="col-md-4">
		 <div class="form-group" >
				<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Code")%></label>
<div class="form-group">
<table><tr><td>
<%if ((new Byte((sv.acdben).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.acdben.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acdben.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.acdben.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td>
	
<td style="padding-left:2px;">

<%if ((new Byte((sv.benefits).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.benefits.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benefits.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:200px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>



</td></tr></table>
  
  </div></div></div>
  
  
  <div class="col-md-4">
		 <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Type of Unit")%></label>
  <div style="width:60px">
  
  <%if ((new Byte((sv.benfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.benfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  </div></div></div>
  
  
  
  <div class="col-md-4">
		 <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Double Indemnity")%></label>
  <div style="width:50px">
  <%if ((new Byte((sv.gcdblind).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.gcdblind.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.gcdblind.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.gcdblind.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  
  </div></div></div>
  
  
  
  </div>











<div class="row">
<table>
<tr><td>
<div class="col-md-4">
		 <div class="form-group"  style="width:290px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Date From")%></label>
				
<div class="input-group">



<%	
	if ((new Byte((sv.datefromDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datefromDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datefromDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="datefromDisp" data-link-format="dd/mm/yyyy">
<input name='datefromDisp' 
type='text' 
value='<%=sv.datefromDisp.getFormData()%>' 
maxLength='<%=sv.datefromDisp.getLength()%>' 
size='<%=sv.datefromDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datefromDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datefromDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>
<%
longValue = null;
%>
<%
	}else if((new Byte((sv.datefromDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px" onClick="showCalendar(this, document.getElementById('datefromDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.datefromDisp).getColor()== null  ? 
"input_cell" :  (sv.datefromDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;"style="position: relative; top:1px; left:0px"  onClick="showCalendar(this, document.getElementById('datefromDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
longValue = null;
%>
<%}%></div>
<%}} %>

</div></div>

</div>	
</td>		


<td>
<div class="col-md-4">
		 <div class="form-group"  style="width:290px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Date  To")%></label>
<div class="input-group">


<%	
	if ((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.datetoDisp.getFormData();  
%>

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="datetoDisp" data-link-format="dd/mm/yyyy">
<input name='datetoDisp' 
type='text' 
value='<%=sv.datetoDisp.getFormData()%>' 
maxLength='<%=sv.datetoDisp.getLength()%>' 
size='<%=sv.datetoDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(datetoDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.datetoDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>
<%
longValue = null;
%>
<%
	}else if((new Byte((sv.datetoDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px" onClick="showCalendar(this, document.getElementById('datetoDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.datetoDisp).getColor()== null  ? 
"input_cell" :  (sv.datetoDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px" onClick="showCalendar(this, document.getElementById('datetoDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
longValue = null;
%>
<%} }}%>


</div></div></div></div>
</td>
<td>
<div class="col-md-4">
		 <div class="form-group" style="width:150px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Amount Incurred")%></label>
 <%if ((new Byte((sv.gincurr).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.gincurr).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.gincurr,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='gincurr' 
type='text'

<%if((sv.gincurr).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.gincurr.getLength(), sv.gincurr.getScale(),3)%>'
maxLength='<%= sv.gincurr.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(gincurr)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.gincurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gincurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gincurr).getColor()== null  ? 
			"input_cell" :  (sv.gincurr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%
longValue = null;
%>
<%}%>

</div></div>
</td>
</table>

</div>





<div class="row">
			<div class="col-md-4">
		 <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Actual Units")%></label>
<div style="width:80px">

<%if ((new Byte((sv.acbenunt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.acbenunt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='acbenunt' 
type='text'

<%if((sv.acbenunt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.acbenunt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.acbenunt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.acbenunt) %>'
	 <%}%>

size='<%= sv.acbenunt.getLength()%>'
maxLength='<%= sv.acbenunt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(acbenunt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.acbenunt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.acbenunt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.acbenunt).getColor()== null  ? 
			"input_cell" :  (sv.acbenunt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%
longValue = null;
%>
<%}%>

</div></div></div>





	<div class="col-md-4">
		 <div class="form-group" style="width:150px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Benefit")%></label>
<%if ((new Byte((sv.amtaout).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.amtaout).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.amtaout,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.amtaout.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div>






<div class="col-md-4">
		 <div class="form-group" style="width:150px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Max. No of Units")%></label>

<%if ((new Byte((sv.mxbenunt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.mxbenunt);
			
			if(!((sv.mxbenunt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 
 </div></div>
 
 


</div>





<div class="row">
			<div class="col-md-4">
		 <div class="form-group" style="width:150px">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Covered Amount")%></label>

<%if ((new Byte((sv.gtotinc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.gtotinc).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.gtotinc,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.gtotinc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div>




<div class="col-md-4">
		 <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Covered Units")%></label>
				<div style="width:80px">
				
<%if ((new Byte((sv.cvbenunt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.cvbenunt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.cvbenunt);
			
			if(!((sv.cvbenunt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div></div>
 
 
 
 
 
 </div>




<div class="row">
			<div class="col-md-6">
		 <div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Actual Paid Amount")%></label>

<table><tr><td>


<%if ((new Byte((sv.gcnetpy).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.gcnetpy).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.gcnetpy,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='gcnetpy' 
type='text'

<%if((sv.gcnetpy).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.gcnetpy.getLength(), sv.gcnetpy.getScale(),3)%>'
maxLength='<%= sv.gcnetpy.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(gcnetpy)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.gcnetpy).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcnetpy).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcnetpy).getColor()== null  ? 
			"input_cell" :  (sv.gcnetpy).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
><%
longValue = null;
%>
<%}%>


</td><td style="padding-left:2px;">

<%if ((new Byte((sv.comt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.comt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.comt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.comt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:150px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>

</td></tr></table>
</div></div>

</div>

</div></div></div>


<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
 --%>
<%@ include file="/POLACommon2NEW.jsp"%>
