

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6319";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S6319ScreenVars sv = (S6319ScreenVars) fw.getVariables();%>
    <%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"K - Pre-Registration");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"L - Pre-Registration-Enquiry");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Register Death Claim");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Adjust Registration");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Approval");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Register First Death");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Enquiry ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract number  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date   ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action           ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.efdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.efdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.efdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


<div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
 <div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-6"> 
			    	     <div class="form-group">
			  	<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
    	 					<div class="input-group" style="min-width:110px">
<input name='chdrsel' id='chdrsel'
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>
 --%>
 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>

</div></div></div>

<!-- <div class="col-md-2"></div> -->
 
 			<div class="col-md-4"> 
			   <div class="form-group">
			    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
    	 					<div class="input-group">
  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">
 <input name='efdateDisp' 
type='text' 
value='<%=sv.efdateDisp.getFormData()%>' 
maxLength='<%=sv.efdateDisp.getLength()%>' 
size='<%=sv.efdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(efdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.efdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.efdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('efdateDisp'),'<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%
	}else { 
%>

class = ' <%=(sv.efdateDisp).getColor()== null  ? 
"input_cell" :  (sv.efdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('efdateDisp'), '<%= av.getAppConfig().getDateFormat()%>'  ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%} %>

</div></div></div></div>
</div> 
</div>
</div>


<div class="panel panel-default">
<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Action")%>
         </div>
<div class="panel-body"> 
<% if ((new Byte((sv.action).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
					<div class="radioButtonSubmenuUIG">
<label for="K"><%= smartHF.buildRadioOption(sv.action, "action", "K")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Pre-Registration")%></label> 	
</div>
 </div></div>
<div class="col-md-4"> 
	 	     <div class="form-group">
					<div class="radioButtonSubmenuUIG">
<label for="L"><%= smartHF.buildRadioOption(sv.action, "action", "L")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Pre-Registration-Enquiry")%></label> 	
</div>
 </div></div>
<div class="col-md-4"> 
	 	     <div class="form-group">
					<div class="radioButtonSubmenuUIG">
<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Register Death Claim")%></label> 	
</div>
 </div></div>
 </div>
<div class="row">
 <div class="col-md-4"> 
			  <div class="form-group">
<div class="radioButtonSubmenuUIG">
	<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Adjust Registration")%> </label> 
</div>
 </div></div>
 

  <div class="col-md-4"> 
	 <div class="form-group">
<div class="radioButtonSubmenuUIG">
<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Approval")%></label>
</div>
 </div></div>
 

		<div class="col-md-4"> 
		<div class="form-group">     
<div class="radioButtonSubmenuUIG">
<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Register First Death")%> </label>
</div>
 </div></div>
 </div>
<div class="row">
 <div class="col-md-4"> 
	    <div class="form-group"> 	     
<div class="radioButtonSubmenuUIG">
<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></label>
</div>
  </div></div>
 

  <div class="col-md-4"> 
		   <div class="form-group">    
<div class="radioButtonSubmenuUIG">
<label for="F"><%= smartHF.buildRadioOption(sv.action, "action", "F")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold-Coverage")%></label>
</div></div>
</div>
 
	<div class="col-md-4"> 
 	 <div class="form-group">
	 <div class="radioButtonSubmenuUIG">
<label for="G"><%= smartHF.buildRadioOption(sv.action, "action", "G")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold - Beneficiary")%> </label>
</div>
  </div></div>
 </div>
 
<div class="row">
 <div class="col-md-4"> 
 <div class="form-group">	    	     
<div class="radioButtonSubmenuUIG">
<label for="H"><%= smartHF.buildRadioOption(sv.action, "action", "H")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Release - Coverage")%> </label>
</div>
 </div></div>
 		
 	<div class="col-md-4"> 
 	 <div class="form-group">
	<div class="radioButtonSubmenuUIG">
<label for="I"><%= smartHF.buildRadioOption(sv.action, "action", "I")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Release - Beneficiary")%> </label>
</div></div>
</div>


				<div class="col-md-4"> 
			    <div class="form-group">    
<div class="radioButtonSubmenuUIG">

<label for="J"><%= smartHF.buildRadioOption(sv.action, "action", "J")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold - Enquiry")%></label>
</div>
 </div></div>
 
 <div class="col-md-4"> </div>
 <div class="col-md-4"> </div>
 </div>

<%} else {%>
<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
					<div class="radioButtonSubmenuUIG">
<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Register Death Claim")%></label> 	
</div>
 </div></div>
 

 <div class="col-md-4"> 
			  <div class="form-group">
<div class="radioButtonSubmenuUIG">
	<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Adjust Registration")%> </label> 
</div>
 </div></div>
 

  <div class="col-md-4"> 
	 <div class="form-group">
<div class="radioButtonSubmenuUIG">
<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Approval")%></label>
</div>
 </div></div>
 </div>
 

<div class="row">
		<div class="col-md-4"> 
		<div class="form-group">     
<div class="radioButtonSubmenuUIG">
<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Register First Death")%> </label>
</div>
 </div></div>
 

 <div class="col-md-4"> 
	    <div class="form-group"> 	     
<div class="radioButtonSubmenuUIG">
<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></label>
</div>
  </div></div>
 

  <div class="col-md-4"> 
		   <div class="form-group">    
<div class="radioButtonSubmenuUIG">
<label for="F"><%= smartHF.buildRadioOption(sv.action, "action", "F")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold-Coverage")%></label>
</div></div>
</div></div>
 
 <div class="row">
	<div class="col-md-4"> 
 	 <div class="form-group">
	 <div class="radioButtonSubmenuUIG">
<label for="G"><%= smartHF.buildRadioOption(sv.action, "action", "G")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold - Beneficiary")%> </label>
</div>
  </div></div>
 

 <div class="col-md-4"> 
 <div class="form-group">	    	     
<div class="radioButtonSubmenuUIG">
<label for="H"><%= smartHF.buildRadioOption(sv.action, "action", "H")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Release - Coverage")%> </label>
</div>
 </div></div>
 		
 	<div class="col-md-4"> 
 	 <div class="form-group">
	<div class="radioButtonSubmenuUIG">
<label for="I"><%= smartHF.buildRadioOption(sv.action, "action", "I")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Release - Beneficiary")%> </label>
</div></div>
</div>
 </div>


	<div class="row">	
				<div class="col-md-4"> 
			    <div class="form-group">    
<div class="radioButtonSubmenuUIG">

<label for="J"><%= smartHF.buildRadioOption(sv.action, "action", "J")%> 
<%=resourceBundleHandler.gettingValueFromBundle("Death Claim Hold - Enquiry")%></label>
</div>
 </div></div>
 
 <div class="col-md-4"> </div>
 <div class="col-md-4"> </div>
 </div>
 <%} %>

			    	
 <input name='action' 
type='hidden'
value='<%=sv.action.getFormData()%>'
size='<%=sv.action.getLength()%>'
maxLength='<%=sv.action.getLength()%>' 
class = "input_cell"
onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >

</div>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

