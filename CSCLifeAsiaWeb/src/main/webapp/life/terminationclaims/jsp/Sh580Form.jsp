<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH580";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh580ScreenVars sv = (Sh580ScreenVars) fw.getVariables();%>
	<%StringData generatedText17 = new StringData("-----------------------------------------------------------------------------");%>
	<%StringData generatedText18 = new StringData("Sum Assured ");%>
	<%StringData generatedText19 = new StringData("Adjustments ");%>
	<%StringData generatedText20 = new StringData("New Sum Assured ");%>
	<%StringData generatedText21 = new StringData("Surrender Amt ");%>
	<%StringData generatedText22 = new StringData("Instalment Premium ");%>
	<%StringData generatedText23 = new StringData("Total ");%>
	<%StringData generatedText24 = new StringData("New Instalment Prem ");%>

<%{
		if (appVars.ind11.isOn()) {
			sv.sumins02.setReverse(BaseScreenData.REVERSED);
			sv.sumins02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.sumins02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.estimateTotalValue.setReverse(BaseScreenData.REVERSED);
			sv.estimateTotalValue.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.estimateTotalValue.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.instprem02.setReverse(BaseScreenData.REVERSED);
			sv.instprem02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.instprem02.setHighLight(BaseScreenData.BOLD);
		}
		 if (appVars.ind57.isOn()) {
				sv.bankacckey.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind62.isOn()) {
				sv.crdtcrd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind56.isOn()) {
				sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
			}
		 if (appVars.ind61.isOn()) {
				sv.crdtcrd.setEnabled(BaseScreenData.DISABLED);
			}
		 if (appVars.ind55.isOn()) {
				sv.bankacckey.setReverse(BaseScreenData.REVERSED);
				sv.bankacckey.setColor(BaseScreenData.RED);
			}
		 if (appVars.ind60.isOn()) {
				sv.crdtcrd.setReverse(BaseScreenData.REVERSED);
				sv.crdtcrd.setColor(BaseScreenData.RED);
			}
		 if (!appVars.ind55.isOn()) {
				sv.bankacckey.setHighLight(BaseScreenData.BOLD);
			}
		 if (!appVars.ind60.isOn()) {
				sv.crdtcrd.setHighLight(BaseScreenData.BOLD);
			}
		 
		 if (appVars.ind80.isOn()) {
				sv.reqntype.setReverse(BaseScreenData.REVERSED);
				sv.reqntype.setColor(BaseScreenData.RED);
			}
		 if (appVars.ind88.isOn()) {
				sv.payee.setInvisibility(BaseScreenData.INVISIBLE);
				sv.payee.setEnabled(BaseScreenData.DISABLED);
			}
		 if (appVars.ind18.isOn()) {
				sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
			}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Loans ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------------------Coverage/Rider Details---------------------------");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ccy");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Surrender Value");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind14.isOn()) {
			sv.policyloan.setReverse(BaseScreenData.REVERSED);
			sv.policyloan.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.policyloan.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>



<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					    		     <table><tr>
					    		     <td>
										<%					
										if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width:100px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
							  		</td>
							  		<td>  		
										<%					
										if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width:70px; margin-left: 1px">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
  									</td>
  									<td>  		
										<%					
										if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
				      			     </td>
				      			   </tr>
				      			</table>
				    		</div>
					</div>
				    		
				    <div class="col-md-4">
						<div class="form-group">
						<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	
							<div class="input-group">
						    		
  		
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
							<table>
								<tr>
									<td>						    			
									<%					
									if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width:100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  									</td>
  									<td>  		
									<%					
									if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 150px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  		</td>
							 	 </tr>
							  </table>
						</div>
				   </div>	
		    </div>
				   
				   
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					    		     <table><tr><td>					    		
									<%					
									if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width:100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  									</td>
  									<td>  		
									<%					
									if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left: 1px">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  									</td></tr></table>

				      			     </div>
				    		</div>
					  
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
							<table><tr><td>				    		
							<%					
							if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  		</td>
					  		<td>
							<%					
							if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:150px;margin-left: 1px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>  
				      		</td></tr></table>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
							<table><tr>
							<td>			
						    		
	
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  <td>  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:100px;margin-left: 1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>  
	</td></tr></table>
				      			  
						</div>
				   </div>	
		    </div>
		    
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
					    		     <div class="input-group">
						    		
	<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
				    		</div>
					</div>
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
							<div class="input-group">
						    			
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Component")%></label>
							<div class="input-group">
						    		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("crtable");
		longValue = (String) mappedItems.get((sv.crtable.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loans")%></label>
					    		     <div class="input-group"  style="width:100px;">
						    				
		<%	
			qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.policyloan,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	

				      			     </div>
				    		</div>
					</div>
					
						<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					    		<div class="input-group">
					    		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					    		
					    	</div>
					    </div>
				    </div>
		    </div>
				   
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#contact_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Financial Details")%></label></a>
						</li>
						<li><a href="#other_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></label></a>
						</li>
						<%if((new Byte((sv.payee).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
						<li><a href="#third_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Payout Details")%></label></a>
						<%}%>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="contact_tab">
					<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
                      <div class="input-group"style="min-width:100px;">	
  			<%	
			qpsf = fw.getFieldXMLDef((sv.sumins01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumins01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.sumins01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
  		
	
    	 				
    	 				</div></div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustments")%></label>	
 <div class="input-group">	
<%	
			qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.otheradjst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='otheradjst' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.otheradjst.getLength(), sv.otheradjst.getScale(),3)%>'
maxLength='<%= sv.otheradjst.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.otheradjst).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.otheradjst).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.otheradjst).getColor()== null  ? 
			"input_cell" :  (sv.otheradjst).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
		
	
    	 				
    	 			</div></div>	</div>
    	 				
    	 				
    
    
                      <div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("New Sum Assured")%></label>	
 <div class="input-group">	

	<%	
			qpsf = fw.getFieldXMLDef((sv.sumins02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumins02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='sumins02' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins02.getLength(), sv.sumins02.getScale(),3)%>'
maxLength='<%= sv.sumins02.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumins02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumins02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumins02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumins02).getColor()== null  ? 
			"input_cell" :  (sv.sumins02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
		
	
    	 				
    	 			</div>
	
  			
    	 					</div></div>					
    	                 </div>
    	 					
<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Amt")%></label>
 <div class="input-group"style="min-width:100px;">	

		
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
    	 				
    	 			</div>
  		
	
    	 				
    	 				</div></div>
    	 				
    	 			
    	 			
    	 			
    	 			<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%></label>	
 <div class="input-group" >	

			<%	
			qpsf = fw.getFieldXMLDef((sv.instprem01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instprem01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.instprem01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
		
	
    	 				
    	 			</div>

		
	
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    
                      <div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("New Instalment Prem")%></label>	

	 <div class="input-group">	

				
		<%	
			qpsf = fw.getFieldXMLDef((sv.instprem02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instprem02,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.instprem02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
	
    	 				
    	 			</div>
  			
    	 					</div></div>					
    	                 </div>
    	 					






                  <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
 <div class="input-group" >	

		<%	
			qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:100px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	
    	 				
    	 			</div>
    	 					</div></div></div>
    	 					
    	 					    	 					
    	 					</div>  
    	 					
    	 				    	 				
    	 		
						
						
						<div class="tab-pane fade" id="other_tab"  class="table-responsive">
							
	         <table id="sh580Table" class="table table-striped table-bordered table-hover"  width="100%">
               <thead>
		
			        <tr class="info">
			        <th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
		<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
		<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
		<th style="text-align:center"><%=resourceBundleHandler.gettingValueFromBundle("Surrender Value")%></th>
		   </tr>
			 </thead>
			 
	<%
		GeneralTable sfl = fw.getTable("sh580screensfl");
		/* int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}
		 */
		%>

<%

	
	Sh580screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sh580screensfl
	.hasMoreScreenRows(sfl)) {
	
%>


<tbody>


		<tr  >
	
											<td >													
										
											<%= formatValue( sv.fieldType.getFormData() )%>	 
										
				 
				
					</td>	
													<td >									
										
											<%= formatValue( sv.descrip.getFormData() )%>	 
										
				 
				
					</td>	
													<td >									
										
											<%= formatValue( sv.cnstcur.getFormData() )%>	 
										
				 
				
					</td>	
													<td>									
										
				    <%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>	
										 		
			 		<%= formatValue( smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) )%>
										
				 
				
					</td>	
		
	</tr></tbody>


	<%
	
	count = count + 1;
	Sh580screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	
	%>

		</table>
		</div>
		<!-- ICIL-254 start  -->
		<%if((new Byte((sv.payee).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
		<div class="tab-pane fade" id="third_tab"  class="table-responsive">
		
		<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
                            <div class="input-group three-controller">	
                        
                              <input name='payee' id='payee' type='text' value='<%= sv.payee.getFormData() %>' 	class = " <%=(sv.payee).getColor()== null  ? 
						"input_cell" :  (sv.payee).getColor().equals("red") ? "input_cell red reverse" : "input_cell" %>" 
						maxLength='<%=sv.payee.getLength()%>' 	onFocus='doFocus(this)' onHelp='return fieldHelp(payee)' onKeyUp='return checkMaxLength(this)' 
						 style = "width: 120px;">		
									
						<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px; left:1px;" type="button" onClick="doFocus(document.getElementById('payee')); changeF4Image(this); doAction('PFKEY04');">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
                       <div name ="payeename" id="payeename" style=" margin-left: 1px; background-color: rgb(238, 238, 238); color: rgb(153, 153, 153); font-weight: bold; width: 150px;"
                        class="form-control ellipsis"><%= sv.payeename.getFormData()==null?"":sv.payeename.getFormData() %></div>
             </div>
             </div>
            </div>
            
           
 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
				    	 <div class="input-group">
				    	<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("reqntype");
							optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
						%>
						
						<% 
							if((new Byte((sv.reqntype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>`
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.reqntype, fw, longValue, "reqntype", optionValue, 0) %>
						<%
							} 
						%>  
				    </div></div>
				 </div>

</div>
          	<div class="row">
					
			 <%	 if (appVars.ind57.isOn() && appVars.ind62.isOn()) { %>		
			 <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

		
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 100px;">
								
							</div>	
							
						
						
				<!-- 	 <div class="input-group" style="min-width:100px">
					 
					 </div> -->
                                  
</div></div>
					
					 <%
                                         }
                                  %>
					
					
 <%	 if (!(appVars.ind57.isOn())) { %>
              <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

	<table><tr><td class="form-group">	  
                                  <div class="input-group" style="width: 210px;">
                                  
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankacckey)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                 
								 
					  </td></tr></table>	

	</div></div>  <%} %>
	
	 <%	 if (!(appVars.ind62.isOn())) { %>          
	
	 <div class="col-md-4">
		 <div class="form-group">
			 <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>
					<table><tr><td>	 
                                  <div class="input-group" style="width: 210px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.crdtcrd)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('crdtcrd')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                
					  </td></tr></table>	
			 </div> 
		</div>
					    		 <%} %>
	
		<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Code"))%></label>
					<table>
					<tr>
					<td style="min-width: 100px;"> <%=smartHF.getHTMLVarExt(fw, sv.bankkey)%>
				<%-- 	<%=smartHF.getHTMLVarExt(fw, sv.bsortcde)%> --%>
                     </td>
                     </tr>
					</table>
					</div></div>
                     
                     
                     <div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Name"))%></label>
					<table>
					<tr> <td style="min-width:100px;">
					
					<%=smartHF.getHTMLVarExt(fw, sv.bankdesc)%>
						<%-- <%					
				if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell" %>' style="min-width: 100px;">
						<%=formatValue%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%> --%>
					</td>
					</tr>
					
					
					</table>
					
					
					</div></div>
	
	
	
	</div>
	
            
       
		
		
		</div>
		<%}%>
		<!-- ICIL-254 end -->





					</div>
				    		</div>
				    		
				  </div>
						</div>
						
						

				
						
						
						
						</div></div>

		<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("-----------------------------------------------------------------------------")%>
</div>

</tr></table></div>	
				
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->




<script>
	$(document).ready(function() {
		 $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
		        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		    } );
		$('#sh580Table').DataTable({
			ordering : false,
			searching : false,
			scrollY : "250px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false
		});
	});
</script> 
<script>
	$(document).ready(function() {
		if($("#paymthbf option:selected").val() == '4' || $("#paymthbf option:selected").val()=='C'){
			$("#bankbtn").removeClass("disabled");
		}else{
			$("#bankbtn").addClass("disabled");
		}
		$("#paymthbf").change(function(){
			if($("#paymthbf option:selected").val() == '4' || $("#paymthbf option:selected").val()=='C'){
				$("#bankbtn").removeClass("disabled");
			}else{
				$("#bankbtn").addClass("disabled");
			}
		});
	});
</script> 


<%@ include file="/POLACommon2NEW.jsp"%>

