<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6676";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>
<%S6676ScreenVars sv = (S6676ScreenVars) fw.getVariables();%>

<%if (sv.S6676screenWritten.gt(0)) {%>
	<%S6676screen.clearClassString(sv);%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText5)%>


<%}%>

<%if (sv.S6676screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("s6676screensfl");
	savedInds = appVars.saveAllInds();
	S6676screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 13;
	String height = smartHF.fmty(13);
	smartHF.setSflLineOffset(9);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(9)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (S6676screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.action.setClassString("");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%sv.shortdesc.setClassString("");%>
<%	sv.shortdesc.appendClassString("string_fld");
	sv.shortdesc.appendClassString("output_txt");
	sv.shortdesc.appendClassString("highlight");
%>
	<%sv.elemdesc.setClassString("");%>
<%	sv.elemdesc.appendClassString("string_fld");
	sv.elemdesc.appendClassString("output_txt");
	sv.elemdesc.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>
	<%sv.hrgpynum.setClassString("");%>
	<%sv.hcrtable.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 2, sfl, sv.action)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 6, sfl, sv.life)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 11, sfl, sv.coverage)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 15, sfl, sv.rider)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 21, sfl, sv.shortdesc)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 33, sfl, sv.elemdesc)%>



		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		S6676screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.S6676protectWritten.gt(0)) {%>
	<%S6676protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S6676screenctlWritten.gt(0)) {%>
	<%S6676screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s6676screensfl");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No   ");%>
	<%sv.chdrsel.setClassString("");%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Register, 2 - Adjust, 3 - Approve, 4 - Reverse, 5 - Terminate, 6 - Enquire");%>
<%	generatedText3.appendClassString("label_txt");
	generatedText3.appendClassString("information_txt");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Act Life Cov Rider  Component Details");%>
<%	generatedText4.appendClassString("subfile_hdg");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%=smartHF.getLit(3, 3, generatedText2)%>

	<%=smartHF.getHTMLSpaceVar(3, 18, fw, sv.chdrsel)%>
	<%=smartHF.getHTMLF4NSVar(3, 18, fw, sv.chdrsel)%>

	<%=smartHF.getHTMLSpaceVar(3, 30, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 30, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 34, fw, sv.ctypedes)%>

	<%=smartHF.getLit(5, 1, generatedText3)%>

	<%=smartHF.getLit(7, 1, generatedText4)%>





<%}%>



<%@ include file="/POLACommon2.jsp"%>
