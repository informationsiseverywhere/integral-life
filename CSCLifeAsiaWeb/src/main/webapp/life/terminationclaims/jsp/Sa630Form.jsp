<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sa630";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sa630ScreenVars sv = (Sa630ScreenVars) fw.getVariables();
%>

<%
	{
	/* 	if (appVars.ind01.isOn()) {
			sv.notifinum.setReverse(BaseScreenData.REVERSED);
			sv.notifinum.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.notifinum.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.notifinum, appVars.ind50);
		if (!appVars.ind01.isOn()) {
			sv.notifinum.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind122.isOn()) {
			sv.accdesc.setReverse(BaseScreenData.REVERSED);
			sv.accdesc.setColor(BaseScreenData.RED);
		}
		if (appVars.ind65.isOn()) {
			sv.accdesc.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.accdesc, appVars.ind50);
		if (!appVars.ind122.isOn()) {
			sv.accdesc.setHighLight(BaseScreenData.BOLD);
		} */
	}
%>
<div class="panel panel-default">
	<div class="panel-body">	
		<br>
		<hr>
		<%-- <div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Investigation Results")%></label>
				</div>
			</div>
		</div> --%>

		<div class="row">
		<!-- Claim Notification Number -->
		<div class="col-md-2">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.notifinum)%></div>
				</div>
			</div>
			</div>
		
		<div class="row">
			<div class="col-md-4" style='width: 100% !important;'>
				<!-- IFSU-2577 -->
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Investigation Result")%></label>
					<div class="tabblock" id="navbar">
						<!-- IFSU-414 Modified it for crossing browsers by xma3 -->
						<div class="tabcontent">
						<div id="content1">
								<textarea id='accdesc'  name="accdesc"  value=''
									style='width: 100%; height: 335px; z-index: 2;'
									onFocus='doFocus(this)' onHelp='return fieldHelp("accdesc")'
									onkeyup="" class='textarea bold'
									<%if ((new Byte((sv.accdesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
									readonly="true" disabled class="output_cell"
									<%} else if ((new Byte((sv.accdesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%> class='input_cell' <%}%>><%=sv.accdesc.getFormData().toString().trim() %></textarea>
								<!-- IFSU-2577 -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="datime" id="maintime">
	</div>
</div>


	</div>
	<style>
#mainareaDiv>div>div.panel.panel-default>div>div:nth-child(18)>div:nth-child(2)>div>div>div
	{
	width: 165px !important;
}
</style>
<script>
var value = new Date();
$("#maintime").val(value);

</script>

	<%@ include file="/POLACommon2NEW.jsp"%>
