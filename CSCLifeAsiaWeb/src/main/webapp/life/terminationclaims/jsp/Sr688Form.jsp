

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR688";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr688ScreenVars sv = (Sr688ScreenVars) fw.getVariables();%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann Acc Amt  ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"TotClm Amt ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Acc Amt ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Level Deductible   ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Level Co-Pay Amt   ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Aggregated Amt   ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Claim Payable ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Enter=Exit");%>

<%{
		if (appVars.ind21.isOn()) {
			sv.taccamt1.setReverse(BaseScreenData.REVERSED);
			sv.taccamt1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.taccamt1.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.tactexp.setReverse(BaseScreenData.REVERSED);
			sv.tactexp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.tactexp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.tclmamt.setReverse(BaseScreenData.REVERSED);
			sv.tclmamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.tclmamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.taccamt2.setReverse(BaseScreenData.REVERSED);
			sv.taccamt2.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.taccamt2.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.tdeduct1.setReverse(BaseScreenData.REVERSED);
			sv.tdeduct1.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.tdeduct1.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.clamamt.setReverse(BaseScreenData.REVERSED);
			sv.clamamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.clamamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.tdeduct2.setReverse(BaseScreenData.REVERSED);
			sv.tdeduct2.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.tdeduct2.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.tdeduct3.setReverse(BaseScreenData.REVERSED);
			sv.tdeduct3.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.tdeduct3.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<%{
		if (appVars.ind12.isOn()) {
			sv.actexp.setReverse(BaseScreenData.REVERSED);
			sv.actexp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.actexp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.gcnetpy.setReverse(BaseScreenData.REVERSED);
			sv.gcnetpy.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.gcnetpy.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comp Code  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Code ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No.of Units ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Incur Date ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Admit Dt  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Discharge ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Diagnosis  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Doctor    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Provider   ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Level ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annl ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dedct ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CoI% ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1-Create");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2-Modify");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3-Delete");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"4-Enquiry");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"   -----Benefit-----  --Per Claim Limit- ------Actual------ ---Claim Payable--");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel      Description  LADC  Amount   Dys       Expense  Dys        Amount  Dys");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Per Claim Limit");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Actual");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Payable");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel    ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"LADC");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dys");%>
	<%StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Expense");%>
	<%StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dys");%>
	<%StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	<%StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dys");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind01.isOn()) {
			sv.incurdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.incurdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.incurdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.gcadmdtDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind80.isOn()) {
			sv.gcadmdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.gcadmdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.gcadmdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.dischdtDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind80.isOn()) {
			sv.dischdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.dischdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.dischdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.diagcde.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind80.isOn()) {
			sv.diagcde.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.diagcde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.diagcde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind80.isOn()) {
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.zdoctor.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.zmedprv.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind80.isOn()) {
			sv.zmedprv.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.zmedprv.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.zmedprv.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
				<div class="col-md-4">
	                 
<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Comp Code")%>
</label>
<%}%>

  <div class="form-group" style="max-width:100px;">
<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div></div>

<div class="col-md-4">
	                   <div class="form-group">
<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Plan Code")%>
</label>
<%}%>


<%if ((new Byte((sv.benpln).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.benpln.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benpln.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benpln.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>

<div class="col-md-4">
	                   <div class="form-group">
<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("No.of Units")%>
</label>
<%}%>


<%if ((new Byte((sv.zunit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zunit).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zunit);
			
			if(!((sv.zunit.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>
</div>

 <div class="row">
				<div class="col-md-4">
<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Incur Date")%>
</label>
<%}%>


<div class="form-group" style="max-width:100px;">

<%if ((new Byte((sv.incurdtDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.incurdtDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</div>
</div>

<div class="col-md-4">
<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Admit Dt")%>
</label>
<%}%>

<div class="form-group">

<%	
	if ((new Byte((sv.gcadmdtDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.gcadmdtDisp.getFormData();  
%>

<% 
	if((new Byte((sv.gcadmdtDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="gcadmdtDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.gcadmdtDisp,(sv.gcadmdtDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>

<%} }%>
</div></div>

<div class="col-md-4">
<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Discharge")%>
</label>
<%}%>

<div class="form-group">
<%	
	if ((new Byte((sv.dischdtDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.dischdtDisp.getFormData();  
%>

<% 
	if((new Byte((sv.dischdtDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<div class="input-group date form_date col-md-12" style="min-width:150px;" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dischdtDisp" data-link-format="dd/mm/yyyy" style="min-width:100px;">
 <%=smartHF.getRichTextDateInput(fw, sv.dischdtDisp,(sv.dischdtDisp.getLength()))%>
<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div>
<%} }%>
</div>
</div>
</div>

 <div class="row">
				<div class="col-md-4">
<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Diagnosis")%>
</label>
<%}%>

<div class="form-group">
<%	
	if ((new Byte((sv.diagcde).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"diagcde"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("diagcde");
	optionValue = makeDropDownList( mappedItems , sv.diagcde.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.diagcde.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.diagcde).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.diagcde).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='diagcde' type='list' style="width:140px;"
<% 
	if((new Byte((sv.diagcde).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.diagcde).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.diagcde).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
</div>
</div>

<div class="col-md-4"> 
<div class="form-group">
			    	    
    <label><%=resourceBundleHandler.gettingValueFromBundle("Doctor")%></label>
		                   
	 <table>
	 <tr>
	 <td>
		 <div class="input-group">                    
		 <input name='zdoctor' id='zdoctor' type='text'
			value='<%=sv.zdoctor.getFormData()%>'
			maxLength='<%=sv.zdoctor.getLength()%>'
			size='<%=sv.zdoctor.getLength()%>' onFocus='doFocus(this)'
			onHelp='return fieldHelp(zdoctor)'
			onKeyUp='return checkMaxLength(this)'
			<%if ((new Byte((sv.zdoctor).getEnabled())).compareTo(new Byte(
					BaseScreenData.DISABLED)) == 0) {%>
			readonly="true" class="output_cell"> <%
 	} else if ((new Byte((sv.zdoctor).getHighLight()))
 			.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
 %> class="bold_cell" >
 
     
     <span class="input-group-btn">
<button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>
     
     
   
   
   <%
 	} else {
 %> class='<%=(sv.zdoctor).getColor() == null ? "input_cell"
								: (sv.zdoctor).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>' > <span class="input-group-btn">
<button class="btn btn-info"  type="button" onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span> <%
 	}
 %>
 </div>
</td>
<td>
		<div
			class='<%=(sv.givname.getFormData()).trim().length() == 0 ? "blank_cell"
							: "output_cell"%> iconPos' style="width:130px";>
							
		<%
			if (!((sv.givname.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.givname.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			} else {
				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.givname.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}
			}
		%> <%=XSSFilter.escapeHtml(formatValue)%>
		</div>
		<%
			longValue = null;
			formatValue = null;
		%>
	
	
	</td>
	</tr>
	</table>	
		
		</div>
		</div>
		
	

<div class="col-md-4">
<div class="form-group">
<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Provider")%>
</label>
<%}%><table><tr><td>


<%if ((new Byte((sv.zmedprv).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.zmedprv.getFormData();  
%>

<% 
	if((new Byte((sv.zmedprv).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='zmedprv' id="zmedprv"
type='text' 
value='<%=sv.zmedprv.getFormData()%>' 
maxLength='<%=sv.zmedprv.getLength()%>' 
size='<%=sv.zmedprv.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(zmedprv)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.zmedprv).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.zmedprv).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 </td>
 <td >
			<span class="input-group-btn">
									<button class="btn btn-info"									
										type="button"
										onClick="doFocus(document.getElementById('zmedprv')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>

<%
	}else { 
%>

class = ' <%=(sv.zmedprv).getColor()== null  ? 
"input_cell" :  (sv.zmedprv).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

		<span class="input-group-btn">
									<button class="btn btn-info"									
										type="button"
										onClick="doFocus(document.getElementById('zmedprv')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
<%}longValue = null;}} %>

</td>
<td>




<%if ((new Byte((sv.cdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell1" %>' style="width: 120px;">     <!-- changed by pmujavadiya -->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
</td></tr></table>	
</div>
</div>
</div>


 <div class="row">
				<div class="col-md-2">
<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Plan Level")%>
</label>
<%}%>
</div>
</div>



<div class="row">
				<div class="col-md-3">
<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
</label>
<%}%>

<div class="form-group">

<%if ((new Byte((sv.amtlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.amtlife).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.amtlife,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.amtlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-3">
<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Annl")%>
</label>
<%}%>


<div class="form-group">

<%if ((new Byte((sv.amtyear).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.amtyear).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.amtyear,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.amtyear.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-3">
<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("Dedct")%>
</label>
<%}%>

<div class="form-group">

<%if ((new Byte((sv.gdeduct).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.gdeduct).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.gdeduct,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.gdeduct.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-2">
<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label>
<%=resourceBundleHandler.gettingValueFromBundle("CoI%")%>
</label>
<%}%>


<div class="form-group">

<%if ((new Byte((sv.copay).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.copay).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.copay);
			
			if(!((sv.copay.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</div>
</div>
</div>




		<%
		GeneralTable sfl = fw.getTable("sr688screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>
<!-- ILIFE-2679 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 5 starts  -->

<%-- 
<div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" class ="tablePos" style='top:21px; width: <%if(totalTblWidth < 730 ) { <%=totalTblWidth%>px;<%} else { %>730px;<%}%> height: 170px; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 210 ) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		
		<DIV id="subf" style="POSITION: relative; WIDTH: <%=totalTblWidth%>px; HEIGHT: 170px;">
		<table style="width:width:980PX; margin-top: -2px; margin-left: -2px;" bgcolor="#dddddd" cellspacing="1px" class="tableTag" id="table">
			 <tr style="background: #316494; height: 25px;z-index:12;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop+1);">
		
														<td rowspan="2"  style="color:white; font-weight:bold; padding: 5px;z-index:8; width:<%=tblColumnWidth[0]%>px; position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></td>
														<td colspan="2" class="tableDataHeader"  style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
														align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></td>
														
														<td colspan="3" class="tableDataHeader" style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
														align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header12")%></td>
														
														<td colspan="2" class="tableDataHeader" style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
														align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header13")%></td>
														
														<td colspan="2" class="tableDataHeader" style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; border-bottom: 1px solid #dddddd;"
														align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header14")%></td>
														
														
														
														
														<td rowspan="2" class="tableColHeaderFixed" align="left">
			 <div class="tableRowHeaderDivFixed" style="padding: 0px; border-right:-1px;height:49px; border-top: -2px; width:<%=tblColumnWidth[0]+5%>px;"  align="left">
				<table style="width: 100%; border-bottom: 0px;border-right:-1px; border-top: -2px;">
				<tr>
				<td  style="width:<%= tblColumnWidth[0] + 5%>px; padding-bottom: 0px;border-right:-1px; " align="center" valign="bottom">
				<div class="tableDataHeader" style="padding: 0px; height:49px; width:100%;" align="center">
				<%=resourceBundleHandler.gettingValueFromBundle("Header1")%>
				</div>
				</td>																													
				</tr>
				</table>		
			</div>					
			</td>	
		</tr>
														
														
														
			<tr style="height: 25px; background: #316494;z-index:6;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop+1);">
				<td style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></td>
				<td style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></td>
				<td style="color:white; font-weight: bold;padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></td>
				<td style="color:white; font-weight: bold;padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></td>
				<td style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></td>
				<td style="color:white; font-weight: bold; padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></td>
				<td style="color:white; font-weight: bold;padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></td>
				<td style="color:white; font-weight: bold;padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></td>
				<td style="color:white; font-weight: bold;padding: 5px;z-index:8;position:relative;border-right: 1px solid #dddddd; "><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></td>
			</tr>
			--%>													


<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                   <div class="table-responsive">
	         <table  id='dataTables-sr688' class="table table-striped table-bordered table-hover" 
	         width="100%" style="border-right: thin solid #dddddd !important;" >
                            <thead>
                            <tr class="info">
		
		<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
		         								
					<th colspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header11")%></th>
					<th colspan="3" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header12")%></th>
					<th colspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header13")%></th>
					<th colspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header14")%></th>
	
					
	</tr> </thead> <thead>
	<tr class="info">
		             <th></th>
		             <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
		         								
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
					<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
	               <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
	               <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
	               <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></th>
	               <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></th>
					
	</tr></thead>
<!-- ILIFE-2679 Life Cross Browser -Coding and UT- Sprint 3 D6: Task 5  ends --> 	
														
														
														
	<%
	Sr688screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr688screensfl
	.hasMoreScreenRows(sfl)) {	
%>

	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						<%if((new Byte((sv.sel).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>
																			
								
													
													
<!-- 					ILIFE-420 -->
					 					<div style="position: relative;left:20px;"> 
					 <input type="checkbox" 
						 value='<%= sv.sel.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr688screensfl" + "." +
						 "sel")' onKeyUp='return checkMaxLength(this)' 
						 name='sr688screensfl.sel_R<%=count%>'
						 id='sr688screensfl.sel_R<%=count%>'
						 onClick="selectedRow('sr688screensfl.sel_R<%=count%>')"
						 class="UICheck"
					 /></div>
					 
					 					
					
											
									</td>
		<%}else{%>
												<td>
					</td>														
										
					<%}%>
								<%if((new Byte((sv.hosben).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
									
											
						<%= sv.hosben.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.shortds).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
									
											
						<%= sv.shortds.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.limitc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
									
											
						<%= sv.limitc.getFormData()%>
						
														 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.benfamt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.benfamt).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.benfamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.benfamt).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.accday).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.accday).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.accday);
							if(!(sv.accday).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.actexp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.actexp).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.actexp,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!(sv.actexp).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.nofday).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.nofday).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.nofday);
							if(!(sv.nofday).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.gcnetpy).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.gcnetpy).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.gcnetpy,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!(sv.gcnetpy).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
								<%if((new Byte((sv.zdaycov).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    									<td>									
								
											
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.zdaycov).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.zdaycov);
							if(!(sv.zdaycov).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
		<%}else{%>
												<td>
														
				    </td>
										
					<%}%>
									
	</tr>

	<%
	count = count + 1;
	Sr688screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>

		

</div>
</div>
</div>
</div>

<table><tr>
		<td>
		    <div class="sectionbutton"><!-- style added by pmujavadiya -->
			<a href="#" class="btn btn-info" onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Create")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" class="btn btn-info" onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
			</div>
		</td>
		<td>
		    <div class="sectionbutton">
			<a href="#" class="btn btn-info" onClick="JavaScript:perFormOperation(3)"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" class="btn btn-info" onClick="JavaScript:perFormOperation(4)"><%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></a>
			</div>
		</td>
		</tr></table>
<!-- Added by by pmujavadiya -->


<div class="row">
            <div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Ann Acc Amt")%>
</label>
<%}%>


<%if ((new Byte((sv.taccamt1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.taccamt1).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.taccamt1,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.taccamt1.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	

</div>
</div>

  <div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Life Acc Amt")%>
</label>
<%}%>


<%if ((new Byte((sv.taccamt2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.taccamt2).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.taccamt2);
			
			if(!((sv.taccamt2.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

  <div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("TotClm Amt")%>
</label>
<%}%>



<%if ((new Byte((sv.tactexp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tactexp).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tactexp,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.tactexp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
</div>
</div>

<div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Claim Amount Payable")%>
</label>
<%}%>




<%if ((new Byte((sv.tclmamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tclmamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tclmamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.tclmamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>
</div>



<div class="row">
            <div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Plan Level Deductible")%>
</label>
<%}%>


<%if ((new Byte((sv.tdeduct1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tdeduct1).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tdeduct1,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.tdeduct1.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-3">
  <div class="form-group">             
<%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Plan Level Co-Pay Amt")%>
</label>
<%}%>
 
<%if ((new Byte((sv.tdeduct2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tdeduct2).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tdeduct2);
			
			if(!((sv.tdeduct2.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>

</div>
</div>
	

<div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Annual Aggregated Amt")%>
</label>
<%}%>


<%if ((new Byte((sv.tdeduct3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tdeduct3).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tdeduct3);
			
			if(!((sv.tdeduct3.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
</div>
</div>

<div class="col-md-3">
                <div class="form-group">
<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<label  style="white-space: nowrap;">
<%=resourceBundleHandler.gettingValueFromBundle("Total Claim Payable")%>
</label>
<%}%>


<br/>

<%if ((new Byte((sv.clamamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
			
			if(!((sv.clamamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
</div>	
</div>
</div>

<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

<%if ((new Byte((sv.shortdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.shortdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td>

<td width='188'>
<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Enter=Exit")%>
</div>
<%}%>


<br/>

<%if ((new Byte((sv.desc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.desc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.desc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.desc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</td></tr>
</table></div>
</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>

