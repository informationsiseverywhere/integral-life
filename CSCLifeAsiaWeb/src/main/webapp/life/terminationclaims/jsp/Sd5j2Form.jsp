

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sd5j2";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sd5j2ScreenVars sv = (Sd5j2ScreenVars) fw.getVariables();
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div>
						<%
							if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.company.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div>
						<%
							if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.tabl.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



</td><td>
<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
				
					</td></tr></table>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
				<br>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Type")%></label>
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Long Term")%></label>
					
						<%if ((new Byte((sv.longTerm).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
		
									<input type='checkbox' name='longTerm' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(longTerm)' onKeyUp='return checkMaxLength(this)'    
									<%
									
									if((sv.longTerm).getColor()!=null){
												 %>style='background-color:#FF0000;'
											<%}
											if((sv.longTerm).toString().trim().equalsIgnoreCase("Y")){
												%>checked
									
									      <% }if((sv.longTerm).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
									    	   disabled
									
											<%}%>
									class ='UICheck' onclick="handleCheckBox('longTerm')"/>
									
									<input type='checkbox' name='longTerm' value='N' 
									
									<% if(!(sv.longTerm).toString().trim().equalsIgnoreCase("Y")){
												%>checked
									
									      <% }%>
									
									style="visibility: hidden" onclick="handleCheckBox('longTerm')"/>
									<%}%>
									
									<% if("red".equals((sv.longTerm).getColor())){
									%>
									
									<%
									} 
									%>
									
									<%
									
									%>	
					
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Short Term")%></label>
					
						<%=smartHF.getHTMLCheckBox(sv.shortTerm, fw, "N")%>
					
				</div>
			</div>
		
		</div>
		

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->


<%@ include file="/POLACommon2NEW.jsp"%>

