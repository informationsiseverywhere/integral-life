

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6690";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6690ScreenVars sv = (S6690ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Default Regular Payment Type for Component      ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.rgpytype.setReverse(BaseScreenData.REVERSED);
			sv.rgpytype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.rgpytype.setHighLight(BaseScreenData.BOLD);
		}
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 75px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<div class="input-group">
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Default Regular Payment Type for Component")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "rgpytype" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("rgpytype");
						optionValue = makeDropDownList(mappedItems, sv.rgpytype.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.rgpytype.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.rgpytype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.rgpytype).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 240px;">
						<%
							}
						%>

						<select name='rgpytype' type='list' style="width: 240px;"
							<%if ((new Byte((sv.rgpytype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							style="max-width:240px;"
							<%} else if ((new Byte((sv.rgpytype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="max-width:240px;" <%} else {%>
							class='input_cell' style="max-width:240px;" <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.rgpytype).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>
				</div>
			</div>
		</div>

	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

