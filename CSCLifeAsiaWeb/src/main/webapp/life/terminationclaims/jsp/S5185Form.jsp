<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5185";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5185ScreenVars sv = (S5185ScreenVars) fw.getVariables();%>

<%if (sv.S5185screenWritten.gt(0)) {%>
	<%S5185screen.clearClassString(sv);%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText16)%>


<%}%>


<%if (sv.S5185protectWritten.gt(0)) {%>
	<%S5185protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S5185screenctlWritten.gt(0)) {%>
	<%S5185screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5185screensfl");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Frequency ");%>
	<%sv.payfreq.setClassString("");%>
<%	sv.payfreq.appendClassString("string_fld");
	sv.payfreq.appendClassString("output_txt");
	sv.payfreq.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid to Date   ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm Date   ");%>
	<%sv.currfromDisp.setClassString("");%>
<%	sv.currfromDisp.appendClassString("string_fld");
	sv.currfromDisp.appendClassString("output_txt");
	sv.currfromDisp.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method    ");%>
	<%sv.mop.setClassString("");%>
<%	sv.mop.appendClassString("string_fld");
	sv.mop.appendClassString("output_txt");
	sv.mop.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed to Date ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel Plan  Risk Status                       Premium Status");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText2)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-7">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLVarExt(fw, sv.chdrnum, 0, 100)%>
					<%=smartHF.getHTMLSpaceVar(fw, sv.cnttype)%>
					<%=smartHF.getHTMLF4NSVarExt(fw, sv.cnttype)%>
					<%=smartHF.getHTMLVarExt(fw, sv.ctypedes, 0, 400)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		     
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText3)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<div class="input-group">
		       			<%=smartHF.getHTMLSpaceVar(fw, sv.cntcurr)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntcurr)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-2">
			       		<div class="form-group">
			       		<label><%=smartHF.getLit(generatedText4)%></label>
			       		</div>
		       		</div>
		       		
		       		<div class="col-md-2">
			       		<div class="form-group">
			       		<div class="input-group">
			       		<%=smartHF.getHTMLVarExt(fw, sv.chdrstatus, 0, 100)%>
			       		</div>
			       		</div>
		       		</div>
		       		
		       		<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText5)%></label>
		       		</div>
		       		</div>
		       		
		       		<div class="col-md-2">
			       		<div class="form-group">
			       		<div class="input-group">
			       		<%=smartHF.getHTMLVarExt(fw, sv.premstatus, 0, 100)%>
			       		</div>
			       		</div>
			       	</div>
		       		
		       		<div class="col-md-1">
			       		<div class="form-group">
			       		<label><%=smartHF.getLit(generatedText6)%></label>
				   		</div>
			   		</div>
		       		
		       		<div class="col-md-1">
			       		<div class="form-group">
			       		<div class="input-group">
			       		<%=smartHF.getHTMLSpaceVar(fw, sv.register)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.register)%>
			       		</div>
			       		</div>
		       		</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText7)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-8">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 600px;">
		       		<%=smartHF.getHTMLVarExt(fw, sv.lifenum, 0, 100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.lifename, 100, 600)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText8)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label></label>
		       		<div class="input-group" style="min-width: 600px;">
		       			<%=smartHF.getHTMLVarExt(fw, sv.jlife, 0, 100)%>
						<%=smartHF.getHTMLVarExt(fw, sv.jlifename, 100, 600)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    
		    <br/><br/>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText9)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLVarExt(fw, sv.numpols, 0, 70)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3" style="max-width: 180px;">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText10)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLVarExt(fw, sv.payfreq, 0, 50)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText11)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLSpaceVar(fw, sv.ptdateDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.ptdateDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText12)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLSpaceVar(fw, sv.currfromDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.currfromDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2" style="min-width: 180px;">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText13)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLSpaceVar(fw, sv.mop)%>
					<%=smartHF.getHTMLF4NSVarExt(fw, sv.mop)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText14)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
		       		<%=smartHF.getHTMLSpaceVar(fw, sv.btdateDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.btdateDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    <br/><br/>
		   <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5185'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<%=smartHF.getLit(0, 2, generatedText15)%>
						</tr>	
			         	</thead>
					      <tbody>
					      <%if (sv.S5185screensflWritten.gt(0)) {%>
							<%GeneralTable sflNew = fw.getTable("s5185screensfl");
							savedInds = appVars.saveAllInds();
							S5185screensfl.set1stScreenRow(sflNew, appVars, sv);
							double sflLine = 0.0;
							int doingLine = 0;
							int sflcols = 1;
							int linesPerCol = 11;
							String height = smartHF.fmty(11);
							smartHF.setSflLineOffset(12);
							%>
							<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(1)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
							<%while (S5185screensfl.hasMoreScreenRows(sflNew)) {%>
							<%sv.select.setClassString("");%>
							<%sv.planSuffix.setClassString("");%>
						<%	sv.planSuffix.appendClassString("num_fld");
							sv.planSuffix.appendClassString("output_txt");
							sv.planSuffix.appendClassString("highlight");
						%>
							<%sv.covRiskStat.setClassString("");%>
						<%	sv.covRiskStat.appendClassString("string_fld");
							sv.covRiskStat.appendClassString("output_txt");
							sv.covRiskStat.appendClassString("highlight");
						%>
							<%sv.rstatdesc.setClassString("");%>
						<%	sv.rstatdesc.appendClassString("string_fld");
							sv.rstatdesc.appendClassString("output_txt");
							sv.rstatdesc.appendClassString("highlight");
						%>
							<%sv.covPremStat.setClassString("");%>
						<%	sv.covPremStat.appendClassString("string_fld");
							sv.covPremStat.appendClassString("output_txt");
							sv.covPremStat.appendClassString("highlight");
						%>
							<%sv.pstatdesc.setClassString("");%>
						<%	sv.pstatdesc.appendClassString("string_fld");
							sv.pstatdesc.appendClassString("output_txt");
							sv.pstatdesc.appendClassString("highlight");
						%>
							<%sv.screenIndicArea.setClassString("");%>
							<%sv.rider.setClassString("");%>
							<%sv.coverage.setClassString("");%>
							<%sv.life.setClassString("");%>
						
							<%
						{
								if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
									sv.select.setColor(BaseScreenData.RED);
								}
								if (appVars.ind02.isOn()) {
									sv.select.setEnabled(BaseScreenData.DISABLED);
								}
								if (appVars.ind04.isOn()) {
									sv.select.setInvisibility(BaseScreenData.INVISIBLE);
								}
								if (!appVars.ind01.isOn()) {
									sv.select.setHighLight(BaseScreenData.BOLD);
								}
							}
						
							%>
						
								<%=smartHF.getTableHTMLVarQual(sflLine, 2, sflNew, sv.select)%>
								<%=smartHF.getTableHTMLVarQual(sflLine, 6, sflNew, sv.planSuffix)%>
								<%=smartHF.getHTMLSFSpaceVar(sflLine, 12, sflNew, sv.covRiskStat)%>
								<%=smartHF.getHTMLF4SSVar(sflLine, 12, sflNew, sv.covRiskStat)%>
								<%=smartHF.getTableHTMLVarQual(sflLine, 15, sflNew, sv.rstatdesc)%>
								<%=smartHF.getHTMLSFSpaceVar(sflLine, 46, sflNew, sv.covPremStat)%>
								<%=smartHF.getHTMLF4SSVar(sflLine, 46, sflNew, sv.covPremStat)%>
								<%=smartHF.getTableHTMLVarQual(sflLine, 49, sflNew, sv.pstatdesc)%>
						
						
						
						
								<%sflLine += 1;
								doingLine++;
								if (doingLine % linesPerCol == 0 && sflcols > 1) {
									sflLine = 0.0;
								}
								S5185screensfl.setNextScreenRow(sflNew, appVars, sv);
							}%>
							</div>
							<%appVars.restoreAllInds(savedInds);%>
						
						
						<%}%>
					      </tbody>
					      
					</table>
					</div>
				</div>
			</div>
		</div> 
		   <br/><br/>
	 </div>
</div>


<%}%>

<%@ include file="/POLACommon2NEW.jsp"%>
