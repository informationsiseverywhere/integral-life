<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH612";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sh612ScreenVars sv = (Sh612ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Lapse Reinstatement");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Lapse Reinstatement Enquiry");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Reinstate Contract for Redating");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Reverse Contract for Redating");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Convert to Proposal for Redating");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reinstatement Date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action             ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group">
						    		 
<input name='chdrsel' id='chdrsel'
type='text' 
value='<%=sv.chdrsel.getFormData()%>' 
maxLength='<%=sv.chdrsel.getLength()%>' 
size='<%=sv.chdrsel.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.chdrsel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.chdrsel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.chdrsel).getColor()== null  ? 
"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

	<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>


	
				      			</div>
				    		</div>
			    	</div>
			    	
			    	<div class="col-md-2"> </div>
			    	<div class="col-md-3"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Reinstatement Date")%></label>
					    		<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="effdateDisp" data-link-format="dd/mm/yyyy">
						    	
<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%} %>
				      			</div>
				    		</div>
			    	</div>
			      
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>

    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-4">
					<div class="radioButtonSubmenuUIG">

<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
<%=resourceBundleHandler.gettingValueFromBundle("Lapse Reinstatement")%></label> 
</div>
				</div>
				<div class="col-md-4">			
					<div class="radioButtonSubmenuUIG">

<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
<%=resourceBundleHandler.gettingValueFromBundle("Lapse Reinstatement Enquiry")%></label> 
</div>
		
			    </div>		        
				
			    <div class="col-md-4">
					  <div class="radioButtonSubmenuUIG">

<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%>
<%=resourceBundleHandler.gettingValueFromBundle("Reinstate Contract for Redating")%></label> 
</div>
				</div></div>
				<div class="row">	
				<div class="col-md-4">	  			
					  <div class="radioButtonSubmenuUIG">

<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%>
<%=resourceBundleHandler.gettingValueFromBundle("Reverse Contract for Redating")%></label> 
</div>		
			    </div>	
			    <div class="col-md-4">	  			
					  <div class="radioButtonSubmenuUIG">

<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%>
<%=resourceBundleHandler.gettingValueFromBundle("Convert to Proposal for Redating")%></label> 
</div>		
			    </div>
					<%
						if (sv.fuflag.compareTo("N") != 0) {
					%>
			        <div class="col-md-4">
					  <div class="radioButtonSubmenuUIG">

<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "F")%>
<%=resourceBundleHandler.gettingValueFromBundle("Contract Follow Ups")%></label>
</div>
			    </div>
					<%
						}
					%>

				</div>
					<%if(1 == sv.uwFlag){%>
					<input type='hidden' name='uworflag'id='uworflag'/>
					<input type='hidden' name='uworreason' id='uworreason' />	
					<% } %>					
		</div>
	</div>	
<%@ include file="/POLACommon2NEW.jsp"%>

<script>
	$(document).ready(function() {
		<% if(1 == sv.uwFlag){ %> 
		
			<% if(1 == sv.mainCovrDisFlag) {%>
				popupWarning();
			<% } %>
		<% } %>
	});
</script> 

<% if(1 == sv.uwFlag){ %>
<style type="text/css">
<!--
.popup-panel{
    display: block;
	z-index: 99;
    width: 50% !important;
    height: 30%;
    position: absolute !important;
    left: 24%;
    top: 30% !important;
    margin-bottom: 0 !important;
    border-color: #bce8f1 !important;
    background-color: #fff;
    border: 1px solid transparent;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
}
.popup-heading{
    font-weight: bolder;
    text-align: center;
    padding-right: 40px;
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1 ;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
}
.popup-body{
	padding: 0 !important;
    padding-left: 15px !important;
    height: auto !important;
    line-height: 1.42857143;
    color: #333;
    clear: both;
	display: table;
	margin-top:30px;
	width:100%;
}
-->
</style>
			

<script type='text/javascript'>
	function saveUWReason(){
		var flag = $("#popup-select").val();
		if("Y" == flag){
			if($("#popup-reason").val().isBlank()){
				alert("Please input the reason.");
				$("#popup-reason").css("cssText", "border-color:#f50000 !important");
				return;
			} else{
				$("#uworreason").val($("#popup-reason").val());
				$("#uworflag").val('Y');
				doAction('PFKEY0');
			}
		}
		closeWarning();
	}
	
	function popupWarning(){
   		$(".container").css("opacity","0.5");
   		$(".container").css("pointer-events","none");
   		$("#navigateButton").css("opacity","0.5");
   		$("#navigateButton").css("pointer-events","none");
   		document.getElementById("popup-button").onclick = function(){saveUWReason();};
   		$(".popup-panel").css("display","block");
   		
   		disableF5();
	}
	function closeWarning(){
   		$(".container").css("opacity","1");
   		$(".container").css("pointer-events","");
   		$("#navigateButton").css("opacity","1");
   		$("#navigateButton").css("pointer-events","");
   		$("#popup-reason").val("");
   		$("#popup-select").val("N");
   		$(".popup-panel").css("display","none");
   		$("#popup-reason").css("cssText", "border-color:#ccc !important");
   		
   		enableF5();
	}
	function clearUWReason(){
		$("#uworreason").val("");
		$("#uworflag").val("");
	}
	String.prototype.isBlank = function(){
	    var s = $.trim(this);
	    if(s == "undefined" || s == null || s == "" || s.length == 0){
	        return true;
	    }
	    return false;
	};
	function disableF5(){
		document.body.onkeydown = function(){ 
			if ( event.keyCode==116) 
			{ 	
				event.keyCode = 0; 
				event.cancelBubble = true; 
				return false; 
			} 
		};
	}
	function enableF5(){
		document.body.onkeydown = checkAllKeys;
	}
</script>
<% } %>
 <div id="popup-panel" class="popup-panel" style="display: none;width: 60% !important;height: 40%;">
   	<div class="popup-heading" style="font-weight: bolder; text-align: left;padding-right: 40px;">
       	Warning
    </div>
    	<div class="popup-body">     
			<div class="row" style="padding-left: 15px !important;width: 100%;margin-top:2px">	
				 <div>
				 	<label style="font-size: 15px !important;width: 100%margin-top:2px">
				 	The selected benefit has higher risk do you still want to accept and proceed?
				 	</label>
				 	<select id="popup-select" name="" type="list" class="" style="width:60px;float:right;margin-top:12px">
				 		<option value="N" title="No">No</option>
				 		<option value="Y" title="Yes">Yes</option>
				 	</select>
				 </div>
				 <div style="margin-top:17px">
				 	<label style="font-size: 15px !important;margin-top:17px">Reason: </label>
				 	<input name="" id="popup-reason" type="text" size="100" style="font-size: 16px !important;overflow: hidden; white-space:
				 	 nowrap; position: relative; top: 13px; left: 0px; text-align: left; min-width: 80% !important;" 
				 	 value="" maxlength="100" class="form-control"/>
				 </div>
				 <div style="float:right;margin-top:15px;">
				 	<input type="button" id="popup-button" style="width:70px" class="popup-button" value=' OK '/>
				 </div>
			</div>
		</div>
</div>
