

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6693";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6693ScreenVars sv = (S6693ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allowable");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Payment");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transactions");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.trcode01.setReverse(BaseScreenData.REVERSED);
			sv.trcode01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.trcode01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.rgpystat01.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.rgpystat01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.trcode02.setReverse(BaseScreenData.REVERSED);
			sv.trcode02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.trcode02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.rgpystat02.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.rgpystat02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.trcode03.setReverse(BaseScreenData.REVERSED);
			sv.trcode03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.trcode03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.rgpystat03.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.rgpystat03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.trcode04.setReverse(BaseScreenData.REVERSED);
			sv.trcode04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.trcode04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.rgpystat04.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.rgpystat04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.trcode05.setReverse(BaseScreenData.REVERSED);
			sv.trcode05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.trcode05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.rgpystat05.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.rgpystat05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.trcode06.setReverse(BaseScreenData.REVERSED);
			sv.trcode06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.trcode06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.rgpystat06.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.rgpystat06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.trcode07.setReverse(BaseScreenData.REVERSED);
			sv.trcode07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.trcode07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.rgpystat07.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.rgpystat07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.trcode08.setReverse(BaseScreenData.REVERSED);
			sv.trcode08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.trcode08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.rgpystat08.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.rgpystat08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.trcode09.setReverse(BaseScreenData.REVERSED);
			sv.trcode09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.trcode09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.rgpystat09.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat09.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.rgpystat09.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.trcode10.setReverse(BaseScreenData.REVERSED);
			sv.trcode10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.trcode10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.rgpystat10.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat10.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.rgpystat10.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.trcode11.setReverse(BaseScreenData.REVERSED);
			sv.trcode11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.trcode11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			sv.rgpystat11.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat11.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind31.isOn()) {
			sv.rgpystat11.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.trcode12.setReverse(BaseScreenData.REVERSED);
			sv.trcode12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.trcode12.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.rgpystat12.setReverse(BaseScreenData.REVERSED);
			sv.rgpystat12.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.rgpystat12.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">	
                <div class="col-md-1">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
			           <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div>  
			     
			     <div class="col-md-3"></div>
			     <div class="col-md-2">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
			           <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			        </div>
			     </div> 
			     
			     <div class="col-md-2"></div>
			     <div class="col-md-4">
		           <div class="form-group"> 
			           <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
			           <div class="input-group" style="max-width:100px">
			           <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:50px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			           </div> 
			        </div>
			     </div> 
			 </div>      
			  
			    <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div>
        </div>   
	&nbsp;
	 <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Allowable"))%></label>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Next Payment"))%></label>
        		</div>
        	  </div>		
       </div> 	  
       
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode01");
	optionValue = makeDropDownList( mappedItems , sv.trcode01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode01' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat01");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' >  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat01' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell'  style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div>
       
      <!-- 2 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode02");
	optionValue = makeDropDownList( mappedItems , sv.trcode02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode02' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat02");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat02' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	  </div>		
       </div> 
       
       <!-- 3 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode03");
	optionValue = makeDropDownList( mappedItems , sv.trcode03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode03' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat03");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat03' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div> 
       
        <!-- 4 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode04");
	optionValue = makeDropDownList( mappedItems , sv.trcode04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode04' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat04");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat04' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div> 
      
       <!-- 5 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode05");
	optionValue = makeDropDownList( mappedItems , sv.trcode05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode05' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat05");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat05' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	  </div>		
       </div> 
       
       <!-- 6 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode06");
	optionValue = makeDropDownList( mappedItems , sv.trcode06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode06' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat06");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat06' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	  </div>		
       </div> 
       
       <!-- 7 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode07");
	optionValue = makeDropDownList( mappedItems , sv.trcode07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode07' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat07");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat07' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div> 
       
       <!-- 8 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode08");
	optionValue = makeDropDownList( mappedItems , sv.trcode08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode08' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat08");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat08' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell"  style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div>
       
        <!-- 9 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode09");
	optionValue = makeDropDownList( mappedItems , sv.trcode09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode09' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat09");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat09' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        				
        		</div>
        	  </div>		
       </div>
       
        <!-- 10 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode10");
	optionValue = makeDropDownList( mappedItems , sv.trcode10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode10' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat10");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat10' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div>
       
       <!-- 11 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode11");
	optionValue = makeDropDownList( mappedItems , sv.trcode11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode11' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat11");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat11' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div>
       
       <!-- 12 -->
      
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode12");
	optionValue = makeDropDownList( mappedItems , sv.trcode12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='trcode12' type='list' style="width:255px;"
<% 
	if((new Byte((sv.trcode12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.trcode12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>	
        	  
        	   <div class="col-md-1"></div>
        	   
        	   <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat12");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:257px;"> 
<%
} 
%>

<select name='rgpystat12' type='list' style="width:255px;"
<% 
	if((new Byte((sv.rgpystat12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell" style="min-width:255px;"
<%
	}else if((new Byte((sv.rgpystat12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" style="min-width:255px;"
<%
	}else { 
%>
	class = 'input_cell' style="min-width:255px;"
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	  </div>		
       </div>
       
      </div>
      </div>




























<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



&nbsp;

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>


&nbsp;

	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='151'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Allowable")%>
</div>


</td><td width='151'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Next Payment")%>
</div>



</tr><tr style='height:22px;'><td width='151'>

<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode01");
	optionValue = makeDropDownList( mappedItems , sv.trcode01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat01");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat01' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode02");
	optionValue = makeDropDownList( mappedItems , sv.trcode02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat02");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode03");
	optionValue = makeDropDownList( mappedItems , sv.trcode03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat03");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat03' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode04");
	optionValue = makeDropDownList( mappedItems , sv.trcode04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat04");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat04' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode05");
	optionValue = makeDropDownList( mappedItems , sv.trcode05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat05");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat05' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode06");
	optionValue = makeDropDownList( mappedItems , sv.trcode06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat06");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat06' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode07");
	optionValue = makeDropDownList( mappedItems , sv.trcode07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat07");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat07.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat07' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat07).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode08");
	optionValue = makeDropDownList( mappedItems , sv.trcode08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat08");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat08.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat08' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat08).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode09");
	optionValue = makeDropDownList( mappedItems , sv.trcode09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat09");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat09.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat09' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat09).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode10");
	optionValue = makeDropDownList( mappedItems , sv.trcode10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat10");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat10.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat10' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat10).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode11");
	optionValue = makeDropDownList( mappedItems , sv.trcode11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode11' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat11");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat11.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat11' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat11).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



<td width='251'></td></tr><tr style='height:22px;'><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"trcode12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("trcode12");
	optionValue = makeDropDownList( mappedItems , sv.trcode12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.trcode12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.trcode12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.trcode12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='trcode12' type='list' style="width:140px;"
<% 
	if((new Byte((sv.trcode12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.trcode12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.trcode12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpystat12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("rgpystat12");
	optionValue = makeDropDownList( mappedItems , sv.rgpystat12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.rgpystat12.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.rgpystat12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.rgpystat12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='rgpystat12' type='list' style="width:140px;"
<% 
	if((new Byte((sv.rgpystat12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.rgpystat12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.rgpystat12).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Transactions")%>
</div>


</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Status")%>
</div>

</tr></table></div><br/></div>
 --%>

<%@ include file="/POLACommon2NEW.jsp"%>

