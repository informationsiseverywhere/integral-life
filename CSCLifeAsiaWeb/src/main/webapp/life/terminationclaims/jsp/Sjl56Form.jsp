<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl56";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%Sjl56ScreenVars sv = (Sjl56ScreenVars) fw.getVariables();%>	
<%{
		if (appVars.ind02.isOn()) {
			sv.confirmation.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.confirmation.setReverse(BaseScreenData.REVERSED);
			sv.confirmation.setColor(BaseScreenData.RED);
		}
}%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">				
        			<div class="form-group" style="width: 250px;">
        				<label><%=resourceBundleHandler.gettingValueFromBundle(sv.scrndesc.getFormData().toString().trim())%></label>
        				<%
							mappedItems = new HashMap<String,String>();
        					mappedItems.put("Yes", resourceBundleHandler.gettingValueFromBundle("Y"));
							mappedItems.put("No", resourceBundleHandler.gettingValueFromBundle("N"));
							optionValue = makeDropDownList(mappedItems,sv.confirmation.getFormData(),1,resourceBundleHandler);
							longValue = (String) mappedItems.get(sv.confirmation.getFormData().trim());
						%>				    	
						 <%
							if((new Byte((sv.confirmation).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0 
								|| fw.getVariables().isScreenProtected()){
						%>
						<div class='output_cell' style="width: 175px;">   
							<%=longValue==null?"":longValue%>
						</div>
						<% } else { %>
							<% if("red".equals((sv.confirmation).getColor())){ %>
							<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;">
							<% } %>
							<select name='confirmation' id='confirmation' type='list' 
							   	onFocus='doFocus(this)'
                                onHelp='return fieldHelp(confirmation)'
                                onKeyUp='return checkMaxLength(this)'
							<% if((new Byte((sv.confirmation).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0){ %>
								class="bold_cell"
							<% } else { %>
								class='input_cell'
							<% } %>
							>
							<%=optionValue%>
							</select>
							<% if("red".equals((sv.confirmation).getColor())){ %>
							</div>
							<% } %>
						<% } %>
						<%
							mappedItems = null;
							optionValue = null; 
							longValue=null;
						%> 
					</div>
				</div>
			</div>
		</div></div>	
		 <script>
			$("#continuebutton").click(function (){
				var str = <%sv.scrndesc.getFormData().toString().trim();%>
				alert(str);
				if($("#confirmation").val() === 'N' &&  (str.includes("Modify") || str.includes("Create"))
					doAction("PFKEY0");
				else if($("#confirmation").val() === 'N' && str.includes("Remove"))
					doAction("PFKEY03");
			});
		</script>
<%@ include file="/POLACommon2NEW.jsp"%>	
		