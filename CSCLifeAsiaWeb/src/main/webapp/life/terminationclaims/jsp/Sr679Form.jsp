<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String screenName = "SR679";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sr679ScreenVars sv = (Sr679ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Number of Policies in Plan ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Number ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Number     ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coverage no ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Rider no ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Lien Code   ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Stat. Fund  ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Section  ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub-Section ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Coverage       ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Single Premium      ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Cessation Date  ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium       ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Coverage RCD        ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk Cessation Date     ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Ann. Proc. Date     ");
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Rerate From Date        ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Rerate Date         ");
%>
<%
	StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Benefit Bill Date   ");
%>
<%
	StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Bonus Appl. Method ");
%>
<%
	StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Plan Code    ");
%>
<%
	StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Number of Lives  ");
%>
<%
	StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Waiver of Premium  ");
%>
<%
	StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Unit  ");
%>

<%
	{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind44.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.benBillDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.optdsc03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.optind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.optdsc01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.optind01.setReverse(BaseScreenData.REVERSED);
			sv.optind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.optind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.optdsc02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind14.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.optdsc05.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.optind05.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind13.isOn()) {
			sv.optind05.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind05.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.optind05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.optind05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.optdsc04.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.optind04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optind04.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.optind04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.optind04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.optdsc06.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
			sv.optind06.setReverse(BaseScreenData.REVERSED);
			sv.optind06.setColor(BaseScreenData.RED);
		}
		if (appVars.ind16.isOn()) {
			sv.optind06.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind06.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.optind06.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.zunit.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.zunit.setReverse(BaseScreenData.REVERSED);
			sv.zunit.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.zunit.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 END */
		//ILIFE-3399-STARTS
		if (appVars.ind43.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind44.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-ENDS
		//ILIFE-3421-STARTS
		if (appVars.ind46.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind47.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind49.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3421-ENDS
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		//ILJ-45 - START		
		/* if (appVars.ind78.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		}	 */	
		//ILJ-45 - END

	}
%>
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-left: 1px
	}
	.bold_cell {
		margin-left: 1px
	}
	.blank_cell {
		margin-left: 1px
	}
}
</style>
<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
	border-radius: 0px 0px 0px 0px;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Contract")%> <%
 	}
 %>
					</label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>

						<%
							if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cnttype.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 50px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>

						<%
							if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ctypedes.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 300px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
			
			</div>
		</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
						<%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
						<%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.premstatus.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Currency")%> <%
 	}
 %>
					</label>
					<div class="input-group" style="width: 150px;">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "cntcurr" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
						%>


						<%
							if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cntcurr.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Register")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.register.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%>
						<%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.numpols.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.numpols.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "padding-right: 50px"
						: "min-width:60px;"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>



						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

							if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 60px;padding-right: 50px">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Life Number")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Rider no")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. Fund")%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("statFund");
								longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
						%>


						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 110px;">
							<%
								if (longValue != null) {
							%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Section")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Sub-Section")%> <%
 	}
 %>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSubsect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSubsect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%> <%
 	}
 %>
					</label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 65px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td style="padding-left: 2px;">




						<%
							if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width:65px;max-width:200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>

						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>

			<div class="col-md-6"></div>

			<div class="col-md-2">
				<div class="form-group">
					<%
						if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>


					<%
						if (!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.zagelit.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<label id='zagelit' onHelp='return fieldHelp("zagelit")'> <%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%>
					</label>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
					<div class="input-group">
						<%
							if ((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

								if (!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 60px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 60px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%> <%
 	}
 %>
					</label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 65px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

						</td>
						<td style="padding-left: 2px;">




						<%
							if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width:82px;max-width:200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#risk_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Risk and Premium")%></label></a>
					</li>
					<li><a href="#processing_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Processing Details")%></label></a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="risk_tab">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Unit")%></label>
									<div>
										<%
											if (!((sv.zunit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.zunit.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.zunit.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 100px">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Plan Code")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.benpln).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.benpln.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.benpln.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.benpln.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 140px">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("mortcls");
											longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());

											if (!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.mortcls.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.mortcls.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:100px;" : "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.liencd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.liencd.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.liencd.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:100px;" : "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Life Coverage")%>
									</label>
									<%
										}
									%>
									<table>
									<tr>
									<td>
										<%
											if ((new Byte((sv.sumin).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
												//ILIFE-1474 STARTS
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.sumin,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												//ILIFE-1474 ENDS
												if (!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue(formatValue);
													} else {
														formatValue = formatValue(longValue);
													}
												}

												if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell" style="text-align: right;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell" style="width: 100px;">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
												formatValue = null;
										%>

										<%
											}
										%>

										</td>
										<td>




										<%
											if ((new Byte((sv.frqdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.frqdesc.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.frqdesc.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div style="margin-left: 1px;"
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</td>
									</tr>
									</table>
								</div>
							</div>
							<div class="col-md-3 col-md-offset-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.singlePremium).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.singlePremium,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

												if (!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue(formatValue);
													} else {
														formatValue = formatValue(longValue);
													}
												}

												if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell"
											style="width: 140px; text-align: right; padding-right: 4px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell" style="width: 100px;">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
												formatValue = null;
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Number of Lives")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.livesno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.livesno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.livesno.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.livesno.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 50px">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<!-- ILJ-45 Starts -->
			               			<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
			               			<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
				        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
				        			<%} else { %>
				        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
				        			<%} %>
				        			<!-- ILJ-45 End -->
									<table>
									<tr>
									<td>
										<%
											if (((BaseScreenData) sv.riskCessAge) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.riskCessAge, (sv.riskCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.riskCessAge) instanceof DecimalData) {
										%>
										<%
											if (sv.riskCessAge.equals(0)) {
										%>
										<%=smartHF.getHTMLVar(fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 40px; \' ")%>
										<%
											} else {
										%>
										<%=smartHF.getHTMLVar(fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
										</td>
										<td style="padding-left:1px;">

										<%
											if (((BaseScreenData) sv.riskCessTerm) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.riskCessTerm, (sv.riskCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.riskCessTerm) instanceof DecimalData) {
										%>
										<%
											if (sv.riskCessTerm.equals(0)) {
										%>
										<%=smartHF.getHTMLVar(fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
										<%
											} else {
										%>
										<%=smartHF.getHTMLVar(fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</td>
									</tr>
									</table>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<!-- ILJ-45 Starts -->

									<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
									<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
										<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation Date")%></label>

									<%} else { %>
										<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cessation date"))%></label>
									<%} %>
									<!-- ILJ-45 End -->
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.riskCessDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.riskCessDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 140px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<%
										//ILJ-386
										if (sv.cntEnqScreenflag.compareTo("Y") == 0) {
									%>
										<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
									<%
										} else if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
				        			<%} else { %>
				        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage RCD"))%></label>
				        			<%} %>
				        			<!-- ILJ-45 End -->	
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.crrcdDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.crrcdDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 140px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Waiver of Premium")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.waiverprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.waiverprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.waiverprem.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.waiverprem.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 50px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3" sty>
								<div class="form-group">
									<%
										if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%>
									</label>
									<%
										}
									%>
									<table>
									<tr>
									<td>
										<%
											if (((BaseScreenData) sv.premCessAge) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.premCessAge, (sv.premCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.premCessAge) instanceof DecimalData) {
										%>
										<%
											if (sv.premCessAge.equals(0)) {
										%>
										<%=smartHF.getHTMLVar(fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 40px; \' ")%>
										<%
											} else {
										%>
										<%=smartHF.getHTMLVar(fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
										</td>
										<td style="padding-left:1px;">
										

										<%
											if (((BaseScreenData) sv.premCessTerm) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.premCessTerm, (sv.premCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.premCessTerm) instanceof DecimalData) {
										%>
										<%
											if (sv.premCessTerm.equals(0)) {
										%>
										<%=smartHF.getHTMLVar(fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
										<%
											} else {
										%>
										<%=smartHF.getHTMLVar(fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
								</td>
								</tr>
								</table>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Cessation Date")%></label>
									<div>
										<%
											if (!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.premcessDisp.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.premcessDisp.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 140px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<%
									if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte( //ILIFE-3517
											BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "prmbasis" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("prmbasis");
												optionValue = makeDropDownList(mappedItems, sv.prmbasis.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.prmbasis.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.prmbasis.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.prmbasis.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
							: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
													formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
							<div class="col-md-3">
								<%
									if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "dialdownoption" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("dialdownoption");
												optionValue = makeDropDownList(mappedItems, sv.dialdownoption.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.dialdownoption.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.dialdownoption.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.dialdownoption.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
							: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
													formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<%
									if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte( //ILIFE-3517
											BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "waitperiod" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("waitperiod");
												optionValue = makeDropDownList(mappedItems, sv.waitperiod.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.waitperiod.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.waitperiod.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.waitperiod.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
							: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
													formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
							<div class="col-md-3">
								<%
									if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte( //ILIFE-3517
											BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "bentrm" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("bentrm");
												optionValue = makeDropDownList(mappedItems, sv.bentrm.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.bentrm.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.bentrm.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.bentrm.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
							: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
													formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
							<div class="col-md-3">
								<%
									if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte( //ILIFE-3517
											BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
									<div>
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "poltyp" }, sv, "E", baseModel);
												mappedItems = (Map) fieldItem.get("poltyp");
												optionValue = makeDropDownList(mappedItems, sv.poltyp.getFormData(), 2, resourceBundleHandler);
												longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());
										%>

										<%
											if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.poltyp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.poltyp.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													} else {

														if (longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue((sv.poltyp.getFormData()).toString());
														} else {
															formatValue = formatValue(longValue);
														}

													}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
							: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
													formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
						</div>

						<%
							if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<!-- ILIFE-3399 -->
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
										%>
										<%
											if (sv.adjustageamt.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.adjustageamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.adjustageamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
										%>
										<%
											if (sv.rateadj.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<%
									if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
										%>
										<%
											if (sv.fltmort.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.loadper) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
										%>
										<%
											if (sv.loadper.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>
						<%
							}
						%>
						<div class="row">
							<%
								if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%><!-- ILIFE-3399 -->
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.premadj) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.premadj, (sv.premadj.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
										%>
										<%
											if (sv.premadj.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<%
								}
							%>
							<div class="col-md-3">
								<div class="form-group">
										<%
											if ((new Byte((sv.zdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.zdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.zdesc.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.zdesc.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<!-- 	ILIFE-1062	Start		 -->

										<label> <%=resourceBundleHandler.gettingValueFromBundle(sv.zdesc.getFormData().toString())%>
										</label>

										<!-- 	ILIFE-1062 END -->
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>


										<%
											if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.zlinstprem,
														COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

												if (!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue(formatValue);
													} else {
														formatValue = formatValue(longValue);
													}
												}

												if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell"
											style="width: 140px; text-align: right; padding-right: 4px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell" style="width: 82px;">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
												formatValue = null;
										%>

										<%
											}
										%>
								</div>
							</div>
							<div class="col-md-3">
								<%
									if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="form-group">
									<%
										if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
										%>
										<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem, (sv.zbinstprem.getLength() + 1), null)
							.replace("absolute", "relative")%>
										<%
											} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
										%>
										<%
											if (sv.zbinstprem.equals(0)) {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
										<%
											} else {
										%>
										<%=smartHF
								.getHTMLVar(fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
										<%
											}
										%>
										<%
											} else {
										%>
										<%
											}
										%>
									</div>
								</div>
								<%
									}
								%>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.instPrem,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

												if (!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue(formatValue);
													} else {
														formatValue = formatValue(longValue);
													}
												}

												if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell"
											style="min-width: 145px; text-align: right;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>

										<div class="blank_cell" style="min-width: 145px;">&nbsp;</div>

										<%
											}
										%>
										<%
											longValue = null;
												formatValue = null;
										%>

										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<% //ILJ-387
										if (sv.cntEnqScreenflag.compareTo("N") == 0){ 
									%> 
									<label  style="white-space: nowrap;"> <!-- ILIFE-1062 Start--> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%>
										<!-- ILIFE-1062 End-->
									</label>
									<%
										}
									%>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>
										<% //ILJ-387
										if (sv.cntEnqScreenflag.compareTo("N") == 0){ 
										%> 
										<%
											qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
												//ILIFE 1487 STARTS
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
												formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt,
														COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
												//ILIFE 1487 ENDS
												if (!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue(formatValue);
													} else {
														formatValue = formatValue(longValue);
													}
												}
												if (!formatValue.trim().equalsIgnoreCase("")) {
										%>
										<div class="output_cell"
											style="min-width: 145px; text-align: right;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											} else {
										%>
										<div class="blank_cell" style="min-width: 145px;">&nbsp;</div>
										<%
											}
										%>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
										<%
										}
									%>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- end risk tab  -->

					<div class="tab-pane fade" id="processing_tab">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label> <%=resourceBundleHandler.gettingValueFromBundle("Ann. Proc. Date")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.annivProcDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.annivProcDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.annivProcDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Rerate Date")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.rerateDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.rerateDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rerateDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rerateDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Rerate From Date")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.rerateFromDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.rerateFromDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rerateFromDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rerateFromDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Benefit Bill Date")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.benBillDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.benBillDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.benBillDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.benBillDateDisp.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<%
										if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl. Method")%>
									</label>
									<%
										}
									%>
									<div>
										<%
											if ((new Byte((sv.bappmeth).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										%>


										<%
											if (!((sv.bappmeth.getFormData()).toString()).trim().equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.bappmeth.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.bappmeth.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:80px;" : "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
												formatValue = null;
										%>
										<%
											}
										%>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<BODY >
	<div class="sidearea">
		<!-- <div class="logoarea" style="height: 50px;">
			<a class="navbar-brand" href="#">Integral Admin</a>
		</div> -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='optind05' id='optind05' type='hidden'
									value="<%=sv.optind05.getFormData()%>"> <!-- text --> <%
 	if (sv.optind05.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.optind05.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc05.toString())%>
										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind05"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc05.toString())%>
											<%
												}
											%> <!-- icon --> <%
 	if (sv.optind05.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}
 	if (sv.optind05.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>
									</a></li>
							</ul>
					</span></li>
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='optind06' id='optind06' type='hidden'
									value="<%=sv.optind06.getFormData()%>"> <!-- text --> <%
 	if (sv.optind06.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.optind06.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc06.toString())%>
										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind06"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc06.toString())%>
											<%
												}
											%> <!-- icon --> <%
 	if (sv.optind06.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}
 	if (sv.optind06.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>
									</a></li>
							</ul>
					</span></li>
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='optind04' id='optind04' type='hidden'
									value="<%=sv.optind04.getFormData()%>"> <!-- text --> <%
 	if (sv.optind04.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.optind04.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc04.toString())%>
										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind04"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc04.toString())%>
											<%
												}
											%> <!-- icon --> <%
 	if (sv.optind04.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}
 	if (sv.optind04.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>
									</a></li>
							</ul>
					</span></li>
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='optind02' id='optind02' type='hidden'
									value="<%=sv.optind02.getFormData()%>"> <!-- text --> <%
 	if (sv.optind02.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.optind02.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc02.toString())%>
										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc02.toString())%>
											<%
												}
											%> <!-- icon --> <%
 	if (sv.optind02.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}
 	if (sv.optind02.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>
									</a></li>
							</ul>
					</span></li>
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<%
										if (sv.optind08.getInvisible() != BaseScreenData.INVISIBLE) {
									%> <input name='optind08' id='optind08' type='hidden'
									value="<%=sv.optind08.getFormData()%>"> <!-- text --> <%
 	if (sv.optind08.getInvisible() == BaseScreenData.INVISIBLE
 				|| sv.optind08.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc08.toString())%>
										<%
											} else {
										%> <a href="javascript:;"
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind08"))'
										class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc08.toString())%>
											<%
												}
											%> <!-- icon --> <%
 	if (sv.optind08.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	}
 		if (sv.optind08.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %>
									</a> <%
 	}
 %>
								</li>
							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>

<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>&nbsp; &nbsp;<br /> <%
 	if ((new Byte((sv.crtabdesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>


			</td>
		</tr>
	</table>
</div>
