<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5026";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5026ScreenVars sv = (S5026ScreenVars) fw.getVariables();%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------- Surrender Details ----------------------------------");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff. Date ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy loans ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net S/V Debt ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy debt  ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Deposit ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Other Adjustments ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Imposed ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adjustment reason ");%>
	

<%{
		if (appVars.ind15.isOn()) {
			sv.clamant.setReverse(BaseScreenData.REVERSED);
			sv.clamant.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.clamant.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.resndesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind06.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.currcd.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind08.isOn()) {
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (appVars.ind12.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind31.isOn()) {
			sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
				}
				
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind32.isOn()) {
			sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
				}
				if(appVars.ind69.isOn()){
					sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
					sv.reserveUnitsDate.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind57.isOn()) {
						sv.bankacckey.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind62.isOn()) {
						sv.crdtcrd.setInvisibility(BaseScreenData.INVISIBLE);
				}
				 if (appVars.ind56.isOn()) {
						sv.bankacckey.setEnabled(BaseScreenData.DISABLED);
					}
				 if (appVars.ind61.isOn()) {
						sv.crdtcrd.setEnabled(BaseScreenData.DISABLED);
					}
				 if (appVars.ind55.isOn()) {
						sv.bankacckey.setReverse(BaseScreenData.REVERSED);
						sv.bankacckey.setColor(BaseScreenData.RED);
					}
				 if (appVars.ind60.isOn()) {
						sv.crdtcrd.setReverse(BaseScreenData.REVERSED);
						sv.crdtcrd.setColor(BaseScreenData.RED);
					}
				 if (!appVars.ind55.isOn()) {
						sv.bankacckey.setHighLight(BaseScreenData.BOLD);
					}
				 if (!appVars.ind60.isOn()) {
						sv.crdtcrd.setHighLight(BaseScreenData.BOLD);
					}
				 
				 if (appVars.ind80.isOn()) {
						sv.reqntype.setReverse(BaseScreenData.REVERSED);
						sv.reqntype.setColor(BaseScreenData.RED);
					}
				 if (appVars.ind18.isOn()) {
						sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
					}	
				 if (appVars.ind19.isOn()) {
						sv.unexpiredprm.setInvisibility(BaseScreenData.INVISIBLE);
					}	
				 if (appVars.ind20.isOn()) {
						sv.suspenseamt.setInvisibility(BaseScreenData.INVISIBLE);
					}	
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Suffix No ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------- Coverage/Rider Details------------------------------");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lif Cov Rid Fund Typ Descriptn.    Ccy    Estimated Value        Actual Value ");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind10.isOn()) {
			sv.ptdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.ptdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ptdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.btdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.btdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.btdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.planSuffix.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 70px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
					</td><td>
				  		
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
				</td><td>
				  		
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 235px;margin-left: 1px;">
										<!-- ILIFE-6336 -->
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
		       		<div class="input-group">
		       		<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
	
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
		       		<div class="input-group">
		       		<%					
							if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
		       			<%					
							if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>

		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
		       		<table><tr><td>
		       		<%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td>
						
					  		
							<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 150px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td>
						<%					
						if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 150px;margin-left: 2px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;" >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td>
					
				  		
						<%					
						if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
		       		
		       		<%					
						if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
		       		
		       		<%					
						if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		
		       		</div>
		       	</div>
		       	
		       		
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix No")%></label>
		       		
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
							
							if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="width: 50px;">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 50px;">&nbsp;</div>
						
						<% 
							} 
						%>
		       		
		       		</div>
		       	</div>
		    </div>
		    
		   		 <%
					/* This block of jsp code is to calculate the variable width of the table at runtime.*/
					int[] tblColumnWidth = new int[9];
					int totalTblWidth = 0;
					int calculatedValue =0;
					
											if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.life.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
								} else {		
									calculatedValue = (sv.life.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[0]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.coverage.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
								} else {		
									calculatedValue = (sv.coverage.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[1]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.rider.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
								} else {		
									calculatedValue = (sv.rider.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[2]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fund.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
								} else {		
									calculatedValue = (sv.fund.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[3]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.fieldType.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
								} else {		
									calculatedValue = (sv.fieldType.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[4]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.shortds.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
								} else {		
									calculatedValue = (sv.shortds.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[5]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.cnstcur.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
								} else {		
									calculatedValue = (sv.cnstcur.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[6]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.estMatValue.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
								} else {		
									calculatedValue = (sv.estMatValue.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[7]= calculatedValue;
								
											if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.actvalue.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
								} else {		
									calculatedValue = (sv.actvalue.getFormData()).length()*12;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[8]= calculatedValue;
								%>
							<%
							GeneralTable sfl = fw.getTable("s5026screensfl");
							int height;
							if(sfl.count()*27 > 210) {
							height = 210 ;
							} else {
							height = sfl.count()*27;
							}
							
							%>
							
				<div class="row">
		        	<div class="col-md-3">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage / Rider Details")%></label>
			       		</div>
			       	</div>
			    </div>
		    
		    
				    <div class="row">		
				 		<div class="col-md-12">
				 		<div class="form-group"> 	
				           <div class="table-responsive">
				    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5026' width="100%">	
					    	 	<thead>
					    	 	<tr class='info'>									
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
		         				<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></th>
								</tr>	
					         	</thead>
							      <tbody>
							      <%

	
	
									S5026screensfl
									.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S5026screensfl
									.hasMoreScreenRows(sfl)) {
									
								%>
								
									<tr  id='<%="tablerow"+count%>' >
														    									<td style="min-width:<%=tblColumnWidth[0 ]%>px;" align="left">														
																								
																	
																			
														<%= sv.life.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.coverage.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[2 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.rider.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[3 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.fund.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.fieldType.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[5 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.shortds.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[6 ]%>px;" align="left">									
																								
																	
																			
														<%= sv.cnstcur.getFormData()%>
														
																						 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[7 ]%>px;" align="right">									
																								
																															
													<%	
														sm = sfl.getCurrentScreenRow();
														qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());						
														//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
													%>
													
																		
														<%
															formatValue = smartHF.getPicFormatted(qpsf,sv.estMatValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
															if(!sv.
															estMatValue
															.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																formatValue = formatValue( formatValue );
															}
														%>
														<%= formatValue%>
														<%
																longValue = null;
																formatValue = null;
														%>
													 			 		
											 		
											    				 
												
																	</td>
												    									<td  style="min-width:<%=tblColumnWidth[8 ]%>px;" align="right">									
																								
																															
													<%	
														sm = sfl.getCurrentScreenRow();
														qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());						
														//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
													%>
													
																		
														<%
															if(sv.actvalue.equals(0)) {
																formatValue = "0.0";
															} else {
															formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
															if(!sv.
															actvalue
															.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																formatValue = formatValue( formatValue );
															}
															}
														%>
														<%= formatValue%>
														<%
																longValue = null;
																formatValue = null;
														%>
													 			 		
											 		
											    				 
												
																	</td>
												
										
										
									</tr>
								
								
									<%
									count = count + 1;
									S5026screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
	
							      </tbody>
							</table>
							</div>
						</div>
					</div>
				</div>
				    
				    
		 <div class="row">
		    <div class="col-md-12">
             <ul class="nav nav-tabs">
                 <li class="active">
                    <a href="#Surrender_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Details")%></label></a>
                 </li>
                 <% if (sv.susur002flag.compareTo("N") != 0) { %>
                 <li>
                     <a href="#Payout_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Payout Details")%></label></a>
                 </li>
                 <%}%>
                 </div></div></ul>
                 
                 
                 
        <div class="tab-content">
            <div class="tab-pane fade in active" id="Surrender_tab">
          
		
		
		    <div class="row">
		    
			    <div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Eff. Date")%></label>
			       		<div class="input-group">
			       				<%					
								if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
			       		</div>
			       		</div>
			       	</div>
			       	
			       		
		       	
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       		<div class="input-group">
		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("currcd");
							optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),1,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
						%>
						
						<% 
							if((new Byte((sv.currcd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
							
						<% if("red".equals((sv.currcd).getColor())){
						%>
						<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
						<%
						} 
						%>
						
						<select name='currcd' type='list' style="width:145px;"
						<% 
							if((new Byte((sv.currcd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							disabled
							class="output_cell"
						<%
							}else if((new Byte((sv.currcd).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						<%
							}else { 
						%>
							class = 'input_cell' 
						<%
							} 
						%>
						>
						<%=optionValue%>
						</select>
						<% if("red".equals((sv.currcd).getColor())){
						%>
						</div>
						<%
						} 
						%>
						
						<%
						} 
						%>
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<% if (sv.susur002flag.compareTo("N") != 0) { %>
		       		<div class="col-md-4">
		       		
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Current Suspense")%></label>
					<div class="input-group">
						<%
						
							qpsf = fw.getFieldXMLDef((sv.sacscurbal).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.sacscurbal,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.sacscurbal.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
				
			</div>
		      <%
							}
						%> 	
		       	
		    </div>
		    
		    
		    <div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>


					<%
						longValue = sv.reserveUnitsInd.getFormData();
						if ("".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Select");
						} else if ("Y".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Y");
						} else if ("N".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("N");
						}
						if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>

						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.reserveUnitsInd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
						<%
							}
						%>
						<select name='reserveUnitsInd' type='list' style="width: 100px;"
							<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>

							<option value=""
								<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
							<option value="Y"
								<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
							<option value="N"
								<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
						</select>
						<%
							if ("red".equals((sv.reserveUnitsInd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
						
						longValue = null;
					%>
				
				</div>
			</div>

<!-- 			<div class="col-md-1"></div> -->
			<div class="col-md-4">
				<div class="form-group" style="max-width: 150px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>

					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> --%>
					<%--  <div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
						data-link-format="dd/mm/yyyy" style="min-width:162px;">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
							
					</div> --%>
					
					<% 
								if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
							%>
								<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
							<% }else {%>
			                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>	
			                <%} %>	
					
				</div>
			</div>
			<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Net  S/V  Debt")%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.netOfSvDebt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							if(sv.netOfSvDebt.equals(0)) {
								formatValue = "0.0";
							} else {
							formatValue = smartHF.getPicFormatted(qpsf,sv.netOfSvDebt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.netOfSvDebt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		</div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Deposit")%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.zrcshamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 
							<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		      	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy loans")%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.policyloan,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
							<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div> 
		       	
		       		
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Other Adjustments")%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		
							<%	
					qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
					//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
					valueThis=smartHF.getPicFormatted(qpsf,sv.otheradjst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
						%>
					
					<input name='otheradjst' 
					type='text'
					<%if((sv.otheradjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						value='<%=valueThis%>'
								 <%	 
						 if(valueThis!=null&& valueThis.trim().length()>0) {%>
						 title='<%=valueThis%>'
						 <%}%>
					
					size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.otheradjst.getLength(), sv.otheradjst.getScale(),3)%>'
					maxLength='<%= sv.otheradjst.getLength()%>' 
					onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
					
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
						decimal='<%=qpsf.getDecimals()%>' 
						onPaste='return doPasteNumber(event,true);'
						onBlur='return doBlurNumberNew(event,true);'
					
					<% 
						if((new Byte((sv.otheradjst).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.otheradjst).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.otheradjst).getColor()== null  ? 
								"input_cell" :  (sv.otheradjst).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy debt")%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.tdbtamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.tdbtamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	 <% if (sv.susur002flag.compareTo("N") != 0) { %>
		       	<div class="col-md-4">
		      
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium Paid")%></label>
		       		<table><tr><td style="min-width:80px"> 
		       		<%=smartHF.getHTMLVarExt(fw, sv.sacscurbal1 )%>
		       		</td></tr></table>
		       		</div>
		       		</div>
		       			<% 
							} 
						%>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Imposed")%></label>
		       
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 260px;" >	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 100px;" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		
		       	</div>
		       	<%
		       	if ((new Byte((sv.unexpiredprm).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		       	%>
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Unexpired Premium")%></label>
		       
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.unexpiredprm).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unexpiredprm,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.unexpiredprm.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 100px; text-align: right " >	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 100px;" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		
		       	</div>
		       	<%} %>
		       	
		       	
		       	
		       	
		</div>
		       	
		    
		    
		     <div class="row">
		     	<%
				if ((new Byte((sv.suspenseamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
		     <div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%></label>
		       
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.suspenseamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.suspenseamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.suspenseamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 100px; text-align: right" >	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 100px;" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		
		       	</div>
		       	<%} %>
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment reason")%></label>
		       		
		       		
		       		<table><tr>
		       		<td>

		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("reasoncd");
							optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
							if(longValue == null) {
								longValue = "&nbsp;&nbsp;";
							} else {
								longValue = formatValue(longValue);
							}
						%>
						
						<% 
							if((new Byte((sv.reasoncd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>
						
							<% }else {%>
							
						<% if("red".equals((sv.reasoncd).getColor())){
						%>
						<div style="border:1px; border-style: solid; border-color: #B55050;  min-width:140px;"> 
						<%
						} 
						%>
						<!--ILIFE2665 starts/ends  -->
						<select name='reasoncd' type='list' style="width:140px; "
						<% 
							if((new Byte((sv.reasoncd).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							disabled
							class="output_cell"
						<%
							}else if((new Byte((sv.reasoncd).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						<%
							}else { 
						%>
							class = 'input_cell' 
						<%
							} 
						%>
						onchange="change()"
						>
						<%=optionValue%>
						</select>
						<% if("red".equals((sv.reasoncd).getColor())){
						%>
						</div>
						<%
						} 
						%>
						
						<%
						} 
						%>
						
						
								<%
								longValue = null;
								formatValue = null;
								%>
						
						</td>
						
						<td style="width: 250px;padding-left: 1px;">
						<input name='resndesc' 
						type='text'
						
						<%
						
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasoncd");
								optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
								formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
								
						%>
						<%if(formatValue==null) {
							formatValue="";
						}
						 %>
						 
						 <% String str=(sv.resndesc.getFormData()).toString().trim(); %>
									<% if(str.equals("") || str==null) {
										str=formatValue;
									}
									
								%>
						 
							value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>
						
						size='50'
						maxLength='50' 
						
						
						<% 
							if((new Byte((sv.resndesc).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.resndesc).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.resndesc).getColor()== null  ? 
									"output_cell" :  (sv.resndesc).getColor().equals("red") ? 
									"output_cell red reverse" : "output_cell" %>'
						 
						<%
							} 
						%>
						>
						
						
								<%
								longValue = null;
								formatValue = null;
								%>


						
						
							
						  		<!-- <%					
								if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								
								 -->
							</td>	
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
		       		 <table><tr><td >
						    		

						<%if ((new Byte((sv.estimateTotalValue).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%	
									qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									
									if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell" style="max-width:100px; text-align: right" style="min-width:80px;"  >	
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" style="max-width:100px;" style="min-width:80px"> &nbsp; </div>
								
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
							
						 <%}%>
	

					</td>
					<td >



							<%if ((new Byte((sv.clamant).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%	
										qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										if(sv.clamant.equals(0)) {
											formatValue = "0.0";
										} else {
										formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										
										if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" style="max-width:180px;margin-left: 1px;">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="width:100px;margin-left: 1px;"> &nbsp; </div>
									
									<% 
										} 
									%>
									<%
									longValue = null;
									formatValue = null;
									%>
								
							 <%}%>
	

			 </td></tr></table>
   		</div>
		    
		</div></div>
		 
		
		
		
		</div>
		
		 
	
		 <div class="tab-pane" id="Payout_tab"> 
		
		    <div class="row">
			<div class="col-md-4" >
				<div class="form-group">
<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
<table>
<tr><td>

<%
                                         if ((new Byte((sv.payrnum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group" style="width: 80px;">
                                                <%=smartHF.getHTMLVarExt(fw, sv.payrnum)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group" style="width: 120px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.payrnum)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('payrnum')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>	
                                  </td><td style="padding-left:1px;">
                                  
			<%
				if( !(sv.payorname.getFormData().toString()).trim().equalsIgnoreCase("") ) {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString());
					} else {
						formatValue = formatValue( longValue);
					}
				} else {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.payorname.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
				}
			%>
			
			<div class='<%=((formatValue==null)||("".equals(formatValue.trim())))?"blank_cell1":"output_cell1" %>' style='width:100px;'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			<%
				longValue = null;
				formatValue = null;
			%>
                                  
                                  
                                  
                                  
                                  </td></tr></table>
</div></div>


 <div class="col-md-4"> 
				    <div class="form-group">
				    	<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
				    	 <div class="input-group">
				    	<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("reqntype");
							optionValue = makeDropDownList( mappedItems , sv.reqntype.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());
						
						if(longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
						%>
						
						<% 
							if((new Byte((sv.reqntype).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
						%>  
						<div class='output_cell'> 
						   <%=XSSFilter.escapeHtml(longValue)%>
						</div>
						
						<%
						longValue = null;
						%>`
						<% }else {%>
							<%=smartHF.getDropDownExt(sv.reqntype, fw, longValue, "reqntype", optionValue, 0) %>
						<%
							} 
						%>  
				    </div></div>
				 </div>

</div>

					<div class="row">
					
			 <%	 if (appVars.ind57.isOn() && appVars.ind62.isOn()) { %>		
			 <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

		
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 100px;">
								
							</div>	
							
						
						
				<!-- 	 <div class="input-group" style="min-width:100px">
					 
					 </div> -->
                                  
</div></div>
					
					 <%
                                         }
                                  %>
					
					
 <%	 if (!(appVars.ind57.isOn())) { %>
              <div class="col-md-4">
                <div class="form-group">
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

	<table><tr><td class="form-group">	  
                                  <div class="input-group" style="width: 210px;">
                                  
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankacckey)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                 
								 
					  </td></tr></table>	

	</div></div>  <%} %>
	
	 <%	 if (!(appVars.ind62.isOn())) { %>          
	
	 <div class="col-md-4">
		 <div class="form-group">
			 <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>
					<table><tr><td>	 
                                  <div class="input-group" style="width: 210px;">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.crdtcrd)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info" type="button"
                                                       onClick="doFocus(document.getElementById('crdtcrd')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                
					  </td></tr></table>	
			 </div> 
		</div>
					    		 <%} %>
	
		<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Code"))%></label>
					<table>
					<tr>
					<td style="min-width: 100px;"> <%=smartHF.getHTMLVarExt(fw, sv.bankkey)%>
				<%-- 	<%=smartHF.getHTMLVarExt(fw, sv.bsortcde)%> --%>
                     </td>
                     </tr>
					</table>
					</div></div>
                     
                     
                     <div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Name"))%></label>
					<table>
					<tr> <td style="min-width:100px;">
					
					<%=smartHF.getHTMLVarExt(fw, sv.bankdesc)%>
						<%-- <%					
				if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell1" : "output_cell" %>' style="min-width: 100px;">
						<%=formatValue%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%> --%>
					</td>
					</tr>
					
					
					</table>
					
					
					</div></div>
	
	
	
	</div>
	
	
	</div>
 
	</div>
	 </div>
</div>

<script type="text/javascript">

function change()
{
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}
</script>



	<script>
	$(document).ready(function() {
		$('#dataTables-s5026').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false
	        
		});
	});
</script> 
	
<script type="text/javascript">
	window.addEventListener("load",function() {
		loadPopup("<%=sv.msgPopup.toString().trim()%>", false, 0);
		document.getElementById('confirmModal-content').setAttribute("style", "width:50% !important;height: 20% !important;top:30% !important;");
		document.getElementById('divConfirmation').setAttribute("style", "height: 0 !important;");
        document.getElementById('btnOk').value = '<%=resourceBundleHandler.gettingValueFromBundle("Y")%>';
        document.getElementById('btnCancel').value = '<%=resourceBundleHandler.gettingValueFromBundle("N")%>';
	});
	
	function doConfirm(str) {
		if (str == 'Yes') {
			document.getElementById('msgresult').value = 'Y';
		} else {
			document.getElementById('msgresult').value = 'N';
		}
		document.getElementById('confirmModal').style.display = "none";
		if (parent.frames["mainForm"].document.form1.action_key.value.toUpperCase() == "PFKEY0"){
			doAction("PFKEY0");
		} else if(parent.frames["mainForm"].document.form1.action_key.value.toUpperCase() =="PFKEY05"){
			doAction("PFKEY05");
		}
	}
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
