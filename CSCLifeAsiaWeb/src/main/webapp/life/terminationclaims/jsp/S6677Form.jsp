

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
    String screenName = "S6677";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
    S6677ScreenVars sv = (S6677ScreenVars) fw.getVariables();
%>
<%
    StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Contract No    ");
%>
<%
    StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cmpnt  ");
%>
<%
    StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Life           ");
%>
<%
    StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Owner          ");
%>
<%
    StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "RCD            ");
%>
<%
    StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Risk/Prem Status   ");
%>
<%
    StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Paid-to-date   ");
%>
<%
    StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Billed-to-date     ");
%>
<%
    StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "CCY ");
%>
<%
    StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "-----------------------------------------------------------------------------");
%>
<%
    StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Claim Seq No ");
%>
<%
    StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Claim Receive Date ");
%>
<%
    StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Incur Date ");
%>
<%
    StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Claim Type        ");
%>
<%
    StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Claim Status        ");
%>
<%
    StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Reason            ");
%>
<%
    StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Evidence          ");
%>
<%
    StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sum");
%>
<%
    StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Assured ");
%>
<%
    StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
    StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
    StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Payment Method    ");
%>
<%
    StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Frequency           ");
%>
<%
    StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Payee             ");
%>
<%
    StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "% of Sum Assured  ");
%>
<%
    StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Destination Key     ");
%>
<%
    StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Claim Amount  ");
%>
<%-- generatedText for Claim Amount --%>
<%
    StringData generatedText43 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Amount  ");
%>
<%-- generatedText for Actual Amount --%>
<%
    StringData generatedText45 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Net Claim Amount  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
    StringData generatedText44 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Reason  ");
%>
<%-- generatedText for Adjustment Reason --%>
<%
    StringData generatedText46 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Adjustment Reason  Description  ");
%>
<%
    StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Total Paid to Date  ");
%>
<%
    StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Payment Currency  ");
%>
<%
    StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Approval Date       ");
%>
<%
    StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Effective Date    ");
%>
<%
    StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Review Date         ");
%>
<%
    StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "First Payment Date");
%>
<%
    StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Last Paid Date      ");
%>
<%
    StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Next Payment Date ");
%>
<%
    StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Anniversary Date    ");
%>
<%
    StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Final Payment Date");
%>
<%
    StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Cancellation Date   ");
%>
<%
    StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Follow Ups  ");
%>
<%
    StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Annuity Details ");
%>
<%
    StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Bank Account ");
%>
<%
    StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
            "Accident Claim ");
%>

<%
    {
        if (appVars.ind02.isOn()) {
            sv.cltype.setReverse(BaseScreenData.REVERSED);
            sv.cltype.setColor(BaseScreenData.RED);
        }
        if (appVars.ind32.isOn()) {
            sv.cltype.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind02.isOn()) {
            sv.cltype.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind03.isOn()) {
            sv.claimevd.setReverse(BaseScreenData.REVERSED);
            sv.claimevd.setColor(BaseScreenData.RED);
        }
        if (appVars.ind33.isOn()) {
            sv.claimevd.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind03.isOn()) {
            sv.claimevd.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind04.isOn()) {
            sv.rgpymop.setReverse(BaseScreenData.REVERSED);
            sv.rgpymop.setColor(BaseScreenData.RED);
        }
        if (appVars.ind34.isOn()) {
            sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind04.isOn()) {
            sv.rgpymop.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind05.isOn()) {
            sv.regpayfreq.setReverse(BaseScreenData.REVERSED);
            sv.regpayfreq.setColor(BaseScreenData.RED);
        }
        if (appVars.ind35.isOn()) {
            sv.regpayfreq.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind05.isOn()) {
            sv.regpayfreq.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind01.isOn()) {
            sv.payclt.setReverse(BaseScreenData.REVERSED);
            sv.payclt.setColor(BaseScreenData.RED);
        }
        if (appVars.ind36.isOn()) {
            sv.payclt.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind01.isOn()) {
            sv.payclt.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind11.isOn()) {
            sv.destkey.setReverse(BaseScreenData.REVERSED);
            sv.destkey.setColor(BaseScreenData.RED);
        }
        if (appVars.ind37.isOn()) {
            sv.destkey.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind11.isOn()) {
            sv.destkey.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind39.isOn()) {
            sv.claimcur.setEnabled(BaseScreenData.DISABLED);
        }
        if (appVars.ind75.isOn()) {
            sv.claimcur.setReverse(BaseScreenData.REVERSED);
            sv.claimcur.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind75.isOn()) {
            sv.claimcur.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind44.isOn()) {
            sv.crtdateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (appVars.ind13.isOn()) {
            sv.crtdateDisp.setReverse(BaseScreenData.REVERSED);
            sv.crtdateDisp.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind13.isOn()) {
            sv.crtdateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind16.isOn()) {
            sv.revdteDisp.setReverse(BaseScreenData.REVERSED);
            sv.revdteDisp.setColor(BaseScreenData.RED);
        }
        if (appVars.ind41.isOn()) {
            sv.revdteDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind16.isOn()) {
            sv.revdteDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind23.isOn()) {
            sv.firstPaydateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (appVars.ind15.isOn()) {
            sv.firstPaydateDisp.setReverse(BaseScreenData.REVERSED);
            sv.firstPaydateDisp.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind15.isOn()) {
            sv.firstPaydateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind40.isOn()) {
            sv.nextPaydateDisp.setReverse(BaseScreenData.REVERSED);
        }
        if (appVars.ind76.isOn()) {
            sv.nextPaydateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (appVars.ind40.isOn()) {
            sv.nextPaydateDisp.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind40.isOn()) {
            sv.nextPaydateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind18.isOn()) {
            sv.anvdateDisp.setReverse(BaseScreenData.REVERSED);
            sv.anvdateDisp.setColor(BaseScreenData.RED);
        }
        if (appVars.ind42.isOn()) {
            sv.anvdateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind18.isOn()) {
            sv.anvdateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind17.isOn()) {
            sv.finalPaydateDisp.setReverse(BaseScreenData.REVERSED);
            sv.finalPaydateDisp.setColor(BaseScreenData.RED);
        }
        if (appVars.ind43.isOn()) {
            sv.finalPaydateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind17.isOn()) {
            sv.finalPaydateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind31.isOn()) {
            generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
        }
        if (appVars.ind31.isOn()) {
            sv.cancelDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
        }
        if (!appVars.ind70.isOn()) {
            sv.totalamt.setReverse(BaseScreenData.REVERSED);
            sv.totalamt.setColor(BaseScreenData.RED);
        }
        if (appVars.ind70.isOn()) {
            sv.totalamt.setInvisibility(BaseScreenData.INVISIBLE);
            sv.totalamt.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind70.isOn()) {
            sv.msgclaim.setInvisibility(BaseScreenData.INVISIBLE);
        }
        if (appVars.ind20.isOn()) {
            sv.fupflg.setReverse(BaseScreenData.REVERSED);
            sv.fupflg.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind20.isOn()) {
            sv.fupflg.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind21.isOn()) {
            sv.ddind.setReverse(BaseScreenData.REVERSED);
            sv.ddind.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind21.isOn()) {
            sv.ddind.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind47.isOn()) {
            sv.zrsumin.setEnabled(BaseScreenData.DISABLED);
        }
        if (appVars.ind45.isOn()) {
            sv.anntind.setReverse(BaseScreenData.REVERSED);
            sv.anntind.setColor(BaseScreenData.RED);
        }
        if (appVars.ind46.isOn()) {
            sv.anntind.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind45.isOn()) {
            sv.anntind.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind71.isOn()) {
            generatedText36.setInvisibility(BaseScreenData.INVISIBLE);
        }
        if (appVars.ind22.isOn()) {
            sv.agclmstz.setReverse(BaseScreenData.REVERSED);
            sv.agclmstz.setColor(BaseScreenData.RED);
        }
        if (appVars.ind71.isOn()) {
            sv.agclmstz.setInvisibility(BaseScreenData.INVISIBLE);
        }
        if (!appVars.ind22.isOn()) {
            sv.agclmstz.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind14.isOn()) {
            sv.recvdDateDisp.setReverse(BaseScreenData.REVERSED);
            sv.recvdDateDisp.setColor(BaseScreenData.RED);
        }
        if (appVars.ind19.isOn()) {
            sv.recvdDateDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind14.isOn()) {
            sv.recvdDateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind06.isOn()) {
            sv.incurdtDisp.setReverse(BaseScreenData.REVERSED);
            sv.incurdtDisp.setColor(BaseScreenData.RED);
        }
        if (appVars.ind48.isOn()) {
            sv.incurdtDisp.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind06.isOn()) {
            sv.incurdtDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind12.isOn()) {
			sv.pymt.setReverse(BaseScreenData.REVERSED);
			sv.pymt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind38.isOn()) {
			sv.pymt.setEnabled(BaseScreenData.DISABLED);
		}
		//=======================claim amount adjustment=========================
		if (appVars.ind44.isOn()) {
            sv.pymtAdj.setReverse(BaseScreenData.REVERSED);
            sv.pymtAdj.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind44.isOn()) {
            sv.pymtAdj.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind67.isOn()) {
        	sv.pymtAdj.setEnabled(BaseScreenData.DISABLED);
        }
				
        //=======================adjustamt amount=========================
        if (appVars.ind07.isOn()) {
            sv.adjustamt.setReverse(BaseScreenData.REVERSED);
            sv.adjustamt.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind07.isOn()) {
            sv.adjustamt.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind69.isOn()) {
        	sv.adjustamt.setEnabled(BaseScreenData.DISABLED);
        }

        //=======================Net Claim Amount=========================
        if (appVars.ind09.isOn()) {
            sv.netclaimamt.setReverse(BaseScreenData.REVERSED);
            sv.netclaimamt.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind09.isOn()) {
            sv.netclaimamt.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind50.isOn()) {
        	 sv.netclaimamt.setEnabled(BaseScreenData.DISABLED);
        }

        //=======================adjustamt Reason=========================
        if (appVars.ind08.isOn()) {
            sv.reasoncd.setReverse(BaseScreenData.REVERSED);
            sv.reasoncd.setColor(BaseScreenData.RED);
        }

        if (!appVars.ind08.isOn()) {
            sv.reasoncd.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind49.isOn()) {
        	 sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
        }

        //=======================adjustamt Reason Description=========================
        if (appVars.ind10.isOn()) {
            sv.resndesc.setReverse(BaseScreenData.REVERSED);
            sv.resndesc.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind10.isOn()) {
            sv.resndesc.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind66.isOn()) {
        	sv.resndesc.setEnabled(BaseScreenData.DISABLED);
        }
		//FWANG3
        if (appVars.ind30.isOn()) {
            sv.investres.setReverse(BaseScreenData.REVERSED);
            sv.investres.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind30.isOn()) {
            sv.investres.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind51.isOn()) {
            sv.claimnotes.setReverse(BaseScreenData.REVERSED);
            sv.claimnotes.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind51.isOn()) {
            sv.claimnotes.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind52.isOn()) {
            sv.notifino.setReverse(BaseScreenData.REVERSED);
            sv.notifino.setColor(BaseScreenData.RED);
        }
        if (appVars.ind68.isOn()) {
            sv.notifino.setEnabled(BaseScreenData.DISABLED);
        }
        if (!appVars.ind52.isOn()) {
            sv.notifino.setHighLight(BaseScreenData.BOLD);
        }
        //ILJ-48 - START
		if (appVars.ind77.isOn()) {
			sv.riskcommdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}			
		//ILJ-48 - END
    }
%>
<style>
    .input-group-addon {
        height: 20px !important;
        padding-top: 1px !important;
        padding-bottom: 2px !important;
        border-radius: 5px 5px 5px 5px !important;
    }

    .form-control {
        margin-right: 1px !important;
        text-align: left;
    }
</style>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
                    <table>
                        <tr>
                            <td>
                                <%
                                    if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.chdrnum.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    } else {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.chdrnum.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    }
                                %>
                                <div
                                        class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                                    <%=XSSFilter.escapeHtml(formatValue)%>
                                </div> <%
                                longValue = null;
                                formatValue = null;
                            %>
                            </td>
                            <td>
                                <%
                                    if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.cnttype.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    } else {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.cnttype.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    }
                                %>
                                <div
                                        class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                                    <%=XSSFilter.escapeHtml(formatValue)%>
                                </div> <%
                                longValue = null;
                                formatValue = null;
                            %>
                            </td>
                            <td style="max-width:150px;">
                                <%
                                    if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.ctypedes.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    } else {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.ctypedes.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    }
                                %>
                                <div
                                        class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                                    <%=XSSFilter.escapeHtml(formatValue)%>
                                </div> <%
                                longValue = null;
                                formatValue = null;
                            %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
          
    	    <div class="col-md-4"> 
    	      <% if (sv.showFlag.compareTo("Y") == 0) { %> 
	    		<div class="form-group">  	  
		    		<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
				<%
                      if ((new Byte((sv.notifino).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                      || fw.getVariables().isScreenProtected()) {
                   %>
				<div class="input-group" style="width: 150px;">
					<%=smartHF.getHTMLVarExt(fw, sv.notifino)%>
				</div>
                   <%
                   } else {
                   %>
                   
                     <div class="input-group" style="width: 170px;">
                                    <%=smartHF.getRichTextInputFieldLookup(fw, sv.notifino)%>
                                    <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                        style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                        type="button"
                                                        onClick="doFocus(document.getElementById('notifino')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                </div>
 
				<%
					}
				%>
				</div><% } %>
    	   </div>         
    	     
    	   <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Cmpnt")%></label>
                    <%
                        if (!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.crtable.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.crtable.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div
                            class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
                    <table><tr><td>
                        <%
                            if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.lifcnum.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.lifcnum.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td><td style="padding-left:1px;">

                        <%
                            if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.linsname.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.linsname.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td></tr></table>

                </div>
            </div>
            <!-- ILJ-48 Starts -->
			<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>

                    <%
                        if (!((sv.riskcommdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 85px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
            <%} %>
			<!-- ILJ-48 End -->
            <div class="col-md-4"></div>
            <% if (sv.showFlag.compareTo("Y") == 0) { %> 
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim No")%></label>
                     <%
                        if (!((sv.claimno.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.claimno.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.claimno.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 150px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
        	<% } %>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
                    <table><tr><td>
                        <%
                            if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.cownnum.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.cownnum.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td><td style="padding-left:1px;">

                        <%
                            if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.ownername.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.ownername.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td></tr></table>

                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="form-group">
                    <!-- ILJ-48 Starts -->
					<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Date")%></label>
					<%} else { %>
	        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("RCD"))%></label>
	        		<%} %>
					<!-- ILJ-48 End -->

                    <%
                        if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 150px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
                    <table><tr><td>
                        <%
                            if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.rstate.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.rstate.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td><td style="padding-left:1px;">

                        <%
                            if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.pstate.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.pstate.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
                        >
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td></tr></table>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
                    <div class="input-group">
                        <%
                            if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div style="width: 120px;"
                             class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
                    <%
                        if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 120px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("CCY")%></label>
                    <table><tr><td>
                        <%
                            if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.currcd.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.currcd.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td><td>

                        <%
                            if (!((sv.currds.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.currds.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.currds.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td></tr></table>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Seq No")%></label>
                    <%
                        if (!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.rgpynum.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.rgpynum.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 150px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Receive Date")%></label>
                
                        <% if ((new Byte((sv.recvdDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || fw.getVariables().isScreenProtected()) {       %>
                        <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp)%>

                        </div>
                        <%}else{%>
                        <div class="input-group date form_date col-md-8" data-date=""
                             data-date-format="dd/mm/yyyy" data-link-field="recvdDateDisp"
                             data-link-format="dd/mm/yyyy" style="width: 150px;">
                            <%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp, (sv.recvdDateDisp.getLength()))%>
                            <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                        </div>

                        <%}%>


                        <%-- <%
                            if ((new Byte((sv.recvdDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                    || fw.getVariables().isScreenProtected()) {
                        %>
                        <%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp, (sv.recvdDateDisp.getLength()))%>
                        <%
                            } else {
                        %>
                        <div class="input-group date form_date col-md-12"
                            style="max-width: 150px;" data-date=""
                            data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
                            data-link-format="dd/mm/yyyy">
                            <%=smartHF.getRichTextDateInput(fw, sv.recvdDateDisp, (sv.recvdDateDisp.getLength()))%>
                            <span class="input-group-addon"><span
                                class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <%
                            }
                        %> --%>
                   
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Incur Date")%></label>
                  

                        <% if ((new Byte((sv.incurdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || fw.getVariables().isScreenProtected()) {       %>
                        <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp)%>

                        </div>
                        <%}else{%>
                        <div class="input-group date form_date col-md-8" data-date=""
                             data-date-format="dd/mm/yyyy" data-link-field="incurdtDisp"
                             data-link-format="dd/mm/yyyy" style="width: 150px;">
                            <%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp, (sv.incurdtDisp.getLength()))%>
                            <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                        </div>

                        <%}%> <%--
						<%
							if ((new Byte((sv.incurdtDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp, (sv.incurdtDisp.getLength()))%>
						<%
							} else {
						%>
						<div class="input-group date form_date col-md-12"
							style="max-width: 150px;" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp, (sv.incurdtDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<%
							}
						%> --%>
                   
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
                    <%
                        if (!((sv.rgpytypesd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.rgpytypesd.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.rgpytypesd.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 150px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>

                </div>
            </div>

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Status")%></label>
                    <table><tr><td>
                        <%
                            if (!((sv.rgpystat.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.rgpystat.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.rgpystat.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td><td style="padding-left:1px;">

                        <%
                            if (!((sv.statdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.statdsc.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            } else {

                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue((sv.statdsc.getFormData()).toString());
                                } else {
                                    formatValue = formatValue(longValue);
                                }

                            }
                        %>
                        <div
                                class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </td></tr></table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>
                    <%
                        fieldItem = appVars.loadF4FieldsLong(new String[] { "cltype" }, sv, "E", baseModel);
                        mappedItems = (Map) fieldItem.get("cltype");
                        optionValue = makeDropDownList(mappedItems, sv.cltype.getFormData(), 2, resourceBundleHandler);
                        longValue = (String) mappedItems.get((sv.cltype.getFormData()).toString().trim());
                    %>

                    <%
                        if ((new Byte((sv.cltype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                    %>
                    <div
                            class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%
                            if (longValue != null) {
                        %>

                        <%=XSSFilter.escapeHtml(longValue)%>

                        <%
                            }
                        %>
                    </div>

                    <%
                        longValue = null;
                    %>

                    <%
                    } else {
                    %>

                    <%
                        if ("red".equals((sv.cltype).getColor())) {
                    %>
                    <div
                            style="border: 1px; border-style: solid; border-color: #B55050; width: 200px;">
                        <%
                            }
                        %>

                        <select name='cltype' type='list' style="width: 215px;"
                                <%if ((new Byte((sv.cltype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
                                readonly="true" disabled class="output_cell" style="width:200px;"
                                <%} else if ((new Byte((sv.cltype).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                                class="bold_cell" style="width:200px;" <%} else {%>
                                class='input_cell' style="width:200px;" <%}%>>
                            <%=optionValue%>
                        </select>
                        <%
                            if ("red".equals((sv.cltype).getColor())) {
                        %>
                    </div>
                    <%
                        }
                    %>

                    <%
                        }
                    %>
                </div>
            </div>

            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Evidence")%></label>
                    <div style="width: 150px;">
                        <input name='claimevd' type='text'
                            <%if ((sv.claimevd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%>
                            <%formatValue = (sv.claimevd.getFormData()).toString();%>
                               value='<%= XSSFilter.escapeHtml(formatValue)%>'
                            <%if (formatValue != null && formatValue.trim().length() > 0) {%>
                               title='<%=formatValue%>' <%}%>
                               size='<%=sv.claimevd.getLength()%>'
                               maxLength='<%=sv.claimevd.getLength()%>' onFocus='doFocus(this)'
                               onHelp='return fieldHelp(claimevd)'
                               onKeyUp='return checkMaxLength(this)'
                            <%if ((new Byte((sv.claimevd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.claimevd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.claimevd).getColor() == null ? "input_cell"
						: (sv.claimevd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Sum")%></label>
                    <div style="width: 150px;">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.zrsumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
                        %>

                        <input name='zrsumin' type='text'
                            <%if ((sv.zrsumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%> value='<%=valueThis%>'
                            <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                               title='<%=valueThis%>' <%}%>
                               size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.zrsumin.getLength(), sv.zrsumin.getScale(), 3)%>'
                               maxLength='<%=sv.zrsumin.getLength()%>'
                               onFocus='doFocus(this),onFocusRemoveCommas(this)'
                               onHelp='return fieldHelp(zrsumin)'
                               onKeyUp='return checkMaxLength(this)'
                               onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                               decimal='<%=qpsf.getDecimals()%>'
                               onPaste='return doPasteNumber(event,true);'
                               onBlur='return doBlurNumberNew(event,true);'
                            <%if ((new Byte((sv.zrsumin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.zrsumin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.zrsumin).getColor() == null ? "input_cell"
						: (sv.zrsumin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
                    <%
                        fieldItem = appVars.loadF4FieldsLong(new String[] { "rgpymop" }, sv, "E", baseModel);
                        mappedItems = (Map) fieldItem.get("rgpymop");
                        optionValue = makeDropDownList(mappedItems, sv.rgpymop.getFormData(), 2, resourceBundleHandler);
                        longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());
                    %>

                    <%
                        if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                    %>
                    <div style="width: 150px;"
                         class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%
                            if (longValue != null) {
                        %>

                        <%=XSSFilter.escapeHtml(longValue)%>

                        <%
                            }
                        %>
                    </div>

                    <%
                        longValue = null;
                    %>

                    <%
                    } else {
                    %>

                    <%
                        if ("red".equals((sv.rgpymop).getColor())) {
                    %>
                    <div
                            style="border: 1px; border-style: solid; border-color: #B55050; width: 215px;">
                        <%
                            }
                        %>

                        <select name='rgpymop' type='list' style="width: 220px;"
                                <%if ((new Byte((sv.rgpymop).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
                                readonly="true" disabled class="output_cell" style="width:220px;"
                                <%} else if ((new Byte((sv.rgpymop).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                                class="bold_cell" style="width:215px;" <%} else {%>
                                class='input_cell' style="width:215px;" <%}%>>
                            <%=optionValue%>
                        </select>
                        <%
                            if ("red".equals((sv.rgpymop).getColor())) {
                        %>
                    </div>
                    <%
                        }
                    %>

                    <%
                        }
                    %>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>
                    <%
                        fieldItem = appVars.loadF4FieldsLong(new String[] { "regpayfreq" }, sv, "E", baseModel);
                        mappedItems = (Map) fieldItem.get("regpayfreq");
                        optionValue = makeDropDownList(mappedItems, sv.regpayfreq.getFormData(), 2, resourceBundleHandler);
                        longValue = (String) mappedItems.get((sv.regpayfreq.getFormData()).toString().trim());
                    %>

                    <%
                        if ((new Byte((sv.regpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                    %>
                    <div
                            class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%
                            if (longValue != null) {
                        %>

                        <%=XSSFilter.escapeHtml(longValue)%>

                        <%
                            }
                        %>
                    </div>

                    <%
                        longValue = null;
                    %>

                    <%
                    } else {
                    %>

                    <%
                        if ("red".equals((sv.regpayfreq).getColor())) {
                    %>
                    <div
                            style="border: 1px; border-style: solid; border-color: #B55050; width: 150px;">
                        <%
                            }
                        %>

                        <select name='regpayfreq' type='list' style="width: 150px;"
                                <%if ((new Byte((sv.regpayfreq).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
                                readonly="true" disabled class="output_cell"
                                <%} else if ((new Byte((sv.regpayfreq).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                                class="bold_cell" <%} else {%> class='input_cell' <%}%>>
                            <%=optionValue%>
                        </select>
                        <%
                            if ("red".equals((sv.regpayfreq).getColor())) {
                        %>
                    </div>
                    <%
                        }
                    %>

                    <%
                        }
                    %>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
                    <table>
                        <%
                            longValue = null;
                        %>
                        <tr>
                            <td>
                                <%
                                    if ((new Byte((sv.payclt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                            || fw.getVariables().isScreenProtected()) {
                                %>

                                <div class="input-group" style="width: 71px;">
                                    <%=smartHF.getHTMLVarExt(fw, sv.payclt)%>


                                </div>
                                <%
                                } else {
                                %>
                                <div class="input-group" style="width: 120px;">
                                    <%=smartHF.getRichTextInputFieldLookup(fw, sv.payclt)%>
                                    <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                        style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                        type="button"
                                                        onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                </div>
                                <%
                                    }
                                %>

                            </td>

                            <td style="padding-left:1px;">
                                <%
                                    if (!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.payenme.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    } else {

                                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                                            formatValue = formatValue((sv.payenme.getFormData()).toString());
                                        } else {
                                            formatValue = formatValue(longValue);
                                        }

                                    }
                                %>
                                <div style="width: 150px; "
                                     class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                                    <%=XSSFilter.escapeHtml(formatValue)%>
                                </div> <%
                                longValue = null;
                                formatValue = null;
                            %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("% of Sum Assured")%></label>
                    <div class="input-group">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
                            qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            formatValue = smartHF.getPicFormatted(qpsf, sv.prcnt);

                            if (!((sv.prcnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue(formatValue);
                                } else {
                                    formatValue = formatValue(longValue);
                                }
                            }

                            if (!formatValue.trim().equalsIgnoreCase("")) {
                        %>
                        <div class="output_cell" style="width: 150px;">
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                        } else {
                        %>

                        <div style="width: 150px;" class="blank_cell">&nbsp;</div>

                        <%
                            }
                        %>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Destination Key")%></label>
                    <div style="width: 150px;">
                        <input name='destkey' type='text'
                            <%if ((sv.destkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%>
                            <%formatValue = (sv.destkey.getFormData()).toString();%>
                               value='<%= XSSFilter.escapeHtml(formatValue)%>'
                            <%if (formatValue != null && formatValue.trim().length() > 0) {%>
                               title='<%=formatValue%>' <%}%> size='<%=sv.destkey.getLength()%>'
                               maxLength='<%=sv.destkey.getLength()%>' onFocus='doFocus(this)'
                               onHelp='return fieldHelp(destkey)'
                               onKeyUp='return checkMaxLength(this)'
                            <%if ((new Byte((sv.destkey).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.destkey).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.destkey).getColor() == null ? "input_cell"
						: (sv.destkey).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                	<%if ((new Byte((sv.pymtAdj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Original Claim Amount")%></label>
                    <%}else{ %>
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></label>
                    <%}%>
                    <div style="width: 150px;">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.pymt).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.pymt,
                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

//                            qpsf = fw.getFieldXMLDef((sv.adjustamt).getFieldName());
//                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
//                            valueAdjAmt = smartHF.getPicFormatted(qpsf, sv.adjustamt,
//                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
                        %>

                        <input name='pymt' type='text'
                            <%if ((sv.pymt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%> value='<%=valueThis%>'
                            <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                               title='<%=valueThis%>' <%}%>
                               size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.pymt.getLength(), sv.pymt.getScale(), 3)%>'
                               maxLength='<%=sv.pymt.getLength()%>'
                               onFocus='doFocus(this),onFocusRemoveCommas(this)'
                               onHelp='return fieldHelp(pymt)'
                               onKeyUp='return checkMaxLength(this)'
                               onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                               decimal='<%=qpsf.getDecimals()%>'
                               onPaste='return doPasteNumber(event,true);'
                               onBlur='return doBlurNumberNew(event,true);'
                            <%if ((new Byte((sv.pymt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.pymt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.pymt).getColor() == null ? "input_cell"
						: (sv.pymt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Total Paid to Date")%></label>
                    <div style="width: 150px;">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.totamnt).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            formatValue = smartHF.getPicFormatted(qpsf, sv.totamnt,
                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

                            if (!((sv.totamnt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
                                if (longValue == null || longValue.equalsIgnoreCase("")) {
                                    formatValue = formatValue(formatValue);
                                } else {
                                    formatValue = formatValue(longValue);
                                }
                            }

                            if (!formatValue.trim().equalsIgnoreCase("")) {
                        %>
                        <div style="width: 850px;" class="output_cell">
                            <%=XSSFilter.escapeHtml(formatValue)%>
                        </div>
                        <%
                        } else {
                        %>

                        <div style="width: 85px;" class="blank_cell">&nbsp;</div>

                        <%
                            }
                        %>
                        <%
                            longValue = null;
                            formatValue = null;
                        %>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%></label>
                    <div style="width: 150px;">
                        <%
                            fieldItem = appVars.loadF4FieldsLong(new String[] { "claimcur" }, sv, "E", baseModel);
                            mappedItems = (Map) fieldItem.get("claimcur");
                            optionValue = makeDropDownList(mappedItems, sv.claimcur.getFormData(), 2, resourceBundleHandler);
                            longValue = (String) mappedItems.get((sv.claimcur.getFormData()).toString().trim());
                        %>

                        <%
                            if ((new Byte((sv.claimcur).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                    || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                        %>
                        <div
                                class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                            <%
                                if (longValue != null) {
                            %>

                            <%=XSSFilter.escapeHtml(longValue)%>

                            <%
                                }
                            %>
                        </div>

                        <%
                            longValue = null;
                        %>

                        <%
                        } else {
                        %>

                        <%
                            if ("red".equals((sv.claimcur).getColor())) {
                        %>
                        <div
                                style="border: 1px; border-style: solid; border-color: #B55050; >
                                    <%
								}
							%>

                                        <select name='claimcur' type='list' style="width: 175px;"
                        <%if ((new Byte((sv.claimcur).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
                        readonly="true" disabled class="output_cell"
                        <%} else if ((new Byte((sv.claimcur).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                        class="bold_cell" <%} else {%> class='input_cell' <%}%>>
                        <%=optionValue%>
                        </select>
                        <%
                            if ("red".equals((sv.claimcur).getColor())) {
                        %>
                    </div>
                    <%
                        }
                    %>

                    <%
                        }
                    %>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Approval Date")%></label>
                <div style="width: 150px;">
                    <%
                        longValue = sv.aprvdateDisp.getFormData();
                        if (!((sv.aprvdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.aprvdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        } else {

                            if (longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue((sv.aprvdateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue(longValue);
                            }

                        }
                    %>
                    <div style="width: 85px;"
                         class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">

                <label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>


                <% if ((new Byte((sv.crtdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                        || fw.getVariables().isScreenProtected()) {       %>
                <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp)%>

                </div>
                <%}else{%>
                <div class="input-group date form_date col-md-8" data-date=""
                     data-date-format="dd/mm/yyyy" data-link-field="crtdateDisp"
                     data-link-format="dd/mm/yyyy" style="width: 150px;">
                    <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp, (sv.crtdateDisp.getLength()))%>
                    <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                </div>

                <%}%>


                <%-- <%
                    if ((new Byte((sv.crtdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {
                %>
                <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp, (sv.crtdateDisp.getLength()))%>
                <%
                    } else {
                %>
                <div class="input-group date form_date col-md-12"
                    style="min-width: 140px;" data-date=""
                    data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
                    data-link-format="dd/mm/yyyy">
                    <%=smartHF.getRichTextDateInput(fw, sv.crtdateDisp, (sv.crtdateDisp.getLength()))%>
                    <span class="input-group-addon"><span
                        class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <%
                    }
                %> --%>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">

                <label><%=resourceBundleHandler.gettingValueFromBundle("Review Date")%></label>
               
                    <% if ((new Byte((sv.revdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {       %>
                    <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.revdteDisp)%>

                    </div>
                    <%}else{%>
                    <div class="input-group date form_date col-md-8" data-date=""
                         data-date-format="dd/mm/yyyy" data-link-field="revdteDisp"
                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                        <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp, (sv.revdteDisp.getLength()))%>
                        <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                    </div>

                    <%}%>

                
                <%-- <%
                    if ((new Byte((sv.revdteDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {
                %>
                <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp, (sv.revdteDisp.getLength()))%>
                <%
                    } else {
                %>
                <div class="input-group date form_date col-md-12"
                    style="min-width: 140px;" data-date=""
                    data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
                    data-link-format="dd/mm/yyyy">
                    <%=smartHF.getRichTextDateInput(fw, sv.revdteDisp, (sv.revdteDisp.getLength()))%>
                    <span class="input-group-addon"><span
                        class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <%
                    }
                %> --%>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">

                <label><%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%></label>
               

                    <% if ((new Byte((sv.firstPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {       %>
                    <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp)%>

                    </div>
                    <%}else{%>
                    <div class="input-group date form_date col-md-8" data-date=""
                         data-date-format="dd/mm/yyyy" data-link-field="firstPaydateDisp"
                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                        <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp, (sv.firstPaydateDisp.getLength()))%>
                        <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                    </div>

                    <%}%>

               

                <%-- <%
                    if ((new Byte((sv.firstPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {
                %>

                <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp, (sv.firstPaydateDisp.getLength()))%>
                <%
                    } else {
                %>
                <div class="input-group date form_date col-md-12"
                    style="max-width: 150px;" data-date=""
                    data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
                    data-link-format="dd/mm/yyyy">
                    <%=smartHF.getRichTextDateInput(fw, sv.firstPaydateDisp, (sv.firstPaydateDisp.getLength()))%>
                    <span class="input-group-addon"><span
                        class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <%
                    }
                %> --%>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Last Paid Date")%></label>
                <div class="input-group" style="width:85px;">

                    <%
                        if(!((sv.lastPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue( longValue);
                            }


                        } else  {

                            if(longValue == null || longValue.equalsIgnoreCase("")) {
                                formatValue = formatValue( (sv.lastPaydateDisp.getFormData()).toString());
                            } else {
                                formatValue = formatValue( longValue);
                            }

                        }
                    %>
                    <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
						"blank_cell" : "output_cell" %>'>
                        <%=XSSFilter.escapeHtml(formatValue)%>
                    </div>
                    <%
                        longValue = null;
                        formatValue = null;
                    %>

                </div>


            </div></div>


        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Next Payment Date")%></label>
                <%
                    longValue = sv.nextPaydateDisp.getFormData();
                %>

                <%
                    if ((new Byte((sv.nextPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                %>
                <div style="width: 120px;"
                     class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                    <%
                        if (longValue != null) {
                    %>

                    <%=XSSFilter.escapeHtml(longValue)%>

                    <%
                        }
                    %>
                </div>

                <%
                    longValue = null;
                %>
                <%
                } else {
                %>
                <div class="input-group date form_date col-md-12" data-date=""
                     data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
                     data-link-format="dd/mm/yyyy"
                     style="width: 150px; padding-top: 13px;">
                    <input name='nextPaydateDisp' type='text'
                           value='<%=sv.nextPaydateDisp.getFormData()%>'
                           maxLength='<%=sv.nextPaydateDisp.getLength()%>'
                           size='<%=sv.nextPaydateDisp.getLength()%>'
                           onFocus='doFocus(this)'
                           onHelp='return fieldHelp(nextPaydateDisp)'
                           onKeyUp='return checkMaxLength(this)'
                        <%if ((new Byte((sv.nextPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
                           readonly="true" class="output_cell">

                    <%
                    } else if ((new Byte((sv.nextPaydateDisp).getHighLight()))
                            .compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
                    %>
                    class="bold_cell" > <span class="input-group-addon"> <span
                        class="glyphicon glyphicon-calendar"></span>
						</span>

                    <%
                    } else {
                    %>

                    class = '
                    <%=(sv.nextPaydateDisp).getColor() == null ? "input_cell"
                            : (sv.nextPaydateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>' > <span
                        class="input-group-addon"> <span
                        class="glyphicon glyphicon-calendar"></span>
						</span>
                </div>
                <%
                        }
                    }
                %>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%></label>

                    <% if ((new Byte((sv.anvdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {       %>
                    <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp)%>

                    </div>
                    <%}else{%>
                    <div class="input-group date form_date col-md-8" data-date=""
                         data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp"
                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                        <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp, (sv.anvdateDisp.getLength()))%>
                        <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                    </div>

                    <%}%>
                
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%></label>
                
                    <% if ((new Byte((sv.finalPaydateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                            || fw.getVariables().isScreenProtected()) {       %>
                    <div style="width: 85px;"><%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp)%>

                    </div>
                    <%}else{%>
                    <div class="input-group date form_date col-md-8" data-date=""
                         data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp"
                         data-link-format="dd/mm/yyyy" style="width: 150px;">
                        <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp, (sv.finalPaydateDisp.getLength()))%>
                        <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                    </div>

                    <%}%>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Cancellation Date")%></label>


                <%
                    if (!((sv.cancelDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                            formatValue = formatValue((sv.cancelDateDisp.getFormData()).toString());
                        } else {
                            formatValue = formatValue(longValue);
                        }

                    } else {

                        if (longValue == null || longValue.equalsIgnoreCase("")) {
                            formatValue = formatValue((sv.cancelDateDisp.getFormData()).toString());
                        } else {
                            formatValue = formatValue(longValue);
                        }

                    }
                %>
                <div style="width: 85px;"
                     class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                    <%=XSSFilter.escapeHtml(formatValue)%>
                </div>
                <%
                    longValue = null;
                    formatValue = null;
                %>
            </div>
        </div>
        
        
        </div>
         
     <%if(sv.action.compareTo("Y") == 0){%>
        	
       
        
         <div class="row">
   
			 <div class="col-md-4" >
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("No. of Days for Interest")%></label>
					 <div class="input-group" style="min-width:80px">
						<%
						
							qpsf = fw.getFieldXMLDef((sv.intstdays).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.intstdays,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.intstdays.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					 </div>
				 </div>
			 </div>
		
		
			 <div class="col-md-4">
				 <div class="form-group" >
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
					 <div class="input-group" style="min-width:80px">
					<%
						
							qpsf = fw.getFieldXMLDef((sv.intstrate).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.intstrate,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.intstrate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>	 </div>
			</div> </div> 
			 
			 		<div class="col-md-4">
				 <div class="form-group">
					 <label><%=resourceBundleHandler.gettingValueFromBundle("Interest Amount")%></label>
					 <div class="input-group" style="min-width: 80px;">
							 
					<%
						
							qpsf = fw.getFieldXMLDef((sv.intstamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.intstamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.intstamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					 </div>
				 </div>
			 </div>
			 
			 </div>
        
         <%}%>
        
        <div class="row">
         <!-- CML-009 -->
         
    <%if ((new Byte((sv.adjustamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
            <div class="col-md-4" style="min-width:120px;">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Amount")%></label>
                    <div style="width: 150px;">
                        <%
                            qpsf = fw.getFieldXMLDef((sv.adjustamt).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.adjustamt,
                                    COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
                        %>

                        <input name='adjustamt' type='text'
                            <%if ((sv.adjustamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
                               style="text-align: right" <%}%> value='<%=valueThis%>'
                            <%if (valueThis != null && valueThis.trim().length() > 0) {%>
                               title='<%=valueThis%>' <%}%>
                               size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.adjustamt.getLength(), sv.adjustamt.getScale(), 3)%>'
                               maxLength='<%=sv.pymt.getLength()%>'
                               onFocus='doFocus(this),onFocusRemoveCommas(this)'
                               onHelp='return fieldHelp(pymt)'
                               onKeyUp='return checkMaxLength(this)'
                               onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
                               decimal='<%=qpsf.getDecimals()%>'
                               onPaste='return doPasteNumber(event,true);'
                               onBlur='return doBlurNumberNew(event,true);'
                            <%if ((new Byte((sv.adjustamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
                               readonly="true" class="output_cell"
                            <%} else if ((new Byte((sv.adjustamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                               class="bold_cell" <%} else {%>
                               class=' <%=(sv.adjustamt).getColor() == null ? "input_cell"
						: (sv.adjustamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
                            <%}%>>
                    </div>
                </div>
            </div>
			<%} %>
            <%-- adjustment reason code --%>            
             <%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
            <div class="col-md-4">
                <div class="form-group" >
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Reason")%></label>
                    
                    <table><tr><td>
                    <%
                        fieldItem = appVars.loadF4FieldsLong(new String[] { "reasoncd" }, sv, "E", baseModel);
                        mappedItems = (Map) fieldItem.get("reasoncd");
                        optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 1, resourceBundleHandler);
                        longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
                    %>

                    <%
                        if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                || (((ScreenModel) fw).getVariables().isScreenProtected())) {
                    %>
                    <div style="width: 150px;"
                         class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
                        <%
                            if (longValue != null) {
                        %>

                        <%=sv.reasoncd%>

                        <%
                            }
                        %>
                    </div>

                    <%
                        longValue = null;
                    %>

                    <%
                    } else {
                    %>

                    <%
                        if ("red".equals((sv.reasoncd).getColor())) {
                    %>
                    <div
                            style="border: 1px; border-style: solid; border-color: #B55050; width: 215px;">
                        <%
                            }
                        %>

                        <select name='reasoncd' type='list' style="width: 142px;"
                                <%if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
                                readonly="true" disabled class="output_cell" style="width:150px;"
                                <%} else if ((new Byte((sv.reasoncd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
                                class="bold_cell" style="width:215px;" <%} else {%>
                                class='input_cell' style="width:215px;" <%}%> onchange="change()">
                            <%=optionValue%>
                        </select>
                        <%
                            if ("red".equals((sv.reasoncd).getColor())) {
                        %>
                    </div>

                    <%
                        }
                    %>

                    <%
                        }
                    %>
               
			<%} %>
			</td><td>
			<%if ((new Byte((sv.resndesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
			
                
                 <%-- Adjustment Reason Description --%>
               <input name='resndesc' 
						type='text'
						
						<%
						
								fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("reasoncd");
								optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
								formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim()); 
								
						%>
						<%if(formatValue==null) {
							formatValue="";
						}
						 %>
						 
						
						 
							value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='50'
						maxLength='50' 						 
							readonly="true"
							class="output_cell">
						
						
								<%
								longValue = null;
								formatValue = null;
								%>
			 
        

    <%} %>
 
     </td></tr></table>
                    </div>
                    </div>
     
     <%if ((new Byte((sv.netclaimamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
            <%-- net claim amount --%>
            <div class="col-md-4">
                <div class="form-group">
                    <label><%=resourceBundleHandler.gettingValueFromBundle("Net Claim Amount")%></label>
                   <div class="input-group" style="min-width: 80px;">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.netclaimamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.netclaimamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.netclaimamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > </div>
						
						<% 
							} 
						%>
						
					
				 		<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
                </div>
            </div>
			<%} %>
     
     </div>
   
   
   
  
    <Div id='mainForm_OPTS' style='visibility: hidden;'>
        <%=smartHF.getMenuLink(sv.fupflg, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>
        <%=smartHF.getMenuLink(sv.anntind, resourceBundleHandler.gettingValueFromBundle("Annuity Details"),
                true)%>
        <%=smartHF.getMenuLink(sv.ddind, resourceBundleHandler.gettingValueFromBundle("Bank Account"))%>
        <%=smartHF.getMenuLink(sv.agclmstz, resourceBundleHandler.gettingValueFromBundle("Accident Claim"))%>
        <% if (sv.showFlag.compareTo("Y") == 0) { %> 
        <%=smartHF.getMenuLink(sv.investres, resourceBundleHandler.gettingValueFromBundle("Investigation Results"))%>
        <%=smartHF.getMenuLink(sv.claimnotes, resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes"))%>
        <% } %> 
    </div>
    
</div>
</div>


<script type="text/javascript">

function change()
{
	
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}
</script> 
 


<%@ include file="/POLACommon2NEW.jsp"%>


