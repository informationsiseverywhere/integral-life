<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6648";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6648ScreenVars sv = (S6648ScreenVars) fw.getVariables();%>

<%if (sv.S6648screenWritten.gt(0)) {%>
	<%S6648screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum-Assured");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Remaining");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Months");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid Up");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid Up");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pol. Term");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"In Force");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(months)");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Option");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"--------------------------------------------------------------------------------------------------------------------------------------------------");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"00");%>
	<%sv.minisuma01.setClassString("");%>
	<%sv.minterm01.setClassString("");%>
	<%sv.minmthif01.setClassString("");%>
	<%sv.minpusa01.setClassString("");%>
	<%sv.minipup01.setClassString("");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"01");%>
	<%sv.minisuma02.setClassString("");%>
	<%sv.minterm02.setClassString("");%>
	<%sv.minmthif02.setClassString("");%>
	<%sv.minpusa02.setClassString("");%>
	<%sv.minipup02.setClassString("");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"02");%>
	<%sv.minisuma03.setClassString("");%>
	<%sv.minterm03.setClassString("");%>
	<%sv.minmthif03.setClassString("");%>
	<%sv.minpusa03.setClassString("");%>
	<%sv.minipup03.setClassString("");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"04");%>
	<%sv.minisuma04.setClassString("");%>
	<%sv.minterm04.setClassString("");%>
	<%sv.minmthif04.setClassString("");%>
	<%sv.minpusa04.setClassString("");%>
	<%sv.minipup04.setClassString("");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"12");%>
	<%sv.minisuma05.setClassString("");%>
	<%sv.minterm05.setClassString("");%>
	<%sv.minmthif05.setClassString("");%>
	<%sv.minpusa05.setClassString("");%>
	<%sv.minipup05.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind01.isOn()) {
			sv.minisuma01.setReverse(BaseScreenData.REVERSED);
			sv.minisuma01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.minisuma01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.minterm01.setReverse(BaseScreenData.REVERSED);
			sv.minterm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.minterm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.minmthif01.setReverse(BaseScreenData.REVERSED);
			sv.minmthif01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.minmthif01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.minpusa01.setReverse(BaseScreenData.REVERSED);
			sv.minpusa01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.minpusa01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.minipup01.setReverse(BaseScreenData.REVERSED);
			sv.minipup01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.minipup01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.minisuma02.setReverse(BaseScreenData.REVERSED);
			sv.minisuma02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.minisuma02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.minterm02.setReverse(BaseScreenData.REVERSED);
			sv.minterm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.minterm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.minmthif02.setReverse(BaseScreenData.REVERSED);
			sv.minmthif02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.minmthif02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.minpusa02.setReverse(BaseScreenData.REVERSED);
			sv.minpusa02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.minpusa02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.minipup02.setReverse(BaseScreenData.REVERSED);
			sv.minipup02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.minipup02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.minisuma03.setReverse(BaseScreenData.REVERSED);
			sv.minisuma03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.minisuma03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.minterm03.setReverse(BaseScreenData.REVERSED);
			sv.minterm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.minterm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.minmthif03.setReverse(BaseScreenData.REVERSED);
			sv.minmthif03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.minmthif03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.minpusa03.setReverse(BaseScreenData.REVERSED);
			sv.minpusa03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.minpusa03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.minipup03.setReverse(BaseScreenData.REVERSED);
			sv.minipup03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.minipup03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.minisuma04.setReverse(BaseScreenData.REVERSED);
			sv.minisuma04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.minisuma04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.minterm04.setReverse(BaseScreenData.REVERSED);
			sv.minterm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.minterm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.minmthif04.setReverse(BaseScreenData.REVERSED);
			sv.minmthif04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.minmthif04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.minpusa04.setReverse(BaseScreenData.REVERSED);
			sv.minpusa04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.minpusa04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.minipup04.setReverse(BaseScreenData.REVERSED);
			sv.minipup04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.minipup04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.minisuma05.setReverse(BaseScreenData.REVERSED);
			sv.minisuma05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.minisuma05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.minterm05.setReverse(BaseScreenData.REVERSED);
			sv.minterm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.minterm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.minmthif05.setReverse(BaseScreenData.REVERSED);
			sv.minmthif05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.minmthif05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.minpusa05.setReverse(BaseScreenData.REVERSED);
			sv.minpusa05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.minpusa05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.minipup05.setReverse(BaseScreenData.REVERSED);
			sv.minipup05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.minipup05.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		   <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div>
				</div>
			
			
			
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				    </div>
				 </div>   	
				 
				
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table>
					<tr>
					<td>
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		</td>
  		<td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		
		 <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div>
        </div>
        
        <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Frequency"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Minimum"))%></label>
        				
        		</div>
        	</div>
        </div>	
        
        <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sum-Assured"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Remaining"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Months"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Paid&nbsp;Up"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Paid&nbsp;Up"))%></label>
        				
        		</div>
        	</div>
        </div>	
        
        <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Pol.&nbsp;Term"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("In&nbsp;Force"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sum"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Cash"))%></label>
        				
        		</div>
        	</div>
        </div>	
        
                <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("(months)"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(""))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Assured"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Option"))%></label>
        				
        		</div>
        	</div>
        </div>	
        
         <div class="row">
              <div class="col-md-12">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"))%></label>
        				
        		</div>
        	</div>
        </div>	
        
                        <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("00"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minisuma01, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minmthif01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minpusa01, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minipup01, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        </div>	
        
         <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("01"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minisuma02, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minterm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minmthif02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minpusa02, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minipup02, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        </div>	
        
                 <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("02"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minisuma03, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minterm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minmthif03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minpusa03, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minipup03, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        </div>	
        
                         <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("04"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minisuma04, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minterm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minmthif04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minpusa04, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minipup04, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        </div>	
        
                                 <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("12"))%></label>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minisuma05, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minterm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-1">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minmthif05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
        				
        		</div>
        	</div>
        	
        	<div class="col-md-1"></div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minpusa05, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        	
        	
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<%=smartHF.getHTMLVar(fw, sv.minipup05, COBOLHTMLFormatter.S11VS2)%>
        				
        		</div>
        	</div>
        </div>	
     </div>
  </div>
       				














































<%-- <div class='outerDiv'>
<table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<style>
@media \0screen\,screen\9
{
.output_cell{margin-right:2px}
}
</style>
<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:22px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	



&nbsp;

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table></div> --%>

	<%-- <div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:16px; width:72px;'>Frequency</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:88px; width:56px;'>Minimum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:218px; width:56px;'>Minimum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:324px; width:56px;'>Minimum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:405px; width:56px;'>Minimum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:164px; left:540px; width:56px;'>Minimum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:184px; left:88px; width:88px;'>Sum-Assured</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:184px; left:218px; width:72px;'>Remaining</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:184px; left:324px; width:48px;'>Months</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:184px; left:405px; width:56px;'>Paid&nbsp;Up</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:184px; left:540px; width:56px;'>Paid&nbsp;Up</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:204px; left:218px; width:72px;'>Pol.&nbsp;Term</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:204px; left:324px; width:64px;'>In&nbsp;Force</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:204px; left:405px; width:24px;'>Sum</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:204px; left:540px; width:32px;'>Cash</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:224px; left:218px; width:64px;'>(months)</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:224px; left:405px; width:56px;'>Assured</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:224px; left:540px; width:48px;'>Option</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:244px; left:16px; width:712px;'>-------------------------------------------------------------------------------------------------------------------------------------------------------------</div>

	<div class='label_txt'class='label_txt' style='position: absolute; top:284px; left:24px; width:16px;'>00</div>

	<%=smartHF.getHTMLVar(14, 11, fw, sv.minisuma01, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(14, 28, fw, sv.minterm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 41, fw, sv.minmthif01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 51, fw, sv.minpusa01, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(14, 68, fw, sv.minipup01, COBOLHTMLFormatter.S11VS2)%>

	<div class='label_txt'class='label_txt' style='position: absolute; top:304px; left:24px; width:16px;'>01</div>

	<%=smartHF.getHTMLVar(15, 11, fw, sv.minisuma02, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(15, 28, fw, sv.minterm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 41, fw, sv.minmthif02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 51, fw, sv.minpusa02, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(15, 68, fw, sv.minipup02, COBOLHTMLFormatter.S11VS2)%>

	<div class='label_txt'class='label_txt' style='position: absolute; top:324px; left:24px; width:16px;'>02</div>

	<%=smartHF.getHTMLVar(16, 11, fw, sv.minisuma03, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(16, 28, fw, sv.minterm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 41, fw, sv.minmthif03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 51, fw, sv.minpusa03, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(16, 68, fw, sv.minipup03, COBOLHTMLFormatter.S11VS2)%>

	<div class='label_txt'class='label_txt' style='position: absolute; top:344px; left:24px; width:16px;'>04</div>

	<%=smartHF.getHTMLVar(17, 11, fw, sv.minisuma04, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(17, 28, fw, sv.minterm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 41, fw, sv.minmthif04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 51, fw, sv.minpusa04, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(17, 68, fw, sv.minipup04, COBOLHTMLFormatter.S11VS2)%>

	<div class='label_txt'class='label_txt' style='position: absolute; top:364px; left:24px; width:16px;'>12</div>

	<%=smartHF.getHTMLVar(18, 11, fw, sv.minisuma05, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(18, 28, fw, sv.minterm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 41, fw, sv.minmthif05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 51, fw, sv.minpusa05, COBOLHTMLFormatter.S11VS2)%>

	<%=smartHF.getHTMLVar(18, 68, fw, sv.minipup05, COBOLHTMLFormatter.S11VS2)%>

 --%>


<%}%>

<%if (sv.S6648protectWritten.gt(0)) {%>
	<%S6648protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<style>
div[id*='minpusa']{padding-right:2px !important} 
div[id*='minipup']{padding-right:2px !important}
div[id*='minmthif']{padding-right:2px !important}
div[id*='minisuma']{padding-right:2px !important}
div[id*='minterm']{padding-right:2px !important}
</style>  
<%@ include file="/POLACommon2NEW.jsp"%>