<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6673";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6673ScreenVars sv = (S6673ScreenVars) fw.getVariables();
    fieldItem = appVars.loadF4FieldsLong(new String[] {"cntcurr", "register"}, sv, "E", baseModel);
%>
<%
	StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Attached Rider         ");
	if (appVars.ind50.isOn()) {
		sv.ridnum.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind50.isOn()) {
		generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
	}
%>
<style>
.input-group-addon {
	height: 20px !important;
	padding-top: 1px !important;
	padding-bottom: 2px !important;
	border-radius: 5px 5px 5px 5px !important;
	min-width: 150px;
}

.form-control {
	margin-right: 2px !important;
	text-align: left;
	max-width: 150px;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<!-- <div class="input-group three-controller"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.chdrnum.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.cnttype.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>

						<%
							formatValue = formatValue((sv.ctypedes.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<div class="input-group">
					<%
						
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.cntcurr.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>

					<div 
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div></div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<%
						formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<div class="input-group">
					<%
						formatValue = formatValue((sv.premstatus.getFormData()).toString());
					%>
					<div 
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div></div>

			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<div class="input-group">	
					<%
						
						mappedItems = (Map) fieldItem.get("register");
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.register.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>
					<div 
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div></div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.lifenum.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.lifename.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td style="min-width:100px;">
						<%
							formatValue = formatValue((sv.jlife.getFormData()).toString());
						%>
						<div style="left: -1px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td style="min-width:100px;">
						<%
							formatValue = formatValue((sv.jlifename.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.crtable.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>
						<%
							formatValue = formatValue((sv.crtabdesc.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid to Date")%></label>
					<div class="input-group">
					<%
						formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
					%>
					<div 
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div></div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed to Date")%></label>
					<div class="input-group">
					<%
						formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
					%>
					<div 
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.billfreq.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div></td><td>
						
						<%
							formatValue = formatValue((sv.bilfrqdesc.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label>
					<%
						formatValue = formatValue((sv.crrcdDisp.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 100px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix")%></label>
					<%
						if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							formatValue = formatValue((sv.planSuffix.getFormData()).toString());
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 75px;">
						<%=(sv.planSuffix.getFormData()).toString()%>
					</div>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
						formatValue = smartHF.getPicFormatted(qpsf, sv.numpols);

						if (!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="max-width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
					<%
						formatValue = formatValue((sv.life.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
					<%
						formatValue = formatValue((sv.coverage.getFormData()).toString());
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
					<%
						if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							formatValue = formatValue((sv.rider.getFormData()).toString());
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 50px;">
						<%=(sv.rider.getFormData()).toString()%>
					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>
		&nbsp;

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Selected Component")%></label>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Attached Rider")%></label>
					<%
						if ((new Byte((sv.ridnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							formatValue = formatValue((sv.ridnum.getFormData()).toString());
					%>

					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style="max-width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum-Assured")%></label>
					<%
						qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
						formatValue = smartHF.getPicFormatted(qpsf, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

						if (!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell">&nbsp;</div>

					<%
						}
					%>

				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Risk Status")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.statcode.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>

						<%
							formatValue = formatValue((sv.statdesc.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Component Premium Status")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>
						<%
							formatValue = formatValue((sv.pstatcode.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td><td>

						<%
							formatValue = formatValue((sv.pstatdesc.getFormData()).toString());
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						</td></tr></table>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
	<div style='visibility: hidden;'>
		<table>
			<tr style='height: 35px;'>
				<td width='188'>
					<div class="label_txt">
						<%=resourceBundleHandler.gettingValueFromBundle("Press ENTER to process")%>
					</div>
			</tr>
		</table>
	</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
