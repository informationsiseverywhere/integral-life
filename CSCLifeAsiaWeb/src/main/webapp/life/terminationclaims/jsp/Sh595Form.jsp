

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH595";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sh595ScreenVars sv = (Sh595ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Number ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Status ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Prm Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Owner ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Commence ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to Date ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Non-forfeiture Option ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------------------------------------------------------------");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"New Non-forfeiture option ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Reason ");
%>

<%
	{
		if (appVars.ind01.isOn()) {
			sv.znfopt02.setReverse(BaseScreenData.REVERSED);
			sv.znfopt02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.znfopt02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
	}
%>


<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<div class="input-group" style="max-width: 100px">

						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div style="margin-right: 1px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>



		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
					<div class="input-group">

						<%
							if (!((sv.statdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statdsc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statdsc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Prm Status")%></label>
					<div class="input-group">

						<%
							if (!((sv.premStatDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premStatDesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premStatDesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Owner")%></label>
					<div class="input-group" style="max-width: 100px">
						<%
							if (!((sv.ownersel.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownersel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownersel.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div style="margin-right: 1px;"
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ownername.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>




					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commence")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
					<div class="input-group">

						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>



					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to Date")%></label>
					<div class="input-group">

						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>

				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Non-forfeiture Option")%></label>
					<div class="input-group" style="min-width: 100px">


						<%
							if (!((sv.znfoptd01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.znfoptd01.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.znfoptd01.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("New Non-forfeiture option")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "znfopt02" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("znfopt02");
						optionValue = makeDropDownList(mappedItems, sv.znfopt02.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.znfopt02.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.znfopt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.znfopt02).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 170px;">
						<%
							}
						%>

						<select name='znfopt02' type='list' style="width: 170px;"
							<%if ((new Byte((sv.znfopt02).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.znfopt02).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.znfopt02).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>




				</div>
			</div>


			<div class="col-md-8">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></label>

					<table>
						<tr>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[] { "reasoncd" }, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("reasoncd");
									optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%
										if (longValue != null) {
									%>

									<%=XSSFilter.escapeHtml(longValue)%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.reasoncd).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 80px;">
									<%
										}
									%>

									<select name='reasoncd' type='list' style="width: 80px;"
										<%if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.reasoncd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell'
										<%}%>>
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.reasoncd).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %>
							</td>
							<td><input style="margin-left: 1px;width: 150px;" name='resndesc' 
type='text'

<%

		formatValue = (sv.resndesc.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='15'
maxLength='<%= sv.resndesc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(resndesc)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.resndesc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.resndesc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.resndesc).getColor()== null  ? 
			"input_cell" :  (sv.resndesc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
						</tr>
					</table>

				</div>
			</div>
		</div>


		<div style='visibility: hidden;'>
			<table>
				<tr style='height: 22px;'>
					<td width='188'>

						<div class="label_txt">
							<%=resourceBundleHandler.gettingValueFromBundle("Number")%>
						</div>
					</td>
					<td>
						<%
							if (!((sv.znfopt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.znfopt01.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.znfopt01.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!--  panel-->
</div>
<!--panel  -->

<%@ include file="/POLACommon2NEW.jsp"%>
