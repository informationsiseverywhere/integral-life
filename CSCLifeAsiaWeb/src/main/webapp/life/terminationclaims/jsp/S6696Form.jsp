<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "S6696";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%S6696ScreenVars sv = (S6696ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Claim %                 ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum Override %              ");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Override %              ");%>
<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Deferment Period        ");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency    ");%>
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Minimum Deferment Period        ");%>
<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency    ");%>
<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Review Term                     ");%>
<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency    ");%>
<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Override of Frequency Allowed   ");%>
<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Indexation Frequency            ");%>
<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Indexation Subroutine           ");%>
<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Enter the Sub Account that will fund the claim ");%>
<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Account Code                ");%>
<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Account Type                ");%>
<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"G/L Account Key                 ");%>
<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"DB/CR (+/-)                     ");%>
<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dissection Method               ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.dfclmpct.setReverse(BaseScreenData.REVERSED);
			sv.dfclmpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dfclmpct.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mxovrpct.setReverse(BaseScreenData.REVERSED);
			sv.mxovrpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mxovrpct.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.mnovrpct.setReverse(BaseScreenData.REVERSED);
			sv.mnovrpct.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.mnovrpct.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.dfdefprd.setReverse(BaseScreenData.REVERSED);
			sv.dfdefprd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.dfdefprd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.freqcy01.setReverse(BaseScreenData.REVERSED);
			sv.freqcy01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.freqcy01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.mndefprd.setReverse(BaseScreenData.REVERSED);
			sv.mndefprd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.mndefprd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.freqcy02.setReverse(BaseScreenData.REVERSED);
			sv.freqcy02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.freqcy02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.revitrm.setReverse(BaseScreenData.REVERSED);
			sv.revitrm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.revitrm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.freqcy03.setReverse(BaseScreenData.REVERSED);
			sv.freqcy03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.freqcy03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.frqoride.setReverse(BaseScreenData.REVERSED);
			sv.frqoride.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.frqoride.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.inxfrq.setReverse(BaseScreenData.REVERSED);
			sv.inxfrq.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.inxfrq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.inxsbm.setReverse(BaseScreenData.REVERSED);
			sv.inxsbm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.inxsbm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.sacscode.setReverse(BaseScreenData.REVERSED);
			sv.sacscode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.sacscode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.sacstype.setReverse(BaseScreenData.REVERSED);
			sv.sacstype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.sacstype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.glact.setReverse(BaseScreenData.REVERSED);
			sv.glact.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.glact.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.sign.setReverse(BaseScreenData.REVERSED);
			sv.sign.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.sign.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.dissmeth.setReverse(BaseScreenData.REVERSED);
			sv.dissmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.dissmeth.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<div class="input-group">
						<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="min-width:30px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
		longValue = null;
		formatValue = null;
		%>
		</div>
</div>
</div>
<div class="col-md-2">
</div>
	<div class="col-md-2">
				<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<div class="input-group">

							<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:60px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>

				</div>
						</div>

</div>
<div class="col-md-2">
</div>

<div class="col-md-4">
				<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
							<div class="input-group">

	<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="max-width:600px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


					</div>
</div>

				</div>
			</div>
			
			
			
			
			<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Valid from")%></label>
					 <table>
						<tr>
						   <td>
						 <%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					
					</td>
					<td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>	

					<td>
							 <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
           </td>
           </tr>
		</table>
				 </div>									
				</div>
			</div>
			
		
		
		
		
		
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Claim %")%></label>
					<div class="input-group">
		         	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct) %>'
	 <%}%>

size='<%= sv.dfclmpct.getLength()%>'
maxLength='<%= sv.dfclmpct.getLength()+1%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dfclmpct).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
</div>
</div>

<div class="col-md-2">
</div>

		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Maximum Override %")%></label>
					<div class="input-group">
					<%	
			qpsf = fw.getFieldXMLDef((sv.mxovrpct).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxovrpct' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxovrpct) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxovrpct);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxovrpct) %>'
	 <%}%>

size='<%= sv.mxovrpct.getLength()%>'
maxLength='<%= sv.mxovrpct.getLength()+1%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxovrpct)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxovrpct).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxovrpct).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxovrpct).getColor()== null  ? 
			"input_cell" :  (sv.mxovrpct).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
					
	</div>					
</div>
</div>

<div class="col-md-1">
</div>

<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Override %")%></label>
					<div class="input-group">
<%	
			qpsf = fw.getFieldXMLDef((sv.mnovrpct).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mnovrpct' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mnovrpct) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mnovrpct);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mnovrpct) %>'
	 <%}%>

size='<%= sv.mnovrpct.getLength()%>'
maxLength='<%= sv.mnovrpct.getLength()+1%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mnovrpct)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mnovrpct).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mnovrpct).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mnovrpct).getColor()== null  ? 
			"input_cell" :  (sv.mnovrpct).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
	</div>
</div>

</div>	
		








		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Default Deferment Period")%></label>	
					<div class="input-group">
		<%	
			qpsf = fw.getFieldXMLDef((sv.dfdefprd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfdefprd' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfdefprd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfdefprd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfdefprd) %>'
	 <%}%>

size='<%= sv.dfdefprd.getLength()%>'
maxLength='<%= sv.dfdefprd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfdefprd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dfdefprd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfdefprd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfdefprd).getColor()== null  ? 
			"input_cell" :  (sv.dfdefprd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
		      
		</div>
			</div>
     </div>
		
		<div class="col-md-1">
		
		 </div>
		
		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>	
					<div class="input-group">
		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy01");
	optionValue = makeDropDownList( mappedItems , sv.freqcy01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy01.getFormData()).toString().trim());  
%>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<%=smartHF.getDropDownExt(sv.freqcy01, fw, longValue, "freqcy01", optionValue,0,150) %> 
		      
		</div>
			</div>
     </div>
		
	</div>	
		
		
		
		
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Minimum Deferment Period")%></label>	
					<div class="input-group">
		<%	
			qpsf = fw.getFieldXMLDef((sv.mndefprd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mndefprd' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.mndefprd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mndefprd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mndefprd) %>'
	 <%}%>

size='<%= sv.mndefprd.getLength()%>'
maxLength='<%= sv.mndefprd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mndefprd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mndefprd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mndefprd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mndefprd).getColor()== null  ? 
			"input_cell" :  (sv.mndefprd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
		</div>
		
		</div>	
		</div>	
		<div class="col-md-1">
		
		 </div>
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>	
		
		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy02");
	optionValue = makeDropDownList( mappedItems , sv.freqcy02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy02.getFormData()).toString().trim());  
%>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<%=smartHF.getDropDownExt(sv.freqcy02, fw, longValue, "freqcy02", optionValue,0,150) %> 
		
		
		
		</div>	
		</div>	
		
	</div>	
		
		
		
	

	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Review Term")%></label>	
					<div class="input-group">
		<%	
			qpsf = fw.getFieldXMLDef((sv.revitrm).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='revitrm' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.revitrm) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.revitrm);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.revitrm) %>'
	 <%}%>

size='<%= sv.revitrm.getLength()%>'
maxLength='<%= sv.revitrm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(revitrm)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.revitrm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.revitrm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.revitrm).getColor()== null  ? 
			"input_cell" :  (sv.revitrm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
		</div>
		 </div> 
		 <div class="col-md-1">
		  </div> 
		 <div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%></label>	
		 <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy03");
	optionValue = makeDropDownList( mappedItems , sv.freqcy03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy03.getFormData()).toString().trim());  
%>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<%=smartHF.getDropDownExt(sv.freqcy03, fw, longValue, "freqcy03", optionValue,0,150) %> 
		
		 
		 
		 
		</div>
		</div>
		</div>   <!-- end of row -->
		
		
		
		
		
		<div class="row">
			<div class="col-md-4">
			<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Override of Frequency Allowed")%></label>
					<table><tr> 
					<td>
					<input name='frqoride' 
type='text'

<%

		formatValue = (sv.frqoride.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.frqoride.getLength()%>'
maxLength='<%= sv.frqoride.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(frqoride)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.frqoride).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.frqoride).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.frqoride).getColor()== null  ? 
			"input_cell" :  (sv.frqoride).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
<td style="padding-left: 10px;">
	<%=resourceBundleHandler.gettingValueFromBundle("(Y/N)")%>		
	</td>			
		</tr></table>
		
			</div> 
		</div> 
		</div>  <!-- end of row -->
		
		
		
		
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Indexation Frequency")%></label>
					<div class="input-group">
					
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"inxfrq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("inxfrq");
	optionValue = makeDropDownList( mappedItems , sv.inxfrq.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.inxfrq.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.inxfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){

%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%if ((longValue == null)||("".equals(longValue.trim()))) {%>
						<%}else { %>
				<%=XSSFilter.escapeHtml(longValue)%>
				<%} %>
			</div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.inxfrq).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='inxfrq' type='list' style="width:180px;"
<% 
	if((new Byte((sv.inxfrq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.inxfrq).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.inxfrq).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</div>
</div>
	</div>
<div class="col-md-1">
</div>
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Indexation Subroutine")%></label>	
					<div class="input-group">
					<input name='inxsbm' 
type='text'

<%

		formatValue = (sv.inxsbm.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.inxsbm.getLength()%>'
maxLength='<%= sv.inxsbm.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(inxsbm)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.inxsbm).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.inxsbm).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.inxsbm).getColor()== null  ? 
			"input_cell" :  (sv.inxsbm).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>			
		</div>
		
		</div>
		</div>
		</div>  <!-- end of row -->
		
		
		
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Enter the Sub Account that will fund the claim")%></label>
					
			</div>
		</div>
		</div>  <!-- end of row -->
		
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Account Code")%></label>
					
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacscode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacscode");
	optionValue = makeDropDownList( mappedItems , sv.sacscode.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacscode.getFormData()).toString().trim());  
%>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<%=smartHF.getDropDownExt(sv.sacscode, fw, longValue, "sacscode", optionValue,0, 250) %> 
					
			</div>
		</div>
		<div class="col-md-1">
		</div>
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Account Type")%></label>
		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"sacstype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("sacstype");
	optionValue = makeDropDownList( mappedItems , sv.sacstype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.sacstype.getFormData()).toString().trim());  
%>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<%=smartHF.getDropDownExt(sv.sacstype, fw, longValue, "sacstype", optionValue,0,290) %> 
		</div>
		</div>
		<div class="col-md-1">
		</div>
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("G/L Account Key")%></label>
					<div class="input-group">
					<input name='glact' id='glact'
type='text' 
value='<%=sv.glact.getFormData()%>' 
maxLength='<%=sv.glact.getLength()%>' 
size='<%=sv.glact.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(glact)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.glact).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.glact).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('glact')); doAction('PFKEY04')">
				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		</button>
</span> 

<%} %>

					</div>
		
		</div>
		</div>
		</div>  <!-- end of row -->
		
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("DB/CR (+/-)")%></label>
					<div class="input-group">
					<input name='sign' 
type='text'

<%

		formatValue = (sv.sign.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.sign.getLength()%>'
maxLength='<%= sv.sign.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(sign)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.sign).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || (((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sign).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sign).getColor()== null  ? 
			"input_cell" :  (sv.sign).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
			</div>		
			</div>
		</div>
		<div class="col-md-1">
		</div>
		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Dissection Method")%></label>
					<div class="input-group">
					<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dissmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dissmeth");
	optionValue = makeDropDownList( mappedItems , sv.dissmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dissmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dissmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.dissmeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='dissmeth' type='list' style="width:180px;"
<% 
	if((new Byte((sv.dissmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dissmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dissmeth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
		</div>
			</div>
		</div>
		
		</div>  <!-- end of row -->
		
		
	</div>   <!--  end ofmpanel body -->
</div>   <!-- end of panel-default? -->


<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->
<script>
$(document).ready(function(){
	createDropdownNotInTable("sacscode",7);
	createDropdownNotInTable("sacstype",7);
});
</script>
<!-- ILIFE-2675 Life Cross Browser - Sprint 3 D6 : Task 1  -->

<%@ include file="/POLACommon2NEW.jsp"%>