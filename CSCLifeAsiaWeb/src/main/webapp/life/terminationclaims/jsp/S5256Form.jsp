  

  <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S5256";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	S5256ScreenVars sv = (S5256ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"---------------- Claim Details -----------------------------------------------");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Date of Death ");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy loans ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date ");
%>
<%
	StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy Debt  ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cause of death ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Other adjustments ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Total ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Follow-ups ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cash Deposit ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Adjustment Reason ");
%>
<%
	StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contact Date ");
%>
<%
	// ilife-3589 starts
	String[][] items = {{"causeofdth", "currcd", "reasoncd"}, {}, {}};
	Map longDesc = appVars.getLongDesc(items, "E", baseModel.getCompany().toString().trim(), baseModel, sv);
	// ilife-3589 endss
%>

<%
	{
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setReverse(BaseScreenData.REVERSED);
			sv.causeofdth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.causeofdth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.resndesc.setReverse(BaseScreenData.REVERSED);
			sv.resndesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.resndesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setReverse(BaseScreenData.REVERSED);
			sv.dtofdeathDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind21.isOn()) {
            sv.bnfying.setReverse(BaseScreenData.REVERSED);
            sv.bnfying.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind21.isOn()) {
            sv.bnfying.setHighLight(BaseScreenData.BOLD);
        }
		if (appVars.ind22.isOn()) {
			sv.bnfying.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind31.isOn()) {
			sv.reserveUnitsInd.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind70.isOn()) {
			sv.reserveUnitsInd.setHighLight(BaseScreenData.BOLD);
				}
				
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setReverse(BaseScreenData.REVERSED);
				}
				if (appVars.ind32.isOn()) {
			sv.reserveUnitsDateDisp.setEnabled(BaseScreenData.DISABLED);
				}
				if (appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setColor(BaseScreenData.RED);
				}
				if (!appVars.ind71.isOn()) {
			sv.reserveUnitsDateDisp.setHighLight(BaseScreenData.BOLD);
				}
				
			 if(appVars.ind69.isOn()){
				sv.reserveUnitsDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
				sv.reserveUnitsDate.setInvisibility(BaseScreenData.INVISIBLE);
			}
		    if (appVars.ind23.isOn()) {
			    sv.claimnumber.setReverse(BaseScreenData.REVERSED);
			    sv.claimnumber.setColor(BaseScreenData.RED);
		    }
			 if (!appVars.ind23.isOn()) {
			    sv.claimnumber.setHighLight(BaseScreenData.BOLD);
		    }
			if (appVars.ind24.isOn()) {
			  	sv.claimnumber.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind25.isOn()) {
			    sv.notifinumber.setReverse(BaseScreenData.REVERSED);
			    sv.notifinumber.setColor(BaseScreenData.RED);
		    }
			 if (!appVars.ind25.isOn()) {
			    sv.notifinumber.setHighLight(BaseScreenData.BOLD);
		    }
			if (appVars.ind26.isOn()) {
			  	sv.notifinumber.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind27.isOn()) {
			    sv.claimnotes.setReverse(BaseScreenData.REVERSED);
			    sv.claimnotes.setColor(BaseScreenData.RED);
		    }
			 if (!appVars.ind27.isOn()) {
			    sv.claimnotes.setHighLight(BaseScreenData.BOLD);
		    }
			if (appVars.ind28.isOn()) {
			  	sv.claimnotes.setInvisibility(BaseScreenData.INVISIBLE);
			}			
			if (appVars.ind29.isOn()) {
			    sv.investres.setReverse(BaseScreenData.REVERSED);
			    sv.investres.setColor(BaseScreenData.RED);
		    }
			 if (!appVars.ind29.isOn()) {
			    sv.investres.setHighLight(BaseScreenData.BOLD);
		    }
			if (appVars.ind30.isOn()) {
			  	sv.investres.setInvisibility(BaseScreenData.INVISIBLE);
			}	
			//ILJ-48 - START
			if (appVars.ind77.isOn()) {
				sv.riskcommdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
			}			
			//ILJ-48 - END
			if (appVars.ind80.isOn()) {
    			sv.claimStat.setInvisibility(BaseScreenData.INVISIBLE);
    		}
            if (appVars.ind78.isOn()) {
                sv.claimStat.setColor(BaseScreenData.RED);
            }
            if (!appVars.ind78.isOn()) {
                sv.claimStat.setHighLight(BaseScreenData.BOLD);
            }
            if (appVars.ind79.isOn()) {
    			sv.claimStat.setEnabled(BaseScreenData.DISABLED);
    		}
            
            if (appVars.ind81.isOn()) {
    			sv.claimTyp.setInvisibility(BaseScreenData.INVISIBLE);
    		}
            if (appVars.ind85.isOn()) {
                sv.claimTyp.setColor(BaseScreenData.RED);
            }
            if (!appVars.ind85.isOn()) {
                sv.claimTyp.setHighLight(BaseScreenData.BOLD);
            }
            if (appVars.ind86.isOn()) {
    			sv.claimTyp.setEnabled(BaseScreenData.DISABLED);
    		}
            
            if (appVars.ind82.isOn()) {
    			sv.contactDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
    		}	
            if (appVars.ind83.isOn()) {
            	sv.contactDateDisp.setReverse(BaseScreenData.REVERSED);
                sv.contactDateDisp.setColor(BaseScreenData.RED);
            }
            if (!appVars.ind83.isOn()) {
                sv.contactDateDisp.setHighLight(BaseScreenData.BOLD);
            }
            if (appVars.ind84.isOn()) {
    			sv.contactDateDisp.setEnabled(BaseScreenData.DISABLED);
    		}
			
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/Life");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to-date ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to-date ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"---------------- Coverage/Rider Review ---------------------------------------");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cov/Rid Fund Typ Descriptn.Lien    Ccy   Estimated Value        Actual Value ");
%>
<%
	appVars.rollup(new int[]{93});
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<!-- ILIFE-1529 END -->

					<label> <%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
					</label>
					<%
						}
					%>
					<table>
						<tr >
							<td>
								<%
									if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.chdrnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td style="padding-left:1px;">
								<%
									if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <!-- ILIFE-1529 END--> <%
 	if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cnttype.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	} 
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ctypedes.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div id="cntdesc" style="margin-left: 1px;max-width: 160px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td>
					
						<%
							if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>

						<!-- ILIFE-1529 START by nnazeer -->

						<%
							}
						%>
							</td>
							<td style="padding-left:1px;">
						<%
							if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:100px;margin-left: 1px;max-width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<!-- ILJ-48 Starts -->
					<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Date")%></label>
					<%} else { %>
	        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("RCD"))%></label>
	        		<%} %>
					<!-- ILJ-48 End -->
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<!-- ILIFE-1529 END -->

						<%
							if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>

					<label> <%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<!-- ILIFE-1529 END-->
						<%
							if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cownnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td style="padding-left:1px;">
						
						<%
							if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>

						<%
							if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ownername.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:120px;max-width:195px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.asterisk.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.asterisk.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td style="padding-left:1px;">
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.linsname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="min-width:120px;max-width:160px;;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("J/Life")%>
					</label>
					<%
						}
					%>
					<table>
						<tr >
							<td>
								<%
									if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.asterisk.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.asterisk.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td style="padding-left:1px;width:65px;">
								<%
									if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %> <!-- ILIFE-1529 START by nnazeer -->
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<!-- Padding Added by pmujavadiya -->
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td style="padding-left:2px;width:65px;">
								<%
									if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlinsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlinsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %> <!-- ILIFE-1529 START by nnazeer -->
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<%if ((new Byte((sv.claimnumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim No.")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<%} %>
			<%if ((new Byte((sv.claimStat).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
			 <div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Status")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimStat.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div> 
			<%} %>
			<%if ((new Byte((sv.claimTyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimTyp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div> 
			<%} %>        
		</div>
		<div class="row">
		<%if ((new Byte((sv.notifinumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<div class="input-group" style="max-width:180px;" >
<input name='notifinumber' id='notifinumber'
type='text' 
value='<%=sv.notifinumber.getFormData()%>' 
maxLength='<%=sv.notifinumber.getLength()%>' 
size='<%=sv.notifinumber.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(notifinumber)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.notifinumber).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	 >

<%
	}else if((new Byte((sv.notifinumber).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 

<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('notifinumber')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%
	}else { 
%>

class = ' <%=(sv.notifinumber).getColor()== null  ? 
"input_cell" :  (sv.notifinumber).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 
<span class="input-group-btn">
<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('notifinumber')); doAction('PFKEY04')">
<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
</button>
       </span>

<%} %>

</div>
				</div>
			</div>
			<%} %>
			<!-- ILJ-48 Starts -->
			<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
			<div class="col-md-4">
				<div class="form-group">
	        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("RCD"))%></label>
					<div style="width: 80px;">
						<%
							if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.riskcommdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.riskcommdteDisp.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
				</div>
			</div>
			<%} %>
			<!-- ILJ-48 End -->
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage / Rider Details")%></label>
				</div>
			</div>
		</div>
		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[9];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.coverage.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 6;
			} else {
				calculatedValue = (sv.coverage.getFormData()).length() * 6;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rider.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 4;
			} else {
				calculatedValue = (sv.rider.getFormData()).length() * 4;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.vfund.getFormData()).length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
			} else {
				calculatedValue = (sv.vfund.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fieldType.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
			} else {
				calculatedValue = (sv.fieldType.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.shortds.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 8;
			} else {
				calculatedValue = (sv.shortds.getFormData()).length() * 8;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.liencd.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 10;
			} else {
				calculatedValue = (sv.liencd.getFormData()).length() * 10;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.cnstcur.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 6;
			} else {
				calculatedValue = (sv.cnstcur.getFormData()).length() * 6;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.estMatValue.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 8;
			} else {
				calculatedValue = (sv.estMatValue.getFormData()).length() * 8;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.actvalue.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length()) * 9;
			} else {
				calculatedValue = (sv.actvalue.getFormData()).length() * 9;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[8] = calculatedValue;
		%>

		<!-- ILIFE-1529 END -->
		<%
			GeneralTable sfl = fw.getTable("s5256screensfl");
			int height;
			if (sfl.count() * 27 > 80) {
				height = 80;
			} else {
				height = sfl.count() * 27;
			}
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s5256' width='100%' style="border-right: thin solid #dddddd !important;">
						<thead>
							<tr class='info'>
								<th colspan='2' style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></th>
							</tr>
						</thead>

						<tbody>
							<%
								S5256screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5256screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<%
									if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:50px;"
									<%if ((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.coverage.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td >
								</td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:50px;"
									<%if ((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.rider.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td >

								</td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.vfund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
							<td style="width:46px;"
									<%if ((sv.vfund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.vfund.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.fieldType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:42px;"
									<%if ((sv.fieldType).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.fieldType.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.shortds).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:96px;"
									<%if ((sv.shortds).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.shortds.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:86px;"
									<%if ((sv.liencd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.liencd.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.cnstcur).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:79px;"
									<%if ((sv.cnstcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>><%=sv.cnstcur.getFormData()%>



								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.estMatValue).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:134px;"
									<%if ((sv.estMatValue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="right" <%}%>>
									<%
										sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.estMatValue,
 					COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 			if (!(sv.estMatValue).getFormData().toString().trim().equalsIgnoreCase("")) {
 				formatValue = formatValue(formatValue);
 			}
 %> <%=formatValue%> <%
 	longValue = null;
 			formatValue = null;
 %>




								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>
								<%
									if ((new Byte((sv.actvalue).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td style="width:106px;"
									<%if ((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="right" <%}%>>
									<%
										sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.actvalue,
 					COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 			if (!(sv.actvalue).getFormData().toString().trim().equalsIgnoreCase("")) {
 				formatValue = formatValue(formatValue);
 			}
 %> <%=formatValue%> <%
 	longValue = null;
 			formatValue = null;
 %>




								</td>
								<%
									} else {
								%>
								<td ></td>

								<%
									}
								%>

							</tr>

							<%
								count = count + 1;
									S5256screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Details")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					<%
						if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.effdateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='effdateDisp' type='text'
							value='<%=sv.effdateDisp.getFormData()%>'
							maxLength='<%=sv.effdateDisp.getLength()%>'
							size='<%=sv.effdateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(effdateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.effdateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.effdateDisp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.effdateDisp).getColor() == null
							? "input_cell"
							: (sv.effdateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

					</div>

					<%
						}
					%>
				</div>
			</div>
			<%if ((new Byte((sv.contactDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contact Date")%></label>
					<%
						if ((new Byte((sv.contactDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.contactDateDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='contactDateDisp' type='text'
							value='<%=sv.contactDateDisp.getFormData()%>'
							maxLength='<%=sv.contactDateDisp.getLength()%>'
							size='<%=sv.contactDateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(contactDateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.contactDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell">
						<%
							} else if ((new Byte((sv.contactDateDisp).getHighLight()))
										.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
						<%
							} else {
						%>
						class = '
						<%=(sv.contactDateDisp).getColor() == null
							? "input_cell"
							: (sv.contactDateDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<%} %>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%></label>
					<%
						if ((new Byte((sv.dtofdeathDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.dtofdeathDisp)%></div>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='dtofdeathDisp' type='text'
							value='<%=sv.dtofdeathDisp.getFormData()%>'
							maxLength='<%=sv.dtofdeathDisp.getLength()%>'
							size='<%=sv.dtofdeathDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dtofdeathDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dtofdeathDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
							} else if ((new Byte((sv.dtofdeathDisp).getHighLight()))
										.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
							} else {
						%>

						class = '
						<%=(sv.dtofdeathDisp).getColor() == null
							? "input_cell"
							: (sv.dtofdeathDisp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						> <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

					</div>

					<%
						}
					%>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cause of death")%></label>					
						<%
							mappedItems = (Map) longDesc.get("causeofdth");
							optionValue = makeDropDownList(mappedItems, sv.causeofdth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.causeofdth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.causeofdth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div class='output_cell' style="max-width: 190px;min-width: 150px;">
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.causeofdth).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 193px;">
							<%
								}
							%>

							<select name='causeofdth' type='list' style="width: 190px;"
								<%if ((new Byte((sv.causeofdth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.causeofdth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>
								onchange="change()">
								<!-- BRD-140 --> 
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.causeofdth).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					
						<%
							mappedItems = (Map) longDesc.get("currcd");
							optionValue = makeDropDownList(mappedItems, sv.currcd.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.currcd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div class='output_cell' style="max-width: 190px;min-width: 150px;">
							<%=XSSFilter.escapeHtml(longValue)%>
						</div>

						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.currcd).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 193px;">
							<%
								}
							%>

							<select name='currcd' type='list' style="width: 190px;"
								<%if ((new Byte((sv.currcd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.currcd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.currcd).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>
			
			
		</div>
		<div class="row">
		
		<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units"))%></label>


					<%
						longValue = sv.reserveUnitsInd.getFormData();
						if ("".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Select");
						} else if ("Y".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("Y");
						} else if ("N".equals(longValue)) {
							longValue = resourceBundleHandler.gettingValueFromBundle("N");
						}
						if ((new Byte((sv.reserveUnitsInd.getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected()))) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:50px;" : "width:50px;"%>'>

						<%
							if (longValue != null) {
						%>

						<%=XSSFilter.escapeHtml(longValue)%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>
					<%
						if ("red".equals((sv.reserveUnitsInd).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050; width: 60px;">
						<%
							}
						%>
						<select name='reserveUnitsInd' type='list' style="width: 100px;"
							<%if ((new Byte((sv.reserveUnitsInd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.reserveUnitsInd).getHighLight()))
						.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell'
							<%}%>>

							<option value=""
								<%if ("".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Select")%></option>
							<option value="Y"
								<%if ("Y".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("Y")%></option>
							<option value="N"
								<%if ("N".equals(sv.reserveUnitsInd.getFormData())) {%> SELECTED
								<%}%>><%=resourceBundleHandler.gettingValueFromBundle("N")%></option>
						</select>
						<%
							if ("red".equals((sv.reserveUnitsInd).getColor())) {
						%>
					</div>
					<%
						}
					%>
					<%
						}
					%>
				</div>
			</div>
		
		
		<%-- <div class="col-md-4"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
		<div class="input-group">
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamant).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamant,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.clamant.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell"  style="width:80px">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:80px">  </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
	

  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:80px">	
					<%= formatValue%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="width:80px"> </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		 </div>
		 </div>
		</div>
		 --%>
		
		
		<div class="col-md-3">
				<div class="form-group" style="max-width: 150px;">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reserve Units Date"))%></label>

					<%-- <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div> --%>
					<%--  <div class="input-group date form_date col-md-8" data-date=""
						data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp"
						data-link-format="dd/mm/yyyy" style="min-width:162px;">
						<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp, (sv.reserveUnitsDateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
							
					</div> --%>
					
					<% 
								if((new Byte((sv.reserveUnitsDateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
							%>
								<%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
							<% }else {%>
			                <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="reserveUnitsDateDisp" data-link-format="dd/mm/yyyy">
			                    <%=smartHF.getRichTextDateInput(fw, sv.reserveUnitsDateDisp,(sv.reserveUnitsDateDisp.getLength()))%>
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			                </div>	
			                <%} %>	
					
				</div>
			</div>
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy loans")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.policyloan,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Debt")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.tdbtamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.tdbtamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>



						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row">
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Deposit")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
							formatValue = smartHF.getPicFormatted(qpsf, sv.zrcshamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"></div>

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Other adjustments")%></label>
					<div style="width: 150px;">
						<%
							qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.otheradjst,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
						%>

						<input name='otheradjst' type='text'
							<%if ((sv.otheradjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.otheradjst.getLength(), sv.otheradjst.getScale(), 3)%>'
							maxLength='<%=sv.otheradjst.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(otheradjst)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.otheradjst).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.otheradjst).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.otheradjst).getColor() == null
						? "input_cell"
						: (sv.otheradjst).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label style="white-space: nowrap;">
					 <%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%>
					</label>
					<%
						}
					%>
					
						<%
							if ((new Byte((sv.susamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.susamt);

								if (!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 150px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Unpaid Premium")%>
					</label>
					<%
						}
					%>
					
						<%
							if ((new Byte((sv.nextinsamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf, sv.nextinsamt);

								if (!((sv.nextinsamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 150px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment Reason")%></label>
					<table>
						<tr class="input-group three-controller">
							<td>
								<%
									mappedItems = (Map) longDesc.get("reasoncd");
									optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 1, resourceBundleHandler);
									longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());
								%> <%
 	if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 			|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
 %>
								<div class='output_cell'>
									<%=XSSFilter.escapeHtml(longValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	} else {
 %> <%
 	if ("red".equals((sv.reasoncd).getColor())) {
 %>
								<div
									style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
									<%
										}
									%>

									<select name='reasoncd' type='list' style="width: 140px;"
										<%if ((new Byte((sv.reasoncd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.reasoncd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell'
										<%}%> onchange="change()">
										<%=optionValue%>
									</select>
									<%
										if ("red".equals((sv.reasoncd).getColor())) {
									%>
								</div> <%
 	}
 %> <%
 	}
 %> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="padding-left:1px;"><input name='resndesc' type='text'
								<%mappedItems = (Map) longDesc.get("reasoncd");
			optionValue = makeDropDownList(mappedItems, sv.reasoncd.getFormData(), 2, resourceBundleHandler);
			formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());%>
								<%if (formatValue == null) {
				formatValue = "";
			}%>
								<%String str = (sv.resndesc.getFormData()).toString().trim();%>
								<%if (str.equals("") || str == null) {
				str = formatValue;
			}%>
								value='<%=str%>'
								<%if (formatValue != null && formatValue.trim().length() > 0) {%>
								title='<%=str%>' <%}%> size='50' maxLength='50'
								onFocus='doFocus(this)' onHelp='return fieldHelp(resndesc)'
								onKeyUp='return checkMaxLength(this)'
								<%if ((new Byte((sv.resndesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
								readonly="true" class="output_cell"
								<%} else if ((new Byte((sv.resndesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%>
								class=' <%=(sv.resndesc).getColor() == null
						? "input_cell"
						: (sv.resndesc).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
								<%}%>> <%
 	longValue = null;
 	formatValue = null;
 %></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
				 
					<label><%=resourceBundleHandler.gettingValueFromBundle("Estimated Total")%></label>
					
						<%
							qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.estimateTotalValue,
									COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

							if (!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"  style="width: 150px !important;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"  style="width: 150px !important;">&nbsp;</div>

						<%
							}
						%>
				
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Actual Total")%></label>
					
						<%
							qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.clamamt,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);

							if (!((sv.clamamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"  style="width: 150px !important;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 150px !important;">&nbsp;</div>

						<%
							}
						%>
					
				</div>
			</div>
			
		</div>
<script type="text/javascript">

function change()
{
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}

</script>
<Div id='mainForm_OPTS' style='visibility:hidden; width: 60px;'>
					<li>
					<input name='fupflg' id='fupflg' type='hidden' value="<%=sv.fupflg.getFormData()%>">
									<!-- text -->
									<%
									if((sv.fupflg.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.fupflg.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.fupflg.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('xoptind01'))"></i> 
			 						<%}%>
			 						</a>
								

							</li>
	                <%if ((new Byte((sv.bnfying).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<li>
							<input name='bnfying' id='bnfying' type='hidden' value="<%=sv.bnfying.getFormData()%>">
									<!-- text -->
									<%
									if((sv.bnfying.getInvisible()== BaseScreenData.INVISIBLE|| sv.bnfying
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Claim Payees")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bnfying"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Claim Payees")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.bnfying.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.bnfying.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.bnfying'))" style="cursor:pointer" src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
			 						<%}%>
			 						</a>
							</li>
                    <%}%>
                      <%if ((new Byte((sv.investres).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<li>
							<input name='investres' id='investres' type='hidden' value="<%=sv.investres.getFormData()%>">
									<!-- text -->
									<%
									if((sv.investres.getInvisible()== BaseScreenData.INVISIBLE|| sv.investres
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Investigation Results")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("investres"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Investigation Results")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.investres.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.investres.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.investres'))" style="cursor:pointer" src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
			 						<%}%>
			 						</a>
							</li>
                    <%}%>
                      <%if ((new Byte((sv.claimnotes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<li>
							<input name='claimnotes' id='claimnotes' type='hidden' value="<%=sv.claimnotes.getFormData()%>">
									<!-- text -->
									<%
									if((sv.claimnotes.getInvisible()== BaseScreenData.INVISIBLE|| sv.claimnotes
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("claimnotes"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.claimnotes.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.claimnotes.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.claimnotes'))" style="cursor:pointer" src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
			 						<%}%>
			 						</a>
							</li>
                    <%}%>
                  
</Div>
	<%-- <Div id="mainForm_OPTS" style="visibility:hidden;">
<!-- ILIFE-964   starts -->
<table>
<tr><td>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg
.getFormData()%>">
<%
if (sv.fupflg
.getFormData().equals("+")) {
%>

<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0">

<%}
	if (sv.fupflg
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('fupflg'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-weight: bold;font-size: 12px; font-family: Arial;padding-left:30px;background-color: white;">
<%

if(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED){
%>
<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
<% } else { %>
<div style="height: 15 px;">
<a href="javascript:;" 
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>

</a>	
</div>
<%} %>


</td></tr> 
</table>
<!-- ILIFE-964   ends -->
</div> --%>

<!-- GChawla End -->





<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
						} else {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
						}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
  
	

</td>

<td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%					
		if(!((sv.astrsk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.astrsk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
						} else {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.astrsk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
						}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
					<%
						longValue = null;
						formatValue = null;
					%>
  
	

</td></tr>
</table></div></div></div>


<script>
$(document).ready(function() {
	if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','230px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','190px')
	} 
	
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>