

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6625";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6625ScreenVars sv = (S6625ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default guaranteed period                               ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"years");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default percentage decrease on death of nominated life  ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default percentage decrease on death of other life      ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default frequency of annuity payments                   ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Certificate of Existence Frequency                      ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversionary bonus method                               ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus allocation (P/C)                                  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"New coverage after vesting                              ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Early vesting lead years                                ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Late vesting lag years                                  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Maximum percentage for commuting to a lump sum          ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commutation method                                      ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commutation method, Joint Life      ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                   ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Vesting Calculation method                              ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Vesting Calculation method, Joint Life                  ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annuity Type                  ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.ncovvest.setReverse(BaseScreenData.REVERSED);
			sv.ncovvest.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ncovvest.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.guarperd.setReverse(BaseScreenData.REVERSED);
			sv.guarperd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.guarperd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.evstperd.setReverse(BaseScreenData.REVERSED);
			sv.evstperd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.evstperd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.lvstperd.setReverse(BaseScreenData.REVERSED);
			sv.lvstperd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.lvstperd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.revBonusMeth.setReverse(BaseScreenData.REVERSED);
			sv.revBonusMeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.revBonusMeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.bonalloc.setReverse(BaseScreenData.REVERSED);
			sv.bonalloc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.bonalloc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.comtmeth.setReverse(BaseScreenData.REVERSED);
			sv.comtmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.comtmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.dthpercn.setReverse(BaseScreenData.REVERSED);
			sv.dthpercn.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.dthpercn.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.dthperco.setReverse(BaseScreenData.REVERSED);
			sv.dthperco.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.dthperco.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.comtperc.setReverse(BaseScreenData.REVERSED);
			sv.comtperc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.comtperc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.freqann.setReverse(BaseScreenData.REVERSED);
			sv.freqann.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.freqann.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.frequency.setReverse(BaseScreenData.REVERSED);
			sv.frequency.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.frequency.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.comtmthj.setReverse(BaseScreenData.REVERSED);
			sv.comtmthj.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.comtmthj.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.vcalcmth.setReverse(BaseScreenData.REVERSED);
			sv.vcalcmth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.vcalcmth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.vclcmthj.setReverse(BaseScreenData.REVERSED);
			sv.vclcmthj.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.vclcmthj.setHighLight(BaseScreenData.BOLD);
		}
		//ILIFE-3595-STARTS
		if (appVars.ind22.isOn()) {
			sv.annuty.setReverse(BaseScreenData.REVERSED);
			sv.annuty.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.annuty.setHighLight(BaseScreenData.BOLD);
		}
		//ILIFE-3595-ENDS
	}

	%>

<div class="panel panel-default">
		<div class="panel-body">
		   <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<div class="input-group">
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.company.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.company.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						</div>
					</div>
				</div>
			
			
			
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<div class="input-group">
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						</div>
				    </div>
				 </div>   	
				 
				 
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
					<%					
			if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.item.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.item.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
  	</td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width:200px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
					</td></tr></table>
				</div>
			</div>
		</div>
		
		 <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates Effective"))%></label>
        				<table>
        				<tr>
        				<td >
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td >
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div>
        </div>
        
         <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Annuity Type"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 <div class="col-md-4">
                <div class="form-group">
        				<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"annuty"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("annuty");
	optionValue = makeDropDownList( mappedItems , sv.annuty.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.annuty.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.annuty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.annuty).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='annuty' type='list' style="width:180px;"
<% 
	if((new Byte((sv.annuty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.annuty).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.annuty).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
        		</div>
        	 </div>	
        	</div> 
        	
        	<div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default guaranteed period"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                
                 <table>	
					<tr>
					<td>
                <%	
			qpsf = fw.getFieldXMLDef((sv.guarperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='guarperd' 
type='text'

<%if((sv.guarperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.guarperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
	 <%}%>

size='<%= sv.guarperd.getLength()%>'
maxLength='<%= sv.guarperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.guarperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.guarperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.guarperd).getColor()== null  ? 
			"input_cell" :  (sv.guarperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
<td><label> <%=resourceBundleHandler.gettingValueFromBundle("&nbsp;years")%></label></td>
	</tr>
	</table>
             
             </div>
           </div>  
         </div>      
         
         <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default percentage decrease on death of nominated life"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
			qpsf = fw.getFieldXMLDef((sv.dthpercn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>
<div class="input-group">
<input name='dthpercn' 
type='text'

<%if((sv.dthpercn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthpercn);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
	 <%}%>

size='<%= sv.dthpercn.getLength()%>'
maxLength='<%= sv.dthpercn.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthpercn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthpercn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthpercn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthpercn).getColor()== null  ? 
			"input_cell" :  (sv.dthpercn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
             </div>
             </div>   
           </div>
           
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default percentage decrease on death of other life"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
			qpsf = fw.getFieldXMLDef((sv.dthperco).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>
<div class="input-group">
<input name='dthperco' 
type='text'

<%if((sv.dthperco).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthperco);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
	 <%}%>

size='<%= sv.dthperco.getLength()%>'
maxLength='<%= sv.dthperco.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthperco)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthperco).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthperco).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthperco).getColor()== null  ? 
			"input_cell" :  (sv.dthperco).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
                </div>
              </div> 
           </div>    
          
           <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default frequency of annuity payments"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqann"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqann");
	optionValue = makeDropDownList( mappedItems , sv.freqann.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqann.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqann).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqann).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqann' type='list' style="width:220px;"
<% 
	if((new Byte((sv.freqann).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqann).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqann).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>  
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Certificate of Existence Frequency"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("frequency");
	optionValue = makeDropDownList( mappedItems , sv.frequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.frequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency' type='list' style="width:220px;"
<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reversionary bonus method"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"revBonusMeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("revBonusMeth");
	optionValue = makeDropDownList( mappedItems , sv.revBonusMeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.revBonusMeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.revBonusMeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.revBonusMeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='revBonusMeth' type='list' style="width:220px;"
<% 
	if((new Byte((sv.revBonusMeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.revBonusMeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.revBonusMeth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bonus allocation (P/C)"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	if ((new Byte((sv.bonalloc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Policy Anniversary");
	}
	if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("C")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Company Fixed Date");
	}
	 
%>

<%	
	if((new Byte((sv.bonalloc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
   				
   			<%} %>
	   		
	   </div>

<%
longValue = null;
%>

	<% }else { %>
	
<% if("red".equals((sv.bonalloc).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='bonalloc' style="width:220px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(bonalloc)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.bonalloc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.bonalloc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="P"<% if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Policy Anniversary")%></option>
<option value="C"<% if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("C")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Company Fixed Date")%></option>
		
		
</select>
<% if("red".equals((sv.bonalloc).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("New coverage after vesting"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"ncovvest"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("ncovvest");
	optionValue = makeDropDownList( mappedItems , sv.ncovvest.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.ncovvest.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.ncovvest).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.ncovvest).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='ncovvest' type='list' style="width:220px;"
<% 
	if((new Byte((sv.ncovvest).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.ncovvest).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.ncovvest).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Early vesting lead years"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group" >
                <%	
			qpsf = fw.getFieldXMLDef((sv.evstperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
<div class="input-group">
<input name='evstperd' 
type='text'

<%if((sv.evstperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.evstperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.evstperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.evstperd) %>'
	 <%}%>

size='<%= sv.evstperd.getLength()%>'
maxLength='<%= sv.evstperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(evstperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.evstperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.evstperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.evstperd).getColor()== null  ? 
			"input_cell" :  (sv.evstperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Late vesting lag years"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
			qpsf = fw.getFieldXMLDef((sv.lvstperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
	<div class="input-group">

<input name='lvstperd' 
type='text'

<%if((sv.lvstperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lvstperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lvstperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lvstperd) %>'
	 <%}%>

size='<%= sv.lvstperd.getLength()%>'
maxLength='<%= sv.lvstperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lvstperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lvstperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lvstperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lvstperd).getColor()== null  ? 
			"input_cell" :  (sv.lvstperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</div>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Maximum percentage for commuting to a lump sum"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
			qpsf = fw.getFieldXMLDef((sv.comtperc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>
	<div class="input-group">

<input name='comtperc' 
type='text'

<%if((sv.comtperc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.comtperc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.comtperc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.comtperc) %>'
	 <%}%>

size='<%= sv.comtperc.getLength()%>'
maxLength='<%= sv.comtperc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(comtperc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.comtperc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.comtperc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.comtperc).getColor()== null  ? 
			"input_cell" :  (sv.comtperc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
      </div>        </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commutation method"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"comtmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("comtmeth");
	optionValue = makeDropDownList( mappedItems , sv.comtmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.comtmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.comtmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.comtmeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='comtmeth' type='list' style="width:220px;"
<% 
	if((new Byte((sv.comtmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.comtmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.comtmeth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Commutation method, Joint Life"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"comtmthj"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("comtmthj");
	optionValue = makeDropDownList( mappedItems , sv.comtmthj.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.comtmthj.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.comtmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.comtmthj).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='comtmthj' type='list' style="width:220px;"
<% 
	if((new Byte((sv.comtmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.comtmthj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.comtmthj).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
                
              </div>
              </div>
            </div>
            
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Vesting Calculation method"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"vcalcmth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("vcalcmth");
	optionValue = makeDropDownList( mappedItems , sv.vcalcmth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.vcalcmth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.vcalcmth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.vcalcmth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='vcalcmth' type='list' style="width:220px;"
<% 
	if((new Byte((sv.vcalcmth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.vcalcmth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.vcalcmth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Vesting Calculation method, Joint Life"))%></label>
        		</div>
        	 </div>		
        	 
        	 <div class="col-md-2"></div>
        	 
        	 <div class="col-md-4">
                <div class="form-group">
                <%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"vclcmthj"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("vclcmthj");
	optionValue = makeDropDownList( mappedItems , sv.vclcmthj.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.vclcmthj.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.vclcmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.vclcmthj).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='vclcmthj' type='list' style="width:220px;"
<% 
	if((new Byte((sv.vclcmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.vclcmthj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.vclcmthj).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
              </div>
              </div>
            </div>
            
         </div>
         </div>
            
              
                

















<%-- 
<div class='outerDiv'>
<table>

<tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>
<style>
@media \0screen\,screen\9
{
.output_cell{margin-right:2px}
}
</style>
<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><br/><table>

<tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Dates effective")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


&nbsp;


<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("to")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr></table><table>
<!-- ILIFE-3595 STARTS-->
<tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Annuity Type")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"annuty"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("annuty");
	optionValue = makeDropDownList( mappedItems , sv.annuty.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.annuty.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.annuty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.annuty).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='annuty' type='list' style="width:140px;"
<% 
	if((new Byte((sv.annuty).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.annuty).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.annuty).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr>
<!-- ILIFE-3595 -ENDS-->
<tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Default guaranteed period")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.guarperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='guarperd' 
type='text'

<%if((sv.guarperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.guarperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.guarperd) %>'
	 <%}%>

size='<%= sv.guarperd.getLength()%>'
maxLength='<%= sv.guarperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(guarperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.guarperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.guarperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.guarperd).getColor()== null  ? 
			"input_cell" :  (sv.guarperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>




<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("years")%>
</div>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Default percentage decrease on death of nominated life")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dthpercn).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='dthpercn' 
type='text'

<%if((sv.dthpercn).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthpercn);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthpercn) %>'
	 <%}%>

size='<%= sv.dthpercn.getLength()%>'
maxLength='<%= sv.dthpercn.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthpercn)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthpercn).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthpercn).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthpercn).getColor()== null  ? 
			"input_cell" :  (sv.dthpercn).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Default percentage decrease on death of other life")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dthperco).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='dthperco' 
type='text'

<%if((sv.dthperco).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dthperco);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dthperco) %>'
	 <%}%>

size='<%= sv.dthperco.getLength()%>'
maxLength='<%= sv.dthperco.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dthperco)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.dthperco).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dthperco).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dthperco).getColor()== null  ? 
			"input_cell" :  (sv.dthperco).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Default frequency of annuity payments")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqann"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqann");
	optionValue = makeDropDownList( mappedItems , sv.freqann.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqann.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqann).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqann).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqann' type='list' style="width:140px;"
<% 
	if((new Byte((sv.freqann).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqann).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqann).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Certificate of Existence Frequency")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"frequency"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("frequency");
	optionValue = makeDropDownList( mappedItems , sv.frequency.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.frequency.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.frequency).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='frequency' type='list' style="width:140px;"
<% 
	if((new Byte((sv.frequency).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.frequency).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.frequency).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Reversionary bonus method")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"revBonusMeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("revBonusMeth");
	optionValue = makeDropDownList( mappedItems , sv.revBonusMeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.revBonusMeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.revBonusMeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.revBonusMeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='revBonusMeth' type='list' style="width:140px;"
<% 
	if((new Byte((sv.revBonusMeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.revBonusMeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.revBonusMeth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus allocation (P/C)")%>
</div>



</td><td width='251'></td><td width='251'>

<%	
	if ((new Byte((sv.bonalloc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("P")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Policy Anniversary");
	}
	if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("C")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Company Fixed Date");
	}
	 
%>

<%	
	if((new Byte((sv.bonalloc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
   				
   			<%} %>
	   		
	   </div>

<%
longValue = null;
%>

	<% }else { %>
	
<% if("red".equals((sv.bonalloc).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='bonalloc' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(bonalloc)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.bonalloc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.bonalloc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="P"<% if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("P")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Policy Anniversary")%></option>
<option value="C"<% if(((sv.bonalloc.getFormData()).toString()).trim().equalsIgnoreCase("C")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Company Fixed Date")%></option>
		
		
</select>
<% if("red".equals((sv.bonalloc).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>




</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("New coverage after vesting")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"ncovvest"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("ncovvest");
	optionValue = makeDropDownList( mappedItems , sv.ncovvest.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.ncovvest.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.ncovvest).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.ncovvest).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='ncovvest' type='list' style="width:140px;"
<% 
	if((new Byte((sv.ncovvest).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.ncovvest).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.ncovvest).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Early vesting lead years")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.evstperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='evstperd' 
type='text'

<%if((sv.evstperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.evstperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.evstperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.evstperd) %>'
	 <%}%>

size='<%= sv.evstperd.getLength()%>'
maxLength='<%= sv.evstperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(evstperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.evstperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.evstperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.evstperd).getColor()== null  ? 
			"input_cell" :  (sv.evstperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Late vesting lag years")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.lvstperd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='lvstperd' 
type='text'

<%if((sv.lvstperd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.lvstperd) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.lvstperd);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.lvstperd) %>'
	 <%}%>

size='<%= sv.lvstperd.getLength()%>'
maxLength='<%= sv.lvstperd.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(lvstperd)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.lvstperd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.lvstperd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.lvstperd).getColor()== null  ? 
			"input_cell" :  (sv.lvstperd).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Maximum percentage for commuting to a lump sum")%>
</div>



</td><td width='251'></td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.comtperc).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
			
	%>

<input name='comtperc' 
type='text'

<%if((sv.comtperc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.comtperc) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.comtperc);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.comtperc) %>'
	 <%}%>

size='<%= sv.comtperc.getLength()%>'
maxLength='<%= sv.comtperc.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(comtperc)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.comtperc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.comtperc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.comtperc).getColor()== null  ? 
			"input_cell" :  (sv.comtperc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Commutation method")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"comtmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("comtmeth");
	optionValue = makeDropDownList( mappedItems , sv.comtmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.comtmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.comtmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.comtmeth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='comtmeth' type='list' style="width:140px;"
<% 
	if((new Byte((sv.comtmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.comtmeth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.comtmeth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Commutation method, Joint Life")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"comtmthj"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("comtmthj");
	optionValue = makeDropDownList( mappedItems , sv.comtmthj.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.comtmthj.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.comtmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.comtmthj).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='comtmthj' type='list' style="width:140px;"
<% 
	if((new Byte((sv.comtmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.comtmthj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.comtmthj).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Vesting Calculation method")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"vcalcmth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("vcalcmth");
	optionValue = makeDropDownList( mappedItems , sv.vcalcmth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.vcalcmth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.vcalcmth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.vcalcmth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='vcalcmth' type='list' style="width:140px;"
<% 
	if((new Byte((sv.vcalcmth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.vcalcmth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.vcalcmth).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</tr><tr style='height:20px;'><td width='251'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Vesting Calculation method, Joint Life")%>
</div>



</td><td width='251'></td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"vclcmthj"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("vclcmthj");
	optionValue = makeDropDownList( mappedItems , sv.vclcmthj.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.vclcmthj.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.vclcmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.vclcmthj).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='vclcmthj' type='list' style="width:140px;"
<% 
	if((new Byte((sv.vclcmthj).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.vclcmthj).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.vclcmthj).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
</tr></table><br/><div style='visibility:hidden;'><table>
<tr style='height:20px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("")%>
</div>

</tr></table></div><br/></div> --%>


<%@ include file="/POLACommon2NEW.jsp"%>
