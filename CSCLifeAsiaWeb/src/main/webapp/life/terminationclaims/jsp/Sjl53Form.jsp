<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sjl53";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sjl53ScreenVars sv = (Sjl53ScreenVars) fw.getVariables();
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div style="width: 150px;" class="form-group">
					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Payment# From")%></label>
		        	<input name='paymentFrom' type='text' size='9' 
		        		<%
			        		formatValue = (sv.paymentFrom.getFormData()).toString();
			        	%>
			        		value='<%=formatValue%>'
			        	<%
			        		if (formatValue != null && formatValue.trim().length() > 0) {
			        	%>
			        		title='<%=formatValue%>' <%}%>
			        		size='<%=sv.paymentFrom.getLength()%>'
			        		maxLength='<%=sv.paymentFrom.getLength()%>' onFocus='doFocus(this)'
			        		onHelp='return fieldHelp(paymentFrom)'
			        		onKeyUp='return checkMaxLength(this)'
			        	<%
			        		if ((new Byte((sv.paymentFrom).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
			        	%>
			        		readonly="true" class="output_cell"
			        	<%
			        		} else if ((new Byte((sv.paymentFrom).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
			        	%>
			        		class="bold_cell" 
			        	<%
			        		} else {
			        	%>
			        		class=' <%=(sv.paymentFrom).getColor() == null ? "input_cell"
									: (sv.paymentFrom).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
			        	<%}%>
		        	/>
      		</div>
      	</div>
      	
      	<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Date")%></label>
					<% if ((new Byte((sv.paymentDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                             <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.paymentDateDisp)%>                                       
                             </div>
               		<%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="paymentDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.paymentDateDisp, (sv.paymentDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
             		 <%}%>	
      			</div>
      		</div>
      		
      		
				<div class="col-md-3"> 
				<div >
				      
                     <label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
				    	<table>
				    	<tr>
				    	<td>
				    	<div class="form-group">
						<%
				          if ((new Byte((sv.payee).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>                   
						<div style="margin-right: 2px">	<%=smartHF.getHTMLVarExt(fw, sv.payee)%></div>			
				        <%
							} else {
						%>
				        <div class="input-group" style="width: 125px;">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.payee)%>
				             <span class="input-group-btn">
				             <button class="btn btn-info"
				                type="button"
				                onClick="doFocus(document.getElementById('payee')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
				        <%
				         }
				        %>							
							</div>	
								</td>
								<td>								
								<%
								longValue = null;
								formatValue = null;
								%>
								<div
									class='<%= (sv.payeeName.getFormData()).trim().length() == 0 ?
													"blank_cell" : "output_cell" %>' style='margin-left:-1px;width: 133px;'>
								<%
								if(!((sv.payeeName.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payeeName.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											} else  {
								
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.payeeName.getFormData()).toString());
													} else {
														formatValue = formatValue( longValue);
													}
											}%>
										<%=formatValue%>
									</div>
								<%
								longValue = null;
								formatValue = null;
								%>
				    			</td>
				    			</tr>
				    			</table>
				    </div>
				    </div>
      		
      		
      		
      			
      		</div>
      		
      		
		<div class="row">

      		<div class="col-md-3">
      				<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
				<div class="input-group" >
						<%
							if ((new Byte((sv.paymentMethod).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"paymentMethod"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("paymentMethod");
								optionValue = makeDropDownList(mappedItems, sv.paymentMethod.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.paymentMethod.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.paymentMethod, fw, longValue, "paymentMethod", optionValue)%>
						<%
							}
						%>
					</div>
			</div>
			<div class="col-md-3">
			<div class="form-group">
				<label><div class='label_txt' style='position:relative; bottom:3px;'><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></div></label>
					<div class="input-group" >
						<%
							if ((new Byte((sv.bankcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"bankcode"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("bankcode");
								optionValue = makeDropDownList(mappedItems, sv.bankcode.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.bankcode.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.bankcode, fw, longValue, "bankcode", optionValue)%>
						<%
							}
						%>
					</div>
			</div>
		</div>
		
			<div class="col-md-3">
      				<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Status")%></label>
				<div class="input-group" >
						<%
							if ((new Byte((sv.paymentStatus).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[]{"paymentStatus"}, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("paymentStatus");
								optionValue = makeDropDownList(mappedItems, sv.paymentStatus.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.paymentStatus.getFormData()).toString().trim());
						%>
						<%=smartHF.getDropDownExt(sv.paymentStatus, fw, longValue, "paymentStatus", optionValue)%>
						<%
							}
						%>
					</div>
			</div>
				<div class="col-md-3" > 
			    	     <div class="form-group">
                      

 					 
                        <table>
                        <tr>
                        <td style="width: 130px;" class="form-group">	
                         <label><%=resourceBundleHandler.gettingValueFromBundle("Amount From /")%></label>						
							<%=smartHF.getHTMLVarExt(fw, sv.amountFrom, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
						</td>
												
						<td style="width: 130px;" class="form-group">	
						<label><%=resourceBundleHandler.gettingValueFromBundle("To")%></label> 					
							<%=smartHF.getHTMLVarExt(fw, sv.amountTo, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
						</td>
        
						</tr>
						</table>

</div></div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table style="table-layout: fixed;"
							class="table table-striped table-bordered table-hover"
							id='Sjl53Table' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center; width: 50px !important"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
									<th style="text-align: center; width: 120px"><%=resourceBundleHandler.gettingValueFromBundle("Payment#")%></th>
									<th style="text-align: center; width: 120px"><%=resourceBundleHandler.gettingValueFromBundle("Payment Date")%></th>
									<th style="text-align: center; width: 270px"><%=resourceBundleHandler.gettingValueFromBundle("Payee 1")%></th>
									<th style="text-align: center; width: 137px"><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></th>
									<th style="text-align: center; width: 137px"><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></th>
									<th style="text-align: center; width: 137px"><%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%></th>
									<th style="text-align: center; width: 220px"><%=resourceBundleHandler.gettingValueFromBundle("Payment Status")%></th>
								</tr>
							</thead>
							<%
								GeneralTable sfl = fw.getTable("Sjl53screensfl");
								GeneralTable sfl1 = fw.getTable("Sjl53screensfl");
								
							%>
							<script language="javascript">
							        $(document).ready(function(){
										var rows = <%=sfl1.count() + 1%>;
										var isPageDown = 1;
										var pageSize = 1;
										var headerRowCount=1;
										var fields = new Array();
										fields[0] = "slt";
								<%if (false) {%>	
									operateTableForSuperTableNEW(rows,isPageDown,pageSize,fields,"Sjl53Table",null,headerRowCount);
								<%}%>	
							
							        });
							    </script>
							<tbody>
							<%
								Sjl53screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (Sjl53screensfl.hasMoreScreenRows(sfl)) {
								hyperLinkFlag = true;
							%>

							<tr id='tr<%=count%>'>
							
							<%
										if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>


									<%
										if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>

									<%
										} else {
									%>

									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.select.getLength()%>'
											value='<%=sv.select.getFormData()%>'
											size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(Sjl53screensfl.select)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="Sjl53screensfl" + "." + "select" + "_R" + count%>'
											id='<%="Sjl53screensfl" + "." + "select" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.select.getLength() * 12%> px;">

									</div>
									<%
										}
									%>
									<%
										}
									%>
								 <td style="text-align: center; width: 50px !important" <%if(!(((BaseScreenData)sv.seqenum) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          					<%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.seqenum, "Sjl53screensfl", count, sfl, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
              					</td>
              					
								<td style="width: 150px;"
										<%if (!(((BaseScreenData) sv.paymentNum) instanceof StringBase)) {%>
										align="left" <%} else {%> align="left" <%}%>>
										<%
											if ((new Byte((sv.paymentNum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0
														|| (new Byte((sv.paymentNum).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
													hyperLinkFlag = false;
												}
										%> <%
 	if ((new Byte((sv.paymentNum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (hyperLinkFlag) {
 %> <a href="javascript:;" class='tableLink'
										onClick='document.getElementById("<%="Sjl53screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.paymentNum.getFormData()%></span></a>
										<%
											} else {
										%> <a href="javascript:;" class='tableLink'><span><%=sv.paymentNum.getFormData()%></span></a>
										<%
											}
										%> <%
 	}
 %>
									</td>

									<td style="text-align: left">
										<%= sv.paymtDateDisp.getFormData()%>		
									</td>
									
									<td>
										<%if((new Byte((sv.clntId).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.clntId.getFormData();
												%>
										<div id="Sjl53screensfl.clntId_R<%=count%>"
											name="Sjl53screensfl.clntId_R<%=count%>"
											<%if (!(((BaseScreenData) sv.clntId) instanceof StringBase)) {%>
													align="left" <%} else {%> align="left" <%}%>>
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="padding: 5px;"
										<%if (!(((BaseScreenData) sv.amount) instanceof StringBase)) {%>
										align="right" <%} else {%> align="right" <%}%>><%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.amount, "Sjl53screensfl", count, sfl,
							COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER)%>
									</td>
									<%-- <td>
									
										<%if((new Byte((sv.amount).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.amount.getFormData();
												%>
										<div id="Sjl53screensfl.amount_R<%=count%>"
											name="Sjl53screensfl.amount_R<%=count%>">
											<%=<%=smartHF.getHTMLVarExt(fw, sv.amount, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td> --%>
									<td>
										<%if((new Byte((sv.paymtMethod).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.paymtMethod.getFormData();
												%>
										<div id="Sjl53screensfl.paymtMethod_R<%=count%>"
											name="Sjl53screensfl.paymtMethod_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td>
										<%
											if((new Byte((sv.bankAccount).getInvisible()))
																			.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){
										%> <%
 	longValue=sv.bankAccount.getFormData();
 %>
										<div id="Sjl53screensfl.bankAccount_R<%=count%>"
											name="Sjl53screensfl.bankAccount_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>
									<td style="width: 200px !important">
										<%if((new Byte((sv.paymtStatus).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%> <%
													longValue=sv.paymtStatus.getFormData();
												%>
										<div id="Sjl53screensfl.paymtStatus_R<%=count%>"
											name="Sjl53screensfl.paymtStatus_R<%=count%>">
											<%=longValue%>
										</div> <%
													longValue = null;
												%> <%}%>
									</td>

								</tr>

								<%
									count = count + 1;
									Sjl53screensfl
									.setNextScreenRow(sfl, appVars, sv);
									}
									%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	
		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	     </div>
	     <input type="hidden" id="totalRecords" value="<%=count-1%>" />
	
</div>
</div>
<script>
$(document).ready(function() {
	var showval= document.getElementById('show_lbl').value;
	var toval= document.getElementById('to_lbl').value;
	var ofval= document.getElementById('of_lbl').value;
	var entriesval= document.getElementById('entries_lbl').value;
	var nextval= document.getElementById('nxtbtn_lbl').value;
	var previousval= document.getElementById('prebtn_lbl').value;
	var dtmessage =  document.getElementById('msg_lbl').value;
	$('#Sjl53Table').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '400',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,     
            "EmptyTable":"No data available in table",	
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "paginate": {                
                "next":       nextval,
                "previous":   previousval
            }	
          }    

	});
})
</script>		
<%@ include file="/POLACommon2NEW.jsp"%>	
		