<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5184";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5184ScreenVars sv = (S5184ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Register Payment  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Adjust Payment    ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"C - Approve Payment   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"D - Reverse Payment   ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"E - Terminate Payment ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F - Enquire on Payment");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No     ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action  ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
		<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
		       		<div class="input-group" style=min-width:130px;>
		       		<input name='chdrsel' id='chdrsel'
					type='text' 
					value='<%=sv.chdrsel.getFormData()%>' 
					maxLength='<%=sv.chdrsel.getLength()%>' 
					size='<%=sv.chdrsel.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.chdrsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.chdrsel).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					 <span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
										
					<%
						}else { 
					%>
					
					class = ' <%=(sv.chdrsel).getColor()== null  ? 
					"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					<span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
												
										
					<%} %>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	 </div>
</div>

<div class="panel panel-default">
		<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Action")%>
         </div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Register Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Adjust Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Approve Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Reverse Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Terminate Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="F"><%= smartHF.buildRadioOption(sv.action, "action", "F")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Enquire on Payment")%></label> 
					</div>
		       		</div>
		       	</div>
		       	<input name='action' 
				type='hidden'
				value='<%=sv.action.getFormData()%>'
				size='<%=sv.action.getLength()%>'
				maxLength='<%=sv.action.getLength()%>' 
				class = "input_cell"
				onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
		    </div>
	 </div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
