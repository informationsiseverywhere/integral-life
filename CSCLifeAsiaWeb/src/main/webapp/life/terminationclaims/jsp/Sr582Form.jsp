

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SR582";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sr582ScreenVars sv = (Sr582ScreenVars) fw.getVariables();%>
<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Payable Amount  ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  No ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Number ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Diagnosis ");%>
<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RiskCommDt   ");%>
<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Admission ");%>
<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Discharge ");%>
<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accident Dte ");%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Doctor    ");%>
<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"S/A ");%>
<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Hospital  ");%>
<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1-Create  2-Modify  3-Delete  4-Enquiry");%>
<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"     -Maximum Benefit-   -Actual Incurr-Double");%>
<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sl Bnft  Bnft Desc        Amnt/Unit Fq Unts   Incurr Amt Unts Indm Payable Amt");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind41.isOn()) {
			sv.diagcde.setReverse(BaseScreenData.REVERSED);
			sv.diagcde.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.diagcde.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind41.isOn()) {
			sv.diagcde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.gcadmdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.gcadmdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.gcadmdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind42.isOn()) {
			sv.gcadmdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.dischdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.dischdtDisp.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.dischdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind43.isOn()) {
			sv.dischdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
			sv.zdoctor.setColor(BaseScreenData.RED);
		}
		if (appVars.ind50.isOn()) {
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind44.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			sv.zmedprv.setReverse(BaseScreenData.REVERSED);
			sv.zmedprv.setColor(BaseScreenData.RED);
		}
		if (appVars.ind37.isOn()) {
			sv.zmedprv.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind40.isOn()) {
			sv.zmedprv.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract  No")%></label>
					<%}%>
					<div>

						<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
							} else  {
									
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
						%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
							style="width: 120px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
								longValue = null;
								formatValue = null;
								%>
						<%}%>
					</div>
				</div>
			</div>
			<div class="col-md-5"></div>
			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Rider")%>
					</label>
					<%}%>
					<div class="input-group">
						<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
							if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
						<!--  </div>
					<div> -->

						<%if ((new Byte((sv.crtabled).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
								if(!((sv.crtabled.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.crtabled.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.crtabled.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style="max-width: 400px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
					</div>

				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<%}%>
					<div class="input-group">

						<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
							if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>
						<%}%>





						<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
							if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="max-width: 400px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>

					</div>
				</div>
			</div>
			<div class="col-md-4" style="margin-left: 78px;">
				<div class="form-group">
					<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Claim Number")%>
					<%-- <label> <%=resourceBundleHandler.gettingValueFromBundle("Diagnosis")%> --%>
					</label>
					<%}%>
					<div>


						<%if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
							if(!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>

					</div>

				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Diagnosis")%>
					<%-- <label> <%=resourceBundleHandler.gettingValueFromBundle("Claim Number")%> --%>
					</label>
					<%}%>
					<div>
						<%	
								if ((new Byte((sv.diagcde).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"diagcde"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("diagcde");
								optionValue = makeDropDownList( mappedItems , sv.diagcde.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.diagcde.getFormData()).toString().trim());  
							%>

						<% 
								if((new Byte((sv.diagcde).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>
							<%if(longValue != null){%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%}%>
						</div>

						<%
							longValue = null;
							%>

						<% }else {%>

						<% if("red".equals((sv.diagcde).getColor())){
							%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
							} 
							%>

							<select name='diagcde' type='list' style="width: 205px;"
								<% 
								if((new Byte((sv.diagcde).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>
								readonly="true" disabled class="output_cell"
								<%
								}else if((new Byte((sv.diagcde).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>
								class="bold_cell" <%
								}else { 
							%>
								class='input_cell' <%
								} 
							%>>
								<%=optionValue%>
							</select>
							<% if("red".equals((sv.diagcde).getColor())){
							%>
						</div>
						<%
							} 
							%>

						<%
							}} 
							%>

					</div>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("RiskCommDt")%></label>
					<%}%>
					<div>

						<%if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
						if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
							style="max-width: 100px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>

					</div>
				</div>
			</div>

			<div class="col-md-4" style="margin-left: 78px;">
				<div class="form-group">

					<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Admission")%></label>
					<%}%>
					<div class="input-group">


						<%
								if((new Byte((sv.gcadmdtDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
						<%=smartHF.getRichTextDateInput(fw, sv.gcadmdtDisp,(sv.gcadmdtDisp.getLength()))%>
						<%
								}else{
							%>
						<div id="occdatePicker"
							class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.gcadmdtDisp,(sv.gcadmdtDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<%
								}
							%>

					</div>
				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discharge")%></label>
					<%}%>
					<div class="input-group">



						<%
								if((new Byte((sv.dischdtDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){
							%>
						<%=smartHF.getRichTextDateInput(fw, sv.dischdtDisp,(sv.dischdtDisp.getLength()))%>
						<%
								}else{
							%>
						<div id="dischdatePicker"
							class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.dischdtDisp,(sv.dischdtDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<%
								}
							%>


					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accident Dte")%></label>
					<%}%>
					<div>
						<%if ((new Byte((sv.incurdtDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
							if(!((sv.incurdtDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.incurdtDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
							style="max-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>
						<%}%>


					</div>

				</div>

			</div>


			<div class="col-md-4" style="margin-left: 78px;">
				<div class="form-group">
					<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Doctor")%></label>
					<%}%>

					<div class="input-group">
						<%if ((new Byte((sv.zdoctor).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {%>
						<%									
										longValue = sv.zdoctor.getFormData();  
									%>

						<% 
										if((new Byte((sv.zdoctor).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
																%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>
							<%if(longValue != null){%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%}%>
						</div>

						<%
									longValue = null;
									%>
						<% }else {%>
						<input name='zdoctor' type='text' id="zdoctor"
							value='<%=sv.zdoctor.getFormData()%>'
							maxLength='<%=sv.zdoctor.getLength()%>'
							size='<%=sv.zdoctor.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zdoctor)'
							onKeyUp='return checkMaxLength(this)'
							<% 
										if((new Byte((sv.zdoctor).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>
							readonly="true" class="output_cell">

						<%
										}else if((new Byte((sv.zdoctor).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										
									%>
						class="bold_cell" > 
						
						<span class="input-group-btn">
							<button class="btn btn-info" style="font-size: 19px;"  type="button" onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
      					</span> 
						<!-- <span class="input-group-btn">
							<button class="btn btn-info" id="zdoctor"
								style="font-size: 19px; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span> -->
						<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('zdoctor')); changeF4Image(this); doAction('PFKEY04')"> 
									<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
									</a>
									 --%>
						<%
										}else { 
									%>

						class = '
						<%=(sv.zdoctor).getColor()== null  ? 
									"input_cell" :  (sv.zdoctor).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>' >
									
									<span class="input-group-btn">
										<button class="btn btn-info" style="font-size: 19px;"  type="button" onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
										<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
      								</span> 
									 <!-- <span
							class="input-group-btn">
							<button class="btn btn-info" id="zdoctor"
								style="font-size: 19px; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span> -->
						<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('zdoctor')); changeF4Image(this); doAction('PFKEY04')"> 
									<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
									</a> --%>

						<%}longValue = null;}} %>






						<%if ((new Byte((sv.givname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
		if(!((sv.givname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.givname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.givname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
							style="width: 130px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
		longValue = null;
		formatValue = null;
		%>
						<%}%>




					</div>


				</div>

			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("S/A")%></label>
					<%}%>

					<div style="width: 168px;">



						<%if ((new Byte((sv.zrsumin).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%	
					qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
				formatValue = smartHF.getPicFormatted(qpsf,sv.zrsumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
				if(!((sv.zrsumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
						if(longValue == null || longValue.equalsIgnoreCase("")) { 			
						formatValue = formatValue( formatValue );
						} else {
						formatValue = formatValue( longValue );
						}
				}
		
			if(!formatValue.trim().equalsIgnoreCase("")) {
			%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
				} else {
			%>

						<div class="blank_cell">&nbsp;</div>

						<% 
				} 
			%>
						<%
			longValue = null;
			formatValue = null;
			%>

						<%}%>
					</div>


				</div>

			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Hospital")%></label>
					<%}%>

					<div class="input-group">

						<%if ((new Byte((sv.zmedprv).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

						<%	
						
						longValue = sv.zmedprv.getFormData();  
					%>

						<% 
						if((new Byte((sv.zmedprv).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>
						<div
							class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
							<%if(longValue != null){%>

							<%=XSSFilter.escapeHtml(longValue)%>

							<%}%>
						</div>

						<%
					longValue = null;
					%>
						<% }else {%>
						<input name='zmedprv' type='text' id="zmedprv"
							value='<%=sv.zmedprv.getFormData()%>'
							maxLength='<%=sv.zmedprv.getLength()%>'
							size='<%=sv.zmedprv.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(zmedprv)'
							onKeyUp='return checkMaxLength(this)'
							<% 
							if((new Byte((sv.zmedprv).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
						%>
							readonly="true" class="output_cell">

						<%
							}else if((new Byte((sv.zmedprv).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
						%>
						class="bold_cell" >
						 <span class="input-group-btn">
							<button class="btn btn-info" 
								style="font-size: 19px; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('zmedprv'));doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('zmedprv')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

						<%
	}else { 
%>

						class = '
						<%=(sv.zmedprv).getColor()== null  ? 
					"input_cell" :  (sv.zmedprv).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					 <span
							class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 19px; border-bottom-width: 2px !important;"
								type="button"
								onClick="doFocus(document.getElementById('zmedprv')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

						<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('zmedprv')); changeF4Image(this); doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a> --%>

						<%}longValue = null;}} %>






						<%if ((new Byte((sv.cdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<%					
					if(!((sv.cdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
							style="width: 139px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>
						<%}%>
					</div>
				</div>
			</div>
		</div>

		<!-- 	</div> -->




		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr582' width='100%'>

							<tr class='info'>

								<td rowspan="2" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></td>
								<td rowspan="2" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></td>
								<td rowspan="2" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></td>
								<td colspan="3" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Maximum Benefit")%></td>
								<td colspan="4" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Actual Incurred Double")%></td>
							</tr>
							<tr class='info'>
								<td align="center"><%=resourceBundleHandler.gettingValueFromBundle("Amount/Unit")%></td>
								<td align="center">
									<!--<%=resourceBundleHandler.gettingValueFromBundle("Header5")%>--><%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
								</td>
								<td align="center">
									<!-- <%=resourceBundleHandler.gettingValueFromBundle("Header6")%> --><%=resourceBundleHandler.gettingValueFromBundle("Units")%>
								</td>
								<td><%=resourceBundleHandler.gettingValueFromBundle("Incurred Amount")%></td>
								<td align="center">
									<!-- <%=resourceBundleHandler.gettingValueFromBundle("Header8")%> --><%=resourceBundleHandler.gettingValueFromBundle("Units")%>
								</td>
								<td align="center">
									<!-- <%=resourceBundleHandler.gettingValueFromBundle("Header9")%> --><%=resourceBundleHandler.gettingValueFromBundle("Indemnity")%>
								</td>
								<td align="center"><%=resourceBundleHandler.gettingValueFromBundle("Payable Amount")%></td>
							</tr>

							<tbody>

								<%
		GeneralTable sfl = fw.getTable("sr582screensfl");
		int height;
		//ILIFE-1821 starts
		if(sfl.count()*27 > 100) {
		height = 100 ;
		//ILIFE-1821 ends
		} else {
		height = sfl.count()*27;
		}	
		%>

								<%
	Sr582screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr582screensfl
	.hasMoreScreenRows(sfl)) {	
%>

								<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[10];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.sel.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = (sv.sel.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.acdben.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*12;								
			} else {		
				calculatedValue = (sv.acdben.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.bendesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
			} else {		
				calculatedValue = (sv.bendesc.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.amtaout.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
			} else {		
				calculatedValue = (sv.amtaout.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.benfreq.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.benfreq.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.mxbenunt.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
			} else {		
				calculatedValue = (sv.mxbenunt.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.actexp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
			} else {		
				calculatedValue = (sv.actexp.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.acbenunt.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*12;								
			} else {		
				calculatedValue = (sv.acbenunt.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[7]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.gcdblind.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*12;								
			} else {		
				calculatedValue = (sv.gcdblind.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[8]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header10").length() >= (sv.gcnetpy.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header10").length())*12;								
			} else {		
				calculatedValue = (sv.gcnetpy.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[9]= calculatedValue;
			%>

								<tr class="tableRowTag" id='<%="tablerow"+count%>'>
									<%if((new Byte((sv.sel).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0 ]%>px;"
										<%if((sv.sel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><input
										type="checkbox" value='<%= sv.sel.getFormData() %>'
										onFocus='doFocus(this)'
										onHelp='return fieldHelp("sr582screensfl" + "." +
						 "sel")'
										onKeyUp='return checkMaxLength(this)'
										name='sr582screensfl.sel_R<%=count%>'
										id='sr582screensfl.sel_R<%=count%>'
										onClick="selectedRow('sr582screensfl.sel_R<%=count%>')"
										class="UICheck" /></td>
									<%}else{%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[0 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.acdben).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[1 ]%>px;"
										<%if((sv.acdben).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><%= sv.acdben.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[1 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.bendesc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[2 ]%>px;"
										<%if((sv.bendesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><%= sv.bendesc.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag tableDataTagFixed"
										style="width:<%=tblColumnWidth[2 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.amtaout).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[3 ]%>px;"
										<%if((sv.amtaout).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>>
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.amtaout).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%> <%
							formatValue = smartHF.getPicFormatted(qpsf,sv.amtaout,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.amtaout).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%> <%= formatValue%> <%
								longValue = null;
								formatValue = null;
						%>




									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[3 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.benfreq).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[4 ]%>px;"
										<%if((sv.benfreq).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="right" <%}%>><%= sv.benfreq.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[4 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.mxbenunt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[5 ]%>px;"
										<%if((sv.mxbenunt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><%= sv.mxbenunt.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[5 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.actexp).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[6 ]%>px;"
										<%if((sv.actexp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>>
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.actexp).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%> <%
							formatValue = smartHF.getPicFormatted(qpsf,sv.actexp,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.actexp).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%> <%= formatValue%> <%
								longValue = null;
								formatValue = null;
						%>




									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[6 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.acbenunt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[7 ]%>px;"
										<%if((sv.acbenunt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><%= sv.acbenunt.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[7 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.gcdblind).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[8 ]%>px;"
										<%if((sv.gcdblind).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>><%= sv.gcdblind.getFormData()%>



									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[8 ]%>px;"></td>

									<%}%>
									<%if((new Byte((sv.gcnetpy).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[9 ]%>px;"
										<%if((sv.gcnetpy).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <% }else {%> align="left" <%}%>>
										<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.gcnetpy).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
					%> <%
							formatValue = smartHF.getPicFormatted(qpsf,sv.gcnetpy,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.gcnetpy).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%> <%= formatValue%> <%
								longValue = null;
								formatValue = null;
						%>




									</td>
									<%}else{%>
									<td class="tableDataTag"
										style="width:<%=tblColumnWidth[9 ]%>px;"></td>

									<%}%>

								</tr>

								<%
	count = count + 1;
	Sr582screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>


							</tbody>
						</table>
					</div>

					<div>
						<table>
							<tr>
								<td>
									<div class="sectionbutton" style="margin: 2px;">
										<a href="#" onClick="JavaScript:perFormOperation(1)"
											class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Create")%></a>
									</div>
								</td>
								<td>
									<div class="sectionbutton" style="margin: 2px;">
										<a href="#" onClick="JavaScript:perFormOperation(2)"
											class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Modify")%></a>
									</div>
								</td>
								<td>
									<div class="sectionbutton" style="margin: 2px;">
										<a href="#" onClick="JavaScript:perFormOperation(3)"
											class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
									</div>
								</td>
								<td>
									<div class="sectionbutton" style="margin: 2px;">
										<a href="#" onClick="JavaScript:perFormOperation(4)"
											class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Enquiry")%></a>
									</div>
								</td>
							</tr>
						</table>
					</div>



				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-md-3" style="float: right;">
				<div class="form-group">
					<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) { %>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Payable Amount")%>
					</label>
					<%}%>


					<!-- <br/> -->

					<%if ((new Byte((sv.tclmamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


					<%	
			qpsf = fw.getFieldXMLDef((sv.tclmamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tclmamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.tclmamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
					<div class="output_cell" style="text-align: right">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
			} else {
		%>

					<div class="blank_cell">&nbsp;</div>

					<% 
			} 
		%>
					<%
		longValue = null;
		formatValue = null;
		%>

					<%}%>


				</div>
			</div>


			<%-- 
		<div class="col-md-3">
			<div class="form-group">

<%if ((new Byte((sv.shortdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.shortdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.shortdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

</div>
</div> --%>
		</div>


	</div>
</div>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr582Table", {
				headerRows:2,
				fixedCols : 0,					
				colWidths : [50,80,300,150,100,100,150,100,100,150],
				hasHorizonScroll :"Y",
				moreBtn: "N",	
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<%@ include file="/POLACommon2NEW.jsp"%>