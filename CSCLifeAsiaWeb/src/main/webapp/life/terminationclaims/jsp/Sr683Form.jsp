<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR683";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr683ScreenVars sv = (Sr683ScreenVars) fw.getVariables();%>

<%if (sv.Sr683screenWritten.gt(0)) {%>
	<%Sr683screen.clearClassString(sv);%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component Code   ");%>
<%	generatedText4.appendClassString("label_txt");
	generatedText4.appendClassString("highlight");
%>
	<%sv.crtable.setClassString("");%>
<%	sv.crtable.appendClassString("string_fld");
	sv.crtable.appendClassString("output_txt");
	sv.crtable.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Code  ");%>
<%	generatedText5.appendClassString("label_txt");
	generatedText5.appendClassString("highlight");
%>
	<%sv.mortcls.setClassString("");%>
<%	sv.mortcls.appendClassString("string_fld");
	sv.mortcls.appendClassString("output_txt");
	sv.mortcls.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective  ");%>
<%	generatedText11.appendClassString("label_txt");
	generatedText11.appendClassString("highlight");
%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
<%	generatedText12.appendClassString("label_txt");
	generatedText12.appendClassString("highlight");
%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefits");%>
<%	generatedText2.appendClassString("label_txt");
	generatedText2.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max.Benefits");%>
<%	generatedText3.appendClassString("label_txt");
	generatedText3.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  No. of Days");%>
<%	generatedText6.appendClassString("label_txt");
	generatedText6.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount Claim");%>
<%	generatedText7.appendClassString("label_txt");
	generatedText7.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(In Thousand)");%>
<%	generatedText13.appendClassString("label_txt");
	generatedText13.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(In Thousand)");%>
<%	generatedText14.appendClassString("label_txt");
	generatedText14.appendClassString("highlight");
%>
	<%sv.benefits01.setClassString("");%>
<%	sv.benefits01.appendClassString("string_fld");
	sv.benefits01.appendClassString("output_txt");
	sv.benefits01.appendClassString("highlight");
%>
	<%sv.benfamt01.setClassString("");%>
<%	sv.benfamt01.appendClassString("num_fld");
	sv.benfamt01.appendClassString("output_txt");
	sv.benfamt01.appendClassString("highlight");
%>
	<%sv.daclaim01.setClassString("");%>
<%	sv.daclaim01.appendClassString("num_fld");
	sv.daclaim01.appendClassString("input_txt");
	sv.daclaim01.appendClassString("highlight");
%>
	<%sv.claimamt01.setClassString("");%>
<%	sv.claimamt01.appendClassString("num_fld");
	sv.claimamt01.appendClassString("input_txt");
	sv.claimamt01.appendClassString("highlight");
%>
	<%sv.benefits02.setClassString("");%>
<%	sv.benefits02.appendClassString("string_fld");
	sv.benefits02.appendClassString("output_txt");
	sv.benefits02.appendClassString("highlight");
%>
	<%sv.benfamt02.setClassString("");%>
<%	sv.benfamt02.appendClassString("num_fld");
	sv.benfamt02.appendClassString("output_txt");
	sv.benfamt02.appendClassString("highlight");
%>
	<%sv.daclaim02.setClassString("");%>
<%	sv.daclaim02.appendClassString("num_fld");
	sv.daclaim02.appendClassString("input_txt");
	sv.daclaim02.appendClassString("highlight");
%>
	<%sv.claimamt02.setClassString("");%>
<%	sv.claimamt02.appendClassString("num_fld");
	sv.claimamt02.appendClassString("input_txt");
	sv.claimamt02.appendClassString("highlight");
%>
	<%sv.benefits03.setClassString("");%>
<%	sv.benefits03.appendClassString("string_fld");
	sv.benefits03.appendClassString("output_txt");
	sv.benefits03.appendClassString("highlight");
%>
	<%sv.benfamt03.setClassString("");%>
<%	sv.benfamt03.appendClassString("num_fld");
	sv.benfamt03.appendClassString("output_txt");
	sv.benfamt03.appendClassString("highlight");
%>
	<%sv.daclaim03.setClassString("");%>
<%	sv.daclaim03.appendClassString("num_fld");
	sv.daclaim03.appendClassString("input_txt");
	sv.daclaim03.appendClassString("highlight");
%>
	<%sv.claimamt03.setClassString("");%>
<%	sv.claimamt03.appendClassString("num_fld");
	sv.claimamt03.appendClassString("input_txt");
	sv.claimamt03.appendClassString("highlight");
%>
	<%sv.benefits04.setClassString("");%>
<%	sv.benefits04.appendClassString("string_fld");
	sv.benefits04.appendClassString("output_txt");
	sv.benefits04.appendClassString("highlight");
%>
	<%sv.benfamt04.setClassString("");%>
<%	sv.benfamt04.appendClassString("num_fld");
	sv.benfamt04.appendClassString("output_txt");
	sv.benfamt04.appendClassString("highlight");
%>
	<%sv.daclaim04.setClassString("");%>
<%	sv.daclaim04.appendClassString("num_fld");
	sv.daclaim04.appendClassString("input_txt");
	sv.daclaim04.appendClassString("highlight");
%>
	<%sv.claimamt04.setClassString("");%>
<%	sv.claimamt04.appendClassString("num_fld");
	sv.claimamt04.appendClassString("input_txt");
	sv.claimamt04.appendClassString("highlight");
%>
	<%sv.benefits05.setClassString("");%>
<%	sv.benefits05.appendClassString("string_fld");
	sv.benefits05.appendClassString("output_txt");
	sv.benefits05.appendClassString("highlight");
%>
	<%sv.benfamt05.setClassString("");%>
<%	sv.benfamt05.appendClassString("num_fld");
	sv.benfamt05.appendClassString("output_txt");
	sv.benfamt05.appendClassString("highlight");
%>
	<%sv.daclaim05.setClassString("");%>
<%	sv.daclaim05.appendClassString("num_fld");
	sv.daclaim05.appendClassString("input_txt");
	sv.daclaim05.appendClassString("highlight");
%>
	<%sv.claimamt05.setClassString("");%>
<%	sv.claimamt05.appendClassString("num_fld");
	sv.claimamt05.appendClassString("input_txt");
	sv.claimamt05.appendClassString("highlight");
%>
	<%sv.benefits06.setClassString("");%>
<%	sv.benefits06.appendClassString("string_fld");
	sv.benefits06.appendClassString("output_txt");
	sv.benefits06.appendClassString("highlight");
%>
	<%sv.benfamt06.setClassString("");%>
<%	sv.benfamt06.appendClassString("num_fld");
	sv.benfamt06.appendClassString("output_txt");
	sv.benfamt06.appendClassString("highlight");
%>
	<%sv.daclaim06.setClassString("");%>
<%	sv.daclaim06.appendClassString("num_fld");
	sv.daclaim06.appendClassString("input_txt");
	sv.daclaim06.appendClassString("highlight");
%>
	<%sv.claimamt06.setClassString("");%>
<%	sv.claimamt06.appendClassString("num_fld");
	sv.claimamt06.appendClassString("input_txt");
	sv.claimamt06.appendClassString("highlight");
%>
	<%sv.benefits07.setClassString("");%>
<%	sv.benefits07.appendClassString("string_fld");
	sv.benefits07.appendClassString("output_txt");
	sv.benefits07.appendClassString("highlight");
%>
	<%sv.benfamt07.setClassString("");%>
<%	sv.benfamt07.appendClassString("num_fld");
	sv.benfamt07.appendClassString("output_txt");
	sv.benfamt07.appendClassString("highlight");
%>
	<%sv.daclaim07.setClassString("");%>
<%	sv.daclaim07.appendClassString("num_fld");
	sv.daclaim07.appendClassString("input_txt");
	sv.daclaim07.appendClassString("highlight");
%>
	<%sv.claimamt07.setClassString("");%>
<%	sv.claimamt07.appendClassString("num_fld");
	sv.claimamt07.appendClassString("input_txt");
	sv.claimamt07.appendClassString("highlight");
%>
	<%sv.benefits08.setClassString("");%>
<%	sv.benefits08.appendClassString("string_fld");
	sv.benefits08.appendClassString("output_txt");
	sv.benefits08.appendClassString("highlight");
%>
	<%sv.benfamt08.setClassString("");%>
<%	sv.benfamt08.appendClassString("num_fld");
	sv.benfamt08.appendClassString("output_txt");
	sv.benfamt08.appendClassString("highlight");
%>
	<%sv.daclaim08.setClassString("");%>
<%	sv.daclaim08.appendClassString("num_fld");
	sv.daclaim08.appendClassString("input_txt");
	sv.daclaim08.appendClassString("highlight");
%>
	<%sv.claimamt08.setClassString("");%>
<%	sv.claimamt08.appendClassString("num_fld");
	sv.claimamt08.appendClassString("input_txt");
	sv.claimamt08.appendClassString("highlight");
%>
	<%sv.benefits09.setClassString("");%>
<%	sv.benefits09.appendClassString("string_fld");
	sv.benefits09.appendClassString("output_txt");
	sv.benefits09.appendClassString("highlight");
%>
	<%sv.benfamt09.setClassString("");%>
<%	sv.benfamt09.appendClassString("num_fld");
	sv.benfamt09.appendClassString("output_txt");
	sv.benfamt09.appendClassString("highlight");
%>
	<%sv.daclaim09.setClassString("");%>
<%	sv.daclaim09.appendClassString("num_fld");
	sv.daclaim09.appendClassString("input_txt");
	sv.daclaim09.appendClassString("highlight");
%>
	<%sv.claimamt09.setClassString("");%>
<%	sv.claimamt09.appendClassString("num_fld");
	sv.claimamt09.appendClassString("input_txt");
	sv.claimamt09.appendClassString("highlight");
%>
	<%sv.benefits10.setClassString("");%>
<%	sv.benefits10.appendClassString("string_fld");
	sv.benefits10.appendClassString("output_txt");
	sv.benefits10.appendClassString("highlight");
%>
	<%sv.benfamt10.setClassString("");%>
<%	sv.benfamt10.appendClassString("num_fld");
	sv.benfamt10.appendClassString("output_txt");
	sv.benfamt10.appendClassString("highlight");
%>
	<%sv.daclaim10.setClassString("");%>
<%	sv.daclaim10.appendClassString("num_fld");
	sv.daclaim10.appendClassString("input_txt");
	sv.daclaim10.appendClassString("highlight");
%>
	<%sv.claimamt10.setClassString("");%>
<%	sv.claimamt10.appendClassString("num_fld");
	sv.claimamt10.appendClassString("input_txt");
	sv.claimamt10.appendClassString("highlight");
%>
	<%sv.benefits11.setClassString("");%>
<%	sv.benefits11.appendClassString("string_fld");
	sv.benefits11.appendClassString("output_txt");
	sv.benefits11.appendClassString("highlight");
%>
	<%sv.benfamt11.setClassString("");%>
<%	sv.benfamt11.appendClassString("num_fld");
	sv.benfamt11.appendClassString("output_txt");
	sv.benfamt11.appendClassString("highlight");
%>
	<%sv.daclaim11.setClassString("");%>
<%	sv.daclaim11.appendClassString("num_fld");
	sv.daclaim11.appendClassString("input_txt");
	sv.daclaim11.appendClassString("highlight");
%>
	<%sv.claimamt11.setClassString("");%>
<%	sv.claimamt11.appendClassString("num_fld");
	sv.claimamt11.appendClassString("input_txt");
	sv.claimamt11.appendClassString("highlight");
%>
	<%sv.benefits12.setClassString("");%>
<%	sv.benefits12.appendClassString("string_fld");
	sv.benefits12.appendClassString("output_txt");
	sv.benefits12.appendClassString("highlight");
%>
	<%sv.benfamt12.setClassString("");%>
<%	sv.benfamt12.appendClassString("num_fld");
	sv.benfamt12.appendClassString("output_txt");
	sv.benfamt12.appendClassString("highlight");
%>
	<%sv.daclaim12.setClassString("");%>
<%	sv.daclaim12.appendClassString("num_fld");
	sv.daclaim12.appendClassString("input_txt");
	sv.daclaim12.appendClassString("highlight");
%>
	<%sv.claimamt12.setClassString("");%>
<%	sv.claimamt12.appendClassString("num_fld");
	sv.claimamt12.appendClassString("input_txt");
	sv.claimamt12.appendClassString("highlight");
%>
	<%sv.benefits13.setClassString("");%>
<%	sv.benefits13.appendClassString("string_fld");
	sv.benefits13.appendClassString("output_txt");
	sv.benefits13.appendClassString("highlight");
%>
	<%sv.benfamt13.setClassString("");%>
<%	sv.benfamt13.appendClassString("num_fld");
	sv.benfamt13.appendClassString("output_txt");
	sv.benfamt13.appendClassString("highlight");
%>
	<%sv.daclaim13.setClassString("");%>
<%	sv.daclaim13.appendClassString("num_fld");
	sv.daclaim13.appendClassString("input_txt");
	sv.daclaim13.appendClassString("highlight");
%>
	<%sv.claimamt13.setClassString("");%>
<%	sv.claimamt13.appendClassString("num_fld");
	sv.claimamt13.appendClassString("input_txt");
	sv.claimamt13.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"============");%>
<%	generatedText9.appendClassString("label_txt");
	generatedText9.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Amount claim  ");%>
<%	generatedText10.appendClassString("label_txt");
	generatedText10.appendClassString("highlight");
%>
	<%sv.claimtot.setClassString("");%>
<%	sv.claimtot.appendClassString("num_fld");
	sv.claimtot.appendClassString("output_txt");
	sv.claimtot.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Enter=Exit");%>
<%	generatedText8.appendClassString("label_txt");
	generatedText8.appendClassString("information_txt");
%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(3, 15, generatedText4)%>

	<%=smartHF.getHTMLSpaceVar(3, 34, fw, sv.crtable)%>
	<%=smartHF.getHTMLF4NSVar(3, 34, fw, sv.crtable)%>

	<%=smartHF.getLit(3, 57, generatedText5)%>

	<%=smartHF.getHTMLSpaceVar(3, 69, fw, sv.mortcls)%>
	<%=smartHF.getHTMLF4NSVar(3, 69, fw, sv.mortcls)%>

	<%=smartHF.getLit(4, 15, generatedText11)%>

	<%=smartHF.getHTMLSpaceVar(4, 34, fw, sv.itmfrmDisp)%>
	<%=smartHF.getHTMLCalNSVar(4, 34, fw, sv.itmfrmDisp)%>

	<%=smartHF.getLit(4, 47, generatedText12)%>

	<%=smartHF.getHTMLSpaceVar(4, 52, fw, sv.itmtoDisp)%>
	<%=smartHF.getHTMLCalNSVar(4, 52, fw, sv.itmtoDisp)%>

	<%=smartHF.getLit(6, 2, generatedText2)%>

	<%=smartHF.getLit(6, 33, generatedText3)%>

	<%=smartHF.getLit(6, 48, generatedText6)%>

	<%=smartHF.getLit(6, 65, generatedText7)%>

	<%=smartHF.getLit(7, 33, generatedText13)%>

	<%=smartHF.getLit(7, 65, generatedText14)%>

	<%=smartHF.getHTMLVar(8, 2, fw, sv.benefits01)%>

	<%=smartHF.getHTMLVar(8, 36, fw, sv.benfamt01, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(8, 53, fw, sv.daclaim01)%>

	<%=smartHF.getHTMLVar(8, 68, fw, sv.claimamt01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(9, 2, fw, sv.benefits02)%>

	<%=smartHF.getHTMLVar(9, 36, fw, sv.benfamt02, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(9, 53, fw, sv.daclaim02)%>

	<%=smartHF.getHTMLVar(9, 68, fw, sv.claimamt02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(10, 2, fw, sv.benefits03)%>

	<%=smartHF.getHTMLVar(10, 36, fw, sv.benfamt03, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(10, 53, fw, sv.daclaim03)%>

	<%=smartHF.getHTMLVar(10, 68, fw, sv.claimamt03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(11, 2, fw, sv.benefits04)%>

	<%=smartHF.getHTMLVar(11, 36, fw, sv.benfamt04, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(11, 53, fw, sv.daclaim04)%>

	<%=smartHF.getHTMLVar(11, 68, fw, sv.claimamt04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(12, 2, fw, sv.benefits05)%>

	<%=smartHF.getHTMLVar(12, 36, fw, sv.benfamt05, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(12, 53, fw, sv.daclaim05)%>

	<%=smartHF.getHTMLVar(12, 68, fw, sv.claimamt05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(13, 2, fw, sv.benefits06)%>

	<%=smartHF.getHTMLVar(13, 36, fw, sv.benfamt06, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(13, 53, fw, sv.daclaim06)%>

	<%=smartHF.getHTMLVar(13, 68, fw, sv.claimamt06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(14, 2, fw, sv.benefits07)%>

	<%=smartHF.getHTMLVar(14, 36, fw, sv.benfamt07, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(14, 53, fw, sv.daclaim07)%>

	<%=smartHF.getHTMLVar(14, 68, fw, sv.claimamt07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(15, 2, fw, sv.benefits08)%>

	<%=smartHF.getHTMLVar(15, 36, fw, sv.benfamt08, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(15, 53, fw, sv.daclaim08)%>

	<%=smartHF.getHTMLVar(15, 68, fw, sv.claimamt08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(16, 2, fw, sv.benefits09)%>

	<%=smartHF.getHTMLVar(16, 36, fw, sv.benfamt09, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(16, 53, fw, sv.daclaim09)%>

	<%=smartHF.getHTMLVar(16, 68, fw, sv.claimamt09, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(17, 2, fw, sv.benefits10)%>

	<%=smartHF.getHTMLVar(17, 36, fw, sv.benfamt10, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(17, 53, fw, sv.daclaim10)%>

	<%=smartHF.getHTMLVar(17, 68, fw, sv.claimamt10, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(18, 2, fw, sv.benefits11)%>

	<%=smartHF.getHTMLVar(18, 36, fw, sv.benfamt11, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(18, 53, fw, sv.daclaim11)%>

	<%=smartHF.getHTMLVar(18, 68, fw, sv.claimamt11, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(19, 2, fw, sv.benefits12)%>

	<%=smartHF.getHTMLVar(19, 36, fw, sv.benfamt12, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(19, 53, fw, sv.daclaim12)%>

	<%=smartHF.getHTMLVar(19, 68, fw, sv.claimamt12, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getHTMLVar(20, 2, fw, sv.benefits13)%>

	<%=smartHF.getHTMLVar(20, 36, fw, sv.benfamt13, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>

	<%=smartHF.getHTMLVar(20, 53, fw, sv.daclaim13)%>

	<%=smartHF.getHTMLVar(20, 68, fw, sv.claimamt13, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>

	<%=smartHF.getLit(21, 65, generatedText9)%>

	<%=smartHF.getLit(22, 42, generatedText10)%>

	<%=smartHF.getHTMLVar(22, 65, fw, sv.claimtot, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN)%>

	<%=smartHF.getLit(23, 2, generatedText8)%>




<%}%>

<%if (sv.Sr683protectWritten.gt(0)) {%>
	<%Sr683protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
