

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5025";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5025ScreenVars sv = (S5025ScreenVars) fw.getVariables();%>


<%{
		if (appVars.ind10.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.efdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.efdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.efdateDisp.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
		<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		       		<div class="input-group" style="min-width:130px;">
		       		<input name='chdrsel'  id='chdrsel'
					type='text' 
					value='<%=sv.chdrsel.getFormData()%>' 
					maxLength='<%=sv.chdrsel.getLength()%>' 
					size='<%=sv.chdrsel.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.chdrsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.chdrsel).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					 <span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
												
					<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
					<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
					</a> --%>
					
					<%
						}else { 
					%>
					
					class = ' <%=(sv.chdrsel).getColor()== null  ? 
					"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					 <span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
					
					<%-- <a href="javascript:;" onClick="doFocus(document.getElementById('chdrsel')); changeF4Image(this); doAction('PFKEY04')"> 
					<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
					</a> --%>
					
					<%} %>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		       		<div class="input-group">
		       		
					
					<% 
						if((new Byte((sv.efdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
				
					
					
					<%
						}else if((new Byte((sv.efdateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">
                   		<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp,(sv.efdateDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
       				</div>
       				
										
					<%
						}else { 
					%>
					
										
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">
                   		<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp,(sv.efdateDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
       				</div>
			            				
									
					<%} %>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	 </div>
</div>

<div class="panel panel-default">
		<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Action")%>
         </div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
			       	<div class="radioButtonSubmenuUIG">
					<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Maturity")%></label> 
					</div>
		       	
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Expiry")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       		
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="I"><%= smartHF.buildRadioOption(sv.action, "action", "I")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%></label> 
					</div>
		       		</div>
		       	</div>
		       	<input name='action' 
				type='hidden'
				value='<%=sv.action.getFormData()%>'
				size='<%=sv.action.getLength()%>'
				maxLength='<%=sv.action.getLength()%>' 
				class = "input_cell"
				onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
		    </div>
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

