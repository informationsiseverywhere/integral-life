<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH5C3";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%
	Sh5c3ScreenVars sv = (Sh5c3ScreenVars) fw.getVariables();
%>
<%{
	if (appVars.ind03.isOn()) {
		sv.action.setReverse(BaseScreenData.REVERSED);
		sv.action.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind03.isOn()) {
		sv.action.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind04.isOn()) {
		sv.efdate.setReverse(BaseScreenData.REVERSED);
		sv.efdate.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind04.isOn()) {
		sv.efdate.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind01.isOn()) {
		sv.chdrsel.setReverse(BaseScreenData.REVERSED);
		sv.chdrsel.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind01.isOn()) {
		sv.chdrsel.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind02.isOn()) {
		sv.claimnmber.setReverse(BaseScreenData.REVERSED);
		sv.claimnmber.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind02.isOn()) {
		sv.claimnmber.setHighLight(BaseScreenData.BOLD);
	}
	

} %>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Input")%></div>
	<div class="panel-body">
		<div class="row">
		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		       		<div class="input-group" style="min-width:130px;">
		       		<input name='chdrsel'  id='chdrsel'
					type='text' 
					value='<%=sv.chdrsel.getFormData()%>' 
					maxLength='<%=sv.chdrsel.getLength()%>' 
					size='<%=sv.chdrsel.getLength()%>'
					onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
					
					<% 
						if((new Byte((sv.chdrsel).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
					readonly="true"
					class="output_cell"	 >
					
					<%
						}else if((new Byte((sv.chdrsel).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					class="bold_cell" >
					 
					 <span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
												
					<%
						}else { 
					%>
					
					class = ' <%=(sv.chdrsel).getColor()== null  ? 
					"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
					"input_cell red reverse" : "input_cell" %>' >
					
					 <span class="input-group-btn">
						<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
						</button>
					</span>
					
					<%} %>
		       		</div>
		       		</div>
		       	</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Number")%></label>

					<div class="input-group" style="max-width: 120px">
						<%=smartHF.getHTMLVarExt(fw, sv.claimnmber)%>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"
								onclick="doFocus(document.getElementById('claimnmber')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>

					</div>
				</div>
			</div>

			<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
		       		<div class="input-group">
		       		
					
					<% 
						if((new Byte((sv.efdateDisp).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
				
					
					
					<%
						}else if((new Byte((sv.efdateDisp).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
					%>	
					
					 
					 <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">
                   		<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp,(sv.efdateDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
       				</div>
       				
										
					<%
						}else { 
					%>
					
										
					<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="efdateDisp" data-link-format="dd/mm/yyyy">
                   		<%=smartHF.getRichTextDateInput(fw, sv.efdateDisp,(sv.efdateDisp.getLength()))%>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
       				</div>
			            				
									
					<%} %>
		       		</div>
		       		</div>
		       	</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->
<style>
.radio-inline>div>label{

font-weight: normal !important;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Register Contract Cancellation")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Approve Contract Cancellation")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="C"><%= smartHF.buildRadioOption(sv.action, "action", "C")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Register Rider Cancellation")%></label> 
					</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="D"><%= smartHF.buildRadioOption(sv.action, "action", "D")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Approve Rider Cancellation")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="E"><%= smartHF.buildRadioOption(sv.action, "action", "E")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Contract Cancellation Registration Reversal")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="F"><%= smartHF.buildRadioOption(sv.action, "action", "F")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Contract Cancellation Approval Reversal")%></label> 
					</div>
		       		</div>
		       	</div>
		      </div>
		      
		      <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="G"><%= smartHF.buildRadioOption(sv.action, "action", "G")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Rider Cancellation Registration Reversal")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="H"><%= smartHF.buildRadioOption(sv.action, "action", "H")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Rider Cancellation Approval Reversal")%></label> 
					</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<div class="radioButtonSubmenuUIG">
					<label for="I"><%= smartHF.buildRadioOption(sv.action, "action", "I")%> 
					<%=resourceBundleHandler.gettingValueFromBundle("Cancellation Enquiry")%></label> 
					</div>
		       		</div>
		       	</div>	
		       	<input name='action' 
				type='hidden'
				value='<%=sv.action.getFormData()%>'
				size='<%=sv.action.getLength()%>'
				maxLength='<%=sv.action.getLength()%>' 
				class = "input_cell"
				onFocus='doFocus(this)' onHelp='return fieldHelp(action)' onKeyUp='return checkMaxLength(this)'  >
		    </div>
	 </div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
