<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr585FormPart";%>

<%@page import="com.csc.life.terminationclaims.screens.Sr585ScreenVars" %>
<%@page import="com.csc.lifeasia.runtime.variables.LifeAsiaAppVars"%>
<%@page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@page import="com.properties.PropertyLoader"%>
<%@page import="com.quipoz.COBOLFramework.util.COBOLHTMLFormatter" %>
<%@page import="com.quipoz.framework.datatype.BaseScreenData" %>
<%@page import="com.quipoz.framework.datatype.FixedLengthStringData" %>
<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.framework.screenmodel.ScreenModel" %>
<%@page import="com.quipoz.framework.util.AppVars" %>
<%@page import="com.quipoz.framework.util.BaseModel"%>
<%@page import="com.quipoz.framework.util.DataModel" %>
<%@page import="com.resource.ResourceBundleHandler" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>

<%-- Start preparing data --%>
<%
String longValue = null;
Map fieldItem=new HashMap();//used to store page Dropdown List
Map mappedItems = null;
String optionValue = null;
String formatValue = null;
QPScreenField qpsf = null;
String valueThis = null;

BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );

ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();

LifeAsiaAppVars av = (LifeAsiaAppVars) baseModel.getApplicationVariables();

LifeAsiaAppVars appVars = av;

av.reinitVariables();

String lang = av.getInstance().getUserLanguage().toString().trim();

SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter(fw.getScreenName(),lang);

smartHF.setLocale(request.getLocale());

ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),request.getLocale());

String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);
%> 

<%Sr585ScreenVars sv = (Sr585ScreenVars) fw.getVariables();%>

<%!
public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler) {

	String opValue = "";
	Map tmp = new HashMap();
	tmp = mp;
	String aValue = "";
	if (val != null) {
		if (val instanceof String) {
			aValue = ((String) val).trim();
		} else if (val instanceof FixedLengthStringData) {
			aValue = ((FixedLengthStringData) val).getFormData().trim();
		}
	}

	Iterator mapIterator = tmp.entrySet().iterator();
	ArrayList keyValueList = new ArrayList();

	while (mapIterator.hasNext()) {
		Map.Entry entry = (Map.Entry) mapIterator.next();
		KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
		keyValueList.add(bean);
	}

	int size = keyValueList.size();

	String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
	opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
			+ strSelect + "---------" + "</option>";
	String mainValue = "";
	//Option 1 fr displaying code
	if (i == 1) {
		//Sorting on the basis of key
		Collections.sort(keyValueList, new KeyComarator());
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\" SELECTED>" + keyValueBean.getKey() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getKey() + "\">" + keyValueBean.getKey() + "</option>";
			}
		}
	}
	//Option 2 for long description
	if (i == 2) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 3 for Short description
	if (i == 3) {
		Collections.sort(keyValueList);
		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
	}
	//Option 4 for format Code--Description
	if (i == 4) {
		Collections.sort(keyValueList);

		for (int ii = 0; ii < size; ii++) {
			KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
			if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\" SELECTED>" + keyValueBean.getKey() + "--"
						+ keyValueBean.getValue() + "</option>";
			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
						+ keyValueBean.getValue() + "\">" + keyValueBean.getKey() + "--" + keyValueBean.getValue()
						+ "</option>";
			}
		}
	}
	return opValue;
}

//Amit for sorting
class KeyValueBean implements Comparable{

private String key;

private String value;


public KeyValueBean(String key, String value) {
	this.key = key;
	this.value = value;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getValue() {
	return value;
}


public void setValue(String value) {
	this.value = value;
}


public int compareTo(Object o) {
	return this.value.compareTo(((KeyValueBean)o).getValue());
}


public String toString() {
	
	return "Key is "+key+" value is "+value;
}

}

//secoond class

public class KeyComarator implements Comparator{

	public int compare(Object o1, Object o2) {
		
		return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
	}

}

public String formatValue(String aValue) {
	return aValue;
}
%>

<tr style='height:22px;'><td width='251'>


<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben01");
	optionValue = makeDropDownList( mappedItems , sv.acdben01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>


	<% }else {%>
	
<% if("red".equals((sv.acdben01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben01' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='251'>

	
  		
		<%					
		if(!((sv.ditdsc01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc01.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='251'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct01' 
type='text'

<%if((sv.dfclmpct01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct01) %>'
	 <%}%>

size='<%= sv.dfclmpct01.getLength()%>'
maxLength='<%= sv.dfclmpct01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct01).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='251'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq01"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq01");
	optionValue = makeDropDownList( mappedItems , sv.benfreq01.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq01.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq01).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:1500px;"> 
<%
} 
%>

<select name='benfreq01' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq01).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt01' 
type='text'

<%if((sv.mxbenunt01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt01) %>'
	 <%}%>

size='<%= sv.mxbenunt01.getLength()%>'
maxLength='<%= sv.mxbenunt01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt01).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld01).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld01' 
type='text'

<%if((sv.amtfld01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld01) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld01);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld01) %>'
	 <%}%>

size='<%= sv.amtfld01.getLength()%>'
maxLength='<%= sv.amtfld01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld01).getColor()== null  ? 
			"input_cell" :  (sv.amtfld01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


<td width='100'>


<input name='gcdblind01' 
type='text'

<%if((sv.gcdblind01).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind01.getLength()%>'
maxLength='<%= sv.gcdblind01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind01).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr><tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben02");
	optionValue = makeDropDownList( mappedItems , sv.acdben02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.acdben02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben02' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='100'>

	
  		
		<%					
		if(!((sv.ditdsc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc02.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct02' 
type='text'

<%if((sv.dfclmpct02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px""<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct02) %>'
	 <%}%>

size='<%= sv.dfclmpct02.getLength()%>'
maxLength='<%= sv.dfclmpct02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct02).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq02");
	optionValue = makeDropDownList( mappedItems , sv.benfreq02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'  style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:150px;"> 
<%
} 
%>

<select name='benfreq02' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt02' 
type='text'

<%if((sv.mxbenunt02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt02) %>'
	 <%}%>

size='<%= sv.mxbenunt02.getLength()%>'
maxLength='<%= sv.mxbenunt02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt02).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld02).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld02' 
type='text'

<%if((sv.amtfld02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld02) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld02);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld02) %>'
	 <%}%>

size='<%= sv.amtfld02.getLength()%>'
maxLength='<%= sv.amtfld02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld02).getColor()== null  ? 
			"input_cell" :  (sv.amtfld02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='100'>


<input name='gcdblind02' 
type='text'

<%if((sv.gcdblind02).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind02.getLength()%>'
maxLength='<%= sv.gcdblind02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind02).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr><tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben03");
	optionValue = makeDropDownList( mappedItems , sv.acdben03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'width:50px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.acdben03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben03' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='100'>

	
  		
		<%					
		if(!((sv.ditdsc03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc03.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct03' 
type='text'

<%if((sv.dfclmpct03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct03) %>'
	 <%}%>

size='<%= sv.dfclmpct03.getLength()%>'
maxLength='<%= sv.dfclmpct03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct03).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq03"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq03");
	optionValue = makeDropDownList( mappedItems , sv.benfreq03.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq03.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq03).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='benfreq03' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq03).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt03' 
type='text'

<%if((sv.mxbenunt03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt03) %>'
	 <%}%>

size='<%= sv.mxbenunt03.getLength()%>'
maxLength='<%= sv.mxbenunt03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt03).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld03).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld03' 
type='text'

<%if((sv.amtfld03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld03) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld03);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld03) %>'
	 <%}%>

size='<%= sv.amtfld03.getLength()%>'
maxLength='<%= sv.amtfld03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld03).getColor()== null  ? 
			"input_cell" :  (sv.amtfld03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='100'>


<input name='gcdblind03' 
type='text'

<%if((sv.gcdblind03).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind03.getLength()%>'
maxLength='<%= sv.gcdblind03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind03).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr><tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben04");
	optionValue = makeDropDownList( mappedItems , sv.acdben04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.acdben04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben04' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='100'>

	
  		
		<%					
		if(!((sv.ditdsc04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc04.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct04' 
type='text'

<%if((sv.dfclmpct04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct04) %>'
	 <%}%>

size='<%= sv.dfclmpct04.getLength()%>'
maxLength='<%= sv.dfclmpct04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct04).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq04"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq04");
	optionValue = makeDropDownList( mappedItems , sv.benfreq04.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq04.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq04).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='benfreq04' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq04).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt04' 
type='text'

<%if((sv.mxbenunt04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt04) %>'
	 <%}%>

size='<%= sv.mxbenunt04.getLength()%>'
maxLength='<%= sv.mxbenunt04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt04).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld04).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld04' 
type='text'

<%if((sv.amtfld04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld04) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld04);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld04) %>'
	 <%}%>

size='<%= sv.amtfld04.getLength()%>'
maxLength='<%= sv.amtfld04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld04).getColor()== null  ? 
			"input_cell" :  (sv.amtfld04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='100'>


<input name='gcdblind04' 
type='text'

<%if((sv.gcdblind04).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind04.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind04.getLength()%>'
maxLength='<%= sv.gcdblind04.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind04)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind04).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind04).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr><tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben05");
	optionValue = makeDropDownList( mappedItems , sv.acdben05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.acdben05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben05' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='100'>

	
  		
		<%					
		if(!((sv.ditdsc05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc05.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct05' 
type='text'

<%if((sv.dfclmpct05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct05) %>'
	 <%}%>

size='<%= sv.dfclmpct05.getLength()%>'
maxLength='<%= sv.dfclmpct05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct05).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq05"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq05");
	optionValue = makeDropDownList( mappedItems , sv.benfreq05.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq05.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq05).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='benfreq05' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq05).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt05' 
type='text'

<%if((sv.mxbenunt05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt05) %>'
	 <%}%>

size='<%= sv.mxbenunt05.getLength()%>'
maxLength='<%= sv.mxbenunt05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt05).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld05).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld05' 
type='text'

<%if((sv.amtfld05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld05) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld05);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld05) %>'
	 <%}%>

size='<%= sv.amtfld05.getLength()%>'
maxLength='<%= sv.amtfld05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld05)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld05).getColor()== null  ? 
			"input_cell" :  (sv.amtfld05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='100'>


<input name='gcdblind05' 
type='text'

<%if((sv.gcdblind05).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind05.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind05.getLength()%>'
maxLength='<%= sv.gcdblind05.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind05)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind05).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind05).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr><tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben06");
	optionValue = makeDropDownList( mappedItems , sv.acdben06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.acdben06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.acdben06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='acdben06' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


</td><td width='100'>

	
  		
		<%					
		if(!((sv.ditdsc06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc06.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	




</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='dfclmpct06' 
type='text'

<%if((sv.dfclmpct06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct06) %>'
	 <%}%>

size='<%= sv.dfclmpct06.getLength()%>'
maxLength='<%= sv.dfclmpct06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'

<% 
	if((new Byte((sv.dfclmpct06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.dfclmpct06).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq06"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq06");
	optionValue = makeDropDownList( mappedItems , sv.benfreq06.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq06.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.benfreq06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.benfreq06).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>

<select name='benfreq06' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq06).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='mxbenunt06' 
type='text'

<%if((sv.mxbenunt06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt06) %>'
	 <%}%>

size='<%= sv.mxbenunt06.getLength()%>'
maxLength='<%= sv.mxbenunt06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mxbenunt06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mxbenunt06).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>



</td><td width='100'>


	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld06).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
			
	%>

<input name='amtfld06' 
type='text'

<%if((sv.amtfld06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld06) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld06);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld06) %>'
	 <%}%>

size='<%= sv.amtfld06.getLength()%>'
maxLength='<%= sv.amtfld06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld06)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.amtfld06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.amtfld06).getColor()== null  ? 
			"input_cell" :  (sv.amtfld06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td><td width='100'>


<input name='gcdblind06' 
type='text'

<%if((sv.gcdblind06).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>

<%

		formatValue = (sv.gcdblind06.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.gcdblind06.getLength()%>'
maxLength='<%= sv.gcdblind06.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind06)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.gcdblind06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.gcdblind06).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind06).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>


</tr>