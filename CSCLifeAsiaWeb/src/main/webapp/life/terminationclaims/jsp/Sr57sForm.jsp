
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr57s";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr57sScreenVars sv = (Sr57sScreenVars) fw.getVariables();%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Claim Details -----------------------------------------------");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dth. Date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pol Lns ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Poldebt ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Intrest ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff. Date ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Oth Adj ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Off Crg ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cause of Death");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Net Tot ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Est Tot ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currncy ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Deposit ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Claim Amt ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adj Reason    ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow ups       ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"%");%>   <!-- ILIFE-3121 -->

<%{
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.dtofdeathDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.causeofdth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.causeofdth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
/* 		if (appVars.ind04.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}*/
		if (appVars.ind09.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.longdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.longdesc.setReverse(BaseScreenData.REVERSED);
			sv.longdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.longdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
		if (appVars.ind31.isOn()) {
		    sv.claimnumber.setReverse(BaseScreenData.REVERSED);
		    sv.claimnumber.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind31.isOn()) {
		    sv.claimnumber.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind24.isOn()) {
		  	sv.claimnumber.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Coverage/Rider Review ---------------------------------------");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                          Lien                 Estimated              Actual");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov/Rd Fund Typ Descriptn.Code     Ccy             Value               Value");%>
<%		appVars.rollup(new int[] {93});
%>



<div class="panel panel-default">
	<div class="panel-body">
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
				<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
				</label>
				<%}%>
				
				<table>
				<tr>
				<td>
				
				<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				</td>
				<td>			
				
				
				<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				</td>
				<td>
				
				<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					</td>
					</tr>
					</table>
				
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
												<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commentcement Date")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
				<%}%>
				
				<table>
				<tr>
				<td>
				<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				  </td>
				  <!-- ILIFE-8700--Start -->
				  <%-- <td>
				  
				  <%if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
								<%=formatValue%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					</td> --%>
					<!-- ILIFE-8700--End -->
					</tr>
					</table>
				
				</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
				<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
				<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Risk/Premium Status")%>
				</label>
				<%}%>
				<table>
				<tr>
				<td>
				
				<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
				  </td>
				  <td>
				  
				  <%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
									
				</td>
				</tr>
				</table>
			</div>
			</div>
				
				
		</div>

	<div class="row">
			
		<div class="col-md-4">
			<div class="form-group">
			<%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
			</label>
			<%}%>
			
			<table>
			<tr>
			<td>
			
			<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				</td>
				<td>					
			
			
			<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				
			</td>
			</tr>
			</table>
			</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
			<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
			</label>
			<%}%>
			
			<table>
			<tr>
			<td>
			
			<%if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				</td>		
				<td>
			
			<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				
			</td>
			<td>	
			
			
			
			<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
							
			</td>
			</tr>
			</table>
			</div>
			</div>
		
		    <%if ((new Byte((sv.claimnumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>	
		 <div class="col-md-4">
		 <div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim No.")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
	</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
	</div> 
		<%} %>
	</div>    
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%>
			</label>
			<%}%>
					

			<%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%>
			</label>
			<%}%>
			
						
			<%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				

			</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
				<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
				</label>
				<%}%>

					
				<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%					
						if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
	

				</div>
				</div>
	</div>

	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
			</label>

			<%if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
	

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%>
			</label>


			<%if ((new Byte((sv.dtofdeathDisp).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.dtofdeathDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.dtofdeathDisp.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
				

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Cause of Death")%>
				</label>

					<%if ((new Byte((sv.causeofdthdsc).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.causeofdthdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.causeofdthdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.causeofdthdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</div>
				</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Total Claim Amount")%>
			</label>


			<%if ((new Byte((sv.totclaim).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%	
						qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.totclaim);
						
						if(!((sv.totclaim.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
							<div class="output_cell">	
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
					<%
						} else {
					%>
					
							<div class="blank_cell" style="width: 100px;"> &nbsp; </div>
					
					<% 
						} 
					%>
					<%
					longValue = null;
					formatValue = null;
					%>
				
			 <%}%>
	
			</div>
		</div>
			
		<div class="col-md-4">
			<div class="form-group">
			<label>
				<%=resourceBundleHandler.gettingValueFromBundle("Total Adjustment")%>
			</label>


				<%if ((new Byte((sv.otheradjst).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.otheradjst);
							
							if(!((sv.otheradjst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 100px;"> &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
					

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label>
			<%=resourceBundleHandler.gettingValueFromBundle("Adjustment Reason")%>
			</label>

			<table>
			<tr>
			<td>
			<%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>
			  </td>
			  <td>
			<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
				
			  		
					<%					
					if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px; width: 150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
			  <%}%>	
			  </td>
			  </tr>
			  </table>

			</div>
		</div>


	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary Hold Details")%></label>
			</div>
		</div>
	</div>


	<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr57s' width='100%'>
						<thead>
							<tr class='info'>
								<th colspan='2' style="text-align: center;width: 123px;"><%=resourceBundleHandler.gettingValueFromBundle("Beneficiary")%></th>
								<th style="text-align: center;width: 66px;"><%=resourceBundleHandler.gettingValueFromBundle("Share %")%></th>   <!--  ILIFE-3121 -->
								<th style="text-align: center;width: 92px;"><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></th>
								<th style="text-align: center;width: 126px;"><%=resourceBundleHandler.gettingValueFromBundle("Adjustment")%></th>
								<th style="text-align: center;width: 142px;"><%=resourceBundleHandler.gettingValueFromBundle("Release Claim Amount")%></th>
								<th style="text-align: center;width: 177px;"><%=resourceBundleHandler.gettingValueFromBundle("Release Adjustment Amount")%></th>
							</tr>
						</thead>
						<tbody>
							<%
							/* This block of jsp code is to calculate the variable width of the table at runtime.*/
							int[] tblColumnWidth = new int[8];
							int totalTblWidth = 0;
							int calculatedValue =0;
							
													if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.bnyclt.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*8;								
										} else {		
											calculatedValue = (sv.bnyclt.getFormData()).length()*8;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[0]= calculatedValue;
										
													if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.bnynam.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*10;								
										} else {		
											calculatedValue = (sv.bnynam.getFormData()).length()*10;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[1]= calculatedValue;
										
													if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.bnypc.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*6;								
										} else {		
											calculatedValue = (sv.bnypc.getFormData()).length()*6;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[2]= calculatedValue;
										
													if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.zhldclmv.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*6;								
										} else {		
											calculatedValue = (sv.zhldclmv.getFormData()).length()*6;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[3]= calculatedValue;
										
													if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.zhldclma.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*7;								
										} else {		
											calculatedValue = (sv.zhldclma.getFormData()).length()*7;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[4]= calculatedValue;
										
													if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.zrlsclmv.getFormData()).length() ) {
											calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*7;								
										} else {		
											calculatedValue = (sv.zrlsclmv.getFormData()).length()*7;								
										}		
											totalTblWidth += calculatedValue;
									tblColumnWidth[5]= calculatedValue;						
									
									if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.zrlsclma.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*7;								
									} else {		
										calculatedValue = (sv.zrlsclma.getFormData()).length()*7;								
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[6]= calculatedValue;	
										%>
									<%
									GeneralTable sfl = fw.getTable("sr57sscreensfl");
									int height;
									if(sfl.count()*27 > 80) {
									height = 210 ;
									} else {
									height = sfl.count()*27;
									}	
									%>
									
									
									
									
									<%
		String backgroundcolor = "#FFFFFF";
	Sr57sscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr57sscreensfl
	.hasMoreScreenRows(sfl)) {
	{
		if (appVars.ind08.isOn()) {
			sv.zrlsclmv.setReverse(BaseScreenData.REVERSED);
		}		
		if (appVars.ind08.isOn()) {
			sv.zrlsclmv.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.zrlsclmv.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.zrlsclma.setReverse(BaseScreenData.REVERSED);
		}		
		if (appVars.ind11.isOn()) {
			sv.zrlsclma.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.zrlsclma.setHighLight(BaseScreenData.BOLD);
		}
	}
	%>

	<tr style="background:<%=backgroundcolor%>;">
						    									<td   style="width: 87px;"
					<%if((sv.bnyclt).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align="right" <%}%> >
																			
										<%if((new Byte((sv.bnyclt).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.bnyclt.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  style="width: 101px;"
					<%if((sv.bnynam).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align=right <%}%> >									
										<%if((new Byte((sv.bnynam).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.bnynam.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  style="width: 67px;"
					<%if((sv.bnypc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.bnypc).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
						<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.bnypc).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.bnypc);
							if(!(sv.bnypc).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
							<%= generatedText29 %>  <!-- ILIFE-3121 -->
						<%
								longValue = null;
								formatValue = null;
						%>				
									<%}%>					
														 
			</td>  									
				    									<td  style="width: 100px;"
					<%if((sv.zhldclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.zhldclmv).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.zhldclmv).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.zhldclmv);
							if(!(sv.zhldclmv).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									<%}%>
			</td>
				    									<td    style="width: 120px;"
					<%if((sv.zhldclma).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.zhldclma).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.zhldclma).getFieldName());						
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.zhldclma);
							if(!(sv.zhldclma).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									<%}%>
			</td>
				    									<td  style="width: 150px;"
					<%if((sv.zrlsclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="right" <%}%> >
					<%if ((new Byte((sv.zrlsclmv).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


					<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.zrlsclmv).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.zrlsclmv,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					%>
				
				<input style="width: 136px;"
				name='<%="sr57sscreensfl" + "." +
									 "zrlsclmv" + "_R" + count %>'
									 id='<%="sr57sscreensfl" + "." +
									 "zrlsclmv" + "_R" + count %>'
				type='text'
				
				<%if((sv.zrlsclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
				
					value='<%= valueThis %>'
							 <%
					 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=smartHF.getPicFormatted(qpsf,sv.zrlsclmv) %>'
					 <%}%>
				
				size='<%= sv.zrlsclmv.getLength()%>'
				maxLength='<%= sv.zrlsclmv.getLength()%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
				
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.zrlsclmv).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.zrlsclmv).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.zrlsclmv).getColor()== null  ? 
							"input_cell" :  (sv.zrlsclmv).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
				<%}%>											
														<%-- <%if((new Byte((sv.zrlsclmv).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
													
													
															
										<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.zrlsclmv).getFieldName());						
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									
														
										<%
											formatValue = smartHF.getPicFormatted(qpsf,sv.zrlsclmv);
											if(!(sv.zrlsclmv).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
												formatValue = formatValue( formatValue );
											}
										%>
										<%= formatValue%>
										<%
												longValue = null;
												formatValue = null;
										%>
									 			 		
							 		
							    				 
								
													<%}%> --%>
							</td>
								    									<td   
									<%if((sv.zrlsclma).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="right" <%}%> >	
														<%if ((new Byte((sv.zrlsclma).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
				
				
					<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.zrlsclma).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							valueThis=smartHF.getPicFormatted(qpsf,sv.zrlsclma,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
					%>
				
				<input  style="width: 177px;"
				name='<%="sr57sscreensfl" + "." +
									 "zrlsclma" + "_R" + count %>'
									 id='<%="sr57sscreensfl" + "." +
									 "zrlsclma" + "_R" + count %>'
				type='text'
				
				<%if((sv.zrlsclma).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
				
					value='<%= valueThis %>'
							 <%
					 
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=smartHF.getPicFormatted(qpsf,sv.zrlsclma) %>'
					 <%}%>
				
				size='<%= sv.zrlsclma.getLength()%>'
				maxLength='<%= sv.zrlsclma.getLength()%>' 
				onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  
				
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event,true);'
					onBlur='return doBlurNumberNew(event,true);'
				
				<% 
					if((new Byte((sv.zrlsclma).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.zrlsclma).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.zrlsclma).getColor()== null  ? 
							"input_cell" :  (sv.zrlsclma).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
				<%}%>		
				<%-- 										<%if((new Byte((sv.zrlsclma).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
													
																											
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.zrlsclma).getFieldName());						
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									
														
										<%
											formatValue = smartHF.getPicFormatted(qpsf,sv.zrlsclma);
											if(!(sv.zrlsclma).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
												formatValue = formatValue( formatValue );
											}
										%>
										<%= formatValue%>
										<%
												longValue = null;
												formatValue = null;
										%>
									 			 		
							 		
							    				 
								
													<%}%>
				 --%>			</td>
								    									<%-- <td class="tableDataTag" style="width:<%=tblColumnWidth[8 ]%>px;" 
									<%if((sv.zhldclmv).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> style="text-align: right"<%}%> >									
														<%if((new Byte((sv.zhldclmv).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
													
																											
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.zhldclmv).getFieldName());						
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
									%>
									
														
										<%
											formatValue = smartHF.getPicFormatted(qpsf,sv.zhldclmv);
											if(!(sv.zhldclmv).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
												formatValue = formatValue( formatValue );
											}
										%>
										<%= formatValue%>
										<%
												longValue = null;
												formatValue = null;
										%>
									 			 		
							 		
							    				 
								
													<%}%>
							</td> --%>
									
					</tr>
				
					<%
						if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
								backgroundcolor = "#ededed";
							} else {
								backgroundcolor = "#FFFFFF";
							}
					count = count + 1;
					Sr57sscreensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
						
						
						</tbody>
					</table>
				</div>
			</div>
	</div>


		




<%-- <script language="javascript">
        $(document).ready(function(){
	
			new superTable("dataTables-sr57s", {
				fixedCols : 0,	
				headerRows:1,
				colWidths : [70,180,120,150,120,140,140],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
 --%>


<!-- <script>
$(document).ready(function() {
    $('#dataTables-sr57s').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false
       
    } );
   
} );

</script> -->


</div>
</div>


<script type="text/javascript">

function change()
{
	document.getElementsByName("longdesc")[0].value="";
	doAction('PFKEY05');
	
}
</script>

						





<%@ include file="/POLACommon2NEW.jsp"%>

