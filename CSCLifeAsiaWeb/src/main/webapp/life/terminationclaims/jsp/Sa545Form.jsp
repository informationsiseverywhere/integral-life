

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sa545";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sa545ScreenVars sv = (Sa545ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No        ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life               ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner              ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commencement date  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Currency     ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment seq. no    ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum assured       ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment type       ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment reason     ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Evidence           ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee              ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method     ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective date        ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Frequency          ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"First payment date    ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment amount     ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Review date           ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% of sum assured   ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Anniversary date      ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment currency   ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Final payment date    ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Destination key    ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total for component this term ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank details          ");%>

<%{
		if (appVars.ind05.isOn()) {
			sv.regpayfreq.setReverse(BaseScreenData.REVERSED);
			sv.regpayfreq.setColor(BaseScreenData.RED);
		}
		if (appVars.ind24.isOn()) {
			sv.regpayfreq.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind05.isOn()) {
			sv.regpayfreq.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.destkey.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind27.isOn()) {
			sv.destkey.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind09.isOn()) {
			sv.destkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.destkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.ddind.setReverse(BaseScreenData.REVERSED);
			sv.ddind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.ddind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.cltype.setReverse(BaseScreenData.REVERSED);
			sv.cltype.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.cltype.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.cltype.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.claimevd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind21.isOn()) {
			sv.claimevd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.claimevd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.claimevd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.rgpymop.setReverse(BaseScreenData.REVERSED);
			sv.rgpymop.setColor(BaseScreenData.RED);
		}
		if (appVars.ind23.isOn()) {
			sv.rgpymop.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind04.isOn()) {
			sv.rgpymop.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.payclt.setReverse(BaseScreenData.REVERSED);
			sv.payclt.setColor(BaseScreenData.RED);
		}
		if (appVars.ind22.isOn()) {
			sv.payclt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind03.isOn()) {
			sv.payclt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.claimcur.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.claimcur.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.claimcur.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.claimcur.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.anvdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind29.isOn()) {
			sv.anvdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.anvdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.anvdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.finalPaydateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind30.isOn()) {
			sv.finalPaydateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.finalPaydateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.finalPaydateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.pymt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.pymt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.pymt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.pymt.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind50.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind50.isOn()) {
			sv.totalamt.setReverse(BaseScreenData.REVERSED);
		}
		if (!appVars.ind50.isOn()) {
			sv.totalamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind50.isOn()) {
			sv.totalamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.totalamt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.prcnt.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.prcnt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.prcnt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.prcnt.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Contract No")%>
						<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					</td>
					<td>
					
					<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
						
					
					</td>
					<td>
					<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 165px;min-width: 165px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Commencement Date")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Contract Currency")%>
						<%}%>
		       		</label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 50px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
							</td>
							<td>
						
						
						<%if ((new Byte((sv.currds).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.currds.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currds.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currds.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Life")%>
						<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						</td>
						<td>
						
						<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width: 230px;min-width: 100px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						  </td>
						  </tr>
						  </table>
		       		
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Owner")%>
						<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
					</td>
					<td>
					<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width: 200px;min-width: 200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       	 
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payee")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.payclt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

								<%	
									
									longValue = sv.payclt.getFormData();  
								%>
								
								<% 
									if((new Byte((sv.payclt).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>  
								<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "output_cell" %>' style="min-width: 160px;">  
									   		<%if(longValue != null){%>
									   		
									   		<%=XSSFilter.escapeHtml(longValue)%>
									   		
									   		<%}%>
									   </div>
								
								<%
								longValue = null;
								%>
								<% }else {%> 
								<input name='payclt' id='payclt'
								type='text' 
								value='<%=sv.payclt.getFormData()%>' 
								maxLength='<%=sv.payclt.getLength()%>' 
								size='<%=sv.payclt.getLength()%>'
								onFocus='doFocus(this)' onHelp='return fieldHelp(payclt)' onKeyUp='return checkMaxLength(this)'  
								
								<% 
									if((new Byte((sv.payclt).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
								%>  
								readonly="true"
								class="output_cell"	 >
								
								<%
									}else if((new Byte((sv.payclt).getHighLight())).
										compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									
								%>	
								class="bold_cell" >

								<span class="input-group-btn">
									<button class="btn btn-info" style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important"; type="button" onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
								
								<%
									}else { 
								%>
								
								class = ' <%=(sv.payclt).getColor()== null  ? 
								"input_cell" :  (sv.payclt).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>' >
							
								<span class="input-group-btn">
									<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('payclt')); doAction('PFKEY04')">
									<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
								</span>
								
								<%}longValue = null;}} %>
								
								<%if ((new Byte((sv.payenme).getInvisible())).compareTo(new Byte(
																BaseScreenData.INVISIBLE)) != 0) {%>
									
								  		
										<%					
										if(!((sv.payenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.payenme.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="margin-left:2px;max-width: 180px;min-width: 150px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
								  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payment Sequence Number")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rgpynum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rgpynum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpynum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payment Type")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.rgpytypesd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%					
						if(!((sv.rgpytypesd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rgpytypesd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payment Reason")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%	
	if ((new Byte((sv.cltype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"cltype"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("cltype");
	optionValue = makeDropDownList( mappedItems , sv.cltype.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.cltype.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.cltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.cltype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='cltype' type='list' style="width:180px;"
<% 
	if((new Byte((sv.cltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.cltype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.cltype).getColor())){
%>
</div>
<!-- MIBT-217 -->
<%
} 
%>
<%
formatValue=null;
longValue = null;
%>
<%
}} 
%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%>
					<%}%></label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%if ((new Byte((sv.sumins).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.sumins).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.sumins.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Evidence")%>
						<%}%></label>
		       		<div class="input-group" style="min-width: 100px;">
		       		<%if ((new Byte((sv.claimevd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


							<input name='claimevd' 
							type='text'
							
							<%if((sv.claimevd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
							
							<%
							
									formatValue = (sv.claimevd.getFormData()).toString();
							
							%>
								value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.claimevd.getLength()%>'
							maxLength='<%= sv.claimevd.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(claimevd)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.claimevd).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.claimevd).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.claimevd).getColor()== null  ? 
										"input_cell" :  (sv.claimevd).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>
							<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%	
								if ((new Byte((sv.rgpymop).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"rgpymop"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("rgpymop");
								optionValue = makeDropDownList( mappedItems , sv.rgpymop.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.rgpymop.getFormData()).toString().trim());  
							%>
							
							<% 
								if((new Byte((sv.rgpymop).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>  
							  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>  
								   		<%if(longValue != null){%>
								   		
								   		<%=XSSFilter.escapeHtml(longValue)%>
								   		
								   		<%}%>
								   </div>
							
							<%
							longValue = null;
							%>
							
								<% }else {%>
								
							<% if("red".equals((sv.rgpymop).getColor())){
							%>
							<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
							<%
							} 
							%>
							
							<select name='rgpymop' type='list' style="width:180px;"
							<% 
								if((new Byte((sv.rgpymop).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
								readonly="true"
								disabled
								class="output_cell"
							<%
								}else if((new Byte((sv.rgpymop).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							<%
								}else { 
							%>
								class = 'input_cell' 
							<%
								} 
							%>
							>
							<%=optionValue%>
							</select>
							<% if("red".equals((sv.rgpymop).getColor())){
							%>
							</div>
							<!-- MIBT-217 -->
							<%
							} 
							%>
							<%
							formatValue=null;
							longValue = null;
							%>
							<%
							}} 
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%>
						<%}%>
		       		</label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.crtdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.crtdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crtdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Frequency")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.regpayfreq).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"regpayfreq"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("regpayfreq");
						optionValue = makeDropDownList( mappedItems , sv.regpayfreq.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.regpayfreq.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.regpayfreq).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.regpayfreq).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
					<%
					} 
					%>
					
					<select name='regpayfreq' type='list' style="width:180px;"
					<% 
						if((new Byte((sv.regpayfreq).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.regpayfreq).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.regpayfreq).getColor())){
					%>
					</div>
					<!-- MIBT-217 -->
					<%
					} 
					%>
					<%
					formatValue=null;
					longValue = null;
					%>
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("First Payment Date")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.firstPaydateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
								<%					
								if(!((sv.firstPaydateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.firstPaydateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.firstPaydateDisp.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Review Date")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%if ((new Byte((sv.revdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
							<%					
							if(!((sv.revdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.revdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.revdateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Anniversary Date")%>
						<%}%></label>
		       		<div class="input-group">
		       		<%	
								if ((new Byte((sv.anvdateDisp).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {
								longValue = sv.anvdateDisp.getFormData();  
							%>
							
							<% 
								if((new Byte((sv.anvdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>  
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>  
								   		<%if(longValue != null){%>
								   		
								   		<%=XSSFilter.escapeHtml(longValue)%>
								   		
								   		<%}%>
								   </div>
							
							<%
							longValue = null;
							%>
							<% }else {%> 
							<%-- <input name='anvdateDisp' 
							type='text' 
							value='<%=sv.anvdateDisp.getFormData()%>' 
							maxLength='<%=sv.anvdateDisp.getLength()%>' 
							size='<%=sv.anvdateDisp.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(anvdateDisp)' onKeyUp='return checkMaxLength(this)'  --%> 
							
							<% 
								if((new Byte((sv.anvdateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
						<!-- 	readonly="true"
							class="output_cell"	> -->
							
							<%
								}else if((new Byte((sv.anvdateDisp).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								
							%>	
							<!-- class="bold_cell" > -->
							 
													
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp" data-link-format="dd/mm/yyyy">
				                    <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp,(sv.anvdateDisp.getLength()))%>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            				</div>
							
							<%
								}else { 
							%>
							
							class = ' <%=(sv.anvdateDisp).getColor()== null  ? 
							"input_cell" :  (sv.anvdateDisp).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' >
							
													
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="anvdateDisp" data-link-format="dd/mm/yyyy">
				                    <%=smartHF.getRichTextDateInput(fw, sv.anvdateDisp,(sv.anvdateDisp.getLength()))%>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            				</div>
							
							<%} }}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<%=resourceBundleHandler.gettingValueFromBundle("Final Payment Date")%>
							<%}%></label>
		       		<div class="input-group">
		       		<%	
								if ((new Byte((sv.finalPaydateDisp).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {
								longValue = sv.finalPaydateDisp.getFormData();  
							%>
							
							<% 
								if((new Byte((sv.finalPaydateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>  
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>  
								   		<%if(longValue != null){%>
								   		
								   		<%=XSSFilter.escapeHtml(longValue)%>
								   		
								   		<%}%>
								   </div>
							
							<%
							longValue = null;
							%>
							<% }else {%> 
							
							
							<% 
								if((new Byte((sv.finalPaydateDisp).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
						
							
							<%
								}else if((new Byte((sv.finalPaydateDisp).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								
							%>	
						
					
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp" data-link-format="dd/mm/yyyy">
				                    <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp,(sv.finalPaydateDisp.getLength()))%>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            				</div>
							
							<%
								}else { 
							%>
							
							
							
					
							<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="finalPaydateDisp" data-link-format="dd/mm/yyyy">
				                    <%=smartHF.getRichTextDateInput(fw, sv.finalPaydateDisp,(sv.finalPaydateDisp.getLength()))%>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            				</div>
							
							<%} }}%>
		       		</div>
		       		</div>
		       	</div>

		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Payment Amount")%>
						<%}%></label>
		       		<div class="input-group" style="min-width: 100px;">
		       		<%if ((new Byte((sv.pymt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


							<%	
									qpsf = fw.getFieldXMLDef((sv.pymt).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
									valueThis=smartHF.getPicFormatted(qpsf,sv.pymt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							%>
						
						<input name='pymt' 
						type='text'
						
						<%if((sv.pymt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
							value='<%=valueThis%>'
									 <%	 
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=valueThis%>'
							 <%}%>
						
						size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.pymt.getLength(), sv.pymt.getScale(),3)%>'
						maxLength='<%= sv.pymt.getLength()%>' 
						onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(pymt)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
						
						<% 
							if((new Byte((sv.pymt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.pymt).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.pymt).getColor()== null  ? 
									"input_cell" :  (sv.pymt).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>
						>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("% of Sum Assured")%>
						<%}%></label>
		       		<div class="input-group" style="min-width: 50px;">
		       				
		       		<%if ((new Byte((sv.prcnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		       		
		       			<%	
				qpsf = fw.getFieldXMLDef((sv.prcnt).getFieldName());
				qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);
								
					%>
				
				<input name='prcnt' 
				type='text'
				
					value='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
							 <%
					 valueThis=smartHF.getPicFormatted(qpsf,sv.prcnt);
					 if(valueThis!=null&& valueThis.trim().length()>0) {%>
					 title='<%=smartHF.getPicFormatted(qpsf,sv.prcnt) %>'
					 <%}%>
				
				size='<%= sv.prcnt.getLength()%>'
				maxLength='<%= sv.prcnt.getLength()+1%>' 
				onFocus='doFocus(this)' onHelp='return fieldHelp(prcnt)' onKeyUp='return checkMaxLength(this)'  
				
					onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
					decimal='<%=qpsf.getDecimals()%>' 
					onPaste='return doPasteNumber(event);'
					onBlur='return doBlurNumber(event);'
				
				<% 
					if((new Byte((sv.prcnt).getEnabled()))
					.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
				%>  
					readonly="true"
					class="output_cell"
				<%
					}else if((new Byte((sv.prcnt).getHighLight())).
						compareTo(new Byte(BaseScreenData.BOLD)) == 0){
				%>	
						class="bold_cell" 
				
				<%
					}else { 
				%>
				
					class = ' <%=(sv.prcnt).getColor()== null  ? 
							"input_cell" :  (sv.prcnt).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>'
				 
				<%
					} 
				%>
				>
				
		       	<%}%>	
		      
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
					<%}%></label>
		       		<div class="input-group">
		       		<%	
						if ((new Byte((sv.claimcur).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"claimcur"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("claimcur");
						optionValue = makeDropDownList( mappedItems , sv.claimcur.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.claimcur.getFormData()).toString().trim());  
					%>
					
					<% 
						if((new Byte((sv.claimcur).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%>  
					  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=XSSFilter.escapeHtml(longValue)%>
						   		
						   		<%}%>
						   </div>
					
					<%
					longValue = null;
					%>
					
						<% }else {%>
						
					<% if("red".equals((sv.claimcur).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
					<%
					} 
					%>
					
					<select name='claimcur' type='list' style="width:180px;"
					<% 
						if((new Byte((sv.claimcur).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>  
						readonly="true"
						disabled
						class="output_cell"
					<%
						}else if((new Byte((sv.claimcur).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					<%
						}else { 
					%>
						class = 'input_cell' 
					<%
						} 
					%>
					>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.claimcur).getColor())){
					%>
					</div>
					<%
					} 
					%>
					
					<%
					}} 
					%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Destination Key")%>
					<%}%>
		       		</label>
		       		<div class="input-group" style="min-width: 80px;">
		       		<%if ((new Byte((sv.destkey).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


					<input name='destkey' 
					type='text'
					
					<%if((sv.destkey).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
					
					<%
					
							formatValue = (sv.destkey.getFormData()).toString();
					
					%>
						value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
					
					size='<%= sv.destkey.getLength()%>'
					maxLength='<%= sv.destkey.getLength()%>' 
					onFocus='doFocus(this)' onHelp='return fieldHelp(destkey)' onKeyUp='return checkMaxLength(this)'  
					
					
					<% 
						if((new Byte((sv.destkey).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
					%>  
						readonly="true"
						class="output_cell"
					<%
						}else if((new Byte((sv.destkey).getHighLight())).
							compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>	
							class="bold_cell" 
					
					<%
						}else { 
					%>
					
						class = ' <%=(sv.destkey).getColor()== null  ? 
								"input_cell" :  (sv.destkey).getColor().equals("red") ? 
								"input_cell red reverse" : "input_cell" %>'
					 
					<%
						} 
					%>
					>
					<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div style='visibility:hidden;'><table></table></div><br/><div style='visibility:hidden;'><table>
				<tr style='height:22px;'><td width='188'>
				<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="label_txt">
				<%=resourceBundleHandler.gettingValueFromBundle("Total for component this term")%>
				</div>
				<%}%>
				
				
				<br/>
				
				<%if ((new Byte((sv.totalamt).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
					
				  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.totalamt).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.totalamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.totalamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  <%}%>
					
				
				</td>
				</tr></table></div>

	 </div>
</div>



 <BODY>
<div class="sidearea">
<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse" style="display: block;">
<ul class="nav" id="mainForm_OPTS">
<li>
<span>
<ul class="nav nav-second-level" aria-expanded="true">
<li >
<%
	FixedLengthStringData desc = new FixedLengthStringData();
	desc.setFormData(generatedText21.getFormData());
%>


<input name='ddind' id='ddind' type='hidden' value="<%=sv.ddind.getFormData()%>">

									<!-- text -->
									<%
									if((sv.ddind.getInvisible()== BaseScreenData.INVISIBLE|| sv.ddind
											.getEnabled()==BaseScreenData.DISABLED)){
										
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Bank Details")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ddind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Bank Details")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.ddind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.ddind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('ddind'))"></i> 
			 						<%}%>
			 						</a>



</li> </ul></span></li></ul></div></div></div></BODY>

<%@ include file="/POLACommon2NEW.jsp"%>

