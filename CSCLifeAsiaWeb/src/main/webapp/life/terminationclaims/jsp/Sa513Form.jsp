

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SA513";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sa513ScreenVars sv = (Sa513ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.tranno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.trdate.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.effdates.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.trancd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.trandes.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.userid.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
	
		<!-- Claim Notification Number -->
		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.notifnum)%></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assurd")%></label>
					<table>
						<tr>
							<td>
								<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
							<td>
								<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 40px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
						</tr>
					</table>
				</div>
				
				<!-- Claimant -->
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claimant")%></label>
					<table>
						<tr>
							<td>
								<%if ((new Byte((sv.claimant).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.claimant.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.claimant.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.claimant.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
							<td>
								<%if ((new Byte((sv.clamnme).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.clamnme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.clamnme.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.clamnme.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 40px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
						</tr>
					</table>
				</div>	
			</div>

			<div class="col-md-4"></div>
			
			<!-- Relationship to Life Assured -->
			<div class="col-md-2">
				<div class="form-group" style="width: 300px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship to Life Assured")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.relation)%></div>
				</div>
			</div>
			
		</div>

			<%
			GeneralTable sfl = fw.getTable("sa513screensfl");
			%>
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sa513' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%> </th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Trans No.")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Trans Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Trans Code")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Trans Description")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					     	 <%
								Sa513screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sa513screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							<%
							{
									if (appVars.ind02.isOn()) {
										sv.select.setReverse(BaseScreenData.REVERSED);
									}
									if (appVars.ind04.isOn()) {
										sv.select.setEnabled(BaseScreenData.DISABLED);
									}
									if (appVars.ind03.isOn()) {
										sv.select.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind02.isOn()) {
										sv.select.setColor(BaseScreenData.RED);
									}
									if (!appVars.ind02.isOn()) {
										sv.select.setHighLight(BaseScreenData.BOLD);
									}
									if (appVars.ind05.isOn()) {
										sv.tranno.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind06.isOn()) {
										sv.trdate.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind07.isOn()) {
										sv.effdates.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind08.isOn()) {
										sv.trancd.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind09.isOn()) {
										sv.trandes.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind10.isOn()) {
										sv.userid.setInvisibility(BaseScreenData.INVISIBLE);
									}
								}
					
							%>

				<tr class="tableRowTag" id='<%="tablerow"+count%>' >
					<td  
						<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="center" <%}%> >
																				
											<%if((new Byte((sv.select).getInvisible()))
							.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
										 
						 <input type="radio" 
							 value='<%= sv.select.getFormData() %>' 
							 onFocus='doFocus(this)' onHelp='return fieldHelp("sa513screensfl" + "." +
							 "select")' onKeyUp='return checkMaxLength(this)' 
							 name='sa513screensfl.select_R<%=count%>'
							 id='sa513screensfl.select_R<%=count%>'
							 onClick="selectedRow('sa513screensfl.select_R<%=count%>')"
							 class="UICheck"
						 />
						<%}%>
						</td>

					    <%-- <td style="min-width: 90px;" 
						<%if((sv.notifnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
						<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sa513screensfl" + "." +
						      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'>									
							<%= sv.notifnum.getFormData()%>
						 </td> --%>
						 
					    <td  style="min-width: 70px;" 
						<%if((sv.tranno).getClass().getSimpleName().equals("FixedLengthStringData")) {%>align="left"<% }else {%> align="right" <%}%> >									
						<%= sv.tranno.getFormData()%>
						</td>
						
					    <td  style="min-width: 90px;" 
						<%if((sv.transDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%> >									
						<%= sv.transDateDisp.getFormData()%>
						</td>
									
				    	<td  style="min-width: 90px;" 
						<%if((sv.effDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="left"<% }else {%> align="right" <%}%> >									
							<%= sv.effDateDisp.getFormData()%>
						 </td>
						 
		  				<td style="min-width: 90px;"
						<%if((sv.trancd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.trancd.getFormData()%>
						</td>
						
						<td  style="min-width: 90px;" 
						<%if((sv.trandes).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.trandes.getFormData()%>
						</td>
						
						<td  style="min-width: 90px;" 
						<%if((sv.userid).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.userid.getFormData()%>
						</td>
					
					</tr>
				
					<%
					count = count + 1;
					Sa513screensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
<script>

	$(document).ready(function() {
    	$('#dataTables-sa513').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollX: true,
        	scrollCollapse:true,
        	moreBtn: true,
        	
      	});
    	fixedColumns: true

    });
</script>   


<%@ include file="/POLACommon2NEW.jsp"%>
