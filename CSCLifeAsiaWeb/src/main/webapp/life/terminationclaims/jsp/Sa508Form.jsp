<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sa508";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>

<%
	Sa508ScreenVars sv = (Sa508ScreenVars) fw.getVariables();
%>
<%
	sv.cltreln.setClassString("");
%>
<%
	sv.cltreln.appendClassString("string_fld");
		sv.cltreln.appendClassString("input_txt");
		sv.cltreln.appendClassString("underline");
%>
<%
	{
		if (appVars.ind03.isOn()) {
			sv.cltreln.setReverse(BaseScreenData.REVERSED);
			sv.cltreln.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.cltreln.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind109.isOn()) {
			sv.incurdtDisp.setReverse(BaseScreenData.REVERSED);
			sv.incurdtDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind109.isOn()) {
			sv.incurdtDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind125.isOn()) {
			sv.clntsel.setReverse(BaseScreenData.REVERSED);
			sv.clntsel.setColor(BaseScreenData.RED);
		}
		if (appVars.ind25.isOn()) {
			sv.clntsel.setEnabled(BaseScreenData.DISABLED);
		}
		appVars.setFieldChange(sv.clntsel, appVars.ind50);
		if (!appVars.ind125.isOn()) {
			sv.clntsel.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind01.isOn()) {
			sv.cltdodxDisp.setReverse(BaseScreenData.REVERSED);
			sv.cltdodxDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.cltdodxDisp.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind114.isOn()) {
			sv.causedeath.setReverse(BaseScreenData.REVERSED);
			sv.causedeath.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind114.isOn()) {
			sv.causedeath.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind115.isOn()) {
			sv.rgpytype.setReverse(BaseScreenData.REVERSED);
			sv.rgpytype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind115.isOn()) {
			sv.rgpytype.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind119.isOn()) {
			sv.hospitalLevel.setReverse(BaseScreenData.REVERSED);
			sv.hospitalLevel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind119.isOn()) {
			sv.hospitalLevel.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind120.isOn()) {
			sv.diagcde.setReverse(BaseScreenData.REVERSED);
			sv.diagcde.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind120.isOn()) {
			sv.diagcde.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind122.isOn()) {
			sv.admiDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.admiDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind122.isOn()) {
			sv.admiDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind121.isOn()) {
			sv.dischargeDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.dischargeDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind121.isOn()) {
			sv.dischargeDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		
		
		if (appVars.ind112.isOn()) {
			sv.inctype.setReverse(BaseScreenData.REVERSED);
			sv.inctype.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind112.isOn()) {
			sv.inctype.setHighLight(BaseScreenData.BOLD);
		}
		
		
		if (appVars.ind05.isOn()) {
			sv.cltreln.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind06.isOn()) {
			sv.incurdtDisp.setEnabled(BaseScreenData.DISABLED);
		}
			
		if (appVars.ind07.isOn()) {
			sv.location.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind08.isOn()) {
			sv.inctype.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind09.isOn()) {
			sv.cltdodxDisp.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind10.isOn()) {
			sv.causedeath.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind11.isOn()) {
			sv.rgpytype.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind11.isOn()) {
			sv.rgpytype.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind12.isOn()) {
			sv.clamamt.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind13.isOn()) {
			sv.zdoctor.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind14.isOn()) {
			sv.zmedprv.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind15.isOn()) {
			sv.hospitalLevel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.diagcde.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.admiDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		
		if (appVars.ind18.isOn()) {
			sv.dischargeDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.accdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.notifhistory.setReverse(BaseScreenData.REVERSED);
			sv.notifhistory.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind23.isOn()) {
        	sv.notifhistory.setHighLight(BaseScreenData.BOLD);
        }
        if (!appVars.ind124.isOn()) {
        	sv.notifhistory.setEnabled(BaseScreenData.INVISIBLE);
        }
       /*  if (!appVars.ind22.isOn()) {
        	sv.notifnotes.setInvisibility(BaseScreenData.INVISIBLE);
        } */
		//ICIL-1484
		if (appVars.ind116.isOn()) {
			sv.clamamt.setReverse(BaseScreenData.REVERSED);
			sv.clamamt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind116.isOn()) {
			sv.clamamt.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind117.isOn()) {
			sv.zdoctor.setReverse(BaseScreenData.REVERSED);
			sv.zdoctor.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind117.isOn()) {
			sv.zdoctor.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind118.isOn()) {
			sv.zmedprv.setReverse(BaseScreenData.REVERSED);
			sv.zmedprv.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind118.isOn()) {
			sv.zmedprv.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Claim Notification Number")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.notifinum.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.notifinum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.notifinum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifcnum.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifcnum.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
									style="min-width: 80px; margin-left: 2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">

				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("ID Type")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.lidtype.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lidtype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lidtype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>

			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("ID Number")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.lsecuityno.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lsecuityno.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lsecuityno.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Gender")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.lcltsex.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lcltsex.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lcltsex.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<div class="row">
			<div class="col-md-2">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Claimant")%></label>
				<table>
					<tr>
						<td>
							
						<div class="form-group">
						<%
				          if ((new Byte((sv.clntsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>                   
						<div style="margin-right: 2px">	<%=smartHF.getHTMLVarExt(fw, sv.clntsel)%></div>			
				        <%
							} else {
						%>
				        <div class="input-group" style="width: 125px;">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.clntsel)%>
				             <span class="input-group-btn">
				             <button class="btn btn-info"
				                type="button"
				                onClick="doFocus(document.getElementById('clntsel')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
				        <%
				         }
				        %>							
							</div>	
								</td>
								<td>								
								<%
								longValue = null;
								formatValue = null;
								%>
								
						
						
							<div
								style="min-width: 100px; max-width: 105px; margin-left: -1px;"
								class='<%=(sv.cltname.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%> '>
								<%
									if (!((sv.cltname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cltname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									} else {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cltname.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}
									}
								%>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
					</tr>
				</table>

			</div>
			
			<div class="col-md-2"></div>
				  <div class="col-md-2">
				<div class="form-group" style="width: 190px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship to Life Assured")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "cltreln" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("cltreln");
						optionValue = makeDropDownList(mappedItems, sv.cltreln.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.cltreln.getFormData()).toString().trim());
						
						
					%>

					<% 
						if((new Byte((sv.cltreln).getEnabled()))
						.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
					%> 
						<div class='output_cell' style="width: 190px;">
							<%=longValue==null?"":XSSFilter.escapeHtml(longValue) %>
						</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<select name='cltreln' type='list' id="cltreln"
								onFocus='doFocus(this)'
                                onHelp='return fieldHelp(cltreln)'
                                onKeyUp='return checkMaxLength(this)'
						<%if ((new Byte((sv.cltreln).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="min-width:220px;" 
						<%}else if("red".equals((sv.cltreln).getColor())){%>
							class= "input_cell red reverse" 
						<%} else {%>
							class="input_cell"
						<%}%>>
						<%=optionValue%>
					</select>
					<%
					optionValue=null;
					longValue=null;
						}
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("ID Type")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.cidtype.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cidtype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cidtype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("ID Number")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.csecuityno.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.csecuityno.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.csecuityno.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Gender")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.ccltsex.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ccltsex.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ccltsex.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.cltpcode.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltpcode.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cltpcode.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
				<div class="col-md-2"></div>
				<div class="col-md-2">
					<div class="form-group" style="width: 200px;">
						<label><%=resourceBundleHandler
					.gettingValueFromBundle("Contact Number")%></label>
						<div style="width: 200px;">
							<%
								if (!((sv.cltphoneidd.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cltphoneidd.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.cltphoneidd.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
								style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Email Address")%></label>
					<div style="width: 200px;">
						<%
							if (!((sv.rinternet.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rinternet.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rinternet.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-2">
											<div class="form-group" class='output_cell'>
												<label><%=resourceBundleHandler.gettingValueFromBundle("Address")%></label>
												<table>
													<tr width="300px">
														<td >
														<% sv.cltaddr01.setEnabled(BaseScreenData.DISABLED); %>
														<%=smartHF.getHTMLVarExt(fw, sv.cltaddr01)%>
														</td>
														
														
													</tr>
												</table>
												
												
												<table>
													<tr width="300px">
														<td >
														<% sv.cltaddr02.setEnabled(BaseScreenData.DISABLED); %>
														<%=smartHF.getHTMLVarExt(fw, sv.cltaddr02)%>
														
														
														</td>
													</tr>
												</table>
												
												<table>
													<tr  width="300px">
														<td >
														<% sv.cltaddr03.setEnabled(BaseScreenData.DISABLED); %>
														<%=smartHF.getHTMLVarExt(fw,sv.cltaddr03)%>
														
														</td>
													</tr>
												</table>
												
												
												<table>
													<tr  width="300px">
														<td >
														<% sv.cltaddr04.setEnabled(BaseScreenData.DISABLED); %>
														<%=smartHF.getHTMLVarExt(fw,sv.cltaddr04)%>
														
														</td>
													</tr>
												</table>
												
												<table>
													<tr  width="300px">
														<td >
														<% sv.cltaddr05.setEnabled(BaseScreenData.DISABLED); %>
														<%=smartHF.getHTMLVarExt(fw,sv.cltaddr05)%>
														
														</td>
													</tr>
												</table>
											
											</div>
										</div>

			</div>
		<br>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Incident Information")%></label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Notification Date")%></label>
					<div style="width: 80px;">
						<%
								if (!((sv.notifiDateDisp.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.notifiDateDisp.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.notifiDateDisp.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
							%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
								longValue = null;
								formatValue = null;
							%>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Incur Date")%></label>
					<%
							if ((new Byte((sv.incurdtDisp).getEnabled())).compareTo(new Byte(
									BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.incurdtDisp)%></div>
					<%
							} else {
						%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='incurdtDisp' type='text'
							value='<%=sv.incurdtDisp.getFormData()%>'
							maxLength='<%=sv.incurdtDisp.getLength()%>'
							size='<%=sv.incurdtDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(incurdtDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.incurdtDisp).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
								} else if ((new Byte((sv.incurdtDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								} else {
							%>

						class = '
						<%=(sv.incurdtDisp).getColor() == null ? "input_cell"
							: (sv.incurdtDisp).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>' > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								}
							%>
						<%
								longValue = null;
									formatValue = null;
							%>

					</div>

					<%
							}
						%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group" style="width: 250px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Location")%></label>
					<div><%=smartHF.getHTMLVarExt(fw, sv.location)%></div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 150px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Incident Type")%></label>
					<%
							if ((new Byte((sv.inctype).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "inctype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("inctype");
								optionValue = makeDropDownList(mappedItems,
										sv.inctype.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.inctype.getFormData())
										.toString().trim());
						%>
					<%=smartHF.getDropDownExt(sv.inctype, fw, longValue,
						"inctype", optionValue)%>
					<%
							}
						%>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Date of Death")%></label>
					<%
							if ((new Byte((sv.cltdodxDisp).getEnabled())).compareTo(new Byte(
									BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.cltdodxDisp)%></div>
					<%
							} else {
						%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='cltdodxDisp' type='text'
							value='<%=sv.cltdodxDisp.getFormData()%>'
							maxLength='<%=sv.cltdodxDisp.getLength()%>'
							size='<%=sv.cltdodxDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(cltdodxDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.cltdodxDisp).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
								} else if ((new Byte((sv.cltdodxDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								} else {
							%>

						class = '
						<%=(sv.cltdodxDisp).getColor() == null ? "input_cell"
							: (sv.cltdodxDisp).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>' > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								}
							%>
						<%
								longValue = null;
									formatValue = null;
							%>

					</div>

					<%
							}
						%>
				</div>
			</div>

			

			<div class="col-md-2">
				<div class="form-group" style="width: 140px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Cause of Death")%></label>
					<%
							if ((new Byte((sv.causedeath).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "causedeath" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("causedeath");
								optionValue = makeDropDownList(mappedItems,
										sv.causedeath.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.causedeath.getFormData())
										.toString().trim());
						%>
					<%=smartHF.getDropDownExt(sv.causedeath, fw, longValue,
						"causedeath", optionValue)%>
					<%
							}
						%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="width: 150px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Regular Claim Reason")%></label>
					<%
							if ((new Byte((sv.rgpytype).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "rgpytype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("rgpytype");
								optionValue = makeDropDownList(mappedItems,
										sv.rgpytype.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.rgpytype.getFormData())
										.toString().trim());
						%>
					<%=smartHF.getDropDownExt(sv.rgpytype, fw, longValue,
						"rgpytype", optionValue)%>
					<%
							}
						%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount")%></label>
					<div style="width: 150px;">
						<%
                            qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
                            //qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
                            valueThis = smartHF.getPicFormatted(qpsf, sv.clamamt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
                        %>

						<input name='clamamt' type='text'
							<%if ((sv.clamamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%> value='<%=valueThis%>'
							<%if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.clamamt.getLength(), sv.clamamt.getScale(), 3)%>'
							maxLength='<%=sv.clamamt.getLength()%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(clamamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.clamamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.clamamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.clamamt).getColor() == null ? "input_cell"
						: (sv.clamamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Doctor")%>
					</label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.zdoctor).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
											|| fw.getVariables().isScreenProtected()) {
								%>

								<div class="input-group" style="width: 130px;">
									<%=smartHF.getHTMLVarExt(fw, sv.zdoctor)%>
									<span class="input-group-addon"><span
										style="font-size: 19px;"><span
											class="glyphicon glyphicon-search"></span></span></span>

								</div> <%
 	} else {
 %>
								<div class="input-group" style="width: 130px;">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.zdoctor)%>
									<span class="input-group-btn">
										<button class="btn btn-info"
											style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
											type="button"
											onClick="doFocus(document.getElementById('zdoctor')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Medical Provider")%></label>
					<%
                                         if ((new Byte((sv.zmedprv).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>

					<div class="input-group" style="width: 130px;">
						<%=smartHF.getHTMLVarExt(fw, sv.zmedprv)%>


					</div>
					<%
                                         } else {
                                  %>
					<div class="input-group" style="width: 130px;">
						<%=smartHF.getRichTextInputFieldLookup(fw, sv.zmedprv)%>
						<span class="input-group-btn">
							<button class="btn btn-info"
								style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
								type="button"
								onClick="doFocus(document.getElementById('zmedprv')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
					<%
                                         }
                                  %>

				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group" style="width: 150px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Hospital Level")%></label>
					<%
							if ((new Byte((sv.hospitalLevel).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "hospitalLevel" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("hospitalLevel");
								optionValue = makeDropDownList(mappedItems,
										sv.hospitalLevel.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.hospitalLevel.getFormData())
										.toString().trim());
						%>
					<%=smartHF.getDropDownExt(sv.hospitalLevel, fw, longValue,
						"hospitalLevel", optionValue)%>
					<%
							}
						%>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group" style="width: 150px;">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Diagnosis")%></label>
					<%
							if ((new Byte((sv.diagcde).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "diagcde" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("diagcde");
								optionValue = makeDropDownList(mappedItems,
										sv.diagcde.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.diagcde.getFormData())
										.toString().trim());
						%>
					<%=smartHF.getDropDownExt(sv.diagcde, fw, longValue,
						"diagcde", optionValue)%>
					<%
							}
						%>
				</div>
			</div>


		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Admit Date")%></label>
					<%
							if ((new Byte((sv.admiDateDisp).getEnabled())).compareTo(new Byte(
									BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.admiDateDisp)%></div>
					<%
							} else {
						%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='admiDateDisp' type='text'
							value='<%=sv.admiDateDisp.getFormData()%>'
							maxLength='<%=sv.admiDateDisp.getLength()%>'
							size='<%=sv.admiDateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(admiDateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.admiDateDisp).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
								} else if ((new Byte((sv.admiDateDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								} else {
							%>

						class = '
						<%=(sv.admiDateDisp).getColor() == null ? "input_cell"
							: (sv.admiDateDisp).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>' > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								}
							%>
						<%
								longValue = null;
									formatValue = null;
							%>

					</div>

					<%
							}
						%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Discharge Date")%></label>
					<%
							if ((new Byte((sv.dischargeDateDisp).getEnabled())).compareTo(new Byte(
									BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
					<div style="width: 120px;"><%=smartHF.getRichTextDateInput(fw, sv.dischargeDateDisp)%></div>
					<%
							} else {
						%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy" style="width: 150px;">
						<input name='dischargeDateDisp' type='text'
							value='<%=sv.dischargeDateDisp.getFormData()%>'
							maxLength='<%=sv.dischargeDateDisp.getLength()%>'
							size='<%=sv.dischargeDateDisp.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(dischargeDateDisp)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.dischargeDateDisp).getEnabled())).compareTo(new Byte(
						BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables()
								.isScreenProtected())) {%>
							readonly="true" class="output_cell">

						<%
								} else if ((new Byte((sv.dischargeDateDisp).getHighLight()))
											.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
							%>
						class="bold_cell" > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								} else {
							%>

						class = '
						<%=(sv.dischargeDateDisp).getColor() == null ? "input_cell"
							: (sv.dischargeDateDisp).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>' > <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>

						<%
								}
							%>
						<%
								longValue = null;
									formatValue = null;
							%>

					</div>

					<%
							}
						%>
				</div>
			</div>


		</div>

		<div class="row">
			<div class="col-md-4" style='width: 100% !important;'>
				<!-- IFSU-2577 -->
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accident Description")%></label>
					<div class="tabblock" id="navbar">
						<!-- IFSU-414 Modified it for crossing browsers by xma3 -->
						<div class="tabcontent">
							<div id="content1">
								<textarea name="accdesc" id='accdesc' value=''
									style='width: 100%; height: 335px; z-index: 2;'
									onFocus='doFocus(this)' onhelp="return fieldHelp('accdesc')"
									 class="textarea bold"
									<%if ((new Byte((sv.accdesc).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
													|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
									readonly="true" disabled class="output_cell"
									<%} else if ((new Byte((sv.accdesc).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
									class="bold_cell" <%} else {%> class='input_cell' <%}%>><%=sv.accdesc.getFormData().toString().trim() %></textarea>
								<!-- IFSU-2577 -->
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		
	
		
</div>


	</div>
	<style>
#mainareaDiv>div>div.panel.panel-default>div>div:nth-child(18)>div:nth-child(2)>div>div>div
	{
	width: 165px !important;
}
</style>

<div>
</div>
<div>
</div>
 <Div id='mainForm_OPTS' style='visibility: hidden;'>
        <%=smartHF.getMenuLink(sv.fupflg, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>
        <%=smartHF.getMenuLink(sv.invresults, resourceBundleHandler.gettingValueFromBundle("Investigation History"))%>
        <%=smartHF.getMenuLink(sv.notifnotes, resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes History"))%>
        <%if ((new Byte((sv.notifhistory).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>
								<%=smartHF.getMenuLink(sv.notifhistory, resourceBundleHandler.gettingValueFromBundle("Notification History"))%>
								<%
							}
						%>
    </div>

<div>
</div>

	<%@ include file="/POLACommon2NEW.jsp"%>
