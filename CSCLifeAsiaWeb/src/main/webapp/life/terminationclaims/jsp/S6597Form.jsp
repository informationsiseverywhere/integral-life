<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6597";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6597ScreenVars sv = (S6597ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
						<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
					</div>
				</div>
			
			
			<!-- <div class="col-md-3"></div> -->
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
						<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'  style="max-width:75px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
				    </div>
				 </div>   	
				 
				<!--  <div class="col-md-2"></div> -->
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<!-- <div class="input-group"> -->
					<table><tr><td>	
					<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'style="max-width:800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
		<%
		longValue = null;
		formatValue = null;
		%> 
					<!-- </div> -->
					</td></tr></table>
				</div>
			</div>
		</div>				
		
		 <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Valid From"))%></label>
        				<table>
        				<tr>
        				<td>
        				<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		
		<td>&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;
		<td>
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        		</div>
        	</div> <div class="col-md-4"></div> <div class="col-md-4"></div>
        </div>	
        &nbsp;
         <div class="row">
              <div class="col-md-2">
                <div class="form-group">
         				<%StringData S6597_Duration_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duration(Months)");%>
                        <%=smartHF.getLit(0, 0, S6597_Duration_LBL).replace("absolute","relative")%>
        		</div>
        		</div>
        		
        		<div class="col-md-1"></div>
        		
        		<div class="col-md-2">
                <div class="form-group">
         				<%StringData S6597_Subroutine_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subroutine");%>
                        <%=smartHF.getLit(0, 0, S6597_Subroutine_LBL).replace("absolute","relative")%>
        		</div>
        		</div>
        		
        		<div class="col-md-1"></div>
        		
        		<div class="col-md-2">
                <div class="form-group">
         				<%StringData S6597_Risk_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status");%>
                        <%=smartHF.getLit(0, 0, S6597_Risk_LBL).replace("absolute","relative")%>
        		</div>
        		</div>
        		
        		<div class="col-md-1"></div>
        		
        		<div class="col-md-2">
                <div class="form-group">
         				<%StringData S6597_Prem_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem Status");%>
                        <%=smartHF.getLit(0, 0, S6597_Prem_LBL).replace("absolute","relative")%>
        		</div>
        		</div>
        	<div class="col-md-1"></div>	
        	</div>	
        	
        	<div class="row">
              <div class="col-md-2">
                <div class="form-group" style="max-width:100px;">
                <%if(((BaseScreenData)sv.durmnth01) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.durmnth01,( sv.durmnth01.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.durmnth01) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                 hello
                 <%}%>
                </div>
               </div>
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group">
                <%if(((BaseScreenData)sv.premsubr01) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.premsubr01,( sv.premsubr01.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.premsubr01) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                hello
                <%}%>
                </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2" >
                <div class="form-group" style="max-width:75px;">
                <%if(((BaseScreenData)sv.crstat01) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.crstat01,( sv.crstat01.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.crstat01) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.crstat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                 hello
                 <%}%>
                </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">
                 <%if(((BaseScreenData)sv.cpstat01) instanceof StringBase) {%>
                 <%=smartHF.getRichText(0,0,fw,sv.cpstat01,( sv.cpstat01.getLength()+1),null).replace("absolute","relative")%>
                 <%}else if (((BaseScreenData)sv.cpstat01) instanceof DecimalData){%>
                 <%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                 <%}else {%>
                  hello
                  <%}%>
                </div>
               </div> 
              </div>  	
              
              <div class="row">
              <div class="col-md-2">
                <div class="form-group" style="max-width:100px;">
                <%if(((BaseScreenData)sv.durmnth02) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.durmnth02,( sv.durmnth02.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.durmnth02) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                 <%}else {%>
                 hello
                 <%}%>
                </div>
              </div>  
              
              <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group">
                  <%if(((BaseScreenData)sv.premsubr02) instanceof StringBase) {%>
                  <%=smartHF.getRichText(0,0,fw,sv.premsubr02,( sv.premsubr02.getLength()+1),null).replace("absolute","relative")%>
                  <%}else if (((BaseScreenData)sv.premsubr02) instanceof DecimalData){%>
                  <%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                  <%}else {%>
                   hello
                   <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">	
                <%if(((BaseScreenData)sv.crstat02) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.crstat02,( sv.crstat02.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.crstat02) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.crstat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                 hello
                 <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">
                <%if(((BaseScreenData)sv.cpstat02) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.cpstat02,( sv.cpstat02.getLength()+1),null).replace("absolute","relative")%>
               <%}else if (((BaseScreenData)sv.cpstat02) instanceof DecimalData){%>
               <%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
               <%}else {%>
               hello
               <%}%>
                </div>
               </div> 
              </div>  	
              
               <div class="row">
              <div class="col-md-2">
                <div class="form-group" style="max-width:100px;">
                <%if(((BaseScreenData)sv.durmnth03) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.durmnth03,( sv.durmnth03.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.durmnth03) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                hello
                <%}%>
                </div>
              </div>  
              
              <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group">
                <%if(((BaseScreenData)sv.premsubr03) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.premsubr03,( sv.premsubr03.getLength()+1),null).replace("absolute","relative")%>
                 <%}else if (((BaseScreenData)sv.premsubr03) instanceof DecimalData){%>
                 <%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                 <%}else {%>
                 hello
                 <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">	
                <%if(((BaseScreenData)sv.crstat03) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.crstat03,( sv.crstat03.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.crstat03) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.crstat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                <%}else {%>
                hello
                <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">
                <%if(((BaseScreenData)sv.cpstat03) instanceof StringBase) {%>
                <%=smartHF.getRichText(0,0,fw,sv.cpstat03,( sv.cpstat03.getLength()+1),null).replace("absolute","relative")%>
                <%}else if (((BaseScreenData)sv.cpstat03) instanceof DecimalData){%>
                <%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
               <%}else {%>
               hello
               <%}%>
                </div>
               </div> 
              </div>
              
               <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                <label><%=resourceBundleHandler.gettingValueFromBundle("Lapse")%></label>
                </div>
              </div>  
              
              <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group">
                <%if(((BaseScreenData)sv.premsubr04) instanceof StringBase) {%>
                 <%=smartHF.getRichText(0,0,fw,sv.premsubr04,( sv.premsubr04.getLength()+1),null).replace("absolute","relative")%>
                  <%}else if (((BaseScreenData)sv.premsubr04) instanceof DecimalData){%>
                  <%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                  <%}else {%>
                   hello
                   <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">	
                  <%if(((BaseScreenData)sv.crstat04) instanceof StringBase) {%>
                  <%=smartHF.getRichText(0,0,fw,sv.crstat04,( sv.crstat04.getLength()+1),null).replace("absolute","relative")%>
                  <%}else if (((BaseScreenData)sv.crstat04) instanceof DecimalData){%>
                  <%=smartHF.getHTMLVar(0, 0, fw, sv.crstat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                  <%}else {%>
                  hello
                  <%}%>
                 </div>
               </div> 
               
               <div class="col-md-1"></div>
               
               <div class="col-md-2">
                <div class="form-group"  style="max-width:75px;">
                  <%if(((BaseScreenData)sv.cpstat04) instanceof StringBase) {%>
                  <%=smartHF.getRichText(0,0,fw,sv.cpstat04,( sv.cpstat04.getLength()+1),null).replace("absolute","relative")%>
                  <%}else if (((BaseScreenData)sv.cpstat04) instanceof DecimalData){%>
                  <%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                  <%}else {%>
                   hello
                   <%}%>
                </div>
               </div> 
              </div>
        
        </div>
        </div>
        	
































<%-- <div class='outerDiv' style='width:750;height:500;overflow:auto;'>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData COMPANY_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
<%=smartHF.getLit(0, 0, COMPANY_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
<br/>
<%if ((new Byte((sv.company).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.company.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.company.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<!-- ILIFE2671 starts -->
<style>
/* for IE 8 */
@media \0screen\,screen\9
{
.output_cell{margin-right:3px;}
}
</style>
<!-- ILIFE2671 ends -->
<td width='251'>
<!--
<%StringData TABL_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table");%>
<%=smartHF.getLit(0, 0, TABL_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>
<br/>
<%if ((new Byte((sv.tabl).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData ITEM_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item");%>
<%=smartHF.getLit(0, 0, ITEM_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>
<br/>
<%if ((new Byte((sv.item).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.item.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.item.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=formatValue%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td>
</tr> </table>
<br/>
<table>
<tr style='height:22px;'><td width='251'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Valid From")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
&nbsp;




<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("To")%>
</div>

&nbsp;


	
  		
		<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=formatValue%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr></table>
<br/>
<table width='100%'>
<tr style='height:22px;'>
<td width='251'>
<!--
<%StringData S6597_Duration_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duration(Months)");%>
<%=smartHF.getLit(0, 0, S6597_Duration_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Duration(Months)")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData S6597_Subroutine_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Subroutine");%>
<%=smartHF.getLit(0, 0, S6597_Subroutine_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Subroutine")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData S6597_Risk_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status");%>
<%=smartHF.getLit(0, 0, S6597_Risk_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<!--
<%StringData S6597_Prem_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem Status");%>
<%=smartHF.getLit(0, 0, S6597_Prem_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Prem Status")%>
</div>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.durmnth01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.durmnth01,( sv.durmnth01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.durmnth01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.premsubr01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premsubr01,( sv.premsubr01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premsubr01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.crstat01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.crstat01,( sv.crstat01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.crstat01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.crstat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.cpstat01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.cpstat01,( sv.cpstat01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.cpstat01) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.durmnth02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.durmnth02,( sv.durmnth02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.durmnth02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.premsubr02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premsubr02,( sv.premsubr02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premsubr02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.crstat02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.crstat02,( sv.crstat02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.crstat02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.crstat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.cpstat02) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.cpstat02,( sv.cpstat02.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.cpstat02) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<%if(((BaseScreenData)sv.durmnth03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.durmnth03,( sv.durmnth03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.durmnth03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.durmnth03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.premsubr03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premsubr03,( sv.premsubr03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premsubr03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.crstat03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.crstat03,( sv.crstat03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.crstat03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.crstat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.cpstat03) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.cpstat03,( sv.cpstat03.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.cpstat03) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 4,7 etc --> 
</tr>
 <tr style='height:22px;'>
<td width='251'>
<!--
<%StringData SUBR04_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lapse");%>
<%=smartHF.getLit(0, 0, SUBR04_LBL).replace("absolute","relative")%>
-->
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Lapse")%>
</div>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.premsubr04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premsubr04,( sv.premsubr04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premsubr04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premsubr04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.crstat04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.crstat04,( sv.crstat04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.crstat04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.crstat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td><!-- END TD FOR ROW 2,5 etc -->
<td width='251'>
<%if(((BaseScreenData)sv.cpstat04) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.cpstat04,( sv.cpstat04.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.cpstat04) instanceof DecimalData){%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.cpstat04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%}else {%>
hello
<%}%>
</td>
</tr> </table>
<br/>
</div> --%>
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
