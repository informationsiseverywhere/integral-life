

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5318";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5318ScreenVars sv = (S5318ScreenVars) fw.getVariables();%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Claim Details -----------------------------------------------");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dth. Date");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pol Lns ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Poldebt ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Intrest ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff. Date ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Oth Adj ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Off Crg ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cause of Death");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Claim Payable ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Est Tot ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currncy ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Deposit ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Amount Payable ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Adj Reason    ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Follow ups       ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Hold Claim Amount ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Hold Adjustment ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contact Date ");%>
	
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.dtofdeathDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.dtofdeathDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind10.isOn()) {
			sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.bnfying.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
            sv.bnfying.setReverse(BaseScreenData.REVERSED);
            sv.bnfying.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind21.isOn()) {
            sv.bnfying.setHighLight(BaseScreenData.BOLD);
        }
        
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setReverse(BaseScreenData.REVERSED);
			sv.currcd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.causeofdth.setEnabled(BaseScreenData.DISABLED);
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.causeofdth.setColor(BaseScreenData.RED);
			sv.currcd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.causeofdth.setHighLight(BaseScreenData.BOLD);
			sv.currcd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind07.isOn()) {
			sv.otheradjst.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.otheradjst.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.fupflg.setReverse(BaseScreenData.REVERSED);
			sv.fupflg.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.fupflg.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.reasoncd.setReverse(BaseScreenData.REVERSED);
			sv.reasoncd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.reasoncd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.longdesc.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.longdesc.setReverse(BaseScreenData.REVERSED);
			sv.longdesc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.longdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
		    sv.claimnumber.setReverse(BaseScreenData.REVERSED);
		    sv.claimnumber.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind23.isOn()) {
		    sv.claimnumber.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind24.isOn()) {
		  	sv.claimnumber.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind25.isOn()) {
		    sv.aacct.setReverse(BaseScreenData.REVERSED);
		    sv.aacct.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind25.isOn()) {
		    sv.aacct.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind26.isOn()) {
		  	sv.aacct.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind27.isOn()) {
		    sv.claimnotes.setReverse(BaseScreenData.REVERSED);
		    sv.claimnotes.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind27.isOn()) {
		    sv.claimnotes.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind28.isOn()) {
		  	sv.claimnotes.setInvisibility(BaseScreenData.INVISIBLE);
		}			
		if (appVars.ind29.isOn()) {
		    sv.investres.setReverse(BaseScreenData.REVERSED);
		    sv.investres.setColor(BaseScreenData.RED);
	    }
		 if (!appVars.ind29.isOn()) {
		    sv.investres.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind30.isOn()) {
		  	sv.investres.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}
		if (appVars.ind80.isOn()) {
			sv.claimStat.setInvisibility(BaseScreenData.INVISIBLE);
		}
        if (appVars.ind78.isOn()) {
            sv.claimStat.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind78.isOn()) {
            sv.claimStat.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind79.isOn()) {
			sv.claimStat.setEnabled(BaseScreenData.DISABLED);
		}
        
        if (appVars.ind81.isOn()) {
			sv.claimTyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
        if (appVars.ind85.isOn()) {
            sv.claimTyp.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind85.isOn()) {
            sv.claimTyp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind86.isOn()) {
			sv.claimTyp.setEnabled(BaseScreenData.DISABLED);
		}
        
        if (appVars.ind82.isOn()) {
			sv.contactDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}	
        if (appVars.ind83.isOn()) {
            sv.contactDateDisp.setColor(BaseScreenData.RED);
        }
        if (!appVars.ind83.isOn()) {
            sv.contactDateDisp.setHighLight(BaseScreenData.BOLD);
        }
        if (appVars.ind84.isOn()) {
			sv.contactDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk/Prem Status ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RCD ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"J/Life");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid-to-date ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed-to-date ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"---------------- Coverage/Rider Review ---------------------------------------");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                          Lien                 Estimated              Actual");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov/Rd Fund Typ Descriptn.Code     Ccy             Value               Value");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Status ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Claim Type ");%>
<%		appVars.rollup(new int[] {93});
%>



 <div class="panel panel-default">
      <div class="panel-body">     

			 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
                         <table>
                         <tr>
                         <td>

<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
 </td>

<td >


<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' Style="min-width:40px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
 </td>

<td >


<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="cntdesc" style="margin-left: 1px;max-width: 160px;" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
 </td>
 </tr>
 </table>
  </div></div>
  
 
  
  
  <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Premium Status")%></label>
                         <table>
                         <tr>
                         <td>
                         
                         
<%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
 </td>

<td >
  <%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div id="rskstat" class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;margin-left: 1px;max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
 </td>
 </tr>
 </table>
  </div></div>
                         
                         
                         
          <div class="col-md-4"> 
			    	  <div class="form-group">
                    <!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->	
                       <%--   <%if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> --%>
	
  		
		<%					
		if(!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.occdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 <%--  <%}%> --%>
  
  </div></div>
                         
  </div>
  
  
  
   <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
                       <table>
                       <tr>
                       <td>
                         
                         <%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:70px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
 </td>

<td >



<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:120px;max-width:195px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
                         
                         
  </td>
  </tr>
  </table>
                         
                         
                         </div></div>
                         
                   
                   
               
                         
             <div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
                        <table>
                        <tr>
                        <td>
                         
                         <%if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
 </td>

<td>


<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

 </td>

<td>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:120px;max-width:160px;;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  
 </td>
 </tr>
 </table>
  
  </div></div>
                         
   
   
   	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
                         <table>
                         <tr>
                         <td>
                         
                         <%if ((new Byte((sv.asterisk).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.asterisk.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.asterisk.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' Style="min-width:70px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
 </td>

<td>

<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'Style="min-width:70px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
 </td>

<td>


<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:70px;margin-left: 2px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
                         
 </td>
 </tr>
 </table>                        
                  
 
  </div>
</div>
       
                         
</div>
  
  
  
  
  
  
  
   <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group" >
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
                        
                         
                         <%if ((new Byte((sv.ptdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
                         
                         
                     </div></div>
                     
                     
               		    	<div class="col-md-4"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>      
                     <%if ((new Byte((sv.btdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.btdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  </div></div>
  <%if ((new Byte((sv.claimnumber).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
                <div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim No.")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimnumber.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimnumber.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>  
			<%} %>
			<%if ((new Byte((sv.claimStat).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>   
			 <div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Status")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimStat.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimStat.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div> 
			<%} %>
			<%if ((new Byte((sv.claimTyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Type")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.claimTyp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.claimTyp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div> 
			<%} %>        

                     
                     </div>    
      <%if ((new Byte((sv.aacct).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
           <div class="row">
		<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Claim Notification Number")%></label>
					<div style="width: 80px;">
						<%
							if (!((sv.aacct.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.aacct.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.aacct.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
		<%} %>
                <div class="row">  
               		 <div class="col-md-4"> 
			    	 	 <div class="form-group">       
               			<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider Details")%></label>         
                  		</div>
                 	 </div>
                </div>       
                         
                         
         
         
         
         
        <div class="row">
  <div class="col-md-12">
    <div class="form-group">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id="dataTable-s5318" width='100%' style="border-right: thin solid #dddddd !important;">
	        <!--  <table id="s5417Table" class="s5417Table"> -->
	         
	         
	         <thead>
	            
		<tr class="info">     							
				<th colspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage/Rider")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Lien")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></th>
		<th rowspan="2" style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></th>
	      
	    </tr>	         
	         	      <tr>
		<%-- <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("  ")%></th>
		<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("  ")%></th> --%>
		
		</tr>    
	         </thead>
                         
                    <%
		GeneralTable sfl = fw.getTable("s5318screensfl");
		/* int height;
		if(sfl.count()*27 > 80) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	 */
		%>     
		
		<script language="javascript">
        $(document).ready(function(){
	
			new superTable("s5318Table", {
				fixedCols : 0,					
				colWidths : [60,60,80,80,100,80,80,100,100],
				headerRows:2,
				hasHorizonScroll :"Y",
				moreBtn: "Y",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
    
    
                         
            <tbody>
            
               <%
		
	S5318screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5318screensfl
	.hasMoreScreenRows(sfl)) {	
%>
            
            
            
            
            
            
            
            <tr >
						    									<td  style="width:50px;"
					<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																			
										<%if((new Byte((sv.coverage).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  style="width:50px;"
					<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.rider).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.vfund).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.vfund).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.vfund.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.fieldType).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >								
										<%if((new Byte((sv.fieldType).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.fieldType.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.shortds).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >								
										<%if((new Byte((sv.shortds).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.shortds.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.liencd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.liencd).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.liencd.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td 
					<%if((sv.cnstcur).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.cnstcur).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
									
											
						<%= sv.cnstcur.getFormData()%>
						
														 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.estMatValue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
										<%if((new Byte((sv.estMatValue).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.estMatValue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!(sv.estMatValue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									<%}%>
			</td>
				    									<td  
					<%if((sv.actvalue).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> style="text-align: right"<%}%> >								
										<%if((new Byte((sv.actvalue).getInvisible()))
						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.actvalue,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!(sv.actvalue).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									<%}%>
			</td>
					
	</tr>
            <%
		 
	count = count + 1;
	S5318screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
            
            </tbody>             
                         
                         
                        </table></div></div></div>
                      <%--   <div class="col-md-12"> 
                        
                        <div class="sectionbutton pull-right">
			<a href="javascript:;" onmouseout="changeMoreImageOut(this);" onmouseover="changeMoreImage(this);" onClick="doAction('PFKey90');" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("More")%></a>
			</div>
           
        
           	
           	
    <!--        	<script>
	$(document).ready(function() {
    	$('#dataTables-s5318').DataTable({
        	ordering: false,
        	searching:false
      	});
    });
</script> -->
                     
                        </div>
                         --%>
                        
                        
                        </div> 
                         
                         
                        
                         
  <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Details")%>  
  </label>
  
  
  <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width: 115px;">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Eff. Date")%></label>
                        
  <div class="input-group">
  <%	
	if ((new Byte((sv.effdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.effdateDisp.getFormData();  
%>

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<!-- <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="premcessDisp" data-link-format="dd/mm/yyyy"> -->


<input name='effdateDisp' 
type='text' 
value='<%=sv.effdateDisp.getFormData()%>' 
maxLength='<%=sv.effdateDisp.getLength()%>' 
size='<%=sv.effdateDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(effdateDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.effdateDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.effdateDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),   '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>
 --%>
 <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.effdateDisp).getColor()== null  ? 
"input_cell" :  (sv.effdateDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<%-- <a href="javascript:;" onClick="showCalendar(this, document.getElementById('effdateDisp'),   '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a> --%>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>

<%}longValue = null; }}%>


</div></div></div>


<%if ((new Byte((sv.contactDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contact Date")%></label>
 <% if ((new Byte((sv.contactDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {	%>
										<div>
											<%=smartHF.getRichTextDateInput(fw, sv.contactDateDisp)%>
							</div>
			<%}else{%>
				<div class="input-group date form_date col-md-5" data-date=""
					data-date-format="dd/mm/yyyy" data-link-field="contactDateDisp"
					data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.contactDateDisp)%>
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
						</span>
				</div>
		<%}%>
		</div> </div>
		<%} %>
<div class="col-md-3"> 
			    	  <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Dth. Date")%></label>
                         
                         
                        
 <% if ((new Byte((sv.dtofdeathDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {	%>
										<div>
											<%=smartHF.getRichTextDateInput(fw, sv.dtofdeathDisp)%>
							</div>
			<%}else{%>
				<div class="input-group date form_date col-md-5" data-date=""
					data-date-format="dd/mm/yyyy" data-link-field="dtofdeathDisp"
					data-link-format="dd/mm/yyyy" style="width: 150px;">
						<%=smartHF.getRichTextDateInput(fw, sv.dtofdeathDisp)%>
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
						</span>
				</div>
					
		<%}%>
		
		<%-- <%	
	if ((new Byte((sv.dtofdeathDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.dtofdeathDisp.getFormData();  
%>

<% 
	if((new Byte((sv.dtofdeathDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 
<input name='dtofdeathDisp' 
type='text' 
value='<%=sv.dtofdeathDisp.getFormData()%>' 
maxLength='<%=sv.dtofdeathDisp.getLength()%>' 
size='<%=sv.dtofdeathDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(dtofdeathDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.dtofdeathDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.dtofdeathDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="showCalendar(this, document.getElementById('dtofdeathDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.dtofdeathDisp).getColor()== null  ? 
"input_cell" :  (sv.dtofdeathDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="showCalendar(this, document.getElementById('dtofdeathDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>

<%}longValue = null; }}%>  
                          --%>
                         
                         
<%--                          <div style="width:180px">
                         <%	
	if ((new Byte((sv.dtofdeathDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	longValue = sv.dtofdeathDisp.getFormData();  
%>

<% 
	if((new Byte((sv.dtofdeathDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/MM/yyyy" data-link-field="dtofdeathDisp" data-link-format="dd/mm/yyyy">


<input name='dtofdeathDisp' 
type='text' 
value='<%=sv.dtofdeathDisp.getFormData()%>' 
maxLength='<%=sv.dtofdeathDisp.getLength()%>' 
size='<%=sv.dtofdeathDisp.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(dtofdeathDisp)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.dtofdeathDisp).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell"	>

<%
	}else if((new Byte((sv.dtofdeathDisp).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="showCalendar(this, document.getElementById('dtofdeathDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%
	}else { 
%>

class = ' <%=(sv.dtofdeathDisp).getColor()== null  ? 
"input_cell" :  (sv.dtofdeathDisp).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="showCalendar(this, document.getElementById('dtofdeathDisp'),  '<%= av.getAppConfig().getDateFormat()%>' ,null,0,-1,-1)"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/calendar.gif" border="0" class='iconPos'>
</a>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
<%}longValue = null; }}%>

</div></div> --%></div> </div>


           
                         
                         
                         
                         
                         
                         <div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Cause of Death")%></label>
                         
                         <%	
	if ((new Byte((sv.causeofdth).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"causeofdth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("causeofdth");
	optionValue = makeDropDownList( mappedItems , sv.causeofdth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.causeofdth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.causeofdth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.causeofdth).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='causeofdth' type='list' style="width:180px;"
<% 
	if((new Byte((sv.causeofdth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.causeofdth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.causeofdth).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>

</div></div> 


     
                         
                         <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Office Charge")%></label>
                         
<%	
			qpsf = fw.getFieldXMLDef((sv.ofcharge).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.ofcharge,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.ofcharge.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
		</div></div> 




</div>





<div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Pol Lns")%></label>
  <%if ((new Byte((sv.policyloan).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.policyloan,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 </div></div>
 
 
 
 
 <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Poldebt")%></label>
                       <%if ((new Byte((sv.tdbtamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.tdbtamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.tdbtamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div> 
                       
                       
                       
                       <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Interest")%></label>
                       
		<%	
			qpsf = fw.getFieldXMLDef((sv.interest).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.interest,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.interest.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</div></div>
                       
                       
                       
                       
                       <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Oth Adj")%></label>
                      <%if ((new Byte((sv.otheradjst).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.otheradjst,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
	%>

<input name='otheradjst' 
type='text'

<%if((sv.otheradjst).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.otheradjst.getLength(), sv.otheradjst.getScale(),3)%>'
maxLength='<%= sv.otheradjst.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(otheradjst)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.otheradjst).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.otheradjst).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.otheradjst).getColor()== null  ? 
			"input_cell" :  (sv.otheradjst).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>

</div></div>
 
 
 
 
 
 
 </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Currncy")%></label>
                       
                       <%	
	if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("currcd");
	optionValue = makeDropDownList( mappedItems , sv.currcd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.currcd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:180px;"> 
<%
} 
%>

<select name='currcd' type='list' style="width:180px;"
<% 
	if((new Byte((sv.currcd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.currcd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.currcd).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>
                       
                       
                       
                       
                       </div></div>
                       
                       
                       
                       
                       
  
  
  <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Cash Deposit")%></label>
                       
                       <%if ((new Byte((sv.zrcshamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zrcshamt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
                       
                       
                       
                       
                       </div></div>
                       
                       
                       
                       
                       
                       <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total Claim Payable")%></label>
                       <%if ((new Byte((sv.clamamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.clamamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.clamamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.clamamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
                       
                       
                       
                       
                       
                       </div></div>
                       
                       
                       
                       
                       
                       
                       
                       
                       <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Est Tot")%></label>
                       
                       
                       <%if ((new Byte((sv.estimateTotalValue).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.estimateTotalValue,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.estimateTotalValue.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
                       
                       
                       
                       </div></div>
                       
  
  
  
  </div>
  
  
  
  
  
  
  <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Adj Reason")%></label>
  
  <table><tr>
  <td>
  
  <%	
	if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("reasoncd");
	optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),1,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.reasoncd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=(sv.reasoncd.getFormData()).toString().trim()%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.reasoncd).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:100px;"> 
<%
} 
%>

<select name='reasoncd' type='list' style="width:120px;"
<% 
	if((new Byte((sv.reasoncd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.reasoncd).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
onchange="change()"
>
<%=optionValue%>
</select>
<% if("red".equals((sv.reasoncd).getColor())){
%>
</div>
<%
} 
%>
<%
longValue = null;
%>
<%
}} 
%>


</td><td style="padding-left:1px;">

<%if ((new Byte((sv.longdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


<input name='longdesc' 
type='text'

<%if((sv.longdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>

<%

		fieldItem=appVars.loadF4FieldsLong(new String[] {"reasoncd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("reasoncd");
		optionValue = makeDropDownList( mappedItems , sv.reasoncd.getFormData(),2,resourceBundleHandler);  
		formatValue = (String) mappedItems.get((sv.reasoncd.getFormData()).toString().trim());

%>

<%if(formatValue==null) {
	formatValue="";
}

 %>

<% String str=(sv.longdesc.getFormData()).toString().trim(); %>
			<% if(str.equals("") || str==null) {
				str=formatValue;
			}
			
		%>
 
	value='<%=str%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=str%>' <%}%>

size='50'
maxLength='50' 
style='width:200px'
onFocus='doFocus(this)' onHelp='return fieldHelp(longdesc)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.longdesc).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.longdesc).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.longdesc).getColor()== null  ? 
			"input_cell" :  (sv.longdesc).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
  </td></tr></table>
    
                   </div></div>
                   
                   
                   
                   <!-- changes add new field -->
                   	       	<% if (sv.defintflag.compareTo("N") != 0) { %>
  <div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
                       
                       <%if ((new Byte((sv.interestrate).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
			//ILIFE-1530 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.interestrate,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			//ILIFE-1530 ENDS
			if(!((sv.interestrate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
	 <%}%>
                    </div></div>
                    
  
  
  <%}%>
  
  
  
  
  
  		    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%></label>
  <%if ((new Byte((sv.susamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
			//ILIFE-1530 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.susamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			//ILIFE-1530 ENDS
			if(!((sv.susamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
  
                    </div></div>
                    
                    
                    
                    
                    		    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Unpaid Premium")%></label>
  
  <%if ((new Byte((sv.nextinsamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
			//ILIFE-1530 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.nextinsamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			//ILIFE-1530 ENDS
			if(!((sv.nextinsamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
                    </div></div>
                    
                    
                    
                    </div>
  
  
  
  
  
  <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Claim Amount Payable")%></label> 
  
  <%if ((new Byte((sv.totclaim).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.totclaim).getFieldName());
		//ILIFE-1530 STARTS
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.totclaim,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
		//ILIFE-1530 ENDS	
			if(!((sv.totclaim.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div>
  
  
  
  
  
  
  
  	    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Hold Claim Amount")%></label> 
                       
                       
                       <%if ((new Byte((sv.zhldclmv).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zhldclmv).getFieldName());
			
			formatValue = smartHF.getPicFormatted(qpsf,sv.zhldclmv,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.zhldclmv.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div>
                       
                       
                       
                       
                       
                       
                       
                       
                       	    	<div class="col-md-3"> 
			    	  <div class="form-group" style="width:180px">
                    
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Hold Adjustment")%></label> 
  <%if ((new Byte((sv.zhldclma).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zhldclma).getFieldName());
			
			formatValue = smartHF.getPicFormatted(qpsf,sv.zhldclma,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.zhldclma.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell">	
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
 
 </div></div>
 
 </div>
  
  <script>
$(document).ready(function() {
	$('#dataTables-s5318').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
paging:   true,
        scrollCollapse: true,
  	});
})
</script>
  </div></div>



		
		
		
		
<%-- <BODY style="background-color: #f8f8f8;">
	<div class="sidearea">
		 <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" style="display: block;">
 
					<ul class="nav" id="mainForm_OPTS">
                        <li>											
							<ul class="nav nav-second-level" aria-expanded="true">
							
							<li>
															
								<%
										if (sv.fupflg.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='fupflg' id='fupflg' type='hidden' value="<%=sv.fupflg.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.fupflg.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.fupflg.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.fupflg.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' class="hyperLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.fupflg.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.fupflg.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%} %>
			 						
			 					</li>	
							
							
							</ul>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
	</BODY>						 --%>






<Div id='mainForm_OPTS' style='visibility:hidden; width:60;'>
					<li>
					<input name='fupflg' id='fupflg' type='hidden' value="<%=sv.fupflg.getFormData()%>">
									<!-- text -->
									<%
									if((sv.fupflg.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.fupflg.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.fupflg.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon" onclick="removeXfield(parent.frames['mainForm'].document.getElementById('xoptind01'))"></i> 
			 						<%}%>
			 						</a>
								

							</li>
	<%if ((new Byte((sv.bnfying).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
	) {%>
							<li>
							<input name='bnfying' id='bnfying' type='hidden' value="<%=sv.bnfying.getFormData()%>">
									<!-- text -->
									<%
									if((sv.bnfying.getInvisible()== BaseScreenData.INVISIBLE|| sv.bnfying
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Claim Payees")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bnfying"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Claim Payees")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.bnfying.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.bnfying.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.bnfying'))" style="cursor:pointer"
src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">; 
			 						<%}%>
			 						</a>
							</li>
	<%}%>
	  <%if ((new Byte((sv.investres).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<li>
							<input name='investres' id='investres' type='hidden' value="<%=sv.investres.getFormData()%>">
									<!-- text -->
									<%
									if((sv.investres.getInvisible()== BaseScreenData.INVISIBLE|| sv.investres
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Investigation Results")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("investres"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Investigation Results")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.investres.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.investres.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.investres'))" style="cursor:pointer" src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
			 						<%}%>
			 						</a>
							</li>
                    <%}%>
	 <%if ((new Byte((sv.claimnotes).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<li>
							<input name='claimnotes' id='claimnotes' type='hidden' value="<%=sv.claimnotes.getFormData()%>">
									<!-- text -->
									<%
									if((sv.claimnotes.getInvisible()== BaseScreenData.INVISIBLE|| sv.claimnotes
											.getEnabled()==BaseScreenData.DISABLED)){
									%> 
									<a href="#" class="disabledLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes")%>
									<%
			 						} else {
									%>
									<a href="javascript:;" 
										onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("claimnotes"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Notification/Claim Notes")%>
									<%}%>
									
									<!-- icon -->
									<%
									if (sv.claimnotes.getFormData().equals("+")) {
									%> 
									<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/check.gif" border="0">
									<%}
			 						if (sv.claimnotes.getFormData().equals("X")) {
			 						%>
			 						<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('sv.claimnotes'))" style="cursor:pointer" src="/<%= AppVars.getInstance().getContextPath()%> /screenFiles/<%=imageFolder%>/xicon.gif" border="0">;
			 						<%}%>
			 						</a>
							</li>
                    <%}%>
                    
</Div>
</Div>




<%-- 
<Div id='mainForm_OPTS' style='visibility:hidden'>
<!-- ILIFE-964   starts -->
<table>
<tr><td>
<input name='fupflg' id='fupflg' type='hidden'  value="<%=sv.fupflg
.getFormData()%>">
<%
if (sv.fupflg
.getFormData().equals("+")) {
%>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0">

<%}
	if (sv.fupflg
.getFormData().equals("X")) {
%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('fupflg'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 15px; font-family: Arial">
<%

if(sv.fupflg
.getInvisible()== BaseScreenData.INVISIBLE|| sv.fupflg
.getEnabled()==BaseScreenData.DISABLED){
%>
<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>
<% } else { %>
<div style="height: 15 px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" style=" text-decoration: none;"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("fupflg"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Follow Ups")%>

</a>	
</div>
<%} %>


</td></tr> 
</table>
<!-- ILIFE-964   ends -->

						



	
</Div>
 --%>


<script>
$(document).ready(function() {
	$('#rskstat').width('63');
	if (screen.height == 900) {
		
		$('#cntdesc').css('max-width','230px')
	} 
if (screen.height == 768) {
		
		$('#cntdesc').css('max-width','190px')
	} 
	
})
</script>





<%@ include file="/POLACommon2NEW.jsp"%>

