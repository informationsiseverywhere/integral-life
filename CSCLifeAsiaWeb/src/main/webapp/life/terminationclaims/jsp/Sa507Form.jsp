<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SA507";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*"%>
<%Sa507ScreenVars sv = (Sa507ScreenVars) fw.getVariables();%>
<%{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.chdrnum.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.cnttype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.pstatcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind09.isOn()) {
			sv.longdesc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.cltype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* if (appVars.ind11.isOn()) {
			sv.chdrnumflg.setInvisibility(BaseScreenData.INVISIBLE);
		} */
	}
	%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assurd")%></label>
					<table>
						<tr>
							<td>
								<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
							<td>
								<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%> <%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width: 40px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
						longValue = null;
						formatValue = null;
						%> <%}%>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>
		</div>

			<%
			GeneralTable sfl = fw.getTable("sa507screensfl");
			%>
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-sa507' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%> </th>
						<%if ((new Byte((sv.chdrnumflg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
								) {%>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></th>
						<%} %>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Product Code")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Product Name")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Commence Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Cessation Date")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Notification Status")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					     	 <%
								Sa507screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sa507screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							<%
							{
									if (appVars.ind02.isOn()) {
										sv.select.setReverse(BaseScreenData.REVERSED);
									}
									if (appVars.ind04.isOn()) {
										sv.select.setEnabled(BaseScreenData.DISABLED);
									}
									if (appVars.ind03.isOn()) {
										sv.select.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind02.isOn()) {
										sv.select.setColor(BaseScreenData.RED);
									}
									if (!appVars.ind02.isOn()) {
										sv.select.setHighLight(BaseScreenData.BOLD);
									}
									if (appVars.ind05.isOn()) {
										sv.chdrnum.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind06.isOn()) {
										sv.cnttype.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind07.isOn()) {
										sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind08.isOn()) {
										sv.pstatcode.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind09.isOn()) {
										sv.longdesc.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind10.isOn()) {
										sv.cltype.setInvisibility(BaseScreenData.INVISIBLE);
									}
								//ICIL-1286 Starts
									if (appVars.ind14.isOn()) {
										sv.slt.setReverse(BaseScreenData.REVERSED);
									}
									if (appVars.ind30.isOn()) {
										sv.slt.setEnabled(BaseScreenData.DISABLED);
									}
									if (appVars.ind14.isOn()) {
										sv.slt.setColor(BaseScreenData.RED);
									}
									if (!appVars.ind14.isOn()) {
										sv.slt.setHighLight(BaseScreenData.BOLD);
									}
									//ICIL-1286 End
								}
					
							%>

				<tr class="tableRowTag" id='<%="tablerow"+count%>' >
					<td  
						<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >			 
						 <input type="checkbox" 
							 value='<%= sv.select.getFormData() %>' 
							 onFocus='doFocus(this)' onHelp='return fieldHelp("sa507screensfl" + "." +
							 "select")' onKeyUp='return checkMaxLength(this)' 
							 name='sa507screensfl.select_R<%=count%>'
							 id='sa507screensfl.select_R<%=count%>'
							 onClick="selectedRow('sa507screensfl.select_R<%=count%>')"
							 class="UICheck"
						 <%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										disabled="disabled" <%}%>
										<%if (!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0")
									|| (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
										checked="checked" <%}%> /> 
						</td>
	<!-- ICIL-1286 Starts -->
						<%
										if ((new Byte((sv.slt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>



									<%
										if ((new Byte((sv.slt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
														|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
									%>

									<%
										} else {
									%>

									<div style='display: none; visiblity: hidden;'>
										<input type='text' maxLength='<%=sv.slt.getLength()%>'
											value='<%=sv.slt.getFormData()%>'
											size='<%=sv.slt.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(sa507screensfl.slt)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="sa507screensfl" + "." + "slt" + "_R" + count%>'
											id='<%="sa507screensfl" + "." + "slt" + "_R" + count%>'
											class="input_cell"
											style="width: <%=sv.slt.getLength() * 12%> px;">

									</div>
									<%
										}
									%>



									<%
										}
									%>
					    <td style="min-width: 50px;" 
						<%if((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >
							<%-- <input name='chdrnumflg' id='chdrnumflg' type='hidden'  value="<%=sv.chdrnum.getFormData()%>">
							  <a href="javascript:;" onClick='hyperLinkTo(document.getElementById("chdrnumflg"))' class="hyperLink">
								<%= sv.chdrnum.getFormData()%>
							</a>  --%>
							<input name=chdrnumflg id='chdrnumflg' type='hidden' value="<%=sv.chdrnumflg.getFormData()%>">
							 	  <%-- <a href="javascript:;" onClick='hyperLinkTo(document.getElementById("chdrnumflg"))' class="hyperLink">
								<%= sv.chdrnum.getFormData()%>
							</a> --%>
							<a href="javascript:;" class='tableLink'
										onClick='document.getElementById("<%="sa507screensfl" + "." + "slt" + "_R" + count%>").value="5"; doAction("PFKEY0");'><span><%=sv.chdrnum.getFormData()%></span></a> 
						 </td>
					    <td  style="min-width: 70px;" 
						<%if((sv.cnttype).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >									
						<%= sv.cnttype.getFormData()%>
						</td>
						
					    <td  style="min-width: 90px;" 
						<%if((sv.longdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >									
						<%= sv.longdesc.getFormData()%>
						</td>
									
				    	<td  style="min-width: 90px;" 
						<%if((sv.riskCommDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
							<%= sv.riskCommDateDisp.getFormData()%>
						 </td>
						 
		  				<td 
						<%if((sv.riskCessDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.riskCessDateDisp.getFormData()%>
						</td>
						
						<td  style="min-width: 90px;" 
						<%if((sv.statcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >									
						<%= sv.statcode.getFormData()%>
							<!-- IF -->
						</td>
						 
						 <td  style="min-width: 90px;" 
						<%if((sv.pstatcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >									
						<%= sv.pstatcode.getFormData()%>
							<!-- IF -->
						 </td>	
						
						<td 
						<%if((sv.cltype).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="middle"<% }else {%> align="middle" <%}%> >									
						<%= sv.cltype.getFormData()%>
						</td>
					
					</tr>
				
					<%
					count = count + 1;
					Sa507screensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
<script>
	$(document).ready(function() {
    	$('#dataTables-sa507').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollX: true,
        	scrollCollapse:true,
        	moreBtn: true,
        	
      	});
    	fixedColumns: true
    });
</script>  
<%@ include file="/POLACommon2NEW.jsp"%>
