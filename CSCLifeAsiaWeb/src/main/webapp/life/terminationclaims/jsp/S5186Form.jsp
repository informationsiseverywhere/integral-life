

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5186";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%S5186ScreenVars sv = (S5186ScreenVars) fw.getVariables();%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind03.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.life.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind08.isOn()) {
			sv.plansuff.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policies in Plan  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel Life Cov Rider  Component Details");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind09.isOn()) {
			sv.chdrstatus.setReverse(BaseScreenData.REVERSED);
			sv.chdrstatus.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.chdrstatus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.premstatus.setReverse(BaseScreenData.REVERSED);
			sv.premstatus.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.premstatus.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.entity.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>



<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<table><tr><td>
		       				<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td><td style="padding-left:1px;">
				  		
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
						</td><td style="padding-left:1px;">	 
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4">
		       	</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       	 
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
					%>
					
				  		
						<%					
						if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 120px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		 
		       		</div>
		       	</div>

		    </div>
		    
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		 
		       		<%					
						if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		 
		       		</div>
		       	</div>
		      
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		 
		       			<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		 
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
		       		 
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("register");
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
					%>
					
				  		
						<%					
						if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.register.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.register.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		 
		       		</div>
		       	</div>
		    </div>
		    
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
						if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  		</td>
				  		<td style="padding-left:1px;">
					
				  		
						<%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table>
		       		<tr>
		       		<td style="min-width: 65px;">
		       		<%					
						if(!((sv.jlifeno.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifeno.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifeno.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  	</td>	
				  	<td style="min-width:65px;padding-left:1px;">
				
				  		
						<%					
						if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policies in Plan")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       			<%	
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
							
							if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</td>
					<td style="padding-left:1px;">			 
					
				  		
						<%					
						if(!((sv.entity.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.entity.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.entity.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
					</td>
					<td style="padding-left:1px;min-width:65px;">
					
				  		
						<%	
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
							
							if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell"  > &nbsp; </div>
						
						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>
		    <br>
		    
		    <%
			GeneralTable sfl = fw.getTable("s5186screensfl");
			%>
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5186' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%> </th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Component Details")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					     	 <%
								S5186screensfl
								.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5186screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							<%
							{
									if (appVars.ind02.isOn()) {
										sv.select.setReverse(BaseScreenData.REVERSED);
									}
									if (appVars.ind04.isOn()) {
										sv.select.setEnabled(BaseScreenData.DISABLED);
									}
									if (appVars.ind03.isOn()) {
										sv.select.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind02.isOn()) {
										sv.select.setColor(BaseScreenData.RED);
									}
									if (!appVars.ind02.isOn()) {
										sv.select.setHighLight(BaseScreenData.BOLD);
									}
									if (appVars.ind05.isOn()) {
										sv.life.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind06.isOn()) {
										sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind07.isOn()) {
										sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
									}
									if (appVars.ind08.isOn()) {
										sv.plansuff.setInvisibility(BaseScreenData.INVISIBLE);
									}
								}
					
							%>

				<tr class="tableRowTag" id='<%="tablerow"+count%>' >
					<td  
				<%-- 	<%if (isEQ(sv.asterisk, SPACES)) {%> --%>
			
						<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else { %> align="left" <%}%> >
					<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {  %>
							
				<%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>	
							 
						 <input type="checkbox" 
							 value='<%= sv.select.getFormData() %>' 
							 onFocus='doFocus(this)' onHelp='return fieldHelp("s5186screensfl" + "." +
							 "select")' onKeyUp='return checkMaxLength(this)' 
							 name='s5186screensfl.select_R<%=count%>'
							 id='s5186screensfl.select_R<%=count%>'
							 onClick="selectedRow('s5186screensfl.select_R<%=count%>')"
							 class="UICheck" disabled
						 />
					
					 <% }  }else{ %>	
					  <input type="checkbox" 
							 value='<%= sv.select.getFormData() %>' 
							 onFocus='doFocus(this)' onHelp='return fieldHelp("s5186screensfl" + "." +
							 "select")' onKeyUp='return checkMaxLength(this)' 
							 name='s5186screensfl.select_R<%=count%>'
							 id='s5186screensfl.select_R<%=count%>'
							 onClick="selectedRow('s5186screensfl.select_R<%=count%>')"
							 class="UICheck" 
						 />
		
					 
					  	<% } %>	
						</td>

					    <td style="min-width: 50px;" 
						<%if((sv.asterisk).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
							<%= sv.asterisk.getFormData()%>
						 </td>
						 
					    <td  style="min-width: 70px;" 
						<%if((sv.life).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.life.getFormData()%>
						</td>
						
					    <td  style="min-width: 90px;" 
						<%if((sv.coverage).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.coverage.getFormData()%>
						</td>
									
				    	<td  style="min-width: 90px;" 
						<%if((sv.rider).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
							<%= sv.rider.getFormData()%>
						 </td>
						 
		  				<td 
						<%if((sv.shortdesc).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
						<%= sv.shortdesc.getFormData()%> -
						<%= sv.elemdesc.getFormData()%>
						</td>
					
					</tr>
				
					<%
					count = count + 1;
					S5186screensfl
					.setNextScreenRow(sfl, appVars, sv);
					}
					%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		    
	 </div>
</div>
<script>
       $(document).ready(function() {
              $('#dataTables-s5186').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script> 

<%@ include file="/POLACommon2NEW.jsp"%>

