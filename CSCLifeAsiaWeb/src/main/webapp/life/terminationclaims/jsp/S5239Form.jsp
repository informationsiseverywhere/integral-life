


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5239";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%S5239ScreenVars sv = (S5239ScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind02.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract no      ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract owner   ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1 - Select,");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"2 - Add, 9 - Delete");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment No");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Amount");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Type");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind01.isOn()) {
			generatedText8.setColor(BaseScreenData.BLUE);
		}
		if (appVars.ind03.isOn()) {
			generatedText9.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			generatedText9.setColor(BaseScreenData.BLUE);
		}
		if (appVars.ind03.isOn()) {
			sv.hselect.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>


<div class="panel panel-default">
<div class="panel-body">    

 
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group" >
			    	   
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract no")%></label>

  						<table>
  						<tr>
  						<td>
  						
	
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:200px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>


  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td>
  
	<td>	
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	</tr>
	</table>
	</div></div>
	
	<div class="col-md-4"> </div>
	
	
	
	
	<div class="col-md-4"> 
			    	     <div class="form-group" >
			    	     
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Contract owner")%></label>
                       <table>
                       <tr>
                       <td>
                        
                        <%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>

  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		
	</td>
	</tr>
	</table>
		</div></div>
     
	
	</div>
	
	<br>
	
	
	<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-s5239'>
	
	<%
/*bug #ILIFE-931 start*/ 
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[5];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*5;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*5;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rgpynum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
			} else {		
				calculatedValue = (sv.rgpynum.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.pymt.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*11;								
			} else {		
				calculatedValue = (sv.pymt.getFormData()).length()*11;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.rgpytype.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
			} else {		
				calculatedValue = (sv.rgpytype.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.rptldesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*11;								
			} else {		
				calculatedValue = (sv.rptldesc.getFormData()).length()*11;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s5239screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
		
		
		
		<thead>
		<tr>
		
		            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
		         	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
		         	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
		         	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
		         	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
		
		
		
		</tr>
		</thead>
		<%
	S5239screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S5239screensfl
	.hasMoreScreenRows(sfl)) {	
%>
		
		
		
		<tbody>
		
		
		<tr  style="text-align: center;" >
						    									<td >
																			
																	
													
					
					 					 
					 <input type="radio" style="margin-left: 23px;"
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s5239screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='s5239screensfl.select_R<%=count%>'
						 id='s5239screensfl.select_R<%=count%>'
						 onClick="selectedRow('s5239screensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
									</td>
				    									<td >									
																
									
											
						<%= sv.rgpynum.getFormData()%>
						
														 
				
									</td>
				    									<td >									
																
																							
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.pymt).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.pymt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							if(!(sv.pymt).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
								longValue = null;
								formatValue = null;
						%>
					 			 		
			 		
			    				 
				
									</td>
				    									<td >									
																
									
											
						<%= sv.rgpytype.getFormData()%>
						
														 
				
									</td>
				    									<td >									
																
									
											
						<%= sv.rptldesc.getFormData()%>
						
														 
				
									</td>
					
	</tr>

	<%
	count = count + 1;
	S5239screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
		
		
		
		
		
		</tbody>
	
	
	</table></div></div></div></div>
	

	
	
	
<div class="row">
  
                  <div class="col-md-4">
        			<div class="form-group">
						<a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(1);"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></a> 
 <%if (sv.scflag.compareTo("Y") != 0) {%> 
           <a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(2);"><%=resourceBundleHandler.gettingValueFromBundle("Add")%></a>
           <a class="btn btn-info" href= "#" onClick="JavaScript:perFormOperation(9);"><%=resourceBundleHandler.gettingValueFromBundle("Delete")%></a>
           <% }%> 
 

       
        			  
        			</div>
        		</div>

</div>
	
	 	<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("1 - Select,")%>
</div>


<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("2 - Add, 9 - Delete")%>
</div>

 
	
  		
		<%					
		if(!((sv.hselect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.hselect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.hselect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>
</table></div>
	
	</div></div>



 

 


<%@ include file="/POLACommon2NEW.jsp"%>

