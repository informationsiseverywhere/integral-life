<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR585";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.terminationclaims.screens.*" %>
<%Sr585ScreenVars sv = (Sr585ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," Rider                                 Benefit     Max           Max  Double  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Description                  %SA/Unit Unit Unit       Amount Indemnity");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Continuation Item  ");%>
<div class="panel panel-default">
      <div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
			    	  <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                         <div style="width:50px">
                         	<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div></div></div>
  <div class="col-md-3"> 
			    	  <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                         <div style="width:90px">
                        	<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div></div></div>
  <div class="col-md-5"> 
			    	  <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                       <div class="input-group" style="width:350px">
                       <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>	
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </div></div></div>
  </div>
  <div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
	         <table class="table table-striped table-bordered table-hover" id='dataTables-sr585'>
               <thead>
			    	    <tr>  
			    	       <td>  </td>
			    	       <td>  </td>
			    	       <td><label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("Benefit %")%></label>  </td>
			    	       <td>  </td>
			    	       <td>  </td>
			    	       <td> <label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("Maximum Amount")%> </label> </td>
			    	       <td></td>
			    	     </tr>
			    	     <tr>  
			    	       <td> <label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("Rider Benefit")%></label> </td>
			    	       <td> <label style="font-size:14px"> <%=resourceBundleHandler.gettingValueFromBundle("Description")%></label></td>
			    	       <td><label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("SA/Unit")%>  </label></td>
			    	       <td> <label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("Unit")%> </label> </td>
			    	       <td><label style="font-size:14px">  <%=resourceBundleHandler.gettingValueFromBundle("Maximum Unit")%></label></td>
			    	       <td > <label style="font-size:14px"><%=resourceBundleHandler.gettingValueFromBundle("Double Idemnity")%> </label> </td>
			    	       <td></td>
			    	     </tr>
			    	 </thead>
			    	 <tbody>    
			    	     <jsp:include page="Sr585FormPart.jsp"></jsp:include>
			    	     <tr style='height:22px;'><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben07");
	optionValue = makeDropDownList( mappedItems , sv.acdben07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben07.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben07' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben07).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='300'>	
		<%					
		if(!((sv.ditdsc07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc07.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='100'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct07' 
type='text'
<%if((sv.dfclmpct07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct07) %>'
	 <%}%>
size='<%= sv.dfclmpct07.getLength()%>'
maxLength='<%= sv.dfclmpct07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct07)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct07).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='100'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq07"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq07");
	optionValue = makeDropDownList( mappedItems , sv.benfreq07.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq07.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq07).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:150px;"> 
<%
} 
%>
<select name='benfreq07' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq07).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='200'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt07' 
type='text'
<%if((sv.mxbenunt07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt07) %>'
	 <%}%>
size='<%= sv.mxbenunt07.getLength()%>'
maxLength='<%= sv.mxbenunt07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt07)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt07).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='100'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld07).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld07' 
type='text'
<%if((sv.amtfld07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld07) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld07);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld07) %>'
	 <%}%>
size='<%= sv.amtfld07.getLength()%>'
maxLength='<%= sv.amtfld07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld07)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld07).getColor()== null  ? 
			"input_cell" :  (sv.amtfld07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='100'>
<div style="width:50px">
<input name='gcdblind07' 
type='text'
<%if((sv.gcdblind07).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind07.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind07.getLength()%>'
maxLength='<%= sv.gcdblind07.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind07)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind07).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind07).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind07).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind07).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</div>
</td>
</tr>
<tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben08");
	optionValue = makeDropDownList( mappedItems , sv.acdben08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben08.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben08' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben08).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc08.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct08' 
type='text'
<%if((sv.dfclmpct08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct08) %>'
	 <%}%>
size='<%= sv.dfclmpct08.getLength()%>'
maxLength='<%= sv.dfclmpct08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct08)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct08).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq08"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq08");
	optionValue = makeDropDownList( mappedItems , sv.benfreq08.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq08.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq08).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq08' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq08).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt08' 
type='text'
<%if((sv.mxbenunt08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt08) %>'
	 <%}%>
size='<%= sv.mxbenunt08.getLength()%>'
maxLength='<%= sv.mxbenunt08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt08)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt08).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld08).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld08' 
type='text'
<%if((sv.amtfld08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld08) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld08);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld08) %>'
	 <%}%>
size='<%= sv.amtfld08.getLength()%>'
maxLength='<%= sv.amtfld08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld08)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld08).getColor()== null  ? 
			"input_cell" :  (sv.amtfld08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind08' 
type='text'
<%if((sv.gcdblind08).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind08.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind08.getLength()%>'
maxLength='<%= sv.gcdblind08.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind08)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind08).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind08).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind08).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind08).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben09");
	optionValue = makeDropDownList( mappedItems , sv.acdben09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben09.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben09' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben09).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc09.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct09' 
type='text'
<%if((sv.dfclmpct09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct09) %>'
	 <%}%>
size='<%= sv.dfclmpct09.getLength()%>'
maxLength='<%= sv.dfclmpct09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct09)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct09).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq09"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq09");
	optionValue = makeDropDownList( mappedItems , sv.benfreq09.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq09.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq09).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq09' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq09).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt09' 
type='text'
<%if((sv.mxbenunt09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt09) %>'
	 <%}%>
size='<%= sv.mxbenunt09.getLength()%>'
maxLength='<%= sv.mxbenunt09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt09)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt09).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld09).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld09' 
type='text'
<%if((sv.amtfld09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld09) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld09);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld09) %>'
	 <%}%>
size='<%= sv.amtfld09.getLength()%>'
maxLength='<%= sv.amtfld09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld09)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld09).getColor()== null  ? 
			"input_cell" :  (sv.amtfld09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind09' 
type='text'
<%if((sv.gcdblind09).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind09.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind09.getLength()%>'
maxLength='<%= sv.gcdblind09.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind09)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind09).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind09).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind09).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind09).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben10");
	optionValue = makeDropDownList( mappedItems , sv.acdben10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben10.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben10' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben10).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc10.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct10' 
type='text'
<%if((sv.dfclmpct10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct10) %>'
	 <%}%>
size='<%= sv.dfclmpct10.getLength()%>'
maxLength='<%= sv.dfclmpct10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct10)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct10).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq10"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq10");
	optionValue = makeDropDownList( mappedItems , sv.benfreq10.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq10.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq10).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq10' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq10).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt10' 
type='text'
<%if((sv.mxbenunt10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt10) %>'
	 <%}%>
size='<%= sv.mxbenunt10.getLength()%>'
maxLength='<%= sv.mxbenunt10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt10)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt10).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld10).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld10' 
type='text'
<%if((sv.amtfld10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld10) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld10);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld10) %>'
	 <%}%>
size='<%= sv.amtfld10.getLength()%>'
maxLength='<%= sv.amtfld10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld10)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld10).getColor()== null  ? 
			"input_cell" :  (sv.amtfld10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind10' 
type='text'
<%if((sv.gcdblind10).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind10.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind10.getLength()%>'
maxLength='<%= sv.gcdblind10.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind10)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind10).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind10).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind10).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind10).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben11");
	optionValue = makeDropDownList( mappedItems , sv.acdben11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben11.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben11' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben11).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc11.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc11.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct11' 
type='text'
<%if((sv.dfclmpct11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct11) %>'
	 <%}%>
size='<%= sv.dfclmpct11.getLength()%>'
maxLength='<%= sv.dfclmpct11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct11)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct11).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq11"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq11");
	optionValue = makeDropDownList( mappedItems , sv.benfreq11.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq11.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq11).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq11' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq11).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt11' 
type='text'
<%if((sv.mxbenunt11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt11) %>'
	 <%}%>
size='<%= sv.mxbenunt11.getLength()%>'
maxLength='<%= sv.mxbenunt11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt11)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt11).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld11).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld11' 
type='text'
<%if((sv.amtfld11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld11) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld11);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld11) %>'
	 <%}%>
size='<%= sv.amtfld11.getLength()%>'
maxLength='<%= sv.amtfld11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld11)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld11).getColor()== null  ? 
			"input_cell" :  (sv.amtfld11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind11' 
type='text'
<%if((sv.gcdblind11).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind11.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind11.getLength()%>'
maxLength='<%= sv.gcdblind11.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind11)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind11).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind11).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind11).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind11).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben12");
	optionValue = makeDropDownList( mappedItems , sv.acdben12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben12.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben12' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben12).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc12.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc12.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct12' 
type='text'
<%if((sv.dfclmpct12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct12) %>'
	 <%}%>
size='<%= sv.dfclmpct12.getLength()%>'
maxLength='<%= sv.dfclmpct12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct12)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct12).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq12"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq12");
	optionValue = makeDropDownList( mappedItems , sv.benfreq12.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq12.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq12).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq12' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq12).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt12' 
type='text'
<%if((sv.mxbenunt12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt12) %>'
	 <%}%>
size='<%= sv.mxbenunt12.getLength()%>'
maxLength='<%= sv.mxbenunt12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt12)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt12).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld12).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld12' 
type='text'
<%if((sv.amtfld12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld12) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld12);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld12) %>'
	 <%}%>
size='<%= sv.amtfld12.getLength()%>'
maxLength='<%= sv.amtfld12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld12)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld12).getColor()== null  ? 
			"input_cell" :  (sv.amtfld12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind12' 
type='text'
<%if((sv.gcdblind12).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind12.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind12.getLength()%>'
maxLength='<%= sv.gcdblind12.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind12)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind12).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind12).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind12).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind12).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben13"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben13");
	optionValue = makeDropDownList( mappedItems , sv.acdben13.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben13.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben13).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben13' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben13).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc13.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc13.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct13).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct13' 
type='text'
<%if((sv.dfclmpct13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct13) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct13);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct13) %>'
	 <%}%>
size='<%= sv.dfclmpct13.getLength()%>'
maxLength='<%= sv.dfclmpct13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct13)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct13).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq13"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq13");
	optionValue = makeDropDownList( mappedItems , sv.benfreq13.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq13.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq13).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq13' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq13).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt13).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt13' 
type='text'
<%if((sv.mxbenunt13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt13) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt13);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt13) %>'
	 <%}%>
size='<%= sv.mxbenunt13.getLength()%>'
maxLength='<%= sv.mxbenunt13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt13)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt13).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld13).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld13' 
type='text'
<%if((sv.amtfld13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld13) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld13);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld13) %>'
	 <%}%>
size='<%= sv.amtfld13.getLength()%>'
maxLength='<%= sv.amtfld13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld13)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld13).getColor()== null  ? 
			"input_cell" :  (sv.amtfld13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind13' 
type='text'
<%if((sv.gcdblind13).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind13.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind13.getLength()%>'
maxLength='<%= sv.gcdblind13.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind13)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind13).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind13).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind13).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind13).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben14"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben14");
	optionValue = makeDropDownList( mappedItems , sv.acdben14.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben14.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben14).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben14' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben14).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc14.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc14.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct14).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct14' 
type='text'
<%if((sv.dfclmpct14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct14) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct14);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct14) %>'
	 <%}%>
size='<%= sv.dfclmpct14.getLength()%>'
maxLength='<%= sv.dfclmpct14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct14)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct14).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq14"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq14");
	optionValue = makeDropDownList( mappedItems , sv.benfreq14.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq14.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq14).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq14' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq14).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt14).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt14' 
type='text'
<%if((sv.mxbenunt14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt14) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt14);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt14) %>'
	 <%}%>
size='<%= sv.mxbenunt14.getLength()%>'
maxLength='<%= sv.mxbenunt14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt14)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt14).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld14).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld14' 
type='text'
<%if((sv.amtfld14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld14) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld14);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld14) %>'
	 <%}%>
size='<%= sv.amtfld14.getLength()%>'
maxLength='<%= sv.amtfld14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld14)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld14).getColor()== null  ? 
			"input_cell" :  (sv.amtfld14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind14' 
type='text'
<%if((sv.gcdblind14).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind14.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind14.getLength()%>'
maxLength='<%= sv.gcdblind14.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind14)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind14).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind14).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind14).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind14).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td>
</tr><tr style='height:22px;'><td width='90'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"acdben15"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("acdben15");
	optionValue = makeDropDownList( mappedItems , sv.acdben15.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.acdben15.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.acdben15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:250px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.acdben15).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='acdben15' type='list' style="width:250px;"
<% 
	if((new Byte((sv.acdben15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.acdben15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.acdben15).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='230'>
		<%					
		if(!((sv.ditdsc15.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc15.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ditdsc15.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:250px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td><td width='80'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.dfclmpct15).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='dfclmpct15' 
type='text'
<%if((sv.dfclmpct15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:80px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct15) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.dfclmpct15);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.dfclmpct15) %>'
	 <%}%>
size='<%= sv.dfclmpct15.getLength()%>'
maxLength='<%= sv.dfclmpct15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(dfclmpct15)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return getdoBlurNumber(event, true);'
<% 
	if((new Byte((sv.dfclmpct15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.dfclmpct15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.dfclmpct15).getColor()== null  ? 
			"input_cell" :  (sv.dfclmpct15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='140'>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"benfreq15"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("benfreq15");
	optionValue = makeDropDownList( mappedItems , sv.benfreq15.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.benfreq15.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.benfreq15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:150px">  
	   		<%if(longValue != null){%>
	   		<%=XSSFilter.escapeHtml(longValue)%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.benfreq15).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:80px;"> 
<%
} 
%>
<select name='benfreq15' type='list' style="width:180px;"
<% 
	if((new Byte((sv.benfreq15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.benfreq15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.benfreq15).getColor())){
%>
</div>
<%
} 
%>
<%
} 
%>
</td><td width='110'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.mxbenunt15).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='mxbenunt15' 
type='text'
<%if((sv.mxbenunt15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:110px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt15) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mxbenunt15);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mxbenunt15) %>'
	 <%}%>
size='<%= sv.mxbenunt15.getLength()%>'
maxLength='<%= sv.mxbenunt15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mxbenunt15)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.mxbenunt15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mxbenunt15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.mxbenunt15).getColor()== null  ? 
			"input_cell" :  (sv.mxbenunt15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='120'>
	<%	
			qpsf = fw.getFieldXMLDef((sv.amtfld15).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S10VS2MINUS);
	%>
<input name='amtfld15' 
type='text'
<%if((sv.amtfld15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:150px"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.amtfld15) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.amtfld15);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.amtfld15) %>'
	 <%}%>
size='<%= sv.amtfld15.getLength()%>'
maxLength='<%= sv.amtfld15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(amtfld15)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'
<% 
	if((new Byte((sv.amtfld15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.amtfld15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.amtfld15).getColor()== null  ? 
			"input_cell" :  (sv.amtfld15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td><td width='40'>
<input name='gcdblind15' 
type='text'
<%if((sv.gcdblind15).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right; width:50px"<% }%>
<%
		formatValue = (sv.gcdblind15.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.gcdblind15.getLength()%>'
maxLength='<%= sv.gcdblind15.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(gcdblind15)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.gcdblind15).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.gcdblind15).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.gcdblind15).getColor()== null  ? 
			"input_cell" :  (sv.gcdblind15).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</td></tr> 
</tbody>
</table></div>
</div></div>
<br>
 <div class="row">	
			    	<div class="col-md-4"> 
			    	  <div class="form-group">
                       <label><%=resourceBundleHandler.gettingValueFromBundle("Continuation Item")%></label>
                         <input name='contitem' 
type='text'
<%if((sv.contitem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
<%
		formatValue = (sv.contitem.getFormData()).toString();
%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
size='<%= sv.contitem.getLength()%>'
maxLength='<%= sv.contitem.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(contitem)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.contitem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.contitem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.contitem).getColor()== null  ? 
			"input_cell" :  (sv.contitem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
</div></div></div>
<div class="row">	
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='100'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Company")%>
</div>
		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td>
<td width='100'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Table")%>
</div>
		<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr>
<tr style='height:22px;'><td width='100'>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Item")%>
</div>
		<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td>
<td width='100'>&nbsp; &nbsp;<br/>  		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
</td></tr></table>
</div></div>
</div></div>
<%@ include file="/POLACommon2NEW.jsp"%>