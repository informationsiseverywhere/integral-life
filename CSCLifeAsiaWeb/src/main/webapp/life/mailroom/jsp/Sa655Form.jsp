\
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sa655";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.mailroom.screens.*" %>
<%Sa655ScreenVars sv = (Sa655ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Document Type ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Inward Number ");%>


<%{
	if (appVars.ind01.isOn()) {
sv.docttype.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind02.isOn()) {
sv.docttype.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
sv.docttype.setColor(BaseScreenData.RED);
}

if (appVars.ind03.isOn()) {
sv.docno.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind04.isOn()) {
sv.docno.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind03.isOn()) {
sv.docno.setColor(BaseScreenData.RED);
}


}
	%>

<div class="panel panel-default">
	<div class="panel-body">
		
		<div class="row">
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText1.getFormData())%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.docttype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "docttype" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("docttype");
								optionValue = makeDropDownList(mappedItems, sv.docttype.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.docttype.getFormData()).toString().trim());
						%>
						<!-- IGroup-937 Begin -->
						<%=smartHF.getUI3DropdownList(0, 0, mappedItems, fw, 100, 100, "code-desc", 1, true, sv.docttype)
						.replace("absolute", "relative")%>
						<!-- IGroup-937 End -->
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(generatedText2.getFormData())%></label>
					<div style="width:100px;">
						<%=smartHF.getHTMLVarExt(fw, sv.docno)%>						
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>


