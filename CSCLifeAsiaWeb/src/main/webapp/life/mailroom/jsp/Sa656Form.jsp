
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sa656";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.mailroom.screens.*" %>
<%Sa656ScreenVars sv = (Sa656ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Record receipt of a new document ");%>


<%{
	if (appVars.ind01.isOn()) {
sv.action.setReverse(BaseScreenData.REVERSED);
}
if (appVars.ind02.isOn()) {
sv.action.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
sv.action.setColor(BaseScreenData.RED);
}


}
	%>

<div class="panel panel-default">
<div class="panel-heading"><%=resourceBundleHandler.gettingValueFromBundle("Actions")%></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-5">
				<label class="radio-inline"><%=smartHF.getRadio(0, 0, fw,sv.action,"A",new StringData(resourceBundleHandler.gettingValueFromBundle("Record receipt of a new document"))).replace("absolute","relative")%>
				</label>
			</div>
			<div class="col-md-7"></div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<%@ include file="/POLACommon2NEW.jsp"%>


