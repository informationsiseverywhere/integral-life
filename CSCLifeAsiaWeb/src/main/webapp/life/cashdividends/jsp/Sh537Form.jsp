<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH537";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%Sh537ScreenVars sv = (Sh537ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
        <div class="panel-body">
                <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
                       <!--  <div class="input-group three-controller"> -->
                       <TABLE><TR><TD>
                        <%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</TD><td style="padding-left:1px">
<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</TD><td style="padding-left:1px">
<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:600px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td></TR></TABLE>
                        <!-- </div> -->
                        </div>
                      </div>
                      
                      
                       <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                      <div class="input-group"> 
	                      <%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
                       </div>
                       </div> 
                   </div>
            
              
              <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
	                      <div class="input-group">
	                      <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                      </div>
	                     </div>
	                   </div>   
                   
        </div>
        <div class="row">
        <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
	                      <div class="input-group">
	                      <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
                           </div>
                       </div>  
                    </div>
                    
                   
                    <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
	                      <div class="input-group">
	                      <%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                      </div>
	                     </div>
	                   </div>  
	                   
	                 
	                   <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Code")%></label>
	                      <div class="input-group">
	                      <%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                      </div>
	                      </div>
	                     </div>  
            </div>
            
             <div class="row">
        <div class="col-md-4">
	                    <div class="form-group">
	               <%--         <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label> --%>
	               		<!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
	                      <div class="input-group">    
	                      <%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%></td>
	                      </div>
	                     </div>
	                    </div> 
	                   
	                    
	  
        <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
	                      <!-- <div class="input-group">  --><table><tr><td>   
	                      <%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px">

<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                     <!--  </div> --></td></tr></table>
	                     </div>
	                    </div>
	                    
	                     <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
	                        <!-- <div class="input-group"> --><table><tr><td>
	                       <%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td><td style="padding-left:1px">
<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>   </td></tr></table>               <!-- </div>  -->
	                      </div>
	                     </div>  
	          </div>                  
	                    
	    <div class="row">
        <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
	                     <!-- <div class="input-group"> -->
	                     <table><tr><td>     
	                      <%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><td style="padding-left:1px">
<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                     <!--  </div> -->
	                     </td></tr></table>
	                    </div>
	                  </div>
	               </div>                      
	         
	          <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("First Dividend Date")%></label>
	                     
	                     <%=smartHF.getRichText(0, 0, fw, sv.firstDivdDateDisp,(sv.firstDivdDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.firstDivdDateDisp).replace("absolute","relative")%></td>
	                  
	                   </div>
	                </div>
	              
	                <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Previous Dividend Date")%></label>
	                    
	                     <%=smartHF.getRichText(0, 0, fw, sv.lastDivdDateDisp,(sv.lastDivdDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						 <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.lastDivdDateDisp).replace("absolute","relative")%></td>
	                  
	                   </div>
	                 </div>    
	               </div>                
	                    
	        <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Previous Interest Date")%></label>
	                  
	                      <%=smartHF.getRichText(0, 0, fw, sv.lastIntDateDisp,(sv.lastIntDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						  <%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.lastIntDateDisp).replace("absolute","relative")%></td>         
	                 
	                   </div>
	                 </div>   
	               
	           <div class="col-md-4">
	                    <div class="form-group"  style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Next Interest Date")%></label>
	                    
	                   <%=smartHF.getRichText(0, 0, fw, sv.nextIntDateDisp,(sv.nextIntDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.nextIntDateDisp).replace("absolute","relative")%></td>
	                
	                    </div>
	                  </div>    
	                </div>           
	                    
	    <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Previous Capitalization Date")%></label>
	                      
	                      <%=smartHF.getRichText(0, 0, fw, sv.lastCapDateDisp,(sv.lastCapDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
							<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.lastCapDateDisp).replace("absolute","relative")%></td>  
	                    
	                   </div>
	                 </div>   
	                
	           <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Next Capitalzation Date")%></label>
	                    
	                  <%=smartHF.getRichText(0, 0, fw, sv.nextCapDateDisp,(sv.nextCapDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
						<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.nextCapDateDisp).replace("absolute","relative")%></td>
	               
	                    </div>
	                  </div>    
	                </div>                  
	                    
	          
	          <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Previous Statement Date")%></label>
	                  
	                     <%=smartHF.getRichText(0, 0, fw, sv.divdStmtDateDisp,(sv.divdStmtDateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
     					<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.divdStmtDateDisp).replace("absolute","relative")%></td> 
	                   
	                   </div>
	                 </div>   
	               
	           <div class="col-md-4">
	                    <div class="form-group" style="width:100px;">
	                       <label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Previous Statement Number")%></label>
	                   
	                  <%if ((new Byte((sv.divdStmtNo).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.divdStmtNo.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.divdStmtNo.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.divdStmtNo.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                   
	                    </div>
	                  </div>    
	                </div>          
	 <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style=" width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total Allocation")%></label>
	                    
	                      	<%	
			qpsf = fw.getFieldXMLDef((sv.tamt01).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.tamt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input style="text-align: right;" name='tamt01' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.tamt01.getLength(), sv.tamt01.getScale(),3)%>'
maxLength='<%= sv.tamt01.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(tamt01)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
      if((new Byte((sv.tamt01).getEnabled()))
      .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
      readonly="true"
      class="output_cell"
<%
      }else if((new Byte((sv.tamt01).getHighLight())).
       compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>    
       class="bold_cell" 

<%
      }else { 
%>

      class = ' <%=(sv.tamt01).getColor()== null  ? 
           "input_cell" :  (sv.tamt01).getColor().equals("red") ? 
           "input_cell red reverse" : "input_cell" %>'

<%
      } 
%>
>
	                   
	                   </div>
	                 </div>   
	                
	           <div class="col-md-4">
	                    <div class="form-group" style=" width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Outstanding Installment")%></label>
	                  
	                  <%	
			qpsf = fw.getFieldXMLDef((sv.tamt02).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.tamt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='tamt02' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.tamt02.getLength(), sv.tamt02.getScale(),3)%>'
maxLength='<%= sv.tamt02.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(tamt02)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
      if((new Byte((sv.tamt02).getEnabled()))
      .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
      readonly="true"
      class="output_cell"
<%
      }else if((new Byte((sv.tamt02).getHighLight())).
       compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>    
       class="bold_cell" 

<%
      }else { 
%>

      class = ' <%=(sv.tamt02).getColor()== null  ? 
           "input_cell" :  (sv.tamt02).getColor().equals("red") ? 
           "input_cell red reverse" : "input_cell" %>'

<%
      } 
%>
>
	                
	                    </div>
	                  </div>    
	                </div>                    
	 <div class="row">
        <div class="col-md-4">
	                    <div class="form-group" style=" width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Account Dividend Balance")%></label>
	                     
	                     <%	
			qpsf = fw.getFieldXMLDef((sv.tamt03).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.tamt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='tamt03' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.tamt03.getLength(), sv.tamt03.getScale(),3)%>'
maxLength='<%= sv.tamt03.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(tamt03)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
      if((new Byte((sv.tamt03).getEnabled()))
      .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
      readonly="true"
      class="output_cell"
<%
      }else if((new Byte((sv.tamt03).getHighLight())).
       compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>    
       class="bold_cell" 

<%
      }else { 
%>

      class = ' <%=(sv.tamt03).getColor()== null  ? 
           "input_cell" :  (sv.tamt03).getColor().equals("red") ? 
           "input_cell red reverse" : "input_cell" %>'

<%
      } 
%>
>
	                    
	                   </div>
	                 </div>
	                
	     <div class="col-md-4">
	                    <div class="form-group" style=" width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Previous Statement  Balance")%></label>
	                    
	                     <%	
			qpsf = fw.getFieldXMLDef((sv.tamt04).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.tamt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='tamt04' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.tamt04.getLength(), sv.tamt04.getScale(),3)%>'
maxLength='<%= sv.tamt04.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(tamt04)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
      if((new Byte((sv.tamt04).getEnabled()))
      .compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
      readonly="true"
      class="output_cell"
<%
      }else if((new Byte((sv.tamt04).getHighLight())).
       compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>    
       class="bold_cell" 

<%
      }else { 
%>

      class = ' <%=(sv.tamt04).getColor()== null  ? 
           "input_cell" :  (sv.tamt04).getColor().equals("red") ? 
           "input_cell red reverse" : "input_cell" %>'

<%
      } 
%>
>
	                 
	                   </div>
	                 </div>                                    
	                    
	              </div>
	                    
	                    
	                    
	                       


</div>
</div>






<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%@ include file="/POLACommon2NEW.jsp"%>
