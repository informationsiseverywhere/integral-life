<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH505";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sh505ScreenVars sv = (Sh505ScreenVars) fw.getVariables();%>
<%if (sv.Sh505screenWritten.gt(0)) {%>
	<%Sh505screen.clearClassString(sv);%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%sv.company.setClassString("");%>
<%	sv.company.appendClassString("string_fld");
	sv.company.appendClassString("output_txt");
	sv.company.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%sv.tabl.setClassString("");%>
<%	sv.tabl.appendClassString("string_fld");
	sv.tabl.appendClassString("output_txt");
	sv.tabl.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%sv.item.setClassString("");%>
<%	sv.item.appendClassString("string_fld");
	sv.item.appendClassString("output_txt");
	sv.item.appendClassString("highlight");
%>
	<%sv.longdesc.setClassString("");%>
<%	sv.longdesc.appendClassString("string_fld");
	sv.longdesc.appendClassString("output_txt");
	sv.longdesc.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Valid from ");%>
	<%sv.itmfrmDisp.setClassString("");%>
<%	sv.itmfrmDisp.appendClassString("string_fld");
	sv.itmfrmDisp.appendClassString("output_txt");
	sv.itmfrmDisp.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"To ");%>
	<%sv.itmtoDisp.setClassString("");%>
<%	sv.itmtoDisp.appendClassString("string_fld");
	sv.itmtoDisp.appendClassString("output_txt");
	sv.itmtoDisp.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum insured min ");%>
	<%sv.sumInsMin.setClassString("");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Max ");%>
	<%sv.sumInsMax.setClassString("");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"F------T");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Issue Age ");%>
	<%sv.ageIssageFrm01.setClassString("");%>
	<%sv.ageIssageTo01.setClassString("");%>
	<%sv.ageIssageFrm02.setClassString("");%>
	<%sv.ageIssageTo02.setClassString("");%>
	<%sv.ageIssageFrm03.setClassString("");%>
	<%sv.ageIssageTo03.setClassString("");%>
	<%sv.ageIssageFrm04.setClassString("");%>
	<%sv.ageIssageTo04.setClassString("");%>
	<%sv.ageIssageFrm05.setClassString("");%>
	<%sv.ageIssageTo05.setClassString("");%>
	<%sv.ageIssageFrm06.setClassString("");%>
	<%sv.ageIssageTo06.setClassString("");%>
	<%sv.ageIssageFrm07.setClassString("");%>
	<%sv.ageIssageTo07.setClassString("");%>
	<%sv.ageIssageFrm08.setClassString("");%>
	<%sv.ageIssageTo08.setClassString("");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cessation Age ");%>
	<%sv.riskCessageFrom01.setClassString("");%>
	<%sv.riskCessageTo01.setClassString("");%>
	<%sv.riskCessageFrom02.setClassString("");%>
	<%sv.riskCessageTo02.setClassString("");%>
	<%sv.riskCessageFrom03.setClassString("");%>
	<%sv.riskCessageTo03.setClassString("");%>
	<%sv.riskCessageFrom04.setClassString("");%>
	<%sv.riskCessageTo04.setClassString("");%>
	<%sv.riskCessageFrom05.setClassString("");%>
	<%sv.riskCessageTo05.setClassString("");%>
	<%sv.riskCessageFrom06.setClassString("");%>
	<%sv.riskCessageTo06.setClassString("");%>
	<%sv.riskCessageFrom07.setClassString("");%>
	<%sv.riskCessageTo07.setClassString("");%>
	<%sv.riskCessageFrom08.setClassString("");%>
	<%sv.riskCessageTo08.setClassString("");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem Cess Age ");%>
	<%sv.premCessageFrom01.setClassString("");%>
	<%sv.premCessageTo01.setClassString("");%>
	<%sv.premCessageFrom02.setClassString("");%>
	<%sv.premCessageTo02.setClassString("");%>
	<%sv.premCessageFrom03.setClassString("");%>
	<%sv.premCessageTo03.setClassString("");%>
	<%sv.premCessageFrom04.setClassString("");%>
	<%sv.premCessageTo04.setClassString("");%>
	<%sv.premCessageFrom05.setClassString("");%>
	<%sv.premCessageTo05.setClassString("");%>
	<%sv.premCessageFrom06.setClassString("");%>
	<%sv.premCessageTo06.setClassString("");%>
	<%sv.premCessageFrom07.setClassString("");%>
	<%sv.premCessageTo07.setClassString("");%>
	<%sv.premCessageFrom08.setClassString("");%>
	<%sv.premCessageTo08.setClassString("");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cess date, Anniversary or Exact ");%>
	<%sv.eaage.setClassString("");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(A/E)");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Issue Age ");%>
	<%sv.termIssageFrm01.setClassString("");%>
	<%sv.termIssageTo01.setClassString("");%>
	<%sv.termIssageFrm02.setClassString("");%>
	<%sv.termIssageTo02.setClassString("");%>
	<%sv.termIssageFrm03.setClassString("");%>
	<%sv.termIssageTo03.setClassString("");%>
	<%sv.termIssageFrm04.setClassString("");%>
	<%sv.termIssageTo04.setClassString("");%>
	<%sv.termIssageFrm05.setClassString("");%>
	<%sv.termIssageTo05.setClassString("");%>
	<%sv.termIssageFrm06.setClassString("");%>
	<%sv.termIssageTo06.setClassString("");%>
	<%sv.termIssageFrm07.setClassString("");%>
	<%sv.termIssageTo07.setClassString("");%>
	<%sv.termIssageFrm08.setClassString("");%>
	<%sv.termIssageTo08.setClassString("");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage Term ");%>
	<%sv.riskCesstermFrom01.setClassString("");%>
	<%sv.riskCesstermTo01.setClassString("");%>
	<%sv.riskCesstermFrom02.setClassString("");%>
	<%sv.riskCesstermTo02.setClassString("");%>
	<%sv.riskCesstermFrom03.setClassString("");%>
	<%sv.riskCesstermTo03.setClassString("");%>
	<%sv.riskCesstermFrom04.setClassString("");%>
	<%sv.riskCesstermTo04.setClassString("");%>
	<%sv.riskCesstermFrom05.setClassString("");%>
	<%sv.riskCesstermTo05.setClassString("");%>
	<%sv.riskCesstermFrom06.setClassString("");%>
	<%sv.riskCesstermTo06.setClassString("");%>
	<%sv.riskCesstermFrom07.setClassString("");%>
	<%sv.riskCesstermTo07.setClassString("");%>
	<%sv.riskCesstermFrom08.setClassString("");%>
	<%sv.riskCesstermTo08.setClassString("");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem Cess Term ");%>
	<%sv.premCesstermFrom01.setClassString("");%>
	<%sv.premCesstermTo01.setClassString("");%>
	<%sv.premCesstermFrom02.setClassString("");%>
	<%sv.premCesstermTo02.setClassString("");%>
	<%sv.premCesstermFrom03.setClassString("");%>
	<%sv.premCesstermTo03.setClassString("");%>
	<%sv.premCesstermFrom04.setClassString("");%>
	<%sv.premCesstermTo04.setClassString("");%>
	<%sv.premCesstermFrom05.setClassString("");%>
	<%sv.premCesstermTo05.setClassString("");%>
	<%sv.premCesstermFrom06.setClassString("");%>
	<%sv.premCesstermTo06.setClassString("");%>
	<%sv.premCesstermFrom07.setClassString("");%>
	<%sv.premCesstermTo07.setClassString("");%>
	<%sv.premCesstermFrom08.setClassString("");%>
	<%sv.premCesstermTo08.setClassString("");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms ");%>
	<%sv.specind.setClassString("");%>
<%	sv.specind.appendClassString("string_fld");
	sv.specind.appendClassString("input_txt");
	sv.specind.appendClassString("highlight");
%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dividend Options ");%>
	<%sv.zdivopt01.setClassString("");%>
<%	sv.zdivopt01.appendClassString("string_fld");
	sv.zdivopt01.appendClassString("input_txt");
	sv.zdivopt01.appendClassString("highlight");
%>
	<%sv.zdivopt02.setClassString("");%>
<%	sv.zdivopt02.appendClassString("string_fld");
	sv.zdivopt02.appendClassString("input_txt");
	sv.zdivopt02.appendClassString("highlight");
%>
	<%sv.zdivopt03.setClassString("");%>
<%	sv.zdivopt03.appendClassString("string_fld");
	sv.zdivopt03.appendClassString("input_txt");
	sv.zdivopt03.appendClassString("highlight");
%>
	<%sv.zdivopt04.setClassString("");%>
<%	sv.zdivopt04.appendClassString("string_fld");
	sv.zdivopt04.appendClassString("input_txt");
	sv.zdivopt04.appendClassString("highlight");
%>
	<%sv.zdivopt05.setClassString("");%>
<%	sv.zdivopt05.appendClassString("string_fld");
	sv.zdivopt05.appendClassString("input_txt");
	sv.zdivopt05.appendClassString("highlight");
%>
	<%sv.zdivopt06.setClassString("");%>
<%	sv.zdivopt06.appendClassString("string_fld");
	sv.zdivopt06.appendClassString("input_txt");
	sv.zdivopt06.appendClassString("highlight");
%>
	<%sv.zdivopt07.setClassString("");%>
<%	sv.zdivopt07.appendClassString("string_fld");
	sv.zdivopt07.appendClassString("input_txt");
	sv.zdivopt07.appendClassString("highlight");
%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Default Option ");%>
	<%sv.zdefdivopt.setClassString("");%>
<%	sv.zdefdivopt.appendClassString("string_fld");
	sv.zdefdivopt.appendClassString("input_txt");
	sv.zdefdivopt.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class ");%>
	<%sv.mortcls01.setClassString("");%>
	<%sv.mortcls02.setClassString("");%>
	<%sv.mortcls03.setClassString("");%>
	<%sv.mortcls04.setClassString("");%>
	<%sv.mortcls05.setClassString("");%>
	<%sv.mortcls06.setClassString("");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien Codes ");%>
	<%sv.liencd01.setClassString("");%>
	<%sv.liencd02.setClassString("");%>
	<%sv.liencd03.setClassString("");%>
	<%sv.liencd04.setClassString("");%>
	<%sv.liencd05.setClassString("");%>
	<%sv.liencd06.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%
{
		if (appVars.ind03.isOn()) {
			sv.sumInsMin.setReverse(BaseScreenData.REVERSED);
			sv.sumInsMin.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.sumInsMin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.sumInsMax.setReverse(BaseScreenData.REVERSED);
			sv.sumInsMax.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.sumInsMax.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.ageIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.ageIssageFrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.ageIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind14.isOn()) {
			sv.ageIssageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.ageIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.ageIssageFrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.ageIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.ageIssageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.ageIssageFrm03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.ageIssageFrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.ageIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.ageIssageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ageIssageFrm04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ageIssageFrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.ageIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.ageIssageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ageIssageFrm05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ageIssageFrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.ageIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.ageIssageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.ageIssageFrm06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ageIssageFrm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.ageIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.ageIssageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.ageIssageFrm07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.ageIssageFrm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.ageIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind20.isOn()) {
			sv.ageIssageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.ageIssageFrm08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageFrm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.ageIssageFrm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.ageIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.ageIssageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ageIssageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.riskCessageFrom01.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.riskCessageFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.riskCessageTo01.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.riskCessageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.riskCessageFrom02.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.riskCessageFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.riskCessageTo02.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.riskCessageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.riskCessageFrom03.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.riskCessageFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.riskCessageTo03.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.riskCessageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.riskCessageFrom04.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.riskCessageFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.riskCessageTo04.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.riskCessageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.riskCessageFrom05.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.riskCessageFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.riskCessageTo05.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.riskCessageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessageFrom06.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.riskCessageFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.riskCessageTo06.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.riskCessageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessageFrom07.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessageFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.riskCessageTo07.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.riskCessageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.riskCessageFrom08.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.riskCessageFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.riskCessageTo08.setReverse(BaseScreenData.REVERSED);
			sv.riskCessageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.riskCessageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.premCessageFrom01.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.premCessageFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.premCessageTo01.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.premCessageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.premCessageFrom02.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.premCessageFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind33.isOn()) {
			sv.premCessageTo02.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind33.isOn()) {
			sv.premCessageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessageFrom03.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.premCessageFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			sv.premCessageTo03.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind34.isOn()) {
			sv.premCessageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.premCessageFrom04.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.premCessageFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.premCessageTo04.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.premCessageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind27.isOn()) {
			sv.premCessageFrom05.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind27.isOn()) {
			sv.premCessageFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.premCessageTo05.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.premCessageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessageFrom06.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind28.isOn()) {
			sv.premCessageFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.premCessageTo06.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind37.isOn()) {
			sv.premCessageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind29.isOn()) {
			sv.premCessageFrom07.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.premCessageFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			sv.premCessageTo07.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind38.isOn()) {
			sv.premCessageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessageFrom08.setReverse(BaseScreenData.REVERSED);
			sv.premCessageFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessageFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind39.isOn()) {
			sv.premCessageTo08.setReverse(BaseScreenData.REVERSED);
			sv.premCessageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind39.isOn()) {
			sv.premCessageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.eaage.setReverse(BaseScreenData.REVERSED);
			sv.eaage.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.eaage.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind41.isOn()) {
			sv.termIssageFrm01.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.termIssageFrm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind50.isOn()) {
			sv.termIssageTo01.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind50.isOn()) {
			sv.termIssageTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.termIssageFrm02.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.termIssageFrm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind51.isOn()) {
			sv.termIssageTo02.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind51.isOn()) {
			sv.termIssageTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind43.isOn()) {
			sv.termIssageFrm03.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.termIssageFrm03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind52.isOn()) {
			sv.termIssageTo03.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind52.isOn()) {
			sv.termIssageTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind44.isOn()) {
			sv.termIssageFrm04.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.termIssageFrm04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.termIssageTo04.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind53.isOn()) {
			sv.termIssageTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind45.isOn()) {
			sv.termIssageFrm05.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.termIssageFrm05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.termIssageTo05.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind54.isOn()) {
			sv.termIssageTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind46.isOn()) {
			sv.termIssageFrm06.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.termIssageFrm06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind55.isOn()) {
			sv.termIssageTo06.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind55.isOn()) {
			sv.termIssageTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			sv.termIssageFrm07.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind47.isOn()) {
			sv.termIssageFrm07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind56.isOn()) {
			sv.termIssageTo07.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind56.isOn()) {
			sv.termIssageTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind48.isOn()) {
			sv.termIssageFrm08.setReverse(BaseScreenData.REVERSED);
			sv.termIssageFrm08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind48.isOn()) {
			sv.termIssageFrm08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind57.isOn()) {
			sv.termIssageTo08.setReverse(BaseScreenData.REVERSED);
			sv.termIssageTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind57.isOn()) {
			sv.termIssageTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.riskCesstermFrom01.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.riskCesstermFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.riskCesstermTo01.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.riskCesstermTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.riskCesstermFrom02.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.riskCesstermFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.riskCesstermTo02.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.riskCesstermTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.riskCesstermFrom03.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.riskCesstermFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.riskCesstermTo03.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.riskCesstermTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.riskCesstermFrom04.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.riskCesstermFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.riskCesstermTo04.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.riskCesstermTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.riskCesstermFrom05.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.riskCesstermFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.riskCesstermTo05.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.riskCesstermTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.riskCesstermFrom06.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.riskCesstermFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.riskCesstermTo06.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.riskCesstermTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.riskCesstermFrom07.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.riskCesstermFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.riskCesstermTo07.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.riskCesstermTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.riskCesstermFrom08.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.riskCesstermFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.riskCesstermTo08.setReverse(BaseScreenData.REVERSED);
			sv.riskCesstermTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.riskCesstermTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.premCesstermFrom01.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.premCesstermFrom01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind59.isOn()) {
			sv.premCesstermTo01.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.premCesstermTo01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.premCesstermFrom02.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.premCesstermFrom02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.premCesstermTo02.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind60.isOn()) {
			sv.premCesstermTo02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.premCesstermFrom03.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.premCesstermFrom03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind61.isOn()) {
			sv.premCesstermTo03.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind61.isOn()) {
			sv.premCesstermTo03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.premCesstermFrom04.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.premCesstermFrom04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind62.isOn()) {
			sv.premCesstermTo04.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind62.isOn()) {
			sv.premCesstermTo04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.premCesstermFrom05.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.premCesstermFrom05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind63.isOn()) {
			sv.premCesstermTo05.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind63.isOn()) {
			sv.premCesstermTo05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.premCesstermFrom06.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.premCesstermFrom06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind64.isOn()) {
			sv.premCesstermTo06.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind64.isOn()) {
			sv.premCesstermTo06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.premCesstermFrom07.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.premCesstermFrom07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind65.isOn()) {
			sv.premCesstermTo07.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo07.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind65.isOn()) {
			sv.premCesstermTo07.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.premCesstermFrom08.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermFrom08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.premCesstermFrom08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind66.isOn()) {
			sv.premCesstermTo08.setReverse(BaseScreenData.REVERSED);
			sv.premCesstermTo08.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind66.isOn()) {
			sv.premCesstermTo08.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind68.isOn()) {
			sv.mortcls01.setReverse(BaseScreenData.REVERSED);
			sv.mortcls01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind68.isOn()) {
			sv.mortcls01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind69.isOn()) {
			sv.mortcls02.setReverse(BaseScreenData.REVERSED);
			sv.mortcls02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind69.isOn()) {
			sv.mortcls02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.mortcls03.setReverse(BaseScreenData.REVERSED);
			sv.mortcls03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind70.isOn()) {
			sv.mortcls03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind71.isOn()) {
			sv.mortcls04.setReverse(BaseScreenData.REVERSED);
			sv.mortcls04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind71.isOn()) {
			sv.mortcls04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind72.isOn()) {
			sv.mortcls05.setReverse(BaseScreenData.REVERSED);
			sv.mortcls05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.mortcls05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind73.isOn()) {
			sv.mortcls06.setReverse(BaseScreenData.REVERSED);
			sv.mortcls06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind73.isOn()) {
			sv.mortcls06.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind77.isOn()) {
			sv.liencd01.setReverse(BaseScreenData.REVERSED);
			sv.liencd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind77.isOn()) {
			sv.liencd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind78.isOn()) {
			sv.liencd02.setReverse(BaseScreenData.REVERSED);
			sv.liencd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind78.isOn()) {
			sv.liencd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind79.isOn()) {
			sv.liencd03.setReverse(BaseScreenData.REVERSED);
			sv.liencd03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind79.isOn()) {
			sv.liencd03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind80.isOn()) {
			sv.liencd04.setReverse(BaseScreenData.REVERSED);
			sv.liencd04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind80.isOn()) {
			sv.liencd04.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind81.isOn()) {
			sv.liencd05.setReverse(BaseScreenData.REVERSED);
			sv.liencd05.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind81.isOn()) {
			sv.liencd05.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind82.isOn()) {
			sv.liencd06.setReverse(BaseScreenData.REVERSED);
			sv.liencd06.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind82.isOn()) {
			sv.liencd06.setHighLight(BaseScreenData.BOLD);
		}
	}
	%>
	<div class="panel panel-default">	
    <div class="panel-body">
       <div class="row">
		  <div class="col-md-1">
    	 	<div class="form-group"> 	
                <label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
                <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
            </div>
          </div>
           <div class="col-md-3"></div>
          <div class="col-md-2">
    	 	<div class="form-group"> 	
                <label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
                <%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:75px;" >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
             </div>
          </div>
          <div class="col-md-1"></div>
              <div class="col-md-2">
    	 	<div class="form-group"> 	
                <label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
                   <div class="input-group"> 
                               <%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		<%
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
             </div>
            </div>    
        </div>
</div>
      <div class="row">	
		    <div class="col-md-3"> 
    	 		<div class="form-group"> 	        				    			  
					   <label><%=resourceBundleHandler.gettingValueFromBundle("Valid From")%></label>
					    	 <table>
					           <tr>
					              <td>
					              <%					
		                      if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					        } else  {
					       if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					              </td>
					              <td style="padding-left:10px;padding-right:10px"><%=resourceBundleHandler.gettingValueFromBundle("To")%></td>
					              <td>
					              <%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					} else  {
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>	
					              </td>
					             </tr>
					            </table>  
					            </div>
					         </div>
					      </div>
					       <div class="row">	
		    <div class="col-md-3"> 
    	 		<div class="form-group"> 	        				    			  
					   <label><%=resourceBundleHandler.gettingValueFromBundle("Sum insured min")%></label>
					    <%	
			qpsf = fw.getFieldXMLDef((sv.sumInsMin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumInsMin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='sumInsMin' 
type='text'
	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumInsMin.getLength(), sv.sumInsMin.getScale(),3)%>'
maxLength='<%= sv.sumInsMin.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumInsMin)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.sumInsMin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumInsMin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.sumInsMin).getColor()== null  ? 
			"input_cell" :  (sv.sumInsMin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>
>
				</div>
			</div>
			 <div class="col-md-1"> </div>
			 <div class="col-md-3"> 
    	 		<div class="form-group"> 	        				    			  
					   <label><%=resourceBundleHandler.gettingValueFromBundle("Max")%></label>
					   <%	
			qpsf = fw.getFieldXMLDef((sv.sumInsMax).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumInsMax,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>
<input name='sumInsMax' 
type='text'
	value='<%=valueThis%>'
	<%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumInsMax.getLength(), sv.sumInsMax.getScale(),3)%>'
maxLength='<%= sv.sumInsMax.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumInsMax)' onKeyUp='return checkMaxLength(this)'
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.sumInsMax).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumInsMax).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.sumInsMax).getColor()== null  ? 
			"input_cell" :  (sv.sumInsMax).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
<%
	} 
%>>
			    </div>
			  </div>			  
</div>   		   	
<div class="row">
<div class="col-md-2">
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText8)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText9)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText10)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText11)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText12)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText13)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText14)%></label>
</div>
<div class="col-md-1">
<label><%=smartHF.getLit(generatedText15)%></label>
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText17)%></label>
</div>     
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo05)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.ageIssageFrm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.ageIssageTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
    </div>
&nbsp;
<div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText18)%></label>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCessageFrom08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCessageTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
</div>
&nbsp;
<div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText19)%></label>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCessageFrom08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCessageTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
</div>
     &nbsp;
  <div class="row">
      <div class="col-md-4">     
   	<label><%=smartHF.getLit(generatedText27)%></label>
   	</div>
      <div class="col-md-1"> 
   <%=smartHF.getHTMLVar(fw, sv.eaage)%>
   	</div>
   	 <div class="col-md-2"> 
   	<label><%=smartHF.getLit(generatedText20)%></label>  
    </div>
     </div>
   &nbsp;  
<div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText21)%></label>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.termIssageFrm08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.termIssageTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
</div>
&nbsp;
 <div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText22)%></label>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.riskCesstermFrom08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.riskCesstermTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
</div>
&nbsp;
<div class="row">
<div class="col-md-2">
<label><%=smartHF.getLit(generatedText23)%></label>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo01, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo02, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo03, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo04, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo05, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo06, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo07, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
<div class="col-md-1">
<div class="input-group">
<%=smartHF.getHTMLVar(fw, sv.premCesstermFrom08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
<%=smartHF.getHTMLVar(fw, sv.premCesstermTo08, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
</div>
</div>
</div>
&nbsp;
   &nbsp;&nbsp; 
            <div class="row">
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Special Terms"))%></label>
        				<%if ((new Byte((sv.specind).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<input type='checkbox' name='specind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(specind)' onKeyUp='return checkMaxLength(this)'    
							<%  if((sv.specind).getColor()!=null){%>style='background-color:#FF0000;'
							<%}	if((sv.specind).toString().trim().equalsIgnoreCase("Y")){
							%>checked
							<%} if((sv.specind).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
								   disabled
							<% } %>
							class ='UICheck' onclick="handleCheckBox('specind')"/>
							<input type='checkbox' name='specind' value=' ' 
							<% if(!(sv.specind).toString().trim().equalsIgnoreCase("Y")) {%>
							checked
							<% } %>
							style="visibility: hidden" onclick="handleCheckBox('specind')" class ='UICheck'/>
						<% } %>
        			</div>
        		</div>
<div class="col-md-4"></div>
<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Default Option"))%></label>
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"zdefdivopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("zdefdivopt");
	optionValue = makeDropDownList( mappedItems , sv.zdefdivopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.zdefdivopt.getFormData()).toString().trim());  
%>
<% 
	if((new Byte((sv.zdefdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
	<% }else {%>
<% if("red".equals((sv.zdefdivopt).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
<select name='zdefdivopt' type='list' style="width:140px;"
<% 
	if((new Byte((sv.zdefdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zdefdivopt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.zdefdivopt).getColor())){
%>
</div>
<%}%>
<%}%>
</div>
</div>
</div>
<div class="row">
                  <div class="col-md-1">     
   	              <label><%=resourceBundleHandler.gettingValueFromBundle("Dividend Options")%></label>
   	                              </div>
   	                   <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 90px;"> 
   	         <%	
	                longValue = sv.zdivopt01.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt01).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt01' id='zdivopt01'
                     type='text' 
                     value='<%=sv.zdivopt01.getFormData()%>' 
                     maxLength='<%=sv.zdivopt01.getLength()%>' 
                     size='<%=sv.zdivopt01.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt01)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt01).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell">
                      <%
	                }else if((new Byte((sv.zdivopt01).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell">
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt01).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt01).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	           </div>
   	          <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt02.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt02).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt02' id='zdivopt02'
                     type='text' 
                     value='<%=sv.zdivopt02.getFormData()%>' 
                     maxLength='<%=sv.zdivopt02.getLength()%>' 
                     size='<%=sv.zdivopt02.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt02)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt02).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell">
                      <%
	                }else if((new Byte((sv.zdivopt02).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell">
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt02).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt02).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>
   	         	          <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt03.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt03).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt03' id='zdivopt03'
                     type='text' 
                     value='<%=sv.zdivopt03.getFormData()%>' 
                     maxLength='<%=sv.zdivopt03.getLength()%>' 
                     size='<%=sv.zdivopt03.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt03)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt03).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell"	 >
                      <%
	                }else if((new Byte((sv.zdivopt03).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell">
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt03).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt03).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>
       <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt04.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt04).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt04' id='zdivopt04'
                     type='text' 
                     value='<%=sv.zdivopt04.getFormData()%>' 
                     maxLength='<%=sv.zdivopt04.getLength()%>' 
                     size='<%=sv.zdivopt04.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt04)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt04).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell"	 >
                      <%
	                }else if((new Byte((sv.zdivopt04).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt04).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt04).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>
   	         	          <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt05.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt05).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt05' id='zdivopt05'
                     type='text' 
                     value='<%=sv.zdivopt05.getFormData()%>' 
                     maxLength='<%=sv.zdivopt05.getLength()%>' 
                     size='<%=sv.zdivopt05.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt05)' onKeyUp='return checkMaxLength(this)'
                    <% 
	                  if((new Byte((sv.zdivopt05).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell">
                      <%
	                }else if((new Byte((sv.zdivopt05).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt05).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt05).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>
   	         	          <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt06.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt06).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt06' id='zdivopt06'
                     type='text' 
                     value='<%=sv.zdivopt06.getFormData()%>' 
                     maxLength='<%=sv.zdivopt06.getLength()%>' 
                     size='<%=sv.zdivopt06.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt06)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt06).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell">
                      <%
	                }else if((new Byte((sv.zdivopt06).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt06).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt06).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>
   	        <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	                longValue = sv.zdivopt07.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.zdivopt07).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='zdivopt07' id='zdivopt07'
                     type='text' 
                     value='<%=sv.zdivopt07.getFormData()%>' 
                     maxLength='<%=sv.zdivopt07.getLength()%>' 
                     size='<%=sv.zdivopt07.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(zdivopt07)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.zdivopt07).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell">
                      <%
	                }else if((new Byte((sv.zdivopt07).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt07')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
      <%
	               }else { 
                        %>
                   class = ' <%=(sv.zdivopt07).getColor()== null  ? 
                 "input_cell" :  (sv.zdivopt07).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >
                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('zdivopt07')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	        </div>
   	        </div>                      
   	        </div>                      
   	     &nbsp;                         
 <div class="row">
                  <div class="col-md-2">     
   	              <label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
                   </div>
   	              <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	            <%	
	                longValue = sv.mortcls01.getFormData();  
                     %>
                      <% 
	                 if((new Byte((sv.mortcls01).getEnabled()))
	                 .compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
                      %>  
                     <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		          <%if(longValue != null){%>
	   		          <%=longValue%>
	   		         <%}%>
	                  </div>
                      <%
                     longValue = null;
                     %>
                     <% }else {%> 
                     <input name='mortcls01' id='mortcls01'
                     type='text' 
                     value='<%=sv.mortcls01.getFormData()%>' 
                     maxLength='<%=sv.mortcls01.getLength()%>' 
                     size='<%=sv.mortcls01.getLength()%>'
                     onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls01)' onKeyUp='return checkMaxLength(this)'  
                    <% 
	                  if((new Byte((sv.mortcls01).getEnabled()))
	                  .compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
                       %>  
                      readonly="true"
                      class="output_cell"	 >
                      <%
	                }else if((new Byte((sv.mortcls01).getHighLight())).
		            compareTo(new Byte(BaseScreenData.BOLD)) == 0){
                    %>	
                    class="bold_cell" > 
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                   <%
	               }else { 
                        %>   
                   class = ' <%=(sv.mortcls01).getColor()== null  ? 
                 "input_cell" :  (sv.mortcls01).getColor().equals("red") ? 
                   "input_cell red reverse" : "input_cell" %>' >

                     	 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
                      <%}longValue = null;} %>
   	                </div>
   	                </div>     
   	               <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.mortcls02.getFormData();  
%>
<% 
	if((new Byte((sv.mortcls02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='mortcls02' id='mortcls02'
type='text' 
value='<%=sv.mortcls02.getFormData()%>' 
maxLength='<%=sv.mortcls02.getLength()%>' 
size='<%=sv.mortcls02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls02)' onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.mortcls02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.mortcls02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
  	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.mortcls02).getColor()== null  ? 
"input_cell" :  (sv.mortcls02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                 </div>
   	                 <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
<%	
	longValue = sv.mortcls03.getFormData();  
%>
<% 
	if((new Byte((sv.mortcls03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='mortcls03' id='mortcls03'
type='text' 
value='<%=sv.mortcls03.getFormData()%>' 
maxLength='<%=sv.mortcls03.getLength()%>' 
size='<%=sv.mortcls03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls03)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.mortcls03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.mortcls03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.mortcls03).getColor()== null  ? 
"input_cell" :  (sv.mortcls03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                 <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.mortcls04.getFormData();  
%>
<% 
	if((new Byte((sv.mortcls04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='mortcls04' id='mortcls04'
type='text' 
value='<%=sv.mortcls04.getFormData()%>' 
maxLength='<%=sv.mortcls04.getLength()%>' 
size='<%=sv.mortcls04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls04)' onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.mortcls04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.mortcls04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.mortcls04).getColor()== null  ? 
"input_cell" :  (sv.mortcls04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.mortcls05.getFormData();  
%>
<% 
	if((new Byte((sv.mortcls05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='mortcls05' id='mortcls05'
type='text' 
value='<%=sv.mortcls05.getFormData()%>' 
maxLength='<%=sv.mortcls05.getLength()%>' 
size='<%=sv.mortcls05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls05)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.mortcls05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.mortcls05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
  	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.mortcls05).getColor()== null  ? 
"input_cell" :  (sv.mortcls05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                 <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	longValue = sv.mortcls06.getFormData();  
%>
<% 
	if((new Byte((sv.mortcls06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='mortcls06' id='mortcls06'
type='text' 
value='<%=sv.mortcls06.getFormData()%>' 
maxLength='<%=sv.mortcls06.getLength()%>' 
size='<%=sv.mortcls06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mortcls06)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.mortcls06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.mortcls06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.mortcls06).getColor()== null  ? 
"input_cell" :  (sv.mortcls06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('mortcls06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
              </div>
                &nbsp;&nbsp; 
            <div class="row"> 
      <div class="col-md-2"> 
   	                <div class="input-group">
   	              <label><%=resourceBundleHandler.gettingValueFromBundle("Lien Codes")%></label>
                   </div>
                   </div>
   	               <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;">  
   	                <%	
	longValue = sv.liencd01.getFormData();  
%>
<% 
	if((new Byte((sv.liencd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd01' id='liencd01'
type='text' 
value='<%=sv.liencd01.getFormData()%>' 
maxLength='<%=sv.liencd01.getLength()%>' 
size='<%=sv.liencd01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd01)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.liencd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
  	<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd01).getColor()== null  ? 
"input_cell" :  (sv.liencd01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.liencd02.getFormData();  
%>
<% 
	if((new Byte((sv.liencd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd02' id='liencd02'
type='text' 
value='<%=sv.liencd02.getFormData()%>' 
maxLength='<%=sv.liencd02.getLength()%>' 
size='<%=sv.liencd02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd02)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.liencd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd02).getColor()== null  ? 
"input_cell" :  (sv.liencd02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                 <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.liencd03.getFormData();  
%>
<% 
	if((new Byte((sv.liencd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd03' id='liencd03'
type='text' 
value='<%=sv.liencd03.getFormData()%>' 
maxLength='<%=sv.liencd03.getLength()%>' 
size='<%=sv.liencd03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd03)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.liencd03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd03).getColor()== null  ? 
"input_cell" :  (sv.liencd03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                 </div>
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.liencd04.getFormData();  
%>
<% 
	if((new Byte((sv.liencd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd04' id='liencd04'
type='text' 
value='<%=sv.liencd04.getFormData()%>' 
maxLength='<%=sv.liencd04.getLength()%>' 
size='<%=sv.liencd04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd04)' onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.liencd04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd04).getColor()== null  ? 
"input_cell" :  (sv.liencd04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.liencd05.getFormData();  
%>
<% 
	if((new Byte((sv.liencd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd05' id='liencd05'
type='text' 
value='<%=sv.liencd05.getFormData()%>' 
maxLength='<%=sv.liencd05.getLength()%>' 
size='<%=sv.liencd05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd05)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.liencd05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd05).getColor()== null  ? 
"input_cell" :  (sv.liencd05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	               <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.liencd06.getFormData();  
%>
<% 
	if((new Byte((sv.liencd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='liencd06' id='liencd06'
type='text' 
value='<%=sv.liencd06.getFormData()%>' 
maxLength='<%=sv.liencd06.getLength()%>' 
size='<%=sv.liencd06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(liencd06)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.liencd06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.liencd06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.liencd06).getColor()== null  ? 
"input_cell" :  (sv.liencd06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
<span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('liencd06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
              </div>
               &nbsp;&nbsp; 
    <div class="row">
                  <div class="col-md-2"> 
                   <div class="input-group">    
   	              <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
                   </div>
                   </div>
   	             <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	longValue = sv.prmbasis01.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis01' id='prmbasis01'
type='text' 
value='<%=sv.prmbasis01.getFormData()%>' 
maxLength='<%=sv.prmbasis01.getLength()%>' 
size='<%=sv.prmbasis01.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis01)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.prmbasis01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
class="bold_cell" >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis01).getColor()== null  ? 
"input_cell" :  (sv.prmbasis01).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis01')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div> 	       
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	longValue = sv.prmbasis02.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis02' id='prmbasis01'
type='text' 
value='<%=sv.prmbasis02.getFormData()%>' 
maxLength='<%=sv.prmbasis02.getLength()%>' 
size='<%=sv.prmbasis02.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis02)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.prmbasis02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){	
%>	
class="bold_cell" >
  <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis02).getColor()== null  ? 
"input_cell" :  (sv.prmbasis02).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis02')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	               <%	
	longValue = sv.prmbasis03.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis03' id='prmbasis03'
type='text' 
value='<%=sv.prmbasis03.getFormData()%>' 
maxLength='<%=sv.prmbasis03.getLength()%>' 
size='<%=sv.prmbasis03.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis03)' onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.prmbasis03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis03).getColor()== null  ? 
"input_cell" :  (sv.prmbasis03).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >
 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis03')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>
   	                </div>
   	                </div>
   	                
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.prmbasis04.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>		
	   		<%=longValue%>
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis04' id='prmbasis04'
type='text' 
value='<%=sv.prmbasis04.getFormData()%>' 
maxLength='<%=sv.prmbasis04.getLength()%>' 
size='<%=sv.prmbasis04.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis04)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.prmbasis04).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis04).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis04).getColor()== null  ? 
"input_cell" :  (sv.prmbasis04).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis04')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>

   	                </div>
   	                </div>
   	                
   	                 <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.prmbasis05.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis05' id='prmbasis05'
type='text' 
value='<%=sv.prmbasis05.getFormData()%>' 
maxLength='<%=sv.prmbasis05.getLength()%>' 
size='<%=sv.prmbasis05.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis05)' onKeyUp='return checkMaxLength(this)'  
<% 
	if((new Byte((sv.prmbasis05).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis05).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis05).getColor()== null  ? 
"input_cell" :  (sv.prmbasis05).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis05')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>

   	                </div>
   	                </div>
   	                
   	                <div class="col-md-2" style="max-width: 120px;"  >
   	        <div class="input-group" style="max-width: 100px;"> 
   	                <%	
	longValue = sv.prmbasis06.getFormData();  
%>
<% 
	if((new Byte((sv.prmbasis06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>
<% }else {%> 
<input name='prmbasis06' id='prmbasis06'
type='text' 
value='<%=sv.prmbasis06.getFormData()%>' 
maxLength='<%=sv.prmbasis06.getLength()%>' 
size='<%=sv.prmbasis06.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(prmbasis06)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.prmbasis06).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 >
<%
	}else if((new Byte((sv.prmbasis06).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
  <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%
	}else { 
%>
class = ' <%=(sv.prmbasis06).getColor()== null  ? 
"input_cell" :  (sv.prmbasis06).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

 <span class="input-group-btn">
					        		<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('prmbasis06')); doAction('PFKEY04')">
					        			<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
									</button>
					      		</span>
<%}longValue = null;} %>

   	                </div>
   	                </div>
              </div>
     </div>
     </div>
<%}%>
<%if (sv.Sh505protectWritten.gt(0)) {%>
	<%Sh505protect.clearClassString(sv);%>
	<%{}%>
<%}%>
<%@ include file="/POLACommon2NEW.jsp"%>
<!-- ILIFE-2438 Coding and Unit testing - Life Cross Browser - Sprint 1 D5: Task 5 starts-->
<style>
div[id*='ageIssageFrm']{padding-right:2px !important} 
div[id*='ageIssageTo']{padding-right:2px !important} 
div[id*='riskCessageFrom']{padding-right:2px !important}
div[id*='riskCessageTo']{padding-right:2px !important}
div[id*='premCessageFrom']{padding-right:2px !important}
div[id*='premCessageTo']{padding-right:2px !important} 
div[id*='termIssageFrm']{padding-right:2px !important} 
div[id*='termIssageTo']{padding-right:2px !important} 
div[id*='riskCesstermFrom']{padding-right:2px !important} 
div[id*='riskCesstermTo']{padding-right:2px !important}
div[id*='premCesstermFrom']{padding-right:2px !important}
div[id*='premCesstermTo']{padding-right:2px !important}
</style> 
<!-- ILIFE-2438 Coding and Unit testing - Life Cross Browser - Sprint 1 D5: Task 5 ends-->