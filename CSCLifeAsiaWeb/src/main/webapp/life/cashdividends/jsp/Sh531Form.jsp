

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH531";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%Sh531ScreenVars sv = (Sh531ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates Effective     ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Participating ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y/N)");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid Up Addition Cash Value Code ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.divdParticipant.setReverse(BaseScreenData.REVERSED);
			sv.divdParticipant.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.divdParticipant.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.puCvCode.setReverse(BaseScreenData.REVERSED);
			sv.puCvCode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.puCvCode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			<div class="input-group">
        			   <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>
        	
        	
        <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			<div class="input-group">
        			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>
        	
        	
        <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<table>
        			<tr>
        			<td>
        			<%					
				if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
        			
        		</div>
        	</div>			
        </div>	
    
 
     <div class="row">	
		       	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>
				   <table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td>&nbsp;to&nbsp;</td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
		</div>
		
		
		&nbsp;
	  <div class="row">
	 <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Participating"))%></label>
        			         
                          
                          <%if ((new Byte((sv.divdParticipant).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input type='checkbox' name='divdParticipant' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(divdParticipant)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.divdParticipant).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.divdParticipant).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.divdParticipant).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('divdParticipant')"/>

<input type='checkbox' name='hlptypfld' value='N' 

<% if(!(sv.divdParticipant).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('divdParticipant')"/>
<%}%>  			
        			 			
		       
		          </div>
	</div>
	</div>
	
	
	  <div class="row">
	 <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Paid Up Addition Cash Value Code"))%></label>
        			<div class="input-group">	
        			<input name='puCvCode' 
type='text'

<%

		formatValue = (sv.puCvCode.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.puCvCode.getLength()%>'
maxLength='<%= sv.puCvCode.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(puCvCode)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.puCvCode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.puCvCode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.puCvCode).getColor()== null  ? 
			"input_cell" :  (sv.puCvCode).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        			</div>
        		</div>
        	</div>
        </div>				            
</div>
</div>			

<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>

<%@ include file="/POLACommon2NEW.jsp"%>

