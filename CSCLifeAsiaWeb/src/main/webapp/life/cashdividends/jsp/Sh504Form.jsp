<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH504";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*"%>

<%
	Sh504ScreenVars sv = (Sh504ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium w/ Tax");
%>
<%
	{
		// change by yy for ILIFE-985
		if (appVars.ind12.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind02.isOn()) {
			sv.sumin.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
			sv.sumin.setColor(BaseScreenData.RED);
		}
		if (appVars.ind01.isOn()) {
			sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind14.isOn()) {
			sv.sumin.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind03.isOn()) {
			sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind15.isOn()) {
			sv.riskCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind15.isOn()) {
			sv.riskCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.riskCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.riskCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.riskCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind29.isOn()) {
			sv.riskCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind41.isOn()) {
			sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.premCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.premCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.premCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.premCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.premCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.premCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.premCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind19.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind08.isOn()) {
			sv.liencd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind20.isOn()) {
			sv.liencd.setColor(BaseScreenData.RED);
		}
		if (appVars.ind07.isOn()) {
			sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind20.isOn()) {
			sv.liencd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			sv.bappmeth.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind37.isOn()) {
			sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
			sv.bappmeth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind36.isOn()) {
			sv.bappmeth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind09.isOn()) {
			sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind21.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.instPrem.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
			sv.instPrem.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.instPrem.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.ratypind.setReverse(BaseScreenData.REVERSED);
			sv.ratypind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind40.isOn()) {
			sv.ratypind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.ratypind.setEnabled(BaseScreenData.DISABLED);
		}
		/*ILIFE-5942*/
		if (appVars.ind34.isOn()) {
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.ratypind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind47.isOn()) {
			//sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.pbind.setReverse(BaseScreenData.REVERSED);
			sv.pbind.setColor(BaseScreenData.RED);
		}

		if (appVars.ind10.isOn()) {
			sv.polinc.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind26.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind27.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind24.isOn()) {
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.numavail.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numapp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind11.isOn()) {
			sv.numapp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind25.isOn()) {
			sv.numapp.setReverse(BaseScreenData.REVERSED);
			sv.numapp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind24.isOn()) {
			sv.numapp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.zdivopt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind42.isOn()) {
			sv.zdivopt.setReverse(BaseScreenData.REVERSED);
			sv.zdivopt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.zdivopt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.payeesel.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind43.isOn()) {
			sv.payeesel.setReverse(BaseScreenData.REVERSED);
			sv.payeesel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind43.isOn()) {
			sv.payeesel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.paymth.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind44.isOn()) {
			sv.paymth.setReverse(BaseScreenData.REVERSED);
			sv.paymth.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind44.isOn()) {
			sv.paymth.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind35.isOn()) {
			sv.paycurr.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind45.isOn()) {
			sv.paycurr.setReverse(BaseScreenData.REVERSED);
			sv.paycurr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.paycurr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind47.isOn()) {
			//sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.taxind.setReverse(BaseScreenData.REVERSED);
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (appVars.ind48.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind47.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		// change end
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		/*ILIFE-3421 starts*/
		if (appVars.ind58.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
			sv.prmbasis.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind59.isOn()) {
			sv.prmbasis.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind60.isOn()) {
			sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		//BRD-NBP-011 ends
	  
		

		
		if (appVars.ind75.isOn()) {
		sv.aepaydet.setEnabled(BaseScreenData.DISABLED);
		}
	
		//ILJ-43
		/* if (appVars.ind123.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		//end

	}
%>

<!-- <style>
.panel-default{
height: 800px !important;
}
</style> -->
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract no"))%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td>
<td>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>

						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 300px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life Assured"))%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifcnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>

						<%
							if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.linsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 200px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>

			<div class="col-md-5"></div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData()))%></label>
					<div class="input-group">
						<%
							if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.zagelit.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.zagelit.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<%-- <div id='zagelit' class='label_txt'class='label_txt'  onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div> --%>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<!-- <div class="input-group"> -->
						<%
							qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

							if (!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 80px;" />

						<%
							}
						%>


						<%
							longValue = null;
							formatValue = null;
						%>
						<!--  </div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint life"))%></label>
					<table>
					<tr>
					<td>
					
						<%
							if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 65px;"> <%-- ILIFE-6050 --%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell"
							style="min-width: 65px; padding-right: 1px"></div> <%-- ILIFE-6050 --%>

						<%
							}
						%>

						<%
							longValue = null;
							formatValue = null;
						%>

						</td>
						<td style="padding-left: 2px;">
						<div
							class='<%=(sv.jlinsname.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'
							style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "min-width:65px;" : "min-width:100px;"%>'>
							<%
								if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlinsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlinsname.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life no"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 70px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<!-- </div> -->
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage no"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.coverage.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 70px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>

					</div>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rider no"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rider.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 70px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<!-- </div> -->
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Statutory fund"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("statFund");
								longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());
						%>


						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 100px;">
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						<!-- </div> -->
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Section"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 70px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>

					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sub-section"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSubsect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.statSubsect.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sum Assured"))%></label>
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							valueThis = smartHF.getPicFormatted(qpsf, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='sumin' type='text'
							<%if ((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; min-width: 100px;" <%}%>
							value='<%=valueThis%>'
							<%valueThis = valueThis;
			if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=valueThis%>' <%}%>
							size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.sumin.getLength(), sv.sumin.getScale(), 3)%>'
							maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.sumin.getLength(), sv.sumin.getScale(), 3) - 4%>'
							onFocus='doFocus(this),onFocusRemoveCommas(this)'
							onHelp='return fieldHelp(sumin)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event,true);'
							onBlur='return doBlurNumberNew(event,true);'
							<%if ((new Byte((sv.sumin).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.sumin).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.sumin).getColor() == null ? "input_cell"
						: (sv.sumin).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Mortality class"))%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("mortcls");
							optionValue = makeDropDownList(mappedItems, sv.mortcls.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
							formatValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
							
						%>

						<%
							if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>

						<%
							longValue = null;
							formatValue = null;
						%>

						<%
							} else {
						%>
						<select name='mortcls' type='list'
							<%if ((new Byte((sv.mortcls).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							style="max-width: 800px;"
							<%} else if ((new Byte((sv.mortcls).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" style="min-width:200px;" <%} else {%>
							class=' <%=(sv.mortcls).getColor() == null ? "input_cell"
							: (sv.mortcls).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							style="min-width:200px;" <%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Currency"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "currcd" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("currcd");
								longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());
						%>


						<%
							if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.currcd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.currcd.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "min-width:140px;"
						: "min-width:180px;"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</div>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<!--
<%StringData BAPPMETH_LBL = resourceBundleHandler.gettingValueFromBundle(StringData.class,
					"Bonus Appl Method");%>
<%=smartHF.getLit(0, 0, BAPPMETH_LBL).replace("absolute", "relative")%>
-->

					<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl Method")%></label>
					<div class="input-group">

						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "bappmeth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("bappmeth");
							optionValue = makeDropDownList(mappedItems, sv.bappmeth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.bappmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<!-- ILIFE 1560 -->
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:160px;" : "width:180px;"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<!-- ILIFE 1560 -->

						<select name='bappmeth' type='list' style="width: 180px;"
							<%if ((new Byte((sv.bappmeth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.bappmeth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.bappmeth).getColor() == null ? "input_cell"
							: (sv.bappmeth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<!--  ILJ-43 -->
				 <%-- <%
					if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%> --%>	
				<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
				<% } else{%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
				<% } %>
				<!--  END  -->	
					<div class="input-group three-controller">
						<table>
							<tr>
								<td>
									<%
										if (((BaseScreenData) sv.riskCessAge) instanceof StringBase) {
									%> <%=smartHF.getRichText(0, 0, fw, sv.riskCessAge, (sv.riskCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%> <%
 	} else if (((BaseScreenData) sv.riskCessAge) instanceof DecimalData) {
 %> <%
 	if (sv.riskCessAge.equals(0)) {
 %> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 40px; \' ")%>
									<%
										} else {
									%> <%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
									<%
										}
									%> <%
 	} else {
 %> hello <%
 	}
 %>
								</td>
								<td>
									<%
										if (((BaseScreenData) sv.riskCessTerm) instanceof StringBase) {
									%> <%=smartHF.getRichText(0, 0, fw, sv.riskCessTerm, (sv.riskCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%> <%
 	} else if (((BaseScreenData) sv.riskCessTerm) instanceof DecimalData) {
 %> <%
 	if (sv.riskCessTerm.equals(0)) {
 %> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
									<%
										} else {
									%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
									<%
										}
									%> <%
 	} else {
 %> hello <%
 	}
 %>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<!--  ILJ-43 -->
				<%-- <%
					if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%> --%>
				<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess date"))%></label>
				<% } else{%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess date"))%></label>
				<% } %>
				<!--  END  -->	
					
					<div class="input-group">
						<%
							longValue = sv.riskCessDateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:80px;" : "min-width:80px;"%>'>

							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>


						<%
							} else {
						%>

						<%
							if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
						%>


						<%
							} else if ((new Byte((sv.riskCessDateDisp).getHighLight()))
										.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>

						<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>



						<%
							} else {
						%>



						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>


						<%
							}
								longValue = null;
							}
						%>

					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
					<div class="input-group">

						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "liencd" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("liencd");
							optionValue = makeDropDownList(mappedItems, sv.liencd.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>

						<!-- ILIFE 1560 -->
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:175px;" : "width:140px;"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<!-- ILIFE 1560 -->

						<select name='liencd' type='list' style="width: 140px;"
							<%if ((new Byte((sv.liencd).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.liencd).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.liencd).getColor() == null ? "input_cell"
							: (sv.liencd).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>


		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term"))%></label>
					<div class="input-group three-controller">
						<table>
							<tr>
								<td>
									<%
										if (((BaseScreenData) sv.premCessAge) instanceof StringBase) {
									%> <%=smartHF.getRichText(0, 0, fw, sv.premCessAge, (sv.premCessAge.getLength() + 1), null)
						.replace("absolute", "relative")%> <%
 	} else if (((BaseScreenData) sv.premCessAge) instanceof DecimalData) {
 %> <%
 	if (sv.premCessAge.equals(0)) {
 %> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'output_cell \' style=\'width: 40px; \' ")%>
									<%
										} else {
									%> <%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
									<%
										}
									%> <%
 	} else {
 %> hello <%
 	}
 %>
								</td>
								<td>
									<%
										if (((BaseScreenData) sv.premCessTerm) instanceof StringBase) {
									%> <%=smartHF.getRichText(0, 0, fw, sv.premCessTerm, (sv.premCessTerm.getLength() + 1), null)
						.replace("absolute", "relative")%> <%
 	} else if (((BaseScreenData) sv.premCessTerm) instanceof DecimalData) {
 %> <%
 	if (sv.premCessTerm.equals(0)) {
 %> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 40px; \' ")%>
									<%
										} else {
									%> <%=smartHF
							.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
									<%
										}
									%> <%
 	} else {
 %> hello <%
 	}
 %>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prem cess date"))%></label>
					<div class="input-group">
						<%
							longValue = sv.premCessDateDisp.getFormData();
						%>

						<%
							if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "min-width:80px;"
						: "min-width:80px;"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>


						<%
							} else {
						%>


						<%
							if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
						%>


						<%
							} else if ((new Byte((sv.premCessDateDisp).getHighLight()))
										.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
						%>



						<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>



						<%
							} else {
						%>


						<div class="input-group date form_date col-md-12" data-date=""
							data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
							data-link-format="dd/mm/yyyy">
							<%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-calendar"></span></span>
						</div>


						<%
							}
								longValue = null;
							}
						%>
					</div>
				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<%
						formatValue = null;
					%>

					<%
						if ((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)"))%></label>
					<div class="input-group">

						<input name='select' type='text'
							<%formatValue = (sv.select.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%> size='<%=sv.select.getLength()%>'
							maxLength='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(select)'
							onKeyUp='return checkMaxLength(this)' style="min-width: 180px;"
							<%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| (((ScreenModel) fw).getVariables().isScreenProtected())) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.select).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.select).getColor() == null ? "input_cell"
							: (sv.select).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>

					</div>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<%
						if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dial Down Option"))%></label>
					<div class="input-group">
						<%
							}
						%>

						<%
							if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "dialdownoption" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dialdownoption");
								optionValue = makeDropDownList(mappedItems, sv.dialdownoption.getFormData(), 2, resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.dialdownoption).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
										|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.dialdownoption).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 180px;">
							<%
								}
							%>

							<select name='dialdownoption' type='list' style="width: 180px;"
								<%if ((new Byte((sv.dialdownoption).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.dialdownoption).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell' <%}%>>
								<%=optionValue%>
							</select>
							<%
								if ("red".equals((sv.dialdownoption).getColor())) {
							%>
						</div>
						<%
							}
						%>

						<%
							}
							}
						%>
						<%
							longValue = null;
						%>


					</div>
				</div>
			</div>
		<!-- </div> --> <!-- ILIFE-6050 -->
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee"))%></label>
					<table>
					<tr>
					<td>
					
							<%
					          if ((new Byte((sv.payeesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					                                                      || fw.getVariables().isScreenProtected()) {
							%>                   
							
							<div style="margin-right: 2px;"	><%=smartHF.getHTMLVarExt(fw, sv.payeesel)%></div>			
					                                         
					   
					        <%
								} else {
							%>
					        <div class="input-group" style="width: 150px;">
					            <%=smartHF.getRichTextInputFieldLookup(fw, sv.payeesel)%>
					             <span class="input-group-btn">
					             <button class="btn btn-info"
					                style="font-size: 19px; border-bottom-width: 2px !important;"
					                type="button"
					                onClick="doFocus(document.getElementById('payeesel')); doAction('PFKEY04')">
					                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					            </button>
					        </div>
					        <%
					         }
					        %>     
						
						
						</td>
						<td>
						
						<%
							if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payeenme.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payeenme.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="max-width: 200px;margin-left: -1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Cash Dividend Option"))%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "zdivopt" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("zdivopt");
							optionValue = makeDropDownList(mappedItems, sv.zdivopt.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.zdivopt.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.zdivopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell' style="max-width: 800px;">
							<%=longValue%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<select name='zdivopt' type='list' style="max-width: 800px;"
							<%if ((new Byte((sv.zdivopt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.zdivopt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.zdivopt).getColor() == null ? "input_cell"
							: (sv.zdivopt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("MOP"))%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "paymth" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("paymth");
							optionValue = makeDropDownList(mappedItems, sv.paymth.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.paymth.getFormData()).toString().trim());
							formatValue = formatValue((sv.paymth.getFormData()).toString());
						%>

						<%
							if ((new Byte((sv.paymth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell' style="max-width: 800px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<select name='paymth' type='list' style="max-width: 800px;"
							<%if ((new Byte((sv.paymth).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.paymth).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.paymth).getColor() == null ? "input_cell"
							: (sv.paymth).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Currency"))%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "paycurr" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("paycurr");
							optionValue = makeDropDownList(mappedItems, sv.paycurr.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.paycurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| fw.getVariables().isScreenProtected()) {
						%>
						<div class='output_cell' style="max-width: 800px;">
							<%=longValue%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>
						<select name='paycurr' type='list' style="max-width: 800px;"
							<%if ((new Byte((sv.paycurr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.paycurr).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.paycurr).getColor() == null ? "input_cell"
							: (sv.paycurr).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
							<%=optionValue%>
						</select>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<%
			if ((new Byte((sv.polinc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total policies in plan"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (!((sv.polinc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.polinc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.polinc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>

					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number available"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (!((sv.numavail.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numavail.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							} else {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numavail.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}
							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Number applicable"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if ((new Byte((sv.numapp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						qpsf = fw.getFieldXMLDef((sv.numapp).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN);
					%>

					<input name='numapp' type='text'
						value='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>'
						<%valueThis = smartHF.getPicFormatted(qpsf, sv.numapp);
					if (valueThis != null && valueThis.trim().length() > 0) {%>
						title='<%=smartHF.getPicFormatted(qpsf, sv.numapp)%>' <%}%>
						size='<%=sv.numapp.getLength()%>'
						maxLength='<%=sv.numapp.getLength()%>' onFocus='doFocus(this)'
						onHelp='return fieldHelp(numapp)'
						onKeyUp='return checkMaxLength(this)' style="width: 140px;"
						onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
						decimal='<%=qpsf.getDecimals()%>'
						onPaste='return doPasteNumber(event);'
						onBlur='return doBlurNumber(event);'
						<%if ((new Byte((sv.numapp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
							|| fw.getVariables().isScreenProtected()) {%>
						readonly="true" class="output_cell" style="width:140px;"
						<%} else if ((new Byte((sv.numapp).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
						class="bold_cell" style="width:140px;" <%} else {%>
						class=' <%=(sv.numapp).getColor() == null ? "input_cell"
								: (sv.numapp).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
						<%}%>>
					<%
						}
					%>
					<%
						longValue = null;
							formatValue = null;
					%>
					<!-- </div> -->
				</div>
			</div>
		</div>

		<%
			}
		%>
		<%
			if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.adjustageamt, (sv.adjustageamt.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
					%>
					<%
						if (sv.adjustageamt.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.adjustageamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.adjustageamt,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.rateadj, (sv.rateadj.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
					%>
					<%
						if (sv.rateadj.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.rateadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.rateadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.fltmort, (sv.fltmort.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
					%>
					<%
						if (sv.fltmort.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.fltmort,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.fltmort,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Load Amount"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.loadper) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.loadper, (sv.loadper.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
					%>
					<%
						if (sv.loadper.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.loadper,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.loadper,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>
		</div>
		<%
			}
		%>
		<div class="row">
			<%
				if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">

					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount"))%></label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.premadj) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.premadj, (sv.premadj.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
					%>
					<%
						if (sv.premadj.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.premadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.premadj,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>

					<!-- 	</div> -->
				</div>
			</div>
			<%
				}
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) 
 	{ %>
 	<!-- ILIFE-8323 Starts -->
<%	if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
	 <%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%>
	<%}

	if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0) {%>
	 <%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%>
	<%}
	}%><!-- ILIFE-8323 ends -->
					</label>
					<div class="input-group" style="min-width: 180px;">
						<!-- <div class="input-group"> -->
						<%
							if (((BaseScreenData) sv.zlinstprem) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zlinstprem, (sv.zlinstprem.getLength() + 1), null)
						.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zlinstprem) instanceof DecimalData) {
						%>
						<%
							if (sv.zlinstprem.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(0, 0, fw, sv.zlinstprem,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(0, 0, fw, sv.zlinstprem,
									COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
						<!-- </div> -->
					</div>
				</div>
			</div>

			<%
				if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
 	if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%> <%
 	}
 %>
					</label>
					<!-- <div class="input-group"> -->
					<%
						if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
					%>
					<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem, (sv.zbinstprem.getLength() + 1), null)
							.replace("absolute", "relative")%>
					<%
						} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
					%>
					<%
						if (sv.zbinstprem.equals(0)) {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\'  ")%>
					<%
						} else {
					%>
					<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zbinstprem,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
					<%
						}
					%>
					<%
						} else {
					%>
					<%
						}
					%>
					<!-- </div> -->
				</div>
			</div>
			<%
				}
			%>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Premium"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
						%>

						<input name='instPrem' type='text'
							<%if ((sv.instPrem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; min-width: 145px !important;" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.instPrem)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.instPrem);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.instPrem)%>' <%}%>
							size='<%=sv.instPrem.getLength()%>'
							maxLength='<%=sv.instPrem.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(instPrem)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.instPrem).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.instPrem).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.instPrem).getColor() == null ? "input_cell"
							: (sv.instPrem).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>

						<%
							}
						%>
						<!-- </div> -->
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax"))%></label>
					<div class="input-group">
						<!-- <div class="input-group"> -->
						<%
							if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='taxamt' type='text'
							<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; min-width: 145px !important;" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.taxamt)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.taxamt)%>' <%}%>
							size='<%=sv.taxamt.getLength()%>'
							maxLength='<%=sv.taxamt.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(taxamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
							: (sv.taxamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>

						<%
							}
						%>

						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<style>
.nav.nav-second-level > li > a{
	padding-top: 0px !important;
	padding-bottom: 0px !important;
    font-size: 15px !important;
    background-color: white !important;
    color: #000 !important;
    font-weight: 500 !important;
    font-family: SANS-SERIF !important;
}
</style>

<BODY >
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span>

							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='bankaccreq' id='bankaccreq' type='hidden'
									value="<%=sv.bankaccreq.getFormData()%>"> <!-- text -->
									<%
										if (!(sv.bankaccreq.getInvisible() == BaseScreenData.INVISIBLE
												|| sv.bankaccreq.getEnabled() == BaseScreenData.DISABLED)) {
									%>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bankaccreq"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Bank Account new")%>

										</a>
										<%
											}
										%>

										<!-- icon -->

										<%
											if (sv.bankaccreq.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.bankaccreq.getFormData().equals("X") || sv.bankaccreq.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.bankaccreq.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.bankaccreq.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>
										</a></li>

								<li><input name='optextind' id='optextind' type='hidden'
									value="<%=sv.optextind.getFormData()%>"> <!-- text -->
									<%
										if (!(sv.optextind.getInvisible() == BaseScreenData.INVISIBLE
												|| sv.optextind.getEnabled() == BaseScreenData.DISABLED)) {
									%>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>

										</a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.optextind.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.optextind.getFormData().equals("X") || sv.optextind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.optextind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.optextind.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>
										</a></li>

								<li><input name='taxind' id='taxind' type='hidden'
									value="<%=sv.taxind.getFormData()%>"> <!-- text --> <%
 	if (!(sv.taxind.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.taxind.getEnabled() == BaseScreenData.DISABLED)) {
 %>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>

										</a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.taxind.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.taxind.getFormData().equals("X") || sv.taxind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.taxind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.taxind.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>
										</a></li>
										
										<li><input name='aepaydet' id='aepaydet' type='hidden'
									value="<%=sv.aepaydet.getFormData()%>"> <!-- text --> <%
 	if (!(sv.aepaydet.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.aepaydet.getEnabled() == BaseScreenData.DISABLED)) {
 %>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aepaydet"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Antcptd Endwnt PayDetails")%>

										</a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.aepaydet.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.aepaydet.getFormData().equals("X") || sv.aepaydet.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.aepaydet.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.aepaydet.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>
										</a></li> 
												
										

								<li><input name='exclind' id='exclind' type='hidden'
									value="<%=sv.exclind.getFormData()%>"> <!-- text --> <%
 	if (!(sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
 			|| sv.exclind.getEnabled() == BaseScreenData.DISABLED)) {
 %>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>

										</a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.exclind.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.exclind.getFormData().equals("X") || sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.exclind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.exclind.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>
										</a></li>

								<li><input name='pbind' id='pbind' type='hidden'
									value="<%=sv.pbind.getFormData()%>"> <!-- text --> <%
 	if (sv.pbind.getInvisible() != BaseScreenData.INVISIBLE
 			|| sv.pbind.getEnabled() != BaseScreenData.DISABLED) {
 %>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>

										</a>
										<%
											} else if (sv.pbind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<a
											style="padding-top: 0px !important; padding-bottom: 0px !important; font-size: 15px !important; background-color: white !important; color: #000 !important; font-weight: 500 !important; font-family: SANS-SERIF !important;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.pbind.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.pbind.getFormData().equals("X") || sv.pbind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.pbind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.pbind.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>

										</a></li>

								<li><input name='ratypind' id='ratypind' type='hidden'
									value="<%=sv.ratypind.getFormData()%>"> <!-- text --> <%
 	if (sv.ratypind.getInvisible() != BaseScreenData.INVISIBLE
 			|| sv.ratypind.getEnabled() != BaseScreenData.DISABLED) {
 %>
									<div style="height: 15 px">
										<a href="javascript:;"
											onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("ratypind"))'
											class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%>

										</a>
										<%
											} else if (sv.ratypind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<a
											style="padding-top: 0px !important; padding-bottom: 0px !important; font-size: 15px !important; background-color: white !important; color: #000 !important; font-weight: 500 !important; font-family: SANS-SERIF !important;"><%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%></a>
										<%
											}
										%>

										<!-- icon -->
										<%
											if (sv.ratypind.getFormData().equals("+")) {
										%>
										<i class="fa fa-tasks fa-fw sidebar-icon"></i>
										<%
											} else {

												if (sv.ratypind.getFormData().equals("X") || sv.ratypind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.ratypind.getEnabled() == BaseScreenData.DISABLED) {
										%>
										<div></div>
										<%
											} else {
										%>
										<div style="width: 15px"></div>

										<%
											}
											}
											if (sv.ratypind.getFormData().equals("X")) {
										%>
										<i class="fa fa-warning fa-fw sidebar-icon"></i>
										<%
											}
										%>

										</a></li>
							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</BODY>

<%@ include file="/POLACommon2NEW.jsp"%>
