

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH534";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%Sh534ScreenVars sv = (Sh534ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Cash Withdrawal");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"I - Contract Enquiry          ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Effective Date ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.effdateDisp.setReverse(BaseScreenData.REVERSED);
			sv.effdateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.effdateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
         </div>
    	<div class="panel-body">     
			 <div class="row">	
			    	<div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
					    		<div class="input-group">
					    		<input name='chdrsel'  style="min-width: 100px;"
									type='text' id="chdrsel" 
									value='<%=sv.chdrsel.getFormData()%>' 
									maxLength='<%=sv.chdrsel.getLength()%>' 
									size='<%=sv.chdrsel.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'  
									
									<% 
										if((new Byte((sv.chdrsel).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
									%>  
									readonly="true"
									class="output_cell"	 >
									
									<%
										}else if((new Byte((sv.chdrsel).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										
									%>	
									class="bold_cell" >
									 
									<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
						<%
							}else { 
						%>
						
						class = ' <%=(sv.chdrsel).getColor()== null  ? 
						"input_cell" :  (sv.chdrsel).getColor().equals("red") ? 
						"input_cell red reverse" : "input_cell" %>' >
						
						<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
						<%} %>
					    		
					    		</div>
					    	</div>
					   </div>
					   
					   <div class="col-md-4"> </div>
					   
					   <div class="col-md-3"> 
    	 					<div class="form-group"> 	        				    			  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					    		<div class="input-group">
					    		 <div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="dobDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdateDisp, (sv.effdateDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					    		</div>
					    	</div>
					    </div>		
				</div>
		</div>
		</div>	
		
		<div class="panel panel-default">
    	
    	<div class="panel-heading">
        	<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
         </div>
    	
     <div class="panel-body">     
			<div class="row">	
			
			    <div class="col-md-4">
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Cash Withdrawal")%></b>
					</label>
				</div>
				
				<div class="col-md-2"></div>
				
				<div class="col-md-4">			
					<label class="radio-inline">
						<b><%=smartHF.buildRadioOption(sv.action, "action", "I")%><%=resourceBundleHandler.gettingValueFromBundle("Contract Enquiry")%></b>
					</label>			
			    </div>	
			    <div class="col-md-2"></div>	
			   </div> 
			 </div>  		    		
</div>

<%@ include file="/POLACommon2NEW.jsp"%>

