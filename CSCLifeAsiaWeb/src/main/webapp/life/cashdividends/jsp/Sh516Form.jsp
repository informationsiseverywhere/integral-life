

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH516";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>

<%Sh516ScreenVars sv = (Sh516ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank/Branch ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                      ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"             ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"             ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Account ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"            ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"             ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"  ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.bankkey.setReverse(BaseScreenData.REVERSED);
			sv.bankkey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.bankkey.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.bankacckey.setReverse(BaseScreenData.REVERSED);
			sv.bankacckey.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.bankacckey.setHighLight(BaseScreenData.BOLD);
		}	
		if (appVars.ind02.isOn()) {
			sv.bankkey.setEnabled(BaseScreenData.DISABLED);	
			sv.bankacckey.setEnabled(BaseScreenData.DISABLED);	
		}
	}

	%>

<div class="panel panel-default">
        <div class="panel-body">
        	<div class="row">
        		<div class="col-md-3">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank/Branch"))%></label>
        			
        			<div class="input-group three-controller">
        			
<%if ((new Byte((sv.bankkey).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.bankkey.getFormData();  
%>

<% 
	if((new Byte((sv.bankkey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
<% }else {%> 

<td valign="top" style="padding-left: 4px" >
<input style="padding-top: 4px"  
name='bankkey' id='bankkey'
type='text' 
value='<%=sv.bankkey.getFormData()%>' 
maxLength='<%=sv.bankkey.getLength()%>' 
size='<%=sv.bankkey.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.bankkey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 ></input></td>

<%
	}else if((new Byte((sv.bankkey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>

<%
	}else { 
%>

class = ' <%=(sv.bankkey).getColor()== null  ? 
"input_cell" :  (sv.bankkey).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
<%}longValue = null;}} %>

<%if ((new Byte((sv.bankdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
 
<%if ((new Byte((sv.branchdesc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<td valign="top" style='padding-top: 0px;'>	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	</td>
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
                
                      </div>
        			</div>
        		</div>
        	</div>	
        	
        	<div class="row">
        		<div class="col-md-4">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account"))%></label>
        			<div class="input-group">	
        			<%if ((new Byte((sv.bankacckey).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<%	
	
	longValue = sv.bankacckey.getFormData();  
%>

<% 
	if((new Byte((sv.bankacckey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
<td valign="top"  ><div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div></td>

<%
longValue = null;
%>
<% }else {%> 

<td valign="top" style="padding-left: 4px" >
<input style="padding-top: 4px"  
name='bankacckey' id='bankacckey'
type='text' 
value='<%=sv.bankacckey.getFormData()%>' 
maxLength='<%=sv.bankacckey.getLength()%>' 
size='<%=sv.bankacckey.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(bankkey)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.bankkey).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
readonly="true"
class="output_cell"	 ></input></td>

<%
	}else if((new Byte((sv.bankacckey).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>

<%
	}else { 
%>

class = ' <%=(sv.bankacckey).getColor()== null  ? 
"input_cell" :  (sv.bankacckey).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >



<span class="input-group-btn">
                           
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
<%}longValue = null;}} %>

<%if ((new Byte((sv.bankaccdsc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<td valign="top" style='padding-top: 0px;'>	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	</td>
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
  
        			</div>
        			</div>
        		</div>
        	</div>				
</div>
</div>


<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>

<%@ include file="/POLACommon2NEW.jsp"%>

