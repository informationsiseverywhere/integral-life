<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH545";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%Sh545ScreenVars sv = (Sh545ScreenVars) fw.getVariables();%>
<%{
}%>

<div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>
	                       <table>
	                       <tr>
	                       <td>
	                           <%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				</td>
				<td>
				<%					
				if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
			</td>
			<td>
				<%					
				if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
							
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
									
									
							} else  {
										
							if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}
							
							}
							%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'  style="width: 175px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
				<%
				longValue = null;
				formatValue = null;
				%>
			</td>
		</tr>
	                         </table>
	                        </div>
	                      </div>
	                      
	                   
	                      <div class="col-md-4">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
	                   
	                    <%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                  
	                   </div>
	                    
             
		 <div class="col-md-4">
		 <div class="form-group">
		  <label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
		  <%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:70px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
		  </div>
		  </div>
		  
		 </div>
		 
		  <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage No")%></label>
	                       <%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:70px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                     </div>
	                    </div>
	                    
	                    
	                  
	                     
	                    <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
	                       <%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:70px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                     </div>
	                    </div>
	                    
	                  
	                    
	                    <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Code")%></label>
	                       <%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:70px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                     </div>
	                    </div>
	                       
		 </div>
		 
		 <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group" style="width: 100px;">
	                       <%-- <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%></label> --%>
	                       <!-- ILJ-49 Starts -->
               		<% if (sv.iljCntDteFlag.compareTo("Y") != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
        			<%} %>
					<!-- ILJ-49 Ends --> 
	                       <%=smartHF.getRichText(0, 0, fw, sv.occdateDisp,(sv.occdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
							<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.occdateDisp).replace("absolute","relative")%>
	                      </div>
	                     </div>
	                   
	                     <div class="col-md-4">
	                    <div class="form-group">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
	                        <table>
	                        <tr>
	                        <td>
	                       <%if ((new Byte((sv.rstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.pstate).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>	
</td>				
</tr>
	                     </table>
	                      </div>
	                     </div>
	                     
	                      
	                     <div class="col-md-4">
	                    <div class="form-group" style="width: 100px;">
	                       <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
	                       <%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute","relative")%>
	                      </div>
	                     </div>
	                         </div>

                       <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group" style="width: 100px;">
	                       <label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Bill To Date")%></label>
	                       <%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null).replace("absolute","relative").replace("width","float:left; width")%>
<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute","relative")%>
	                     </div>
	                   </div>    
                       
                     
                       
                      <div class="col-md-4">
	                    <div class="form-group">
	                     <label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
	                     <table>
	                     <tr>
	                     <td>
	                     <%if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
<td>
<%if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width: 200px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
</td>
</tr>
</table>
	                    </div>
	                    </div>

                    
                      
                      <div class="col-md-4">
	                    <div class="form-group">
	                     <label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
	                      <div class="input-group">
	                       <%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:170px">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
	                       
	                      </div>
	                     </div>
	                   </div>   
                   </div>

                 <div class="row">
                      <div class="col-md-4">
	                    <div class="form-group" style="width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Total Component SA")%></label>
	                       <%	
			/* qpsf = fw.getFieldXMLDef((sv.mpsi).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER); */
			qpsf = fw.getFieldXMLDef((sv.mpsi).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.mpsi,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

	%>

<input name='mpsi' 
type='text'
<%if((sv.mpsi).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
	value='<%=smartHF.getPicFormatted(qpsf,sv.mpsi) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.mpsi);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.mpsi) %>'
	 <%}%>

<%-- size='<%= sv.mpsi.getLength()%>' --%>
size='<%=COBOLHTMLFormatter.getLengthWithCommas(sv.mpsi.getLength(), sv.mpsi.getScale(),3)%>'
maxLength='<%= sv.mpsi.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(mpsi)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.mpsi).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mpsi).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mpsi).getColor()== null  ? 
			"input_cell" :  (sv.mpsi).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
	                     </div>
	                   </div>
	                   
	                  
	                   <div class="col-md-4"> 
	                    <div class="form-group" style="width: 200px;">
	                       <label><%=resourceBundleHandler.gettingValueFromBundle("Basic SA")%></label>
	                       <%	
			qpsf = fw.getFieldXMLDef((sv.sumins).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input style="text-align: right;" name='sumins' 
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumins.getLength(), sv.sumins.getScale(),3)%>'
maxLength='<%= sv.sumins.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumins)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.sumins).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumins).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.sumins).getColor()== null  ? 
			"input_cell" :  (sv.sumins).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'

<%
	} 
%>
>
	                       
	                     </div>
	                   </div>
	                </div>     
	                
	                 <div class="row">		
            <div class="col-md-12">
               <div class="table-responsive">
    	 	      <table class="table table-striped table-bordered table-hover" id='dataTables-sh545'>	
    	 	      <thead>
    	 	      <tr class='info'>	
                  <th><center><%=resourceBundleHandler.gettingValueFromBundle("No")%></center></th>
	              <th><center><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></center></th>
	               <th><center><%=resourceBundleHandler.gettingValueFromBundle("Cess Date")%></center></th>
	                <th><center><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></center></th>
	                 <th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></center></th>
	                  <th><center><%=resourceBundleHandler.gettingValueFromBundle("RiskSt")%></center></th>
	                   <th><center><%=resourceBundleHandler.gettingValueFromBundle("PrmSt")%></center></th>
	                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("IssAge")%></center></th>
	                      <th><center><%=resourceBundleHandler.gettingValueFromBundle("Par")%></center></th>
	                     </tr>
	                     
	                     <% 
 int[] tblColumnWidth = new int[22];
int totalTblWidth = 0;
int calculatedValue = 0;
int arraySize=0;

calculatedValue=60;
totalTblWidth += calculatedValue;
tblColumnWidth[0] = calculatedValue;

calculatedValue=156;
totalTblWidth += calculatedValue;
tblColumnWidth[1] = calculatedValue;

calculatedValue=156;
totalTblWidth += calculatedValue;
tblColumnWidth[2] = calculatedValue;

calculatedValue=204;
totalTblWidth += calculatedValue;
tblColumnWidth[3] = calculatedValue;

calculatedValue=240;
totalTblWidth += calculatedValue;
tblColumnWidth[4] = calculatedValue;

calculatedValue=72;
totalTblWidth += calculatedValue;
tblColumnWidth[5] = calculatedValue;

calculatedValue=60;
totalTblWidth += calculatedValue;
tblColumnWidth[6] = calculatedValue;

calculatedValue=72;
totalTblWidth += calculatedValue;
tblColumnWidth[7] = calculatedValue;

calculatedValue=36;
totalTblWidth += calculatedValue;
tblColumnWidth[8] = calculatedValue;

if(totalTblWidth>730){
		totalTblWidth=730;
}
arraySize=tblColumnWidth.length;
GeneralTable sfl = fw.getTable("sh545screensfl");
GeneralTable sfl1 = fw.getTable("sh545screensfl");
Sh545screensfl.set1stScreenRow(sfl, appVars, sv);
int height;
if(sfl.count()*27 > 210) {
height = 210 ;
} else if(sfl.count()*27 > 118) {
height = sfl.count()*27;
} else {
height = 118;
}	
%>

 <tbody>
<%
Sh545screensfl.set1stScreenRow(sfl, appVars, sv);
int count = 1;
boolean hyperLinkFlag;
while (Sh545screensfl.hasMoreScreenRows(sfl)) {	
hyperLinkFlag=true;
%>

<tr id='tr<%=count%>' height="30">

 <input type='hidden' maxLength='<%=sv.screenIndicArea.getLength()%>'
 value='<%= sv.screenIndicArea.getFormData() %>' 
 size='<%=sv.screenIndicArea.getLength()%>'
 onFocus='doFocus(this)' onHelp='return fieldHelp(sh545screensfl.screenIndicArea)' onKeyUp='return checkMaxLength(this)' 
 name='<%="sh545screensfl" + "." + "screenIndicArea" + "_R" + count %>'
 id='<%="sh545screensfl" + "." + "screenIndicArea" + "_R" + count %>'		  >
 
 <td style="width:<%=tblColumnWidth[0]%>px;" 
	<%if(!(((BaseScreenData)sv.puAddNbr) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
	<%if((new Byte((sv.puAddNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0||(new Byte((sv.puAddNbr).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0){hyperLinkFlag=false;}%>
			<%if((new Byte((sv.puAddNbr).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
															
					<%if(hyperLinkFlag){%>
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sh545screensfl" + "." +
					      "slt" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.puAddNbr.getFormData()%></span></a>							 						 		
						  <%}else{%>
							<a href="javascript:;" class = 'tableLink'><span><%=sv.puAddNbr.getFormData()%></span></a>							 						 		
						 <%}%>
														 
				
									<%}%>
			 			</td>
			 			
			 			 <td
            style="color:#434343; padding: 5px;width:120px;font-weight: bold;border-right: 1px solid #dddddd;"
            align="left">
                                             
            <input style="padding-top: 4px" type='text'
            maxLength='<%=sv.riskCessDateDisp.getLength()+50%>'
            value='<%=sv.riskCessDateDisp.getFormData()%>'
            size='<%=sv.riskCessDateDisp.getLength()-40%>' onFocus='doFocus(this)'
            onHelp='return fieldHelp(sh545screensfl.riskCessDateDisp)'
            onKeyUp='return checkMaxLength(this)'
            name='<%="sh545creensfl" + "." + "riskCessDateDisp" + "_R" + count%>'
            id='<%="sh545screensfl" + "." + "riskCessDateDisp" + "_R" + count%>'
            readonly="true" class="output_cell"
            style="width: 145 px;"
            >
            
      </td>
      
      <td
      style="color:#434343; padding: 5px;width:120px;font-weight: bold;border-right: 1px solid #dddddd;"
      align="left">
                                       
      <input style="padding-top: 4px" type='text'
      maxLength='<%=sv.riskCessDateDisp.getLength()-50%>'
      value='<%=sv.riskCessDateDisp.getFormData()%>'
      size='<%=sv.riskCessDateDisp.getLength()-50%>' onFocus='doFocus(this)'
      onHelp='return fieldHelp(sh545screensfl.riskCessDateDisp)'
      onKeyUp='return checkMaxLength(this)'
      name='<%="sh545creensfl" + "." + "riskCessDateDisp" + "_R" + count%>'
      id='<%="sh545screensfl" + "." + "riskCessDateDisp" + "_R" + count%>'
      readonly="true" class="output_cell"
      style="width: 145 px;"
      >
      
      </td>
      
      <td>

<%=smartHF.getHTMLVarExt(fw, sv.sumin, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 10, 170)%> 
</td>

<td>

<%=smartHF.getHTMLVarExt(fw, sv.singp, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS, 30, 150)%> 
</td>

<td style="width:<%=tblColumnWidth[5]%>px;" 
	<%if(!(((BaseScreenData)sv.rstatcode) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.rstatcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.rstatcode.getFormData();
							%>
					 		<div id="sh545screensfl.rstatcode_R<%=count%>" name="sh545screensfl.rstatcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

<td style="width:<%=tblColumnWidth[6]%>px;" 
	<%if(!(((BaseScreenData)sv.pstatcode) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.pstatcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.pstatcode.getFormData();
							%>
					 		<div id="sh545screensfl.pstatcode_R<%=count%>" name="sh545screensfl.pstatcode_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

<td style="width:<%=tblColumnWidth[7]%>px;" 
	<%if(!(((BaseScreenData)sv.anbAtCcd) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.anbAtCcd).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.anbAtCcd.getFormData();
							%>
					 		<div id="sh545screensfl.anbAtCcd_R<%=count%>" name="sh545screensfl.anbAtCcd_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

<td style="width:<%=tblColumnWidth[8]%>px;" 
	<%if(!(((BaseScreenData)sv.divdParticipant) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >									
				<%if((new Byte((sv.divdParticipant).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
							<%
								longValue=sv.divdParticipant.getFormData();
							%>
					 		<div id="sh545screensfl.divdParticipant_R<%=count%>" name="sh545screensfl.divdParticipant_R<%=count%>">
									<%=longValue%>
							</div>
							<%
								longValue = null;
							%>
							
						<%}%>
</td>

</tr>
<%	count = count + 1;
Sh545screensfl.setNextScreenRow(sfl, appVars, sv);
}
%>
</table>
<%-- <div id="load-more" class="col-md-offset-10">
						    <a class="btn btn-info" href="#" style='width: 74px;'
						       onclick="doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("More")%>
						    </a>
						</div> --%>
</div>
</div>

           </div>
</div>



</div>



<script>
$(document).ready(function() {
	$('#dataTables-sh545').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
  	});
})
</script>

		
<!-- <script>
	$(document).ready(function() {
    	$('#dataTables-sh545').DataTable({
        	ordering: false,
        	searching:false
      	});
    });
</script>	 --> 			


