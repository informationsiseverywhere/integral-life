<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SH533";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*"%>
<%
	Sh533ScreenVars sv = (Sh533ScreenVars) fw.getVariables();
%>
<%
	{
		if (appVars.ind12.isOn()) {
			sv.currcd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.otheradjst.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.reasoncd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind12.isOn()) {
			sv.resndesc.setEnabled(BaseScreenData.DISABLED);
		}

		if (appVars.ind15.isOn()) {
			sv.actvalue.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.estMatValue.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind47.isOn()) {
			sv.estMatValue.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		
		if (appVars.ind03.isOn()) {
			sv.payeeno.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind02.isOn()) {
            sv.payeeno.setReverse(BaseScreenData.REVERSED);
            sv.payeeno.setColor(BaseScreenData.RED);
     	}
	    if (!appVars.ind02.isOn()) {
	        sv.payeeno.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind09.isOn()) {
			sv.paymthbf.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind14.isOn()) {
            sv.paymthbf.setReverse(BaseScreenData.REVERSED);
            sv.paymthbf.setColor(BaseScreenData.RED);
     	}
	    if (!appVars.ind14.isOn()) {
	        sv.paymthbf.setHighLight(BaseScreenData.BOLD);
	    }
		
		if (appVars.ind21.isOn()) {
			sv.bankactkey.setEnabled(BaseScreenData.DISABLED);
		}
		 if (appVars.ind20.isOn()) {
			sv.bankactkey.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 if (appVars.ind19.isOn()) {
	            sv.bankactkey.setReverse(BaseScreenData.REVERSED);
	            sv.bankactkey.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind19.isOn()) {
	            sv.bankactkey.setHighLight(BaseScreenData.BOLD);
	    }
		if (appVars.ind23.isOn()) {
			sv.crdtcrd.setInvisibility(BaseScreenData.DISABLED);
		}
		if (appVars.ind24.isOn()) {
			sv.crdtcrd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind22.isOn()) {
	            sv.crdtcrd.setReverse(BaseScreenData.REVERSED);
	            sv.crdtcrd.setColor(BaseScreenData.RED);
	     }
	    if (!appVars.ind22.isOn()) {
	            sv.crdtcrd.setHighLight(BaseScreenData.BOLD);
	    }
	    if (appVars.ind18.isOn()) {
			sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
		}	

	}
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract no ")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 150px; margin-left: 1px;" id="ctypedes">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
				<table>
					<tr>
						<td>
							<%
								if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.rstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
						<td>
							<%
								if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.pstate.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
								style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
					</tr>
				</table>
			</div>



			<div class="col-md-4">
				<div class="form-group" style="width: 100px;">
				<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label> 
						<%} %>
                   <!-- ILJ-49 ends -->	
					<%
						if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.cownnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cownnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.cownnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.ownername).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ownername.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.ownername.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="width: 215px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.lifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.lifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.linsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.linsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px; max-width: 200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
							<td>
								<%
									if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlinsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		} else {
 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.jlinsname.getFormData()).toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}
 		}
 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 200px; margin-left: 1px; min-width: 71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 100px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
					<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp, (sv.ptdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
					<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.ptdateDisp).replace("absolute", "relative")%></td>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group" style="width: 100px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
					<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp, (sv.btdateDisp.getLength()), null)
					.replace("absolute", "relative").replace("width", "float:left; width")%>
					<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.btdateDisp).replace("absolute", "relative")%></td>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Rider Details")%></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sh533' width="100%">
						<thead>
							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Lif")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Cov")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Current Amount")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Withdrawal Amount")%></center></th>

							</tr>
							<%
								int[] tblColumnWidth = new int[22];
								int totalTblWidth = 0;
								int calculatedValue = 0;
								int arraySize = 0;

								calculatedValue = 35;
								totalTblWidth += calculatedValue;
								tblColumnWidth[0] = calculatedValue;

								calculatedValue = 40;
								totalTblWidth += calculatedValue;
								tblColumnWidth[1] = calculatedValue;

								calculatedValue = 74;
								totalTblWidth += calculatedValue;
								tblColumnWidth[2] = calculatedValue;

								calculatedValue = 40;
								totalTblWidth += calculatedValue;
								tblColumnWidth[3] = calculatedValue;

								calculatedValue = 126;
								totalTblWidth += calculatedValue;
								tblColumnWidth[4] = calculatedValue;

								calculatedValue = 65;
								totalTblWidth += calculatedValue;
								tblColumnWidth[5] = calculatedValue;

								calculatedValue = 170;
								totalTblWidth += calculatedValue;
								tblColumnWidth[6] = calculatedValue;

								calculatedValue = 170;
								totalTblWidth += calculatedValue;
								tblColumnWidth[7] = calculatedValue;

								if (totalTblWidth > 730) {
									totalTblWidth = 730;
								}
								arraySize = tblColumnWidth.length;
								GeneralTable sfl = fw.getTable("sh533screensfl");
								GeneralTable sfl1 = fw.getTable("sh533screensfl");
								Sh533screensfl.set1stScreenRow(sfl, appVars, sv);
								int height;
								if (sfl.count() * 27 > 210) {
									height = 310;
								} else if (sfl.count() * 27 > 118) {
									height = sfl.count() * 27;
								} else {
									height = 310;
								}
							%>
						
						<tbody>
							<%
								Sh533screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								boolean hyperLinkFlag;
								while (Sh533screensfl.hasMoreScreenRows(sfl)) {
									hyperLinkFlag = true;
							%>

							<tr id='tr<%=count%>' height="30">

								<input type='hidden' maxLength='<%=sv.hcover.getLength()%>'
									value='<%=sv.hcover.getFormData()%>'
									size='<%=sv.hcover.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hcover)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hcover" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hcover" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.hcrtable.getLength()%>'
									value='<%=sv.hcrtable.getFormData()%>'
									size='<%=sv.hcrtable.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hcrtable)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hcrtable" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hcrtable" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.hemv.getLength()%>'
									value='<%=sv.hemv.getFormData()%>'
									size='<%=sv.hemv.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hemv)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hemv" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hemv" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.hjlife.getLength()%>'
									value='<%=sv.hjlife.getFormData()%>'
									size='<%=sv.hjlife.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hjlife)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hjlife" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hjlife" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.zcshdivmth.getLength()%>'
									value='<%=sv.zcshdivmth.getFormData()%>'
									size='<%=sv.zcshdivmth.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.zcshdivmth)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "zcshdivmth" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "zcshdivmth" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.hrider.getLength()%>'
									value='<%=sv.hrider.getFormData()%>'
									size='<%=sv.hrider.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hrider)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hrider" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hrider" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.zdivopt.getLength()%>'
									value='<%=sv.zdivopt.getFormData()%>'
									size='<%=sv.zdivopt.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.zdivopt)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "zdivopt" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "zdivopt" + "_R" + count%>'>


								<input type='hidden'
									maxLength='<%=sv.screenIndicArea.getLength()%>'
									value='<%=sv.screenIndicArea.getFormData()%>'
									size='<%=sv.screenIndicArea.getLength()%>'
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.screenIndicArea)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "screenIndicArea" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "screenIndicArea" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.zwdv.getLength()%>'
									value='<%=sv.zwdv.getFormData()%>'
									size='<%=sv.zwdv.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.zwdv)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "zwdv" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "zwdv" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.nextCapDate.getLength()%>'
									value='<%=sv.nextCapDate.getFormData()%>'
									size='<%=sv.nextCapDate.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.nextCapDate)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "nextCapDate" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "nextCapDate" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.hcnstcur.getLength()%>'
									value='<%=sv.hcnstcur.getFormData()%>'
									size='<%=sv.hcnstcur.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.hcnstcur)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "hcnstcur" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "hcnstcur" + "_R" + count%>'>


								<input type='hidden' maxLength='<%=sv.planSuffix.getLength()%>'
									value='<%=sv.planSuffix.getFormData()%>'
									size='<%=sv.planSuffix.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.planSuffix)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "planSuffix" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "planSuffix" + "_R" + count%>'>

								<td style="width:<%=tblColumnWidth[0]%>px;"
									<%if (!(((BaseScreenData) sv.life) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0
													|| (new Byte((sv.life).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
												hyperLinkFlag = false;
											}
									%> <%
 	if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (hyperLinkFlag) {
 %> <a href="javascript:;" class='tableLink'
									onClick='document.getElementById("<%="sh533screensfl" + "." + "slt" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.life.getFormData()%></span></a>
									<%
										} else {
									%> <a href="javascript:;" class='tableLink'><span><%=sv.life.getFormData()%></span></a>
									<%
										}
									%> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[1]%>px;"
									<%if (!(((BaseScreenData) sv.coverage) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	longValue = sv.coverage.getFormData();
 %>
									<div id="sh533screensfl.coverage_R<%=count%>"
										name="sh533screensfl.coverage_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[2]%>px;"
									<%if (!(((BaseScreenData) sv.rider) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	longValue = sv.rider.getFormData();
 %>
									<div id="sh533screensfl.rider_R<%=count%>"
										name="sh533screensfl.rider_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[3]%>px;"
									<%if (!(((BaseScreenData) sv.fieldType) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.fieldType).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	longValue = sv.fieldType.getFormData();
 %>
									<div id="sh533screensfl.fieldType_R<%=count%>"
										name="sh533screensfl.fieldType_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[4]%>px;"
									<%if (!(((BaseScreenData) sv.shortds) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.shortds).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	longValue = sv.shortds.getFormData();
 %>
									<div id="sh533screensfl.shortds_R<%=count%>"
										name="sh533screensfl.shortds_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>
								<td style="width:<%=tblColumnWidth[5]%>px;"
									<%if (!(((BaseScreenData) sv.cnstcur) instanceof StringBase)) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										if ((new Byte((sv.cnstcur).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	longValue = sv.cnstcur.getFormData();
 %>
									<div id="sh533screensfl.cnstcur_R<%=count%>"
										name="sh533screensfl.cnstcur_R<%=count%>">
										<%=longValue%>
									</div> <%
 	longValue = null;
 %> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[6]%>px;"
									<%if (!(((BaseScreenData) sv.estMatValue) instanceof StringBase)) {%>
									align="center" <%} else {%> align="right" <%}%>>
									<%
										if ((new Byte((sv.estMatValue).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	sm = sfl.getCurrentScreenRow();
 			qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
 			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
 			formatValue = smartHF.getPicFormatted(qpsf, sv.estMatValue,
 					COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 %> <%
 	if ((new Byte((sv.estMatValue).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected()) || sv.configflag.compareTo("N") != 0) {
 %>
									<div id="sh533screensfl.estMatValue_R<%=count%>"  style="min-width: 150px"
										name="sh533screensfl.estMatValue_R<%=count%>"
										class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
										<%
											if (formatValue != null) {
										%>

										<%=XSSFilter.escapeHtml(formatValue)%>


										<%
											}
										%>
									</div> <%
 	} else {
 %> <input type='text' value='<%=formatValue%>'
									<%if (qpsf.getDecimals() > 0) {%>
									size='<%=sv.estMatValue.getLength() + 1%>'
									maxLength='<%=sv.estMatValue.getLength() + 1%>' <%} else {%>
									size='<%=sv.estMatValue.getLength()%>'
									maxLength='<%=sv.estMatValue.getLength()%>' <%}%>
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.estMatValue)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "estMatValue" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "estMatValue" + "_R" + count%>'
									class=" <%=(sv.estMatValue).getColor() == null
								? "input_cell"
								: (sv.estMatValue).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
									style="width: <%=sv.estMatValue.getLength() * 9%>px; text-align='right';"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									title='<%=formatValue%>'> <%
 	}
 %> <%
 	}
 %>
								</td>

								<td style="width:<%=tblColumnWidth[7]%>px;"
									<%if (!(((BaseScreenData) sv.actvalue) instanceof StringBase)) {%>
									align="center" <%} else {%> align="right" <%}%>>
									<%
										if ((new Byte((sv.actvalue).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%> <%
 	sm = sfl.getCurrentScreenRow();
 			qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
 			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
 			formatValue = smartHF.getPicFormatted(qpsf, sv.actvalue,
 					COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 %> <%
 	if ((new Byte((sv.actvalue).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
 					|| (((ScreenModel) fw).getVariables().isScreenProtected()) || sv.configflag.compareTo("N") != 0) {
 %>
									<div id="sh533screensfl.actvalue_R<%=count%>" style="min-width: 150px;"
										name="sh533screensfl.actvalue_R<%=count%>"
										class='<%=((formatValue == null) || ("".equals(formatValue.trim())))
								? "blank_cell"
								: "output_cell"%>'>
										<%
											if (formatValue != null) {
										%>

										<%=XSSFilter.escapeHtml(formatValue)%>


										<%
											}
										%>
									</div> <%
 	} else {
 %> <input type='text' value='<%=formatValue%>'
									<%if (qpsf.getDecimals() > 0) {%>
									size='<%=sv.actvalue.getLength() + 1%>'
									maxLength='<%=sv.actvalue.getLength() + 1%>' <%} else {%>
									size='<%=sv.actvalue.getLength()%>'
									maxLength='<%=sv.actvalue.getLength()%>' <%}%>
									onFocus='doFocus(this)'
									onHelp='return fieldHelp(sh533screensfl.actvalue)'
									onKeyUp='return checkMaxLength(this)'
									name='<%="sh533screensfl" + "." + "actvalue" + "_R" + count%>'
									id='<%="sh533screensfl" + "." + "actvalue" + "_R" + count%>'
									class=" <%=(sv.actvalue).getColor() == null
								? "input_cell"
								: (sv.actvalue).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>"
									style="width: <%=sv.actvalue.getLength() * 9%>px; text-align='right';"
									onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
									decimal='<%=qpsf.getDecimals()%>'
									onPaste='return doPasteNumber(event,true);'
									onBlur='return doBlurNumberNew(event,true);'
									title='<%=formatValue%>'> <%
 	}
 %> <%
 	}
 %>
								</td>

							</tr>

							<%
								count = count + 1;
									Sh533screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						
					</table>

				</div>
			</div>

		</div>
	 	<%
		if (sv.configflag.compareTo("N") != 0) {		
		%>
		<div class="row">
			<div class="col-md-4">
				<div>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
					<table>
						<tr>
							<td>
								<%
								if((new Byte((sv.payeeno).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>

								<div class="input-group" style="width: 72px;">
									<%=smartHF.getHTMLVarExt(fw, sv.payeeno)%>
								</div> <%
								 	} else {
								 %>
								<div class="input-group" style="width: 120px;">
								<div class="form-group">
									<%=smartHF.getRichTextInputFieldLookup(fw, sv.payeeno)%></div>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('payeeno')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								
								</div> 
								<%
								 	}
								 %>

							</td>
							<td>
								<%
									if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
								 	if (!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								
								 			if (longValue == null || longValue.equalsIgnoreCase("")) {
								 				formatValue = formatValue((sv.payeenme.getFormData()).toString());
								 			} else {
								 				formatValue = formatValue(longValue);
								 			}
								
								 		} else {
								
								 			if (longValue == null || longValue.equalsIgnoreCase("")) {
								 				formatValue = formatValue((sv.payeenme.getFormData()).toString());
								 			} else {
								 				formatValue = formatValue(longValue);
								 			}
								
								 		}
								 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 100px; margin-left: 1px; max-width: 167px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								 	longValue = null;
								 		formatValue = null;
								 %> <%
								 	}
								 %>

							</td>


						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Method of Payout")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[]{"paymthbf"}, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("paymthbf");
						optionValue = makeDropDownList(mappedItems, sv.paymthbf.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.paymthbf.getFormData()).toString().trim());

						if (longValue == null) {
							longValue = "&nbsp;&nbsp;";
						} else {
							longValue = formatValue(longValue);
						}
					%>

					<%
						if ((new Byte((sv.paymthbf).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<div class='output_cell' id="paymthbf">
						<%=longValue%>
					</div>

					<%
						longValue = null;
					%>
					<%
						} else {
					%>
					<%=smartHF.getDropDownExt(sv.paymthbf, fw, longValue, "paymthbf", optionValue, 0)%>
					<%
						}
					%>
				</div>
			</div>

		</div>
		
		
		<div class="row">
			<%-- <%	 if (appVars.ind20.isOn() && appVars.ind24.isOn()) { %>	 --%>	
			 <div class="col-md-4" id="bankactkeydiv3">
                <div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account No/CC No"))%></label>

		
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 100px;">
								
						</div>												
			
					</div>
			</div>
					
			<%-- <%
                }
             %> --%>
					
		
		<%--  <%	 if (!(appVars.ind20.isOn())) { %>   --%> 
			<div class="col-md-4" id="bankactkeydiv2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Ac No/CC No")%></label>
										
						<%
								if((new Byte((sv.bankactkey).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>

								<div class="input-group" style="min-width: 200px;">
									<%=smartHF.getHTMLVarExt(fw, sv.bankactkey)%>
								</div> <%
								 	} else {
								 %>
								<div class="input-group" style="width: 225px;">
								<% if("red".equals((sv.bankactkey).getColor())){%>
								<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;height: 30px;">
								<%}%>
								<input name='bankactkey' id='bankactkey'
									type='text'
									value='<%=sv.bankactkey.getFormData()%>'
									maxLength='<%=sv.bankactkey.getLength()%>'
									size='<%=sv.bankactkey.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(bankactkey)' 
									onKeyUp='return checkMaxLength(this)'>
									<% if("red".equals((sv.bankactkey).getColor())){%>
									</div><%}%>
								
									<%-- <%=smartHF.getRichTextInputFieldLookup(fw, sv.bankactkey)%> --%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('bankactkey')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> 	
								
							<%
								}
							%>
						
				</div>
			</div>
			<%-- <%} %> --%>
			<%--  <%	 if (!(appVars.ind24.isOn())) { %> --%>
			 	<div class="col-md-4" id="bankactkeydiv1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Ac No/CC No")%></label>
										
						<%
								if((new Byte((sv.crdtcrd).getEnabled()))
									.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
								%>

								<div class="input-group" style="min-width: 200px;">
									<%=smartHF.getHTMLVarExt(fw, sv.crdtcrd)%>
								</div> <%
								 	} else {
								 %>
								<div class="input-group" style="width: 225px;"  >
								<% if("red".equals((sv.crdtcrd).getColor())){%>
								<div style="border-style: solid;  border: 2px; border-style: solid;border-color: #ec7572;height: 30px;">
								<%}%>
								<input name='crdtcrd' id='crdtcrd'
									type='text'
									value='<%=sv.crdtcrd.getFormData()%>'
									maxLength='<%=sv.crdtcrd.getLength()%>'
									size='<%=sv.crdtcrd.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(crdtcrd)' 
									onKeyUp='return checkMaxLength(this)'>
									<% if("red".equals((sv.crdtcrd).getColor())){%>
									</div><%}%>
									<%-- <%=smartHF.getRichTextInputFieldLookup(fw, sv.crdtcrd)%> --%>
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onClick="doFocus(document.getElementById('crdtcrd')); doAction('PFKEY04')">
											<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
										</button>
									</span>
								</div> 		
								
								
							<%
								}
							%>
						
				</div>
			</div>
			<%--  <%} %>    --%>
			 
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></label>
						<%=smartHF.getHTMLVarExt(fw, sv.bankkey)%>	
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Name")%></label>
						<%=smartHF.getHTMLVarExt(fw, sv.bnkcdedsc)%>
					</div>
				</div>
		
		
		</div>
		<%} %>
	

	<script>
$(document).ready(function() {
	$('#dataTables-sh533').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
  	});
	
	if (screen.height == 900) {
		
		$('#ctypdesc').css('max-width','215px');
	}
	if (screen.width == 1440) {
		
		$('#ctypdesc').css('max-width','200px');
	}
})
</script>



<script>
$(function () {	
	$('#paymthbf').change(function() {			
	  if ( this.value === 'C'){	
		 // alert("C")
		$("#bankactkeydiv1").show();	
		$("#bankactkeydiv2").hide();	 
		$("#bankactkeydiv3").hide();
		
		$("input[name*='bankactkey']").val(' ');
	  }else if(this.value === '4'){	
		 // alert("4=")
		$("#bankactkeydiv2").show();
		$("#bankactkeydiv3").hide();
		$("#bankactkeydiv1").hide();
		
		 $("input[name*='crdtcrd']").val(' ');
		
	  }else{
		//  alert("Other")
		$("#bankactkeydiv3").show();
		$("#bankactkeydiv1").hide();
		$("#bankactkeydiv2").hide();
		
		 $("input[name*='bankactkey']").val(' ');
		 $("input[name*='crdtcrd']").val(' ');
	  }
	});
});

$(document).ready(function(){		
	if(document.getElementById("paymthbf").value === 'C'){	
		//alert("c")
		$("#bankactkeydiv1").show();	
		$("#bankactkeydiv2").hide();	 
		$("#bankactkeydiv3").hide();	 
	
	}else if(document.getElementById("paymthbf").value === '4'){	
		//alert("4")
		$("#bankactkeydiv2").show();
		$("#bankactkeydiv3").hide();
		$("#bankactkeydiv1").hide();
	}else{
		//alert("other")
		$("#bankactkeydiv3").show();
		$("#bankactkeydiv1").hide();
		$("#bankactkeydiv2").hide();	
	}
	
	
	
});
</script>


	<INPUT type="HIDDEN" name="nextCapDate" id="nextCapDate"
		value="<%=(sv.nextCapDate.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hcnstcur" id="hcnstcur"
		value="<%=(sv.hcnstcur.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hcover" id="hcover"
		value="<%=(sv.hcover.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hcrtable" id="hcrtable"
		value="<%=(sv.hcrtable.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hemv" id="hemv"
		value="<%=(sv.hemv.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hjlife" id="hjlife"
		value="<%=(sv.hjlife.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="hrider" id="hrider"
		value="<%=(sv.hrider.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="screenIndicArea" id="screenIndicArea"
		value="<%=(sv.screenIndicArea.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="planSuffix" id="planSuffix"
		value="<%=(sv.planSuffix.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="zcshdivmth" id="zcshdivmth"
		value="<%=(sv.zcshdivmth.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="zdivopt" id="zdivopt"
		value="<%=(sv.zdivopt.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="zwdv" id="zwdv"
		value="<%=(sv.zwdv.getFormData()).toString()%>"> <INPUT
		type="HIDDEN" name="subfilePosition" id="subfilePosition"
		value="<%=(sv.subfilePosition.getFormData()).toString()%>">

</div>
</div>


<div style="display: none" id="subfileTable"></div>
<div style="display: none" id="addRemoveDiv"></div>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 starts-->
