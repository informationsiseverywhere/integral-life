

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH502";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sh502ScreenVars sv = (Sh502ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversionary Bonus ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cash Dividend ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allocation at Contract Anniversary ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allocation at Company Anniversary ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.zrevbonalc.setReverse(BaseScreenData.REVERSED);
			sv.zrevbonalc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.zrevbonalc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.zcshdivalc.setReverse(BaseScreenData.REVERSED);
			sv.zcshdivalc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.zcshdivalc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.zconannalc.setReverse(BaseScreenData.REVERSED);
			sv.zconannalc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.zconannalc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.zcoyannalc.setReverse(BaseScreenData.REVERSED);
			sv.zcoyannalc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.zcoyannalc.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			<div class="input-group">
        			<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>
        	
        	
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			<div class="input-group">
        			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>	
        	
        	
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<table><tr>
        			<td>
        			<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
        		</td>
        		</tr></table>
        		</div>
        	</div>			
        		
        </div>
      
        <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reversionary Bonus"))%></label>
<%if ((new Byte((sv.zrevbonalc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input type='checkbox' name='zrevbonalc' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(zrevbonalc)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.zrevbonalc).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.zrevbonalc).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.zrevbonalc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('zrevbonalc')"/>

<input type='checkbox' name='zrevbonalc' value='N' 

<% if(!(sv.zrevbonalc).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('zrevbonalc')"/>
<%}%>
        			
        			</div>
        		</div>
        
        	
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Cash Dividend"))%></label>
        				<%if ((new Byte((sv.zcshdivalc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

<input type='checkbox' name='zcshdivalc' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(zcshdivalc)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.zcshdivalc).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.zcshdivalc).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }if((sv.zcshdivalc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('zcshdivalc')"/>

<input type='checkbox' name='zcshdivalc' value='N' 

<% if(!(sv.zcshdivalc).toString().trim().equalsIgnoreCase("Y")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('zcshdivalc')"/>
<%}%>
        				

        		</div>
        	</div>		
        </div>
       
         <div class="row">
              <div class="col-md-4"><!-- ILIFE-6672 -->
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Allocation at Contract Anniversary"))%></label><!-- ILIFE-6672 label content changed as per iSeries -->
        			<%if ((new Byte((sv.zconannalc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
        			<input type='checkbox' name='zconannalc' value='X' onFocus='doFocus(this)' onHelp='return fieldHelp(zconannalc)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.zconannalc).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.zconannalc).toString().trim().equalsIgnoreCase("x")){
			%>checked
		
      <% }if((sv.zconannalc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('zconannalc')"/>

<input type='checkbox' name='zconannalc' value=' ' 

<% if(!(sv.zconannalc).toString().trim().equalsIgnoreCase("x")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('zconannalc')"/>
  <%}%>      			
        		</div>
        	</div>	
        	
        
        	<div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Allocation at Company Anniversary"))%></label>
        			<%if ((new Byte((sv.zconannalc).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
        			<input type='checkbox' name='zcoyannalc' value='X' onFocus='doFocus(this)' onHelp='return fieldHelp(zcoyannalc)' onKeyUp='return checkMaxLength(this)'    
<%

if((sv.zcoyannalc).getColor()!=null){
			 %>style='background-color:#FF0000;'
		<%}
		if((sv.zcoyannalc).toString().trim().equalsIgnoreCase("x")){
			%>checked
		
      <% }if((sv.zcoyannalc).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){%>
    	   disabled
		
		<%}%>
class ='UICheck' onclick="handleCheckBox('zcoyannalc')"/>

<input type='checkbox' name='zcoyannalc' value=' ' 

<% if(!(sv.zcoyannalc).toString().trim().equalsIgnoreCase("x")){
			%>checked
		
      <% }%>

style="visibility: hidden" onclick="handleCheckBox('zcoyannalc')"/>
  <%}%>  
        			
        		</div>
        	</div>		
        		
        </div>				
   </div>
 </div>    		

<%@ include file="/POLACommon2NEW.jsp"%>

