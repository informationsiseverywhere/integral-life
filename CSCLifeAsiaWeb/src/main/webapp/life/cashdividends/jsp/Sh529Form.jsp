

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH529";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%Sh529ScreenVars sv = (Sh529ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Annual Interest Rate ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"%");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.intRate.setReverse(BaseScreenData.REVERSED);
			sv.intRate.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.intRate.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			<div class="input-group">
        			   <%					
						if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
        			</div>
        		</div>
        	</div>
        	
        	
        <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			<div class="input-group">
        			<%					
					if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
        			</div>
        		</div>
        	</div>
        	
        
        <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<div class="input-group">	
        			<%					
						if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
					
					<%					
					if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="max-width:800px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
					longValue = null;
					formatValue = null;
					%>
        			</div>
        		</div>
        	</div>			
        </div>	
      
      <div class="row">	
		       	<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>
				   <table>
					<tr>
					<td>
						<%					
						if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td>&nbsp;To&nbsp;</td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
			
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
				
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Annual Interest Rate"))%></label>
			    <div class="input-group">
			    <table>	
					<tr>
					<td><%	
			qpsf = fw.getFieldXMLDef((sv.intRate).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>
						
						<input name='intRate' 
						type='text'
						
							value='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
									 <%
							 valueThis=smartHF.getPicFormatted(qpsf,sv.intRate);
							 if(valueThis!=null&& valueThis.trim().length()>0) {%>
							 title='<%=smartHF.getPicFormatted(qpsf,sv.intRate) %>'
							 <%}%>
						
						size='<%= sv.intRate.getLength()%>'
						maxLength='<%= sv.intRate.getLength()%>' 
						onFocus='doFocus(this)' onHelp='return fieldHelp(intRate)' onKeyUp='return checkMaxLength(this)'  
						
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>' 
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
						
						<% 
							if((new Byte((sv.intRate).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>  
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.intRate).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>	
								class="bold_cell" 
						
						<%
							}else { 
						%>
						
							class = ' <%=(sv.intRate).getColor()== null  ? 
									"input_cell" :  (sv.intRate).getColor().equals("red") ? 
									"input_cell red reverse" : "input_cell" %>'
						 
						<%
							} 
						%>	></td>
						<td>
						<%=resourceBundleHandler.gettingValueFromBundle("&nbsp;%")%>
						</td>
					</tr>
				</table>	
				</div>
			</div>		
		</div>		
		 </div>  			
</div>
</div>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>

<%@ include file="/POLACommon2NEW.jsp"%>

