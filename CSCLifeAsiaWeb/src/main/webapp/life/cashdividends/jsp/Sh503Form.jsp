<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH503";%>
<!---Ticket ILIFE-758 starts--><%@ include file="/POLACommon1NEW.jsp"%><!---Ticket ILIFE-758 ends-->
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sh503ScreenVars sv = (Sh503ScreenVars) fw.getVariables();%>
<%{
}%>



<div class="panel panel-default">
        <div class="panel-body">
        	<div class="row">
        		<div class="col-md-10">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank/Branch"))%></label>
        				<table><tr><td>
<%=smartHF.getRichText(0, 0, fw, sv.bankkey,(sv.bankkey.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
 </td><td>
                       <span class="input-group-btn">                       
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankkey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
                          
                          
</td><td>
&nbsp; &nbsp;
</td><td>

<%if ((new Byte((sv.bankdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>

</td><td>
&nbsp;
</td><td>

<%if ((new Byte((sv.branchdesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.branchdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.branchdesc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
          </td></tr></table>
          </div>
         </div>
       </div>  
         
   <div class="row">
        		<div class="col-md-7">
        			<div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bank Account"))%></label>
        			<table><tr><td>	
        			<%=smartHF.getRichText(0, 0, fw, sv.bankacckey,(sv.bankacckey.getLength()),null).replace("absolute","relative").replace("width","float:left; width").replace(" bold","").replace("input_cell","bold_cell")%>
</td><td>

                  <span class="input-group-btn">                          
                           <button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('bankacckey')); doAction('PFKEY04')">
                           <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                          </button>
                          </span>
                          
</td><td>
&nbsp;&nbsp;&nbsp;
</td><td>                          
                          <%if ((new Byte((sv.bankaccdsc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
 	<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	 	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			</td></tr></table>
        		</div>
        	</div>
        </div>			      
         
         
         
</div>
<INPUT type="HIDDEN" name="currcode" id="currcode" value="<%=	(sv.currcode.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="facthous" id="facthous" value="<%=	(sv.facthous.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="numsel" id="numsel" value="<%=	(sv.numsel.getFormData()).toString() %>" >


<INPUT type="HIDDEN" name="payrnum" id="payrnum" value="<%=	(sv.payrnum.getFormData()).toString() %>" >

<br/>
</div>   
<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>        
<%@ include file="/POLACommon2NEW.jsp"%><!---Ticket ILIFE-758 ends-->
