<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH515";%>
<!---Ticket ILIFE-758 starts-->
<%@ include file="/POLACommon1NEW.jsp"%>
<!---Ticket ILIFE-758 ends-->
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sh515ScreenVars sv = (Sh515ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind02.isOn()) {
	sv.sumin.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind01.isOn()) {
	sv.sumin.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind03.isOn()) {
	sv.riskCessAge.setEnabled(BaseScreenData.DISABLED);
}

if (appVars.ind04.isOn()) {
	sv.riskCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind24.isOn()) {
	sv.premCessAge.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind25.isOn()) {
	sv.premCessTerm.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind28.isOn()) {
	sv.premCessDateDisp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind06.isOn()) {
	sv.mortcls.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind05.isOn()) {
	sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind37.isOn()) {
	sv.bappmeth.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind38.isOn()) {
	sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind08.isOn()) {
	sv.liencd.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind07.isOn()) {
	sv.liencd.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.zlinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind31.isOn()) {
	sv.instPrem.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind31.isOn()) {
	sv.linstamt.setInvisibility(BaseScreenData.INVISIBLE);
	sv.linstamt.setEnabled(BaseScreenData.DISABLED);	
}
if (appVars.ind32.isOn()) {
	sv.instPrem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind09.isOn()) {
	sv.optextind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind43.isOn()) {
	sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
	sv.taxamt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind84.isOn()) {
	sv.taxamt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind34.isOn()) {
	sv.pbind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind34.isOn()) {
	sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind42.isOn()) {
	sv.taxind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind42.isOn()) {
	sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind26.isOn()) {
	sv.select.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind27.isOn()) {
	sv.select.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind40.isOn()) {
	sv.zdivopt.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.payeesel.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.paymth.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind40.isOn()) {
	sv.paycurr.setEnabled(BaseScreenData.DISABLED);
}
/*BRD-306 START */
if (appVars.ind32.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind32.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind54.isOn()) {
	sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind52.isOn()) {
	sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind55.isOn()) {
	sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
}
/*BRD-306 END */
if (appVars.ind68.isOn()) {
	sv.prmbasis.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind57.isOn()) {
	sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind74.isOn()) {
			sv.prmbasis.setReverse(BaseScreenData.REVERSED);
			sv.prmbasis.setColor(BaseScreenData.RED);
}
if (!appVars.ind74.isOn()) {
sv.prmbasis.setHighLight(BaseScreenData.BOLD);
}
//BRD-NBP-011 starts
if (appVars.ind82.isOn()) {
	sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
	sv.dialdownoption.setColor(BaseScreenData.RED);
}
if (!appVars.ind82.isOn()) {
	sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind80.isOn()) {
	sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind81.isOn()) {
	sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind83.isOn()) {
	sv.exclind.setInvisibility(BaseScreenData.INVISIBLE);
}
//BRD-NBP-011 ends
//ICIL-560 FWANG3
if (appVars.ind87.isOn()) {
	sv.cashvalarer.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>

<style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
.bold_cell{margin-left:1px}
.blank_cell{margin-left:1px}
}
</style>



<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract no"))%></label>
        			<table>
        			<tr>
        			<td>
        			<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					<td>

						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
				<%
				longValue = null;
				formatValue = null;
				%>
				</td>
				<td>

							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:300px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
        			
        			</td>
        			</tr>
        			</table>
        		</div>
        	</div>		
        </div>
        
        <div class="row">
           <div class="col-md-3">
             <div class="form-group">
        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life Assured"))%></label>
        			<table>
        			<tr>
        			<td>
        			<%if ((new Byte((sv.lifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					<td>

						<%if ((new Byte((sv.linsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:65px;max-width:300px;margin-left: 1px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
        			</td>
        			</tr>
        			</table>
        		</div>
        	</div>
        	
        	<div class="col-md-5"></div>
        	<div class="col-md-4">
                <div class="form-group">
        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData()))%></label>		
        			
        	<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<%-- <div id='zagelit' class='label_txt'class='label_txt'  onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div> --%>
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  <!-- <div class="input-group"> -->
  <%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell"  style="width:50px;"/>
		
		<% 
			} 
		%>
		
	
<%
longValue = null;
formatValue = null;
%>	
           <!--  </div> -->
          </div>
        </div>
      </div>  		
       
       <div class="row">
         <div class="col-md-2">
           <div class="form-group">
        	<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint life"))%></label>
        		<table>
        		<tr>
        		<td>
        			<%if ((new Byte((sv.jlifcnum).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
											"blank_cell" : "output_cell" %>' style="min-width:65px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
					</td>
					<td>
					<%-- <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="max-width:800px;"> --%>
					<%if ((new Byte((sv.jlinsname).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?
											"blank_cell" : "output_cell" %>' style="min-width:65px;max-width:300px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
        			</td>
        			</tr>
        			</table>
        		</div>
        	</div>
        	<div class="col-md-6"></div>
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Plan Policies"))%></label>
        			
        			<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
	<%}%>
        			
        		</div>
        	</div>	
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Policy Num"))%></label>
        				<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.planSuffix.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>'style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        		</div>
        	</div>				
        </div>  
        
         <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Life no"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.life.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.life.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	 <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage no"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Rider no"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.rider.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Stat. fund"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Section"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="width:50px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sub-sect"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
	<% if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
			if(longValue == null || longValue.equalsIgnoreCase("")) {
				formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			} else {
				formatValue = formatValue( longValue);
			}						
		}else{
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
			}else{
				formatValue = formatValue( longValue);
			}
		}
	%>
<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="max-width:80px;">
<%=XSSFilter.escapeHtml(formatValue)%>
</div>	
	<%
	longValue = null;
	formatValue = null;
	%>
<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        </div>			  
        <br>		  
      <hr> 
      <br>
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Effective Date"))%></label> 
        				<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        	    </div>
        	  </div>
        	
        <!-- 	  <div class="col-md-1"></div>   -->
        	  <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Sum Assured"))%></label>
        			
        			<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='sumin' 
type='text'
<%if((sv.sumin).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>
	value='<%=valueThis %>'
			 <%
	 valueThis=valueThis;
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis %>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)%>'
maxLength='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.sumin.getLength(), sv.sumin.getScale(),3)-4%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(sumin)' onKeyUp='return checkMaxLength(this)'  
	
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.sumin).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.sumin).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { %>
	class = ' <%=(sv.sumin).getColor()== null  ? 
			"input_cell" :  (sv.sumin).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%
longValue = null;
formatValue = null;
%>
        			
        		</div>
        	</div>
        	
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Mortality class"))%></label>
        			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.mortcls).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell' style="width:150px;;"> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.mortcls).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:200px;"> 
					<%
					} 
					%>
					<select name='mortcls' type='list' style="width:200px;"
					<% 
				if((new Byte((sv.mortcls).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.mortcls).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.mortcls).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	
</td>
<td width='181'><%
longValue = null;
formatValue = null;
%> 	
        	    </div>
        	 </div>   			

        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Currency"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if ((new Byte((sv.currcd).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currcd"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currcd");
		longValue = (String) mappedItems.get((sv.currcd.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currcd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
        			<!-- </div> -->
        		</div>
        	     </div>	    			
               </div>
                <div class="row">	
               <div class="col-md-3">
                <div class="form-group">
                	<!-- ILJ-47 Starts -->
               		<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
        			<%} %>
        			<!-- ILJ-47 End -->
        			<table>
        			<tr>
        			<td>
        			<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
					<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()+1),null).replace("absolute","relative")%>
					<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
					<%if(sv.riskCessAge.equals(0)) {%>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
					<%} else { %>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
					<%} %>
					<%}else {%>
					hello
					<%}%>
					</td>
					<td style="padding-left: 2px;">

					<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
					<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm,( sv.riskCessTerm.getLength()+1),null).replace("absolute","relative")%>
					<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
					<%if(sv.riskCessTerm.equals(0)) {%>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
					<%} else { %>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
					<%} %>
					<%}else {%>
					hello
					<%}%>	
        			</td>
        			</tr>
        			</table>
        			</div>
        		</div>		 	
      
      <!-- <div class="col-md-1"> </div> -->
        	 <div class="col-md-3"> 
    	 		<div class="form-group"> 	        				    			  
				<!-- ILJ-47 Starts -->
				<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract cess date")%></label>
				<%} else { %>
        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess date"))%></label>
        		<%} %>
				<!-- ILJ-47 End -->
				
				 <% if ((new Byte((sv.riskCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                 <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp)%>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="riskCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.riskCessDateDisp, (sv.riskCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
             	 <%}%>
					    		
					    		 
				</div>
			 </div>		
        	
        
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Lien code"))%></label>
        			
        			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("liencd");
	optionValue = makeDropDownList( mappedItems , sv.liencd.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.liencd).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.liencd).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					<select name='liencd' type='list' style="width:145px;"
					<% 
				if((new Byte((sv.liencd).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.liencd).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.liencd).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	

</td>
<td width='181'>
<%
	longValue = null;
	formatValue = null;
	%>
        			</div>
        		</div>
        
             
             <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Bonus App Method"))%></label>
        			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bappmeth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bappmeth");
	optionValue = makeDropDownList( mappedItems , sv.bappmeth.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bappmeth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.bappmeth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.bappmeth).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
					<%
					} 
					%>
					<select name='bappmeth' type='list' style="width:145px;"
					<% 
				if((new Byte((sv.bappmeth).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.bappmeth).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.bappmeth).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
		} 
	%>	

        		
        		</div>
        	</div>
        </div>			   
        
         <div class="row">		
            <div class="col-md-3">
                <div class="form-group">
        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term"))%></label>
        		<table>
        		<tr>
        		<td>
        			<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
					<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
					<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
					<%if(sv.premCessAge.equals(0)) {%>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
					<%} else { %>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
					<%} %>
					<%}else {%>
					hello
					<%}%>
				</td>
				<td style="padding-left: 2px;">

				<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
				<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
				<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
				<%if(sv.premCessTerm.equals(0)) {%>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
				<%} else { %>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
				<%} %>
				<%}else {%>
				hello
				<%}%>
        		</td>
        		</tr>
        		</table>
        	</div>
         </div> 	  
        	
        	<!-- <div class="col-md-1"></div> -->
        	<div class="col-md-3">
                <div class="form-group">
        			<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prem cess date"))%></label>
        		
        		
        		 <% if ((new Byte((sv.premCessDateDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                                 <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp)%>
                                      
                               
                <%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="premCessDateDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.premCessDateDisp, (sv.premCessDateDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
              <%}%>

	   		
	   		
	

	
 




        		</div>
        	</div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)"))%></label>
        			<!-- <div class="input-group">  -->
                      <input name='select' 
type='text'

<%

		formatValue = (sv.select.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.select.getLength()%>'
maxLength='<%= sv.select.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(select)' onKeyUp='return checkMaxLength(this)'  
	style="width:50px;"

<% 
	if((new Byte((sv.select).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.select).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.select).getColor()== null  ? 
			"input_cell" :  (sv.select).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>

        		<!-- 	</div> -->
        		</div>
        	</div>
        	
        	
           <div class="col-md-3">
                <div class="form-group">  
        			<%	
	if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("prmbasis");
	optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.prmbasis).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:183px;"> 
<%
} 
%>

<select name='prmbasis' type='list' style="width:180px;"
<% 
	if((new Byte((sv.prmbasis).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.prmbasis).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.prmbasis).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>
</div>
</div>
        			
        </div>	
        
         <div class="row">		
            <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee"))%></label>
        			<table><tr>
        			<td >
        				
        				
   						<%
				          if ((new Byte((sv.payeesel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                                                      || fw.getVariables().isScreenProtected()) {
						%>                   
						
						<div style="margin-right: 2px;"><%=smartHF.getHTMLVarExt(fw, sv.payeesel)%></div>
				        <%
							} else {
						%>
				        <div class="input-group" style="width: 150px;">
				            <%=smartHF.getRichTextInputFieldLookup(fw, sv.payeesel)%>
				             <span class="input-group-btn">
				             <button class="btn btn-info"type="button"
				                onClick="doFocus(document.getElementById('payeesel')); doAction('PFKEY04')">
				                <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
				            </button>
				        </div>
				        <%
				         }
				        %>    
                          
                      </td> 
                          
                            <td>
						<%if ((new Byte((sv.payeenme).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
						<% if(!((sv.payeenme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}						
							}else{
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.payeenme.getFormData()).toString()); 
								}else{
									formatValue = formatValue( longValue);
								}
							}
						%>
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left: -2px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
					<%}%>
        		</td></tr></table>
        		</div>
        	</div>
        	
        	<div class="col-md-6"></div>
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dial Down Option"))%></label>
        			
                  <%	
	if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'> 
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.dialdownoption).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #ec7572;  width:183px;"> 
<%
} 
%>

<select name='dialdownoption' type='list' style="width:180px;"
<% 
	if((new Byte((sv.dialdownoption).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.dialdownoption).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.dialdownoption).getColor())){
%>
</div>
<%
} 
%>

<%
}} 
%>
<% longValue = null;%>	
                  
                   
        			
			      
        		</div>
        	</div>
        </div>			 						
        
        <div class="row">		
            <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dividend Option"))%></label>
        			
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"zdivopt"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("zdivopt");
	optionValue = makeDropDownList( mappedItems , sv.zdivopt.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.zdivopt.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.zdivopt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell'> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>

			<%	if("red".equals((sv.zdivopt).getColor())){
					%>
					<div style="border:1px; border-style: solid; border-color: #B55050;  max-width:180px;"> 
					<%
					} 
					%>
					<select name='zdivopt' type='list' style="max-width:180px;"
					<% 
				if((new Byte((sv.zdivopt).getEnabled()))
				.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){  
			%>  
				readonly="true"
				disabled
				class="output_cell"
			<%
				}else if((new Byte((sv.zdivopt).getHighLight())).
					compareTo(new Byte(BaseScreenData.BOLD)) == 0){
			%>	
					class="bold_cell" 
			<%
				}else { 
			%>
					class = 'input_cell' 
			<%
				} 
			%>
			>
					<%=optionValue%>
					</select>
					<% if("red".equals((sv.zdivopt).getColor())){
					%>
					</div>
					<%
					} 
					%>
					<%
					longValue = null;
					formatValue=null;
		} 
	%>	
</td>


			
        			
        		</div>
        	</div>
        	
        	 <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("MOP"))%></label>
        		
        			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paymth"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paymth");
	optionValue = makeDropDownList( mappedItems , sv.paymth.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.paymth.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paymth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell' style="max-width:800px;"> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
<select name='paymth' type='list' style="max-width:800px;"
<% 
	if((new Byte((sv.paymth).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.paymth).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.paymth).getColor()== null  ? 
			"input_cell" :  (sv.paymth).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<%
	} 
%>
        			
        		</div>
        	</div>
        	
        	 <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payment Currency"))%></label>
        		
        			<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"paycurr"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("paycurr");
	optionValue = makeDropDownList( mappedItems , sv.paycurr.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.paycurr.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
<div class='output_cell' style="max-width:800px;"> 
   <%=longValue%>
</div>

<%
longValue = null;
%>

	<% }else {%>
<select name='paycurr' type='list' style="max-width:800px;"
<% 
	if((new Byte((sv.paycurr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.paycurr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = ' <%=(sv.paycurr).getColor()== null  ? 
			"input_cell" :  (sv.paycurr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<%
	} 
%>
<%
longValue = null;
%>        		
        		</div>
        	</div>
        </div>			
        <br>		
        
     <hr>   
     <br>
          <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
				BaseScreenData.INVISIBLE)) != 0){ %>
       <div class="row">
		  
		
		<div class="col-md-3">
           <div class="form-group">
        	<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
			<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
			<%} %>
		
			<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
			<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
			<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
			<%if(sv.adjustageamt.equals(0)) {%>
			<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
			<%} else { %>
			<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
			<%} %>
			<%}else {%>
			<%}%>
		</div>
	</div>
	<div class="col-md-3">
    	<div class="form-group">
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
		<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>		
		<%} %>
		
		<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
		<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
		<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
		<%if(sv.rateadj.equals(0)) {%>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
		<%} else { %>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
		<%} %>
		<%}else {%>
		<%}%>
		</div>
	</div>
		
	<div class="col-md-3">
    	<div class="form-group">
    	<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
		<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
		
		<%} %>
		
		<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
		<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
		<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
		<%if(sv.fltmort.equals(0)) {%>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
		<%} else { %>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;text-align: right;\' ")%>
		<%} %>
		<%}else {%>
		<%}%>
		</div>
	</div>
	<div class="col-md-3">
    	<div class="form-group">
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
		<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
		
		<%} %>
	
		<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
		<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
		<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
		<%if(sv.loadper.equals(0)) {%>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
		<%} else { %>
		<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
		<%} %>
		<%}else {%>
		<%}%>
		</div>
	</div>
		
		
       
    </div>
    <%} %>



 
        <div class="row">		
         
         <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
			BaseScreenData.INVISIBLE)) != 0){ %>
			<div class="col-md-3">
                <div class="form-group">
				<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
				
				<%} %>
				
				<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
				<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
				<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
				<%if(sv.premadj.equals(0)) {%>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
				<%} else { %>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%} %>
				<%}else {%>
				<%}%>
				</div>
			</div>
			<%} %>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium"))%></label>
        			<!-- <div class="input-group"> -->
        			<%if(((BaseScreenData)sv.zlinstprem) instanceof StringBase) {%>
					<%=smartHF.getRichText(0,0,fw,sv.zlinstprem,( sv.zlinstprem.getLength()+1),null).replace("absolute","relative")%>
					<%}else if (((BaseScreenData)sv.zlinstprem) instanceof DecimalData){%>
					<%if(sv.zlinstprem.equals(0)) {%>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important;\'  ") %>
					<%} else { %>
					<%=smartHF.getHTMLVar(0, 0, fw, sv.zlinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
					<%} %>
					<%}else {%>
					<%}%>
        			<!-- </div> -->
        		</div>
        	</div>
        	
        	
        	<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
			BaseScreenData.INVISIBLE)) != 0){ %>

			<div class="col-md-3">
                <div class="form-group">
				<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
												BaseScreenData.INVISIBLE)) != 0) {%>
				<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label> 
				
				<%} %>
				
				<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
				<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
				<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
				<%if(sv.zbinstprem.equals(0)) {%>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
				<%} else { %>
				<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
				<%} %>
				<%}else {%>
				<%}%>
				</div>
			</div>
			<%} %>
        	
        
        </div>	
        
         <div class="row">		
            <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Premium"))%></label>
        				<%if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
	%>

<input name='instPrem' style="width: 145px !important"
type='text'

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.instPrem.getLength(), sv.instPrem.getScale(),3)%>'
maxLength='<%= sv.instPrem.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(instPrem)' onKeyUp='return checkMaxLength(this)'  
	style="width:145px;text-align: right;"
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);'
	onBlur='return doBlurNumberNew(event,true);'

<% 
	if((new Byte((sv.instPrem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.instPrem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.instPrem).getColor()== null  ? 
			"input_cell" :  (sv.instPrem).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
        	  </div>
        	</div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax"))%></label>
        				<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


	<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
	%>

<input name='taxamt' 
type='text'

<%if((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width: 145px !important;"<% }%>

	value='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.taxamt);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.taxamt) %>'
	 <%}%>

size='<%= sv.taxamt.getLength()+4%>'
maxLength='<%= sv.taxamt.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(taxamt)' onKeyUp='return checkMaxLength(this)'  
	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.taxamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.taxamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.taxamt).getColor()== null  ? 
			"input_cell" :  (sv.taxamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
<%}%>
        		</div>
        	</div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Prorate Prem to collect"))%></label>
        				<%	
			qpsf = fw.getFieldXMLDef((sv.linstamt).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			valueThis=smartHF.getPicFormatted(qpsf,sv.linstamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);//ILIFE-2090
			
	%>

<input name='linstamt' 
type='text'
<%if((sv.linstamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right;width:145px;"<% }%>

	value='<%=valueThis%>'
			 <%	 
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=valueThis%>'
	 <%}%>

size='<%=COBOLHTMLFormatter.getLengthWithCommas( sv.linstamt.getLength(), sv.linstamt.getScale(),3)%>'
maxLength='<%= sv.linstamt.getLength()%>' 
onFocus='doFocus(this),onFocusRemoveCommas(this)' onHelp='return fieldHelp(linstamt)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%>,true); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event,true);' 
	onBlur='return doBlurNumberNew(event,true);'
<% 
	if((new Byte((sv.linstamt).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.linstamt).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.linstamt).getColor()== null  ? 
			"input_cell" :  (sv.linstamt).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        		</div>
        	</div>	
        	
		<%
			if ((new Byte((sv.cashvalarer).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>	        	
        	 <div class="col-md-3">
							<div class="form-group">
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Value in Arrears")%></label>
								<%
									qpsf = fw.getFieldXMLDef((sv.cashvalarer).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.cashvalarer,
												COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
										if (!((sv.cashvalarer.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}
			
										if (!formatValue.trim().equalsIgnoreCase("")) {
								%>
								<div class="output_cell"
									style="width: 175px !important; text-align: right;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
									} else {
								%>
			
								<div class="blank_cell" style="width: 175px !important;">
									&nbsp;</div>
			
								<%
									}
								%>
								<%
									longValue = null;
										formatValue = null;
								%>
							</div>
				</div>		
				<%
					}
				%>        				
        </div>	  				 
    </div>
    </div>    
        
        <INPUT type="HIDDEN" name="zbinstprem" id="zbinstprem" value="<%=	(sv.zbinstprem.getFormData()).toString() %>" >

<Div id='mainForm_OPTS' style='visibility:hidden;'>

      
      						<li>
															
								<%
										if (sv.optextind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='optextind' id='optextind' type='hidden' value="<%=sv.optextind.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.optextind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.optextind.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.optextind.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink">
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.optextind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.optextind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%} %>
			 						
			 					</li>	
			 					<li>
								<%
										if (sv.comind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='comind' id='comind' type='hidden' value="<%=sv.comind.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.comind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.comind.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.comind.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("comind"))' class="hyperLink">
									<%=resourceBundleHandler.gettingValueFromBundle("Agent Commission Split")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.comind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.comind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%} %>
								</li>
								
																
								
								<li>
									<%
										if (sv.pbind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='pbind' id='pbind' type='hidden' value="<%=sv.pbind.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.pbind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.pbind.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.pbind.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.pbind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.pbind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%}%>
								</li>
								
								<li>
								<%
										if (sv.bankaccreq.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='bankaccreq' id='bankaccreq' type='hidden' value="<%=sv.bankaccreq.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.bankaccreq.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.bankaccreq.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.bankaccreq.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bankaccreq"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Bank Account")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.bankaccreq.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.bankaccreq.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%}%>
								</li>
								<li>
								<%
										if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='taxind' id='taxind' type='hidden' value="<%=sv.taxind.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.taxind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.taxind.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.taxind.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Tax Detail")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.taxind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.taxind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%} %>
								</li>
								
								<li>
								<%
										if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE) {
									%> 
									<input name='exclind' id='exclind' type='hidden' value="<%=sv.exclind.getFormData()%>"/>                        	
									<!-- text -->
									<%
										if (sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
														|| sv.exclind.getEnabled() == BaseScreenData.DISABLED) {
									%> 
									<a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle(sv.exclind.toString())%>
									<%
			 						} else {
									%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
									<%}%>
									
									<!-- icon -->
									<%
										if (sv.exclind.getFormData().equals("+")) {
									%> 
									<i class="fa fa-tasks fa-fw sidebar-icon"></i>
									<%}
			 						if (sv.exclind.getFormData().equals("X")) {
			 						%>
			 						<i class="fa fa-warning fa-fw sidebar-icon"></i> 
			 						<%}%>
			 						</a>
			 						<%} %>
								</li>
														


</div>	


        
  <%@ include file="/POLACommon2NEW.jsp"%>      