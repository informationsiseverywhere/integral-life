

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH501";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sh501ScreenVars sv = (Sh501ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dates effective     ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"to");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Capitalisation ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Allocation ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed DD ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed DD ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed MM ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fixed MM ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Calculation Subroutine ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dividend Allocation ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"At due date ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"On receipt of full prem ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"On receipt of full prem + 1 instalment ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.freqcy01.setReverse(BaseScreenData.REVERSED);
			sv.freqcy01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.freqcy01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.freqcy02.setReverse(BaseScreenData.REVERSED);
			sv.freqcy02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.freqcy02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.hfixdd01.setReverse(BaseScreenData.REVERSED);
			sv.hfixdd01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.hfixdd01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.hfixdd02.setReverse(BaseScreenData.REVERSED);
			sv.hfixdd02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.hfixdd02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.hfixmm01.setReverse(BaseScreenData.REVERSED);
			sv.hfixmm01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.hfixmm01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.hfixmm02.setReverse(BaseScreenData.REVERSED);
			sv.hfixmm02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.hfixmm02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.intCalcSbr.setReverse(BaseScreenData.REVERSED);
			sv.intCalcSbr.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.intCalcSbr.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.ind01.setReverse(BaseScreenData.REVERSED);
			sv.ind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.ind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.ind02.setReverse(BaseScreenData.REVERSED);
			sv.ind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.ind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			sv.ind03.setReverse(BaseScreenData.REVERSED);
			sv.ind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.ind03.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			<div class="input-group">
        			   <%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>
        	
        	<div class="col-md-2"></div>
        <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			<div class="input-group">
        			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>
        	
        	<div class="col-md-2"></div>
        <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<div class="input-group">	
        			<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>			
        </div>	
      
      <div class="row">	
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dates effective"))%></label>
				   <table>
					<tr>
					<td>
<%					
		if(!((sv.itmfrmDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmfrmDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
</td>

							<td>&nbsp;To&nbsp;</td>


      <td>
						<%					
		if(!((sv.itmtoDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.itmtoDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
		<%
		longValue = null;
		formatValue = null;
		%>
 </td>
 </tr>
 </table>
					
				</div>
			</div>
		 </div>  				
      &nbsp;
      <div class="row">	
            <div class="col-md-2"></div>
		    
		    <div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Capitalisation"))%></label>
				</div>
			</div>
			
			<div class="col-md-3"></div>
		    
		    <div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Allocation"))%></label>
				</div>
			</div>
	 </div>		
     
     <div class="row">
     <div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Freq"))%></label>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">

						<%-- <%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy01" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("freqcy01");
							optionValue = makeDropDownList(mappedItems, sv.freqcy01.getFormData(), 2, resourceBundleHandler);
							longValue = (String) mappedItems.get((sv.freqcy01.getFormData()).toString().trim());
						%>

						<%
							if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
									|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
						%>
						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>

						<%
							longValue = null;
						%>

						<%
							} else {
						%>

						<%
							if ("red".equals((sv.freqcy01).getColor())) {
						%>
						<div
							style="border: 1px; border-style: solid; border-color: #B55050; width: 140px;">
							<%
								}
							%>

							<select name='freqcy01' type='list' style="width: 140px;"
								<%if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
								readonly="true" disabled class="output_cell"
								<%} else if ((new Byte((sv.freqcy01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
								class="bold_cell" <%} else {%> class='input_cell'
								<%}%>>
								<%=optionValue%>
							</select>
							<% if("red".equals((sv.freqcy01).getColor())){
%>
						</div>
						<%
} 
%>

						<%
} 
%> --%>



	<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "freqcy01" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("freqcy01");
						optionValue = makeDropDownList(mappedItems, sv.freqcy01.getFormData(), 2, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.freqcy01.getFormData()).toString().trim());
					%>

					<%
						if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
					%>
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>

					<%
						longValue = null;
					%>

					<%
						} else {
					%>

					<%
						if ("red".equals((sv.freqcy01).getColor())) {
					%>
					<div
						style="border: 1px; border-style: solid; border-color: #B55050;">
						<%
							}
						%>

						<select name='freqcy01' type='list'
							"
							<%if ((new Byte((sv.freqcy01).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" disabled class="output_cell"
							<%} else if ((new Byte((sv.freqcy01).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%> class='input_cell' <%}%>>
							<%=optionValue%>
						</select>
						<%
							if ("red".equals((sv.freqcy01).getColor())) {
						%>
					</div>
					<%
						}
					%>

					<%
						}
					%>

					</div>
			</div>
		</div>	
			
	<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Freq"))%></label>
				</div>
			</div>
			
		<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		
<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"freqcy02"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("freqcy02");
	optionValue = makeDropDownList( mappedItems , sv.freqcy02.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.freqcy02.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.freqcy02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.freqcy02).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='freqcy02' type='list' style="width:140px;"
<% 
	if((new Byte((sv.freqcy02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.freqcy02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.freqcy02).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>


				</div>
			</div>
		</div>		
	</div>
	
	<div class="row">		
	<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fixed DD"))%></label>
				</div>
			</div>
			
		<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		<tr>
		<td width='171'>


<input name='hfixdd01' 
type='text'

<%

		formatValue = (sv.hfixdd01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hfixdd01.getLength()%>'
maxLength='<%= sv.hfixdd01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hfixdd01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hfixdd01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hfixdd01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hfixdd01).getColor()== null  ? 
			"input_cell" :  (sv.hfixdd01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>	
		 </tr>
		  </div>
		  </div>
		  </div>  
		  <div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fixed DD"))%></label>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		<tr>
		<td width='171'>


<input name='hfixdd02' 
type='text'

<%

		formatValue = (sv.hfixdd02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hfixdd02.getLength()%>'
maxLength='<%= sv.hfixdd02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hfixdd02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hfixdd02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hfixdd02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hfixdd02).getColor()== null  ? 
			"input_cell" :  (sv.hfixdd02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
></td>
		</tr>
		           </div>
		         </div>
		       </div>    
		   </div>  
	<div class="row">		
	<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fixed MM"))%></label>
				</div>
			</div>
			
		<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		<tr>
		<td width='171'>


<input name='hfixmm01' 
type='text'

<%

		formatValue = (sv.hfixmm01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hfixmm01.getLength()%>'
maxLength='<%= sv.hfixmm01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hfixmm01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hfixmm01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hfixmm01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hfixmm01).getColor()== null  ? 
			"input_cell" :  (sv.hfixmm01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
		</tr>
		          </div>
		        </div>
		     </div>     
		<div class="col-md-2">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Fixed MM"))%></label>
				</div>
			</div>	   
		<div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		<tr>
		<td width='171'>


<input name='hfixmm02' 
type='text'

<%

		formatValue = (sv.hfixmm02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.hfixmm02.getLength()%>'
maxLength='<%= sv.hfixmm02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(hfixmm02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.hfixmm02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.hfixmm02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.hfixmm02).getColor()== null  ? 
			"input_cell" :  (sv.hfixmm02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
		</tr>
		         </div>
		        </div>
		  </div> 
   </div>
	
	&nbsp;
	<div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Interest Calculation Subroutine"))%></label>
        	    </div>
        	  </div>
        	  <div class="col-md-1"></div>
        	  <div class="col-md-3">
				<div class="form-group">
			      <div class="input-group">
		<tr>
		<td width='188'>


<input name='intCalcSbr' 
type='text'

<%

		formatValue = (sv.intCalcSbr.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.intCalcSbr.getLength()%>'
maxLength='<%= sv.intCalcSbr.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(intCalcSbr)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.intCalcSbr).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.intCalcSbr).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.intCalcSbr).getColor()== null  ? 
			"input_cell" :  (sv.intCalcSbr).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
</td>
		</tr>
		</div>
		</div>
		</div>
        </div>	 
        &nbsp;   			
    <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Dividend Allocation"))%></label>
        	    </div>
        	  </div>
        </div>	      			
	 <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("At due date"))%></label>
        		<div class="input-group">
        		<input name='ind01' 
type='text'

<%

		formatValue = (sv.ind01.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ind01.getLength()%>'
maxLength='<%= sv.ind01.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ind01)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ind01).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ind01).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ind01).getColor()== null  ? 
			"input_cell" :  (sv.ind01).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	
        		</div>	
        		</div>
        	</div>
        </div>
       <div class="row">
              <div class="col-md-4">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("On receipt of full prem"))%></label>
        	    <div class="input-group">
        	    <input name='ind02' 
type='text'

<%

		formatValue = (sv.ind02.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ind02.getLength()%>'
maxLength='<%= sv.ind02.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ind02)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ind02).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ind02).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ind02).getColor()== null  ? 
			"input_cell" :  (sv.ind02).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        	    </div>	
        	  </div>
        	</div> 
        	
        	<div class="col-md-1"></div>
        	 <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("On receipt of full prem + 1 instalment"))%></label>
        	    <div class="input-group">
        	    <input name='ind03' 
type='text'

<%

		formatValue = (sv.ind03.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.ind03.getLength()%>'
maxLength='<%= sv.ind03.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(ind03)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.ind03).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.ind03).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.ind03).getColor()== null  ? 
			"input_cell" :  (sv.ind03).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        	    </div>
        	   </div>
        	 </div>      		  
        </div>					        			
</div>
</div>





<%@ include file="/POLACommon2NEW.jsp"%>

