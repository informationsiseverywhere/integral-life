

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH500";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.cashdividends.screens.*" %>

<%Sh500ScreenVars sv = (Sh500ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Table ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Item ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Processing Subroutine ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reversal Subroutine ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payee Details Required ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Y=Yes, N=No, O=Optional)");%>

<div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Company"))%></label>
        			<div class="input-group">	
        			<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>	
        	
        	<div class="col-md-2"></div>
        	
        	<div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Table"))%></label>
        			<div class="input-group">	
        			<%					
		if(!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tabl.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>	
           
           
           <div class="col-md-2"></div>
           
           <div class="col-md-2">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Item"))%></label>
        			<div class="input-group">	
        			<%					
		if(!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.item.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%					
		if(!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.longdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:800px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
		<%
		longValue = null;
		formatValue = null;
		%>
        			</div>
        		</div>
        	</div>			
        </div>
        &nbsp;
       <div class="row">
              <div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Processing Subroutine"))%></label>
        			<div class="input-group"> 
        			<input name='subprog' 
type='text'

<%

		formatValue = (sv.subprog.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.subprog.getLength()%>'
maxLength='<%= sv.subprog.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(subprog)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.subprog).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.subprog).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.subprog).getColor()== null  ? 
			"input_cell" :  (sv.subprog).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        			</div>
        		</div>
        	</div>
        	
        	<div class="col-md-5"></div>
        	
        	<div class="col-md-3">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Reversal Subroutine"))%></label>
        			<div class="input-group">
        			<input name='trevsub' 
type='text'

<%

		formatValue = (sv.trevsub.getFormData()).toString();

%>
	value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.trevsub.getLength()%>'
maxLength='<%= sv.trevsub.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(trevsub)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.trevsub).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.trevsub).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.trevsub).getColor()== null  ? 
			"input_cell" :  (sv.trevsub).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
        			</div>
        		</div>
        	</div>		
        </div>			
        
        <div class="row">
              <div class="col-md-6">
                <div class="form-group">
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Payee Details Required"))%></label>
        			<div class="input-group"> 
        			<%
if(sv.payeereq.getFormData().equalsIgnoreCase("Y")){
longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
}
else if(sv.payeereq.getFormData().equalsIgnoreCase("N")){
longValue=resourceBundleHandler.gettingValueFromBundle("No");
}
else if(sv.payeereq.getFormData().equalsIgnoreCase("O")){
longValue=resourceBundleHandler.gettingValueFromBundle("Optional");
}
%>
<% 
	if((new Byte((sv.payeereq).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
<select name="payeereq"
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(payeereq)'
	onKeyUp='return checkMaxLength(this)'
	<%
						if ((new Byte((sv.payeereq).getEnabled())).compareTo(new Byte(
								BaseScreenData.DISABLED)) == 0) {
					%>
					readonly="true" disabled class="output_cell"
					<%
						} else if ((new Byte((sv.payeereq).getHighLight()))
								.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
					%>
					class="bold_cell"
		
					<%
						} else {
					%>
		
					class = '
					<%=(sv.payeereq).getColor() == null ? "input_cell"
										: (sv.payeereq).getColor().equals("red") ? "input_cell red reverse"
												: "input_cell"%>'
		
					<%
						}
					%>
	>
	
<option value="Y"<% if(sv.payeereq.getFormData().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(sv.payeereq.getFormData().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
<option value="O"<% if(sv.payeereq.getFormData().equalsIgnoreCase("O")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Optional")%></option>
		
					
				</select>

<%} %>
        			</div>
        		</div>
        	</div>
        <div style='visibility:hidden;'><table>
<tr style='height:42px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("(Y=Yes, N=No, O=Optional)")%>
</div>

</tr></table></div><br/></div>	
        </div>			   
 </div>
     		
        				




<%@ include file="/POLACommon2NEW.jsp"%>

