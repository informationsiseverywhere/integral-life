

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5b3";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>

<%Sr5b3ScreenVars sv = (Sr5b3ScreenVars) fw.getVariables();%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind20.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	
<%		appVars.rollup(new int[] {93});
%> 


<div class="panel panel-default">
	<div class="panel-body">

	<div class="row">

	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		
  			<table>
  			<tr>
  			<td>
			<%					
			if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
	  		</td>
	  		<td>	
	  		
			<%					
			if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
	  		</td>
	  		<td>		
	  		
			<%					
			if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="margin-left: 1px;width: 150px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
	  		</td>
	  		</tr>
	  		</table>
	  	</div>
	</div>
	
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<div class="form-group">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
	
	
	  		
			<%	
			fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("register");
			longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
		%>
		<table>
		<tr>
		<td>
	    
		   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
								"blank_cell" : "output_cell" %>'  style="width: 100px;"> 
		   <%=	(sv.register.getFormData()).toString()%>
		   </div>
		</td>
		<td>   
		   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 100px;margin-left: 1px;">  
		   		<%if(longValue != null){%>
		   		
		   		<%=longValue%>
		   		
		   		<%}%>
		   </div>
		 </td>
		 </tr>
		 </table>
	   		<%
			longValue = null;
			formatValue = null;
			%>
	   
	
		</div>
	</div>

	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>

			<%					
			if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width: 100px">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
  
			</div>
		</div>

		

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

			<%					
			if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
						
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
								
								
						} else  {
									
						if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
						
						}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width: 100px">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div>	
			<%
			longValue = null;
			formatValue = null;
			%>
  		</div>
  	</div>

	<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

	
  		
			<%	
			fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
			mappedItems = (Map) fieldItem.get("cntcurr");
			longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
		%>
		<table>
		<tr>
		<td>
	    
		   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 100px;"> 
		   <%=	(sv.cntcurr.getFormData()).toString()%>
		   </div>
		   </td>
		   <td>
		   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
								"blank_cell" : "output_cell" %>' style="width: 100px;margin-left: 1px;">  
		   		<%if(longValue != null){%>
		   		
		   		<%=longValue%>
		   		
		   		<%}%>
		   </div>
		  </td>
		 </tr>
		 </table>
	   		<%
			longValue = null;
			formatValue = null;
			%>
	

		</div>
	</div>


	</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sr5b3' width='100%'>
						<thead>
							<tr class='info'>
								
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Select")%></th>
								         								
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Tran No.")%></th>
						
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Tran Code")%></th>
								
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Tran Description")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Tran Date")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("NLG Flag")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Tran Amount")%></th>
								         								
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("NLG Balance")%></th>
							</tr>
						</thead>
						<tbody>
							<%
							GeneralTable sfl = fw.getTable("sr5b3screensfl");
							/* int height;
							if(sfl.count()*27 > 150) {
							height = 210 ;
							} else {
							height = sfl.count()*27;
							} */
							
							%>
							
							
									
									
		<%
	
	String backgroundcolor="#FFFFFF";
	
	Sr5b3screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr5b3screensfl
	.hasMoreScreenRows(sfl)) {
	
%>

	<tr style="background:<%= backgroundcolor%>;">
						    										
													
					
					 						 
						 <td>
						 						 	<div style="margin-left: 20px;">
						 						 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr5b3screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr5b3screensfl.select_R<%=count%>'
						 id='sr5b3screensfl.select_R<%=count%>'
						 onClick="selectedRow('sr5b3screensfl.select_R<%=count%>')"
						 class="UICheck"
					 />
						  
						  						 	</div>
						 						 
										
					
											
									</td>
				    									<td style="color:#434343; padding: 5px; width:100px;z-index:8;position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;" align="left">														
																
									
												
							<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="sr5b3screensfl" + "." +
					      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.tranno.getFormData()%></span></a>							 						 		
						
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:150px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.batctrcde.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.trandesc.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.effdate.getFormData()%>
						
														 
				
									</td>
				    									<%-- <td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.batcactmn.getFormData()%>
						
														 
				
									</td>
									
							
														<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.batcactyr.getFormData()%>
						
														 
				
									</td> --%>
																	<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="center">									
																
									
											
						<%= sv.nlgflag.getFormData()%>
						
														 
				
									</td>
																	<%-- <td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.fromdate.getFormData()%>
						
														 
				
									</td>
																	<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.todate.getFormData()%>
						
														 
				
									</td> --%>
																	<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="right">									
																
									
									
						<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.tranamt).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.tranamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!sv.tranamt.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>&nbsp;
						<%
								longValue = null;
								formatValue = null;
						%>
						
														 
				
									</td>
																	<td style="color:#434343; padding: 5px; width:100px;font-weight: bold;font-size: 12px; font-family: Arial" align="right">									
																
									
											
					<%	
						sm = sfl.getCurrentScreenRow();
						qpsf = sm.getFieldXMLDef((sv.nlgbal).getFieldName());						
						//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
					%>
					
										
						<%
							formatValue = smartHF.getPicFormatted(qpsf,sv.nlgbal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!sv.tranamt.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>&nbsp;
						<%
								longValue = null;
								formatValue = null;
						%>
						
														 
				
									</td>
	
		
		
						</tr>
					
					
						<%
						if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
							backgroundcolor="#ededed";
							}else{
							backgroundcolor="#FFFFFF";
							}
						count = count + 1;
						Sr5b3screensfl
						.setNextScreenRow(sfl, appVars, sv);
						}
						%>
										
						</tbody>
					</table>
				</div>
			</div>
		</div>


	
		
<%-- 

<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr5b3Table", {
				fixedCols : 0,	
				
				colWidths : [60,70,80,180,80,75,130,130],
				hasHorizonScroll :"Y",
				moreBtn: "N",	
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
 --%>

		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
</div>
</div>


<script>
$(document).ready(function() {
    var showval= document.getElementById('show_lbl').value;
    var toval= document.getElementById('to_lbl').value;
    var ofval= document.getElementById('of_lbl').value;
    var entriesval= document.getElementById('entries_lbl').value;
    var nextval= document.getElementById('nxtbtn_lbl').value;
    var previousval= document.getElementById('prebtn_lbl').value;
    var dtmessage =  document.getElementById('msg_lbl').value;
    $('#dataTables-sr5b3').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "ordering": false,
        "info":     false,
        "searching": false,
        "language": {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {
                "next":       nextval,
                "previous":   previousval
            }
        }
    } );
   
} );

</script>

<%@ include file="/POLACommon2NEW.jsp"%>

