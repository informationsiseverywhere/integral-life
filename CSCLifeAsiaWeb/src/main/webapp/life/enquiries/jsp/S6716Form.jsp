<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6716";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	{
		appVars.rollup();
		appVars.rolldown();
	}
%>
<%
	S6716ScreenVars sv = (S6716ScreenVars) fw.getVariables();
%>

<%
	if (sv.S6716screenWritten.gt(0)) {
%>
<%
	S6716screen.clearClassString(sv);
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Contract number ");
%>

<%
	sv.chdrnum.setClassString("");
%>
<%
	sv.chdrnum.appendClassString("string_fld");
		sv.chdrnum.appendClassString("output_txt");
		sv.chdrnum.appendClassString("highlight");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life no ");
%>

<%
	sv.life.setClassString("");
%>
<%
	sv.life.appendClassString("string_fld");
		sv.life.appendClassString("output_txt");
		sv.life.appendClassString("highlight");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Joint Life ");
%>

<%
	sv.jlife.setClassString("");
%>
<%
	sv.jlife.appendClassString("string_fld");
		sv.jlife.appendClassString("output_txt");
		sv.jlife.appendClassString("highlight");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life    ");
%>

<%
	sv.lifenum.setClassString("");
%>
<%
	sv.lifenum.appendClassString("string_fld");
		sv.lifenum.appendClassString("output_txt");
		sv.lifenum.appendClassString("highlight");
%>
<%
	sv.lifename.setClassString("");
%>
<%
	sv.lifename.appendClassString("string_fld");
		sv.lifename.appendClassString("output_txt");
		sv.lifename.appendClassString("highlight");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Sex              ");
%>

<%
	sv.sex.setClassString("");
%>
<%
	sv.sex.appendClassString("string_fld");
		sv.sex.appendClassString("output_txt");
		sv.sex.appendClassString("highlight");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Date of birth ");
%>

<%
	sv.dobDisp.setClassString("");
%>
<%
	sv.dobDisp.appendClassString("string_fld");
		sv.dobDisp.appendClassString("output_txt");
		sv.dobDisp.appendClassString("highlight");
%>
<%
	sv.dummy.setClassString("");
%>
<%
	sv.dummy.appendClassString("string_fld");
		sv.dummy.appendClassString("output_txt");
		sv.dummy.appendClassString("highlight");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Age admitted");
%>

<%
	sv.zagelit.setClassString("");
%>
<%
	sv.anbage.setClassString("");
%>
<%
	sv.anbage.appendClassString("num_fld");
		sv.anbage.appendClassString("output_txt");
		sv.anbage.appendClassString("highlight");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Medical Evidence ");
%>

<%
	sv.selection.setClassString("");
%>
<%
	sv.selection.appendClassString("string_fld");
		sv.selection.appendClassString("output_txt");
		sv.selection.appendClassString("highlight");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Smoking          ");
%>

<%
	sv.smoking.setClassString("");
%>
<%
	sv.smoking.appendClassString("string_fld");
		sv.smoking.appendClassString("output_txt");
		sv.smoking.appendClassString("highlight");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Occupation    ");
%>

<%
	sv.occup.setClassString("");
%>
<%
	sv.occup.appendClassString("string_fld");
		sv.occup.appendClassString("output_txt");
		sv.occup.appendClassString("highlight");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Pursuit code     ");
%>

<%
	sv.pursuit01.setClassString("");
%>
<%
	sv.pursuit01.appendClassString("string_fld");
		sv.pursuit01.appendClassString("output_txt");
		sv.pursuit01.appendClassString("highlight");
%>
<%
	sv.pursuit02.setClassString("");
%>
<%
	sv.pursuit02.appendClassString("string_fld");
		sv.pursuit02.appendClassString("output_txt");
		sv.pursuit02.appendClassString("highlight");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Relationship with Joint Owner  ");
%>

<%
	sv.relation.setClassString("");
%>
<%
	sv.relation.appendClassString("string_fld");
		sv.relation.appendClassString("output_txt");
		sv.relation.appendClassString("highlight");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Height/Weight    ");
%>

<%
	sv.height.setClassString("");
%>
<%
	sv.height.appendClassString("num_fld");
		sv.height.appendClassString("output_txt");
		sv.height.appendClassString("highlight");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "/");
%>

<%
	sv.weight.setClassString("");
%>
<%
	sv.weight.appendClassString("num_fld");
		sv.weight.appendClassString("output_txt");
		sv.weight.appendClassString("highlight");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"BMI           ");
%>

<%
	sv.bmi.setClassString("");
%>
<%
	sv.bmi.appendClassString("num_fld");
		sv.bmi.appendClassString("output_txt");
		sv.bmi.appendClassString("highlight");
		sv.bmi.appendClassString("highlight");
%>
<%
	sv.optdsc01.setClassString("");
%>
<%
	sv.optdsc01.appendClassString("string_fld");
		sv.optdsc01.appendClassString("output_txt");
		sv.optdsc01.appendClassString("highlight");
%>
<%
	sv.optind01.setClassString("");
%>
<%
	sv.optdsc02.setClassString("");
%>
<%
	sv.optdsc02.appendClassString("string_fld");
		sv.optdsc02.appendClassString("output_txt");
		sv.optdsc02.appendClassString("highlight");
%>
<%
	sv.optind02.setClassString("");
%>
<%
	sv.rollit.setClassString("");
%>
<%
	sv.rollit.appendClassString("string_fld");
		sv.rollit.appendClassString("output_txt");
		sv.rollit.appendClassString("highlight");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
				"Use roll keys to display joint Life details");
%>

<%
	generatedText16.appendClassString("label_txt");
		generatedText16.appendClassString("information_txt");
%>
<%
	sv.screenRow.setClassString("");
%>
<%
	sv.screenColumn.setClassString("");
%>

<%
	{
			if (appVars.ind02.isOn()) {
				sv.zagelit.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind02.isOn()) {
				generatedText12.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind02.isOn()) {
				sv.height.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind02.isOn()) {
				generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind02.isOn()) {
				sv.weight.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind02.isOn()) {
				generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind04.isOn()) {
				sv.optind01.setReverse(BaseScreenData.REVERSED);
				sv.optind01.setColor(BaseScreenData.RED);
			}
			if (appVars.ind01.isOn()) {
				sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
				sv.optind01.setEnabled(BaseScreenData.DISABLED);
			}
			if (!appVars.ind04.isOn()) {
				sv.optind01.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind05.isOn()) {
				sv.optind02.setReverse(BaseScreenData.REVERSED);
				sv.optind02.setColor(BaseScreenData.RED);
			}
			if (appVars.ind02.isOn()) {
				sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
				sv.optind02.setEnabled(BaseScreenData.DISABLED);
			}
			if (!appVars.ind05.isOn()) {
				sv.optind02.setHighLight(BaseScreenData.BOLD);
			}
			if (appVars.ind53.isOn()) {
				sv.rollit.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind53.isOn()) {
				generatedText16.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind54.isOn()) {
				generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind54.isOn()) {
				sv.dummy.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind02.isOn()) {
				sv.bmi.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind03.isOn()) {
				sv.relationwithowner.setInvisibility(BaseScreenData.INVISIBLE);
			}
			if (appVars.ind04.isOn()) {
				sv.occupcls.setInvisibility(BaseScreenData.INVISIBLE);
			}
			//ILJ-387
			if (appVars.ind06.isOn()) {
				sv.relation.setInvisibility(BaseScreenData.INVISIBLE);
			}
		}
	}
%>






<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					<div class="form-group" style="width: 100px">

						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life no")%></label>
					<div class="form-group" style="width: 60px">
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<div class="form-group" style="width: 71px">
						<%
							if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlife.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>



		</div>




		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="max-width: 100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;max-width:150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<%
				if ((new Byte((sv.relationwithowner).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Owner")%></label>
					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "relationwithowner" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("relationwithowner");
						longValue = (String) mappedItems.get((sv.relationwithowner.getFormData()).toString().trim());
						if (longValue == null || longValue.equals("")) {
							formatValue = formatValue((sv.relationwithowner.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}
					%>
					<div class='output_cell' style="width: 220px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>					
				</div>
			</div>
			<% } %>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
					<div class="form-group" style="width: 60px">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "sex" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("sex");
							longValue = (String) mappedItems.get((sv.sex.getFormData()).toString().trim());
						%>

						<%
							if (!((sv.sex.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.sex.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.sex.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Date of birth")%></label>
					<div class="form-group" style="width: 120px">
						<%
							if (!((sv.dobDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.dobDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.dobDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>



			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Age admitted")%></label>
					<div class="form-group" style="width: 71px">
						<%
							if (!((sv.dummy.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.dummy.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.dummy.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>







		</div>



		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Last Brth")%></label>
					<div class="form-group" style="width: 60px">
						<%
							qpsf = fw.getFieldXMLDef((sv.anbage).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.anbage);

							if (!((sv.anbage.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>







					</div>
				</div>
			</div>






			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Medical Evidence")%></label>
					<div class="form-group" style="width: 180px">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "selection" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("selection");
							longValue = (String) mappedItems.get((sv.selection.getFormData()).toString().trim());
						%>
						<%
							if (!((sv.selection.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.selection.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.selection.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
				</div>
			</div>



			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
					<div class="form-group" style="width: 110px">
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "smoking" }, sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("smoking");
							longValue = (String) mappedItems.get((sv.smoking.getFormData()).toString().trim());
						%>

						<%
							if (!((sv.smoking.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.smoking.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.smoking.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>


					</div>
				</div>
			</div>




		</div>




		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="width: 260px">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
					<table>
						<tr>
							<td>
								<%
									if ((new Byte((sv.occup).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%> <%
 	fieldItem = appVars.loadF4FieldsLong(new String[] { "occup" }, sv, "E", baseModel);
 		mappedItems = (Map) fieldItem.get("occup");
 		longValue = (String) mappedItems.get((sv.occup.getFormData()).toString().trim());
 %>


								<div style="width: 100px;"
									class='<%=((sv.occup.getFormData() == null) || ("".equals((sv.occup.getFormData()).trim())))
						? "blank_cell" : "output_cell"%>'>
									<%=(sv.occup.getFormData()).toString()%>
								</div>
							</td>
							<td>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px; width: 100px;">
									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div> <%
 	longValue = null;
 		formatValue = null;
 %> <%
 	}
 %>
							</td>

							<!--<%if (!((sv.occup.getFormData()).toString()).trim().equalsIgnoreCase("")) {

				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.occup.getFormData()).toString());
				} else {
					formatValue = formatValue(longValue);
				}

			} else {

				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.occup.getFormData()).toString());
				} else {
					formatValue = formatValue(longValue);
				}

			}%>			
				<div class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'  style="max-width:400px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%longValue = null;
			formatValue = null;%>
		-->
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit code")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.pursuit01.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pursuit01.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pursuit01.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 71px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.pursuit02.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pursuit02.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.pursuit02.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="min-width: 71px; margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>



			<div class="col-md-4">
				<div class="form-group">
					<!-- ILJ-387 -->
					<%-- if ((new Byte((sv.relation).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ --%>
					 <% if (sv.cntEnqScreenflag.compareTo("N") == 0){ %>  
						<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Joint Owner")%></label>
					<% } else{%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Relationship with Contract Owner")%></label>
					<% } %>
					<!--  END  -->
					<div class="form-group" style="width: 71px">

						<%
							if (!((sv.relation.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.relation.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.relation.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</div>
					<%// } %>
				</div>
			</div>


		</div>



		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Height/Weight")%></label>

					<table>
						<tr>
							<td>
								<%
									qpsf = fw.getFieldXMLDef((sv.height).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf, sv.height);

									if (!((sv.height.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue(formatValue);
										} else {
											formatValue = formatValue(longValue);
										}
									}

									if (!formatValue.trim().equalsIgnoreCase("")) {
								%>
								<div class="output_cell">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	} else {
 %>

								<div class="blank_cell">&nbsp;</div> <%
 	}
 %>
							</td>
							<td>
								<%
									qpsf = fw.getFieldXMLDef((sv.weight).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf, sv.weight);

									if (!((sv.weight.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue(formatValue);
										} else {
											formatValue = formatValue(longValue);
										}
									}

									if (!formatValue.trim().equalsIgnoreCase("")) {
								%>
								<div style="margin-left: 1px;" class="output_cell">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	} else {
 %>

								<div style="margin-left: 1px;" class="blank_cell">&nbsp;</div> <%
 	}
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("BMI")%></label>
					<div class="input-group" style="width: 71px">
						<%
							qpsf = fw.getFieldXMLDef((sv.bmi).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS1);
							formatValue = smartHF.getPicFormatted(qpsf, sv.bmi);

							if (!((sv.bmi.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div  class="blank_cell">&nbsp;</div>

						<%
							}
						%>
					</div>
				</div>
			</div>
<!-- fwang3 ICIL-4 -->
			<%
				if ((new Byte((sv.occupcls).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-4">
				<div class="form-group" style="width: 200px;">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>
					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "occupcls" }, sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("occupcls");
						longValue = (String) mappedItems.get((sv.occupcls.getFormData()).toString().trim());
					%>

						<div class='output_cell' style="width: 200x;">
							<%=longValue==null?"":longValue%>
						</div>

					<%
						longValue = null;
					%>
				</div>
			</div>
			<%
				}
			%>			
<!-- fwang3 ICIL-4 END-->			
		</div>
		<br> <br>
		
		<div class="row">

			<div class="col-md-4">
				<div class="form-group">
					<a class="btn btn-info" href="#"
						onClick=" JavaScript:doAction('PFKey90');"><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></a>
					<a class="btn btn-info" href="#"
						onClick=" JavaScript:doAction('PFKey91');"><%=resourceBundleHandler.gettingValueFromBundle("Previous")%></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div style='visibility: hidden;'>
	<table>
		<tr style='height: 22px;'>
			<td width='188'>

				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("/")%>
				</div> <br /> <%
 	if (!((sv.rollit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.rollit.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.rollit.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%
 	if (!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.zagelit.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.zagelit.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
 	longValue = null;
 	formatValue = null;
 %>
			</td>

			<td width='188'>

				<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("Use roll keys to display joint Life details")%>
				</div>
		</tr>
	</table>
</div>

<BODY>
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li><span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li><input name='optind01' id='optind01' type='hidden'
									value="<%=sv.optind01.getFormData()%>"> <%
 	if (sv.optind01.getInvisible() != BaseScreenData.INVISIBLE
 			|| sv.optind01.getEnabled() != BaseScreenData.DISABLED) {
 %> <a href="javascript:;"
									onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind01"))'
									class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Clt Detail")%>.
										. <%
 	}
 %> <%
 	if (sv.optind01.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	} else {

 		if (sv.optind01.getFormData().equals("X")) {
 %>
										<div></div> <%
 	} else {
 %>
										<div style="width: 15px"></div> <%
 	}
 	}
 	if (sv.optind01.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %></a></li>
								<li><input name='optind02' id='optind02' type='hidden'
									value="<%=sv.optind02.getFormData()%>"> <%
 	if (sv.optind02.getInvisible() != BaseScreenData.INVISIBLE
 			|| sv.optind02.getEnabled() != BaseScreenData.DISABLED) {
 %> <a href="javascript:;"
									onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))'
									class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Questions")%>.
										. <%
 	}
 %> <%
 	if (sv.optind02.getFormData().equals("+")) {
 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
 	} else {

 		if (sv.optind02.getFormData().equals("X")) {
 %>
										<div></div> <%
 	} else {
 %>
										<div style="width: 15px"></div> <%
 	}
 	}
 	if (sv.optind02.getFormData().equals("X")) {
 %> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
 	}
 %></a></li>

							</ul>
					</span></li>
				</ul>
			</div>
		</div>
	</div>
</body>

<%-- 
<Div id="mainForm_OPTS" style="visibility:hidden">


<table>
<tr style='height:40px;'>

<td>
<input name='optind01' id='optind01' type='hidden'  value="<%=sv.optind01
.getFormData()%>">
<%
if (sv.optind01
.getFormData().equals("+")) {
%>

&nbsp;&nbsp;&nbsp;<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0">

<%}
	if (sv.optind01
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind01'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.optind01
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind01
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 30 px">
<a href="javascript:;"  style="text-decoration: none"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind01"))' 
class="hyperLink">

<%=resourceBundleHandler.gettingValueFromBundle("Clt Detail")%>. .
 
</a>
</div>	
<%} %>
</td>

</tr> 
 

<tr><td>
<input name='optind02' id='optind02' type='hidden'  value="<%=sv.optind02
.getFormData()%>">
<%
if (sv.optind02
.getFormData().equals("+")) {
%>

&nbsp;&nbsp;&nbsp;<img  src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_tick_01.gif" border="0">

<%}
	if (sv.optind02
.getFormData().equals("X")) {
%>
<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optind02'))" style="cursor:pointer" 
src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0">
<%							            
}
%>
</td>
<td style="font-size: 14px; font-family: Arial">
<%

if(!(sv.optind02
.getInvisible()== BaseScreenData.INVISIBLE|| sv.optind02
.getEnabled()==BaseScreenData.DISABLED)){
%>
<div style="height: 30 px">
<a href="javascript:;" style="text-decoration: none"
onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))' 
class="hyperLink"> 

<%=resourceBundleHandler.gettingValueFromBundle("Questions")%>. .

</a>
</div>	
<%} %>
</td></tr>


</table>

</Div>
 --%>





<%@ include file="/POLACommon2NEW.jsp"%>

