

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SD5E2";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sd5e2ScreenVars sv = (Sd5e2ScreenVars) fw.getVariables();%>
<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner ");%>
<%{
    if (appVars.ind02.isOn()) {
        sv.bonusVal.setInvisibility(BaseScreenData.INVISIBLE);
    }
    if (appVars.ind18.isOn()) {
		sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
	}
}%>
<%	appVars.rollup(new int[] {93});%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%					
								if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							<td>
								<%					
								if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
						</tr>
					</table>

				</div>
			</div>
		</div>
	
    	<div class="row" >
			<div class="col-md-12">
				<div class="table-responsive" style="width:98.6% !important;border-radius: 5px;">
					<table class="table table-striped table-bordered table-hover"
						 width="98.6%"
						style="border-right: thin solid #dddddd !important; margin-bottom: 0px !important;">
						<thead>
							<tr class='info'>
							<th><label>Total</label></th>
        	
        	</tr>
        	</thead>
        	</table>
        	</div>
        	</div>
        	</div>	
		<div class="row" >
		
		
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>		
					<div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">											
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.sumin, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>											
			</div>	</div>
			</div>
					
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Accident Benefit")%></label>	
					<div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">												
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.accident, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>											
				</div></div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Hospital Benefit")%></label>
					<div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">													
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.hospital, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>											
			</div>	</div>
			</div>
								
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Fund Value")%></label>	
						<div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">											
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.fundamnt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>											
			</div>	</div>
			</div>
			
			<!-- </div>
			<div class="row" >  -->
			

	<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Amount")%></label>	
					<div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">												
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.loanVal, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>											
				</div></div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Amount")%></label>
					 <div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">												
						<%=smartHF.getHTMLVar(0, 0, fw, null,sv.surrval, true,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>											
				</div></div>
			</div>


            <%if ((new Byte((sv.bonusVal).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Amount")%></label>
                        <div class="input-group" style='max-width: 100px !important;min-width:100px !important' align="right">
                        <%=smartHF.getHTMLVar(0, 0, fw, null,sv.bonusVal, true,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
                    </div></div>
                </div>
            <%} %>

		</div>
<div class="row">
			<div class="col-md-12">
				<div class="table-responsive" style="width:98.6% !important;border-radius: 5px;">
					<table class="table table-striped table-bordered table-hover"
						 
						style="border-right: thin solid #dddddd !important;margin-bottom: 0px !important;">
						<thead>
							<tr class='info'>
							<th><label><%=resourceBundleHandler.gettingValueFromBundle("Policy List")%></label></th>
        	
        	</tr>
        	</thead>
        	</table>
        	</div>
        	</div>
        	</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-sd5e2" width="100%"
						style="border-right: thin solid #dddddd !important;">
						<thead>
							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Select")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract No.")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract Type")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></center></th>
								<%-- <%=resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date")%> --%>
								<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<th><center><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></center></th>
					<%} else { %>
					<th><center><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></center></th>
						<%} %>
                   <!-- ILJ-49 ends -->
							</tr>
						</thead>
						<tbody>
							<%
                            	GeneralTable sfl = fw.getTable("sd5e2screensfl");
                            	Sd5e2screensfl.set1stScreenRow(sfl, appVars, sv);
                            	int count = 1;
                            	while (Sd5e2screensfl.hasMoreScreenRows(sfl)) {
								%>
							<tr>
								<td><input type="radio"
									value='<%=sv.select.getFormData()%>'
									name='sd5e2screensfl.select_R<%=count%>'
									id='sd5e2screensfl.select_R<%=count%>'
									onClick="selectedRow('sd5e2screensfl.select_R<%=count%>')"
									<% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                               		||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
									disabled="disabled" <% } %> /></td>

								<td><b><%=sv.chdrnum.getFormData()%></b></td>
								<td><b><%=sv.cnttype.getFormData()%></b></td>
								<td><b><%=sv.chdrstatus.getFormData()%></b></td>
								<td><b><%=sv.premstatus.getFormData()%></b></td>
								<td><b><%=sv.occdateDisp.getFormData()%></b></td>



							</tr>
							<%
								count = count + 1;
								Sd5e2screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
$(document).ready(function() {
	$('#dataTables-sd5e2').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

