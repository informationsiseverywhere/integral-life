<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr60u";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>

<%Sr60uScreenVars sv = (Sr60uScreenVars) fw.getVariables();%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client Number ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Number");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Scheme Number ");%>
	
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel   Tax Period From    Tax Period To");%>
<%		appVars.rollup(new int[] {93});
%>



<div class="panel panel-default">
		
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
<%=resourceBundleHandler.gettingValueFromBundle("Client Number")%>
<div class="input-group"> 




	
  		
		<%					
		if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    		
		<%					
		if(!((sv.clntname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important; width: 60px;
    text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</div></div></div></div>

<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
<%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%>
<div class="input-group"> 
 		
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important; width: 60px;
    text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</div></div></div></div>

<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
<%=resourceBundleHandler.gettingValueFromBundle("Scheme Number")%>

<table><tr><td style="min-width:60px;">

 		
		<%					
		if(!((sv.schmno.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    		</td><td>
		<%					
		if(!((sv.schmnme.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmnme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.schmnme.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="float: left !important;margin-left: 2px !important;border-radius: 5px !important;height: 25px !important;
    font-size: 12px !important; width: 60px;
    text-align: left;border:1px solid #ccc !important;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td></tr></table>
</div></div></div>
<br><br>
		
<%

			%>
		<%
		GeneralTable sfl = fw.getTable("sr60uscreensfl");
		%>
	 
 <style type="text/css">
.fakeContainer {
	width:600px;		
	height:330px;	
	top: 140px;
	left:4px;			
}
.sSky th, .sSky td{
font-size:12px !important;
}
.sr60uTable tr{height:22px}
</style>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr60uTable", {
				fixedCols : 0,		
				colWidths : [100,150,150,184],
				hasHorizonScroll :"Y",
				moreBtn: "N",	
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-sr60u'>
							<thead>
								<tr class='info'>
 <tr >
		
		<th><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
		         								
		<th><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
	
		<th><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
		
		<th><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>

</tr>
		</thead>
	
	<%
	Sr60uscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr60uscreensfl
	.hasMoreScreenRows(sfl)) {	
%>
	<tr class="tableRowTag" id='<%="tablerow"+count%>' >
		<%if((new Byte((sv.select).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		 <td  
			<%if((sv.select).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="center" <%}%> >
			 					 
					 <input type="checkbox" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr60uscreensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr60uscreensfl.select_R<%=count%>'
						 id='sr60uscreensfl.select_R<%=count%>'
						 onClick="selectedRow('sr60uscreensfl.select_R<%=count%>')"
						 class="UICheck"
						 <%if ((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										disabled="disabled" <%}%>
										<%if (!((sv.select.getFormData()).toString().trim().equalsIgnoreCase("0")
									|| (sv.select.getFormData()).toString().trim().equalsIgnoreCase(""))) {%>
										checked="checked" <%}%> 
					 />
		</td>		 
						
									
		<%}else{%><td>
					</td>														
										
		<%}%>
			
		<%if((new Byte((sv.datefrmDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    <td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">														
						<%= sv.datefrmDisp.getFormData()%>
			</td>
						
		<%}else{%>
			<td >
	
			</td>					
					<%}%>
				
			<%if((new Byte((sv.datetoDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
		    	 <td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">														
						<%= sv.datetoDisp.getFormData()%>
			</td>
				</td>			
					<%}%>
								
	
			
		     <td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
								
									 
								



<%if ((new Byte((sv.totamnt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									<%
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.totamnt).getFieldName());									
									valueThis=smartHF.getPicFormatted(qpsf,sv.totamnt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);							
									
									}else{
										valueThis = null;
										
									} %>
<%=valueThis %>
</td>
				
									
	</tr>

	<%
	count = count + 1;
	Sr60uscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
</table>
</div>
<script>
	$(document).ready(function() {
    	$('#dataTables-sr60u').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: false,
			scrollCollapse: true,
      	});
    });
</script>
	</div></div></div></div>
<br/><div style='visibility:hidden;'><table></table></div><br/></div>

<%@ include file="/POLACommon2NEW.jsp"%>

