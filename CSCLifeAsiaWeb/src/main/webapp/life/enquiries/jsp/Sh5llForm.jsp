

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sh5ll";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.productdefinition.screens.*"%>
<%
	Sh5llScreenVars sv = (Sh5llScreenVars) fw.getVariables();
%>
<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Company ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Table ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Item ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Days");
%>


<%
	{
		if (appVars.ind26.isOn()) {
			sv.day.setReverse(BaseScreenData.REVERSED);
			sv.day.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.day.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<div class="panel panel-default">
	<div class="panel-body">
		<!-- row 1 -->
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					<%
						if (!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.company.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Table")%></label>
					<%
						if (!((sv.tabl.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.tabl.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Item")%></label>
					<table><tr><td>
						<%
							if (!((sv.item.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.item.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td><td>
						<%
							if (!((sv.longdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.longdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
id="idesc" style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Days")%></label>
					
					
					<div class="input-group" style="min-width: 71px;">
						<input name='day' type='text'
							<%if ((sv.day).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right" <%}%>
							<%formatValue = (sv.day.getFormData()).toString();%>
							value='<%= XSSFilter.escapeHtml(formatValue)%>'
							<%if (formatValue != null && formatValue.trim().length() > 0) {%>
							title='<%=formatValue%>' <%}%>
							size='<%=sv.day.getLength()%>'
							maxLength='<%=sv.day.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(day)'
							onKeyUp='return checkMaxLength(this)'
							<%if ((new Byte((sv.day).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
					|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.day).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.day).getColor() == null ? "input_cell"
						: (sv.day).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>
					</div>
				</div>
			</div>
			


<%@ include file="/POLACommon2NEW.jsp"%>


<!-- ILIFE-2577 Life Cross Browser - Sprint 2 D3 : Task 1  -->
<style>
@media \0screen\,screen\9
 {
	.output_cell {
		margin-right: 1px
	}
}

.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<!-- ILIFE-2577 Life Cross Browser - Sprint 2 D3 : Task 1  -->
