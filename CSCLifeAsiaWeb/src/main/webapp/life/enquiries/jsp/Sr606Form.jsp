<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR606";%>
<%@ include file="/POLACommon1.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr606ScreenVars sv = (Sr606ScreenVars) fw.getVariables();%>

<%if (sv.Sr606screenWritten.gt(0)) {%>
	<%Sr606screen.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.Sr606screensflWritten.gt(0)) {%>
	<%GeneralTable sfl = fw.getTable("sr606screensfl");
	savedInds = appVars.saveAllInds();
	Sr606screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 12;
	String height = smartHF.fmty(12);
	smartHF.setSflLineOffset(9);
	%>
	<div style='position: absolute; left: 0%; top: <%=smartHF.fmty(9)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
	<%while (Sr606screensfl.hasMoreScreenRows(sfl)) {%>
	<%sv.rdocnum.setClassString("");%>
<%	sv.rdocnum.appendClassString("string_fld");
	sv.rdocnum.appendClassString("output_txt");
	sv.rdocnum.appendClassString("highlight");
%>
	<%sv.trandateDisp.setClassString("");%>
<%	sv.trandateDisp.appendClassString("string_fld");
	sv.trandateDisp.appendClassString("output_txt");
	sv.trandateDisp.appendClassString("highlight");
%>
	<%sv.zdesc.setClassString("");%>
<%	sv.zdesc.appendClassString("string_fld");
	sv.zdesc.appendClassString("output_txt");
	sv.zdesc.appendClassString("highlight");
%>
	<%sv.acctamt.setClassString("");%>
<%	sv.acctamt.appendClassString("num_fld");
	sv.acctamt.appendClassString("output_txt");
	sv.acctamt.appendClassString("highlight");
%>
	<%sv.chqnum.setClassString("");%>
<%	sv.chqnum.appendClassString("string_fld");
	sv.chqnum.appendClassString("output_txt");
	sv.chqnum.appendClassString("highlight");
%>
	<%sv.screenIndicArea.setClassString("");%>

	<%
{
	}

	%>

		<%=smartHF.getTableHTMLVarQual(sflLine, 2, sfl, sv.rdocnum)%>
		<%=smartHF.getHTMLSFSpaceVar(sflLine, 13, sfl, sv.trandateDisp)%>
		<%=smartHF.getHTMLCalSSVar(sflLine, 13, sfl, sv.trandateDisp)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 27, sfl, sv.zdesc)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 50, sfl, sv.acctamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
		<%=smartHF.getTableHTMLVarQual(sflLine, 71, sfl, sv.chqnum)%>

		<%sflLine += 1;
		doingLine++;
		if (doingLine % linesPerCol == 0 && sflcols > 1) {
			sflLine = 0.0;
		}
		Sr606screensfl.setNextScreenRow(sfl, appVars, sv);
	}%>
	</div>
	<%appVars.restoreAllInds(savedInds);%>


<%}%>

<%if (sv.Sr606protectWritten.gt(0)) {%>
	<%Sr606protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.Sr606screenctlWritten.gt(0)) {%>
	<%Sr606screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("sr606screensfl");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.contdesc.setClassString("");%>
<%	sv.contdesc.appendClassString("string_fld");
	sv.contdesc.appendClassString("output_txt");
	sv.contdesc.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner     ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.cownname.setClassString("");%>
<%	sv.cownname.appendClassString("string_fld");
	sv.cownname.appendClassString("output_txt");
	sv.cownname.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Doc No");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Amount");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cheque No");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"______________________________________________________________________________");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

	<%=smartHF.getLit(3, 3, generatedText2)%>

	<%=smartHF.getHTMLVar(3, 15, fw, sv.chdrnum)%>

	<%=smartHF.getHTMLSpaceVar(3, 26, fw, sv.cnttype)%>
	<%=smartHF.getHTMLF4NSVar(3, 26, fw, sv.cnttype)%>

	<%=smartHF.getHTMLVar(3, 32, fw, sv.contdesc)%>

	<%=smartHF.getLit(4, 3, generatedText3)%>

	<%=smartHF.getHTMLVar(4, 15, fw, sv.cownnum)%>

	<%=smartHF.getHTMLVar(4, 26, fw, sv.cownname)%>

	<%=smartHF.getLit(6, 2, generatedText9)%>

	<%=smartHF.getLit(6, 13, generatedText4)%>

	<%=smartHF.getLit(6, 27, generatedText5)%>

	<%=smartHF.getLit(6, 62, generatedText6)%>

	<%=smartHF.getLit(6, 71, generatedText7)%>

	<%=smartHF.getLit(7, 2, generatedText8)%>





<%}%>
<%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%>

<%@ include file="/POLACommon2.jsp"%>
