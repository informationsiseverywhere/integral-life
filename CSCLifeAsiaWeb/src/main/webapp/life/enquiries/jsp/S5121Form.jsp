<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5121";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S5121ScreenVars sv = (S5121ScreenVars) fw.getVariables();%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage        ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Virtual Fund      Fund Type          Unit Balance     Deemed Unit Bal ");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind80.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind80.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       	<table><tr><td>
						<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' >
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						
							
						  		</td><td style="min-width:1px"></td><td>
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width:40px">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						
							</td><td style="min-width:1px"></td><td>
						  		
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="max-width: 170px;margin-left: 1px;" id="ctypedes">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	 
		       		
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       	<table><tr><td>
		      
					    	<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("cntcurr");
								longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
							%>
							
						    
						   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
												"blank_cell" : "output_cell" %>' > 
						   <%=	(sv.cntcurr.getFormData()).toString()%>
						   </div>
						   </td><td style="min-width:1px"></td><td>
						   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		<%=longValue%>
						   		<%}%>
						   </div>
						   
						   <%
								longValue = null;
								formatValue = null;
								%> 
		       	</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		 
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
		       		<table><tr><td>
		       		
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						
					    
					   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>' > 
					   <%=	(sv.register.getFormData()).toString()%>
					   </div>
					   </td><td style="min-width:1px"></td><td>
					   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   <%
							longValue = null;
							formatValue = null;
							%>
		       	</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		            <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		<div class="input-group">
							<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		     
		       		
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
		       		
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    
		            <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
		       		<div class="input-group">
		       		
							<%	
								qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
								
								if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" />
							
							<% 
								} 
							%>
							
						<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		        
		       		
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		       	<table><tr><td>
		       		
								<%					
								if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' >
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							  		</td><td style="min-width:1px"></td><td>
								<%					
								if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' >
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       	</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		 
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table><tr><td>
		
								<%					
								if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
						  		</td><td style="min-width:1px"></td><td>
								<%					
								if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></label>
		       		<table><tr><td>
		       		
								<%	
												
								if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' >
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
						</td><td style="min-width:1px"> </td><td>
						  		
							<%	
										
								if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rider.getFormData()).toString()); 
														
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.rider.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</td></tr></table>
		       		</div>
		       	</div>
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total_fund_value")%></label>
		       		<div class="input-group">
		       		
							<%	
								qpsf = fw.getFieldXMLDef((sv.fundAmount).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.fundAmount);
								
								if(!((sv.fundAmount.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="text-align: right;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" />
							
							<% 
								} 
							%>
							
						<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>		       	
		    </div>
		    
		    <%
				GeneralTable sfl = fw.getTable("s5121screensfl");
			%>
		    
		    
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5121'>	
			    	
			    	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th colspan="2" style="min-width:100px"><center><%=resourceBundleHandler.gettingValueFromBundle("Virtual Fund")%></center></th>
						<th rowspan="1" style="min-width:130px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Unit Type")%></center></th>
						<th rowspan="1" style="max-width:97px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Unit Balance")%></center></th>
						<th rowspan="1" style="max-width:72px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
						<th rowspan="1" style="max-width:72px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></center></th>
						<th rowspan="1" style="max-width:72px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></center></th>
						<th rowspan="1" style="max-width:72px" ><center><%=resourceBundleHandler.gettingValueFromBundle("Header10")%></center></th>
						</tr>	
			         	</thead>
					    
					    
					    <tbody>
					      
					      	<%
								String backgroundcolor="#FFFFFF";
								S5121screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5121screensfl.hasMoreScreenRows(sfl)) {
								
							%>

							<tr>
						    										
						 				<div style='display:none; visiblity:hidden;'>
						 					<input type='text' 
											maxLength='<%=sv.select.getLength()%>'
											value='<%= sv.select.getFormData() %>' 
											size='<%=sv.select.getLength()%>'
											onFocus='doFocus(this)' onHelp='return fieldHelp(s5121screensfl.select)' onKeyUp='return checkMaxLength(this)' 
											name='<%="s5121screensfl" + "." +
											"select" + "_R" + count %>'
											class = "input_cell"
											  id='s5121screensfl.select_R<%=count%>'
											style = "width: <%=sv.select.getLength()*12%> px;"
											
											>
						  				</div>
											
				    					<td >														
											<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s5121screensfl" + "." +
									      "select" + "_R" + count %>").value="1";  doAction("PFKEY0");'><span><%=sv.unitVirtualFund.getFormData()%></span></a>							 						 		
										</td>
				    									
    									<td><b>								
											<%= sv.fundShortDesc.getFormData()%>
						
									</b>	</td>
				    					
				    					<td>	<b>								
						
                    					<%= sv.fundTypeShortDesc.getFormData()%>
								
									</b>	</td>
				    					
				    					<td style="text-align: right;">	<b>								
																
																							
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.currentUnitBal).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
										
															
											<%
											
											formatValue = smartHF.getPicFormatted(qpsf,sv.currentUnitBal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
											
											
												if(!sv.
												currentUnitBal
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
		
									<b> </td>
				    				
				    					<td style="text-align: right;">	<b>								
													
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.currentDunitBal).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.currentDunitBal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
													currentDunitBal
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
					 			 		
									</b></td>
				    					<td align="left">	<b>								
													<%=sv.effDateDisp.getFormData()%>				 			 		
									</b></td>
				    					<td style="text-align: right;">	<b>								
													
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.uoffpr).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.uoffpr,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
															uoffpr
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
					 			 		
									</b></td>
				    					<td style="text-align: right;">	<b>								
													
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.estval).getFieldName());						
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.estval,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
															estval
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														longValue = null;
														formatValue = null;
												%>
					 			 		
									</b></td>																											
		
								</tr>

									<%
									count = count + 1;
									S5121screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	 </div>
</div>

<!-- <script>
	$(document).ready(function() {
    	$('#dataTables-s5121').DataTable({
    		paging: false,
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
			scrollCollapse: true,
      	});
    });
</script> -->

<%@ include file="/POLACommon2NEW.jsp"%>

 <div style='visibility:hidden;'>
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
		       		<div class="input-group">
					  <%	
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
							
							if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" />
						
						<% 
							} 
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></label>
		       		<div class="input-group">
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    </div>
