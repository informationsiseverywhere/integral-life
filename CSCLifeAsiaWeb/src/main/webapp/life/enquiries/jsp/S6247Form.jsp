<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6247";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6247ScreenVars sv = (S6247ScreenVars) fw.getVariables();%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select  Effective From  Client Name                     Relationship    %");%>
<%		appVars.rollup(new int[] {93});
%>
	
<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 60px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
			  		</td><td style="padding-left:1px;">
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td style="padding-left:1px;">
			  		
						<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 300px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		</div>
		       		
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
		       		<div class="input-group">
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("register");
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
					%><!--
					
				    
				   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
										"blank_cell" : "output_cell" %>'> 
				   <%=	(sv.register.getFormData()).toString()%>
				   </div>
				   
				   --><div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">  
				   		<%if(longValue != null){%>
				   		<%=longValue%>
				   		<%}%>
				   </div>
				   <%
						longValue = null;
						
						%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	 	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       		<div class="input-group">
		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>
						
					    
					   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 50px;"> 
					   <%=	(sv.cntcurr.getFormData()).toString()%>
					   </div><!--
					   
					   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   
					   --><%
							longValue = null;
							
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				</td><td style="padding-left:1px;">
				  		
						<%					
						if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width: 150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td></tr></table>
		       		</div>
		       		</div>
		       
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table><tr><td>
		       		<%					
						if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td style="padding-left:1px;">
			  		
						<%					
						if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <%
		GeneralTable sfl = fw.getTable("s6247screensfl");
		
		%>
		    
		     <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id='dataTables-s5010'>
			    	 	<thead>
			    	 	<tr class='info' height='25px'>									
						<th style="min-width: 200px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th></center>
						<th style="min-width: 120px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th></center>
	               		<th style="min-width: 90px;"><center><%=resourceBundleHandler.gettingValueFromBundle("End Date")%></th></center>
						<th style="min-width: 100px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Bnfcry Type")%></th></center>
						<%if (sv.actionflag.compareTo("N") != 0) {%>
						<th style="min-width: 100px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Sequence")%></th></center>
						<%} %>
						<th style="min-width: 90px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th></center>
						<th style="min-width: 70px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Rev Flg")%></th></center>
						<th style="min-width: 60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Rel To")%></th></center>
						<th style="min-width: 60px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th></center>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
							S6247screensfl.set1stScreenRow(sfl, appVars, sv);
							int count = 1;
							while (S6247screensfl.hasMoreScreenRows(sfl)) {
								
							%>
						<tr>
						    										
								<div style='display:none; visiblity:hidden;'>
									 <input type='text' 
									maxLength='<%=sv.select.getLength()%>'
									value='<%= sv.select.getFormData() %>' 
									size='<%=sv.select.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(s6247screensfl.select)' onKeyUp='return checkMaxLength(this)' 
									name='<%="s6247screensfl" + "." +
									"select" + "_R" + count %>'
									class = "input_cell"
									id='<%="s6247screensfl" + "." +
									"select" + "_R" + count %>'
									 style = "width: <%=sv.select.getLength()*12%> px;"						  
									 >
								</div>
				    			<td align="left">														
								<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6247screensfl" + "." +
						      "select" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.clntsname.getFormData()%></span></a>							 						 		
								</td>
				    			
				    			<td align="left">									
									<%= sv.effdateDisp.getFormData()%>
								</td>
								
								<td align="left">									
									<%= sv.enddateDisp.getFormData()%>
								</td>
								
								<td align="left">									
									<%= sv.bnytype.getFormData()%>
								</td>		

							<!-- FWAMG3 -->
							<%if (sv.actionflag.compareTo("N") != 0) {%>
								<td align="left">
									<%
										longValue = sv.sequence.getFormData().trim();
									%>
										<%=longValue==null?"":longValue%>
									<%
										longValue=null;
									%>
								</td>
								<%} %>
							<!-- FWAMG3 END-->

				    			<td align="left">									
									<%	
									//smalchi2 for ILIFE-597 STARTS UI Issues
										fieldItem=appVars.loadF4FieldsShort(new String[] {"bnyrlndesc"},sv,"E",baseModel);							
										Map bnyrlndescMap = (Map) fieldItem.get("bnyrlndesc");								
										longValue = (String) bnyrlndescMap.get((sv.bnyrlndesc.getFormData()).toString().trim());  
										//ENDS
									%>
									
									<%= sv.bnyrlndesc.getFormData()%>&nbsp;-&nbsp;<%=longValue%>
									<%
										longValue = null;
										bnyrlndescMap = null;
										fieldItem = null;
									%>
								</td>
								
								<td align="left">									
									<%= sv.revcflg.getFormData()%>
								</td>
								
								<td align="left">									
									<%= sv.relto.getFormData()%>
								</td>
				    			
				    			<td align="left">									
									<%	
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.bnypc).getFieldName());						
									qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);				
									%>
									
													
									<%
										formatValue = smartHF.getPicFormatted(qpsf,sv.bnypc);
										if(!sv.
										bnypc
										.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%= formatValue%>
									<%
											longValue = null;
											formatValue = null;
									%>
									</td>
		
								</tr>
							
							
								<%
								count = count + 1;
								S6247screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								mappedItems = null;
								%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		
	 </div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-s6247').DataTable({
    		paging: true,
        	ordering: false,
        	searching: false,
        	scrollX: true,
        	scrollY: "350px",
			scrollCollapse: true
      	});
    });
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

