<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR586";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr586ScreenVars sv = (Sr586ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client");%>
	<%-- <%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number");%> --%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client Name");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Details");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coy");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured");%>
<%		appVars.rollup(new int[] {93});
%>


	<div class="panel panel-default">
    <div class="panel-body">
		<div class="row">
		<div class="col-md-4">
		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
		<table><tr><td>
		<%					
		if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width: 80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  </td><td style="padding-left:1px;">
	


	<%					
		if(!((sv.clntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.clntnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:300px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr></table>
		</div></div>
		
		<br><br>
		
		<%GeneralTable sfl = fw.getTable("sr586screensfl"); %>
<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id='dataTables-sr586' width='100%'>
								<thead>
									<tr class='info'>
										<th><center>  <%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center> </th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
										<!--  IFSU-429 START by nnazeer-->
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
										<!--  IFSU-429 END-->
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
										<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
										
									</tr>
								</thead>
								<tbody>
									<%
										
										Sr586screensfl
										.set1stScreenRow(sfl, appVars, sv);
										int count = 1;
										while (Sr586screensfl
										.hasMoreScreenRows(sfl)) {	
									%>
									
										<tr class="tableRowTag" id='<%="tablerow"+count%>' >
															    									<td  
														<%if((sv.sel).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="center"<% }else {%> align="center" <%}%> >
																												
																										
																						
														
														 					 
														 <input type="radio" 
															 value='<%= sv.sel.getFormData() %>' 
															 onFocus='doFocus(this)' onHelp='return fieldHelp("sr586screensfl" + "." +
															 "sel")' onKeyUp='return checkMaxLength(this)' 
															 name='sr586screensfl.sel_R<%=count%>'
															 id='sr586screensfl.sel_R<%=count%>'
															 onClick="selectedRow('sr586screensfl.sel_R<%=count%>')"
															 class="radio"
														 />
														 
														 					
														
																				
																		</td>
													    									<td 
														<%if((sv.coy).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																											
																
																<%= sv.coy.getFormData()%>
																					 
													
																		</td>
													    									<td  
														<%if((sv.currcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																		
																				
															<%= sv.currcode.getFormData()%>
															
																							 
													
																		</td>
													    									<td 
														<%if((sv.lrkcls).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																		
																				
															<%= sv.lrkcls.getFormData()%>
															
																							 
													
																		</td>
													    									<td 
														<%if((sv.sumins).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																									
																																
														<%	
															sm = sfl.getCurrentScreenRow();
															qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());						
															qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);				
														%>
														
																			
															<%
																formatValue = smartHF.getPicFormatted(qpsf,sv.sumins);
																if(!(sv.sumins).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
																	formatValue = formatValue( formatValue );
																}
															%>
															<%= formatValue%>
															<%
																	longValue = null;
																	formatValue = null;
															%>
														 			 		
												 		
												    				 
													
																		</td>
														
										</tr>
									
										<%
										count = count + 1;
										Sr586screensfl
										.setNextScreenRow(sfl, appVars, sv);
										}
										%>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
					
		<table><tr>
		<div class="sectionbutton">
         <a href="#" onClick="JavaScript:perFormOperation(1)" class="btn btn-info"> <%=resourceBundleHandler.gettingValueFromBundle("Detail")%></a>
         </div>	 
		</tr></table>	


</div></div>

<script>
       $(document).ready(function() {
              $('#dataTables-sr586').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script>  

<!-- <script>
$(document).ready(function() {
	$('#dataTables-sr586').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '350px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script> -->



<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Number")%>
</div>

</tr></table></div>
<%@ include file="/POLACommon2NEW.jsp"%>


