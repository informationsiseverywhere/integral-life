<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6354";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6354ScreenVars sv = (S6354ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured     ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life       ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Ref No   ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mandate Status   ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factoring House  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Code        ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account Number   ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Despatch Addr    ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Details ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Details ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Trustee ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Beneficiaries ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Assignees ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Group Details ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Despatch ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loyalty Bonus ");%>

<%{
		if (appVars.ind10.isOn()) {
			sv.conben.setReverse(BaseScreenData.REVERSED);
			sv.conben.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.conben.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind11.isOn()) {
			sv.asgnind.setReverse(BaseScreenData.REVERSED);
			sv.asgnind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind11.isOn()) {
			sv.asgnind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind14.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind13.isOn()) {
			sv.prmdetails.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind14.isOn()) {
			sv.prmdetails.setEnabled(BaseScreenData.DISABLED);
			sv.prmdetails.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind13.isOn()) {
			sv.prmdetails.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.prmdetails.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			generatedText18.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.hlndetails.setReverse(BaseScreenData.REVERSED);
			sv.hlndetails.setColor(BaseScreenData.RED);
		}
		if (appVars.ind16.isOn()) {
			sv.hlndetails.setEnabled(BaseScreenData.DISABLED);
			sv.hlndetails.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind15.isOn()) {
			sv.hlndetails.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind12.isOn()) {
			sv.grpind.setReverse(BaseScreenData.REVERSED);
			sv.grpind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.grpind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.ctrsind.setReverse(BaseScreenData.REVERSED);
			sv.ctrsind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind12.isOn()) {
			sv.ctrsind.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind18.isOn()) {
			generatedText23.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind17.isOn()) {
			sv.ind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind18.isOn()) {
			sv.ind.setEnabled(BaseScreenData.DISABLED);
			sv.ind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind17.isOn()) {
			sv.ind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.ind.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind20.isOn()) {
			sv.ctrycode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind21.isOn()) {
			sv.cntryState.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		sv.payrnum.setEnabled(BaseScreenData.DISABLED);
		sv.payor.setEnabled(BaseScreenData.DISABLED);
		sv.payrname.setEnabled(BaseScreenData.DISABLED);
		sv.payorname.setEnabled(BaseScreenData.DISABLED);
		
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row" >
	        	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<table><tr><td>
				<%=smartHF.getHTMLVarReadOnly(fw, sv.chdrnum,1)%>
				</td><td style="margin-left:2px;">
  		<%=smartHF.getHTMLVarReadOnly(fw, sv.cnttype,1)%>
  		</td><td style="margin-left:2px;">
  		<%=smartHF.getHTMLVarReadOnly(fw, sv.ctypedes,1)%>
		       		
		       		</td></tr></table></div>
		       	</div>
		       	
		       
		       	
		       	
		       	<div class="col-md-4">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
								<%}%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
								<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
							%>
							
						  		
								<%					
								if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		<div class="input-group">
							<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
								<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 60px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		       		<table><tr><td>
						<%					
							if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
							</td><td>
							<%					
							if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px; margin-left:2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table><tr><td>
							<%					
							if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td>
							<%					
							if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px; margin-left:2px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       
		    </div>
		    <br>
		    <hr>
		    <br>
		    <div class="row">
	        	<div class="col-md-1" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payor")%></label>
		       	<table><tr><td style="min-width:71px">
				  <%=smartHF.getRichTextInputFieldLookup(fw, sv.payor,(sv.payor.getLength()))%>
				  <%=smartHF.getHTMLF4NSVarExt(fw, sv.payor, 1)%>
				  </td><td style="min-width:100px">
		<%=smartHF.getHTMLVarReadOnly(fw, sv.payrname,1)%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	
		       	<div class="col-md-2">
		       
		    
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Bank Code")%></label>
		       		<table><tr><td >
							
							<%					
							if(!((sv.bankkey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankkey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankkey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:71px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					 	</td><td>
							<%					
							if(!((sv.bankdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankdesc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px; margin-left:1px; max-width:150px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment AC No. / Card No.")%></label>
		      <table><tr><td>
		       		
	
							<%					
							if(!((sv.bankacckey.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankacckey.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td>
							<%					
							if(!((sv.bankaccdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bankaccdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px; margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		
		       	
		       		<div class="col-md-2">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Payment Currency")%>
								<%}%></label>
		       		<div class="input-group">
					<%if ((new Byte((sv.bnkcurr).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		
							<%					
							if(!((sv.bnkcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnkcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.bnkcurr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Mandate Ref No.")%></label>
		       		<div class="input-group">

						<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"mandref"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("mandref");
						longValue = (String) mappedItems.get((sv.mandref.getFormData()).toString().trim());  
						%>
					
				    
				   		<div class='<%= ((sv.mandref.getFormData() == null) || ("".equals((sv.mandref.getFormData()).trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;"> 
				   			<%=	(sv.mandref.getFormData()).toString()%>
				   		</div>
		       		</div>
		       		</div>
		       	</div>
		       	
		      


		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Mandate Status")%></label>
		       		<div class="input-group">
							<%					
							if(!((sv.manstatdsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.manstatdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.manstatdsc.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Factoring House")%></label>
		       		<div class="input-group">
	  		
						<%					
						if(!((sv.fcthsedsc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.fcthsedsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.fcthsedsc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>

		       	
		    </div>
		    <br><hr><br>
		    

			
		    <div class="row">
	        	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payee")%></label>
		       		<table><tr><td style="min-width:70px">
					 <%=smartHF.getRichTextInputFieldLookup(fw, sv.payrnum,(sv.payrnum.getLength()))%>
					 </td><td style="min-width:70px">
		<%=smartHF.getHTMLVarReadOnly(fw, sv.payorname,1)%>
		       </td></tr></table>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Bank Code")%></label>
		       		<table><tr><td>
						<%if ((new Byte((sv.bankkey02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.bankkey02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankkey02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankkey02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
					  	</td><td>
					  	
					  	<%if ((new Byte((sv.bankdesc02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.bankdesc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankdesc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankdesc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px; margin-left:1px; max-width:150px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
	
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout  AC No. / Card No.")%></label>
		       		<table><tr><td>
						<%if ((new Byte((sv.bankacckey02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.bankacckey02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankacckey02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankacckey02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 80px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
					  	
					  	</td><td>
					  	<%if ((new Byte((sv.bankaccdsc02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.bankaccdsc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankaccdsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bankaccdsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px; margin-left:1px; max-width: 150px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       
		       		<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Currency")%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.bnkcurr02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.bnkcurr02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bnkcurr02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.bnkcurr02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    

			
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Mandate Ref No.")%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.zmandref).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.zmandref.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.zmandref.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.zmandref.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Mandate Status")%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.manstatdsc02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.manstatdsc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.manstatdsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.manstatdsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Factoring House")%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.fcthsedsc02).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
						
					  		<%					
							if(!((sv.fcthsedsc02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.fcthsedsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
							} else  {
													
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( (sv.fcthsedsc02.getFormData()).toString()); 
								} else {
									formatValue = formatValue( longValue);
								}
										
							}
							%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  	<%}%>
  	
		       		</div>
		       		</div>
		       	</div>
		       	
		  		       	
		    </div>
		    
		    <br><hr><br>
		    

		
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Despatch Addr")%></label>
		       	<div style="text-align:left;padding:1px; border:#5085b5 1px solid;width:345px;height:120px;">
		       	
									       		<%					
									if(!((sv.despnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.despnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.despnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div>
											<%=formatValue%> 
										</div>
									<%
									longValue = null;
									formatValue = null;
									%>
  
									<%					
										if(!((sv.despname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.despname.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.despname.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div style="
    margin-top: -18px;
    padding-left: 70px;
">
												<%=formatValue%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
								

	
  		
									<%					
									if(!((sv.despaddr01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.despaddr01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.despaddr01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div style="padding-top:3px;">
											<%=formatValue%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							

	
  		
								<%					
								if(!((sv.despaddr02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.despaddr02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.despaddr02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div style="padding-top:3px;">
										<%=formatValue%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						


		
							<%					
							if(!((sv.despaddr03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.despaddr03.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.despaddr03.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div style="padding-top:3px;">
									<%=formatValue%>
								</div>		
							<%
							longValue = null;
							formatValue = null;
							%>
					


		
						<%					
						if(!((sv.despaddr04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.despaddr04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.despaddr04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div style="padding-top:3px;">
								<%=formatValue%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				


		
							<%					
							if(!((sv.despaddr05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.despaddr05.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.despaddr05.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div style="padding-top:3px;">
									<%=formatValue%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		      		</div>
		       		</div>
		       	</div>
		       	
		  
		       	
		       		<div class="col-md-2">
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label>
		       		<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Client..")%>
						<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Street..")%>
						<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Address Line 2..")%>
						<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Address Line 3..")%>
						<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Address Line 4..")%>
						<br/>
						<%=resourceBundleHandler.gettingValueFromBundle("Address Line 5..")%>
					</label>
		       		<div class="input-group">
		       		</div>
		       		</div>
		       	</div>
	       	
		    </div>
		  
		
		      <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label> <%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.desppcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.desppcode.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.desppcode.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;"><!-- MTL321 -->
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       			       	<div class="col-md-3">
											<div class="form-group" style="position:relative; left:48px;">
											<%if ((new Byte((sv.cntryState).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>

												<label><%=resourceBundleHandler.gettingValueFromBundle("State")%></label>
												<div style="max-width: 100px;">
													<%
													fieldItem = appVars.loadF4FieldsLong(new String[]{"cntryState"}, sv, "E", baseModel);
														mappedItems = (Map) fieldItem.get("cntryState");
														for(Iterator<Map.Entry<String,String>>it=mappedItems.entrySet().iterator();it.hasNext();){
															Map.Entry<String, String> entry = it.next();
															String entryStr = ((String)entry.getKey()).substring(0,3);
															if(!entryStr.equalsIgnoreCase(sv.ctrycode.toString())){
																it.remove();
															}
														}
														optionValue = makeDropDownList(mappedItems, sv.cntryState.getFormData(), 2, resourceBundleHandler);
														longValue = (String) mappedItems.get((sv.cntryState.getFormData()).toString().trim());
												%>

													<%
													if ((new Byte((sv.cntryState).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
																|| fw.getVariables().isScreenProtected()) {
												if(longValue==null) longValue=""; 
												%>
													<div class='output_cell'>
														<%=longValue%>
													</div>

													<%
													longValue = null;
												%>

													<%
													} else {
												%>
													<%=smartHF.getDropDownExt(sv.cntryState, fw, longValue, "cntryState", optionValue)%>
													<%
													}
												%>

												</div>
												<% } %>
											</div>
										</div>
		       			<div class="col-md-3">
											<div class="form-group">
											
											<%if ((new Byte((sv.ctrycode).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {%>
												<label><%=resourceBundleHandler.gettingValueFromBundle("Country")%></label>
												<div style="width: 210px;">
													<%
													fieldItem = appVars.loadF4FieldsLong(new String[]{"ctrycode"}, sv, "E", baseModel);
														mappedItems = (Map) fieldItem.get("ctrycode");
														optionValue = makeDropDownList(mappedItems, sv.ctrycode.getFormData(), 2, resourceBundleHandler);
														longValue = (String) mappedItems.get((sv.ctrycode.getFormData()).toString().trim());
												%>

													<%
													if ((new Byte((sv.ctrycode).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
																|| fw.getVariables().isScreenProtected()) {
												%>
													<div class='output_cell'>
														<%=longValue%>
													</div>

													<%
													longValue = null;
												%>

													<%
													} else {
												%>
													<%=smartHF.getDropDownExt(sv.ctrycode, fw, longValue, "ctrycode", optionValue)%>
													<%
													}
												%>
												</div>
												<% } %>
											</div>
										</div>
		   </div>
	 </div>
</div>

<style>
		       	#mainareaDiv > div > div.panel.panel-default > div > div:nth-child(18) > div:nth-child(2) > div > div > div
		       	{
		       	width:165px !important;
		       	}
		       	
		       	</style>
 <Div id='mainForm_OPTS' style='visibility:hidden'>
  <%=smartHF.getMenuLink(sv.prmdetails, resourceBundleHandler.gettingValueFromBundle("Premium Details"))%>
<%=smartHF.getMenuLink(sv.hlndetails, resourceBundleHandler.gettingValueFromBundle("Loan Details"))%>
<%=smartHF.getMenuLink(sv.ctrsind, resourceBundleHandler.gettingValueFromBundle("Trustee"))%>
<%=smartHF.getMenuLink(sv.conben, resourceBundleHandler.gettingValueFromBundle("Contract Beneficiaries"))%>
<%=smartHF.getMenuLink(sv.asgnind, resourceBundleHandler.gettingValueFromBundle("Contract Assignees"))%>
<%=smartHF.getMenuLink(sv.grpind, resourceBundleHandler.gettingValueFromBundle("Group Details"))%>
<%=smartHF.getMenuLink(sv.ind, resourceBundleHandler.gettingValueFromBundle("Policy Despatch"))%>
<%=smartHF.getMenuLink(sv.loyaltyind, resourceBundleHandler.gettingValueFromBundle("Loyalty Bonus"))%>


</Div> 
<%@ include file="/POLACommon2NEW.jsp"%>


						<div style='visibility:hidden;'><table>
						<tr style='height:22px;'><td width='188'>


						<%					
							if(!((sv.facthous.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.facthous.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
						</td>
						<td>
						
							<%					
							if(!((sv.mandstat.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mandstat.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mandstat.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
						</td>
						</tr></table>
					</div>
						
						