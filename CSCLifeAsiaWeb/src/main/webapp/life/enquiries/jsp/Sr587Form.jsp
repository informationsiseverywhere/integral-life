

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SR587";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%
	Sr587ScreenVars sv = (Sr587ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client Number");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Client Name");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Risk Class");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Running Total Sum/Ass");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Coy");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "No");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Typ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Covrg");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sum");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Assured");
%>
<%
	appVars.rollup(new int[]{93});
%>


<style>
.input-group.three-controller>.input-group-addon {
	width: 100% !important;
	text-align: left;
}

.input-group.three-controller>.form-control {
	min-width: 50px !important;
	text-align: left;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Client")%>
					</label>
					<%
						}
					%>
					<table><tr><td>
						<%
							if ((new Byte((sv.clntnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td style="padding-left:1px;">

						<%
							if ((new Byte((sv.clntnam).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.clntnam.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnam.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.clntnam.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:200px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
					</td></tr></table>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%
						if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label> <%=resourceBundleHandler.gettingValueFromBundle("Risk Class")%>
					</label>
					<%
						}
					%>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.lrkcls).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.lrkcls.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lrkcls.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lrkcls.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td style="padding-left:1px;">

						<%
							if ((new Byte((sv.ldesc).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.ldesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ldesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.ldesc.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="max-width:150px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group" style="width: 125px;">
					<%
						if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label  style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle("Running Total Sum/Ass")%>
					</label>
					<%
						}
					%>
					<div>
						<%
							if ((new Byte((sv.tranamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							qpsf = fw.getFieldXMLDef((sv.tranamt).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
								formatValue = smartHF.getPicFormatted(qpsf, sv.tranamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);

								if (!((sv.tranamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue(formatValue);
									} else {
										formatValue = formatValue(longValue);
									}
								}

								if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
								formatValue = null;
						%>

						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		<br />
		
		<%
			GeneralTable sfl = fw.getTable("sr587screensfl");
			/* int height;
			if(sfl.count()*27 > 210) {
			height = 210 ;
			} else {
			height = sfl.count()*27;
			}	 */
		%>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<%-- <div id="load-more" class="col-md-offset-10">
						<a class="btn btn-info" href="#" onclick="doAction('PFKey90');"
							style='width: 74px;'> <%=resourceBundleHandler.gettingValueFromBundle("More")%>
						</a>
					</div> --%>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id='dataTables-sr587' width='100%'>
							<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></th>
								</tr>
							</thead>

							<tbody>
								<%
									Sr587screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (Sr587screensfl.hasMoreScreenRows(sfl)) {
								%>
								<tr>
									<%
										if ((new Byte((sv.coy).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.coy).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.coy.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.chdrnum).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.chdrnum.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.cnttyp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.cnttyp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.cnttyp.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.crtable).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.crtable.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.currcode).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.currcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>><%=sv.currcode.getFormData()%>



									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>
									<%
										if ((new Byte((sv.sumins).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
									<td
										<%if ((sv.sumins).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
										align="right" <%} else {%> align="left" <%}%>>
										<%
											sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());
													//qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
										%> <%
 	formatValue = smartHF.getPicFormatted(qpsf, sv.sumins, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER);
 			if (!(sv.sumins).getFormData().toString().trim().equalsIgnoreCase("")) {
 				formatValue = formatValue(formatValue);
 			}
 %> <%=formatValue%> <%
 	longValue = null;
 			formatValue = null;
 %>




									</td>
									<%
										} else {
									%>
									<td></td>

									<%
										}
									%>

								</tr>

								<%
									count = count + 1;
										Sr587screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
		<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
	</div>
	<!-- Close div panel-body -->
</div>
<!-- Close div panel panel-default -->

<script>
$(document).ready(function() {
    var showval= document.getElementById('show_lbl').value;
    var toval= document.getElementById('to_lbl').value;
    var ofval= document.getElementById('of_lbl').value;
    var entriesval= document.getElementById('entries_lbl').value;
    var nextval= document.getElementById('nxtbtn_lbl').value;
    var previousval= document.getElementById('prebtn_lbl').value;
    var dtmessage =  document.getElementById('msg_lbl').value;
    $('#dataTables-sr587').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {
                "next":       nextval,
                "previous":   previousval
            }
        }
    });
})
</script>

<!-- <script>
	$(document).ready(function() {
		$('#dataTables-sr587').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true
		});
		$('#load-more').appendTo($('.col-sm-6:eq(-1)'));
	});
</script> -->

<%@ include file="/POLACommon2NEW.jsp"%>

<%
	if (!cobolAv3.isPagedownEnabled()) {
%>
<script language="javascript">
	window.onload = function() {
		setDisabledMoreBtn();
	}
</script>
<%
	}
%>
