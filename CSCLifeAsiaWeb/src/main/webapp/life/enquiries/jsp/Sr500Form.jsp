
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR500";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr500ScreenVars sv = (Sr500ScreenVars) fw.getVariables();%>

<%if (sv.Sr500screenWritten.gt(0)) {%>
	<%Sr500screen.clearClassString(sv);%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Owner           ");%>
	<%sv.cownnum.setClassString("");%>
<%	sv.cownnum.appendClassString("string_fld");
	sv.cownnum.appendClassString("output_txt");
	sv.cownnum.appendClassString("highlight");
%>
	<%sv.ownername.setClassString("");%>
<%	sv.ownername.appendClassString("string_fld");
	sv.ownername.appendClassString("output_txt");
	sv.ownername.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Owner     ");%>
	<%sv.jowner.setClassString("");%>
<%	sv.jowner.appendClassString("string_fld");
	sv.jowner.appendClassString("output_txt");
	sv.jowner.appendClassString("highlight");
%>
	<%sv.jownername.setClassString("");%>
<%	sv.jownername.appendClassString("string_fld");
	sv.jownername.appendClassString("output_txt");
	sv.jownername.appendClassString("highlight");
%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor           ");%>
	<%sv.payer.setClassString("");%>
<%	sv.payer.appendClassString("string_fld");
	sv.payer.appendClassString("output_txt");
	sv.payer.appendClassString("highlight");
%>
	<%sv.payername.setClassString("");%>
<%	sv.payername.appendClassString("string_fld");
	sv.payername.appendClassString("output_txt");
	sv.payername.appendClassString("highlight");
%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assignees       ");%>
	<%sv.indic.setClassString("");%>
<%	sv.indic.appendClassString("string_fld");
	sv.indic.appendClassString("output_txt");
	sv.indic.appendClassString("highlight");
%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Agent ");%>
	<%sv.servagnt.setClassString("");%>
<%	sv.servagnt.appendClassString("string_fld");
	sv.servagnt.appendClassString("output_txt");
	sv.servagnt.appendClassString("highlight");
%>
	<%sv.servagnam.setClassString("");%>
<%	sv.servagnam.appendClassString("string_fld");
	sv.servagnam.appendClassString("output_txt");
	sv.servagnam.appendClassString("highlight");
%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch          ");%>
	<%sv.servbr.setClassString("");%>
<%	sv.servbr.appendClassString("string_fld");
	sv.servbr.appendClassString("output_txt");
	sv.servbr.appendClassString("highlight");
%>
	<%sv.brchname.setClassString("");%>
<%	sv.brchname.appendClassString("string_fld");
	sv.brchname.appendClassString("output_txt");
	sv.brchname.appendClassString("highlight");
%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bill Ccy ");%>
	<%sv.billcurr.setClassString("");%>
<%	sv.billcurr.appendClassString("string_fld");
	sv.billcurr.appendClassString("output_txt");
	sv.billcurr.appendClassString("highlight");
%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment Method  ");%>
	<%sv.mop.setClassString("");%>
<%	sv.mop.appendClassString("string_fld");
	sv.mop.appendClassString("output_txt");
	sv.mop.appendClassString("highlight");
%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Freq ");%>
	<%sv.payfreq.setClassString("");%>
<%	sv.payfreq.appendClassString("string_fld");
	sv.payfreq.appendClassString("output_txt");
	sv.payfreq.appendClassString("highlight");
%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Instalment ");%>
	<%sv.lastinsdteDisp.setClassString("");%>
<%	sv.lastinsdteDisp.appendClassString("string_fld");
	sv.lastinsdteDisp.appendClassString("output_txt");
	sv.lastinsdteDisp.appendClassString("highlight");
%>
	<%sv.instpramt.setClassString("");%>
<%	sv.instpramt.appendClassString("num_fld");
	sv.instpramt.appendClassString("output_txt");
	sv.instpramt.appendClassString("highlight");
%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Comm Date  ");%>
	<%sv.currfromDisp.setClassString("");%>
<%	sv.currfromDisp.appendClassString("string_fld");
	sv.currfromDisp.appendClassString("output_txt");
	sv.currfromDisp.appendClassString("highlight");
%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next Bill. Date ");%>
	<%sv.nextinsdteDisp.setClassString("");%>
<%	sv.nextinsdteDisp.appendClassString("string_fld");
	sv.nextinsdteDisp.appendClassString("output_txt");
	sv.nextinsdteDisp.appendClassString("highlight");
%>
	<%sv.nextinsamt.setClassString("");%>
<%	sv.nextinsamt.appendClassString("num_fld");
	sv.nextinsamt.appendClassString("output_txt");
	sv.nextinsamt.appendClassString("highlight");
%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date    ");%>
	<%sv.ptdateDisp.setClassString("");%>
<%	sv.ptdateDisp.appendClassString("string_fld");
	sv.ptdateDisp.appendClassString("output_txt");
	sv.ptdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Proposal Date   ");%>
	<%sv.hpropdteDisp.setClassString("");%>
<%	sv.hpropdteDisp.appendClassString("string_fld");
	sv.hpropdteDisp.appendClassString("output_txt");
	sv.hpropdteDisp.appendClassString("highlight");
%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billed To Date  ");%>
	<%sv.btdateDisp.setClassString("");%>
<%	sv.btdateDisp.appendClassString("string_fld");
	sv.btdateDisp.appendClassString("output_txt");
	sv.btdateDisp.appendClassString("highlight");
%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prop Recd Date  ");%>
	<%sv.hprrcvdtDisp.setClassString("");%>
<%	sv.hprrcvdtDisp.appendClassString("string_fld");
	sv.hprrcvdtDisp.appendClassString("output_txt");
	sv.hprrcvdtDisp.appendClassString("highlight");
%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"1st Issue Date  ");%>
	<%sv.hoissdteDisp.setClassString("");%>
<%	sv.hoissdteDisp.appendClassString("string_fld");
	sv.hoissdteDisp.appendClassString("output_txt");
	sv.hoissdteDisp.appendClassString("highlight");
%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"U/W Decision Dt ");%>
	<%sv.huwdcdteDisp.setClassString("");%>
<%	sv.huwdcdteDisp.appendClassString("string_fld");
	sv.huwdcdteDisp.appendClassString("output_txt");
	sv.huwdcdteDisp.appendClassString("highlight");
%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Non-fft option ");%>
	<%sv.znfopt.setClassString("");%>
<%	sv.znfopt.appendClassString("string_fld");
	sv.znfopt.appendClassString("output_txt");
	sv.znfopt.appendClassString("highlight");
%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Iss Date ");%>
	<%sv.hissdteDisp.setClassString("");%>
<%	sv.hissdteDisp.appendClassString("string_fld");
	sv.hissdteDisp.appendClassString("output_txt");
	sv.hissdteDisp.appendClassString("highlight");
%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"------------------------------------------------------------------------------");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Plan Components ");%>
	<%sv.ind01.setClassString("");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Loan  ");%>
	<%sv.ind02.setClassString("");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Trans. History  ");%>
	<%sv.ind03.setClassString("");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payment History ");%>
	<%sv.ind04.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind10.isOn()) {
			generatedText15.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.numpols.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind23.isOn()) {
			sv.ind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind21.isOn()) {
			sv.ind01.setReverse(BaseScreenData.REVERSED);
			sv.ind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind21.isOn()) {
			sv.ind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind24.isOn()) {
			sv.ind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
			sv.ind02.setReverse(BaseScreenData.REVERSED);
			sv.ind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.ind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind25.isOn()) {
			sv.ind03.setReverse(BaseScreenData.REVERSED);
			sv.ind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind25.isOn()) {
			sv.ind03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind26.isOn()) {
			sv.ind04.setReverse(BaseScreenData.REVERSED);
			sv.ind04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind26.isOn()) {
			sv.ind04.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>


<%}%>



<div class="panel panel-default">    	
    	<div class="panel-body">     
			<div class="row">	
			    <div class="col-md-3"> 
                   		 <div class="form-group"style="max-width:250px;"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>                    
                        <div class="input-group three-controller">
                         	<%=smartHF.getHTMLVarExt( fw, sv.chdrnum)%>
							<%=smartHF.getHTMLSpaceVar( fw, sv.cnttype)%>
							<%=smartHF.getHTMLF4NSVar( fw, sv.cnttype)%>
							<%=smartHF.getHTMLVarExt( fw, sv.ctypedes)%>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> 
				 </div>
                
                <div class="col-md-2" > 
                   	<div class="form-group" style="max-width:50px;"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>                    
                        	
                          		<%=smartHF.getHTMLSpaceVar( fw, sv.cntcurr)%>
								<%=smartHF.getHTMLF4NSVar( fw, sv.cntcurr)%>
                        	
                    </div>
                </div>
                </div>
                <div class="row">	
			    <div class="col-md-2" > 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>                    
                        	<%=smartHF.getHTMLVarExt(fw, sv.chdrstatus)%>
                    </div>
                </div>
                <div class="col-md-3"> 
                
				 </div>
                
                <div class="col-md-2" > 
                   	<div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status  ")%></label>                    
                        	<%=smartHF.getHTMLVarExt( fw, sv.premstatus)%>
                    </div>
                </div>
                <div class="col-md-2"> 
                
				 </div>
                  <div class="col-md-2 " style="max-width:100px;" > 
                   	<div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Register ")%></label>                    
                        	<%=smartHF.getHTMLSpaceVar(fw, sv.register)%>
							<%=smartHF.getHTMLF4NSVar( fw, sv.register)%>
                    </div>
                </div>
                </div>
                
                 <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured ")%></label> 
                   		 	<div class="input-group">                    
                        	<%=smartHF.getHTMLVarExt( fw, sv.lifenum)%>
							<%=smartHF.getHTMLVarExt( fw, sv.lifename)%>
                    		</div>
               		 	</div>
					</div>
                </div>
                
                <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life ")%></label> 
								<div class="input-group">                   
	                        		<%=smartHF.getHTMLVarExt( fw, sv.jlife)%>
									<%=smartHF.getHTMLVarExt( fw, sv.jlifename)%>
                    			</div>
               			 </div>
					</div>
                </div>
                
                   <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Owner ")%></label>     
                   		 <div class="input-group">                
	                        		<%=smartHF.getHTMLVarExt( fw, sv.cownnum)%>
									<%=smartHF.getHTMLVarExt(fw, sv.ownername)%>
                    	</div>
               		 </div>
				</div>
                </div>
                
                   <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Payor ")%></label>   
                   		 <div class="input-group">                  
	                        		<%=smartHF.getHTMLVar( fw, sv.payer)%>
								   <%=smartHF.getHTMLVar( fw, sv.payername)%>
                    	</div>
               		 </div>
					</div>
                </div>
                
                   <div class="row">	
			   		 <div class="col-md-2" style="max-width:50px;"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Assignees ")%></label> 
                   		                    
	                        		<%=smartHF.getHTMLVarExt( fw, sv.indic)%>
                    	</div>
               		 </div>
                </div>
                
                 <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Agent  ")%></label> 
                   		 <div class="input-group">                    
	                        		<%=smartHF.getHTMLVarExt( fw, sv.servagnt)%>
									<%=smartHF.getHTMLVarExt(fw, sv.servagnam)%>
                    	</div>
               		 </div>
                </div>
              </div>  
                <div class="row">	
			   		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Branch ")%></label>  
                   		  <div class="input-group">                  
	                        		<%=smartHF.getHTMLVarExt( fw, sv.servbr)%>
									<%=smartHF.getHTMLVarExt( fw, sv.brchname)%>
                    	</div>
               		 </div>
                </div>
               </div>
                <div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan ")%></label>                    
	                        		<%=smartHF.getHTMLVarExt( fw, sv.numpols, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)%>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 </div>
               		 <div class="col-md-2" style="max-width:100px;"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Bill Ccy ")%></label>                    
	                        		<%=smartHF.getHTMLSpaceVar( fw, sv.billcurr)%>
									<%=smartHF.getHTMLF4NSVar( fw, sv.billcurr)%>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 </div>
               		 <div class="col-md-2"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method  ")%></label>                    
	                        		<%=smartHF.getHTMLSpaceVar(fw, sv.mop)%>
								<%=smartHF.getHTMLF4NSVar( fw, sv.mop)%>
                    	</div>
               		 </div>
                </div>
                <div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Freq ")%></label>                    
	                        	<%=smartHF.getHTMLVarExt( fw, sv.payfreq)%>	
                    	</div>
               		 </div>
               		 
               	</div>
               	
               	<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Last Instalment")%></label>   
                   		  <div class="input-group">                  
	                        		<%=smartHF.getHTMLSpaceVar( fw, sv.lastinsdteDisp)%>
									<%=smartHF.getHTMLCalNSVar( fw, sv.lastinsdteDisp)%>
									<%=smartHF.getHTMLVar( fw, sv.instpramt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 
               		 </div>
               		 
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Risk Comm Date")%></label>   
                   		  <div class="input-group">                  
	                        			<%=smartHF.getHTMLSpaceVar( fw, sv.currfromDisp)%>
										<%=smartHF.getHTMLCalNSVar( fw, sv.currfromDisp)%>
						</div>
                    	</div>
               		 </div>
               	</div>
               	<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Next Bill. Date ")%></label>   
                   		  <div class="input-group">                  	                        		
							<%=smartHF.getHTMLSpaceVar(fw, sv.nextinsdteDisp)%>
							<%=smartHF.getHTMLCalNSVar(fw, sv.nextinsdteDisp)%>
							<%=smartHF.getHTMLVarExt( fw, sv.nextinsamt, COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
						</div>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 
               		 </div>
               		 
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date ")%></label>   
                   		  <div class="input-group">                  
	                        			<%=smartHF.getHTMLSpaceVar( fw, sv.currfromDisp)%>
										<%=smartHF.getHTMLCalNSVar( fw, sv.currfromDisp)%>
						</div>
                    	</div>
               		 </div>
               	</div>
               	
               	  	<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Proposal Date ")%></label>   
                   		  <div class="input-group">                                          		
								<%=smartHF.getHTMLSpaceVar( fw, sv.hpropdteDisp)%>
								<%=smartHF.getHTMLCalNSVar( fw, sv.hpropdteDisp)%>
						</div>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 
               		 </div>
               		 
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date   ")%></label>   
                   		  <div class="input-group">                  
	                        			<%=smartHF.getHTMLSpaceVar( fw, sv.currfromDisp)%>
										<%=smartHF.getHTMLCalNSVar( fw, sv.currfromDisp)%>
						</div>
                    	</div>
               		 </div>
               	</div>
               	
               		<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Prop Recd Date  ")%></label>   
                   		  <div class="input-group">                                          		
							<%=smartHF.getHTMLSpaceVar( fw, sv.hprrcvdtDisp)%>
							<%=smartHF.getHTMLCalNSVar(fw, sv.hprrcvdtDisp)%>
						</div>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 
               		 </div>
               		 
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("1st Issue Date  ")%></label>   
                   		  <div class="input-group">                  
	                        			<%=smartHF.getHTMLSpaceVar( fw, sv.hoissdteDisp)%>
									<%=smartHF.getHTMLCalNSVar(fw, sv.hoissdteDisp)%>
						</div>
                    	</div>
               		 </div>
               	</div>
               	
               	<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("U/W Decision Dt  ")%></label>   
                   		  <div class="input-group">                                          		
							<%=smartHF.getHTMLSpaceVar( fw, sv.huwdcdteDisp)%>
								<%=smartHF.getHTMLCalNSVar( fw, sv.huwdcdteDisp)%>
						</div>
                    	</div>
               		 </div>
               		 <div class="col-md-2"> 
               		 
               		 </div>
               		 
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("1st Issue Date  ")%></label>   
                   		  <div class="input-group">                  
	                        		<%=smartHF.getHTMLSpaceVar( fw, sv.znfopt)%>
									<%=smartHF.getHTMLF4NSVar( fw, sv.znfopt)%>
						</div>
                    	</div>
               		 </div>
               		  <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Iss Date ")%></label>   
                   		  <div class="input-group">                  
	                        	
							<%=smartHF.getHTMLSpaceVar( fw, sv.hissdteDisp)%>
							<%=smartHF.getHTMLCalNSVar(fw, sv.hissdteDisp)%>
						</div>
                    	</div>
               		 </div>
               	</div>
               		<div class="row">	
			   		 <div class="col-md-6"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("------------------------------------------------------------------------------  ")%></label>   
                   		  
                    	</div>
               		 </div>
               		 </div>
               		 
               		<div class="row">	
			   		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Plan Components ")%></label>   
                   		
                    	</div>
               		 </div>
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Loan ")%></label>   
                   		  
                    	</div>
               		 </div>
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Trans. History ")%></label>   
                   		  
                    	</div>
               		 </div>
               		 <div class="col-md-3"> 
                   		 <div class="form-group"> 
                   		 <label><%=resourceBundleHandler.gettingValueFromBundle("Payment History")%></label>   
                   		  
                    	</div>
               		 </div>
               		 </div>
          </div>
 </div>
               
<%if (sv.Sr500protectWritten.gt(0)) {%>
	<%Sr500protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>



<%@ include file="/POLACommon2NEW.jsp"%>
