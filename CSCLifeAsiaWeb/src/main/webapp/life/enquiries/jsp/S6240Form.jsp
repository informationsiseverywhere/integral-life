<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6240";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%
	S6240ScreenVars sv = (S6240ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}
%>

<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"* - indicates Client Role is no longer current");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Sel  Number    Name");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Role");
%>
<%
	appVars.rollup(new int[]{93});
%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled
								style="max-width: 100px;"> <%
 	longValue = null;
 	formatValue = null;
 %>

							</td>
							<td style="min-width: 1px"></td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <%-- <div class="input-group-addon"><%=formatValue%></div> --%>
								<input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled style="max-width: 60px;">
								<%
									longValue = null;
									formatValue = null;
								%>

							</td>
							<td style="min-width: 1px"></td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <%-- <div class="input-group-addon" style="max-width:900px;"><%=formatValue%></div> --%>
								<input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled
								style="min-width: 100px;"> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
				<%
					fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
					mappedItems = (Map) fieldItem.get("register");
					longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
				%>
				<%
					if (!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) {

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.register.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}

					} else {

						if (longValue == null || longValue.equalsIgnoreCase("")) {
							formatValue = formatValue((sv.register.getFormData()).toString());
						} else {
							formatValue = formatValue(longValue);
						}

					}
				%>
				<input class="form-control" type="text"
					placeholder='<%=formatValue%>' disabled style="max-width: 150px;">
				<%
					longValue = null;
					formatValue = null;
				%>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					<%
						if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<input class="form-control" type="text"
						placeholder='<%=formatValue%>' disabled style="max-width: 150px;">
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					<%
						if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<input class="form-control" type="text"
						placeholder='<%=formatValue%>' disabled style="max-width: 150px;">
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<%
						if (!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.cntcurr.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.cntcurr.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<input class="form-control" type="text"
						placeholder='<%=formatValue%>' disabled style="min-width: 60px;">
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled style="max-width: 80px;">
								<%
									longValue = null;
									formatValue = null;
								%>
							</td>
							<td style="min-width: 1px"></td>
							<%-- <td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <div class="input-group-addon" style="min-width:400px;"><%=formatValue%></div>
								<input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled
								style="min-width: 100px;"> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td> --%>
							<td><div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%></div></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled
								style="max-width: 100px;"> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td style="min-width: 1px"></td>
							<%-- <td>
								<%
									if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%> <div class="input-group-addon" style="min-width:300px;"><%=formatValue%></div>
								<input class="form-control" type="text"
								placeholder='<%=formatValue%>' disabled
								style="max-width: 100px;"> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td> --%>
							<td><div style="min-width:100px;max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.jlifename, true)%></div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6240' width='100%'>
						<thead>
							<tr class='info'>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></th>
							</tr>
						</thead>
						<tbody>
							<%
								GeneralTable sfl = fw.getTable("s6240screensfl");
								S6240screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S6240screensfl.hasMoreScreenRows(sfl)) {
							%>
							<tr>
								<td>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' style="font-weight: bold;"
											maxLength='<%=sv.select.getLength()%>'
											value='<%=sv.select.getFormData()%>'
											size='<%=sv.select.getLength()%>' onFocus='doFocus(this)'
											onHelp='return fieldHelp(s6240screensfl.select)'
											onKeyUp='return checkMaxLength(this)'
											name='<%="s6240screensfl" + "." + "select" + "_R" + count%>'
											id='<%="s6240screensfl" + "." + "select" + "_R" + count%>'>
									</div> <a href="javascript:;" class='tableLink'
									onClick='document.getElementById("<%="s6240screensfl" + "." + "select" + "_R" + count%>").value="1"; doAction("PFKEY0");'><span><%=sv.clntnum.getFormData()%></span></a>
								</td>
								<td style="font-weight: bold; padding-top: 11px;" width="60%"><%=sv.clntname.getFormData()%></td>
								<td style="font-weight: bold; padding-top: 11px;"><%=sv.clrole.getFormData()%><%= sv.delFlag.getFormData()%></td>
							</tr>
							<%
								count = count + 1;
									S6240screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Indicates Client Role is no longer current")%></label>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		$('#dataTables-s6240').DataTable({
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollCollapse : true,
			scrollX : true,
			paging : false,
			info : false
		});
	});
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

