

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SA575";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%-- <%=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())%> --%>
<%
	Sa575ScreenVars sv = (Sa575ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>

<%
	{
		
	}
%>


<%
	appVars.rollup(new int[]{93});
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Payer")%></label>

				<table>
					<tR>
						<td>
							<%
								if (!((sv.payer.getFormData()).toString()).trim().equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payer.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.payer.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %> <%
 	if (!((sv.payername.getFormData()).toString()).trim().equalsIgnoreCase("")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.payername.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.payername.getFormData()).toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>


						</td>
						<td>
							<!-- ICIL-1359 --><div style="min-width:91px"
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
 	longValue = null;
 	formatValue = null;
 %>
						</td>
					</tR>
				</table>

			</div>
		</div>
		<%
			GeneralTable sfl = fw.getTable("sa575screensfl");
			/* int height;
			if(sfl.count()*27 > 360) {
			height = 360 ;
			} else {
			height = sfl.count()*27;
			}
			 */
		%>
		<script type="text/javascript">
     /*  $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      }); */
</script>

		<style type="text/css">
.fakeContainer {
	width: 720px;
	height: 300px; /*ILIFE-2143*/
	top: 150px;
	left: 4px;
}

.sSky th, .sSky td {
	font-size: 12px !important;
}

.sa575Table tr {
	height: 35px
}

.seqno {
	text-align: center;
}
</style>
		<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sa575Table", {
				fixedCols : 0,					
				colWidths : [70,80,100,100,74,180,100],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>
		/moreButton.gif",
										isReadOnlyFlag : true

									});

						});
	</script>
		<script>
		function isNumeric(evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode >= 48 && charCode <= 57) {
				return true;
			} else {
				return false;
			}
		}
	</script>

		<div class="row" style="margin-top: 40px;">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-sa575' width='100%'>
						<thead>
							<tr class='info'>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Debit / Credit")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Debit/Credit Date")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Bank Code")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Account#/Ccard#")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Status")%></center></th>
								<th width="9%"><center><%=resourceBundleHandler.gettingValueFromBundle("Trans Code")%></center></th>
								<th width="9%" style="white-space: nowrap;"><center><%=resourceBundleHandler.gettingValueFromBundle("Trans Description")%></center></th>	<!-- ICIL-1359 -->
								<th width="9%" style="white-space: nowrap;"><center><%=resourceBundleHandler.gettingValueFromBundle("Trans Eff Date")%></center></th>		<!-- ICIL-1359 -->
							</tr>
						</thead>

						<tbody>


							<%
								String backgroundcolor = "#FFFFFF";

								Sa575screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (Sa575screensfl.hasMoreScreenRows(sfl)) {
							%>

							<%
								{
										
								}
							%>



							<tr style="background:<%=backgroundcolor%>;">

								<td class="seqno"></td>
								<td><%=sv.dbtcdtdesc.getFormData()%></td>
								<td><%=sv.dbtcdtdateDisp.getFormData()%></td>
								<%
									if ((new Byte((sv.amnt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<td>
									<%
										sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.amnt).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
 						formatValue = smartHF.getPicFormatted(qpsf, sv.amnt,
 									COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 					if (!(sv.amnt).getFormData().toString().trim().equalsIgnoreCase("")) {
 						formatValue = formatValue(formatValue);
 					}
										 %> <%=formatValue%> <%
										 	longValue = null;
										 			formatValue = null;
										 %>
								</td>
								<%
									} else {
								%>
								<td></td>
								<%
									}
								%>

								<td><%=sv.bankcode.getFormData()%></td>
								<td><%=sv.aacct.getFormData()%></td>
								<td><%=sv.statdesc.getFormData()%></td>
								<td><%=sv.transcode.getFormData()%></td>
								<td style="white-space: nowrap;"><%=sv.trcdedesc.getFormData()%></td>	<!-- ICIL-1359 -->
								<td><%=sv.trdateDisp.getFormData()%></td>

							</tr>


							<%
								if (backgroundcolor.equalsIgnoreCase("#FFFFFF")) {
										backgroundcolor = "#ededed";
									} else {
										backgroundcolor = "#FFFFFF";
									}
									count = count + 1;
									Sa575screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
						</tbody>
					</table>
				</div>
				<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
				<input type="text" style="visibility: hidden;height: 3px !important;" id="msg_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Datatablemsg")%>">
			</div>
		</div>


		<script>
$(document).ready(function() {
	$('#dataTables-sa575').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300',
        scrollCollapse: true,
        paging:   true,		
        info:     false,       
        orderable: false,
        language: {
            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,
            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
            "sInfoEmpty": showval+" " +"0 "+ toval+" " +"0 "+ ofval+" " +"0 "+ entriesval,
            "sEmptyTable": dtmessage,
            "paginate": {
                "next":       nextval,
                "previous":   previousval
            }
        }
  	});
});

$(document).ready(function() {

	var sqlfeild = $('.seqno');
	var count=1;
	for(var i=0; i<sqlfeild.length;i++){
		
			sqlfeild[i].innerHTML=count;
			count++;
		}
	
});

</script>


		<div></div>
		<%@ include file="/POLACommon2NEW.jsp"%>