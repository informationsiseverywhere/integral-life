<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6242";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6242ScreenVars sv = (S6242ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Number     ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien Code   ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. Fund  ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section  ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-section ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured         ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Single Prem   ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Date   ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Inst. Premium ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage RCD    ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Date      ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann Proc Date   ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Bonus Date          ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ind    ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate Date     ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate from Date         ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit bill    ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms Indicator  ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl.Method ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Details ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefits Schedule        ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ttl Prem w/Tax ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tax Detail ");%>
<%{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText8.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.benBillDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind34.isOn()) {
			generatedText9.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.unitStatementDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			generatedText30.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.bonusInd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind36.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			sv.payflag.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind40.isOn()) {
			sv.payflag.setInvisibility(BaseScreenData.INVISIBLE);
			sv.payflag.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind42.isOn()) {
			sv.payflag.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind42.isOn()) {
			sv.payflag.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind40.isOn()) {
			generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind41.isOn()) {
			sv.optsmode.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind40.isOn()) {
			sv.optsmode.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optsmode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind41.isOn()) {
			sv.optsmode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind41.isOn()) {
			sv.optsmode.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind38.isOn()) {
			generatedText34.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind38.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind43.isOn()) {
			generatedText35.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind46.isOn()) {
			generatedText36.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.taxind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind46.isOn()) {
			sv.taxind.setEnabled(BaseScreenData.DISABLED);
			sv.taxind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.taxind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) {
			sv.taxind.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//ILIFE-3399-STARTS
		if (appVars.ind47.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind49.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind48.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-ENDS
		if (appVars.ind50.isOn()) {   //ILIFE-3421
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
		
		/* START OF ILIFE-4172 */
		if (appVars.ind40.isOn()) {
			sv.zsredtrm.setInvisibility(BaseScreenData.INVISIBLE);
			sv.zsredtrm.setEnabled(BaseScreenData.DISABLED);
		}
		/* END OF ILIFE-4172 */
		/*ILIFE-6968 start*/
		if (!appVars.ind121.isOn()) {
			sv.lnkgno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind122.isOn()) {
			sv.lnkgsubrefno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6968 end*/
		/* ILIFE-7118-starts */
		if (appVars.ind77.isOn()) {
			sv.tpdtype.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* ILIFE-7118-ends */
		if (appVars.ind62.isOn()) {
			sv.lnkgind.setInvisibility(BaseScreenData.INVISIBLE);
		}

		
		if (appVars.ind21.isOn()) {
			sv.aepaydet.setReverse(BaseScreenData.REVERSED);
			sv.aepaydet.setColor(BaseScreenData.RED);
		}
		if (appVars.ind22.isOn()) {
			sv.aepaydet.setEnabled(BaseScreenData.DISABLED);			
		}
		if (!appVars.ind21.isOn()) {
			sv.aepaydet.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.aepaydet.setInvisibility(BaseScreenData.INVISIBLE);
		}

		//ILIFE-7746
		if(appVars.ind63.isOn()){
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-45 - START
		if (appVars.ind80.isOn()) {
			sv.crrcdDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* if (appVars.ind81.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		if (appVars.ind82.isOn()) {
			sv.riskCessDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-45 - END

	}
			%>

<!-- ILIFE-7746 -->
<style>
	#stampduty{
		width: 145px !important;
	}
</style>

<div class="panel panel-default">
	<div class="panel-body">
	
		<div class="row">	
				
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
					<tr>
					<td>
						<%					
						if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width: 70px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td>
						
						<td>
						
						<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="min-width: 30px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td>
						
						<td>
										<%					
						if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'	style="max-width:300px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
					</td>
					
					</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2" style="text-align: left;">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Contract Status")%></label>
					
						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-2" style="text-align: left;">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Premium Status")%></label>
					
						<%
							if (!((sv.premstatus.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-2" style="text-align: left;">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "cntcurr" },
									sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData())
									.toString().trim());
						%>



						<%
							if (!((sv.cntcurr.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cntcurr.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>' id="cntdesc"
							style="min-width: 120px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
				
				</div>
			</div>

			<div class="col-md-2" style="text-align: left;">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					
						<%
							fieldItem = appVars.loadF4FieldsLong(new String[] { "register" },
									sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData())
									.toString().trim());
						%>


						<%
							if (!((sv.register.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.register.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Number of Policies in Plan")%></label>
					
						<%
							if (!((sv.numpols.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numpols.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.numpols.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "padding-right:50px;"
					: "min-width: 80px;"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
				
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Policy Number")%></label>
					<div class="input-group">
						<%
							qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

							if (!((sv.planSuffix.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 80px;">&nbsp;</div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Number")%></label>
					<div class="input-group">
						<%
							if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase(
									"")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.life.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Coverage No")%></label>
					<div class="input-group">
						<%
							if (!((sv.coverage.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.coverage.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
					<div class="input-group">
						<%
							if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase(
									"")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rider.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Stat. Fund")%></label>
					<div class="input-group">
						<%
							fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" },
									sv, "E", baseModel);
							mappedItems = (Map) fieldItem.get("statFund");
							longValue = (String) mappedItems.get((sv.statFund.getFormData())
									.toString());
						%>


						<div
							class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%
								if (longValue != null) {
							%>

							<%=longValue%>

							<%
								}
							%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
					<div class="input-group">
						<%
							if (!((sv.statSect.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Sub-Section")%></label>
					<div class="input-group">
						<%
							if (!((sv.statSubsect.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSubsect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Assured")%></label>
					<table>
					<tr>
					<td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					<td>

						<%
							if (!((sv.lifename.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>' style="margin-left: 1px;max-width: 200px;min-width: 10px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4"></div>

			<div class="col-md-2">
				<div class="form-group">
					<label> <%
 	if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
 			BaseScreenData.INVISIBLE)) != 0) {
 %> <%
 	if (!((sv.zagelit.getFormData()).toString()).trim()
 				.equalsIgnoreCase("")) {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.zagelit.getFormData())
 						.toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		} else {

 			if (longValue == null || longValue.equalsIgnoreCase("")) {
 				formatValue = formatValue((sv.zagelit.getFormData())
 						.toString());
 			} else {
 				formatValue = formatValue(longValue);
 			}

 		}
 %> <%=resourceBundleHandler
						.gettingValueFromBundle(sv.zagelit.getFormData())%> <%
							longValue = null;
								formatValue = null;
						%> <%
 	}
 %>
					</label>
					<div class="input-group">

						<%
							qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

							if (!((sv.anbAtCcd.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue(formatValue);
								} else {
									formatValue = formatValue(longValue);
								}
							}

							if (!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell" style="min-width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="min-width: 80px;"></div>

						<%
							}
						%>
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Joint Life")%></label>
					<table>
					<tr>
					<td>
						
						<%
		       			if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase(
		       					"")) {

		       				if (longValue == null || longValue.equalsIgnoreCase("")) {
		       					formatValue = formatValue((sv.jlife.getFormData())
		       							.toString());
		       				} else {
		       					formatValue = formatValue(longValue);
		       				}

		       			} else {

		       				if (longValue == null || longValue.equalsIgnoreCase("")) {
		       					formatValue = formatValue((sv.jlife.getFormData())
		       							.toString());
		       				} else {
		       					formatValue = formatValue(longValue);
		       				}

		       			}

		       			if (!formatValue.trim().equalsIgnoreCase("")) {
		       		%>
						<div class="output_cell" style="min-width:71px;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
								} else {
							%>

						<div class="blank_cell" style="min-width:71px;"></div>

						<% 
								} 
							%>
						<%
							longValue = null;
							formatValue = null;
							%>
						</td>
						<td>

						<%					
							if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
							
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="min-width:71px;max-width: 200px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>

					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		
		<%-- IBPLIFE-2139 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  
	<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#sum_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured / Premium")%></label></a>
						</li>
						<li><a href="#cover_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></label></a>
						</li>
					</ul>
				</div>   
		</div>       
		
		<div class="tab-content">
			<div class="tab-pane fade in active" id="sum_tab">
	<%} %>
<%-- IBPLIFE-2139 end --%>
		
		
		
		
		
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
					<div class="input-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"
							style="text-align: right; width: 150px;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 150px;"></div>

						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				
				<% //ILJ-386
					   if (sv.cntEnqScreenflag.compareTo("Y") == 0){ %>
						<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>		
					<%}else if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage RCD"))%></label>
        			<%} %>
        			<!-- ILJ-45 End -->
					
						<%					
							if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
				
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
					<div class="input-group">
						<%		
							fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("mortcls");
							longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim()); 
										
							if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="min-width: 120px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%></label>
					
						<%					
							if(!((sv.liencd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
					
				</div>
			</div>
			<%						
			if ((new Byte((sv.lnkgind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			   <div class="col-md-3">	  
	                   		 <div class="form-group">                    
		                      			<label><%=resourceBundleHandler.gettingValueFromBundle("Linked TPD")%></label>
		                      			
		                      			<input type='checkbox' name='lnkgind' value='Y' onFocus='doFocus(this)' onHelp='return fieldHelp(lnkgind)' onKeyUp='return checkMaxLength(this)' disabled    
								<%													
										if((sv.lnkgind).toString().trim().equalsIgnoreCase("Y")){
											%>checked
										
								      <% }%>
								class ='UICheck' onclick="handleCheckBox('lnkgind')"/>															
										                      			
		                      </div>
	         			</div>
				<%
				}
			%>	
			<!-- ILIFE-7118-starts -->
			<div class="col-md-3">
				<div class="form-group">
					<%
					if ((new Byte((sv.tpdtype).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0) {
				%>
					<label><%=resourceBundleHandler.gettingValueFromBundle("TPD Type")%></label>
					
						<%					
							if(!((sv.tpdtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.tpdtype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.tpdtype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
					<%}%>
				</div>
			</div>
			<!-- ILIFE-7118-ends -->			
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<!-- ILJ-45 Starts -->
               		<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
               		<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
        			<%} else { %>
        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
        			<%} %>
        			<!-- ILJ-45 End -->
					<table>
					<tr>
					<td>
						<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
						<%if(sv.riskCessAge.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 60px; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</td>
					<td style="padding-left: 2px;">

						<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm,( sv.riskCessTerm.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
						<%if(sv.riskCessTerm.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 60px;\' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</td>
					</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<!-- ILJ-45 Starts -->
				<%-- <% if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
				<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation Date")%></label>
				<%} else { %>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cessation Date"))%></label>
				<%} %>
				<!-- ILJ-45 End -->
					<div class="input-group">
						<%					
						if(!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Ann Proc Date")%></label>
					
						<%					
						if(!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> <%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%>
						<%}%>
					</label>
					
						<%					
							if(!((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.select.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.select.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
						<div
							class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
							%>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label> <%
							if ((new Byte((generatedText17).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%> <%=resourceBundleHandler
						.gettingValueFromBundle("Prem cess Age / Term")%> <%
							}
						%>
					</label>
					<table>
					<tr>
					<td>

						<%
							if (((BaseScreenData) sv.premCessAge) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.premCessAge,
						(sv.premCessAge.getLength() + 1), null).replace(
						"absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.premCessAge) instanceof DecimalData) {
						%>
						<%
							if (sv.premCessAge.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'output_cell \' style=\'width: 60px; \' ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>

						</td>
						<td style="padding-left: 2px;">
						
						<%
							if (((BaseScreenData) sv.premCessTerm) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.premCessTerm,
						(sv.premCessTerm.getLength() + 1), null).replace(
						"absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.premCessTerm) instanceof DecimalData) {
						%>
						<%
							if (sv.premCessTerm.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'blank_cell\' style=\'width: 60px; \' ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Premium Cessation Date")%></label>
					
						<%
							if (!((sv.premcessDisp.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premcessDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premcessDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Rerate Date")%></label>
					
						<%
							if (!((sv.rerateDateDisp.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rerateDateDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rerateDateDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				 <%
					if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
					%> 
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
					<%}%>
						<%	
						if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("prmbasis");
						optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
					%>

						<% if ((longValue == null)||("".equals(longValue.trim()))) {
									%>
						<div class="output_cell" style="width:100px;">
							<%if(longValue != null){%>
							<%= longValue%>
							<%}%>
						</div>
						<%
										} else {
									%>
						<div class="blank_cell" style="width: 100px;">
							<%if(longValue != null){%>
							<%= longValue%>
							<%}%>
						</div>
						<% 
										} 
									%>
						<%
					longValue = null;
					%>

						<% }%>
					
				</div>
			</div>
		</div>

		<div class="row">
		<%
							if (!(sv.benCessAge.equals(0)||sv.benCessTerm.equals(0))) {
							
							%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Bene cess Age / Term")%></label>
					<table>
					<tr>
					<td>
						<%
							if (((BaseScreenData) sv.benCessAge) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.benCessAge,
						(sv.benCessAge.getLength() + 1), null).replace(
						"absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.benCessAge) instanceof DecimalData) {
						%>
						<%
							if (sv.benCessAge.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.benCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'output_cell \' style=\'width: 60px; \' ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.benCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
						
					</td>
					<td style="padding-left: 2px;">
						<%
							if (((BaseScreenData) sv.benCessTerm) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.benCessTerm,
						(sv.benCessTerm.getLength() + 1), null).replace(
						"absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.benCessTerm) instanceof DecimalData) {
						%>
						<%
							if (sv.benCessTerm.equals(0)) {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.benCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'output_cell \' style=\'width: 60px; \' ")%>
						<%
							} else {
						%>
						<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.benCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 60px; text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</td>
					</tr>
					</table>
				</div>
			</div>
		<%} %>

<%
							if (!((sv.benCessDateDisp.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {
							
							%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Benefit Cess Date")%></label>
					
						<%
							if (!((sv.benCessDateDisp.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.benCessDateDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.benCessDateDisp.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<%
							if ((formatValue == null) || ("".equals(formatValue.trim()))) {
						%>
						<div class="output_cell" style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell" style="width: 80px;"></div>
						<%
							}
						%>
						<%=XSSFilter.escapeHtml(formatValue)%>

						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>
<%} %>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Rerate from Date")%></label>
					
						<%
							if (!((sv.rerateFromDateDisp.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rerateFromDateDisp
											.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.rerateFromDateDisp
											.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label> <%
							if ((new Byte((sv.dialdownoption).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%> <%=resourceBundleHandler
						.gettingValueFromBundle("Dial Down Option")%> <%
							}
						%>
					</label>
					<div class="input-group">
						<%
							if ((new Byte((sv.dialdownoption).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								fieldItem = appVars.loadF4FieldsLong(
										new String[] { "dialdownoption" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("dialdownoption");
								optionValue = makeDropDownList(mappedItems,
										sv.dialdownoption.getFormData(), 2,
										resourceBundleHandler);
								longValue = (String) mappedItems.get((sv.dialdownoption
										.getFormData()).toString().trim());
						%>

						<%
							if ((longValue == null) || ("".equals(longValue.trim()))) {
						%>
						<div class="output_cell" style="width: 100px;">
							<%
								if (longValue != null) {
							%>
							<%=longValue%>
							<%
								}
							%>
						</div>
						<%
							} else {
						%>
						<div class="blank_cell" style="width: 100px;">
							<%
								if (longValue != null) {
							%>
							<%=longValue%>
							<%
								}
							%>
						</div>
						<%
							}
						%>
						<%
							longValue = null;
						%>

						<%
							}
						%>

					</div>
				</div>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<%
			if ((new Byte((sv.adjustageamt).getInvisible()))
					.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%
		       			if ((new Byte((sv.zlinstprem).getInvisible()))
		       						.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		       		%> <%=resourceBundleHandler
							.gettingValueFromBundle("Age Adjusted Amount")%> <%} %>
					</label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
						<%if(sv.adjustageamt.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label> <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%>
						<%} %>
					</label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
						<%if(sv.rateadj.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label> <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%>
						<%} %></label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
						<%if(sv.fltmort.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important; text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label> <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%>
						<%} %></label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
						<%if(sv.loadper.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
				</div>
			</div>
		</div>
		<%} %>

		<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label>
						<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%> <%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%>
						<%} %>
					</label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
						<%if(sv.premadj.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>

					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
					<div class="input-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							//ILIFE 1487 STARTS
							formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							//ILIFE 1487 ENDS
							if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"
							style="width:  145px !important; text-align: right;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 145px !important;"></div>

						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>

			<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0){ %><!-- ILIFE-3399 -->
			<div class="col-md-3">
				<div class="form-group">
					<label> <%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%> <%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%>
						<%} %>
					</label>
					<div class="input-group">
						<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
						<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
						<%if(sv.zbinstprem.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:  145px !important;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
				</div>
			</div>
			<%}%>
		</div>
		<%} %>
	<!-- ILIFE-8313 starts --> 
					<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) == 0){ %>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
					<div class="input-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"
							style="width:  145px !important; text-align: right;">
							<%= formatValue%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 145px !important;"></div>

						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>
			</div>
		<%} %>
	<!-- ILIFE-8313 ends -->
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Inst. Premium")%></label>
					<div class="input-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"
							style="width:  145px !important; text-align: right;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 145px !important;"></div>

						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				 
					<label> <%if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> 
								<% //ILJ-387
								if (sv.cntEnqScreenflag.compareTo("N") == 0){ %> 
								<!-- ILIFE-1062 Start--> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with tax")%>
						<!-- ILIFE-1062 End--> <%}%>
						<%}%>
					</label>
					<div class="input-group">
						<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
														
							<% //ILJ-387
								if (sv.cntEnqScreenflag.compareTo("N") == 0){ %> 
						<%	
									qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
									//ILIFE 1487 STARTS
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									//ILIFE 1487 ENDS
									if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
						<div class="output_cell"
							style="width: 145px !important; text-align: right;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
									} else {
								%>
						<div class="blank_cell" style="width: 145px !important;"></div>
						<% 
									} 
								%>
						<%
								longValue = null;
								formatValue = null;
								%>
						<%}%>
						<%}%>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Single Prem")%></label>
					<div class="input-group">
						<%	
							qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
						<div class="output_cell"
							style="width: 145px !important; text-align: right;">
							<%= XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							} else {
						%>

						<div class="blank_cell" style="width: 145px !important;"></div>

						<% 
							} 
						%>
						<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>
			</div>
		</div>
			<!-- ILIFE-6968 start-->
			<div class="row">
			<%
				if ((new Byte((sv.lnkgno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%> 
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Number")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.lnkgno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgno, (sv.lnkgno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
	 			<%
				}
			%>
			
			<%
				if ((new Byte((sv.lnkgsubrefno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
			<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Sub Ref Number")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.lnkgsubrefno) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.lnkgsubrefno, (sv.lnkgsubrefno.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.lnkgsubrefno) instanceof DecimalData) {
						%>
						<%
							if (sv.lnkgsubrefno.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
				<%
				}
			%>			
			<!--ILIFE-6968 end -->
			<!-- START OF ILIFE-7746 -->
			<% if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
						<%
							qpsf = fw.getFieldXMLDef((sv.zstpduty01).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.zstpduty01,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!((sv.zstpduty01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								if(longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue( formatValue); 
								} else {
									formatValue = formatValue( longValue);
								}							
							}
						%>
							
						<div id='stampduty' class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
						<%
							longValue = null;
							formatValue = null;
						%>
					</div>
				</div>
			<% } %>
			<!-- END OF ILIFE-7746 -->
		</div>
		
<div style='visibility: hidden;'>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label></label>
						<div class="input-group">
							<%					
						if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
						longValue = null;
						formatValue = null;
						%>
						</div>
					</div>
				</div>
				
				
			
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Last Bonus Date")%></label>
						<div class="input-group">
							<%					
						if(!((sv.unitStatementDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.unitStatementDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.unitStatementDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
								style="width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
						longValue = null;
						formatValue = null;
						%>
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Ind")%></label>
						<div class="input-group">

							<%					
						if(!((sv.bonusInd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bonusInd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.bonusInd.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
						longValue = null;
						formatValue = null;
						%>
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit bill")%></label>
						<div class="input-group">
							<%					
						if(!((sv.benBillDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
								style="width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
						longValue = null;
						formatValue = null;
						%>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl.Method")%></label>
						<div class="input-group">
							<%
								if (!((sv.bappmeth.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.bappmeth.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.bappmeth.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
								style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:80px;"
					: "width:140px;"%>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
						</div>
					</div>
				</div>								
			</div>

		</div>
		<!-- <div id='mainForm_OPTS' style='visibility: hidden'>
				<table style="font-size: 14px !important;line-height: 1.42857143 !important; margin-left: 20px !important;" id="S6242Sidemenu">
					<style>
						 
						/* .sidebar-icon {
							padding-top: 3px !important;
							padding-left: 15px;
						} */
					</style>
					<tr>
						<td style="padding: 6px !important;"> -->
						
						
						
						
						
						
						
						
						
						
						</div>
						
						
						
						
						
						
						
						
						
												<%-- IBPLIFE-2139 start --%>
<% if (sv.contnewBScreenflag.compareTo("Y") == 0){ %>  

<div class="tab-pane fade" id="cover_tab">
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
			<table><tr>
			<td style="min-width: 80px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcode, true)%></td>
			<td style="padding-left:1px;min-width: 200px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.trcdedesc, true)%></td>
			</tr></table>
			</div>
		</div>

					<div class="col-md-1"></div>
					<div class="col-md-2">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
						<div class="form-group">
							<div class="input-group">
								<input name='dateeffDisp' id="dateeffDisp" type='text'
									value='<%=sv.dateeffDisp.getFormData()%>'
									maxLength='<%=sv.dateeffDisp.getLength()%>'
									size='<%=sv.dateeffDisp.getLength()%>' onFocus='doFocus(this)'
									onHelp='return fieldHelp(dateeffDisp)'
									onKeyUp='return checkMaxLength(this)' readonly="true"
									class="output_cell" />
							</div>
						</div>
					</div>

				</div>
				

	     <div class="row">
			<div class="col-md-8">
			<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose")%></label>
						<table><tr>
							 <td width='251' style="padding-right: 40px; padding-top: 4px;">
							
				
				 <textarea rows="3" cols="50"  onfocus="this.blur()" readonly="readonly"  class="text-left" style="background-color:#eeeeee; border: 2px solid #ccc !important;resize:none;font-weight:bold; ">
<%=sv.covrprpse.getFormData()%>
</textarea>
	</td></tr>
							
							</table>
					</div>
			</div>
							
								
</div>
<%} %>
<%-- IBPLIFE-2139 end --%> 	
						
						
						
						
						
						
						


</div>
</div>
</div>
						
						
						
						
						
						
						
						
						
	
						
						<BODY>
	<div class="sidearea">
		<!-- 	<div class="logoarea" style="height: 50px;">
			<a class="navbar-brand" href="#">Integral Admin</a>
		</div> -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<!-- <a href="#">Extra Info --> <span> <!-- </a>	 -->
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
						
						
						
						
						
						
							<input name='payflag' id='payflag' type='hidden' value="<%=sv.payflag.getFormData()%>"> 
							<%if (sv.payflag.getFormData().equals("+")) { %> 
								<i class="fa fa-tasks fa-fw sidebar-icon"></i>
							<%} else {
								if (sv.payflag.getFormData().equals("X")) { %>
							<i class="fa fa-warning fa-fw sidebar-icon"></i>  <%
							 	} else {
							 %>
							<div style="width: 15px"></div> <%
						 		}
						 	}
						 	if (sv.payflag.getFormData().equals("X")) { %> 
						 	<img
							onclick="removeXfield(parent.frames['mainForm'].document.getElementById('payflag'))"
							style="cursor: pointer"
							src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
							border="0" align="right"> <%
							}
							%>
						<!-- </td>
						<td style="padding: 6px !important;"> -->
						<li>
						</li>
						
						<!-- START OF ILIFE-4172 -->
 							<%if (sv.payflag.getInvisible() != BaseScreenData.INVISIBLE
 					        || sv.payflag.getEnabled() != BaseScreenData.DISABLED){%>
							<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("payflag"))'
							class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Instalment Details")%></a>
							<%} %>
 						<!-- END OF ILIFE-4172 -->
						<!-- </td>
					</tr>
					<tr>
						<td style="padding: 6px !important;"> -->
						<li></li>
						
						
							<input name='optsmode' id='optsmode' type='hidden'  value="<%=sv.optsmode.getFormData()%>">
							<%
							if (sv.optsmode.getFormData().equals("+")) {
							%>
							<i class="fa fa-tasks fa-fw sidebar-icon"></i>
							
							<%}else {
								
							    if (sv.optsmode.getFormData().equals("X") ) {%>
							<i class="fa fa-warning fa-fw sidebar-icon"></i> 
							<% } else {%>
							    <div style="width: 15px"> </div>
							    
							<%}
							}
								if (sv.optsmode.getFormData().equals("X")) {
							%>
							<img  onclick="removeXfield(parent.frames['mainForm'].document.getElementById('optsmode'))" style="cursor:pointer"
							src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif" border="0" align="right" >
							<%
							}
							%>
						<!-- </td>
						<td style="padding: 6px !important;"> -->
						
						
						</li><li>
						
							<%if (sv.optsmode.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optsmode.getEnabled() != BaseScreenData.DISABLED){%>
								<a href="javascript:;"
								onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optsmode"))'
								class="hyperLink"
								>
								<%=resourceBundleHandler.gettingValueFromBundle("Benefit Schedule")%></a>
							<%} %>
						<!-- </td>
					</tr>
					<tr>
						<td style="padding: 6px !important;"> -->
						
					
					
					<li>

			
			
									 <input name='optextind' id='optextind' type='hidden'
									value="<%=sv.optextind.getFormData()%>" /> <!-- text --> <%
 	if (sv.optextind.getInvisible() == BaseScreenData.INVISIBLE
 				|| sv.optextind.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
										<%
											}
									
 	else {
		%> <a href="javascript:;"
		onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))'
		class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
			<%
				}
			%> <!-- icon --> <%
if (sv.optextind.getFormData().equals("+")) {
%> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
}
if (sv.optextind.getFormData().equals("X")) {
%> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
}
%>
	</a> 
</li>

					
					<% if (sv.mrtaFlag.compareTo("N") != 0) { %>
					<!-- <tr>
						<td style="padding: 6px !important;"> -->
						
						</li><li>
							<input name='zsredtrm' id='zsredtrm' type='hidden'  value="<%=sv.zsredtrm.getFormData()%>"> <%
							if (sv.zsredtrm.getFormData().equals("+")) {
							%> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%}else {
								
							    if (sv.zsredtrm.getFormData().equals("X") ) {%> <i
						class="fa fa-warning fa-fw sidebar-icon"></i> <% } else {%>
						<div style="width: 15px"></div> <%}
							}
								if (sv.zsredtrm.getFormData().equals("X")) {
							%> <img
						onclick="removeXfield(parent.frames['mainForm'].document.getElementById('taxind'))"
						style="cursor: pointer"
						src="/<%= AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
						border="0" align="right"> <%
							}
							%>
						<!-- </td>
						<td style="padding: 6px !important;"> -->
						
						</li><li>
						
						<%if (sv.zsredtrm.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.zsredtrm.getEnabled() != BaseScreenData.DISABLED){%>
							<a href="javascript:;"
								onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("zsredtrm"))'
								class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Reducing Term")%></a>
						<%} %>
						<!-- </td>
					</tr> -->
					
					</li><li>
					<%} %>
					
					<!-- <tr>
						<td style="padding: 6px !important;"> -->
					<li>
									 <input name='taxind' id='taxind' type='hidden'
									value="<%=sv.taxind.getFormData()%>" /> <!-- text --> <%
 	if (sv.taxind.getInvisible() == BaseScreenData.INVISIBLE
 				|| sv.taxind.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Tax Details")%>
										<%
											}
									
 	else {
		%> <a href="javascript:;"
		onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))'
		class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Tax Details")%>
			<%
				}
			%> <!-- icon --> <%
if (sv.taxind.getFormData().equals("+")) {
%> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
}
if (sv.taxind.getFormData().equals("X")) {
%> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
}
%>
	</a> 
</li>
					
					
					
					
					
					
					

					<%if(sv.exclind.getInvisible()!= BaseScreenData.INVISIBLE){%>
					<!-- <tr>
						<td style="padding: 6px !important;"> -->
						
						
					
					
					<li>

			
			
									 <input name='exclind' id='exclind' type='hidden'
									value="<%=sv.exclind.getFormData()%>" /> <!-- text --> <%
 	if (sv.exclind.getInvisible() == BaseScreenData.INVISIBLE
 				|| sv.exclind.getEnabled() == BaseScreenData.DISABLED) {
 %> <a href="#" class="disabledLink"><%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
										<%
											}
									
 	else {
		%> <a href="javascript:;"
		onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))'
		class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
			<%
				}
			%> <!-- icon --> <%
if (sv.exclind.getFormData().equals("+")) {
%> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
}
if (sv.exclind.getFormData().equals("X")) {
%> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
}
%>
	</a> 
</li>
						<%} %>
	
					<!-- <tr>
						<td style="padding: 6px !important;"> -->
						<li>
							<input name='pbind' id='pbind' type='hidden'
							value="<%=sv.pbind.getFormData()%>"> <%
							 	if (sv.pbind.getFormData().equals("+")) {
							 %> <i class="fa fa-tasks fa-fw sidebar-icon"></i>			 <%
							 	} else {
							
							 		if (sv.pbind.getFormData().equals("X")) {
							 %>
												<i class="fa fa-warning fa-fw sidebar-icon"> <%
							 	} else {
							 %>
							<div style="width: 15px"></div> <%
						 		}
						 	}
						 	if (sv.pbind.getFormData().equals("X")) {
						 %> <img
												onclick="removeXfield(parent.frames['mainForm'].document.getElementById('pbind'))"
							style="cursor: pointer"
							src="/<%=AppVars.getInstance().getContextPath()%>/screenFiles/<%=imageFolder%>/ico_greent_01.gif"
							border="0" align="right"> <%
					 	}
					 %><!-- </td>
	
						<td style="padding: 6px !important;"> -->
						
						</i><li>
							<%
								if (sv.pbind.getInvisible() != BaseScreenData.INVISIBLE
										|| sv.pbind.getEnabled() != BaseScreenData.DISABLED) {
							%>
							<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("pbind"))'
							class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%></a>
							 <%} %>
						<!-- </td>
					</tr>
				</table> -->
				
				</li>
					<li>
					 <input name='aepaydet' id='aepaydet' type='hidden'
									value="<%=sv.aepaydet.getFormData()%>" /> <!-- text --> 
						  <%
						 	if (sv.aepaydet.getInvisible() != BaseScreenData.INVISIBLE) {/* ILJ-130 */
						 %> 
						 <a href="javascript:;"
								onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aepaydet"))'
								class="hyperLink"> <%=resourceBundleHandler.gettingValueFromBundle("AntcptdEndwnt PayDetails")%>
						<%
						}
						%> <!-- icon --> <%
						if (sv.aepaydet.getFormData().equals("+")) {
						%> <i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
						}
						if (sv.aepaydet.getFormData().equals("X")) {
						%> <i class="fa fa-warning fa-fw sidebar-icon"></i> <%
						}
						%>
							</a> 
						</li>
				</ul>
				</div>
				</div>
				
		</div>
		
	</div>
</div>
<script>
$(document).ready(function() {
	if (screen.height == 1024) {
		
		$('#cntdesc').css('max-width','140px')
	} 
 
	
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
