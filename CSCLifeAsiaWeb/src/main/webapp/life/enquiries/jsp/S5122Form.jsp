<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5122";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S5122ScreenVars sv = (S5122ScreenVars) fw.getVariables();%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund Currency ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage        ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Real Units");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Deemed Units");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund Amount");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Compl");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"-ete");%>	
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction Code");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Transaction Number");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Price Used Date");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Price Used");%>
 
 	
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind80.isOn()) {
			generatedText9.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind80.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			generatedText14.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			generatedText17.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind02.isOn()) {
			sv.zvar01.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind02.isOn()) {
			sv.zvar02.setHighLight(BaseScreenData.BOLD);
		}
		if (!appVars.ind02.isOn()) {
			sv.zvariable.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.ind01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.ind02.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText1).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						<%=resourceBundleHandler.gettingValueFromBundle("Contract")%>
						<%}%></label>
		       		<table>
		       		<tr>
		       		<td>

							<%if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
							</td>
							<td>	
							
							
							<%if ((new Byte((sv.cnttype).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="margin-left: 1px;'">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							</td>
							<td>
							
							<%if ((new Byte((sv.ctypedes).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width: 170px;margin-left: 1px;" id="ctypedes">
											<%=formatValue%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       	 
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
					<%}%></label>
		       		
							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
									<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("register");
									longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
								%>
								
							    <table>
							    <tr><td>
								   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width: 50px;"> 
								   <%=	(sv.register.getFormData()).toString()%>
								   </div>
								  </td>
								  <td>
								   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'
								 style="max-width: 200px;margin-left: 1px;min-width: 100px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
								   </td>
								   </tr>
								   </table>
							   		<%
									longValue = null;
									formatValue = null;
									%>
							   
							  <%}%>
		       		
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
					<%}%></label>
		       		
							<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
									<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("cntcurr");
									longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
								%>
								
							    <table>
							    <tr>
							    <td>
								   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width: 50px;"> 
								   <%=	(sv.cntcurr.getFormData()).toString()%>
								   </div>
								</td>
								<td>   
								   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'
								 style="margin-left: 1px;max-width: 200px;min-width: 100px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
								</td>
								</tr>
								</table>
							   		<%
									longValue = null;
									formatValue = null;
									%>
							   
							  <%}%>
		       		
		       		</div>
		       	</div>
		    </div>
		    
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText3).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%>
					<%}%></label>
		       		<div class="input-group">
						<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
	
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText4).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Fund Currency")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.fundCurrency).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.fundCurrency.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundCurrency.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundCurrency.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%	
										qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
										
										if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" > &nbsp; </div>
									
									<% 
										} 
									%>
									<%
									longValue = null;
									formatValue = null;
									%>
								
							 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		    
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%>
					<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
							<%if ((new Byte((sv.lifenum).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
								
							</td>
							<td>
							<%if ((new Byte((sv.lifename).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'
													 style="margin-left: 1px;max-width: 200px;min-width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       			</td>
		       			</tr>
		       			</table>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%>
					<%}%></label>
		       		<table><tr><td style="min-width:100px">
						<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						
						</td><td style="width:1px">
						
						</td><td style="min-width:100px">
						<%if ((new Byte((sv.jlifename).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						</td><td style="width:1px">
						
						</td><td style="min-width:100px">
						
						<%if ((new Byte((sv.ind02).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.ind02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ind02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ind02.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Coverage")%>
					<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
						<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width: 50px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						</td>
						<td>
						
						<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue =  formatValue((sv.rider.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.rider.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
									 style="margin-left: 1px;max-width: 200px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		     
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Fund")%>
					<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
								<%if ((new Byte((sv.unitVirtualFund).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.unitVirtualFund.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.unitVirtualFund.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.unitVirtualFund.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="max-width: 60px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
							</td>
							<td>
							<%if ((new Byte((sv.fundShortDesc).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.fundShortDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundShortDesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundShortDesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' 
										style="margin-left: 1px;max-width: 200px;min-width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		        
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Type")%>
					<%}%></label>
		       		<table>
		       		<tr>
		       		<td>
							<%if ((new Byte((sv.fundtype).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.fundtype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundtype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundtype.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'style="max-width: 50px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
							</td>
							<td>
							<%if ((new Byte((sv.fundTypeShortDesc).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.fundTypeShortDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundTypeShortDesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.fundTypeShortDesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'
									 style="margin-left: 1px;max-width: 200px;min-width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>
		    
		    
								    <%
						/* This block of jsp code is to calculate the variable width of the table at runtime.*/
						int[] tblColumnWidth = new int[10];//TMLII-591
						int totalTblWidth = 0;
						int calculatedValue =0;
						
												if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.nofUnits.getFormData()).length() ) {
												//Ticket #ILIFE-1402 start by akhan203
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*7;								
									} else {		
										calculatedValue = (sv.nofUnits.getFormData()).length()*7;								
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[0]= calculatedValue;
									
												if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.nofDunits.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*7;								
									} else {		
										calculatedValue = (sv.nofDunits.getFormData()).length()*7;							
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[1]= calculatedValue;
									
												if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.moniesDateDisp.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*7;								
									} else {		
										calculatedValue = (sv.moniesDateDisp.getFormData()).length()*7;								
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[2]= calculatedValue;
									
												if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fundAmount.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*11;								
									} else {		
										calculatedValue = (sv.fundAmount.getFormData()).length()*11;								
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[3]= calculatedValue;
									
												if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.nowDeferInd.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*9;								
									} else {		
										calculatedValue = (sv.nowDeferInd.getFormData()).length()*9;								
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[4]= calculatedValue;
									
												if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.feedbackInd.getFormData()).length() ) {
										calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
									} else {		
										calculatedValue = (sv.feedbackInd.getFormData()).length()*12;		
										//Ticket #ILIFE-1402 end						
									}		
										totalTblWidth += calculatedValue;
								tblColumnWidth[5]= calculatedValue;
								/* TMLII-591 starts */
								if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.priceUsed.getFormData()).length() ) {
									calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
								} else {		
									calculatedValue = (sv.priceUsed.getFormData()).length()*4;								
								}		
									totalTblWidth += calculatedValue;
							tblColumnWidth[6]= calculatedValue;
							
							
							if(resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.proctrancd.getFormData()).length() ) {
								calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length())*8;								
							} else {		
								calculatedValue = (sv.proctrancd.getFormData()).length()*4;								
							}		
								totalTblWidth += calculatedValue;
						tblColumnWidth[7]= calculatedValue;
						
						 
						
						if(resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.tranno.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length())*8;								
						} else {		
							calculatedValue = (sv.tranno.getFormData()).length()*8;								
						}		
							totalTblWidth += calculatedValue;
					tblColumnWidth[8]= calculatedValue;
					
					
					if(resourceBundleHandler.gettingValueFromBundle("Header10").length() >= (sv.pricdte.getFormData()).length() ) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header10").length())*8;								
					} else {		
						calculatedValue = (sv.pricdte.getFormData()).length()*8;								
					}		
						totalTblWidth += calculatedValue;
				tblColumnWidth[9]= calculatedValue;
					
							/* TMLII-591 ends */
									%>
								<%
								GeneralTable sfl = fw.getTable("s5122screensfl");
								int height;
								if(sfl.count()*27 > 180) {
								height = 210 ;
								} else {
								height = sfl.count()*27;
								}	
								%>
						<!-- <script type="text/javascript">
						      $(function() {
						        $("table tr:nth-child(even)").addClass("striped");
						      });
						</script> -->
						
				<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s5122' width="100%">	
			    	 	<thead>
			    	 	<tr class='info'>	
			    	 			<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction Code")%></center></th>		
			    	 			<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Transaction Number")%></center></th>	
			    	 			<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Monies Date")%></center></th>
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Price Used Date")%></center></th>
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Price Used")%></center></th>
													
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Real Units")%></center></th>
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Deemed Units")%></center></th>
							
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Amount")%></center></th>
								<th ><center><%=resourceBundleHandler.gettingValueFromBundle("Now Def")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Complete")%></center></th>								
							
							
								
						</tr>	
			         	</thead>
					      <tbody>
					      <%
								S5122screensfl
								.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S5122screensfl
								.hasMoreScreenRows(sfl)) {	
							%>
							
								<tr class="tableRowTag" id='<%="tablerow"+count%>' >
								
								
													<%if((new Byte((sv.proctrancd).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" 
												<%if((sv.proctrancd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.proctrancd.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" >
																					
											    </td>
																	
												<%}%>
												
												
												<%if((new Byte((sv.tranno).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[7 ]%>px;" 
												<%if((sv.tranno).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.tranno.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[7]%>px;" >
																					
											    </td>
																	
												<%}%>
												
												
													<%if((new Byte((sv.moniesDateDisp).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" 
												<%if((sv.moniesDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.moniesDateDisp.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" >
																					
											    </td>
																	
												<%}%>
												 <!-- TMLII-591 SAN starts -->
												 
												 
												 	<%if((new Byte((sv.priceDateDisp).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[8 ]%>px;" 
												<%if((sv.priceDateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.priceDateDisp.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[8 ]%>px;" >
																					
											    </td>
																	
												<%}%>
												
																	<%if((new Byte((sv.priceUsed).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" 
												<%if((sv.priceUsed).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																														
												<%	
													sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.priceUsed).getFieldName());						
													//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
												%>
												
																	
													<%
														formatValue = smartHF.getPicFormatted(qpsf,sv.priceUsed,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
														if(!(sv.priceUsed).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
															formatValue = formatValue( formatValue );
														}
													%>
													<%= formatValue%>&nbsp;
													<%
															longValue = null;
															formatValue = null;
													%>
												 			 		
										 		
										    				 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[6 ]%>px;" >
																					
											    </td>
																	
												<%}%>
												
												
													<%if((new Byte((sv.nofUnits).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" 
												<%if((sv.nofUnits).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >
																										
															
																		
																														
												<%	
													sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.nofUnits).getFieldName());						
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
												%>
												
																	
													<%
														formatValue = smartHF.getPicFormatted(qpsf,sv.nofUnits);
														if(!(sv.nofUnits).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
															formatValue = formatValue( formatValue );
														}
													%>
													<%= formatValue%>&nbsp;
													<%
															longValue = null;
															formatValue = null;
													%>
												 			 		
										 		
										    				 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag tableDataTagFixed" style="width:<%=tblColumnWidth[0 ]%>px;" >
												</td>														
																	
												<%}%>
															<%if((new Byte((sv.nofDunits).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" 
												<%if((sv.nofDunits).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																														
												<%	
													sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.nofDunits).getFieldName());						
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
												%>
												
																	
													<%
														formatValue = smartHF.getPicFormatted(qpsf,sv.nofDunits);
														if(!(sv.nofDunits).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
															formatValue = formatValue( formatValue );
														}
													%>
													<%= formatValue%>&nbsp;
													<%
															longValue = null;
															formatValue = null;
													%>
												 			 		
										 		
										    				 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															
							 					<!-- TMLII-591 ends -->
							
											
															<%if((new Byte((sv.fundAmount).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" 
												<%if((sv.fundAmount).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																														
												<%	
													sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.fundAmount).getFieldName());						
													//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
												%>
												
																	
													<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.fundAmount,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
														if(!(sv.fundAmount).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
															formatValue = formatValue( formatValue );
														}
													%>
													<%= formatValue%>&nbsp;
													<%
															longValue = null;
															formatValue = null;
													%>
												 			 		
										 		
										    				 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															<%if((new Byte((sv.nowDeferInd).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" 
												<%if((sv.nowDeferInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.nowDeferInd.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" >
																					
											    </td>
																	
												<%}%>
															<%if((new Byte((sv.feedbackInd).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){%>	
									    									<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" 
												<%if((sv.feedbackInd).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
															
																		
																
																		
													<%= sv.feedbackInd.getFormData()%>
													
																					 
											
																</td>
									<%}else{%>
																			<td class="tableDataTag" style="width:<%=tblColumnWidth[5 ]%>px;" >
																					
											    </td>
																	
												<%}%>
												
												
												
												
												
												
											
																
								</tr>
							
								<%
								count = count + 1;
								S5122screensfl
								.setNextScreenRow(sfl, appVars, sv);
								}
								%>
					      </tbody>
					</table>
					
					</div>
				</div>
			</div>
		</div>
	 </div>
</div>

<script>
	$(document).ready(function() {
    	$('#dataTables-s5122').DataTable({
    		paging: true,
        	ordering: false,
        	searching:false,
        	scrollY: "400px",
			scrollCollapse: true,
      	});
    	
    	if (screen.height == 1024) {
    		$("#ctypedes").css('max-width','170px')
    	}
		if (screen.height == 900) {
			$("#ctypedes").css('max-width','220px')  		
		}
		
    	
    });
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

<div style='visibility:hidden;'>
		<div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.planSuffix).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%	
										qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
										
										if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" > &nbsp; </div>
									
									<% 
										} 
									%>
									<%
									longValue = null;
									formatValue = null;
									%>
								
							 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Rider")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.ind01).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.ind01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ind01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.ind01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
	
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Real Units")%>
					<%}%></label>
		       		<div class="input-group">
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Deemed Units")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvariable).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvariable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvariable.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvariable.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Fund Amount")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar01).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar01.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Compl")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label> 
					<%=resourceBundleHandler.gettingValueFromBundle("Transaction Code")%>
					 </label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Transaction Number")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Price Used Date")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Price Used")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.zvar02).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.zvar02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zvar02.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	
		    </div>

		</div>

<%
if(!cobolAv3.isPagedownEnabled()){
%>
	<script language="javascript">
	window.onload= function(){
		setDisabledMoreBtn();
	}
	</script>
	
<%} %>  