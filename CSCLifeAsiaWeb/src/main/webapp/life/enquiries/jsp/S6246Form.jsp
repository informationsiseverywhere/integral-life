<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6246";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6246ScreenVars sv = (S6246ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Cov. No ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Insured ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage Table ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub Section ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Premium ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund Split Plan ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"% or Prem Amnt ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Allocation ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Fund");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Percentage");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Price");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Policies in Plan  ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Auto Fund Rebalancing (AFR)");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"AFR Freq / Risk Prof");%>
	
<%{
		if (appVars.ind44.isOn()) {
			sv.zagelit.setReverse(BaseScreenData.REVERSED);
			sv.zagelit.setColor(BaseScreenData.RED);
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Number")%></label>
		       		<table><tr><td>
						 		  		
		<%
			if (!((sv.chdrnum.getFormData()).toString()).trim()
					.equalsIgnoreCase("")) {

				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.chdrnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}

			} else {

				if (longValue == null || longValue.equalsIgnoreCase("")) {
					formatValue = formatValue((sv.chdrnum.getFormData())
							.toString());
				} else {
					formatValue = formatValue(longValue);
				}

			}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:60px;">
						
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
			longValue = null;
			formatValue = null;
		%>
		</td><td style="padding-left:1px;">
  <!-- ILIFE-4036 started -->
<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  	</td><td style="padding-left:1px;">	
<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
			<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:250px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
		    </td></tr></table>   		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-4"></div>
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Table")%></label>
		       		<table><tr><td>
									<%	
								//Amit hack
									fieldItem=appVars.loadF4FieldsLong(new String[] {"crtable"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("crtable");
									longValue = (String) mappedItems.get((sv.crtable.getFormData()).toString());  
								%>
							  		<div class='<%= ((sv.crtable.getFormData() == null) || ("".equals((sv.crtable.getFormData()).trim()))) ? 
														"blank_cell" : "output_cell" %>'> 
								   <%=	(sv.crtable.getFormData()).toString()%>
								   </div>
								   </td><td style="padding-left:1px;">	
								   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="max-width:195px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
							   		<%
									longValue = null;
									formatValue = null;
									%>
									</td></tr></table>
		       		
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Insured")%></label>
		       		<table><tr><td>
	
							  		<div 	
									class='<%= ((sv.lifenum.getFormData().trim().length() == 0 ) ? 
													"blank_cell'" : "output_cell'") %>' style="min-width: 80px;">
									<%					
									if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  </td><td style="padding-left:1px;">	
							
							  		<div 	
									class='<%= (sv.linsname.getFormData()).trim().length() == 0 ? 
													"blank_cell" : "output_cell" %>' style="max-width:150px;">
									<%					
									if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
		       		</td></tr></table>
		       		</div>
		       	</div>
		       			<div class="col-md-4"></div>
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
						  		
								<%					
								if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<label> <%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label> 
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
						  </label>
		       		
		       		<div class="input-group">
		
									<%	
										qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
										
										if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" style="min-width: 71px;max-width: 800px;">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="min-width: 71px;max-width: 800px;">
											</div>
									
									<% 
										} 
									%>
									
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table><tr><td>

						  		<div 	
								class='<%= (sv.jlifcnum.getFormData()).trim().length() == 0 ? 
												"blank_cell'" : "output_cell'" %> ' style="min-width: 71px;max-width: 300px;">
								<%					
								if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td><td style="padding-left:1px;">
						  
							<div 	
								class='<%= (sv.jlinsname.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>' style="min-width: 71px;max-width: 300px;">
								<%					
								if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
								</td></tr></table>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		
		       	</div>
		    </div>
		    
	     	<div class="row">
	        		<div class="col-md-2" style="max-width: 120px;">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life No")%></label>
			       		<div class="input-group">
						  		<div 	
								class='<%= (sv.life.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>'>
								<%					
								if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.life.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.life.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-2" style="max-width: 160px;">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Cov. No")%></label>
			       		<div class="input-group">
							  		<div 	
									class='<%= (sv.coverage.getFormData()).trim().length() == 0 ? 
													"blank_cell" : "output_cell" %>' >
									<%					
									if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-2" style="max-width: 130px;">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>
			       		<div class="input-group">
							  		<div 	
									class='<%= (sv.rider.getFormData()).trim().length() == 0 ? 
													"blank_cell" : "output_cell" %>' >
									<%					
									if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rider.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rider.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-2" style="max-width: 140px;">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Statutory Fund")%></label>
			       		<div class="input-group">
			       		

						<%if ((new Byte((sv.statfund).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
								<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"statfund"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("statfund");
								longValue = (String) mappedItems.get((sv.statfund.getFormData()).toString().trim());  
							%>
							
						    
							   <!--<div class='<%= ((sv.statfund.getFormData() == null) || ("".equals((sv.statfund.getFormData()).trim()))) ? 
													"blank_cell" : "output_cell" %>'> 
							   <%=	(sv.statfund.getFormData()).toString()%>
							   </div>
							   
							   --><div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						   		<%
								longValue = null;
								formatValue = null;
								%>
						   
						  <%}%>
						
						  		
								<!--<%
									if (!((sv.statfund.getFormData()).toString()).trim()
											.equalsIgnoreCase("")) {
						
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.statfund.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
						
									} else {
						
										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.statfund.getFormData())
													.toString());
										} else {
											formatValue = formatValue(longValue);
										}
						
									}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
									longValue = null;
									formatValue = null;
								%>
						  
							
						
								-->
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-2">
			       		<div class="form-group">
			       		<label><%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Section")%>
								<%}%>
			       		</label>
			       		<div class="input-group">
								<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(
																BaseScreenData.INVISIBLE)) != 0) {%>
										<%	
										fieldItem=appVars.loadF4FieldsLong(new String[] {"statSect"},sv,"E",baseModel);
										mappedItems = (Map) fieldItem.get("statSect");
										longValue = (String) mappedItems.get((sv.statSect.getFormData()).toString().trim());  
									%>
									
								  		
										<%					
										if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="min-width: 150px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
								  <%}%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-2" style="padding-left: 40px;">
			       		<div class="form-group">
			       		<label><%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Sub Section")%>
								<%}%>
						</label>
			       		<div class="input-group">
							<%if ((new Byte((sv.stsubsect).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
									<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"stsubsect"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("stsubsect");
									longValue = (String) mappedItems.get((sv.stsubsect.getFormData()).toString().trim());  
								%>
								
							  		
									<%					
									if(!((sv.stsubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.stsubsect.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.stsubsect.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' id="cntdesc" style="min-width: 180px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
			       		</div>
			       		</div>
		       	    </div>
				</div>
				
	
				
				<div class="row">
	        		<div class="col-md-4" >
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%></label>
			       		<div class="input-group">
								<%	
										qpsf = fw.getFieldXMLDef((sv.instprem).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											formatValue = smartHF.getPicFormatted(qpsf,sv.instprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										
										if(!((sv.instprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell"  style="min-width: 80px; ">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="min-width: 80px;"> </div>
									
									<% 
										} 
									%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    
		       	    
		       	    <div class="col-md-4" >
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Allocation")%></label>
			       		<div class="input-group">
									<%	
										qpsf = fw.getFieldXMLDef((sv.virtFundSplitMethod).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
											formatValue = smartHF.getPicFormatted(qpsf,sv.virtFundSplitMethod);
										
										if(!((sv.virtFundSplitMethod.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" style="min-width: 80px; max-width: 150px;">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="min-width: 80px; max-width: 150px;"> </div>
									
									<% 
										} 
									%>
			       		</div>
			       		</div>
		       	    </div>
		       	   
		       	    
		       	    <div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("% or Prem Amnt")%></label>
			       		<div class="input-group">
							<div 	
		class='<%= (sv.percOrAmntInd.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.percOrAmntInd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.percOrAmntInd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.percOrAmntInd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    
				</div>
				
				
				
				<div class="row">
	        		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Fund Rebalancing (AFR)")%></label>
			       		<div class="input-group">
							<div class='<%= (sv.zafropt1.getFormData()).trim().length() == 0 ? 
													"blank_cell" : "output_cell" %>'>
									<%					
									if(!((sv.zafropt1.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															fieldItem = appVars.loadF4FieldsLong(new String[] { "zafropt1" }, sv, "E", baseModel);
															mappedItems = (Map) fieldItem.get("zafropt1");
															formatValue = formatValue( ((String) mappedItems.get((sv.zafropt1.getFormData()).toString().trim())).toString());
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.zafropt1.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
			       		</div>
			       		</div>
		       	    </div>
		       	    
		       	    <div class="col-md-4">
			       		<div class="form-group">
			       		<label><%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("AFR Freq / Risk Prof")%>
								<%}%>
						</label>
			       		<div class="input-group">
								<div class='<%= (sv.zafritem.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>'>
								<%					
								if(!((sv.zafritem.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														fieldItem = appVars.loadF4FieldsLong(new String[] { "zafritem" }, sv, "E", baseModel);
														mappedItems = (Map) fieldItem.get("zafritem");
														formatValue = formatValue( ((String) mappedItems.get((sv.zafritem.getFormData()).toString().trim())).toString());
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.zafritem.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
			       		</div>
			       		</div>
			       		 </div>
			       		 
			       		<div class="col-md-4">
			       		<div class="form-group">
			       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total Policies in Plan")%></label>
			       		<div class="input-group">
							<%	
									qpsf = fw.getFieldXMLDef((sv.numapp).getFieldName());
									qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.numapp);
									
									if(!((sv.numapp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell" >	
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" > </div>
								
								<% 
									} 
								%>
			       		</div>
			       		</div>
		       	    </div>
			       		
			       		</div>
			       		
		       	   
		       	    
		       	    

 <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-s6246' width='100%'>
							<thead>
								<tr class='info'>								
							<th style="width: 220px;"> <center> <%=resourceBundleHandler.gettingValueFromBundle("Fund")%></center></th>
							<th style="width: 220px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
							<th style="width: 220px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Percentage")%></center></th>
							<th style="width: 220px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Price")%></center></th>
						</tr>	
			         	</thead>
			         	<tbody>
			         	
			         	
			         	
			         	<tr style="height: 25px;">	
	
	
						<td align="left">														
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("unitVirtualFund01");
							longValue = (String) mappedItems.get((sv.unitVirtualFund01.getFormData()).toString().trim());  
							%>
							<%=	(sv.unitVirtualFund01.getFormData()).toString()%>
							
							<%=checkforNull(longValue)%>
							<%longValue = null;%>
						</td>
									
						<td align="left">									
							<%					
							if(!((sv.currcy01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currcy01.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.currcy01.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
							<%=formatValue%>
							<%
							longValue = null;
							formatValue = null;
							%>	
						</td>
									
						<td align="right">									
							<%	
								qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt01).getFieldName());
							//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
									formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
								
								if(!((sv.unitAllocPercAmt01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
							
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<%= formatValue%>
							<%
							} else {
							%>
							
							
							<% 
							} 
							%>
					   </td>
									
						<td align="right">									
																		
							<%	
								qpsf = fw.getFieldXMLDef((sv.unitBidPrice01).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
								formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice01,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
								
								if(!((sv.unitBidPrice01.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
										<%= formatValue%>
							<%
								} else {
							%>
							
							
							<% 
								} 
							%>
						</td>
					</tr>
				
					<tr style="height: 25px;">		
	
							<td align="left">														
																 
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund02"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("unitVirtualFund02");
								longValue = (String) mappedItems.get((sv.unitVirtualFund02.getFormData()).toString().trim());  
							%>
							
							   
							 
							  <%=	(sv.unitVirtualFund02.getFormData()).toString()%>
							 
							 
							  <%=checkforNull(longValue)%>
							 <%longValue = null;%>
							</td>
									
						<td align="left">									
						<%					
						if(!((sv.currcy02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy02.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy02.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt02).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						</td>
									
					<td align="right">									
																
						<%	
							qpsf = fw.getFieldXMLDef((sv.unitBidPrice02).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice02,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.unitBidPrice02.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
									<%= formatValue%>
						<%
							} else {
						%>
						
						
						<% 
							} 
						%>
					</td>
					</tr>
				
					<tr style="height: 25px;">				
							<td align="left">														
																 
								<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund03"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("unitVirtualFund03");
									longValue = (String) mappedItems.get((sv.unitVirtualFund03.getFormData()).toString().trim());  
								%>
								
							    
							  
							   <%=(sv.unitVirtualFund03.getFormData()).toString()%>
							  
							  
							   <%=checkforNull(longValue)%>
							  <%!
							   private String checkforNull(String value){
							    if(value!=null){
							    return value;
							    }else{
							    return " ";
							    }
							   }
							  
							   %>
							   <%longValue = null;%>
							</td>
									
							<td align="left">									
																		
						
								<%					
								if(!((sv.currcy03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currcy03.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.currcy03.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=formatValue%>
								<%
								longValue = null;
								formatValue = null;
								%>	
				
							</td>
												
								<td align="right">									
									<%	
										qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt03).getFieldName());
									//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
										formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										
										if(!((sv.unitAllocPercAmt03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
												<%= formatValue%>
									<%
										} else {
									%>
									
									
									<% 
										} 
									%>
									</td>
									
							<td align="right">									
								<%	
									qpsf = fw.getFieldXMLDef((sv.unitBidPrice03).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
									formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice03,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									
									if(!((sv.unitBidPrice03.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
											<%= formatValue%>
								<%
									} else {
								%>
								
								
								<% 
									} 
								%>
							</td>
				</tr>
					
					
					
					
					<tr style="height: 25px;">			
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund04"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund04");
						longValue = (String) mappedItems.get((sv.unitVirtualFund04.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund04.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt04).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
						formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice04).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice04,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice04.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>	
				
				<tr style="height: 25px;">	
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund05"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund05");
						longValue = (String) mappedItems.get((sv.unitVirtualFund05.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund05.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy05.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy05.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt05).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
								formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice05).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice05,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice05.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
				
				
				
				<tr style="height: 25px;">		
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund06"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund06");
						longValue = (String) mappedItems.get((sv.unitVirtualFund06.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund06.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy06.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy06.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy06.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt06).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice06).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
						formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice06,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice06.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
				
				<tr style="height: 25px;">		
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund07"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund07");
						longValue = (String) mappedItems.get((sv.unitVirtualFund07.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund07.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td  align="left">									
																
				
						<%					
						if(!((sv.currcy07.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy07.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy07.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt07).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt07,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice07).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
												formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice07,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice07.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
				
				<tr style="height: 25px;">	 		
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund08"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund08");
						longValue = (String) mappedItems.get((sv.unitVirtualFund08.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund08.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy08.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy08.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy08.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt08).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt08,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice08).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
												formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice08,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice08.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
				
				<tr style="height: 25px;">	
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund09"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund09");
						longValue = (String) mappedItems.get((sv.unitVirtualFund09.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund09.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy09.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy09.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy09.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt09).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
												formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt09,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice09).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
							formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice09,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice09.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
				<tr style="height: 25px;">	  		
	
	
					<td align="left">														
														 
					<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"unitVirtualFund10"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("unitVirtualFund10");
						longValue = (String) mappedItems.get((sv.unitVirtualFund10.getFormData()).toString().trim());  
					%>
					
				    
				  
				   <%=	(sv.unitVirtualFund10.getFormData()).toString()%>
				  
				  
				   <%=checkforNull(longValue)%>
				  <%longValue = null;%>
				</td>
									
				<td align="left">									
																
				
						<%					
						if(!((sv.currcy10.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy10.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.currcy10.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
								<%=formatValue%>
						<%
						longValue = null;
						formatValue = null;
						%>	
											
						
														 
				
				</td>
									
					<td align="right">									
																
									
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitAllocPercAmt10).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
												formatValue = smartHF.getPicFormatted(qpsf,sv.unitAllocPercAmt10,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitAllocPercAmt10.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
						
														 
				
									</td>
									
					<td align="right">									
																
					<%	
						qpsf = fw.getFieldXMLDef((sv.unitBidPrice10).getFieldName());
						//qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS5);
						formatValue = smartHF.getPicFormatted(qpsf,sv.unitBidPrice10,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
						
						if(!((sv.unitBidPrice10.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
								<%= formatValue%>
					<%
						} else {
					%>
					
					
					<% 
						} 
					%>
											
						
														 
				
				</td>
				</tr>
					      
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>



<div style='visibility:hidden;'><table>f
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		<div 	
		class='<%= (sv.crtabdesc.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

<td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Fund Split Plan")%>
</div>


<br/>

	
  		<div 	
		class='<%= (sv.virtFundSplitMethod.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.virtFundSplitMethod.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.virtFundSplitMethod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.virtFundSplitMethod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>

<tr style='height:22px;'>
</tr></table></div>

	 </div>
</div></div>
<script>
$(document).ready(function() {
	if (screen.height == 1024) {
		
		$('#cntdesc').css('min-width','125px')
	} 
	$('#dataTables-s6246').DataTable({
        ordering : false,
        searching : false,
        scrollY : "350px",
        scrollCollapse : true,
        scrollX : true,
        paging:   false,
        ordering: false,
  info:     false,
  searching: false
 });
	
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
