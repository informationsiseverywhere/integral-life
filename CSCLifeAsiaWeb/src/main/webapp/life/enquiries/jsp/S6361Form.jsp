<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6361";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6361ScreenVars sv = (S6361ScreenVars) fw.getVariables();%>

<%
	int[] tblColumnWidth = new int[9];
	int totalTblWidth = 0;
	int calculatedValue = 0;

	calculatedValue = 20;
	totalTblWidth += calculatedValue;
	tblColumnWidth[0] = calculatedValue;

	calculatedValue = 120;
	totalTblWidth += calculatedValue;
	tblColumnWidth[1] = calculatedValue;

	calculatedValue = 210;
	totalTblWidth += calculatedValue;
	tblColumnWidth[2] = calculatedValue;

	calculatedValue = 70;
	totalTblWidth += calculatedValue;
	tblColumnWidth[3] = calculatedValue;

	calculatedValue = 120;
	totalTblWidth += calculatedValue;
	tblColumnWidth[4] = calculatedValue;

	calculatedValue = 90;
	totalTblWidth += calculatedValue;
	tblColumnWidth[5] = calculatedValue;

	calculatedValue = 42;
	totalTblWidth += calculatedValue;
	tblColumnWidth[6] = calculatedValue;

	calculatedValue = 60;
	totalTblWidth += calculatedValue;
	tblColumnWidth[7] = calculatedValue;

%>

<%
	GeneralTable sfl = fw.getTable("s6361screensfl");
	
	if (sfl.count() * 27 > 405) {
		calculatedValue = 10;
		totalTblWidth += calculatedValue;
		tblColumnWidth[8] = calculatedValue;
	}

	savedInds = appVars.saveAllInds();
	S6361screensfl.set1stScreenRow(sfl, appVars, sv);
	double sflLine = 0.0;
	int doingLine = 0;
	int sflcols = 1;
	int linesPerCol = 18;
	smartHF.setSflLineOffset(5);

	int height;
	if (sfl.count() * 27 > 210) {
		height = 210;
	} else {
		height = sfl.count() * 27;
	}
%>
<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
</script>

<div class="panel panel-default">
	 <div class="panel-body">
	     
	  <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s6361' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center></th>
			         		<th><center><%=resourceBundleHandler.gettingValueFromBundle("Name")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Address")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Postcode")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("DOB")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Role")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
									S6361screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S6361screensfl.hasMoreScreenRows(sfl)) {	
										
										
									{	
										if (appVars.ind01.isOn()) {
											sv.select.setReverse(BaseScreenData.REVERSED);
											sv.select.setColor(BaseScreenData.RED);
										}
										if (appVars.ind03.isOn()) {
											sv.select.setInvisibility(BaseScreenData.INVISIBLE);
											sv.select.setEnabled(BaseScreenData.DISABLED);
										}
										if (!appVars.ind01.isOn()) {
											sv.select.setHighLight(BaseScreenData.BOLD);
										} 
									 }
										
									
									String clientName = sv.clntdets.toString().substring(2, 22) + " " + sv.clntdetsEx.toString().substring(2, 22);
									String clientAddress = sv.clntdets.toString().substring(23, 73) + " " + sv.clntdetsEx.toString().substring(22, 73);/*pmujavadiya ILIFE-3212*/
									String clientPostCode = sv.clntdets.toString().substring(74, 84) ;
									String clientNumber = sv.clntdets.toString().substring(87, 95) ;
									String clientDOB = sv.clntdetsEx.toString().substring(74, 84) ;
									String clientSex = sv.clntdetsEx.toString().substring(85, 86) ;
									String clientRole = sv.clntdetsEx.toString().substring(90, 92) ;
										
										
								%>
									<tr id='<%="tablerow"+count%>' >
										<td align="center">	
											<input type="radio" 
														 value='<%= sv.select.getFormData() %>' 
														 onFocus='doFocus(this)' onHelp='return fieldHelp("s6361screensfl" + "." +
														 "select")' onKeyUp='return checkMaxLength(this)' 
														 name='s6361screensfl.select_R<%=count%>'
														 id='s6361screensfl.select_R<%=count%>'
														 onClick="selectedRow('s6361screensfl.select_R<%=count%>')"
														 class="radio"
													 />
										</td>
										<td align="center">									
											<%=clientName %>
										</td>
										<td align="center" >									
											<%= clientAddress%>
										</td>
										<td align="center">									
											<%= clientPostCode%>
										</td>
										<td align="center">									
											<%= clientNumber%>
										</td>
										<td align="center">									
											<%= clientDOB%>
										</td>
										<td align="center">									
											<%= clientSex%>											
										</td>
										<td align="center">									
											<%= clientRole%>										
										</td>
										
											<%if(sfl.count()*27 > 405 ) {%>
											<!-- <td align="center">									
																					
										</td> -->
											<%} %>
									</tr>
								
									<%
									count = count + 1;
									S6361screensfl.setNextScreenRow(sfl, appVars, sv);
									}
									%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>   
	     
	 </div>
</div>
<script>
       $(document).ready(function() {
              $('#dataTables-s6361').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "450px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   true,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script>  
<!-- <script>
	$(document).ready(function() {
		$('#dataTables-s6361').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	});
    

    });
</script> -->
<%@ include file="/POLACommon2NEW.jsp"%>
