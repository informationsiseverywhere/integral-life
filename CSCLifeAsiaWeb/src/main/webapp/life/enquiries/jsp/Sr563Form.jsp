

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR563";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr563ScreenVars sv = (Sr563ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status  ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Paying Register  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status   ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Rate                           ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interim Period                          ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Funds Source (Bank or Self-financed)    ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Installment Option   ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rest Indicator                          ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank Reference       ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loan Duration            ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Interest Calculation Type");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.prat.setReverse(BaseScreenData.REVERSED);
			sv.prat.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.prat.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.prmdetails.setReverse(BaseScreenData.REVERSED);
			sv.prmdetails.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.prmdetails.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.mlinsopt.setReverse(BaseScreenData.REVERSED);
			sv.mlinsopt.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.mlinsopt.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.mlresind.setReverse(BaseScreenData.REVERSED);
			sv.mlresind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.mlresind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.coverc.setReverse(BaseScreenData.REVERSED);
			sv.coverc.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.coverc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.loandur.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.intcaltype.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

	<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
						<table>
						<tr>
						<td>
						<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						</td>
						<td style="min-width:1px">
						
						</td>
						<td style="min-width:46px">
										<%					
						if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="max-width:50px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
						</td>
						<td style="min-width:1px">
						
						</td>
						<td style="min-width:120px">
						
						<%					
							if(!((sv.descrip.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.descrip.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width:120px;max-width: 300px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						</td>
						</tr>																
						</table>
					</div>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Paying Register")%></label>
						<div class="input-group" style="max-width:95px">
						<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("register");
						longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
				  		<%
						longValue = null;
						formatValue = null;
						%>
					</div>
				</div>				
			</div></div>
			
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>	
						<div class="input-group"style="max-width:95px">
						<%					
						if(!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>					
					</div></div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>	
						<div class="input-group"  style="max-width:120px"><%					
						if(!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pstate.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>			
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
						<%
						longValue = null;
						formatValue = null;
						%>					
					</div>
				</div>	</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>	
						<div class="input-group" style="max-width:70px"><%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>			
					<div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
						"blank_cell" : "output_cell" %>'> 
					   <%=	(sv.cntcurr.getFormData()).toString()%>
					</div>	
						<%
						longValue = null;
						formatValue = null;
						%>					
					</div>
				</div>								
			</div>	</div>
			<br>
			
			<HR>				
			
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Rate")%></label>
							<div class="input-group" style="max-width:90px"><%	
							qpsf = fw.getFieldXMLDef((sv.prat).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
						%>
						<input name='prat' 
							type='text'
							
								value='<%=smartHF.getPicFormatted(qpsf,sv.prat) %>'
										 <%
								 valueThis=smartHF.getPicFormatted(qpsf,sv.prat);
								 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								 title='<%=smartHF.getPicFormatted(qpsf,sv.prat) %>'
								 <%}%>
							
							size='<%= sv.prat.getLength()%>'
							maxLength='<%= sv.prat.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(prat)' onKeyUp='return checkMaxLength(this)'  
							
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>' 
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
							
							<% 
								if((new Byte((sv.prat).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.prat).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.prat).getColor()== null  ? 
										"input_cell" :  (sv.prat).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>											
												
					</div></div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Interim Period")%></label>
							<div class="input-group" style="width:80px"><%	
							qpsf = fw.getFieldXMLDef((sv.coverc).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL);
						%>
						<input name='coverc' type='text'
							
								value='<%=smartHF.getPicFormatted(qpsf,sv.coverc) %>'
										 <%
								 valueThis=smartHF.getPicFormatted(qpsf,sv.coverc);
								 if(valueThis!=null&& valueThis.trim().length()>0) {%>
								 title='<%=smartHF.getPicFormatted(qpsf,sv.coverc) %>'
								 <%}%>
							
							size='<%= sv.coverc.getLength()%>'
							maxLength='<%= sv.coverc.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(coverc)' onKeyUp='return checkMaxLength(this)'  
							
								onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
								decimal='<%=qpsf.getDecimals()%>' 
								onPaste='return doPasteNumber(event);'
								onBlur='return doBlurNumber(event);'
							
							<% 
								if((new Byte((sv.coverc).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.coverc).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.coverc).getColor()== null  ? 
										"input_cell" :  (sv.coverc).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>										
												
					</div>
				</div>	</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Funds Source (Bank or Self-financed)")%></label>
						<div class="input-group" style="max-width:60px"><%	
							formatValue = (sv.prmdetails.getFormData()).toString();
						%>
						<input name='prmdetails' type='text'
							
							<%
							
									formatValue = (sv.prmdetails.getFormData()).toString();
							
							%>
								value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
							
							size='<%= sv.prmdetails.getLength()%>'
							maxLength='<%= sv.prmdetails.getLength()%>' 
							onFocus='doFocus(this)' onHelp='return fieldHelp(prmdetails)' onKeyUp='return checkMaxLength(this)'  
							
							
							<% 
								if((new Byte((sv.prmdetails).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
							%>  
								readonly="true"
								class="output_cell"
							<%
								}else if((new Byte((sv.prmdetails).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							%>	
									class="bold_cell" 
							
							<%
								}else { 
							%>
							
								class = ' <%=(sv.prmdetails).getColor()== null  ? 
										"input_cell" :  (sv.prmdetails).getColor().equals("red") ? 
										"input_cell red reverse" : "input_cell" %>'
							 
							<%
								} 
							%>
							>											
												
					</div>
				</div>							
			</div>			
			</div>
			
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Installment Option")%></label>
						<div class="input-group" style="width:65px"><% 
							if((new Byte((sv.mlinsopt).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
						%>					
							<input class="form-control" type="text" placeholder='<%=sv.mlinsopt.getFormData()%>' disabled>
							
							
						<%}else{%>
							<div class="input-group">
								<input name='mlinsopt' id='mlinsopt'
									type='text' 
									value='<%=sv.mlinsopt.getFormData()%>' 
									maxLength='<%=sv.mlinsopt.getLength()%>' 
									size='<%=sv.mlinsopt.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(mlinsopt)' onKeyUp='return checkMaxLength(this)'  
									/>
								<span class="input-group-btn">
					        		<button class="btn btn-info" type="button" onClick="doFocus(document.getElementById('mlinsopt')); doAction('PFKEY04');">Search</button>
					      		</span>
							</div>
								
						<%}%>
						
						<%
						longValue = null;
						formatValue = null;
						%>						
					</div>
				</div></div>
			
				
			<div class="col-md-4">
					<div class="form-group">
					<% if (sv.mrtaFlag.compareTo("N") != 0) { %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("MRTA Reducing Frequency")%></label>
						<div class="input-group" style="width:100px">
				<% 	fieldItem = appVars.loadF4FieldsLong(new String[] { "mlresindvpms" }, sv, "E", baseModel);//ILIFE-8348
				mappedItems = (Map) fieldItem.get("mlresindvpms");
				optionValue = makeDropDownList(mappedItems, sv.mlresindvpms.getFormData(), 2, resourceBundleHandler);
				longValue = (String) mappedItems.get((sv.mlresindvpms.getFormData()).toString().trim()); 
				 
%>
<% 
	if((new Byte((sv.mlresindvpms).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.mlresindvpms).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>

<select name='mlresindvpms' type='list' style="width:140px;"
<% 
	if((new Byte((sv.mlresindvpms).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.mlresindvpms).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.mlresindvpms).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>

<%}else{%>
<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Rest Indicator")%>
</div>


 
<input name='mlresind' 
type='text' 
value='<%=sv.mlresind.getFormData()%>' 
maxLength='<%=sv.mlresind.getLength()%>' 
size='<%=sv.mlresind.getLength()%>'
onFocus='doFocus(this)' onHelp='return fieldHelp(mlresind)' onKeyUp='return checkMaxLength(this)'  

<% 
	if((new Byte((sv.mlresind).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
readonly="true"
class="output_cell" 	 >

<%
	}else if((new Byte((sv.mlresind).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
	
%>	
class="bold_cell" >
 
<a href="javascript:;" onClick="doFocus(document.getElementById('mlresind'));  doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%
	}else { 
%>

class = ' <%=(sv.mlresind).getColor()== null  ? 
"input_cell" :  (sv.mlresind).getColor().equals("red") ? 
"input_cell red reverse" : "input_cell" %>' >

<a href="javascript:;" onClick="doFocus(document.getElementById('mlresind'));  doAction('PFKEY04')"> 
<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
</a>

<%} %>
<%
} 
%>

					</div></div>
				</div> <%-- > --%>

				 <div class="col-md-4">
						<div class="form-group">	
							<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Bank Reference")%></label>
							<div class="input-group" style="max-width:120px">
						    	
<input name='mbnkref' 
type='text'

<%

		formatValue = (sv.mbnkref.getFormData()).toString();

%>
	value='<%= XSSFilter.escapeHtml(formatValue)%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>

size='<%= sv.mbnkref.getLength()%>'
maxLength='<%= sv.mbnkref.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(mbnkref)' onKeyUp='return checkMaxLength(this)'  


<% 
	if((new Byte((sv.mbnkref).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.mbnkref).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.mbnkref).getColor()== null  ? 
			"input_cell" :  (sv.mbnkref).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>	

				      			     </div>
						</div>
				   </div>		
			
			
			</div>		
			<div class="row">	
<%	
			if ((new Byte((sv.loandur).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {%>
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Loan Duration")%></label>
					    		     <div class="input-group">
						    			<%	
			qpsf = fw.getFieldXMLDef((sv.loandur).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.S4VS2);
			
	%>

<input name='loandur' 
type='text'

	value='<%=smartHF.getPicFormatted(qpsf,sv.loandur) %>'
			 <%
	 valueThis=smartHF.getPicFormatted(qpsf,sv.loandur);
	 if(valueThis!=null&& valueThis.trim().length()>0) {%>
	 title='<%=smartHF.getPicFormatted(qpsf,sv.loandur) %>'
	 <%}%>

size='<%= sv.loandur.getLength()%>'
maxLength='<%= sv.loandur.getLength()%>' 
onFocus='doFocus(this)' onHelp='return fieldHelp(loandur)' onKeyUp='return checkMaxLength(this)'  

	onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
	decimal='<%=qpsf.getDecimals()%>' 
	onPaste='return doPasteNumber(event);'
	onBlur='return doBlurNumber(event);'

<% 
	if((new Byte((sv.loandur).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	class="output_cell"
<%
	}else if((new Byte((sv.loandur).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 

<%
	}else { 
%>

	class = ' <%=(sv.loandur).getColor()== null  ? 
			"input_cell" :  (sv.loandur).getColor().equals("red") ? 
			"input_cell red reverse" : "input_cell" %>'
 
<%
	} 
%>
>
 
			      			     </div>
				    		</div>
					</div> <%} %>
				    		<div class="col-md-4"></div>
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Interest Calculation Type")%></label>
							<div class="input-group" style="min-width:100px">
						    		<%	
	if ((new Byte((sv.intcaltype).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {

	if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("SI")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Simple Interest");
	}
	if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("CO")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Compound Interest");
	}
	 
%>

<% 
	if((new Byte((sv.intcaltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>

	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>

<% if("red".equals((sv.intcaltype).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
		
<select name='intcaltype' style="width:140px;" 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(intcaltype)'
	onKeyUp='return checkMaxLength(this)'
					<%
	if((new Byte((sv.intcaltype).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
					%>
	readonly="true"
	disabled
	class="output_cell"
					<%
	}else if((new Byte((sv.intcaltype).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
					%>
					class="bold_cell"
					<%
						} else {
					%>
	class = 'input_cell' 
<%
	} 
%>
>
		
<option value="">--<%=resourceBundleHandler.gettingValueFromBundle("Select")%>--</option>
<option value="SI"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("SI")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Simple Interest")%></option>
<option value="CO"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("CO")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Compound Interest")%></option>
<option value="N/A"<% if(((sv.intcaltype.getFormData()).toString()).trim().equalsIgnoreCase("N/A")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Interest N/A")%></option>		
		
</select>
<% if("red".equals((sv.intcaltype).getColor())){
%>
</div>
					<%
						}
					%>

<%
}longValue = null;} 
%>

				      			     </div>
						</div>
				   </div>		
		    </div>
			
 <Div id='mainForm_OPTS' style='visibility:hidden;'>

</div>	 
		</div>
	</div>


<%@ include file="/POLACommon2NEW.jsp"%>
