<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr5b4";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr5b4ScreenVars sv = (Sr5b4ScreenVars) fw.getVariables();%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accumulated Top Up");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Accumulated Withdrawal");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unpaid Premium");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Overdue Premium");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"NLG Balance");%>
	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract"))%></label>
				<table>
				<tr>
				<td>
				<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
				<%
		longValue = null;
		formatValue = null;
		%>
				</td><td style="padding-left:1px;">
				<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
 
				</td><td style="padding-left:1px;max-width:150px;">
				<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</td>
				</tr>
				</table>
				</div></div></div>
				<br>
				
				<div class="row">
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Accumulated Top up")%></label>
				
				</div></div>
				<div class="col-md-3">
				<div class="form-group">
				<%=smartHF.getHTMLVarExt(fw, sv.tranamt01)%>
				</div></div>
				</div>
				<div class="row">
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Accumulated Withdrawal")%></label>
				
				</div></div>
				<div class="col-md-3">
				<div class="form-group">
				<%=smartHF.getHTMLVarExt(fw, sv.tranamt02)%>
				</div></div>
				</div>
				<div class="row">
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Overdue Premium")%></label>
				
				</div></div>
				<div class="col-md-3">
				<div class="form-group">
				<%=smartHF.getHTMLVarExt(fw, sv.tranamt03)%>
				</div></div>
				</div>
				<div class="row">
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Unpaid Premium")%></label>
				
				</div></div>
				<div class="col-md-3">
				<div class="form-group">
				<%=smartHF.getHTMLVarExt(fw, sv.tranamt04)%>
				</div></div>
				</div>
				<div class="row">
				<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("NLG Balance")%></label>
				
				</div></div>
				<div class="col-md-3">
				<div class="form-group">
				<%=smartHF.getHTMLVarExt(fw, sv.nlgbal)%>
				</div></div>
				</div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				</div></div>



<%@ include file="/POLACommon2NEW.jsp"%>
		