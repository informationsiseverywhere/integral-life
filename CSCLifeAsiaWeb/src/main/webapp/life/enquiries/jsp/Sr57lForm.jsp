<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "Sr57l";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>

<%
	Sr57lScreenVars sv = (Sr57lScreenVars) fw.getVariables();
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------- Surrender Details ----------------------------------");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Eff. Date ");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy loans ");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Net S/V Debt ");
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Policy debt  ");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Cash Deposit ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Other Adjustments ");
%>
<%
	StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tax Imposed ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Total ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Adjustment reason ");
%>


<%
	StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract No ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Risk/Prem Status ");
%>
<%
	 StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "RCD "); 
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Owner ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Life ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "J/Life");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Paid-to-date ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Billed-to-date ");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Plan Suffix No ");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"------------------------- Coverage/Rider Details------------------------------");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Lif Cov Rid Fund Typ Descriptn.    Ccy    Estimated Value        Actual Value ");
%>
<%
	appVars.rollup(new int[]{93});
%>
<%if (appVars.ind18.isOn()) {
	sv.occdateDisp.setInvisibility(BaseScreenData.INVISIBLE);//ILJ-49
}	 %>

<div class="panel panel-default">
	<div class="panel-body">


	<%
		longValue = null;
		formatValue = null;
	%>
	
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
			<table>
			<tr>
			<td>
		 	<%
			 	if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.chdrnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.chdrnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %> 
			 
			 </td>
			 <td>
				 <%
				 	if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.cnttype.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	} else {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.cnttype.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	}
				 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
		 	longValue = null;
		 	formatValue = null;
			 %>
			 </td>
			 <td>
			 
			  <%
			 	if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ctypedes.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ctypedes.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="margin-left: 1px; max-width: 235px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			 </td>
			 </tr>
			 </table>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<!-- ILJ-49 start -->
					<%
						if ((new Byte((sv.occdateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
					%>
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
					<%} else { %>
					 <label><%=resourceBundleHandler.gettingValueFromBundle("RCD")%></label>
						<%} %>
                   <!-- ILJ-49 ends -->
			 <%
			 	if (!((sv.occdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.occdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>' style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
				 	longValue = null;
				 	formatValue = null;
				 %>
	
			</div>
		</div>
							
	</div>
	
	<div class="row">
	
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Risk/Prem Status")%></label>
			 <%
			 	if (!((sv.rstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.rstate.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.rstate.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>'  style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
			<%
			 	if (!((sv.pstate.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.pstate.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.pstate.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>

			</div>
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
			<table>
			<tr>
			<td>
			 <%
			 	if (!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.cownnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.cownnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
		 	longValue = null;
		 	formatValue = null;
			 %> 
			 </td>
			 <td>
					 <%
			 	if (!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ownername.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ownername.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>' style="max-width: 150px;margin-left: 1px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
				</td>
				</tr>
				</table>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Life")%></label>
			<table>
			<tr>
			<td>
			<%
			 	if (!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.lifcnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.lifcnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>' style="width: 100px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
				 	longValue = null;
				 	formatValue = null;
				 %> 
			</td>
			<td>
				 <%
				 	if (!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.linsname.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	} else {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.linsname.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	}
				 %>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell"%>' style="width: 150px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
				 	longValue = null;
				 	formatValue = null;
				 %>
				</td>
				</tr>
				</table>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("J/Life")%></label>
			<table>
			<tr>
			<td>
			<%
			 	if (!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.jlifcnum.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="width: 100px;">
					<!-- ILIFE-6336 -->
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			 </td>
			 <td>
			  <%
			 	if (!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.jlinsname.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.jlinsname.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
							<div
								class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell"%>' style="width: 100px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			 </td>
			 </tr>
			 </table>

			</div>
		</div>
	
	</div>
	
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Paid-to-date")%></label>
			<%
			 	if (!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.ptdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="width: 80px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>


			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Billed-to-date")%></label>
			<%
			 	if (!((sv.btdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.btdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>' style="width: 80px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
				 	longValue = null;
				 	formatValue = null;
				 %>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Plan Suffix No")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
			 	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);
			
			 	if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 100px;">&nbsp;</div> <%
			 	}
			 %>
			</div>
		</div>

	</div>
	
	
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage / Rider Details")%></label>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover"
					id='dataTables-sr57l' width='100%'>
					<thead>
					<tr class='info'>				
					
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Fund")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Type")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Estimated Value")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Actual Value")%></th>
					</tr>
					</thead>
					
					<tbody>
					
					<%
					/* This block of jsp code is to calculate the variable width of the table at runtime.*/
					int[] tblColumnWidth = new int[9];
					int totalTblWidth = 0;
					int calculatedValue = 0;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.life.getFormData()).length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
					} else {
						calculatedValue = (sv.life.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[0] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.coverage.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 12;
					} else {
						calculatedValue = (sv.coverage.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[1] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.rider.getFormData()).length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length()) * 12;
					} else {
						calculatedValue = (sv.rider.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[2] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.fund.getFormData()).length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length()) * 12;
					} else {
						calculatedValue = (sv.fund.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[3] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.fieldType.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length()) * 12;
					} else {
						calculatedValue = (sv.fieldType.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[4] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.shortds.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length()) * 12;
					} else {
						calculatedValue = (sv.shortds.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[5] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.cnstcur.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length()) * 12;
					} else {
						calculatedValue = (sv.cnstcur.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[6] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header8").length() >= (sv.estMatValue.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header8").length()) * 12;
					} else {
						calculatedValue = (sv.estMatValue.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[7] = calculatedValue;
			
					if (resourceBundleHandler.gettingValueFromBundle("Header9").length() >= (sv.actvalue.getFormData())
							.length()) {
						calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header9").length()) * 12;
					} else {
						calculatedValue = (sv.actvalue.getFormData()).length() * 12;
					}
					totalTblWidth += calculatedValue;
					tblColumnWidth[8] = calculatedValue;
				%>
				<%
					GeneralTable sfl = fw.getTable("Sr57lscreensfl");
					int height;
					if (sfl.count() * 27 > 210) {
						height = 210;
					} else {
						height = sfl.count() * 27;
					}
				%>
				
				
					
			<%
				Sr57lscreensfl.set1stScreenRow(sfl, appVars, sv);
				int count = 1;
				while (Sr57lscreensfl.hasMoreScreenRows(sfl)) {
			%>

			<tr class="tableRowTag" id='<%="tablerow" + count%>'>
				<td class="tableDataTag tableDataTagFixed"
					style="width:<%=tblColumnWidth[0]%>px;" align="left"><%=sv.life.getFormData()%>
				</td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[1]%>px;"
					align="left"><%=sv.coverage.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[2]%>px;"
					align="left"><%=sv.rider.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[3]%>px;"
					align="left"><%=sv.fund.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[4]%>px;"
					align="left"><%=sv.fieldType.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[5]%>px;"
					align="left"><%=sv.shortds.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[6]%>px;"
					align="left"><%=sv.cnstcur.getFormData()%></td>
				<td class="tableDataTag" style="width:<%=tblColumnWidth[7]%>px;"
					align="right">
					<%
						sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.estMatValue).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
										%> <%
					 	formatValue = smartHF.getPicFormatted(qpsf, sv.estMatValue,
					 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					 		if (!sv.estMatValue.getFormData().toString().trim().equalsIgnoreCase("")) {
					 			formatValue = formatValue(formatValue);
					 		}
					 %> <%=formatValue%> <%
					 	longValue = null;
					 		formatValue = null;
					 %>
					</td>
					<td class="tableDataTag" style="width:<%=tblColumnWidth[8]%>px;"
										align="right">
										<%
											sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.actvalue).getFieldName());
												//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
										%> <%
					 	formatValue = smartHF.getPicFormatted(qpsf, sv.actvalue,
					 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
					 		if (!sv.actvalue.getFormData().toString().trim().equalsIgnoreCase("")) {
					 			formatValue = formatValue(formatValue);
					 		}
					 %> <%=formatValue%> <%
					 	longValue = null;
					 		formatValue = null;
					 %>
					 </td>
					
					
					
								<%
									count = count + 1;
										Sr57lscreensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	

	<script type="text/javascript">
      $(function() {
        $("table tr:nth-child(even)").addClass("striped");
      });
	</script>

<!--  ILIFE-6336 starts -->	
	<div class="row">
		    <div class="col-md-12">
             <ul class="nav nav-tabs">
                 <li class="active">
                    <a href="#Surrender_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Surrender Details")%></label></a>
                 </li>
                
                 </div></div></ul>
                 
 <!--  ILIFE-6336 ends -->	                
                 
        <div class="tab-content">
            <div class="tab-pane fade in active" id="Surrender_tab">
	<div class="row">	
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Eff. Date")%></label>
			 <%
			 	if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="width: 80px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
			<%
			 	if (!((sv.currcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.currcd.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.currcd.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? 
							"blank_cell" : "output_cell"%>' style="width: 125px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>			
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Net S/V Debt")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.netOfSvDebt).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.netOfSvDebt,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			 	if (!((sv.netOfSvDebt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 125px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;"> </div><%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			</div>
		</div>		
		
	</div>
	
	<div class="row">
	
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Cash Deposit")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.zrcshamt).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.S11VS2);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.zrcshamt,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	if (!((sv.zrcshamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 125px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;" ></div> <%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Policy loans")%></label>
			<%
			 	qpsf = fw.getFieldXMLDef((sv.policyloan).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.policyloan,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	if (!((sv.policyloan.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
				<div class="output_cell" style="width: 125px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;"></div> <%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>

			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Other Adjustments")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.otheradjst).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.otheradjst,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	if (!((sv.otheradjst.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 125px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;"></div> <%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
		</div>
	</div>

		
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Policy debt")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.tdbtamt).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.tdbtamt,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	if (!((sv.tdbtamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 125px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;"> </div>
							<%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Tax Imposed")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.taxamt,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	if (!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
							<div class="output_cell" style="width: 125px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div> <%
			 	} else {
			 %>
			
							<div class="blank_cell" style="width: 125px;"></div> <%
			 	}
			 %> <%
			 	longValue = null;
			 	formatValue = null;
			 %>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Total")%></label>
			 <%
			 	qpsf = fw.getFieldXMLDef((sv.estimateTotalValue).getFieldName());
			 	//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			 	PackedDecimalData temp = new PackedDecimalData(17, 2).init(0);
			 	temp.set(sv.estimateTotalValue);
			 	temp.add(sv.clamant);
			 	sv.estimateTotalValue.set(temp.getFormData());
			 	formatValue = smartHF.getPicFormatted(qpsf, sv.estimateTotalValue,
			 			COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			 	if (!((temp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue(formatValue);
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			 	}
			
			 	if (!formatValue.trim().equalsIgnoreCase("")) {
			 %>
										<div class="output_cell" style="width: 125px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div> <%
			 	} else {
			 %>
			
										<div class="blank_cell" style="width: 125px;">&nbsp;</div> <%
			 	}
			 %>

			</div>
		</div>
		
	</div>
		
	<div class="row">

		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Adjustment reason")%></label>
			<table>
			<tr>
			<td>
			 <%
				 	if (!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.reasoncd.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	} else {
				
				 		if (longValue == null || longValue.equalsIgnoreCase("")) {
				 			formatValue = formatValue((sv.reasoncd.getFormData()).toString());
				 		} else {
				 			formatValue = formatValue(longValue);
				 		}
				
				 	}
				 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>'  style="margin-left: 1px;min-width:140px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>	
			 </td>
			 <td>
			 
			 <%
			 	if (!((sv.resndesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.resndesc.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	} else {
			
			 		if (longValue == null || longValue.equalsIgnoreCase("")) {
			 			formatValue = formatValue((sv.resndesc.getFormData()).toString());
			 		} else {
			 			formatValue = formatValue(longValue);
			 		}
			
			 	}
			 %>
				<div
					class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ?
							"blank_cell" : "output_cell"%>' style="margin-left: 1px;min-width:140px;">
					<%=XSSFilter.escapeHtml(formatValue)%>
				</div> <%
			 	longValue = null;
			 	formatValue = null;
			 %>	
			 </td>
			 </tr>
			 </table>	
 	
			</div>
		</div>
	</div>
<script type="text/javascript">

function change()
{
	document.getElementsByName("resndesc")[0].value="";
	doAction('PFKEY05');
	
}
</script>




	
	
	
	<div style='visibility: hidden;'>
		<table>
			<tr style='height: 22px;'>
				<td width='188'>
					<%
						if (!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.planSuffix.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.planSuffix.getFormData()).toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div> <%
					 	longValue = null;
					 	formatValue = null;
					 %>

				</td>
			</tr>
		</table>
	</div>



	</div>
</div>



<script>
$(document).ready(function() {
	$('#dataTables-sr57l').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

