

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "SA577";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.smart.screens.*" %>
<%
	Sa577ScreenVars sv = (Sa577ScreenVars) fw.getVariables();
%>

 <%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Factoring House");%>
<%appVars.rollup(new int[] {93});%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Factoring House"))%></label>
					<table>
						<tr>
							<td>
								<div
									class='<%=(sv.factHouseNum.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.factHouseNum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.factHouseNum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.factHouseNum.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 									longValue = null;
 									formatValue = null; %> 
							</td>
							<td>
								<div style="margin-left: 1px;"
									class='<%=(sv.factHouseName.getFormData()).trim().length() == 0 ? "blank_cell" : "output_cell"%>'>
									<%
										if (!((sv.factHouseName.getFormData()).toString()).trim().equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.factHouseName.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.factHouseName.getFormData()).toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 									longValue = null;
 									formatValue = null;%> 
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>		
    	 
    	<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sa577Table", {
				fixedCols : 0,					
				colWidths : [70,80,100,100,74,180,100],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
			});

        });
   		</script>

    <div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover"
						id='dataTables-sa577' width='100%'>
    	 	<thead>
	    	 <tr class='info'>
	    	 	<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Seq")%></th>
	            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Extract/upload Date")%></th>
	            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("File Total Amount")%></th>
	            <th style="text-align: center; min-width:80px;"><%=resourceBundleHandler.gettingValueFromBundle("File Total # of records")%></th>
	            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("File Type")%></th>
	            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></th>
	            <th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Extract File")%></th>
	            
	         </tr> 
         </thead>
         <tbody>
             <% 
              GeneralTable sfl = fw.getTable("sa577screensfl");
              GeneralTable sfl1 = fw.getTable("sa577screensfl");
              Sa577screensfl.set1stScreenRow(sfl, appVars, sv); 
              %> 
             <% String backgroundcolor="#FFFFFF";%>	
             <%
              int count = 1;
              boolean hyperLinkFlag;
              while (Sa577screensfl.hasMoreScreenRows(sfl)) {	
              hyperLinkFlag=true;
              %>
              
              <tr id='tr<%=count%>' height="30">
                  <%=smartHF.getHTMLHiddenFieldSFL(sv.screenIndicArea, "screenIndicArea", "sa577screensfl", count)%>
                  <% String fileName =  sv.extractFile.getFormData();
                	 String folderName = sv.ftype.toString().toUpperCase();
	                 String param = "fileName=" + fileName + "&folderName=" + folderName;%>
     
               <td align="center" <%if(!(((BaseScreenData)sv.seq) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.seq, "sa577screensfl", count, sfl, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
              </td>
     
              <td align="center"  <%if(!(((BaseScreenData)sv.dateDisp) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFL(sv.dateDisp, "dateDisp", "sa577screensfl", count)%>
              </td>
     
              <td align="right" <%if(!(((BaseScreenData)sv.fTotalAmt) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.fTotalAmt, "sa577screensfl", count, sfl, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS) %>
              </td>
     
              <td align="center"  <%if(!(((BaseScreenData)sv.fTotalRecords) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFLWithFormater(sv.fTotalRecords, "sa577screensfl", count, sfl, COBOLHTMLFormatter.ZEROSUPPRESS_IGNOREDECIMAL)%>
              </td>
     
              <td align="center"  <%if(!(((BaseScreenData)sv.ftype) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFL(sv.ftype, "ftype", "sa577screensfl", count)%>
              </td>
              
              <td align="center"  <%if(!(((BaseScreenData)sv.userID) instanceof StringBase)) {%> align="right"<% }else {%> align="left" <%}%>>
	          <%=smartHF.getHTMLOutputFieldSFL(sv.userID, "userID", "sa577screensfl", count)%>
              </td>
     
              <td align="center" 
	          <%if(!(((BaseScreenData)sv.extractFile) instanceof StringBase)) {%>align="right"<% }else {%> align="left" <%}%> >	
	          <a href="<%= request.getContextPath() %>/DebitCreditPrintReport?<%=param%>" class = 'tableLink' ><span><%=sv.extractFile.getFormData()%></span></a>					
              </td>
              
              </tr>
                 <%	count = count + 1;
                 Sa577screensfl.setNextScreenRow(sfl, appVars, sv);
                  }
                  %> 
                  
                  </tbody>
          </table>  
           
       </div>     
     </div>
   </div>      
   <style>
     .adisabled,.adisabled:hover
     {
     	color:red !important;
     	
     	text-decoration: none !important;
     }
   </style>
     <script>
   		var resptext="";
   		var element;
   		var event;
            
		$(".tableLink").on('click', function(event) {
			element = this;
			$.get(this, function(responseText) {
				resptext = responseText;
				if (resptext == "not found") {

					$(element).html("File Doesn't Exist");
					$(element).addClass("adisabled");

				} else {
					window.location.replace(element);

				}

			});

			event.preventDefault();

		});
	</script>
</div>
</div>  	


<script>
$(document).ready(function() {
	$('#dataTables-sa577').DataTable({
    	ordering: false,
    	searching:false,
    	scrollY: '350',
        scrollCollapse: true,
  	});	
	
	
	$("div[style*='background-color: rgb(238, 238, 238)']").each(function(){
		if($(this).text().replace(/(^\s+|\s+$)/g, "").length != 0){
			 if( $(this).text().replace(/(^\s+|\s+$)/g, "").length >=9 && $(this).text().replace(/(^\s+|\s+$)/g, "").length <16 ){
				 $(this).css('width','135px');
			
			} 
		}		
	});
	
	
})
</script> 					

<%@ include file="/POLACommon2NEW.jsp"%>