
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6228";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6228ScreenVars sv = (S6228ScreenVars) fw.getVariables();%>
<%{
if (appVars.ind10.isOn()) {
	sv.asgnflag.setInvisibility(BaseScreenData.INVISIBLE);
}
}%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Name")%></label>
		       		<div class="input-group">
		       		<!--
					<%StringData NAME_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>
					<%=smartHF.getLit(0, 0, NAME_LBL).replace("absolute","relative")%>
					-->
						<%if ((new Byte((sv.name).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
							<% if(!((sv.name.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									if(longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue( (sv.name.getFormData()).toString()); 
									} else {
										formatValue = formatValue( longValue);
									}						
								}else{
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.name.getFormData()).toString()); 
									}else{
										formatValue = formatValue( longValue);
									}
								}
							%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 115px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Code   ")%></label>
		       		<table><tr><td>
			       		<!--
						<%StringData S6228_Code_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code   ");%>
						<%=smartHF.getLit(0, 0, S6228_Code_LBL).replace("absolute","relative")%>
						-->
						<%if ((new Byte((sv.reasoncd).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.reasoncd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.reasoncd.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							</td><td style="padding-left:10px;">

								<%if ((new Byte((sv.asgnCodeDesc).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
									<% if(!((sv.asgnCodeDesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.asgnCodeDesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}						
										}else{
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.asgnCodeDesc.getFormData()).toString()); 
											}else{
												formatValue = formatValue( longValue);
											}
										}
									%>
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
								<%}%>
								</td></tr></table>
		       		
		       		</div>
		       	</div>
		       
		       	
		       	
		    	       	
		       	<div class="col-md-1" >
		       		<div class="form-group" style="padding-right: 0%;">
		       		<label><%StringData S6228_From_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"From   ");%>
						<%=smartHF.getLit(0, 0, S6228_From_LBL).replace("absolute","relative")%></label>
		       		<div class="input-group" style="min-width: 110px;">
					<%
						if(browerVersion.equals(IE11)||browerVersion.equals(Chrome)){
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.commfromDisp,(sv.commfromDisp.getLength()), null, true)%>
						<%}
						
						else { %> 
						<%=smartHF.getRichText(0, 0, fw, sv.commfromDisp,(sv.commfromDisp.getLength()), null, true)%>
						<%}%> 
						
						
						
						<%=smartHF.getHTMLCalNSVarExt(fw, sv.commfromDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       	</div>
		       	
		       	<div class="col-md-1" style="padding-right: 0%; padding-left: 0%;">
		       		<div class="form-group">
		       		<label>
						<%StringData S6228_To_LBL=resourceBundleHandler.gettingValueFromBundle(StringData.class,"to ");%>
						<%=smartHF.getLit(0, 0, S6228_To_LBL).replace("absolute","relative")%>
						
						</label>
		       		<div class="input-group" style="min-width: 110px;">
		       		<%
						if(browerVersion.equals(IE11)||browerVersion.equals(Chrome)){
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.commtoDisp,(sv.commtoDisp.getLength()), null, true)%>
						<%}else { %> 
						<%=smartHF.getRichText(0, 0, fw, sv.commtoDisp,(sv.commtoDisp.getLength()), null, true)%>
						<%}%> 
						
						<%=smartHF.getHTMLCalNSVarExt(fw, sv.commtoDisp)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	</div>
		   
		    
					<div class="row">
			        	<div class="col-md-2">
				       		<div class="form-group">
				       		<label><%=resourceBundleHandler.gettingValueFromBundle("Address  ")%></label>
				       		
				       		<%if ((new Byte((sv.cltaddr01).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.cltaddr01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltaddr01.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							<br>
							
							
							<%if ((new Byte((sv.cltaddr02).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.cltaddr02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltaddr02.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
							
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
		<br>
		
								<%if ((new Byte((sv.cltaddr03).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
									<% if(!((sv.cltaddr03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}						
										}else{
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cltaddr03.getFormData()).toString()); 
											}else{
												formatValue = formatValue( longValue);
											}
										}
									%>
									
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
								<%}%>
		
		
		<br>
									<%if ((new Byte((sv.cltaddr04).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
										<% if(!((sv.cltaddr04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}						
											}else{
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cltaddr04.getFormData()).toString()); 
												}else{
													formatValue = formatValue( longValue);
												}
											}
										%>
										
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
									<%}%>
		
		<br>
		
									<%if ((new Byte((sv.cltaddr05).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
										<% if(!((sv.cltaddr05.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}						
											}else{
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.cltaddr05.getFormData()).toString()); 
												}else{
													formatValue = formatValue( longValue);
												}
											}
										%>
										
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 110px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
									<%}%>
							
				       		</div>
				       	</div>
				    </div>
	    
				    <div class="row">
			        	<div class="col-md-2">
				       		<div class="form-group">
				       		<label><%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
				       		<div class="input-group">
		
							<%if ((new Byte((sv.cltpcode).getInvisible())).compareTo(new Byte( BaseScreenData.INVISIBLE)) != 0) {%>
								<% if(!((sv.cltpcode.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}						
									}else{
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cltpcode.getFormData()).toString()); 
										}else{
											formatValue = formatValue( longValue);
										}
									}
								%>
								
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ?"blank_cell" : "output_cell" %>' style="min-width: 115px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							<%}%>
							<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.commtoDisp)%>
				       		</div>
				       		</div>
				       	</div>
				    </div>

	 </div>
</div>




<%@ include file="/POLACommon2NEW.jsp"%>


