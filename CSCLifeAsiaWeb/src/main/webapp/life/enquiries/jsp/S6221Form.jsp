<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6221";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6221ScreenVars sv = (S6221ScreenVars) fw.getVariables();%>

<%if (sv.S6221screenWritten.gt(0)) {%>
	<%S6221screen.clearClassString(sv);%>
	<%StringData generatedText2 = new StringData("A - Client Underwriting Inquiry by Contract");%>
	<%StringData generatedText5 = new StringData("B");%>
	<%StringData generatedText7 = new StringData("-");%>
	<%StringData generatedText6 = new StringData("Underwriting Client Report by Risk Class");%>
	<%StringData generatedText3 = new StringData("Client Number ");%>
	<%sv.clntnum.setClassString("");%>
	<%StringData generatedText4 = new StringData("Action ");%>
	<%sv.action.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind02.isOn()) {
			sv.clntnum.setReverse(BaseScreenData.REVERSED);
			sv.clntnum.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.clntnum.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.action.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.action.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>

<div class="panel panel-default">
<div class="panel-heading">
<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
</div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Client Number")%></label>
		       		<div class="input-group">
							<input name='clntnum' id='clntnum'
						type='text'
						value='<%=sv.clntnum.getFormData()%>'
						maxLength='<%=sv.clntnum.getLength()%>'
						size='<%=sv.clntnum.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(clntnum)' onKeyUp='return checkMaxLength(this)'
						
						<%
							if((new Byte((sv.clntnum).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
						%>
						readonly="true"
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.clntnum).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						
						<span class="input-group-btn">
		        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('clntnum')); doAction('PFKEY04');">
		        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		        			</button>
		      			</span>
						
						<%
							}else {
						%>
						
						class = ' <%=(sv.clntnum).getColor()== null  ?
						"input_cell" :  (sv.clntnum).getColor().equals("red") ?
						"input_cell red reverse" : "input_cell" %>' >
					
						<span class="input-group-btn">
		        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('clntnum')); doAction('PFKEY04');">
		        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		        			</button>
		      			</span>
						
						<%} %>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	 </div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
</div>
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-5">
		       		<div class="radioButtonSubmenuUIG">			 
					<label for="A">
					<%= smartHF.buildRadioOption(sv.action, "action", "A")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Client Underwriting Inquiry by Contract")%>
					</label>					
					</div>	
		       	</div>
		       	
		       		<div class="col-md-5">
		       		<div class="radioButtonSubmenuUIG">			 
					<label for="B">
					<%= smartHF.buildRadioOption(sv.action, "action", "B")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Underwriting Client Report by Risk Class")%>
					</label>					
					</div>
		       	</div>
		       </div>
		       <!-- ICIL-79 starts -->
		       <%
				if ((new Byte((sv.action).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
				%>
		       	<div class="row">
		       	<div class="col-md-5">
		       		<div class="radioButtonSubmenuUIG">			 
					<label for="B">
					<%= smartHF.buildRadioOption(sv.action, "action", "C")%>
					<%=resourceBundleHandler.gettingValueFromBundle("Client Accumulated Risk Amount")%>
					</label>					
					</div>
		       	</div>
		  		</div>
		    <%}%>
		    <!-- ICIL-79 ends-->
	 </div>
</div>

<%}%>

<%if (sv.S6221protectWritten.gt(0)) {%>
	<%S6221protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>


<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>


<%@ include file="/POLACommon2NEW.jsp"%>
