


<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SD5EG";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>

<%Sd5egScreenVars sv = (Sd5egScreenVars) fw.getVariables();%> 
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%{
		if (appVars.ind03.isOn()) {
			sv.indic.setInvisibility(BaseScreenData.INVISIBLE);
		}
	}

	%>
	
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select  1 - Financial Details   2 - Fund Movements");%>
		<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran No ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran Date");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff Date   Code Description");%>
	<%//StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loc");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User ID   ");%>
<%		appVars.rollup(new int[] {93});
%>

	<div class="panel panel-default">

    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							
								 <table><tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
				</td>
				<td>
						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
						 		} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td></tr></table>
					
					</div>
				</div>
				
				<div class="col-md-2">
				<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						
					    
					
					   
					   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   
					   <%
							longValue = null;
							formatValue = null;
							%>						
				</div>	
				</div>	
			
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>
						
					    
					   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>'> 
					   <%=	(sv.cntcurr.getFormData()).toString()%>
					   </div>
					   
					
					   
					   <%
							longValue = null;
							formatValue = null;
							%>						
					</div>
				</div>
			    
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>			
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>					
					</div>
				</div>	
				
		</div>
				
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						
					<table><tr><td>
						<%
							if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifenum.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
</td><td>
						<%
							if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;max-width: 150px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			
			</td></tr></table>
					
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						
		       		<table><tr><td>
		       		<%					
						if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  </td><td>
			  		
						<%					
						if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="margin-left: 1px;width: 100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td></tr></table>
		       				
					</div>
				</div>	
			</div>
			
			</br>
			
			<div class="row">
			
      			<div class="col-md-1">
					<div class="form-group">
						<label  style="white-space: nowrap;">Tran No.</label>
        	
			        	<input name='trannosearch' type='text' size='8'
				        	<%
				        		formatValue = (sv.trannosearch.getFormData()).toString();
					        	if("00000".equalsIgnoreCase(formatValue)){
					    			formatValue="";
					    		}
				        	%>
				        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
				        	<%
				        		if (formatValue != null && formatValue.trim().length() > 0) {
				        	%>
				        		title='<%=formatValue%>' <%}%>
				        		size='<%=sv.trannosearch.getLength()%>'
				        		maxLength='<%=sv.trannosearch.getLength()%>' onFocus='doFocus(this)'
				        		onHelp='return fieldHelp(trannoSearch)'
				        		onKeyUp='return checkMaxLength(this)'
				        		onkeypress='return isNumeric(event)'
				    	        onPaste="return false"
				        	<%
				        		if ((new Byte((sv.trannosearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
				        	%>
				        		readonly="true" class="output_cell"
				        	<%
				        		} else if ((new Byte((sv.trannosearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
				        	%>
				        		class="bold_cell" 
				        	<%
				        		} else {
				        	%>
				        		class=' <%=(sv.trannosearch).getColor() == null ? "input_cell"
										: (sv.trannosearch).getColor().equals("red") ? "input_cell red reverse"
												: "input_cell"%>'
				        	<%}%>
			        	/>
      			</div>
      		</div>

      		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tran Date")%></label>	
					
					
                	<% if ((new Byte((sv.effdatesearchDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {       %>
                             <div class="input-group date col-md-8"><%=smartHF.getRichTextDateInput(fw, sv.effdatesearchDisp)%>                                       
                             </div>
               		<%}else{%>
                           <div class="input-group date form_date col-md-12" data-date=""
                                  data-date-format="dd/mm/yyyy" data-link-field="effdatesearchDisp"
                                  data-link-format="dd/mm/yyyy" style="width: 150px;">
                                         <%=smartHF.getRichTextDateInput(fw, sv.effdatesearchDisp, (sv.effdatesearchDisp.getLength()))%>
                                         <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                           </div>
                                  
             		 <%}%>	        	
		        	<%-- <%=smartHF.getRichTextDateInput(fw, sv.effdatesearchDisp)%>
					<%=smartHF.getHTMLCalNSVarExt(fw, sv.effdatesearchDisp, -2)%> --%>
				</div>
			</div>
      
      		<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tran Description")%></label>
        	
		        	<input name='trandescsearch' type='text' size='30' 
		        		<%
		        		formatValue = (sv.trandescsearch.getFormData()).toString();
		        	%>
		        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
		        	<%
		        		if (formatValue != null && formatValue.trim().length() > 0) {
		        	%>
		        		title='<%=formatValue%>' <%}%>
		        		size='<%=sv.trandescsearch.getLength()%>'
		        		maxLength='<%=sv.trandescsearch.getLength()%>' onFocus='doFocus(this)'
		        		onHelp='return fieldHelp(trandescsearch)'
		        		onKeyUp='return checkMaxLength(this)'
		        	<%
		        		if ((new Byte((sv.trandescsearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
		        	%>
		        		readonly="true" class="output_cell"
		        	<%
		        		} else if ((new Byte((sv.trandescsearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
		        	%>
		        		class="bold_cell" 
		        	<%
		        		} else {
		        	%>
		        		class=' <%=(sv.trandescsearch).getColor() == null ? "input_cell"
								: (sv.trandescsearch).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
		        	<%}%>
		        	/>
      			</div>
      		</div>
      		<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></label>
        	
		        	<input name='crtusersearch' type='text' size='12' 
		        		<%
		        		formatValue = (sv.crtusersearch.getFormData()).toString();
		        	%>
		        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
		        	<%
		        		if (formatValue != null && formatValue.trim().length() > 0) {
		        	%>
		        		title='<%=formatValue%>' <%}%>
		        		size='<%=sv.crtusersearch.getLength()%>'
		        		maxLength='<%=sv.crtusersearch.getLength()%>' onFocus='doFocus(this)'
		        		onHelp='return fieldHelp(crtusersearch)'
		        		onKeyUp='return checkMaxLength(this)'
		        	<%
		        		if ((new Byte((sv.crtusersearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
		        	%>
		        		readonly="true" class="output_cell"
		        	<%
		        		} else if ((new Byte((sv.crtusersearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
		        	%>
		        		class="bold_cell" 
		        	<%
		        		} else {
		        	%>
		        		class=' <%=(sv.crtusersearch).getColor() == null ? "input_cell"
								: (sv.crtusersearch).getColor().equals("red") ? "input_cell red reverse"
										: "input_cell"%>'
		        	<%}%>
		        	/>
	      		</div>
	     	 </div>
			
			</div>
			
			</br>
			
<%
		GeneralTable sfl = fw.getTable("sd5egscreensfl");
		/* int height;
		if(sfl.count()*27 > 360) {
		height = 360 ;
		} else {
		height = sfl.count()*27;
		}
		 */
		%>


		
	
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sd5egTable", {
				fixedCols : 0,					
				colWidths : [70,80,100,100,74,180,100],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
    <script>
	  function isNumeric(evt) 
	  {
	 		evt = (evt) ? evt : window.event;
	 		var charCode = (evt.which) ? evt.which : evt.keyCode;
	 		if(charCode >= 48 && charCode <= 57){
	 			return true;
	 		}else{
	 			return false;
	 		}
	 	}
</script>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover "
							id='dataTables-sd5eg' width='100%'>
							<thead>
								<tr class='info'>		
		
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction Date") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction Number") %></th>
				 
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction Description") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Amount") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Total") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("LB AL%") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Allocation Amt") %></th>
								<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("User ID") %></th>

								</tr>
							</thead>
	
		
		
							<%
					
						String backgroundcolor="#FFFFFF";
						
							Sd5egscreensfl
						.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (Sd5egscreensfl
						.hasMoreScreenRows(sfl)) {
						
						%>	


	<tr style="background:<%= backgroundcolor%>;">
	
	

			<td> <%= sv.effdateDisp.getFormData()%>	</td> 
			<td> <%= sv.fillh.getFormData()%><%= sv.tranno.getFormData()%><%= sv.filll.getFormData()%> </td>
		 
			<td> <%= sv.trandesc.getFormData()%>	</td>
			<td style="text-align: right;">	<b>								
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.tranamt).getFieldName());				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.tranamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
															tranamt
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														formatValue = null;
												%>
					 			 		
									</b></td>
			<td style="text-align: right;">	<b>								
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.total).getFieldName());				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.total,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
															total
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														formatValue = null;
												%>
					 			 		
									</b></td>
			<td> 	<b>								
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.lbalperc).getFieldName());				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.lbalperc,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.lbalperc.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														formatValue = null;
												%>
					 			 		
									</b></td>
			<td style="text-align: right;">	<b>								
											<%	
												sm = sfl.getCurrentScreenRow();
												qpsf = sm.getFieldXMLDef((sv.allocamt).getFieldName());				
											%>
											
																
												<%
													formatValue = smartHF.getPicFormatted(qpsf,sv.allocamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS);
													if(!sv.
															allocamt
													.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
														formatValue = formatValue( formatValue );
													}
												%>
												<%= formatValue%>
												<%
														formatValue = null;
												%>
					 			 		
									</b></td>
			<td> <%= sv.crtuser.getFormData()%>	</td>		
								</tr>


						<%
						if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
							backgroundcolor="#ededed";
							}else{
							backgroundcolor="#FFFFFF";
							}
						count = count + 1;
						Sd5egscreensfl
						.setNextScreenRow(sfl, appVars, sv);
						}
						%>
							</table>
					</div>
				</div>
			</div>
			</div>
			
			
</div>
</div>


<script>
$(document).ready(function() {
	$('#dataTables-sd5eg').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '350',
        scrollCollapse: true,
        paging:   false,
		ordering: false,
        info:     false,
        searching: false,
       
  	});	
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

