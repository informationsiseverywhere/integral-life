<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6259";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6259ScreenVars sv = (S6259ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Number     ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien Code   ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. Fund  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section  ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Section ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Amount      ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Date  ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium       ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage RCD        ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Date     ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann. Proc. Date     ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate Date         ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate From Date        ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Bill Date   ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class     ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl. Method ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ttl Prem w/Tax ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stamp Duty");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Cessation Date  ");%>
	
<%{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind44.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.benBillDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind04.isOn()) {
			sv.optdsc03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind04.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind06.isOn()) {
			sv.optind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.optdsc01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.optind01.setReverse(BaseScreenData.REVERSED);
			sv.optind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.optind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.optdsc02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}		
		if (appVars.ind42.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.optdsc04.setInvisibility(BaseScreenData.INVISIBLE);
		}		
		if (appVars.ind46.isOn()) {
			sv.optind04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind43.isOn()) {
			sv.optind04.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind46.isOn()) {
			sv.optind04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind46.isOn()) {
			sv.optind04.setHighLight(BaseScreenData.BOLD);
		}
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind17.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind17.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind18.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind18.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind53.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 START */
		if (appVars.ind58.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind59.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//BRD-009
		if (appVars.ind66.isOn()) {
			sv.statcode.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) {
			sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-009
		/*ILIFE-6968 start*/
		if (!appVars.ind121.isOn()) {
			sv.lnkgno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind122.isOn()) {
			sv.lnkgsubrefno.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*ILIFE-6968 end*/
		if (appVars.ind50.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-45 - START
		if (appVars.ind77.isOn()) {
			sv.crrcdDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* if (appVars.ind78.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		if (appVars.ind79.isOn()) {
			sv.riskCessDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-45 - END
	}

	%>


	<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
				<div class="col-md-6">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
						<table>
						<tr>
						<td>
							
		<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:60px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td>
	<td>





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td>
<td>

	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:300px;margin-left: 1px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

																									
					</td>
					</tr>
					</table>
					</div>
				</div>
			</div>
			
			<div class="row">        
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						<div class="input-group">
								<%					
								if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="width:110px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
						</div>					
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<div class="input-group">
	
					  		
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="width:110px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
						</div>					
					</div>
				</div>				
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<div class="input-group">
						<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("cntcurr");
								longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
							%> 
								
						  		
								<%					
								if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="width:110px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
  
		
								</div>					
					</div>
				</div>				
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
						<div class="input-group">

								<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
							%>
							
						  		
								<%					
								if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.register.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="width:110px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
		
						</div>										
					</div>
				</div>	
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
						<div class="input-group">
	
  		
								<%					
								if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
											style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"padding-right: 50px" : "width:50px;" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>		
								<%
								longValue = null;
								formatValue = null;
								%>
						  
	
						</div>									
					</div>
				</div>					
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
						<div class="input-group">
	
					  		
							<%	
								qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
								
								if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="width:50px;">
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" style="padding-right: 50px;width:50px; "> &nbsp; </div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
	
 								
					</div>
					</div>
				</div>								
			</div>	
			
			<div class="row">        
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Number")%></label>
						<div class="input-group">
	
  		
								<%					
								if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.life.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.life.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'style="width:50px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  
						</div>					
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage Number")%></label>
						<div class="input-group">
					
				  		
						<%					
						if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'style="width:50px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
	
	
						</div>				
					</div>
				</div>				
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Rider Number")%></label>
						<div class="input-group">
	
  		
							<%					
							if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rider.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rider.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="width:50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
	
						</div>				
					</div>
				</div>	
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. Fund")%></label>
						<div class="input-group">
								
							<%	
								fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("statFund");
								longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
							%>
							
							
								 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="width:50px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
									<%
									longValue = null;
									formatValue = null;
									%>
  
	
						</div>			
					</div>
				</div>				
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
						<div class="input-group">
							<%					
							if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="width:50px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						</div>		
					</div>
				</div>									
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Sub Section")%></label>
						<div class="input-group">
				 		
									<%					
									if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'style="width:50px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  
						</div>
					</div>
				</div>					
			</div>						
				
			<div class="row">        
				<div class="col-md-5">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>	
						<table>
						<tr>
						<td>
						<%					
							if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
											style="min-width:65px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
					</td>
					
					<td style="padding-left: 1px;">
						
					  		
							<%					
							if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
										style="min-width:65px;max-width:200px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>									
						</td>
						</tr>
						</table>
					</div>
				</div>
				<div class="col-md-3"></div>
				
				<div class="col-md-2">
					<div class="form-group"> 
					<%					
					if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
					%>	
						<label id='zagelit'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>	
						<div class="input-group" style="max-width: 80px;">				
					<%
					longValue = null;
					formatValue = null;
					%>					

					<%	
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
						
						if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
								if(longValue == null || longValue.equalsIgnoreCase("")) { 			
								formatValue = formatValue( formatValue );
								} else {
								formatValue = formatValue( longValue );
								}
						}
				
						if(!formatValue.trim().equalsIgnoreCase("")) {
					%>
						<input class="form-control" type="text" placeholder='<%=formatValue%>' disabled>
					<%
						} else {
					%>
						<input class="form-control" type="text" placeholder='' disabled>
					<% 
						} 
					%>
					<%
					longValue = null;
					formatValue = null;
					%>						
					</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group"> 
					<%if ((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>	
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("statcode");
							optionValue = makeDropDownList( mappedItems , sv.statcode.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.statcode.getFormData()).toString().trim());  
						%>			
						
						<% 
							if((new Byte((sv.statcode).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
						%>  
							<% 
							if(longValue != null)
							{
							%>
								<input class="form-control" type="text" placeholder='<%=longValue%>' disabled>
							<%
							}else{
							%>
								<input class="form-control" type="text" placeholder='' disabled>
							<%}%>				
					
						<%
						longValue = null;
						%>
						<% }else {	%>
							<% 
								if((new Byte((sv.statcode).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%> 			
								<input class="form-control" type="text" placeholder=<%=longValue%> disabled>
							<% }else {%>
								<select name='statcode' id='statcode' 
								class = ' <%= (sv.statcode).getColor().equals("red") ? "input_cell red" : "input_cell" %>' >
									<%=optionValue%>					
								</select>	
							<%}%>													
						<%
						}}
						%>								
					</div>
				</div>		
				</div>
				<div class="row">		
				<div class="col-md-4" style="text-align: left;">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table>
						<tr>
						<td>
										<%					
						if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
										style="min-width:65px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
				  
					</td>
					<td style="padding-left: 1px;">
				
				  		
						<%					
						if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
									style="min-width:65px;max-width:200px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>		
						<%
						longValue = null;
						formatValue = null;
						%>			
						</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
		<!-- IBPLIFE-2141 start -->
		<%if (sv.nbprp126lag.compareTo("Y") == 0) {%>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assure / Premium")%></a>
					</li>

					<li><a href="#prem_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Cover Notes")%></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="basic_tab">
		<%} else {%>
		<hr>
		<%}%>
		<!-- IBPLIFE-2141 end -->
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Amount")%></label>
							<table>
								<tr>
									<td>
										<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
		//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			
			if(!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
										<div class="output_cell"
											style="width: 120px; text-align: right;">
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div> <%
			} else {
		%>

										<div class="blank_cell" style="width: 60px;">&nbsp;</div> <% 
			} 
		%> <%
		longValue = null;
		formatValue = null;
		%>

									</td>
									<td>
										<%					
		if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
										<div
											class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
											style="margin-left: 1px; max-width: 150px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div> <%
		longValue = null;
		formatValue = null;
		%>

									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">

							<%
								//ILJ-386
								if (sv.cntEnqScreenflag.compareTo("Y") == 0) {
							%>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
							<%
								} else if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
							<%} else { %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage RCD"))%></label>
							<%} %>
							<!-- ILJ-45 End -->


							<%					
							if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							longValue = null;
							formatValue = null;
							%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>


							<%	
							
							fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("mortcls");
							longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim()); 
										
							if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"width:82px;" : "width:140px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							longValue = null;
							formatValue = null;
							%>


						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%></label>


							<%					
								if(!((sv.liencd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"width:82px;" : "width:140px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
								%>


						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<!-- ILJ-45 Starts -->
							<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
							<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
							<%} else { %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
							<%} %>
							<!-- ILJ-45 End -->
							<table>
								<tr>
									<td>
										<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
										<%if(sv.riskCessAge.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.riskCessAge, 0, 60)%>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.riskCessAge, 0, 60)%>
										<%} %> <%}else {%> <%}%>
									</td>
									<td style="padding-left: 1px;">
										<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm,( sv.riskCessTerm.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
										<%if(sv.riskCessTerm.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.riskCessTerm, 0, 80) %>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.riskCessTerm, 0, 80) %>
										<%} %> <%}%>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<!-- ILJ-45 Starts -->

							<%-- <% if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
							<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation Date")%></label>

							<%} else { %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cessation date"))%></label>
							<%} %>
							<!-- ILJ-45 End -->


							<%					
							if(!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							longValue = null;
							formatValue = null;
							%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Ann. Proc. Date")%></label>


							<%					
		if(!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) { %>
							<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Joint Life (J/L)")%></label>

							<%}%>
							<div class="input-group" style="max-width: 80px;">
								<%					
								if(!((sv.select.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.select.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.select.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
									style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"width: 50px" : "width:50px;" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>
								<%
								longValue = null;
								formatValue = null;
								%>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
							<table>
								<tr>
									<td>
										<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
										<%if(sv.premCessAge.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.premCessAge, 0, 60)%>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.premCessAge, 0, 60)%>
										<%} %> <%}else {%> <%}%>
									</td>
									<td style="padding-left: 1px;">
										<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
										<%if(sv.premCessTerm.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.premCessTerm, 0, 80)%>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.premCessTerm, 0, 80)%>
										<%} %> <%}else {%> <%}%>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Cessation Date")%></label>


							<%					
		if(!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate Date")%></label>


							<%					
		if(!((sv.rerateDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
							"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
		longValue = null;
		formatValue = null;
		%>

						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>
								<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> <%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%>
								<%}%>
							</label>

							<%	
								if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {
								fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("dialdownoption");
								optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
								longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
							%>

							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>'>
								<%if(longValue != null){%>

								<%=longValue%>

								<%}%>
							</div>

							<%
							longValue = null;
							%>

							<% }%>

						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Bene cess Age / Term")%></label>
							<table>
								<tr>
									<td>
										<%if(((BaseScreenData)sv.benCessAge) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.benCessAge,( sv.benCessAge.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.benCessAge) instanceof DecimalData){%>
										<%if(sv.benCessAge.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.benCessAge, 0, 60)%>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.benCessAge, 0, 60)%>
										<%} %> <%}else {%> <%}%>
									</td>
									<td style="padding-left: 1px;">
										<%if(((BaseScreenData)sv.benCessTerm) instanceof StringBase) {%>
										<%=smartHF.getRichText(0,0,fw,sv.benCessTerm,( sv.benCessTerm.getLength()+1),null)%>
										<%}else if (((BaseScreenData)sv.benCessTerm) instanceof DecimalData){%>
										<%if(sv.benCessTerm.equals(0)) {%> <%=smartHF.getHTMLVarExt(fw, sv.benCessTerm, 0, 80)%>
										<%} else { %> <%=smartHF.getHTMLVarExt(fw, sv.benCessTerm, 0, 80)%>
										<%} %> <%}else {%> <%}%>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Cess Date")%></label>


							<%					
							if(!((sv.benCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.benCessDateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.benCessDateDisp.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							longValue = null;
							formatValue = null;
							%>


						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate From Date")%></label>


							<%					
						if(!((sv.rerateFromDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
							<div
								class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'
								style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"width:80px;" : "width:80px;" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
						longValue = null;
						formatValue = null;
						%>

						</div>
					</div>
					<!-- fwang3 start -->
					<div class="col-md-3">
						<div class="form-group">
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Payout Option")%>
							</label>
							<%
								fieldItem = appVars.loadF4FieldsLong(new String[] { "payoutoption" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("payoutoption");
								longValue = (String) mappedItems.get((sv.payoutoption.getFormData()).toString().trim());
							%>
							<div class='output_cell' style="width: 180px;">
								<%=longValue == null ? "" : longValue%>
							</div>
							<%longValue = null;%>
						</div>
					</div>
					<!-- fwang3 end -->
				</div>

				<%
									if (((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0)
											|| ((new Byte((sv.waitperiod).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0)) {
								%>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
							<%}%>
							<%	
						if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) {
						fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("prmbasis");
						optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
						longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
					%>

							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
								style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
												"width:82px;" : "width:140px;" %>'>
								<%if(longValue != null){%>

								<%=longValue%>

								<%}%>
							</div>

							<%
					longValue = null;
					%>

							<% }%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>
							<%}%>
							<%	
							if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							fieldItem=appVars.loadF4FieldsLong(new String[] {"waitperiod"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("waitperiod");
							optionValue = makeDropDownList( mappedItems , sv.waitperiod.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());  
						%>



							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'
								style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"width:82px;" : "width:140px;" %>'>
								<%if(longValue != null){%>

								<%=longValue%>

								<%}%>
							</div>

							<%
						longValue = null;
						%>

							<% } 
						%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>
							<%}%>
							<%	
							if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {
							fieldItem=appVars.loadF4FieldsLong(new String[] {"bentrm"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("bentrm");
							optionValue = makeDropDownList( mappedItems , sv.bentrm.getFormData(),2,resourceBundleHandler);  
							longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());  
						%>
							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'
								style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"width:82px;" : "width:140px;" %>'>
								<%if(longValue != null){%>

								<%=longValue%>

								<%}%>
							</div>

							<%
						longValue = null;
						%>

							<% } 
						%>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
							<%}%>
							<%	
	if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
	fieldItem=appVars.loadF4FieldsLong(new String[] {"poltyp"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("poltyp");
	optionValue = makeDropDownList( mappedItems , sv.poltyp.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());  
%>



							<div
								class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'
								style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>
								<%if(longValue != null){%>

								<%=longValue%>

								<%}%>
							</div>

							<%
longValue = null;
%>

							<% }%>

						</div>
					</div>
				</div>
				<%}%>


				<hr>
				<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0){ %>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
							<%}%>
							<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
							<%if(sv.adjustageamt.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%>
							</label>
							<%} %>
							<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
							<%if(sv.rateadj.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%>
							</label>
							<%} %>
							<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
							<%if(sv.fltmort.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%>
							</label>
							<%} %>
							<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
							<%if(sv.loadper.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
				</div>
				<%}%>

				<div class="row">
					<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0){ %>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
							<%}%>
							<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
							<%if(sv.premadj.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
					<%}%>
					<div class="col-md-3">
						<div class="form-group">
							<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
							<%} else { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
							<%} %>
							<%	
								qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								
								if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
							<div class="output_cell"
								style="width: 140px; text-align: right; padding-right: 4px;"
								id="zlinstprem">
								<%= formatValue%>
							</div>
							<%
								} else {
							%>

							<div class="blank_cell" style="width: 82px;" id="zlinstprem">
								&nbsp;</div>

							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
						</div>
					</div>
					<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
							BaseScreenData.INVISIBLE)) != 0){ %>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%>
							</label>
							<%} %>
						</div>
						<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null)%>
						<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
						<%if(sv.zbinstprem.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px; \' ") %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'min-width: 145px;  text-align: right;\' ")%>
						<%} %>
						<%}else {%>
						<%}%>
					</div>
					<%}%>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<label> <%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%>
							</label>
							<%} %>
							<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null)%>
							<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
							<%if(sv.zstpduty01.equals(0)) {%>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
							<%} else { %>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>
							<%} %>
							<%}else {%>
							<%}%>
						</div>
					</div>
					<div class="col-md-3"></div>
					<!-- ILIFE-6968 start-->

					<%
				if ((new Byte((sv.lnkgno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Number")%></label>
							<div style="width: 150px;">
								<%
							if (((BaseScreenData) sv.lnkgno) instanceof StringBase) {
						%>
								<%=smartHF.getRichText(0, 0, fw, sv.lnkgno, (sv.lnkgno.getLength() + 1), null)
							.replace("absolute", "relative")%>
								<%
							} else if (((BaseScreenData) sv.lnkgno) instanceof DecimalData) {
						%>
								<%
							if (sv.lnkgno.equals(0)) {
						%>
								<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
								<%
							} else {
						%>
								<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
								<%
							}
						%>
								<%
							} else {
						%>
								<%
							}
						%>
							</div>
						</div>
					</div>
					<%
				}
			%>
					<!--ILIFE-6968 end -->
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>


							<%	
									qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									
									if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
							<div class="output_cell" style="width: 145px; text-align: right;"
								id="instPrem">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
									} else {
								%>

							<div class="blank_cell" style="width: 145px;" id="instPrem">
								&nbsp;</div>

							<% 
									} 
								%>
							<%
								longValue = null;
								formatValue = null;
								%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) { %>
							<% //ILJ-387
								if (sv.cntEnqScreenflag.compareTo("N") == 0){ %>
							<label> <!-- ILIFE-1062 Start --> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with tax")%>
								<!-- ILIFE-1062 End -->
							</label>
							<%}%>
							<%}%>

							<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<% //ILJ-387
								if (sv.cntEnqScreenflag.compareTo("N") == 0){ %>
							<%	
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							//ILIFE-1783
							formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
							<div class="output_cell" style="width: 145px; text-align: right;"
								id="taxamt">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
							} else {
						%>
							<div class="blank_cell" style="width: 145px;" id="taxamt">
								&nbsp;</div>
							<% 
							} 
						%>
							<%
						longValue = null;
						formatValue = null;
						%>
							<%}%>
							<%}%>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%></label>

							<%	
									qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
								//	qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									
									if(!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
							<div class="output_cell" style="width: 82px; text-align: right;"
								id="singlePremium">
								<%= XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
									} else {
								%>

							<div class="blank_cell" style="width: 82px;" id="singlePremium">
								&nbsp;</div>

							<% 
									} 
								%>
							<%
								longValue = null;
								formatValue = null;
								%>
						</div>
					</div>
					<!--ILIFE-6968 start-->
					<%
				if ((new Byte((sv.lnkgsubrefno).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
			%>
					<div class="col-md-3">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Linkage Sub Ref Number")%></label>
							<div style="width: 150px;">
								<%
							if (((BaseScreenData) sv.lnkgsubrefno) instanceof StringBase) {
						%>
								<%=smartHF.getRichText(0, 0, fw, sv.lnkgsubrefno, (sv.lnkgsubrefno.getLength() + 1), null)
							.replace("absolute", "relative")%>
								<%
							} else if (((BaseScreenData) sv.lnkgsubrefno) instanceof DecimalData) {
						%>
								<%
							if (sv.lnkgsubrefno.equals(0)) {
						%>
								<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
								<%
							} else {
						%>
								<%=smartHF
								.getHTMLVar(0, 0, fw, sv.lnkgsubrefno,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
								<%
							}
						%>
								<%
							} else {
						%>
								<%
							}
						%>
							</div>
						</div>
					</div>
					<%
				}
			%>
					<!--ILIFE-6968 end -->
				</div>
			<!-- IBPLIFE-2141 start -->
           	<% if (sv.nbprp126lag.compareTo("Y") == 0){%>	
           	</div>
           <div class="tab-pane fade" id="prem_tab">
           		<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Transaction")%></label>
								<table>
								<tr>
								<td>
		<%					
		if(!((sv.trancd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trancd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trancd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		
		</td>
		<td>
				<%					
				if(!((sv.trandesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
					if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.trandesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
					
					
			} else  {
						
			if(longValue == null || longValue.equalsIgnoreCase("")) {
						formatValue = formatValue( (sv.trandesc.getFormData()).toString()); 
					} else {
						formatValue = formatValue( longValue);
					}
			
			}
			%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px; width:200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
<%
longValue = null;
formatValue = null;
%>
  </td>
							</tr>
							</table>
						</div>
				  </div>
					<div class="col-md-4">
						<div class="form-group">
							<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
							<%					
						if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:1px; width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
							<%
						longValue = null;
						formatValue = null;
						%>
						</div>
					</div>					
				</div>
             
             <div class="row">
             	<div class="col-md-9">
             	<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Cover Purpose")%></label>

					<table><tr>
							 <td width='251' style="padding-right: 40px; padding-top: 4px;">
							
				
				 <textarea rows="3" cols="60"  onfocus="this.blur()" readonly="readonly"  class="text-left" style="background-color:#eeeeee; border: 2px solid #ccc !important;resize:none;font-weight:bold;font-size: 13px !important; ">
<%=sv.covrprpse.getFormData()%>
</textarea>
	</td></tr>
							
							</table>
					
              </div>
             </div>
             </div>
             
             
             </div>
             </div>
             <%}%>
			<!-- IBPLIFE-2141 end -->
			</div>
		</div>

		<div  id='mainForm_OPTS' style='visibility:hidden'>
	<li>
		<input name='optind01' id='optind01' type='hidden'  value="<%=sv.optind01.getFormData()%>">
	
		<a href="javascript:;"
		onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind01"))'>
			<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc01.getFormData() )%>
		
			<!-- icon -->
			<%
			if (sv.optind01.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.optind01.getFormData().equals("X")) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>  
			<%}%>
		</a>
	</li>

	<li>
		<input name='optind02' id='optind02' type='hidden'  value="<%=sv.optind02.getFormData()%>">
		
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))'>
			<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc02.getFormData() )%>		
		
			<%if (sv.optind02.getFormData().equals("+")) {%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.optind02.getFormData().equals("X")) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>  
			<%}%>
		</a>

	</li>

	<li>
		<input name='optind04' id='optind04' type='hidden'  value="<%=sv.optind04.getFormData()%>">
		<%if (sv.optind04.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind04.getEnabled() != BaseScreenData.DISABLED){%>
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind04"))'>
			<%=resourceBundleHandler.gettingValueFromBundle( sv.optdsc04.getFormData() )%>	
			
			<%
			if (sv.optind04.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.optind04.getFormData().equals("X") ) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>
			<%}%>
		</a>
		<%} %>
	</li>
	
	<li>
		<input name='optind05' id='optind05' type='hidden'  value="<%=sv.optind05.getFormData()%>">
		<%if (sv.optind05.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind05.getEnabled() != BaseScreenData.DISABLED){%>
		<a href="javascript:;"
			onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind05"))'>
			<%=resourceBundleHandler.gettingValueFromBundle( sv.optdsc05.getFormData() )%>	
			
			<%
			if (sv.optind05.getFormData().equals("+")) {
			%>
			<i class="fa fa-tasks fa-fw sidebar-icon"></i>
			<%}%>
			<%if (sv.optind05.getFormData().equals("X") ) {%>
			<i class="fa fa-warning fa-fw sidebar-icon"></i>
			<%}%>
		</a>
		<%} %>
	</li>
	
</div>							
					
							
<div style='visibility:hidden;'><table>
<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>

	
  		
		<%	
						
		if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td>

</tr>

<tr style='height:22px;'><td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Benefit Bill Date")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.benBillDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:140px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

</td>

<td width='188'>

<div class="label_txt">
<%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl. Method")%>
</div>


<br/>

	
  		
		<%					
		if(!((sv.bappmeth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bappmeth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bappmeth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:80px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td></tr>
</table></div>


<script type="text/javascript">
$(document).ready(function(){
$("div[style*='background-color: rgb(238, 238, 238)']").each(function(){
	if($(this).text().replace(/(^\s+|\s+$)/g, "").length != 0){
		if($(this).text().replace(/(^\s+|\s+$)/g, "").length > 10  && $(this).text().replace(/(^\s+|\s+$)/g, "").length <= 20 ){
			$(this).css('width','140px');//reduce size amount
		}
	}
	
});
});
</script>

		
<%@ include file="/POLACommon2NEW.jsp"%>

