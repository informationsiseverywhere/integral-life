<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6363";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6363ScreenVars sv = (S6363ScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Contract Details");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Client Enquiry");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action               ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No          ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client No            ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Surname              ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Forename             ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Role                 ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Date of Birth        ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sex                  ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Post Code            ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.clttwo.setReverse(BaseScreenData.REVERSED);
			sv.clttwo.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.clttwo.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind04.isOn()) {
			sv.surname.setReverse(BaseScreenData.REVERSED);
			sv.surname.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.surname.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind05.isOn()) {
			sv.givname.setReverse(BaseScreenData.REVERSED);
			sv.givname.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.givname.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind06.isOn()) {
			sv.role.setReverse(BaseScreenData.REVERSED);
			sv.role.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind06.isOn()) {
			sv.role.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			sv.cltdobDisp.setReverse(BaseScreenData.REVERSED);
			sv.cltdobDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind07.isOn()) {
			sv.cltdobDisp.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind08.isOn()) {
			sv.cltsex.setReverse(BaseScreenData.REVERSED);
			sv.cltsex.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind08.isOn()) {
			sv.cltsex.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind09.isOn()) {
			sv.cltpcode.setReverse(BaseScreenData.REVERSED);
			sv.cltpcode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.cltpcode.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
<div class="panel-heading">
<%=resourceBundleHandler.gettingValueFromBundle("Input")%>
</div>
<div class="panel-body">
				<div class="row">
						<div class="col-md-4"> 
						    		<div class="form-group">  	  
							    		<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>

						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<div class="input-group" style="min-width:110px;">
					<%}%>
						
						<%if ((new Byte((sv.chdrsel).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						
						<%
						
							longValue = sv.chdrsel.getFormData();
						%>
						
						<%
							if((new Byte((sv.chdrsel).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
						%>
						<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
													"blank_cell" : "output_cell" %>' >
							   		<%if(longValue != null){%>
						
							   		<%=longValue%>
						
							   		<%}%>
							   </div>
						
						<%
						longValue = null;
						%>
						<% }else {%>
						<input name='chdrsel' id='chdrsel'
						type='text'
						value='<%=sv.chdrsel.getFormData()%>'
						maxLength='<%=sv.chdrsel.getLength()%>'
						size='<%=sv.chdrsel.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(chdrsel)' onKeyUp='return checkMaxLength(this)'
						
						<%
							if((new Byte((sv.chdrsel).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
						%>
						readonly="true" 
						class="output_cell"	 >
						
						<%
							}else if((new Byte((sv.chdrsel).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						
						%>
						class="bold_cell" >
						
						<span class="input-group-btn">
		        			<button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
		        				<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
		        			</button>
		      			</span>
						
						
						
						<%
							}else {
						%>
						
						class = ' <%=(sv.chdrsel).getColor()== null  ?
						"input_cell" :  (sv.chdrsel).getColor().equals("red") ?
						"input_cell red reverse" : "input_cell" %>' >
						<!-- ILIFE2448 starts -->
						<span class="input-group-btn">
					        <button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04');">
					        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					        </button>
						</span>
						<!-- ILIFE2448 ends -->
						<%}longValue = null;}} %>
						
						</div>
						</div>
				</div>
						
						<div class="col-md-4"></div>
							  <div class="col-md-4">
						        	<div class="form-group">
						        	<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
						        	<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
						        <div class="input-group" style="min-width:110px;">
						        
						        	<%}%>
						        	<%if ((new Byte((sv.clttwo).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
							
							<%
							
								longValue = sv.clttwo.getFormData();
							%>
							
							<%
								if((new Byte((sv.clttwo).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
							%>
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
														"blank_cell" : "output_cell" %>'>
								   		<%if(longValue != null){%>
							
								   		<%=longValue%>
							
								   		<%}%>
								   </div>
							
							<%
							longValue = null;
							%>
							<% }else {%>
							<input name='clttwo' id='clttwo'
							type='text'
							value='<%=sv.clttwo.getFormData()%>'
							maxLength='<%=sv.clttwo.getLength()%>'
							size='<%=sv.clttwo.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(clttwo)' onKeyUp='return checkMaxLength(this)'
							
							<%
								if((new Byte((sv.clttwo).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
							%>
							readonly="true"
							class="output_cell"	 >
							
							<%
								}else if((new Byte((sv.clttwo).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
							
							%>
							class="bold_cell" >
							<span class="input-group-btn">
							        <button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04');">
							        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							        </button>
							</span>
							
														
							<%
								}else {
							%>
							
							class = ' <%=(sv.clttwo).getColor()== null  ?
							"input_cell" :  (sv.clttwo).getColor().equals("red") ?
							"input_cell red reverse" : "input_cell" %>' >
							
							<span class="input-group-btn">
							        <button class="btn btn-info" style="font-size: 20px;" type="button" onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04');">
							        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							        </button>
							 </span>
							
							
							
							<%}longValue = null;}} %>
										        	
										        	</div>
							</div>
							</div>
						</div>

		<hr>
			<div class="row">
					<div class="col-md-4">
			        	<div class="form-group">
			        	
			        	<label><%=resourceBundleHandler.gettingValueFromBundle("Client Search (Client Enquiry Only)")%></label>
			       
			       
			       </div>
			       </div>
			       </div>
			<div class="row">			       
				<div class="col-md-4">
			        	<div class="form-group" style="width: 250px;">
			        	<label><%=resourceBundleHandler.gettingValueFromBundle("Surname")%></label>
			       <%if ((new Byte((sv.surname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>

						&nbsp;&nbsp;
						<input name='surname'
						type='text'
						
						<%if((sv.surname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.surname.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.surname.getLength()%>'
						maxLength='<%= sv.surname.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(surname)' onKeyUp='return checkMaxLength(this)'
						
						
						<%
							if((new Byte((sv.surname).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){
						%>
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.surname).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>
								class="bold_cell"
						
						<%
							}else {
						%>
						
							class = ' <%=(sv.surname).getColor()== null  ?
									"input_cell" :  (sv.surname).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						
						<%
							}
						%>
						>
						<%}%>
			       
			       </div>
			       </div>
					<div class="col-md-4">
			        	<div class="form-group" style="width: 200px;">
			        	<label><%=resourceBundleHandler.gettingValueFromBundle("Forename")%></label>
			        	<%if ((new Byte((sv.givname).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>


						<input name='givname'
						type='text'
						
						<%if((sv.givname).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
						
						<%
						
								formatValue = (sv.givname.getFormData()).toString();
						
						%>
							value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
						
						size='<%= sv.givname.getLength()%>'
						maxLength='<%= sv.givname.getLength()%>'
						onFocus='doFocus(this)' onHelp='return fieldHelp(givname)' onKeyUp='return checkMaxLength(this)'
						
						
						<%
							if((new Byte((sv.givname).getEnabled()))
							.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){
						%>
							readonly="true"
							class="output_cell"
						<%
							}else if((new Byte((sv.givname).getHighLight())).
								compareTo(new Byte(BaseScreenData.BOLD)) == 0){
						%>
								class="bold_cell"
						
						<%
							}else {
						%>
						
							class = ' <%=(sv.givname).getColor()== null  ?
									"input_cell" :  (sv.givname).getColor().equals("red") ?
									"input_cell red reverse" : "input_cell" %>'
						
						<%
							}
						%>
						>
						<%}%>
					</div>
				</div>

				
							<div class="col-md-4">
			        			<div class="form-group">
					        	<label><%=resourceBundleHandler.gettingValueFromBundle("Role")%></label>
									<%
										if ((new Byte((sv.role).getInvisible())).compareTo(new Byte(
																	BaseScreenData.INVISIBLE)) != 0) {
										fieldItem=appVars.loadF4FieldsLong(new String[] {"role"},sv,"E",baseModel);
										mappedItems = (Map) fieldItem.get("role");
										optionValue = makeDropDownList( mappedItems , sv.role.getFormData(),2,resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.role.getFormData()).toString().trim());
									%>
									
									<%
										if((new Byte((sv.role).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
									%>
									  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
																"blank_cell" : "output_cell" %>' style="width: 190px;">
										   		<%if(longValue != null){%>
									
										   		<%=longValue%>
									
										   		<%}%>
										   </div>
									
									<%
									longValue = null;
									%>
									
										<% }else {%>
									
									<% if("red".equals((sv.role).getColor())){
									%>
									<div style="width:190px;">
									<%
									}
									%>
									
									<select name='role' type='list' style="width:190px;"
									<%
										if((new Byte((sv.role).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
									%>
										readonly="true"
										disabled
										class="output_cell"
									<%
										}else if((new Byte((sv.role).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>
											class="bold_cell"
									<%
										}else {
									%>
										class = 'input_cell'
									<%
										}
									%>
									>
									<%=optionValue%>
									</select>
									<% if("red".equals((sv.role).getColor())){
									%>
									</div>
									<%
									}
									%>
									
									<%
									}}
									%>
							</div>
							</div>

						</div>
						<div class="row">			       
							<div class="col-md-4">
					        	<div class="form-group">
					        	<label><%=resourceBundleHandler.gettingValueFromBundle("Date of Birth")%></label>
										        	
						        	<%
										if ((new Byte((sv.cltdobDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| fw.getVariables().isScreenProtected()) {
									%>
									<%=smartHF.getRichTextDateInput(fw, sv.cltdobDisp, (sv.cltdobDisp.getLength()))%>
									<%
										} else {
									%>
									<div class="input-group date form_date col-md-12" data-date=""
										data-date-format="dd/mm/yyyy" data-link-field="cltdobDisp"
										data-link-format="dd/mm/yyyy">
										<%=smartHF.getRichTextDateInput(fw, sv.cltdobDisp, (sv.cltdobDisp.getLength()))%>
										<span class="input-group-addon"><span
											class="glyphicon glyphicon-calendar"></span></span>
									</div>
									<%
										}
									%>
										        	
										        	
							
							</div>
							</div>
						<div class="col-md-4"></div>
							<div class="col-md-4">
						        	<div class="form-group">
						        	<label><%=resourceBundleHandler.gettingValueFromBundle("Sex")%></label>
										        	<%
											if ((new Byte((sv.cltsex).getInvisible())).compareTo(new Byte(
																		BaseScreenData.INVISIBLE)) != 0) {
											fieldItem=appVars.loadF4FieldsLong(new String[] {"cltsex"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("cltsex");
											optionValue = makeDropDownList( mappedItems , sv.cltsex.getFormData(),2,resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.cltsex.getFormData()).toString().trim());
										%>
										
										<%
											if((new Byte((sv.cltsex).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){
										%>
										  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ?
																	"blank_cell" : "output_cell" %>' style="width: 190px;">
											   		<%if(longValue != null){%>
										
											   		<%=longValue%>
										
											   		<%}%>
											   </div>
										
										<%
										longValue = null;
										%>
										
											<% }else {%>
										
										<% if("red".equals((sv.cltsex).getColor())){
										%>
										<div style="width:190px;">
										<%
										}
										%>
										
										<select name='cltsex' type='list' style="width:190px;"
										<%
											if((new Byte((sv.cltsex).getEnabled()))
											.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){
										%>
											readonly="true"
											disabled
											class="output_cell"
										<%
											}else if((new Byte((sv.cltsex).getHighLight())).
												compareTo(new Byte(BaseScreenData.BOLD)) == 0){
										%>
												class="bold_cell"
										<%
											}else {
										%>
											class = 'input_cell'
										<%
											}
										%>
										>
										<%=optionValue%>
										</select>
										<% if("red".equals((sv.cltsex).getColor())){
										%>
										</div>
										<%
										}
										%>
										
										<%
										}}
										%>
							</div>
							</div>
						</div>
						
						<div class="row">			       
							<div class="col-md-2">
						        	<div class="form-group">
						        	<label><%=resourceBundleHandler.gettingValueFromBundle("Post Code")%></label>
						        	<%if ((new Byte((sv.cltpcode).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) {%>
							
							
									&nbsp;&nbsp;&nbsp;<input name='cltpcode'
									type='text'
									
									<%if((sv.cltpcode).getClass().getSimpleName().equals("ZonedDecimalData")) {%>style="text-align: right"<% }%>
									
									<%
									
											formatValue = (sv.cltpcode.getFormData()).toString();
									
									%>
										value='<%=formatValue%>' <%if(formatValue!=null && formatValue.trim().length()>0) {%> title='<%=formatValue%>' <%}%>
									
									size='<%= sv.cltpcode.getLength()%>'
									maxLength='<%= sv.cltpcode.getLength()%>'
									onFocus='doFocus(this)' onHelp='return fieldHelp(cltpcode)' onKeyUp='return checkMaxLength(this)'
									
									
									<%
										if((new Byte((sv.cltpcode).getEnabled()))
										.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 || fw.getVariables().isScreenProtected()){
									%>
										readonly="true"
										class="output_cell"
									<%
										}else if((new Byte((sv.cltpcode).getHighLight())).
											compareTo(new Byte(BaseScreenData.BOLD)) == 0){
									%>
											class="bold_cell"
									
									<%
										}else {
									%>
									
										class = ' <%=(sv.cltpcode).getColor()== null  ?
												"input_cell" :  (sv.cltpcode).getColor().equals("red") ?
												"input_cell red reverse" : "input_cell" %>'
									
									<%
										}
									%>
									>
									<%}%>
							</div>
							</div>
						</div>
					</div>
					</div>

						<div class="panel panel-default">
							<div class="panel-heading">
							<%=resourceBundleHandler.gettingValueFromBundle("Actions")%>
							</div>
							<div class="panel-body">
								<div class="row">
								<div class="col-md-3"> 
									<div class="radioButtonSubmenuUIG">
									
									<label for="A"><%= smartHF.buildRadioOption(sv.action, "action", "A")%>
									<%=resourceBundleHandler.gettingValueFromBundle("Contract Details")%></label>
									</div>
								</div>
								<div class="col-md-4"> 
								</div>
								<div class="col-md-3">
									<div class="radioButtonSubmenuUIG">
									
									<label for="B"><%= smartHF.buildRadioOption(sv.action, "action", "B")%>
									<%=resourceBundleHandler.gettingValueFromBundle("Client Details")%></label>
									</div>
								</div>
								</div>
							</div>
						</div>

<div style="display:none" id="subfileTable"></div> <div style="display:none" id="addRemoveDiv"></div>

<%@ include file="/POLACommon2NEW.jsp"%>

