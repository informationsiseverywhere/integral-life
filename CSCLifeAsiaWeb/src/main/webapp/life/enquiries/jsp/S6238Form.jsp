<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6238";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6238ScreenVars sv = (S6238ScreenVars) fw.getVariables();%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Commission Agent ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch          ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Element  Eff. Date    Ann. Premium  Comm. Payable     Comm. Paid");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Comm ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Earned");%>
<%		appVars.rollup(new int[] {93});
%>
<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<table><tr><td>
		       			<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  </td><td>
  							<%					
							if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="margin-left:1px;"
    >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
						</td><td>
					  		
							<%					
							if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-4">
		       		
		       		</div>
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
		       		<div class="input-group">
							<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
						<!--
						
					    
					   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px"> 
					   <%=	(sv.register.getFormData()).toString()%>
					   </div>
					   
					   --><div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px">  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   
					   	<%
							longValue = null;
							formatValue = null;
						%>
  
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		    </div>
		    
		     <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		<div class="input-group">
					<%					
						if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width:100px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		 
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
		       		<div class="input-group">
						<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
					%>
					
				    
				   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
										"blank_cell" : "output_cell" %>'  style="min-width:100px"> 
				   <%=	(sv.cntcurr.getFormData()).toString()%>
				   </div><!--
				   
				   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' >  
				   		<%if(longValue != null){%>
				   		<%=longValue%>
				   		<%}%>
				   </div>
				   
				  --><%
						longValue = null;
						formatValue = null;
					%>
  
	
		       		</div>
		       		</div>
		       	</div>
		       	
		    </div>
		    
		     <div class="row">
	        	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Commission Agent")%></label>
		       		<table><tr><td>
						<%					
							if(!((sv.servagnt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							</td><td>

						<%					
							if(!((sv.servagnam.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="margin-left:1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  						</td></tr></table>
		       		
		       		</div>
		       	</div>
		   
		    
		    <div class="col-md-4">
	
		       	</div>
		   
		    
		    
		    <div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
		       		<div class="input-group">
		       		<!--
						<%					
							if(!((sv.servbr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servbr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servbr.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' >
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  		
							--><%					
							if(!((sv.servbrname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="min-width:100px">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    		<!-- ILIFE-1479 START by nnazeer -->


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
if (sv.ctenq011Flag.compareTo("Y") == 0) {
	tblColumnWidth = new int[7];
}
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.chdrelem.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
			} else {		
				calculatedValue = (sv.chdrelem.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.effdateDisp.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*7;								
			} else {		
				calculatedValue = (sv.effdateDisp.getFormData()).length()*7;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.annprem.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*12;								
			} else {		
				calculatedValue = (sv.annprem.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.initcom.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*12;								
			} else {		
				calculatedValue = (sv.initcom.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.compay.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*12;								
			} else {		
				calculatedValue = (sv.compay.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.comern.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*12;								
			} else {		
				calculatedValue = (sv.comern.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
		if (sv.ctenq011Flag.compareTo("Y") == 0) {
			if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.dormflag.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*12;								
			} else {		
				calculatedValue = (sv.dormflag.getFormData()).length()*12;								
			}		
			totalTblWidth += calculatedValue;
			tblColumnWidth[6]= calculatedValue;		
		}
			%>
			
			<!-- ILIFE-1479 END  -->
		<%
		GeneralTable sfl = fw.getTable("s6238screensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>


		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s6238' width='100%'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<th> <center> <%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>									
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></center></th>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></center></th>
						<%if (sv.ctenq011Flag.compareTo("Y") == 0) {%>
						<th><center><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></center></th>
						<%}%>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
						S6238screensfl
						.set1stScreenRow(sfl, appVars, sv);
						int count = 1;
						while (S6238screensfl
						.hasMoreScreenRows(sfl)) {
						
						%>

						<tr>
						 <td 
						<%if((sv.chdrelem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	
						<%= sv.chdrelem.getFormData()%>
						</td>
				    	
				    	<td 
						<%if((sv.effdateDisp).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >																	
						<%= sv.effdateDisp.getFormData()%>
						</td>
				    									
				    	<td 
				    	<%if((sv.annprem).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >							
																
																							
							<%	
								sm = sfl.getCurrentScreenRow();
								qpsf = sm.getFieldXMLDef((sv.annprem).getFieldName());						
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
							%>
							
												
								<%
									formatValue = smartHF.getPicFormatted(qpsf,sv.annprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.
									annprem
									.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
								%>
								<%= formatValue%>
								<%
										longValue = null;
										formatValue = null;
								%>
						</td>
				    									
				    	<td  
						<%if((sv.initcom).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
							<%	
							sm = sfl.getCurrentScreenRow();
							qpsf = sm.getFieldXMLDef((sv.initcom).getFieldName());						
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
						%>
						
											
							<%
								formatValue = smartHF.getPicFormatted(qpsf,sv.initcom,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
								if(!sv.
								initcom
								.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
									formatValue = formatValue( formatValue );
								}
							%>
							<%= formatValue%>
							<%
									longValue = null;
									formatValue = null;
							%>
						</td>
						
				    	<td  
						<%if((sv.compay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																
																									
							<%	
								sm = sfl.getCurrentScreenRow();
								qpsf = sm.getFieldXMLDef((sv.compay).getFieldName());						
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
							%>
							
												
								<%
									formatValue = smartHF.getPicFormatted(qpsf,sv.compay,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.
									compay
									.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
								%>
								<%= formatValue%>
								<%
										longValue = null;
										formatValue = null;
								%>
						</td>
				    								
				    	<td  
						<%if((sv.comern).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >									
																
																							
							<%	
								sm = sfl.getCurrentScreenRow();
								qpsf = sm.getFieldXMLDef((sv.comern).getFieldName());						
								//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
							%>
							
												
								<%
									formatValue = smartHF.getPicFormatted(qpsf,sv.comern,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.
									comern
									.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
								%>
								<%= formatValue%>
								<%
										longValue = null;
										formatValue = null;
								%>
							 			 		
						</td>
						<%if (sv.ctenq011Flag.compareTo("Y") == 0) {%>
						<td 
						<%if((sv.dormflag).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	
						<%= sv.dormflag.getFormData()%>
						</td>
						<%}%>
						
						</tr>
						<%
						count = count + 1;
						S6238screensfl.setNextScreenRow(sfl, appVars, sv);
						}
						if (sv.ctenq011Flag.compareTo("Y") == 0) {
						%>
						<tr>
						<td colspan="4"  align="right" style="min-width:100px;"><div style="padding-right:100px;"><strong>Total</strong></div></td>
				    	
				    	<td  
						<%if((sv.totalcompay).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	  
						<%	
							qpsf = fw.getFieldXMLDef((sv.totalcompay).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.totalcompay,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!sv.totalcompay.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>				    	
				    	<td  
						<%if((sv.totalcomern).getClass().getSimpleName().equals("ZonedDecimalData")) {%>align="right"<% }else {%> align="left" <%}%> >	  
						<%	
							qpsf = fw.getFieldXMLDef((sv.totalcomern).getFieldName());
							formatValue = smartHF.getPicFormatted(qpsf,sv.totalcomern,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							if(!sv.totalcomern.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
								formatValue = formatValue( formatValue );
							}
						%>
						<%= formatValue%>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td></td>
						</tr>
						<%}%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	 </div>
</div>
<script>
       $(document).ready(function() {
              $('#dataTables-s6238').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script>  

<%@ include file="/POLACommon2NEW.jsp"%>
