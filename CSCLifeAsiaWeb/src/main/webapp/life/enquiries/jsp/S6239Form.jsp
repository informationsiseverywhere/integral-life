

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6239";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.text.NumberFormat"%>

<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6239ScreenVars sv = (S6239ScreenVars) fw.getVariables();%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind02.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind02.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select  1 - Component Details,  2 - Fund Holdings,");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"3 - Reassurance");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel Component Description ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status     Prem Stat");%>
<%		appVars.rollup(new int[] {93});
%>
<%{
		if (appVars.ind80.isOn()) {
			generatedText11.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind80.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			generatedText13.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.ind.setInvisibility(BaseScreenData.INVISIBLE);
		}
		 //ILJ-388
		if (appVars.ind05.isOn()) {
			sv.totalPrem.setInvisibility(BaseScreenData.INVISIBLE);
		} 
	}

	%>

	<div class="panel panel-default">
	
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr><td>
								
								<%-- <input class="form-group" style="max-width:80px;" type="text" placeholder='<%=sv.chdrnum.getFormData().toString()%>' disabled> --%>
							<%					
							if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
							
							</td><td style="min-width:1px">
							</td><td>	
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							   <%-- <input class="form-group" style="max-width:50px;" type="text" placeholder='<%=sv.cnttype%>' disabled> --%>
							</td><td style="min-width:1px">
							</td><td>	
								
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="max-width: 300px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
							   <%--  <input class="form-group" style="min-width:100px;" type="text" placeholder='<%=sv.ctypedes%>' disabled> --%>
							</td></tr></table>
							
							
						</div>											
					</div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
						<div class="form-group"> 
							<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%}%>
							
							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
							%>
							<%					
									if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.register.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.register.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
									%>			
										<%-- <input class="form-control" style="max-width:100px;" type="text" placeholder='<%=formatValue%>' disabled> --%>
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width:100px;max-width:90px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
									longValue = null;
									formatValue = null;
									%>
							<%}%>							
							
						</div>
				</div>
				
			</div>
			
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						<%					
						if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>			
							<%-- <input class="form-control" style="max-width:100px;" type="text" placeholder='<%=formatValue%>' disabled> --%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="min-width:100px;max-width:120px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>						
					</div>
				</div>
				 
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<%					
						if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>		
							<%-- <input class="form-control" type="text" placeholder='<%=formatValue%>' disabled> --%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="max-width: 120px;min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
						longValue = null;
						formatValue = null;
						%>						
					</div>
				</div>	
			 
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<%					
						if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>			
							<%-- <input class="form-control" style="max-width:80px;" type="text" placeholder='<%=formatValue%>' disabled> --%>
							
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 50px;max-width: 65px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							
						<%
						longValue = null;
						formatValue = null;
						%>						
					</div>
				</div>							
			</div>
			
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<table><tr><td>
						
		<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						</td>
						<td>
							
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
						
						 </td></tr></table>				
					</div>
				</div>
				 
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table><tr><td>
						<%					
						if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						
						</td><td style="min-width:1px">
						</td><td>
						<%					
							if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 100px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>				
						</td></tr></table>						
					</div>
				</div>
				 	
				<div class="col-md-4" >
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
							
						<%	
							qpsf = fw.getFieldXMLDef((sv.numpols).getFieldName());
							qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.numpols);
							
							if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="max-width: 150px;">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 100px;"></div>
						
						<% 
							} 
						%>
		
				
					</div>
				</div>							
			</div>			
<br>
			<div class="row">        
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-s6239" width="100%"
						style="border-right: thin solid #dddddd !important;">
                         	<thead>
                            	<tr class='info'>
                                	<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel") %></center></th>
                                    <th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Component ") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Description ") %></center></th>
                                    <!-- ILJ-388 -->
									<% if (sv.cntEnqScreenflag.compareTo("Y") == 0){ %>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Sum Insured") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium") %></center></th>
                                   <%} %>
                                   <!-- ILJ-388 end-->
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Status") %></center></th>
                                    <th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium Status ") %></center></th>
                               </tr>
                            </thead>
                            <tbody>
                            	<%
                            	GeneralTable sfl = fw.getTable("s6239screensfl");
                            	S6239screensfl.set1stScreenRow(sfl, appVars, sv);
                            	int count = 1;
                            	BigDecimal totalPrem=BigDecimal.ZERO;//ILJ-388
                            	while (S6239screensfl.hasMoreScreenRows(sfl)) {
                            		//ILJ-388
                            		 {
                            			if (appVars.ind04.isOn()) {
                            				sv.sumins.setInvisibility(BaseScreenData.INVISIBLE);
                            			}
                            		} 
								%> 
								<tr>
						    		<td>
							 			<input type="radio"	value='<%=sv.select.getFormData()%>' 
									 			name='s6239screensfl.select_R<%=count%>'
									 			id='s6239screensfl.select_R<%=count%>'
									 		    onClick="selectedRow('s6239screensfl.select_R<%=count%>')"
				                       			<% if((new Byte((sv.select).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
				                               		||(((ScreenModel) fw).getVariables().isScreenProtected())){%>
				                           			disabled="disabled"
			    	                   			<% } %>
		                         		/>						 			
									</td>
				    				<td><b><%=sv.cmpntnum.getFormData()%></b></td>
				    				<td><b><%=sv.component.getFormData()%></b></td>
				    				<td><b><%=sv.compdesc.getFormData()%></b></td>
				    				 <!-- ILJ-388 -->
									<% if ((new Byte((sv.sumins).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
				    				<td  align="right" style="vertical-align:middle;padding: 3px !important;">									
									<%	
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.sumins).getFieldName());						
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
										formatValue = smartHF.getPicFormatted(qpsf,sv.sumins,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!(sv.sumins).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
									%>
									</td>
				    				<td align="right" style="vertical-align:middle;padding: 3px !important;">									
								   <%	if(!(sv.cntcurr.equals("JPY"))){
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.instPrem).getFieldName());						
										formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										//formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZERO_BEFORE);
										if(!(sv.instPrem).getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}
									%>
									<%=formatValue%>
									<%
										longValue = null;
										formatValue = null;
									}
									else{ 
									 if(!(sv.instPrem.toString().trim().equals(""))){ %>
									
									<%=NumberFormat.getNumberInstance(Locale.US).format(sv.instPrem.getbigdata().intValue())%>
									<%}} %> 
									</td>
				    				<%
				    				if(!(sv.instPrem.toString().trim().equals(""))){
				    				totalPrem=totalPrem.add(sv.instPrem.getbigdata());
				    				
				    				//+ Integer.parseInt(sv.instPrem.getFormData());
				    				} 
				    				}%>
				    				 <!-- ILJ-388 end-->
				    				<td><b><%=sv.statcode.getFormData()%></b></td>
				    				<td><b><%=sv.pstatcode.getFormData()%></b></td>
								</tr>								
								<%
								count = count + 1;
								S6239screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>								
                            </tbody>
                       </table>
                	</div>			
				</div>
			</div>	
			
			<!-- ILJ-388 -->
			<% if ((new Byte((sv.totalPrem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>  
			<div class="row">
				<div class="col-md-6" style="margin-top:-10px">
				<input type='text' style="visibility: hidden"></input>
				</div>
				
				<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium Paid")%></label>
					<div class="input-group" align="right" >
							<%			sv.instPrem.set(totalPrem);
										
									%>
									<%					
									if(sv.cntcurr.equals("JPY")){
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (NumberFormat.getNumberInstance(Locale.US).format(sv.instPrem.getbigdata().intValue()))); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="width:100px">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						}
						else{%>
									<%--=NumberFormat.getNumberInstance(Locale.US).format(sv.instPrem.getbigdata().intValue()) --%>
									<%=smartHF.getHTMLVar(0, 0, fw, sv.instPrem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:  145px !important;  text-align: right;\' ")%>
						<%} %>
					</div>
				</div>
			</div>
				
			</div> 
			<%} %>
			
			<div class="row">        
				<div class="col-md-12" style="margin-top:-10px">
				
				<div class="btn-group">
					<div class="sectionbutton">
						
						<p style="font-size: 12px; font-weight: bold;">
							<a class="btn btn-info" href="#" onClick="JavaScript:perFormOperation(1)"><%=resourceBundleHandler.gettingValueFromBundle("Component Details")%></a>
					<a class="btn btn-info" href="#" onClick="JavaScript:perFormOperation(2)"><%=resourceBundleHandler.gettingValueFromBundle("Fund Holdings")%></a>
					<a class="btn btn-info" href="#" onClick="JavaScript:perFormOperation(3)"><%=resourceBundleHandler.gettingValueFromBundle("Reassurance")%></a>
						</p>
					</div>
				</div>
					
				</div>	
				</div>
			</div>
				<br>		
		</div>
			
	</div>
	

<%@ include file="/POLACommon2NEW.jsp"%>