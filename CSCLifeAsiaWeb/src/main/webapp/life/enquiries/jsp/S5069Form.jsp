<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5069";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S5069ScreenVars sv = (S5069ScreenVars) fw.getVariables();%>

<%if (sv.S5069screenWritten.gt(0)) {%>
	<%S5069screen.clearClassString(sv);%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

	<%
{
	}

	%>

	<%=smartHF.getLit(23, 4, generatedText11)%>


<%}%>


<%if (sv.S5069protectWritten.gt(0)) {%>
	<%S5069protect.clearClassString(sv);%>

	<%
{
	}

	%>


<%}%>

<%if (sv.S5069screenctlWritten.gt(0)) {%>
	<%S5069screenctl.clearClassString(sv);%>
	<%GeneralTable sfl = fw.getTable("s5069screensfl");%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel  Plan  Coverage Risk Status              Coverage Premium Status");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>
	<%sv.subfilePosition.setClassString("");%>

	<%
{
		appVars.rollup(new int[] {93});
	}

	%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        		<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText1)%></label>
		       		</div>
		       	</div>
		  		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 500">
		       			<%=smartHF.getHTMLVarExt(fw, sv.chdrnum, 0, 100)%>
						<%=smartHF.getHTMLSpaceVar(fw, sv.cnttype)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.cnttype)%>
						<%=smartHF.getHTMLVarExt(fw, sv.ctypedes, 0, 500)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-5">
		       	</div>
		       	
		       		<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText2)%></label>
		       		</div>
		       	</div>
		  
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
						<%=smartHF.getHTMLSpaceVar(fw, sv.cntcurr)%>
						<%=smartHF.getHTMLF4NSVarExt(fw, sv.cntcurr)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        		<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText3)%></label>
		       		</div>
		       	</div>

		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLVarExt(fw, sv.chdrstatus, 0 ,130)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText4)%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLVarExt(fw, sv.premstatus, 0, 130)%>
		       		</div>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-1">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText5)%></label>
		       		</div>
		       	</div>
		     
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLSpaceVar(fw, sv.register)%>
					<%=smartHF.getHTMLF4NSVarExt(fw, sv.register)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText6)%></label>
		       		</div>
		       	</div>
		       	
		      
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group">
					<%=smartHF.getHTMLVarExt(fw, sv.numpols, 0, 70)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		   <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText7)%></label>
		       		</div>
		       	</div>
		      		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 600px;">
					<%=smartHF.getHTMLVarExt(fw, sv.lifenum, 0, 100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.lifename, 0, 500)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=smartHF.getLit(generatedText8)%></label>
		       		</div>
		       	</div>
		   
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<div class="input-group" style="min-width: 600px;">
					<%=smartHF.getHTMLVarExt(fw, sv.jlife, 0, 100)%>
					<%=smartHF.getHTMLVarExt(fw, sv.jlifename, 0, 500)%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
			    
		    
		    <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s0087'>	
			    	 	<thead>
			    	 	<tr class='info'>									
						<%=smartHF.getLit(0, 2, generatedText10)%> 
						</tr>	
			         	</thead>
					      <tbody>
					      <%if (sv.S5069screensflWritten.gt(0)) {%>
							<%GeneralTable sflnew = fw.getTable("s5069screensfl");
							savedInds = appVars.saveAllInds();
							S5069screensfl.set1stScreenRow(sflnew, appVars, sv);
							double sflLine = 0.0;
							int doingLine = 0;
							int sflcols = 1;
							int linesPerCol = 12;
							String height = smartHF.fmty(12);
							smartHF.setSflLineOffset(10);
							%>
							<div style='position: absolute; left: 0%;  top: <%=smartHF.fmty(2)%>; width: 100%; height: <%=height%>; overflow-y:auto;'>
							<%while (S5069screensfl.hasMoreScreenRows(sflnew)) {%>
							<%sv.select.setClassString("");%>
							<%sv.planSuffix.setClassString("");%>
						<%	sv.planSuffix.appendClassString("num_fld");
							sv.planSuffix.appendClassString("output_txt");
							sv.planSuffix.appendClassString("highlight");
						%>
							<%sv.statcode.setClassString("");%>
						<%	sv.statcode.appendClassString("string_fld");
							sv.statcode.appendClassString("output_txt");
							sv.statcode.appendClassString("highlight");
						%>
							<%sv.rstatdesc.setClassString("");%>
						<%	sv.rstatdesc.appendClassString("string_fld");
							sv.rstatdesc.appendClassString("output_txt");
							sv.rstatdesc.appendClassString("highlight");
						%>
							<%sv.pstatcode.setClassString("");%>
						<%	sv.pstatcode.appendClassString("string_fld");
							sv.pstatcode.appendClassString("output_txt");
							sv.pstatcode.appendClassString("highlight");
						%>
							<%sv.pstatdesc.setClassString("");%>
						<%	sv.pstatdesc.appendClassString("string_fld");
							sv.pstatdesc.appendClassString("output_txt");
							sv.pstatdesc.appendClassString("highlight");
						%>
							<%sv.screenIndicArea.setClassString("");%>
							<%sv.rider.setClassString("");%>
							<%sv.coverage.setClassString("");%>
							<%sv.life.setClassString("");%>
						
							<%
						{
								if (appVars.ind01.isOn()) {
									sv.select.setReverse(BaseScreenData.REVERSED);
									sv.select.setColor(BaseScreenData.RED);
								}
								if (!appVars.ind01.isOn()) {
									sv.select.setHighLight(BaseScreenData.BOLD);
								}
							}
						
							%>
						
								<%=smartHF.getTableHTMLVarQual(0, 2, sflnew, sv.select)%>
								<%=smartHF.getTableHTMLVarQual(0, 7, sflnew, sv.planSuffix)%>
								<%=smartHF.getHTMLSFSpaceVar(0, 13, sflnew, sv.statcode)%>
								<%=smartHF.getHTMLF4SSVar(0, 13, sflnew, sv.statcode)%>
								<%=smartHF.getTableHTMLVarQual(0, 16, sflnew, sv.rstatdesc)%>
								<%=smartHF.getTableHTMLVarQual(0, 47, sflnew, sv.pstatcode)%>
								<%=smartHF.getTableHTMLVarQual(0, 50, sflnew, sv.pstatdesc)%> 
								
								<%sflLine += 1;
								doingLine++;
								if (doingLine % linesPerCol == 0 && sflcols > 1) {
									sflLine = 0.0;
								}
								S5069screensfl.setNextScreenRow(sflnew, appVars, sv);
							}%>
							</div>
							<%appVars.restoreAllInds(savedInds);%>
						<%}%>
					      </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		    
	 <br>
	 <br>
<%}%>
</div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>
