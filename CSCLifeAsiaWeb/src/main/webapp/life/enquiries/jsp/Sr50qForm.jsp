

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR50Q";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr50qScreenVars sv = (Sr50qScreenVars) fw.getVariables();%>

<%{
		if (appVars.ind10.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (appVars.ind11.isOn()) {
			sv.select.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.select.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract number  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency  ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract status  ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium status  ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register  ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured     ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life       ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran no          ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran code  ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran date        ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff date   ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User id  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Status");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Prem stat");%>
<%		appVars.rollup(new int[] {93});
%>


<div class="panel panel-default">
    	
    	<div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
					    		    <table><tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td><td style="padding-left:1px;">

						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td><td style="padding-left:1px;max-width:300px;">

						<%
							if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
						 		} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
			</td></tr></table>

					
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<div class="input-group">
						    		
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("register");
		longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
	%>
	
    
   <div class='output_cell'> 
  
   <%=(sv.register.getFormData()).toString()%>
  
   </div>
 <%--  <div class='output_cell'> 
  
   <%=longValue%>
  
   </div> --%>

<%
		longValue = null;
		formatValue = null;
		%>	
  
  

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				 
				 
				 <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract status")%></label>
					    		     <div class="input-group">
						    		<div 	
		class='<%= (sv.chdrstatus.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Premium status")%></label>
							<div class="input-group">
						    		<div 	
		class='<%= (sv.premstatus.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group">
						    		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"currency"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("currency");
		longValue = (String) mappedItems.get((sv.currency.getFormData()).toString().trim());  
	%>
	
    
   <div class='output_cell'> 
  
   <%=	(sv.currency.getFormData()).toString()%>
  
   </div><!--
   
   <div class='output_cell'> 
  
   <%=longValue%>
  
   </div>

	
  
	

-->

				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
					    		 <table><tr><td>   	
							<%
								if (!((sv.lifenum.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div 
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
							</td><td style="padding-left:1px;">
							<%
								if (!((sv.lifedesc.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:155px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	 
			</td></tr></table> 
								
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
							
						<table><tr><td>
							<%
								if (!((sv.jlife.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
</td><td style="padding-left:1px;">
							<%
								if (!((sv.jlifedesc.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
			</td></tr></table>
								
						</div>
				   </div>		
			
			    	
		    </div>
		    
		    <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Tran no")%></label>
					    		     <div class="input-group">
						    		<div 	
		class='<%= (sv.tranno.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.tranno.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tranno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.tranno.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						    		

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Tran date")%></label>
							<div class="input-group">
						    		
<div 	
		class='<%= (sv.trandateDisp.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.trandateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Eff date")%></label>
							<div class="input-group">
						    		
	<div 	
		class='<%= (sv.effdateDisp.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.effdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
										
						</div>
				   </div>	
		    </div>
				   
				   
				   <div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Tran code")%></label>
					    		    <table><tr><td>
						    			<div 	
		class='<%= (sv.trancd.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px">
		<%					
		if(!((sv.trancd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trancd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trancd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

</td><td style="padding-left:1px;max-width:210px;">



	
  		<div 	
		class='<%= (sv.trandesc.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.trandesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.trandesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	</td></tr></table>
						    		

				      			     
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("User id")%></label>
							<div class="input-group">
						    		
	<div 	
		class='<%= (sv.crtuser.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'>
		<%					
		if(!((sv.crtuser.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtuser.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtuser.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				      			     </div>
						</div>
				   </div>		
			
			    	
		    </div>
				   
				      <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="table-responsive">
	         <table  id='dataTables-sr50q' class="table table-striped table-bordered table-hover" width="100%" style="border-right: thin solid #dddddd !important;">
               <thead>
               
		           <tr class="info">
			        <th><center><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></center></th>
			        <th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Component")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Description")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></center></th>
					<th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></center></th>
					
					</tr>
			 </thead>
			 


<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*12;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.cmpntnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*8;								
			} else {		
				calculatedValue = (sv.cmpntnum.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.component.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
			} else {		
				calculatedValue = (sv.component.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.compdesc.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
			} else {		
				calculatedValue = (sv.compdesc.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.statcode.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
			} else {		
				calculatedValue = (sv.statcode.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.pstatcode.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*8;								
			} else {		
				calculatedValue = (sv.pstatcode.getFormData()).length()*8;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
		totalTblWidth = totalTblWidth -240;
			%>
		<%
		GeneralTable sfl = fw.getTable("sr50qscreensfl");
		int height;
		if(sfl.count()*27 > 210) {
		height = 210 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>
			<%


	
	Sr50qscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr50qscreensfl
	.hasMoreScreenRows(sfl)) {
	
%>


<tbody>


		<tr >
						    									<td  align="center">														
																	
													
					
					 					 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr50qscreensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr50qscreensfl.select_R<%=count%>'
						 id='sr50qscreensfl.select_R<%=count%>'
						 onClick="selectedRow('sr50qscreensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
									</td>
				    									<td align="left">									
																
									
											
						<%-- <%= sv.cmpntnum.getFormData()%>/<%= sv.component.getFormData()%> --%>
						
 <%= sv.cmpntnum.getFormData()%>
</td>
<td align="left">
<%= sv.component.getFormData()%>
														 
				
									</td>
				    									
				    									<td  align="left">									
																
									
											
						<%= sv.compdesc.getFormData()%>
						
														 
				
									</td>
	<td  align="left">																	
									
											
						<%= sv.statcode.getFormData()%>
						
														 
				
									</td>
	<td  align="left">																	
									
											
						<%= sv.pstatcode.getFormData()%>
						
														 
				
									</td>
									
			                       
		
	</tr>


	<%
	
	count = count + 1;
	Sr50qscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
	

		</table>
		</div>


</div></div></div> 
				
				   	
			    	
		</div>  <!--  panel-->
</div>  <!--panel  -->
<!-- <script>
       $(document).ready(function() {
              $('#dataTables-sr50q').DataTable({
                     ordering : false,
                     searching : false,
                     scrollY : "350px",
                     scrollCollapse : true,
                     scrollX : true,
                     paging:   false,
                     ordering: false,
               info:     false,
               searching: false
              });
       });
</script>  --> 

<%@ include file="/POLACommon2NEW.jsp"%>
