<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6234";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%
	S6234ScreenVars sv = (S6234ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>
<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Contract ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Currency ");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract Status ");
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Premium Status ");
%>
<%
	StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Register ");
%>
<%
	StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Effective Date  ");
%>
<%
	StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Tr Code");
%>
<%
	StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Life Assured    ");
%>
<%
	StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Joint Life      ");
%>
<%
	StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Sub-AC");
%>
<%
	StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " Original");
%>
<%
	StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Accounting");
%>
<%
	StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"G/L Code      ");
%>
<%
	StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Entity          ");
%>
<%
	StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Cd  Ty");
%>
<%
	StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"        Amount ");
%>
<%
	StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Ccy");
%>
<%
	StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"        Amount ");
%>
<%
	StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "Ccy");
%>
<%
	appVars.rollup(new int[]{93});
%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrnum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="margin-left: 1px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' id="cntdesc"
									style="margin-left: 1px;max-width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
					<table>
						<tr>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"register"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("register");
									longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());
								%>


								<div
									class='<%=((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim())))
					? "blank_cell"
					: "output_cell"%>'>
									<%=(sv.register.getFormData()).toString()%>
								</div>
							</td>
							<td>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="margin-left: 1px;max-width: 150px;">
									<%
										if (longValue != null) {
									%>
									<%=longValue%>
									<%
										}
									%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
					<table>
						<tr>
							<td>
								<%
									fieldItem = appVars.loadF4FieldsLong(new String[]{"cntcurr"}, sv, "E", baseModel);
									mappedItems = (Map) fieldItem.get("cntcurr");
									longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());
								%>


								<div
									class='<%=((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim())))
					? "blank_cell"
					: "output_cell"%> col-md-2'>
									<%=(sv.cntcurr.getFormData()).toString()%>
								</div>
							</td>
							<td>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-9'
									style="margin-left: 1px !important;max-width: 150px;">
									<%
										if (longValue != null) {
									%>
									<%=longValue%>
									<%
										}
									%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
					

						<%
							if (!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
					

						<%
							if (!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.premstatus.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
					

						<%
							if (!((sv.effdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.effdateDisp.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
							style="width: 80px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Tr Code")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.trcode.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.trcode.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.trcode.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.trandesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.trandesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.trandesc.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-9' id="cdesc"
									style="margin-left: 1px !important;max-width: 200px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifenum.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%> col-md-2'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.lifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>  col-md-9'
									style="margin-left: 1px !important;max-width: 150px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlife.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style="width: 71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.jlifename.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>  col-md-9'
									style="width: 71px; margin-left: 1px !important;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>


		<%
			/* This block of jsp code is to calculate the variable width of the table at runtime.*/
			int[] tblColumnWidth = new int[8];
			int totalTblWidth = 0;
			int calculatedValue = 0;

			if (resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.glcode.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length()) * 12;
			} else {
				calculatedValue = (sv.glcode.getFormData()).length() * 12;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[0] = calculatedValue;

			if (resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.rldgacct.getFormData())
					.length()) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length()) * 8;
			} else {
				calculatedValue = (sv.rldgacct.getFormData()).length() * 8;
			}
			totalTblWidth += calculatedValue;
			tblColumnWidth[1] = calculatedValue;

			calculatedValue = (sv.sacscode.getFormData()).length() * 8;

			totalTblWidth += calculatedValue;
			tblColumnWidth[2] = calculatedValue;

			calculatedValue = (sv.sacstyp.getFormData()).length() * 6;

			totalTblWidth += calculatedValue;
			tblColumnWidth[3] = calculatedValue;

			calculatedValue = (sv.hmhii.getFormData()).length() * 12;

			totalTblWidth += calculatedValue;
			tblColumnWidth[4] = calculatedValue;

			calculatedValue = (sv.origccy.getFormData()).length() * 6;

			totalTblWidth += calculatedValue;
			tblColumnWidth[5] = calculatedValue;

			calculatedValue = (sv.hmhli.getFormData()).length() * 12;

			totalTblWidth += calculatedValue;
			tblColumnWidth[6] = calculatedValue;

			calculatedValue = (sv.acctccy.getFormData()).length() * 6;

			totalTblWidth += calculatedValue;
			tblColumnWidth[7] = calculatedValue;
			totalTblWidth += 70;
		%>
		<%
			GeneralTable sfl = fw.getTable("s6234screensfl");
			savedInds = appVars.saveAllInds();
			int height;
			if (sfl.count() * 27 > 210) {
				height = 210;
			} else {
				height = sfl.count() * 27;
			}
			smartHF.setSflLineOffset(10);
		%>


		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id='dataTables-s6234' width='100%'>
						<thead>
							<tr class='info'>

								<th rowspan="2" style="padding-bottom: 28px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></center></th>
								<th rowspan="2" style="padding-bottom: 28px;"><center><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></center></th>
								<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Sub-Account")%></center></th>
								<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Original")%></center></th>
								<th colspan="2"><center><%=resourceBundleHandler.gettingValueFromBundle("Accounting")%></center></th>
							</tr>

							<tr class='info'>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Code")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Amount")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
							</tr>

						</thead>
						<tbody style="font-weight: bold;">
							<%
								S6234screensfl.set1stScreenRow(sfl, appVars, sv);

								int count = 1;
								while (S6234screensfl.hasMoreScreenRows(sfl)) {
							%>

							<%
								sv.glcode.setClassString("");
							%>
							<%
								sv.glcode.appendClassString("string_fld");
									sv.glcode.appendClassString("output_txt");
									sv.glcode.appendClassString("highlight");
							%>
							<%
								sv.rldgacct.setClassString("");
							%>
							<%
								sv.rldgacct.appendClassString("string_fld");
									sv.rldgacct.appendClassString("output_txt");
									sv.rldgacct.appendClassString("highlight");
							%>
							<%
								sv.sacscode.setClassString("");
							%>
							<%
								sv.sacscode.appendClassString("string_fld");
									sv.sacscode.appendClassString("output_txt");
									sv.sacscode.appendClassString("highlight");
							%>
							<%
								sv.sacstyp.setClassString("");
							%>
							<%
								sv.sacstyp.appendClassString("string_fld");
									sv.sacstyp.appendClassString("output_txt");
									sv.sacstyp.appendClassString("highlight");
							%>
							<%
								sv.hmhii.setClassString("");
							%>
							<%
								sv.hmhii.appendClassString("num_fld");
									sv.hmhii.appendClassString("output_txt");
									sv.hmhii.appendClassString("highlight");
							%>
							<%
								sv.origccy.setClassString("");
							%>
							<%
								sv.origccy.appendClassString("string_fld");
									sv.origccy.appendClassString("output_txt");
									sv.origccy.appendClassString("highlight");
							%>
							<%
								sv.hmhli.setClassString("");
							%>
							<%
								sv.hmhli.appendClassString("num_fld");
									sv.hmhli.appendClassString("output_txt");
									sv.hmhli.appendClassString("highlight");
							%>
							<%
								sv.acctccy.setClassString("");
							%>
							<%
								sv.acctccy.appendClassString("string_fld");
									sv.acctccy.appendClassString("output_txt");
									sv.acctccy.appendClassString("highlight");
							%>
							<%
								sv.screenIndicArea.setClassString("");
							%>
							<%
								sv.glsign.setClassString("");
							%>


							<tr>
								<td style="min-width: 150px" ; align="left" id="glcode.<%=count%>"><%=sv.glcode.getFormData()%>
								</td>

								<td align="left" id="rldgacct.<%=count%>"><%=sv.rldgacct.getFormData()%></td>

								<td align="left" id="sacscode.<%=count%>"><%=sv.sacscode.getFormData()%></td>

								<td align="left" id="sacstyp.<%=count%>"><%=sv.sacstyp.getFormData()%></td>
								<td  id="hmhii.<%=count%>"
									<%if ((sv.hmhii).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hmhii).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
	if(sv.hmhii.equals(0)) {
		formatValue = "0.0";
	} else {
 	formatValue = smartHF.getPicFormatted(qpsf, sv.hmhii,
 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 		if (!sv.hmhii.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
	}
 %> <%=formatValue%>&nbsp; <%
 	longValue = null;
 		formatValue = null;
 %>
								</td>

								<td align="left" id="origccy.<%=count%>"><%=sv.origccy.getFormData()%></td>

								<td id="hmhli.<%=count%>"
									<%if ((sv.hmhli).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
									align="right" <%} else {%> align="left" <%}%>>
									<%
										sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.hmhli).getFieldName());
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									%> <%
	if(sv.hmhli.equals(0)) {
		formatValue = "0.0";
	} else {			
 	formatValue = smartHF.getPicFormatted(qpsf, sv.hmhli,
 				COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
 		if (!sv.hmhli.getFormData().toString().trim().equalsIgnoreCase("")) {
 			formatValue = formatValue(formatValue);
 		}
	}
 %> <%=formatValue%>&nbsp; <%
 	longValue = null;
 		formatValue = null;
 %>
								</td>

								<td align="left" id="acctccy.<%=count%>"><%=sv.acctccy.getFormData()%></td>

							</tr>


							<%
								count = count + 1;
									S6234screensfl.setNextScreenRow(sfl, appVars, sv);
								}
							%>
							<%
								appVars.restoreAllInds(savedInds);
							%>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
</div>

<%@ include file="/POLACommon2NEW.jsp"%>
<script>
	$(document).ready(function() {
		if (screen.height == 900) {
			
			$('#cntdesc').css('max-width','215px')
		} 
	if (screen.height == 768) {
			
			$('#cntdesc').css('max-width','190px')
		} 
if (screen.height == 900) {
		
		$('#cdesc').css('max-width','215px')
	} 
if (screen.height == 768) {
		
		$('#cdesc').css('max-width','215px')
	} 
		$('#dataTables-s6234').DataTable({
			paging : false,
			ordering : false,
			searching : false,
			scrollY : "300px",
			scrollX : "300px",
			scrollCollapse : true,
			info : false,
			paging : false
		});
	});
</script>