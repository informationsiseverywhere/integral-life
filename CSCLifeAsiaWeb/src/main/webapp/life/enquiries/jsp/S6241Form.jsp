<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S6241";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6241ScreenVars sv = (S6241ScreenVars) fw.getVariables();%>
	<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life number     ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien code   ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. Fund  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section  ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-Section ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured         ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment Premium  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation date   ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium      ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk CD             ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation date      ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann. Proc. date ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Unit Statement date      ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate date         ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate from date         ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit bill date   ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit debt amount o/s  ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special terms            ");%>
	<!-- MIBT-203 STARTS -->
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ttl Prem w/Tax ");%> 
	<!-- MIBT-203 ENDS -->
	<!--ILIFE-2964 IL_BASE_ITEM-001-Base development(added Benefit Schedule)-->
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Benefit Schedule "); %>
<%{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind44.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.optextind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		if (appVars.ind46.isOn()) {
			generatedText36.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.optsmode.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind46.isOn()) {
			sv.optsmode.setEnabled(BaseScreenData.DISABLED);
			sv.optsmode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.optsmode.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind45.isOn()) { 
			sv.optsmode.setHighLight(BaseScreenData.BOLD);
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
		//ILIFE-3399-STARTS
		if (appVars.ind53.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-ENDS
		//ILIFE-7805 - START		
		if (appVars.ind66.isOn()) {
			sv.singpremtype.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind67.isOn()) { 
			sv.singpremtype.setInvisibility(BaseScreenData.INVISIBLE);
		}		
		//ILIFE-7805 - END
		//ILJ-45 - START
		if (appVars.ind70.isOn()) {
			sv.crrcdDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/* if (appVars.ind71.isOn()) {
			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
		} */
		if (appVars.ind72.isOn()) {
			sv.riskCessDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILJ-45 - END
	}

	%>


<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<table><tr>
		       		<td>
		       	
					<%					
					if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' >
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  

                </td>
                           
				<td >	
					<%					
					if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  
	


            </td>
              
			<td>	
					<%					
					if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
								
										if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
										
										
								} else  {
											
								if(longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
										} else {
											formatValue = formatValue( longValue);
										}
								
								}
								%>			
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
									"blank_cell" : "output_cell" %>' style="max-width:250px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>	
					<%
					longValue = null;
					formatValue = null;
					%>
  
  
		       	</td>
		       	</tr></table>
		       </div>
		    </div>
		 </div>
		    
		    
		    <div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
		       		  		
							<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'  style="max-width: 130px;min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
		       		<div class="input-group">
 		
							<%					
							if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
  
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText2).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<%=resourceBundleHandler.gettingValueFromBundle("Currency")%>
							<%}%>
					</label>
		       		<div class="input-group">

						<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("cntcurr");
								longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
							%> 
								
						
						<%if ((new Byte((sv.cntcurr).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 130px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<%=resourceBundleHandler.gettingValueFromBundle("Register")%>
							<%}%>
					</label>
		       		<div class="input-group">

							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
									<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("register");
									longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
								%>
								
							    
								   <!--  <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
														"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 400px;"> 
								   <%=	(sv.register.getFormData()).toString()%>
								   </div> -->
								   
								   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 130px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
							   		<%
									longValue = null;
									formatValue = null;
									%>
							   
							  <%}%>
	
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
		       		<div class="input-group">

							<%					
							if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>		
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	  
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>
		       		<div class="input-group">
							<%	
								qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
								formatValue = smartHF.getPicFormatted(qpsf,sv.planSuffix);
								
								if(!((sv.planSuffix.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell"style="min-width: 80px;max-width: 100px;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell"style="min-width: 80px;max-width: 100px;"></div>
							
							<% 
								} 
							%>
							
						
					 <%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
	
	
  
		   <div class="row">
	        	<div class="col-md-2" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life number")%></label>
		       		<div class="input-group">
		
							<%					
							if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.life.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.life.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px; max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
		       		<div class="input-group">
		
							<%					
							if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width: 80px;max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       		 
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
		       		<div class="input-group">
  		
							<%					
							if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rider.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.rider.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width: 80px;max-width: 130px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. Fund")%></label>
		       		<div class="input-group">
						
						<%if ((new Byte((sv.statFund).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
							<%	
							fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("statFund");
							longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
						%>
						
						
							 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;max-width: 130px;">  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						   
						  <%}%>
		       		</div>
		       		</div>
		       	</div> 
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
		       		<div class="input-group">
						
							<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							    
							   <div class='<%= ((sv.statSect.getFormData() == null) || ("".equals((sv.statSect.getFormData()).trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;"> 
							   <%=	(sv.statSect.getFormData()).toString()%>
							   </div>
							   
							  
						   		<%
								longValue = null;
								formatValue = null;
								%>
						   
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-Section")%></label>
		       		<div class="input-group">
						
							<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
								
						  
							   <div class='<%= ((sv.statSubsect.getFormData() == null) || ("".equals((sv.statSubsect.getFormData()).trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;"> 
							   <%=	(sv.statSubsect.getFormData()).toString()%>
							   </div>
							   
						   		<%
								longValue = null;
								formatValue = null;
								%>
						   
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		
		    
		    
		    <div class="row">
	        	<div class="col-md-3" style="">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
		       		<table><tr><td>
						 			<%					
		if(!((sv.lifenum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifenum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width: 65px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

          </td>
          

           <td >



	
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style="min-width:100px;margin-left: 1px;max-width: 200px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
															
		       		</td></tr></table>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-5">
		       		</div>
		       		
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Age")%></label>		<!-- IBPLIFE-1726 -->
		       		<div class="input-group">
		       		<%if ((new Byte((sv.zagelit).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="min-width: 80px;max-width: 100px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="min-width: 80px;max-width: 150px;"/>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-2" style="">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table><tr><td>
						
								<%					
								if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 71px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  </td><td style="min-width:1px">
						  </td><td>
							<%					
								if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
											style="min-width: 71px;max-width: 200px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
		       		</td></tr></table>
		       		</div>
		
		       </div></div>	
		       	
       			<div class="row">
					<div class="col-md-12">
	                	<ul class="nav nav-tabs">
	                    	<li class="active">
	                        	<a href="#risk_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Risk and Premium")%></label></a>
	                        </li>
	                        <li>
	                        	<a href="#processing_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Processing Details")%></label></a>
	                        </li>
	                    </ul>
	                  </div>
	               </div>	   
                                
                    <div class="tab-content">
                    	<div class="tab-pane fade in active" id="risk_tab">
                    	<div class="row">
				        	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></label>
					       		<div class="input-group">
					       		<%	
									qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);			
									
									if(!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
							
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell" style="text-align: right;width: 145px !important;">	
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
								
										<div class="blank_cell" style="width: 145px !important;"></div>
								
								<% 
									} 
								%>
					       		</div>
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
							<%
								//ILJ-386
								if (sv.cntEnqScreenflag.compareTo("Y") == 0) {
							%>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
							<%
								} else if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
			        			<%} else { %>
			        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk CD"))%></label>
			        			<%} %>
			        			<!-- ILJ-45 End -->
					       		

					       		<%					
									if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
					       		
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
					       		
					       		<%	
									fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
									mappedItems = (Map) fieldItem.get("mortcls");
									longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
									%>
								
							  		
									<%					
									if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'
												style="width: 120px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
					       		
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%></label>
					       		<div class="input-group">
						       		<%	
										qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());			
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										
										if(!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell" style="text-align: right; width: 145px;">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" style="width: 145px;"> </div>
									
									<% 
										} 
									%>
					       		</div>
					       		</div>
					       	</div>
					       	
					   </div>
		    
					    <div class="row">
				        	<div class="col-md-3">
					       		<div class="form-group">
					       		<!-- ILJ-45 Starts -->
				               	<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
				               	<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
					        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
					        	<%} else { %>
					        		 <label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cess Age / Term"))%></label> 
					        		<%} %>
					        	<!-- ILJ-45 End -->
					       		<table><tr><td>
					       		<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
									<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()),null, true)%>
									<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
									<%if(sv.riskCessAge.equals(0)) {%>
									<%=smartHF.getHTMLVarExt(fw, sv.riskCessAge, 0, 60)%>
									<%} else { %>
									<%=smartHF.getHTMLVarExt(fw, sv.riskCessAge, 0, 60)%>
									<%} %>
									<%}else {%>
									<%}%>
									</td><td style="width:2px">
                                        </td><td>
									<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
									<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm, (sv.riskCessTerm.getLength()),null, true)%>
									<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
									<%if(sv.riskCessTerm.equals(0)) {%>
									<%=smartHF.getHTMLVarExt(fw, sv.riskCessTerm, 0, 120)%>
									<%} else { %>
									<%=smartHF.getHTMLVarExt(fw, sv.riskCessTerm, 0, 120)%>
									<%} %>
									<%}else {%>
									<%}%>
									</td></tr></table>
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<!-- ILJ-45 Starts -->
								<%-- <% if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
								<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation date")%></label>
								<%} else { %>
									<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cessation date"))%></label>
								<%} %>
								<!-- ILJ-45 End -->
					       		

					       		<%					
									if(!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
					       	
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<%=resourceBundleHandler.gettingValueFromBundle("Lien code")%>
								<%}%></label>
					       		<div class="input-group">
									<%if ((new Byte((sv.liencd).getInvisible())).compareTo(new Byte(
																	BaseScreenData.INVISIBLE)) != 0) {%>
											<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"liencd"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("liencd");
											longValue = (String) mappedItems.get((sv.liencd.getFormData()).toString().trim());  
										%>
										
									    
										 <!--   <div class='<%= ((sv.liencd.getFormData() == null) || ("".equals((sv.liencd.getFormData()).trim()))) ? 
																"blank_cell" : "output_cell" %>'> 
										   <%=	(sv.liencd.getFormData()).toString()%>
										   </div> -->
										   
										   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
															"blank_cell" : "output_cell" %>'
														style="min-width: 180px;">				  
										   		<%if(longValue != null){%>
										   		
										   		<%=longValue%>
										   		
										   		<%}%>
										   </div>
									   		<%
											longValue = null;
											formatValue = null;
											%>
									   
									  <%}%>
					       		</div>
					       		</div>
					       	</div>
					    </div>
					    
					    <div class="row">
				        	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age/Term")%></label>
					       		<table><tr><td>
					       		<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
									<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()),null,true)%>
									<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
									<%if(sv.premCessAge.equals(0)) {%>
									<%=smartHF.getHTMLVarExt(fw, sv.premCessAge, 0, 60)%>
									<%} else { %>
									<%=smartHF.getHTMLVarExt(fw, sv.premCessAge, 0, 60)%>
									<%} %>
									<%}else {%>
									<%}%>
									</td><td style="width:2px">
                                        </td><td>
									
									<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
									<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()),null,true)%>
									<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
									<%if(sv.premCessTerm.equals(0)) {%>
									<%=smartHF.getHTMLVarExt(fw, sv.premCessTerm, 0, 120)%>
									<%} else { %>
									<%=smartHF.getHTMLVarExt(fw, sv.premCessTerm, 0, 120)%>
									<%} %>
									<%}else {%>
									<%}%>
					       		</td></tr></table>
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Cessation date")%></label>
					       		

					       		<%					
									if(!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
					       		
					       		</div>
					       	</div>
					       	
					       	<!-- ILIFE-7805 : Starts -->
					<div class="col-md-3">
						<div class="form-group">
							<%
								if ((new Byte((sv.singpremtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Single Premium Type"))%></label>
							<%
								}
							%>
							<div class="input-group">
								<%
									if ((new Byte((sv.singpremtype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
										fieldItem = appVars.loadF4FieldsLong(new String[] { "singpremtype" }, sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("singpremtype");
										optionValue = makeDropDownList(mappedItems, sv.singpremtype.getFormData(), 2, resourceBundleHandler);
										longValue = (String) mappedItems.get((sv.singpremtype.getFormData()).toString().trim());
								%>

								<%
									if ((new Byte((sv.singpremtype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
												|| (((ScreenModel) fw).getVariables().isScreenProtected())) {
								%>
								<div
									class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell" : "output_cell"%>'
									style='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "width:82px;" : "width:140px;"%>'>

									<%
										if (longValue != null) {
									%>

									<%=longValue%>

									<%
										}
									%>
								</div>

								<%
									longValue = null;
								%>

								<%
									} else {
								%>

								<%
									if ("red".equals((sv.singpremtype).getColor())) {
								%>
								<div
									style="border: 2px; border-style: solid; border-style: solid; border-color: #ec7572 !important;
	/* width: 149px; */ height: 34px">
									<%
										}
									%>

									<select name='singpremtype' type='list' style="width: 145px;"
										<%if ((new Byte((sv.singpremtype).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {%>
										readonly="true" disabled class="output_cell"
										<%} else if ((new Byte((sv.singpremtype).getHighLight()))
							.compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
										class="bold_cell" <%} else {%> class='input_cell'
										<%}%>>
										<%=optionValue%>
									</select>
									<% if("red".equals((sv.singpremtype).getColor())){
                                    %>
								</div>
								<%
}
                                %>
								<%
									formatValue=null;
									longValue = null;
								%>
								<%
}
} 
%>
							</div>
						</div>
					</div>
					<!-- ILIFE-7805-ends -->
					       	
					       	 
			       	 	</div>
					   
					  <hr>  
					


			 <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
					BaseScreenData.INVISIBLE)) != 0){%>
 			<div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
						<%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%>
						<%} %></label>
		       		<div class="input-group">
						<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()),null,true)%>
						<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
						<%if(sv.adjustageamt.equals(0)) {%>
						<%=smartHF.getHTMLVarExt(fw, sv.adjustageamt, 0, 145)%>
						<%} else { %>
						<%=smartHF.getHTMLVarExt(fw, sv.adjustageamt, 0, 145)%>
						<%} %>
						<%}else {%>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
							<%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%>
							<%} %></label>
		       		<div class="input-group">
		       		
							<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()),null,true)%>
							<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
							<%if(sv.rateadj.equals(0)) {%>
							<%=smartHF.getHTMLVarExt(fw, sv.rateadj, 0, 145)%>
							<%} else { %>
							<%=smartHF.getHTMLVarExt(fw, sv.rateadj, 0, 145)%>
							<%} %>
							<%}else {%>
							<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label>		       		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
					<%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%>
					<%} %></label>
		       		<div class="input-group">
							<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()),null,true)%>
							<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
							<%if(sv.fltmort.equals(0)) {%>
							<%=smartHF.getHTMLVarExt(fw, sv.fltmort, 0, 145)%>
							<%} else { %>
							<%=smartHF.getHTMLVarExt(fw, sv.fltmort, 0, 145)%>
							<%} %>
							<%}else {%>
							<%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label>	<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
						<%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%>
						<%} %></label>
		       		<div class="input-group">
							<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
							<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()),null,true)%>
							<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
							<%if(sv.loadper.equals(0)) {%>
							<%=smartHF.getHTMLVarExt(fw, sv.loadper, 0, 145)%>
							<%} else { %>
							<%=smartHF.getHTMLVarExt(fw, sv.loadper, 0, 145)%>
							<%} %>
							<%}else {%>
							<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    <%} %>
		    
		    
		    <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0){ %>
				     
				     
				     
				     <div class="row">
			        	<div class="col-md-3">
				       		<div class="form-group">
				       		<label><%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%>
								<%} %></label>
				       		<div class="input-group">

								<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
								<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()),null,true)%>
								<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
								<%if(sv.premadj.equals(0)) {%>
								<%=smartHF.getHTMLVarExt(fw, sv.premadj, 0, 145)%>
								<%} else { %>
								<%=smartHF.getHTMLVarExt(fw, sv.premadj, 0, 145)%>
								<%} %>
								<%}else {%>
								<%}%>

				       		</div>
				       		</div>
				       	</div>

				  
		    
		    <%} %>
		    
		      
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<!-- ILIFE-8323 Starts -->
		       		 <%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0){ %>
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
		       		<% } %>
		       		<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) == 0){ %>
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
		       		<% } %>
		       		<!-- ILIFE-8323 ends -->
		       		<div class="input-group">
		       		<%	
							qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
							
							if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="text-align: right;width: 145px !important;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" style="width: 145px !important;"></div>
							
							<% 
								} 
							%>
							
						<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    
		    
		    
		    <%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
				BaseScreenData.INVISIBLE)) != 0){ %>
		     
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%>
					<%} %></label>
		       		<div class="input-group" style="min-width: 145px !important;">
		       		<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
						<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()),null, true)%>
						<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
						<%if(sv.zbinstprem.equals(0)) {%>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS) %>
						<%} else { %>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
						<%} %>
						<%}else {%>
						<%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <%}%>
		    
		    
		     <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment Premium")%></label>
		       		<div class="input-group">
		       			<%	
							qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
							//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
							formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
							
							if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if(longValue == null || longValue.equalsIgnoreCase("")) { 			
									formatValue = formatValue( formatValue );
									} else {
									formatValue = formatValue( longValue );
									}
							}
					
							if(!formatValue.trim().equalsIgnoreCase("")) {
						%>
								<div class="output_cell" style="text-align: right; width: 145px !important;">	
									<%= XSSFilter.escapeHtml(formatValue)%>
								</div>
						<%
							} else {
						%>
						
								<div class="blank_cell" style="width: 145px !important;"></div>
						
						<% 
							} 
						%>
		       		
		       		</div>
		       		</div>
		       	</div>
		  
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText35).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<% //ILJ-387
					 if (sv.cntEnqScreenflag.compareTo("N") == 0){ %> 
					<%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%>
					<%}%>
					<%}%>
					</label>
		       		<div class="input-group">
						<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
						<% //ILJ-387
								if (sv.cntEnqScreenflag.compareTo("N") == 0){ %> 
								<%	
									qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
									//ILIFE-1474 STARTS
									//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
									formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									//ILIFE-1474 ENDS
									if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
											if(longValue == null || longValue.equalsIgnoreCase("")) { 			
											formatValue = formatValue( formatValue );
											} else {
											formatValue = formatValue( longValue );
											}
									}
									if(!formatValue.trim().equalsIgnoreCase("")) {
								%>
										<div class="output_cell" style="text-align: right; width: 145px !important;">	
											<%= XSSFilter.escapeHtml(formatValue)%>
										</div>
								<%
									} else {
								%>
										<div class="blank_cell" style="width: 145px !important;"></div>
								<% 
									} 
								%>
								<%
								longValue = null;
								formatValue = null;
								%>
						 <%}%>
						 <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<!-- ALS-7685 starts-->
		       	<div class="col-md-3">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
					<div style="width: 150px;">
						<%
							if (((BaseScreenData) sv.zstpduty01) instanceof StringBase) {
						%>
						<%=smartHF.getRichText(0, 0, fw, sv.zstpduty01, (sv.zstpduty01.getLength() + 1), null)
							.replace("absolute", "relative")%>
						<%
							} else if (((BaseScreenData) sv.zstpduty01) instanceof DecimalData) {
						%>
						<%
							if (sv.zstpduty01.equals(0)) {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'blank_cell\' ", "class=\'blank_cell\' style=\'width: 145px !important;\' ")%>
						<%
							} else {
						%>
						<%=smartHF
								.getHTMLVar(0, 0, fw, sv.zstpduty01,
										COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
								.replace("class=\'output_cell \' ",
										"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
						<%
							}
						%>
						<%
							} else {
						%>
						<%
							}
						%>
					</div>
				</div>
			</div>
			
		       	<!-- ALS-7685 ends -->
		       	
		   </div>
		  </div>
                    
                    	<div class="tab-pane fade" id="processing_tab">
                    	 <div class="row">
				        	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Ann. Proc. date")%></label>
					       		
									<%					
										if(!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="width: 80px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
					       		
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate date")%></label>
					       		
									<%					
									if(!((sv.rerateDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
  
					       		
					       		</div>
					       	</div>
					       	
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate from date")%></label>
					       		
										<%					
										if(!((sv.rerateFromDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="width: 80px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
					       		
					       		</div>
					       	</div>
					    	</div>
                    	
                    	
                    	 <div class="row">
				        	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Unit Statement date")%></label>
					       		
									<%					
										if(!((sv.unitStatementDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
													
															if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.unitStatementDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
															
															
													} else  {
																
													if(longValue == null || longValue.equalsIgnoreCase("")) {
																formatValue = formatValue( (sv.unitStatementDateDisp.getFormData()).toString()); 
															} else {
																formatValue = formatValue( longValue);
															}
													
													}
													%>			
												<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="width: 80px;">
												<%=XSSFilter.escapeHtml(formatValue)%>
											</div>	
										<%
										longValue = null;
										formatValue = null;
										%>
					       	
					       		</div>
					       	</div>
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit bill date")%></label>
					       		
									<%					
									if(!((sv.benBillDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.benBillDateDisp.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
					       		
					       		</div>
					       	</div>
					       	
					       	
					       	<div class="col-md-3">
					       		<div class="form-group">
					       		<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit debt amount o/s")%></label>
					       		<div class="input-group">
										<%	
											qpsf = fw.getFieldXMLDef((sv.coverageDebt).getFieldName());
											//ILIFE-1474 STARTS
											//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf,sv.coverageDebt,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
											//ILIFE-1474 ENDS
											if(!((sv.coverageDebt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
													if(longValue == null || longValue.equalsIgnoreCase("")) { 			
													formatValue = formatValue( formatValue );
													} else {
													formatValue = formatValue( longValue );
													}
											}
									
											if(!formatValue.trim().equalsIgnoreCase("")) {
										%>
												<div class="output_cell" style="min-width: 145px; text-align: right;">	
													<%= XSSFilter.escapeHtml(formatValue)%>
												</div>
										<%
											} else {
										%>
										
												<div class="blank_cell" style="min-width: 145px;"></div>
										
										<% 
											} 
										%>
										
									
								 <%
										longValue = null;
										formatValue = null;
										%>
					       		</div>
					       		</div>
					       	</div>
					    </div>
			
			</div>
		           	
       </div>
     </div>
 </div>
 <Div id='mainForm_OPTS' style='visibility: hidden;'>

	<%=smartHF.getMenuLink(sv.optextind, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
	<%=smartHF.getMenuLink(sv.taxind,
					resourceBundleHandler.gettingValueFromBundle("Tax Details"))%>
					<%=smartHF.getMenuLink(sv.optsmode,
					resourceBundleHandler.gettingValueFromBundle("Benefit Schedule"))%>
					
					</Div>

		    
						

<%@ include file="/POLACommon2NEW.jsp"%>

	<div style='visibility:hidden;'><table>
							<tr style='height:22px;'><td width='188'>&nbsp; &nbsp;<br/>
							
								
							  		
									<%					
									if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  
								
							
							</td>
							
							<td width='188'>&nbsp; &nbsp;<br/>
							
								
							  		
									<%	
										qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
										
										if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
												if(longValue == null || longValue.equalsIgnoreCase("")) { 			
												formatValue = formatValue( formatValue );
												} else {
												formatValue = formatValue( longValue );
												}
										}
								
										if(!formatValue.trim().equalsIgnoreCase("")) {
									%>
											<div class="output_cell">	
												<%= XSSFilter.escapeHtml(formatValue)%>
											</div>
									<%
										} else {
									%>
									
											<div class="blank_cell" />
									
									<% 
										} 
									%>
									<%
									longValue = null;
									formatValue = null;
									%>
								
							 
								
							
							</td></tr>
							
							<tr style='height:22px;'><td width='188'>
							
							<div class="label_txt">
							<%=resourceBundleHandler.gettingValueFromBundle("")%>
							</div>
							</td>
							</tr></table></div> 



 
