<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "Sr59a";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr59aScreenVars sv = (Sr59aScreenVars) fw.getVariables();%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Update User ID ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Update Date ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Owner ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Commencement Date ");%>
	<%StringData generatedText9= resourceBundleHandler.gettingValueFromBundle(StringData.class,"Paid To Date    ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Auto Fund Rebalancing (AFR)");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"AFR Freq / Risk Prof");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last AFR Date");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Next AFR Date");%>
<style type="text/css">

.panel.panel-default{

  height: 680px !important;
}
}

</style>
	
	<div class="panel panel-default">
 <div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
				<table><tr><td>
					<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td><td>
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td></tr>
				</table>
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Last Update User ID ")%></label>
				<div class="input-group">
					<%					
		if(!((sv.zusrprf.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zusrprf.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zusrprf.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				</div>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Last Update Date ")%></label>
				<div class="input-group">
					<%					
		if(!((sv.zlstupdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zlstupdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zlstupdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
				
				</div>
				</div>
				</div>		
		<!-- row 2 -->	
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></label>
				<div class="input-group">
				  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				</div>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
				<div class="input-group">
						<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				</div>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				</div>		
				
		<!-- row 3 -->	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				</div>		
				
		<!-- row 4 -->
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
				<table><tr><td>
					<%					
		if(!((sv.cownnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


</td><td>


	
  		
		<%					
		if(!((sv.ownername.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="margin-left:1px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
				</td></tr>
				</table>
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commencement Date ")%></label>
				<div class="input-group">
					<%					
		if(!((sv.currfromDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.currfromDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
				</div>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date ")%></label>
				<div class="input-group">
					<%					
		if(!((sv.ptdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ptdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
				</div>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				</div>		
				
		<!-- row 5 -->		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Fund Rebalancing (AFR)")%></label>
				<div class="input-group">
					<div 	
		class='<%= (sv.zafropt1disp.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'style="width:100px">
		<%					
		if(!((sv.zafropt1disp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "zafropt1disp" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("zafropt1disp");
								formatValue = formatValue( ((String) mappedItems.get((sv.zafropt1disp.getFormData()).toString().trim())).toString());
								
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zafropt1disp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("AFR Freq / Risk Prof")%></label>
				<div class="input-group">
				<div 	
		class='<%= (sv.zafritemdisp.getFormData()).trim().length() == 0 ? 
						"blank_cell" : "output_cell" %>'style="width:100px">
		<%					
		if(!((sv.zafritemdisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								fieldItem = appVars.loadF4FieldsLong(new String[] { "zafritemdisp" }, sv, "E", baseModel);
								mappedItems = (Map) fieldItem.get("zafritemdisp");
								formatValue = formatValue( ((String) mappedItems.get((sv.zafritemdisp.getFormData()).toString().trim())).toString());
								
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zafritemdisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Last AFR Date ")%></label>
				<div class="input-group">
						<%					
		if(!((sv.zlastdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zlastdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zlastdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	
				</div>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Next AFR Date ")%></label>
				<div class="input-group">
				
				<%					
		if(!((sv.znextdteDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.znextdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.znextdteDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:100px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
				</div>
				
				</div>
				</div>
				</div>		
				
		<!-- row 6 -->		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				</div>		
				
				<!-- row 7 -->	
				<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="width: 200px;">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Auto Fund Rebalancing (AFR)")%></label>
				<%
			fieldItem = appVars.loadF4FieldsLong(
					new String[] { "zafropt1" }, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("zafropt1");
			optionValue = makeDropDownList(mappedItems, sv.zafropt1
					.getFormData(), 2);
		longValue = (String) mappedItems.get((sv.zafropt1.getFormData()).toString().trim());  
		%>
		
		<% 
			if((new Byte((sv.zafropt1).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
		<div class='output_cell'>  
		   <%=longValue%>
		</div>
		
		<%
		longValue = null;
		%>
		
			<% }else {%>

<% if("red".equals((sv.zafropt1).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
		
		<select name='zafropt1' type='list' style="width: 140px;"
<% 
	if((new Byte((sv.zafropt1).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zafropt1).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
			<%=optionValue%>
		</select>
<% if("red".equals((sv.zafropt1).getColor())){
%>
</div>
<%
}  
%> 
		
		<%
			} 
		%> 
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label><%=resourceBundleHandler.gettingValueFromBundle("AFR Freq / Risk Prof")%></label>
					<%
			fieldItem = appVars.loadF4FieldsLong(
					new String[] { "zafritem" }, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("zafritem");
			optionValue = makeDropDownList(mappedItems, sv.zafritem
					.getFormData(), 2);
		longValue = (String) mappedItems.get((sv.zafritem.getFormData()).toString().trim());  
		%>
		<input id="afrval" type="hidden" value ="<%=sv.zafritem.getFormData().toString().trim() %>" />
		<% 
			if((new Byte((sv.zafritem).getEnabled()))
			.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
		%>  
		<div class='output_cell'> 
		   <%=longValue%>
		</div>
		
		<%
		longValue = null;
		%>
		
			<% }else {%>

<% if("red".equals((sv.zafritem).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:140px;"> 
<%
} 
%>
		
		<select name='zafritem' id='zafritem' type='list' style="width: 140px;"
<% 
	if((new Byte((sv.zafritem).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.zafritem).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
			<%=optionValue%>
		</select>
<% if("red".equals((sv.zafritem).getColor())){
%>
</div>
<%
}  
%> 
		
		<%
			} 
		%> 
				
				</div>
				</div>
				
			<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				
		<div class="col-md-3">
				<div class="form-group">
				<label></label>
				
				</div>
				</div>
				</div>		
				</div>
				</div>					
				
<%@ include file="/POLACommon2NEW.jsp"%>					