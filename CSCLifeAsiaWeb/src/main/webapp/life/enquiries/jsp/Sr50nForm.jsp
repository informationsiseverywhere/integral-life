

   <%@ page language="java" pageEncoding="UTF-8" %>
    <%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR50N";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr50nScreenVars sv = (Sr50nScreenVars) fw.getVariables();%>

  <%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	   <%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract        ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status    ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran No ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Tran Date");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Eff Date");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Description");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User ID");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Document");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Statz");%>
<%		appVars.rollup(new int[] {93});
%>



	<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' style="margin-left:1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
</td><td>
						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData()).toString());
						 		} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>' 
							style="max-width:165px; margin-left:1px;min-width: 100px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
					</td></tr></table>
					</div>
				</div>
				
				<div class="col-md-2">
						<div class="form-group"> 
							<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
											BaseScreenData.INVISIBLE)) != 0) { %>						
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<%}%>
							<div class="input-group" style="width:80px;"> 
							<%if ((new Byte((sv.register).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
							<%	
								fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
								mappedItems = (Map) fieldItem.get("register");
								longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
							%>
							<%					
									if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.register.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.register.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
									%>			
								 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
													"blank_cell" : "output_cell" %>'>  
								   		<%if(longValue != null){%>
								   		<%=longValue%>
								   		<%}%>
								   </div>
									<%
									longValue = null;
									formatValue = null;
									%>
							<%}%>							
							
						</div>
				</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
							<div class="input-group" style="width:80px;"> 
						<%					
						if(!((sv.cntcurr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
						%>			
							<div class='output_cell'> 
						   		<%=	(sv.cntcurr.getFormData()).toString()%>
						   	</div>
						<%
						longValue = null;
						formatValue = null;
						%>						
					</div>
				</div>						
				</div>
			
			
			       
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						<div 	style="width:80px;"
							class='<%= (sv.chdrstatus.getFormData()).trim().length() == 0 ? 
											"blank_cell" : "output_cell" %>'>
							<%					
							if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>				
					</div>
				</div>
				
				
				
				<div class="col-md-2">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
							<div style="width: 100px;"	
								class='<%= (sv.premstatus.getFormData()).trim().length() == 0 ? 
												"blank_cell" : "output_cell" %>'>
								<%					
								if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
				</div>	
					</div>
			</div>		

			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
				<table><tr><td>
							<%
								if (!((sv.lifenum.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div id="lifcnum"
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
</td><td>
							<%
								if (!((sv.lifename.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifename.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div id="lifename"
								class='<%=(sv.lifename.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="width: 100px; margin-left:1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							</td></tr></table>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						
						<table><tr><td>
							<%
								if (!((sv.jlife.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div id="jlife"
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="width: 100px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
</td><td>
							<%
								if (!((sv.jlifename.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifename.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div id="jlifename"
								class='<%=(sv.jlifename.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="width: 100px; margin-left:1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
			</td></tr></table>
								
					</div>
				</div>							
			</div>
		
		
		<div class="row">        
				<div class="col-md-1" style="min-width:88px;">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Tran No.")%></label>
        	
        	<input name='trannosearch' type='text' size='4'
	        	<%
	        		formatValue = (sv.trannosearch.getFormData()).toString();
		        	if("00000".equalsIgnoreCase(formatValue)){
		    			formatValue="";
		    		}
	        	%>
	        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
	        	<%
	        		if (formatValue != null && formatValue.trim().length() > 0) {
	        	%>
	        		title='<%=formatValue%>' <%}%>
	        		size='<%=sv.trannosearch.getLength()%>'
	        		maxLength='<%=sv.trannosearch.getLength()%>' onFocus='doFocus(this)'
	        		onHelp='return fieldHelp(trannoSearch)'
	        		onKeyUp='return checkMaxLength(this)'
	        		onkeypress='return isNumeric(event)'
	        		onPaste="return false"
	        	<%
	        		if ((new Byte((sv.trannosearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
	        	%>
	        		readonly="true" class="output_cell"
	        	<%
	        		} else if ((new Byte((sv.trannosearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
	        	%>
	        		class="bold_cell" 
	        	<%
	        		} else {
	        	%>
	        		class=' <%=(sv.trannosearch).getColor() == null ? "input_cell"
							: (sv.trannosearch).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
	        	<%}%>
        	/>
      </div></div>
		    
				<div class="col-md-1" style="min-width:162px;">
				<div class="form-group"> 
				
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Tran Date")%></label>
        <%
						if ((new Byte((sv.datesubsearchDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.datesubsearchDisp, (sv.datesubsearchDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.datesubsearchDisp, (sv.datesubsearchDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
        	
      	</div></div>
		
		
		<div class="col-md-1" style="min-width:162px; margin-left:-40px;">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Effective Date")%></label>
 <%
						if ((new Byte((sv.effdatesearchDisp).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
								|| fw.getVariables().isScreenProtected()) {
					%>
					<%=smartHF.getRichTextDateInput(fw, sv.effdatesearchDisp, (sv.effdatesearchDisp.getLength()))%>
					<%
						} else {
					%>
					<div class="input-group date form_date col-md-12" data-date=""
						data-date-format="dd/mm/yyyy" data-link-field="startDateDisp"
						data-link-format="dd/mm/yyyy">
						<%=smartHF.getRichTextDateInput(fw, sv.effdatesearchDisp, (sv.effdatesearchDisp.getLength()))%>
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<%
						}
					%>
        	        	 </div></div>
        	        	 
        	        	 
        	        	 
        	<div class="col-md-1" style="min-width:98px; margin-left:-40px;"">
				<div class="form-group"> 
				
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Tran code")%></label>
        	
        	<input name='trcodesearch' type='text' size='4' 
        		<%
	        		formatValue = (sv.trcodesearch.getFormData()).toString();
	        	%>
	        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
	        	<%
	        		if (formatValue != null && formatValue.trim().length() > 0) {
	        	%>
	        		title='<%=formatValue%>' <%}%>
	        		size='<%=sv.trcodesearch.getLength()%>'
	        		maxLength='<%=sv.trcodesearch.getLength()%>' onFocus='doFocus(this)'
	        		onHelp='return fieldHelp(trcodesearch)'
	        		onKeyUp='return checkMaxLength(this)'
	        	<%
	        		if ((new Byte((sv.trcodesearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
	        	%>
	        		readonly="true" class="output_cell"
	        	<%
	        		} else if ((new Byte((sv.trcodesearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
	        	%>
	        		class="bold_cell" 
	        	<%
	        		} else {
	        	%>
	        		class=' <%=(sv.trcodesearch).getColor() == null ? "input_cell"
							: (sv.trcodesearch).getColor().equals("red") ? "input_cell red reverse"
									: "input_cell"%>'
	        	<%}%>
        	/>
 </div></div>

        	
 <div class="col-md-1" style="min-width:141px;">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Tran Description")%></label>
        	
        	<input name='trandescsearch' type='text' size='20' 
        		<%
        		formatValue = (sv.trandescsearch.getFormData()).toString();
        	%>
        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
        	<%
        		if (formatValue != null && formatValue.trim().length() > 0) {
        	%>
        		title='<%=formatValue%>' <%}%>
        		size='<%=sv.trandescsearch.getLength()%>'
        		maxLength='<%=sv.trandescsearch.getLength()%>' onFocus='doFocus(this)'
        		onHelp='return fieldHelp(trandescsearch)'
        		onKeyUp='return checkMaxLength(this)'
        	<%
        		if ((new Byte((sv.trandescsearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
        	%>
        		readonly="true" class="output_cell"
        	<%
        		} else if ((new Byte((sv.trandescsearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
        	%>
        		class="bold_cell" 
        	<%
        		} else {
        	%>
        		class=' <%=(sv.trandescsearch).getColor() == null ? "input_cell"
						: (sv.trandescsearch).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
        	<%}%>
        	/>
 </div></div>
		
		<div class="col-md-1" style="min-width:80px;">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("User ID")%></label>
        	<br/>
        	<input name='crtusersearch' type='text' size='8' 
        		<%
        		formatValue = (sv.crtusersearch.getFormData()).toString();
        	%>
        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
        	<%
        		if (formatValue != null && formatValue.trim().length() > 0) {
        	%>
        		title='<%=formatValue%>' <%}%>
        		size='<%=sv.crtusersearch.getLength()%>'
        		maxLength='<%=sv.crtusersearch.getLength()%>' onFocus='doFocus(this)'
        		onHelp='return fieldHelp(crtusersearch)'
        		onKeyUp='return checkMaxLength(this)'
        	<%
        		if ((new Byte((sv.crtusersearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
        	%>
        		readonly="true" class="output_cell"
        	<%
        		} else if ((new Byte((sv.crtusersearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
        	%>
        		class="bold_cell" 
        	<%
        		} else {
        	%>
        		class=' <%=(sv.crtusersearch).getColor() == null ? "input_cell"
						: (sv.crtusersearch).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
        	<%}%>
        	/>
   </div></div>
		
		<div class="col-md-1" style="min-width:127px;">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Document No.")%></label>
        	
        	<input name='rdocnumsearch' type='text' size='8' 
        		<%
        		formatValue = (sv.rdocnumsearch.getFormData()).toString();
        	%>
        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
        	<%
        		if (formatValue != null && formatValue.trim().length() > 0) {
        	%>
        		title='<%=formatValue%>' <%}%>
        		size='<%=sv.rdocnumsearch.getLength()%>'
        		maxLength='<%=sv.rdocnumsearch.getLength()%>' onFocus='doFocus(this)'
        		onHelp='return fieldHelp(rdocnumsearch)'
        		onKeyUp='return checkMaxLength(this)'
        	<%
        		if ((new Byte((sv.rdocnumsearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
        	%>
        		readonly="true" class="output_cell"
        	<%
        		} else if ((new Byte((sv.rdocnumsearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
        	%>
        		class="bold_cell" 
        	<%
        		} else {
        	%>
        		class=' <%=(sv.rdocnumsearch).getColor() == null ? "input_cell"
						: (sv.rdocnumsearch).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
        	<%}%>
        	/>
 </div></div>
		<div class="col-md-1">
				<div class="form-group">
        	<label><%=resourceBundleHandler.gettingValueFromBundle("Status")%></label>
        	
        	<input name='statzsearch' type='text' size='5' 
        		<%
        		formatValue = (sv.statzsearch.getFormData()).toString();
        	%>
        		value='<%= XSSFilter.escapeHtml(formatValue)%>'
        	<%
        		if (formatValue != null && formatValue.trim().length() > 0) {
        	%>
        		title='<%=formatValue%>' <%}%>
        		size='<%=sv.statzsearch.getLength()%>'
        		maxLength='<%=sv.statzsearch.getLength()%>' onFocus='doFocus(this)'
        		onHelp='return fieldHelp(statzsearch)'
        		onKeyUp='return checkMaxLength(this)'
        	<%
        		if ((new Byte((sv.statzsearch).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0) {
        	%>
        		readonly="true" class="output_cell"
        	<%
        		} else if ((new Byte((sv.statzsearch).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {
        	%>
        		class="bold_cell" 
        	<%
        		} else {
        	%>
        		class=' <%=(sv.statzsearch).getColor() == null ? "input_cell"
						: (sv.statzsearch).getColor().equals("red") ? "input_cell red reverse"
								: "input_cell"%>'
        	<%}%>
        	/>
      	</div></div></div>
		<br><br>
		
			<%

int totalTblWidth = 0;

		GeneralTable sfl = fw.getTable("sr50nscreensfl");
		int height;
		totalTblWidth=1000;
		if(sfl.count()*27 > 345) {
		height = 345 ;
		} else {
		height = sfl.count()*27;
		}
		
		%>

<%-- <div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" style=' display:inline-block; position: relative; top:-4px; width: <%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%>  height: 325px;  border:#316494 1px solid; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 120) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		 <DIV id="subf" style="POSITION: relative font-weight: bold; WIDTH: <%=totalTblWidth%>px;margin-left: -4px; HEIGHT: 345px;  ">
		<table style="border:1px; width:100%;" bgcolor="#dddddd" cellspacing="1px">		
		
		<tr style="background:#316494;">
														<td style=" color:white; font-weight: bold; padding: 5px; border-right: 1px solid #dddddd;z-index:10;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1);left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1);" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></td>									
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header2")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header3")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header4")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header5")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header6")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header7")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header8")%></td>
										
														<td style=" color:white; font-weight: bold; padding: 5px; z-index:7;position:relative;top:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollTop-1); border-right: 1px solid #dddddd;K" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header9")%></td>
										
				
			
		</tr>
		 --%>
		 
		 
		 <style type="text/css">
.fakeContainer {
	width:750px;		
	height:300px;	/*ILIFE-2143*/
	top: 160px;
	left:4px;			
}
.sSky th, .sSky td{
font-size:12px !important;
}
.sr50nTable tr{height:35px}
</style>
<script language="javascript">
        $(document).ready(function(){
	
			new superTable("sr50nTable", {
				fixedCols : 0,					
				colWidths : [30,100,100,100,100,150,100,100,100],
				hasHorizonScroll :"Y",
				moreBtn: "N",	/*ILIFE-2143*/
				moreBtnPath: "<%=ctx%>screenFiles/<%=localeimageFolder%>/moreButton.gif",
				isReadOnlyFlag: true				
				
			});

        });
    </script>
    <script>
	  function isNumeric(evt) 
	  {
	 		evt = (evt) ? evt : window.event;
	 		var charCode = (evt.which) ? evt.which : evt.keyCode;
	 		if(charCode >= 48 && charCode <= 57){
	 			return true;
	 		}else{
	 			return false;
	 		}
	 	}
</script>
<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-sr50n'>
							<thead>
								<tr class='info'>
		
		<th><%=resourceBundleHandler.gettingValueFromBundle("Sel")%></th>
		         								
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction No.")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction Date")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("EffectiveDate")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Transaction Code")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Description")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("UserId")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Document No.")%></th>
					<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Status")%></th>
				
	</tr></thead>
	
		<%

	String backgroundcolor="#FFFFFF";
	
	Sr50nscreensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (Sr50nscreensfl
	.hasMoreScreenRows(sfl)) {
	
%>

	<tr style="background:<%= backgroundcolor%>;">
						    									<td style="color:#434343; padding: 5px; z-index:8;position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;" align="left">														
																	
													
					
					 					 
					 <input type="radio" 
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("sr50nscreensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 name='sr50nscreensfl.select_R<%=count%>'
						  id='sr50nscreensfl.select_R<%=count%>'
						 onClick="selectedRow('sr50nscreensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 					
					
											
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.tranno.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.datesubDisp.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.effdateDisp.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.trcode.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.trandesc.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.crtuser.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.rdocnum.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; font-weight: bold;font-size: 12px; font-family: Arial; border-right: 1px solid #dddddd;" align="left">									
																
									
											
						<%= sv.statz.getFormData()%>
						
														 
				
									</td>
	</tr>


	<%
	if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
		backgroundcolor="#ededed";
		}else{
		backgroundcolor="#FFFFFF";
		}
	count = count + 1;
	Sr50nscreensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
		</table>
		</div>
		
	
	
		</div>
	</div>
</div>

<div>
	    <table>
	    <tr>
		<td>
		     <div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(1)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Component")%></a>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<a href="#" onClick="JavaScript:perFormOperation(2)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Despatch")%></a>
			</div>
		</td>
		</tr>
		</table>
</div>	


</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTables-sr50n').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        paging: false,
        info: false
        
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>

