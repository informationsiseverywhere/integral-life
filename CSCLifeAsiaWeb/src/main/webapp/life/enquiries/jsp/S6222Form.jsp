<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6222";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6222ScreenVars sv = (S6222ScreenVars) fw.getVariables();%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CLIENT NUMBER  -");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"CLIENT NAME");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"RUNNING TOTAL SUM/ASS - ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Type");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured");%>
<%		appVars.rollup(new int[] {93});
%>

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Client")%></label>
		       		<div class="input-group">
					<%					
							if(!((sv.clntnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.clntnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 80px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					<div class="output_cell" style="max-width: 600px;">
						
					
							<%					
							if(!((sv.name2.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.name2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.name2.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									
									<%=XSSFilter.escapeHtml(formatValue)%>
								
							<%
							longValue = null;
							formatValue = null;
							%>
					  		
							<%					
							if(!((sv.name1.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.name1.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.name1.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									
									<%=formatValue%>
								
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    </div>
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Running Total Sum Assured")%></label>
		       		<div class="input-group">
							<%	
								qpsf = fw.getFieldXMLDef((sv.tsumins).getFieldName());
								//qpsf.setPicinHTML(COBOLHTMLFormatter.S16VS2);
								formatValue = smartHF.getPicFormatted(qpsf,sv.tsumins,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
								
								if(!((sv.tsumins.getFormData()).toString()).trim().equalsIgnoreCase("")) {
										if(longValue == null || longValue.equalsIgnoreCase("")) { 			
										formatValue = formatValue( formatValue );
										} else {
										formatValue = formatValue( longValue );
										}
								}
						
								if(!formatValue.trim().equalsIgnoreCase("")) {
							%>
									<div class="output_cell" style="min-width:100px; text-align:right;">	
										<%= XSSFilter.escapeHtml(formatValue)%>
									</div>
							<%
								} else {
							%>
							
									<div class="blank_cell" style="min-width:100px;"> &nbsp; </div>
							
							<% 
								} 
							%>
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[6];
int totalTblWidth = 0;
int calculatedValue =0;

						if(resourceBundleHandler.gettingValueFromBundle("Company").length() >= (sv.company.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Company").length())*12;								
			} else {		
				calculatedValue = (sv.company.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Contract No").length() >= (sv.chdrnum.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Contract No").length())*10;								
			} else {		
				calculatedValue = (sv.chdrnum.getFormData()).length()*10;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Type").length() >= (sv.cnttype.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Type").length())*12;								
			} else {		
				calculatedValue = (sv.cnttype.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Component").length() >= (sv.crtable.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Component").length())*12;								
			} else {		
				calculatedValue = (sv.crtable.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Currency").length() >= (sv.sicurr.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Currency").length())*12;								
			} else {		
				calculatedValue = (sv.sicurr.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Sum Assured").length() >= (sv.sumin.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Sum Assured").length())*12;								
			} else {		
				calculatedValue = (sv.sumin.getFormData()).length()*12;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s6222screensfl");
		int height;
		if(sfl.count()*27 > 320) {
		height = 320 ;
		} else {
		height = sfl.count()*27;
		}	
		%>
		
		<div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		           <div class="table-responsive">
		    	 	<table class="table table-striped table-bordered table-hover" id='dataTables-s6222'>	
			    	 	<thead>
			    	 	<tr class='info'>									
							<th style="width:10px"><center><%=resourceBundleHandler.gettingValueFromBundle("Company")%></center></th>
		         			<th><center><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Type")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Component")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></center></th>
							<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></center></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%
								S6222screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S6222screensfl.hasMoreScreenRows(sfl)) {	
							%>

						<tr class="tableRowTag" id='<%="tablerow"+count%>' >
						    				<td class="tableDataTag tableDataTagFixed" style="max-width: 70px;" align="left">														
																
									
											
						<%= sv.company.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[1 ]%>px;" align="left">									
																
									
											
						<%= sv.chdrnum.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[2 ]%>px;" align="left">									
																
									
											
						<%= sv.cnttype.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[3 ]%>px;" align="left">									
																
									
											
						<%= sv.crtable.getFormData()%>
						
														 
				
									</td>
				    									<td class="tableDataTag" style="width:<%=tblColumnWidth[4 ]%>px;" align="left">									
																
									
											
						<%= sv.sicurr.getFormData()%>
						
														 
				
									</td>
				    			<td class="tableDataTag" align="right" style="width:<%=tblColumnWidth[5 ]%>px;">									
																
																							
									<%	
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.sumin).getFieldName());						
									//qpsf.setPicinHTML(COBOLHTMLFormatter.S16VS2);				
									%>
									
									
									<%
									formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
									if(!sv.
									sumin
									.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
									%>
									<%= formatValue%>
									<%
									longValue = null;
									formatValue = null;
									%>
					 			 		
			 		
			    				 
				
									</td>
					
								</tr>
							
								<%
								count = count + 1;
								S6222screensfl
								.setNextScreenRow(sfl, appVars, sv);
								}
								%>
							</table>
							 
					      </tbody>
					</table>
					</div>
				</div>
		 
			</div>
		</div>
	<input type="text" style="visibility: hidden;margin-top: -60px;height: 3px !important;" id="show_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Showing")%>">	
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="to_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("to")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="of_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("of")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="entries_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("entries")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="nxtbtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Next")%>">
        		<input type="text" style="visibility: hidden;height: 3px !important;" id="prebtn_lbl" value="<%=resourceBundleHandler.gettingValueFromBundle("Previous")%>">
	
	</div>
</div>

<script>
	$(document).ready(function() {
		var showval= document.getElementById('show_lbl').value;
		var toval= document.getElementById('to_lbl').value;
		var ofval= document.getElementById('of_lbl').value;
		var entriesval= document.getElementById('entries_lbl').value;	
		var nextval= document.getElementById('nxtbtn_lbl').value;
		var previousval= document.getElementById('prebtn_lbl').value;
    	$('#dataTables-s6222').DataTable({
        	ordering: false,
        	searching:false,
        	scrollY: "300px",
        	scrollCollapse:true,
      	
			language: {
	            "lengthMenu": showval +" "+ "_MENU_ "+ entriesval,     
	            "EmptyTable":"No data available in table",	
	            "info": showval+" " +"_START_ "+ toval+" " +"_END_ "+ ofval+" " +"_TOTAL_ "+ entriesval,
	            "paginate": {                
	                "next":       nextval,
	                "previous":   previousval
	            }	
	          }    
		});
    });
</script>

<%@ include file="/POLACommon2NEW.jsp"%>
<%
if(!cobolAv3.isPagedownEnabled()){
%>
       <script language="javascript">
       window.onload= function(){
              setDisabledMoreBtn();
       }
       </script>
<%} %>  
