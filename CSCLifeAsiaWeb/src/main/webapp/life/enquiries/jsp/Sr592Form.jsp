

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR592";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr592ScreenVars sv = (Sr592ScreenVars) fw.getVariables();%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Company ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"User Name ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Underwriter Approval Level  ");%>

<%{
		if (appVars.ind09.isOn()) {
			sv.uwlevel.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind10.isOn()) {
			sv.uwlevel.setEnabled(BaseScreenData.DISABLED);
		}
		if (!appVars.ind10.isOn()) {
			sv.uwlevel.setHasCursor(BaseScreenData.HASCURSOR);
		}
		if (appVars.ind09.isOn()) {
			sv.uwlevel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind09.isOn()) {
			sv.uwlevel.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>
	<script>
	function setValues(){
		var v = document.getElementById("uwdecision1");
		if (v.checked) {
			document.getElementById("uwdecision").value="Y";
		} else {
			document.getElementById("uwdecision").value="N";
		}
		
		var v = document.getElementById("skipautouw1");
		if (v.checked) {
			document.getElementById("skipautouw").value="Y";
		} else {
			document.getElementById("skipautouw").value="N";
		}
	}
	</script>

<div class="panel panel-default">
    <div class="panel-body">     
			<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Company")%></label>
					    		<%					
		if(!((sv.company.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.company.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					    	</div>
					    </div>
					    
					   
					    <div class="col-md-4"> 
				    		<div class="form-group" style="width: 200px;">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("User Name")%></label>
					    		<%					
		if(!((sv.userid.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.userid.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.userid.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
					       </div>
					    </div>   
					   
					     <div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Underwriter Approval Level")%></label>
					    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"uwlevel"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("uwlevel");
	optionValue = makeDropDownList( mappedItems , sv.uwlevel.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.uwlevel.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.uwlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.uwlevel).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:210px;"> 
<%
} 
%>

<select name='uwlevel' type='list' style="width:210px;"
<% 
	if((new Byte((sv.uwlevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.uwlevel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.uwlevel).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					    	</div>
					    </div> 				
					 </div>   
					 
					 
					 
	       <div class="row">	
	       <% if (sv.automatciUWFlag.compareTo("N") != 0) { %> 
					
					<div class="col-md-4">
					<div class="form-group">
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Allow to Overwrite U/W Decision")%>
						<input name='uwdecision' id='uwdecision' type="hidden" /> 
							<input name='uwdecision1' id='uwdecision1' type='checkbox' readonly='false' onclick="setValues()"
							<%if ((new Byte((sv.uwdecision).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<%if ((sv.uwdecision).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%} else {%> unchecked <%}%> style="visibility: visible" />
						<%
							}
						%>
					</div>
				</div> 
				<div class="col-md-4">
					<div class="form-group">
						<label> <%=resourceBundleHandler.gettingValueFromBundle("Allow Skip Auto Underwrite")%>
						<input name='skipautouw' id='skipautouw' type="hidden" /> 
							<input name='skipautouw1' id='skipautouw1' type='checkbox' readonly='false' onclick="setValues()"
							<%if ((new Byte((sv.skipautouw).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
							<%if ((sv.skipautouw).toString().trim().equalsIgnoreCase("Y")) {%>
							checked <%} else {%> unchecked <%}%> style="visibility: visible" />
						<%
							}
						%>
					</div>
				</div> 
			<%} %>
					 
	<% if (sv.susur002flag.compareTo("N") != 0) { %>
				<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("POS Approval Level")%></label>
					    		<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"poslevel"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("poslevel");
	optionValue = makeDropDownList( mappedItems , sv.poslevel.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.poslevel.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.poslevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.poslevel).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:210px;"> 
<%
} 
%>

<select name='poslevel' type='list' style="width:210px;"
<% 
	if((new Byte((sv.poslevel).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||fw.getVariables().isScreenProtected()){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.poslevel).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.poslevel).getColor())){
%>
</div>
<%
} 
%>

<%
} 
%>
					    	</div>
					    </div> 				
					 
<%
} 
%> 
					</div>    	 
					 
					 
					 <br>
					 
					 
		</div>
</div>					 		







<%@ include file="/POLACommon2NEW.jsp"%>
