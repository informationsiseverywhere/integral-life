<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6248";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S6248ScreenVars sv = (S6248ScreenVars) fw.getVariables();%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"No ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life no ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Smoking ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Occupation");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Code ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pursuit");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Codes ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage/rider ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reason");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Reas");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rate");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Flat");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ind ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Assured%");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Load%");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"adjust ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Duration");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality%");%>
	
	
	

<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
		       		<div class="input-group">
		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"chdrnum"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("chdrnum");
							longValue = (String) mappedItems.get((sv.chdrnum.getFormData()).toString().trim());  
						%>
						
					    
					   <div class='<%= ((sv.chdrnum.getFormData() == null) || ("".equals((sv.chdrnum.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>'> 
					   <%=(sv.chdrnum.getFormData()).toString()%>
					   </div>
					   
					   <!-- <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'>  
					   		<%if(longValue != null){ %>
					   		<%=longValue%>
					   		<%}%> 
					   		
					   </div>-->
					   
					   <%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life_no")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.life.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.life.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage_no")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
		       		<div class="input-group">
		       		<%					
						if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rider.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.rider.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>'>
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		   
		     
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
						if(!((sv.lifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.lifcnum.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
			  			</td>
			  			<td>
						<%					
						if(!((sv.linsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.linsname.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;margin-left: 1px;max-width:150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
  
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		       	
		       
		    </div> 
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
		       		<div class="input-group">
		       			<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"smoking01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("smoking01");
							longValue = (String) mappedItems.get((sv.smoking01.getFormData()).toString().trim());  
						%>
						
					    
					   <!-- <div class='<%= ((sv.smoking01.getFormData() == null) || ("".equals((sv.smoking01.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>'> 
					   <%=	(sv.smoking01.getFormData()).toString()%>
					   </div>
					   
					    -->
					   
					      <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width:71px;">  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   
					  <%
							longValue = null;
							formatValue = null;
							%> 
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		       	
		       		<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
		       		<div class="input-group">
		       		<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"occup01"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("occup01");
							longValue = (String) mappedItems.get((sv.occup01.getFormData()).toString().trim());  
						%>
						
					   <!-- 
					   <div class='<%= ((sv.occup01.getFormData() == null) || ("".equals((sv.occup01.getFormData()).trim()))) ? 
											"blank_cell" : "output_cell" %>'> 
					   <%=	(sv.occup01.getFormData()).toString()%>
					   </div>
					    --> 
					   
					   
					   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
											"blank_cell" : "output_cell" %>'style="min-width:71px;">  
					   		<%if(longValue != null){%>
					   		<%=longValue%>
					   		<%}%>
					   </div>
					   
					   
					  <%
							longValue = null;
							formatValue = null;
							%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       
		       	
		       	<div class="col-md-4" >
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
						if(!((sv.pursuit01.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit01.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
			  			</td>
			  			<td>
						<%					
						if(!((sv.pursuit02.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit02.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>
		    
		  
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
							if(!((sv.jlifcnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlifcnum.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 71px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
					  		</td>
					  		<td>
				  		
							<%					
							if(!((sv.jlinsname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
										
												if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
												
												
										} else  {
													
										if(longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue( (sv.jlinsname.getFormData()).toString()); 
												} else {
													formatValue = formatValue( longValue);
												}
										
										}
										%>			
									<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
											"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div>	
							<%
							longValue = null;
							formatValue = null;
							%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>  
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Smoking")%></label>
		       		<div class="input-group">
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"smoking02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("smoking02");
						longValue = (String) mappedItems.get((sv.smoking02.getFormData()).toString().trim());  
					%>
					
				    <!--
				     <div class='<%= ((sv.smoking02.getFormData() == null) || ("".equals((sv.smoking02.getFormData()).trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;"> 
				   <%=	(sv.smoking02.getFormData()).toString()%>
				   </div>
				   
				      -->
				  
				   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">  
				   		<%if(longValue != null){%>
				   		<%=longValue%>
				   		<%}%>
				   </div>
				   
				   	<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Occupation")%></label>
		       		<div class="input-group">
		       		<%	
						fieldItem=appVars.loadF4FieldsLong(new String[] {"occup02"},sv,"E",baseModel);
						mappedItems = (Map) fieldItem.get("occup02");
						longValue = (String) mappedItems.get((sv.occup02.getFormData()).toString().trim());  
					%>
					
				    <!-- 
				   <div class='<%= ((sv.occup02.getFormData() == null) || ("".equals((sv.occup02.getFormData()).trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 80px;"> 
				   <%=	(sv.occup02.getFormData()).toString()%>
				   </div> 
				     -->
				   
				   
				   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">  
				   		<%if(longValue != null){%>
				   		<%=longValue%>
				   		<%}%>
				   </div>
				   
				   
				  	<%
						longValue = null;
						formatValue = null;
						%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	 	
		       	
		       	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Pursuit")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
						if(!((sv.pursuit03.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit03.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
						</td>
						<td>
						<%					
						if(!((sv.pursuit04.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.pursuit04.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       		</td>
		       		</tr>
		       		</table>
		       		</div>
		       	</div>
		    </div>  
		
		    
		    <div class="row">
	        	<div class="col-md-4">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage/rider")%></label>
		       		<table>
		       		<tr>
		       		<td>
		       		<%					
						if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
			  		</td>
			  		<td>
						<%					
						if(!((sv.crtabdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
									
											if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
											
											
									} else  {
												
									if(longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue( (sv.crtabdesc.getFormData()).toString()); 
											} else {
												formatValue = formatValue( longValue);
											}
									
									}
									%>			
								<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
										"blank_cell" : "output_cell" %>' style="min-width: 71px;margin-left: 1px;max-width:150px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>	
						<%
						longValue = null;
						formatValue = null;
						%>
		       	</td>
		       	</tr>
		       	</table>
		       		</div>
		       	</div>
		    </div>
		 
		    
		    
		<%
		GeneralTable sfl = fw.getTable("s6248screensfl");
	
		%>
	 <div class="row">		
		 		<div class="col-md-12">
		 		<div class="form-group"> 	
		          <div class="table-responsive">
		    	  <table  id='dataTables-s6248' class="table table-striped table-bordered table-hover"  width="100%" >
			    	 	<thead>
			    	 	<tr class='info'>									
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Reason")%></th>
		           		<th style="text-align: center;" width="100px;"><%=resourceBundleHandler.gettingValueFromBundle("Reassurance Indicator")%></th>									
						<th  style="text-align: center;" width="140px;"><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured%")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Age")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Load%")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjustment")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Premium_Adjusted")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Duration")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
						<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality")%></th>
						</tr>	
			         	</thead>
					      <tbody>
					      <%

		
								S6248screensfl.set1stScreenRow(sfl, appVars, sv);
								int count = 1;
								while (S6248screensfl.hasMoreScreenRows(sfl)) {
								
							%>
							
								<tr >
									<td align="left">														
										<%=sv.opcda.getFormData()%><br><%=sv.shortdesc.getFormData()%>
									</td>
									<td align="left">									
										<%= sv.reasind.getFormData()%>
									</td>
									<td align="right">									
							 			<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.znadjperc).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.S3VS2);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.znadjperc);
												if(!sv.
												znadjperc
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
									</td>
									<td align="left">									
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.agerate).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.agerate);
												if(!sv.
												agerate
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
									</td>
									<td align="right">									
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.oppc).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.oppc);
												if(!sv.
												oppc
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
									</td>
									<td align="right">									
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.insprm).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.insprm);
												if(!sv.
												insprm
												.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
									</td>
									<td align="right">									
										<%	
											sm = sfl.getCurrentScreenRow();
											qpsf = sm.getFieldXMLDef((sv.premadj).getFieldName());						
											qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);				
										%>
										
															
											<%
												formatValue = smartHF.getPicFormatted(qpsf,sv.premadj);
												if(!sv.premadj.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
													formatValue = formatValue( formatValue );
												}
											%>
											<%= formatValue%>
											<%
													longValue = null;
													formatValue = null;
											%>
									</td>
									<td align="left">									
												<%	
													sm = sfl.getCurrentScreenRow();
													qpsf = sm.getFieldXMLDef((sv.extCessTerm).getFieldName());						
													qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);				
												%>
												
																	
													<%
														formatValue = smartHF.getPicFormatted(qpsf,sv.extCessTerm);
														if(!sv.
														extCessTerm
														.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
															formatValue = formatValue( formatValue );
														}
													%>
													<%= formatValue%>
													<%
															longValue = null;
															formatValue = null;
													%>
									</td>
									<td align="left">									
										<%= sv.select.getFormData()%>
									</td>
									<td align="right">									
									
										
										<%= sv.zmortpct.getFormData()%>
										
									</td>
								</tr>
							
								<%
								count = count + 1;
								S6248screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>
					      </tbody>
					</table>
					
				</div>
			</div>
		</div>
		</div>
		
<div style='visibility:hidden;'>
			<div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("No")%></label>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Code")%></label>
		       		
		       		</div>
		       	</div>
		    </div>


			<div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Codes")%></label>
		       		
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Code")%></label>
		       		
		       		</div>
		       	</div>
		    </div>


			<div class="row">
	        	<div class="col-md-2">
		       		<div class="form-group">
		       		<label><%=resourceBundleHandler.gettingValueFromBundle("Codes")%></label>
		       		
		       		</div>
		       	</div>
		    </div>
</div>



<script>
	$(document).ready(function() {
		$('#dataTables-s6248').DataTable({
			ordering : false,
			searching : false,
			scrollY : "235px",
			scrollCollapse : true,
			scrollX : true,
			paging:   false,
			ordering: false,
	        info:     false,
	        searching: false,
	        orderable: false,
	        
		});
	});
</script> 

	 </div>
</div>



	



<%@ include file="/POLACommon2NEW.jsp"%>
