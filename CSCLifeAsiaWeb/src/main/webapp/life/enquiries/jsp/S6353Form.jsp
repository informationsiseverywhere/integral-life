
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S6353";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6353ScreenVars sv = (S6353ScreenVars) fw.getVariables();%>
<!-- MTL321-BEGIN -->
<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Extra Details ");%>
<!-- MTL321-END -->
<%{
if (appVars.ind10.isOn()) {
	sv.numpols.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind10.isOn()) {
	sv.polcomp.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind10.isOn()) {
	sv.polcomp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind30.isOn()) {
	sv.aiind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind30.isOn()) {
	sv.aiind.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILIFE-4108 starts
if (appVars.ind103.isOn()) {
	sv.indxflg.setEnabled(BaseScreenData.DISABLED);
}
//ILIFE-4108 ends
/*ILIFE-5327 starts*/
if (appVars.ind65.isOn()) {
	sv.zctaxind.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind64.isOn()) {
	sv.zctaxind.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind68.isOn()) {
	sv.schmno.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind67.isOn()) {
	sv.schmno.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind71.isOn()) {
	sv.schmnme.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind70.isOn()) {
	sv.schmnme.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind74.isOn()) {
	//sv.cownnum.setInvisibility(BaseScreenData.INVISIBLE);
	sv.cownnum.setInvisibility(BaseScreenData.VISIBLE);
}
if (appVars.ind73.isOn()) {
	//sv.cownnum.setEnabled(BaseScreenData.DISABLED);
	sv.cownnum.setEnabled(BaseScreenData.ENABLED);
}
if (appVars.ind75.isOn()) {
	sv.billday.setInvisibility(BaseScreenData.INVISIBLE);
}
/*ALS-700 by rsubramani42 starts*/
if (appVars.ind78.isOn()) {
		sv.zroloverind.setReverse(BaseScreenData.REVERSED);
		sv.zroloverind.setColor(BaseScreenData.RED);
	}
	if (appVars.ind77.isOn()) {
		sv.zroloverind.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind76.isOn()) {
		sv.zroloverind.setEnabled(BaseScreenData.DISABLED);
	}
	if (!appVars.ind78.isOn()) {
		sv.zroloverind.setHighLight(BaseScreenData.BOLD);
	}
	/*ALS-700 by rsubramani42 ends*/
	/*ILIFE-5327 Ends*/
	//ILIFE-6016 Starts	
if (appVars.ind102.isOn()) {
	sv.nlgflg.setReverse(BaseScreenData.REVERSED);
	sv.nlgflg.setColor(BaseScreenData.RED);
}
if (appVars.ind101.isOn()) {
	sv.nlgflg.setEnabled(BaseScreenData.DISABLED);
}
if (!appVars.ind102.isOn()) {
	sv.nlgflg.setHighLight(BaseScreenData.BOLD);
}
if (appVars.ind103.isOn()) {
	sv.indxflg.setEnabled(BaseScreenData.DISABLED);
}
if (appVars.ind105.isOn()) {
	sv.cntaccriskamt.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILIFE-6016 End

//ICIL-12 Start
if (appVars.ind106.isOn()) {
	sv.bnkout.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind107.isOn()) {
	sv.bnksm.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind108.isOn()) {
	sv.bnktel.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind35.isOn()) {
	sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE); 
} 
//ICIL-End

//ILIFE-7277 starts 
if (appVars.ind123.isOn()) {
        sv.zmultOwner.setReverse(BaseScreenData.REVERSED);
        sv.zmultOwner.setColor(BaseScreenData.RED);
}
if (appVars.ind123.isOn()) {
    sv.zmultOwner.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind123.isOn()) {
    sv.zmultOwner.setEnabled(BaseScreenData.DISABLED);
}
    
//ILIFE-7277 ends 
//ILJ-45 - START
if (appVars.ind124.isOn()) {
	sv.riskcommdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind125.isOn()) {
	sv.decldteDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind126.isOn()) {
	sv.fstprmrcptdteDisp.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILJ-45 - END
//ILJ-387
if (appVars.ind127.isOn()) {
	sv.jowner.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind128.isOn()) {
	sv.jlife.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind129.isOn()) {
	sv.reqntype.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind130.isOn()) {
	sv.indxflg.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind131.isOn()) {
	sv.lastinsdte.setInvisibility(BaseScreenData.INVISIBLE);
}
if (appVars.ind132.isOn()) {
	sv.instpramt.setInvisibility(BaseScreenData.INVISIBLE);
}
//ILJ-387 end

}%>

	<div class="panel panel-default" >
		
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4" >
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr><td>
								<%=smartHF.getHTMLVar(0, 0, fw, sv.chdrnum, true)%>
								</td><td style="
    			padding-left: 1px;
			">
			<%=smartHF.getHTMLVar(0, 0, fw, sv.cnttype, true)%>
				</td><td style="
			    padding-left: 1px;
			">
			<%=smartHF.getHTMLVar(0, 0, fw, sv.ctypedes, true)%>
							</td>
				
				
		</tr></table>
						</div>											
					</div>
					<div class="col-md-3 col-md-offset-2" >
					
						<div class="form-group"> 
						<%if ((new Byte((sv.schmno).getInvisible())).compareTo(new Byte(
						BaseScreenData.INVISIBLE)) != 0) { %>
							<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Scheme   "),0,0)%></label>
							<table><tr><td>
								<%=smartHF.getHTMLVar(0, 0, fw, sv.schmno, true)%>
								</td><td style="
    			padding-left: 1px;
			">
			<%=smartHF.getHTMLVar(0, 0, fw, sv.schmnme, true)%>
				</td>				
				<%} %>
		</tr></table>
						</div>											
					</div>
					</div>
					
			
			<div class="row">
			<div class="col-md-3" >
					
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>						
						<div class="input-group" style="width:120px;"> <%=smartHF.getHTMLVar(0, 0, fw, sv.chdrstatus, true)%><!-- ILJ-130 -->							
					</div></div>			
			<!-- <div class="col-md-1"></div> -->
				<div class="col-md-3" >
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<div class="input-group" style="width:120px;"> <%=smartHF.getHTMLVar(0, 0, fw, sv.premstatus, true)%>
					</div></div>
				</div>
				<!-- <div class="col-md-1"></div> -->
				<div class="col-md-2">
					<div class="form-group" style="max-width:140px;"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("cntcurr");
							longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
						%>
						 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
						   		<%if(longValue != null){%>
						   		
						   		<%=longValue%>
						   		
						   		<%}%>
						   </div>
						<%
							longValue = null;
							formatValue = null;
						%>  						
					</div>
				</div>	
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="form-group" style="max-width:140px;"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
						<div class="input-group">
						<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>		
							 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'>  
							   		<%if(longValue != null){%>
							   		
							   		<%=longValue%>
							   		
							   		<%}%>
							   </div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</div>  							
					</div>
				</div>											
			</div>
				<div class="row">
				<div class="col-md-3">
					<div class="form-group"> 
						<%if ((new Byte((sv.zctaxind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) == 0) { %>
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Owner     "))%></label>
<%}else{ %>
<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Member     "))%></label>
<%} %>
						<table><tr><td>
						<%=smartHF.getHTMLVar(0, 0, fw, sv.cownnum, true)%>
						</td><td style="
    padding-left: 1px;
">
<div style="max-width:400px;"> <%=smartHF.getHTMLVar(0, 0, fw, sv.ownername, true)%></div>
					</td></tr></table>
					</div>
				</div>						
			
			
			
			
				<div class="col-md-3 col-md-offset-3" style="max-width:300px;">
					<div class="form-group"> 
					<!-- ILJ-387 -->
					<% if ((new Byte((sv.jowner).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Owner")%></label>
							<table><tr><td style="min-width:80px">
							<%=smartHF.getHTMLVar(0, 0, fw, sv.jowner, true)%>
							</td><td style="
    padding-left: 1px;min-width:80px;
">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.jownername, true)%></div>
						</td></tr></table>
					<% } %>
					</div>
				</div>		
					</div>
					<div class="row">
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<table><tr><td>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%>
							</td><td style="
    padding-left: 1px;">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%></div>
</td></tr></table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-4" style="min-width:100px;">
					<div class="form-group"> 
					<!-- ILJ-387 -->
					<% if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table><tr><td style="min-width:80px">
							<%=smartHF.getHTMLVar(0, 0, fw, sv.jlife, true)%>
							</td><td style="min-width:80px;padding-left:1px;">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.jlifename, true)%></div>
						</td></tr></table>
					<% } %>
					</div>
				</div>											
		</div>
			
			<div class="row">
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Payor")%></label>
						<table><tr><td>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.payer, true)%>
							</td><td style="
    padding-left: 1px;">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.payername, true)%></div>
						</td></tr></table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-4">
					<div class="form-group"> 
					
						<%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Assignee"),0,0)%>
						<table><tr><td style="min-width:80px">
						<%=smartHF.getHTMLVar(0, 0, fw, sv.asgnnum, true)%>
						</td><td style="min-width:80px;padding-left:1px;">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.asgnname, true)%></div>
				</td></tr></table>
				</div>
			</div>	
			</div>
			
			<br>
			<%if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
<div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#basic_tab" data-toggle="tab"><b><%=resourceBundleHandler.gettingValueFromBundle("Basic Information")%></b></a>
                        </li>
                        <li>
                        	<a href="#dates_tab" data-toggle="tab"><b><%=resourceBundleHandler.gettingValueFromBundle("Scheduled Dates")%></b></a>
                        </li>
                      
                    </ul>
                    </div>
                    </div>
                   <%}else{%>
                   <div class="row">
				<div class="col-md-12">
                	<ul class="nav nav-tabs">
                    	<li class="active">
                        	<a href="#basic_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Basic Information")%></a>
                        </li>
                        <li>
                        	<a href="#dates_tab" data-toggle="tab"><%=resourceBundleHandler.gettingValueFromBundle("Scheduled Dates")%></a>
                        </li>
                       
                    </ul>
                  
                    </div>
                      
                    </div>
                    <%}%> 
					<div class="tab-content">
                    	<div class="tab-pane fade in active" id="basic_tab">
							<div class="row">
								<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Currency")%></label>
				<div class="input-group" >							<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"billcurr"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("billcurr");
											longValue = (String) mappedItems.get((sv.billcurr.getFormData()).toString().trim());  
										%>
								   		 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:120px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		formatValue = null;
		%>   									
									</div>
								</div></div>
								<div class="col-md-4">	
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Payment Method")%></label>
				
										<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("mop");
											longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());  
										%>
										<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="max-width:200px;">  
	   		<%if(longValue != null){%>
	   		 
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		formatValue = null;
		%>   
   																				
									
								</div></div>
								<div class="col-md-4">	
								<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Frequency")%></label>
			<div class="input-group" >								<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"payfreq"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("payfreq");
											longValue = (String) mappedItems.get((sv.payfreq.getFormData()).toString().trim());  
										%>
										<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="min-width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		formatValue = null;
		%>   																				
									</div>
								</div>		</div>						
							</div>
							<div class="row">
								<div class="col-md-4">
										<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Non-Forfeiture Option")%></label>
		<div class="input-group" >								<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"znfopt"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("znfopt");
											longValue = (String) mappedItems.get((sv.znfopt.getFormData()).toString().trim());  
										%>
											  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>'style="width:120px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		formatValue = null;
		%>   																			
									</div></div>
								</div>
						<!-- ILJ-387 -->
						<% if ((new Byte((sv.reqntype).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>	
								<div class="col-md-4">	
										<label><%=resourceBundleHandler.gettingValueFromBundle("Payout Method")%></label>
				<div class="form-group" style="max-width:100px;">						<%	
											fieldItem=appVars.loadF4FieldsLong(new String[] {"reqntype"},sv,"E",baseModel);
											mappedItems = (Map) fieldItem.get("reqntype");
											longValue = (String) mappedItems.get((sv.reqntype.getFormData()).toString().trim());  

										%>
										<%	if(longValue == null){
												longValue = "";
											}
										%>
											<input class="form-control" type="text" placeholder='<%=longValue%>' disabled>
									   	<%
									   		longValue = null;
											formatValue = null;
										%> 																										
									</div>		
								</div>
								<% } %>		
								<%	if ((new Byte((sv.billday).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
								<div class="col-md-4">
								<label><%=resourceBundleHandler.gettingValueFromBundle("Billing day")%></label>
				<%longValue=(sv.billday.getFormData()).toString().trim();
	 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:100px;" : "width:50px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>
								</div>
								<%} %>
								</div>	
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Agent")%></label>
										<table><tr><td>
											<%=smartHF.getHTMLVar(0, 0, fw, sv.servagnt, true)%></td><td style="padding-left:1px;">
<div style="max-width:400px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.servagnam, true)%></div>
</td></tr></table>
										
									</div>
								</div>
								
								<div class="col-md-4">
									
										<label><%=resourceBundleHandler.gettingValueFromBundle("No Lapse Guarantee")%></label>
						<div class="form-group" >
										<%	
	if ((new Byte((sv.nlgflg).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
						
	if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {
		longValue=resourceBundleHandler.gettingValueFromBundle("Yes");
	}
	if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("No");
	}
	if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("E")) {
	longValue=resourceBundleHandler.gettingValueFromBundle("Expired");
	}	 
%>

<% 
	if((new Byte((sv.nlgflg).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px !important;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {%>
	
<% if("red".equals((sv.nlgflg).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:100px;"> 
<%
} 
%>

<select name='nlgflg' style="width:100px; " 	
	onFocus='doFocus(this)'
	onHelp='return fieldHelp(nlgflg)'
	onKeyUp='return checkMaxLength(this)'
<% 
	if((new Byte((sv.nlgflg).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.nlgflg).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<option value="Y"<% if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("Y")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Yes")%></option>
<option value="N"<% if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("N")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("No")%></option>
<option value="E"<% if(((sv.nlgflg.getFormData()).toString()).trim().equalsIgnoreCase("E")) {%> Selected <% }%>><%=resourceBundleHandler.gettingValueFromBundle("Expired")%></option>
</select>
<% if("red".equals((sv.nlgflg).getColor())){
%>
</div>
<%
} 
%>
<%
}longValue = null;} 
%>
</div>
</div>
<!-- ILIFE-4108 -starts-->
<div class="col-md-4">
<!-- ILJ-387 -->
<% if ((new Byte((sv.indxflg).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>	

	<label>
	<%=resourceBundleHandler.gettingValueFromBundle("Auto. Increase")%>
	</label>

<br/>
<div class="form-group" style="max-width:100px;">	
<input type='checkbox' name='indxflg' value='Y' onFocus='doFocus(this)'
onHelp='return fieldHelp(indxflg)' onKeyUp='return checkMaxLength(this)'

<%
		if((sv.indxflg).getColor()!=null){
				%>style='background-color:#FF0000;'
		<%}

		if((sv.indxflg).toString().trim().equalsIgnoreCase("C")){
			%>checked 
			
      	<% }else if((sv.indxflg).toString().trim().equalsIgnoreCase("U")){    	  
      		%>unchecked<%
      	}
		if((sv.indxflg).getEnabled() == BaseScreenData.DISABLED || fw.getVariables().isScreenProtected()){
      	%>
    	   disabled

		<%}%>
class ='UICheck' onclick="handleCheckBox('indxflg')"/>
 </div>
<% } %>
</div>
</div>

<div class="row">
	<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
			<table><tr><td>
				<div class="input-group">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.servbr, true)%>
				</td><td style="padding-left:1px;">
				<%=smartHF.getHTMLVar(0, 0, fw, sv.brchname, true)%>
				</td></tr></table>
				</div>									
			</div>
		<!-- ICIL-12 start	 -->
		<%
		if ((new Byte((sv.bnkout).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>	
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Outlet")%></label>
			<table><tr>
			<td style="min-width: 71px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkout, true)%></td>
			<td style="padding-left:1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnkoutname, true)%></td>
			</tr></table>
			</div>
		</div>	
		<%} %>
		<%
		if ((new Byte((sv.bnksm).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>	
				<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label>
					<table>
						<tr>
							<td><input name='bnksm' id='bnksm' type='text'
								style="min-width: 150px;" value='<%=sv.bnksm%>'
								maxLength='<%=sv.bnksm.getLength()%>'
								size='<%=sv.bnksm.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bnksm)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control"></td>

							<td>
							<td>
								<%-- <label><%=resourceBundleHandler.gettingValueFromBundle("Bank Service Manager")%></label> --%>
								<input name='bnksmname' id='bnksmname' style="margin-left: 1px;"
								type='text' value='<%=sv.bnksmname%>'
								maxLength='<%=sv.bnksmname.getLength()%>'
								size='<%=sv.bnksmname.getLength()%>' onFocus='doFocus(this)'
								onHelp='return fieldHelp(bnksm)'
								onKeyUp='return checkMaxLength(this)' readonly="true"
								class="form-control">
							</td>
						</tr>

					</table>
				</div>

			</div>
		
		<%} %>	
	
		<!-- ICIL-12 end	 -->
			
	
</div>	
<!--</div>--><!-- ILIFE-6357-->
<!-- ILIFE-6293 S -->
<div class="row">	
	<!-- ICIL-12 start	 -->
	<%
		if ((new Byte((sv.bnktel).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
		%>
		<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("Bank Teller")%></label>
			<table><tr>
			<td style="min-width: 71px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktel, true)%></td>
			<td style="padding-left:1px;min-width: 100px;"><%=smartHF.getHTMLVar(0, 0, fw, sv.bnktelname, true)%></td>
			</tr></table>
			</div>
		</div>
		<%} %>
	<!-- ICIL-12 end	 -->

<!-- fwang3 China localization start -->
	<div class="col-md-4"> 
	 	<div class="form-group" style="width: 175px;">
	    	<label><%=resourceBundleHandler.gettingValueFromBundle("Refund_Overpay")%></label>
			<%
				mappedItems = new HashMap<String,String>();
				mappedItems.put("Y", resourceBundleHandler.gettingValueFromBundle("Yes"));
				mappedItems.put("N", resourceBundleHandler.gettingValueFromBundle("No"));
				longValue = (String) mappedItems.get(sv.refundOverpay.getFormData().trim());
			%>				    	
			<div class='output_cell' style="width: 100px;">
				<%=longValue==null?"":longValue%>
			</div>
			<%
				mappedItems = null;
				longValue=null;
			%>
	 	</div>
	</div>
	<!-- fwang3 China localization end --> 	
	
<%if ((new Byte((sv.fatcastatus).getInvisible())).compareTo(new Byte(
	    		BaseScreenData.INVISIBLE)) != 0) { %>

	<div class="col-md-4">
			<div class="form-group">
			<label><%=resourceBundleHandler.gettingValueFromBundle("FATCA Status")%></label>
				<div class="input-group">
				<table><tr><td>
				<input name='fatcastatus' id='fatcastatus'
				type='text'	
				value='<%=sv.fatcastatus.getFormData()%>'
				maxLength='<%=sv.fatcastatus.getLength()+50%>'
				size='<%=sv.fatcastatus.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(fatcastatus)' onKeyUp='return checkMaxLength(this)'
				style="width:55px;"
				readonly="true"  class="output_cell">
				</td><td style="padding-left:1px;">
				<input name='fatcastatusdesc' id='fatcastatusdesc'
				type='text'
				value='<%=sv.fatcastatusdesc.getFormData()%>'
				maxLength='<%=sv.fatcastatusdesc.getLength()+50%>'
				size='<%=sv.fatcastatusdesc.getLength()%>'
				onFocus='doFocus(this)' onHelp='return fieldHelp(fatcastatusdesc)' onKeyUp='return checkMaxLength(this)'
				style="width:180px;"
				readonly="true"  class="output_cell">
				</td>
				 </tr></table>
				 </div>
				</div>									
			</div>
	</div>	
<%} %>
</div>	
<!-- ILIFE-6293 E -->	
<!-- ILIFE-4108 -ends-->	
						<div class="tab-pane fade" id="dates_tab">
							<div class="row">
								<div class="col-md-4">
									
										<!-- ILJ-45 Starts -->
										<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
											<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Commencement Date")%></label>
										<%} else { %>
											<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
										<%} %>
										<!-- ILJ-45 End -->
										<div class="form-group" style="max-width:100px;"><%=smartHF.getRichText(0, 0, fw, sv.currfromDisp,(sv.currfromDisp.getLength()),null)%>
									</div>
								</div>
								<div class="col-md-4">
									
								<label><%=resourceBundleHandler.gettingValueFromBundle("Paid To Date")%></label>
					<div class="form-group" style="max-width:100px;">							<%=smartHF.getRichText(0, 0, fw, sv.ptdateDisp,(sv.ptdateDisp.getLength()),null)%>
									</div>
								</div>
								<div class="col-md-4">
								
										<label><%=resourceBundleHandler.gettingValueFromBundle("Billed To Date")%></label>
				<div class="form-group" style="max-width:100px;">						<%=smartHF.getRichText(0, 0, fw, sv.btdateDisp,(sv.btdateDisp.getLength()),null)%>
									</div>
								</div>																
							</div>
							<!-- ILJ-45 Starts -->							
							<div class="row">
								<div class="col-md-4">
								<% if ((new Byte((sv.riskcommdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Commencement Date"))%></label>
									<div class="form-group" style="max-width:100px;">
										<%=smartHF.getRichText(0, 0, fw, sv.riskcommdteDisp,(sv.riskcommdteDisp.getLength()),null)%>
									</div>
								<% } %>
								</div>
								<div class="col-md-4">
								<% if ((new Byte((sv.decldteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Declaration Date")%></label>
									<div class="form-group" style="max-width:100px;">
										<%=smartHF.getRichText(0, 0, fw, sv.decldteDisp,(sv.decldteDisp.getLength()),null)%>
									</div>
								<% } %>
								</div>
								<div class="col-md-4">
								<% if ((new Byte((sv.fstprmrcptdteDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								<label><%=resourceBundleHandler.gettingValueFromBundle("First Premium Receipt Date")%></label>
									<div class="form-group" style="max-width:100px;">
										<%=smartHF.getRichText(0, 0, fw, sv.fstprmrcptdteDisp,(sv.fstprmrcptdteDisp.getLength()),null)%>
									</div>
								<% } %>
								</div>																
							</div>						
							<!-- ILJ-45 End -->
							
							<div class="row">
								<div class="col-md-4">
									
										<label><%=resourceBundleHandler.gettingValueFromBundle("1st Issue Date")%></label>
							<div class="form-group" style="max-width:100px;">			<%=smartHF.getRichText(0, 0, fw, sv.hoissdteDisp,(sv.hoissdteDisp.getLength()),null)%>
									</div>
								</div>
								<div class="col-md-4">
								
										<label><%=resourceBundleHandler.gettingValueFromBundle("UW Decision Date")%></label>
		<div class="form-group" style="max-width:100px;">								<%=smartHF.getRichText(0, 0, fw, sv.huwdcdteDisp,(sv.huwdcdteDisp.getLength()),null)%>
									</div>
								</div>
								<div class="col-md-4">
								
										<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Issue Date")%></label>
			<div class="form-group" style="max-width:100px;"><%=smartHF.getRichText(0, 0, fw, sv.hissdteDisp,(sv.hissdteDisp.getLength()),null)%>
	<%=smartHF.getHTMLCalNSVar(0, 0, fw, sv.hissdteDisp).replace("absolute","relative")%>
									</div>
								</div>																
							</div>	
							
							<div class="row">
								<div class="col-md-4">
								
										<label><%=resourceBundleHandler.gettingValueFromBundle("Proposal Date")%></label>
			<div class="form-group" style="max-width:100px;">							<%=smartHF.getRichText(0, 0, fw, sv.hpropdteDisp,(sv.hpropdteDisp.getLength()),null)%>
									</div>
								</div>
								<div class="col-md-4">
									
										<label><%=resourceBundleHandler.gettingValueFromBundle("Proposal Received Date")%></label>
		<div class="form-group" style="max-width:100px;">								<%=smartHF.getRichText(0, 0, fw, sv.hprrcvdtDisp,(sv.hprrcvdtDisp.getLength()),null)%>
									</div>
								</div>															
							</div>	
							
							<div class="row">
							<div class="col-md-4">
							<!-- ILJ-387 -->
								<% if ((new Byte((sv.lastinsdte).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								
										<label><%=resourceBundleHandler.gettingValueFromBundle("Last Installment Date")%></label>
		<div class="form-group" style="max-width:100px;">								<%=smartHF.getRichText(0, 0, fw, sv.lastinsdteDisp,(sv.lastinsdteDisp.getLength()),null)%>
									</div>
									<% } %>
								</div>
						
								<div class="col-md-4">
									
										<label><%=resourceBundleHandler.gettingValueFromBundle("Next Bill Date")%></label>
		<div class="form-group" style="max-width:100px;"><%=smartHF.getRichText(0, 0, fw, sv.nextinsdteDisp,(sv.nextinsdteDisp.getLength()),null)%>
									</div>
								</div>															
							</div>
							
							<div class="row">
								<div class="col-md-4">
								<!-- ILJ-387 -->
									<% if ((new Byte((sv.instpramt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
										<label><%=resourceBundleHandler.gettingValueFromBundle("Last Installment Premium")%></label>
		<div class="form-group" style="min-width:100px;max-width: 150px;text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null,sv.instpramt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>			
									</div>
									<% } %>
								</div>
								<div class="col-md-4">
							
										<label><%=resourceBundleHandler.gettingValueFromBundle("Installment Premium")%></label>
		<div class="form-group" style="min-width:100px;max-width:167px;text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null, sv.nextinsamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
										
									</div>
								</div>	
								<div class="col-md-4">
							
										<label><%=resourceBundleHandler.gettingValueFromBundle("Billing Premium")%></label>
		<div class="form-group" style="max-width:167px;min-width: 100px;text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null, sv.cbillamt, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
										
									</div>
								</div>															
							</div>	
							<!-- ILIFE-6357 -->
						<!-- 	</div>
							
								
						
							<div class="tab-pane fade" id="prem_tab"> --> <!-- ILIFE-6357 -->
							<%if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
							<div class="row">
								<div class="col-md-4">
									
											<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
											
				<div class="form-group" style="max-width:145px;min-width: 100px;text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null,sv.instPrem, true,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS)%>
										</div>
									</div> 

									
									<%}%> 
								
								
								
								<%if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
									<div class="col-md-4">
											<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
											
			<div class="form-group" style="max-width:100px;text-align: right;">								<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
												<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null)%>
											<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
												<%if(sv.zstpduty01.equals(0)) {%>
													<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>
												<%} else { %>
													<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)%>	
												<%} %>
											<%}else {%>
												<input class="form-control" type="text" placeholder='' disabled>
											<%}%>
										</div>
									</div>
								<%}%>
								<!-- ILIFE-7062 : Start-->
								<%
				 					if ((new Byte((sv.cntfee).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
								<div class="col-md-4">							
										<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Fee")%></label>
		<div class="form-group" style="max-width:167px;min-width: 100px;text-align: right;"><%=smartHF.getHTMLVar(0, 0, fw, null, sv.cntfee, true,COBOLHTMLFormatter.S1ZEROVS2)%>										
									</div>
								</div>	
								<%}%>
								<!-- ILIFE-7062 : End-->															
							</div>																																
						</div>
						</div>
						</div><!-- ILIFE-6357-->
						
						</div>
						</div>
						
						

					                  
                
        
<Div id='mainForm_OPTS' style='visibility:hidden'>
<%=smartHF.getMenuLink(sv.claimsind, resourceBundleHandler.gettingValueFromBundle("Claims Enquiry"))%>
<%=smartHF.getMenuLink(sv.zmultOwner, resourceBundleHandler.gettingValueFromBundle("Multiple Owners"))%><!-- //ILIFE-7277  -->
<%=smartHF.getMenuLink(sv.plancomp, resourceBundleHandler.gettingValueFromBundle("Plan Components"))%>
<%=smartHF.getMenuLink(sv.polcomp, resourceBundleHandler.gettingValueFromBundle("Policy Components"))%>
<%=smartHF.getMenuLink(sv.cltrole, resourceBundleHandler.gettingValueFromBundle("Roles"))%>
<%=smartHF.getMenuLink(sv.subacbal, resourceBundleHandler.gettingValueFromBundle("Sub A/C Balances"))%>
<%=smartHF.getMenuLink(sv.transhist, resourceBundleHandler.gettingValueFromBundle("Transaction History"))%>
<%=smartHF.getMenuLink(sv.agntsdets, resourceBundleHandler.gettingValueFromBundle("Agents Details"))%>
<%=smartHF.getMenuLink(sv.extradets, resourceBundleHandler.gettingValueFromBundle("Extra Details"))%>
<%=smartHF.getMenuLink(sv.aiind, resourceBundleHandler.gettingValueFromBundle("Anniversary"))%>	
<%=smartHF.getMenuLink(sv.fupflg, resourceBundleHandler.gettingValueFromBundle("Follow Ups"))%>
<%=smartHF.getMenuLink(sv.ind, resourceBundleHandler.gettingValueFromBundle("Policy Notes"))%>
<%=smartHF.getMenuLink(sv.zsredtrm, resourceBundleHandler.gettingValueFromBundle("Reducing Term"))%>
<%=smartHF.getMenuLink(sv.outind, resourceBundleHandler.gettingValueFromBundle("Output"))%>
<%=smartHF.getMenuLink(sv.comphist, resourceBundleHandler.gettingValueFromBundle("Component History"))%>
<%if ((new Byte((sv.zctaxind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
<%=smartHF.getMenuLink(sv.zctaxind, resourceBundleHandler.gettingValueFromBundle("Contribution Details"))%>
<%} %>

<%=smartHF.getMenuLink(sv.zroloverind, resourceBundleHandler.gettingValueFromBundle("Rollover Details"))%>
<%=smartHF.getMenuLink(sv.znlghist, resourceBundleHandler.gettingValueFromBundle("NLG History"))%>
<%if ((new Byte((sv.cntaccriskamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
<%=smartHF.getMenuLink(sv.cntaccriskamt, resourceBundleHandler.gettingValueFromBundle("Client Accumulated Risk Amount"))%><!-- ICIL-82 -->
<%} %>


<%if(sv.action.compareTo("Y") == 0){%>
<%=smartHF.getMenuLink(sv.debcrdhistry, resourceBundleHandler.gettingValueFromBundle("Debit and Credit History"))%>
<%} %>
</div>

<script type="text/javascript">
$(document).ready(function() {	
	$("#bnkout").width('148px');
	if($("#bnkout").text().replace(/(^\s+|\s+$)/g, "").length> 10 
			&& $("#bnkout").valuereplace(/(^\s+|\s+$)/g, "").length <= 20){
		$("#bnkout").css('min-width','170px');
		
	} else if($("#bnkout").text().replace(/(^\s+|\s+$)/g, "").length > 8 && $("#bnkout").text().replace(/(^\s+|\s+$)/g, "").length <= 10 ){
		$("#bnkout").css('min-width','100px');
	}
	$("#bnksm").width('150px');
	if($("#bnksm").text().replace(/(^\s+|\s+$)/g, "").length> 10 
			&& $("#bnksm").valuereplace(/(^\s+|\s+$)/g, "").length <= 20){
		$("#bnksm").css('min-width','150px');
	} else if($("#bnksm").text().replace(/(^\s+|\s+$)/g, "").length > 8 && $("#bnksm").text().replace(/(^\s+|\s+$)/g, "").length <= 10 ){
		$("#bnksm").css('min-width','150px');
	}
	$("#bnktel").width('130px');
	if($("#bnktel").text().replace(/(^\s+|\s+$)/g, "").length> 10 
			&& $("#bnktel").valuereplace(/(^\s+|\s+$)/g, "").length <= 20){
		$("#bnktel").css('min-width','165px');
	} else if($("#bnktel").text().replace(/(^\s+|\s+$)/g, "").length > 8 && $("#bnktel").text().replace(/(^\s+|\s+$)/g, "").length <= 10 ){
		$("#bnktel").css('min-width','130px');
	}
});

</script>

<%@ include file="/POLACommon2NEW.jsp"%>