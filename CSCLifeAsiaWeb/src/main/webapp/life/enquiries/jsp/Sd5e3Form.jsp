<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "SD5E3";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%Sd5e3ScreenVars sv = (Sd5e3ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>

<%		appVars.rollup(new int[] {93});
%>
<%{
	if (appVars.ind02.isOn()) {
		sv.survalind.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind04.isOn()) {
		sv.loanvalind.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind06.isOn()) {
		sv.bonusvalind.setInvisibility(BaseScreenData.INVISIBLE);
	}
}%>
<div class="panel panel-default">


	<div class="panel-body">
	
	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Owner")%></label>
					<table>
						<tr>
							<td>
								<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cownnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							<td>
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ownername.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							
						</tr>
					</table>

				</div>
			</div>
			
					
		</div>
	
		<div class="row">
				<div class="col-md-2">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
						<table><tr><td>
							<%=smartHF.getHTMLVar(0, 0, fw, sv.lifenum, true)%>
							</td><td style="
    padding-left: 1px;">
<%=smartHF.getHTMLVar(0, 0, fw, sv.lifename, true)%>
</td></tr></table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-2" style="min-width:100px;">
					<div class="form-group"> 
						<label><%=resourceBundleHandler.gettingValueFromBundle("Joint Life")%></label>
						<table><tr><td style="min-width:80px">
							<%=smartHF.getHTMLVar(0, 0, fw, sv.jlife, true)%>
							</td><td style="min-width:80px;padding-left:1px;">
<%=smartHF.getHTMLVar(0, 0, fw, sv.jlifename, true)%>
						</td></tr></table>
					</div>
				</div>											
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
					<table>
						<tr>
							<td>
								<%					
								if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							<td>
								<%					
								if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
									style="margin-left: 1px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
							<td>
								<%					
								if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>
								<div
									class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>'
									style="margin-left: 1px; max-width: 250px;">
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
								longValue = null;
								formatValue = null;
								%>
							</td>
						</tr>
					</table>

				</div>
			</div>

			<div class="col-md-4"></div>


			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
				<div class="form-group" style='max-width: 100px !important;min-width:100 !important'>
					<%	
							fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
							mappedItems = (Map) fieldItem.get("register");
							longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
						%>
					<input class="form-control" type="text"
						placeholder='<%=longValue%>' disabled>
					<%
							longValue = null;
							formatValue = null;
						%>

				</div>
			</div>



		</div>

		<div class="row">
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
				<div class="form-group" style='max-width: 100px !important;min-width:100 !important'> 
				 
					<input class="form-control" type="text" placeholder='<%=sv.chdrstatus%>' disabled>
				
					 </div>
			</div>
			
			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
				<div class="input-group" style='max-width: 120px !important;min-width:120 !important'> 
					<input class="form-control" type="text" placeholder='<%=sv.premstatus%>' disabled>
				</div>
			</div>

			<div class="col-md-4">
				<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
				<div class="form-group" style='max-width: 100px !important;min-width:100 !important'> 				
					<input class="form-control" type="text" placeholder='<%=sv.cntcurr%>' disabled>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-sd5e3" width="100%"
						style="border-right: thin solid #dddddd !important;">
						<thead>
							<tr class='info'>
								<th><center>
										<%=resourceBundleHandler.gettingValueFromBundle("Component")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Premium")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Sum Assured")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Risk Status")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Prem Status")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Fund Value")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Surrender Value")%></center></th>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Loan Value")%></center></th>
								<%if ((new Byte((sv.bonusvalind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
								) {%>
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Bonus Value")%></center></th>
								<%} %>
								
								<th style='max-width: 70px !important;min-width:70px !important'><center><%=resourceBundleHandler.gettingValueFromBundle("Component Issue Date")%></center></th>
								
								<th><center><%=resourceBundleHandler.gettingValueFromBundle("Suspense Amount")%></center></th>
							</tr>
						</thead>
						<tbody>
							<%
                            	GeneralTable sfl = fw.getTable("sd5e3screensfl");
                            	Sd5e3screensfl.set1stScreenRow(sfl, appVars, sv);
                            	int count = 1;
                            	while (Sd5e3screensfl.hasMoreScreenRows(sfl)) {
								%>
							<tr>

								<td><b><%=sv.component.getFormData()%></b></td>
								<% 
				    				sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.instPrem).getFieldName());										
									formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.instPrem.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
							%>
								<td align="right"><b><%=formatValue%></b></td>
								<% longValue = null;
									formatValue = null;%>

								<% 
				    				sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.sumin).getFieldName());										
									formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.sumin.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
									
				    				
							%>
								<td align="right"><b><%=formatValue%></b></td>
								<% longValue = null;
									formatValue = null;%>
								<td><b><%=sv.statcode.getFormData()%></b></td>
								<td><b><%=sv.pstatcode.getFormData()%></b></td>
								<% 
				    				
				    				sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.fundamnt).getFieldName());										
									formatValue = smartHF.getPicFormatted(qpsf,sv.fundamnt,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.fundamnt.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
				    				%>
								<td align="right"><b><%=formatValue%></b></td>
								<% longValue = null;
									formatValue = null;%>
								<% 
									
									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.surrval).getFieldName());										
									formatValue = smartHF.getPicFormatted(qpsf,sv.surrval,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.surrval.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
										formatValue = formatValue( formatValue );
									}
									
									%>
								<td align="right"><b><%=formatValue%></b></td>
								<% longValue = null;
									formatValue = null;%>
								<% 
										
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.loanVal).getFieldName());										
										formatValue = smartHF.getPicFormatted(qpsf,sv.loanVal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!sv.loanVal.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}																				
				    				%>
								<td align="right"><b><%=formatValue%></b></td>

								<% longValue = null;
									formatValue = null;%>
								<%

									sm = sfl.getCurrentScreenRow();
									qpsf = sm.getFieldXMLDef((sv.bounsVal).getFieldName());
									formatValue = smartHF.getPicFormatted(qpsf,sv.bounsVal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
									if(!sv.bounsVal.getFormData().toString().trim().equalsIgnoreCase("")) {
										formatValue = formatValue( formatValue );
									}
								%>
								<%if ((new Byte((sv.bonusvalind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
								) {%>
								<td align="right"><b><%=formatValue%></b></td>
								<% }%>
								<% longValue = null;
									formatValue = null;%>

								<td><b><%=sv.hissdteDisp.getFormData()%></b></td>
								<% 
										
										sm = sfl.getCurrentScreenRow();
										qpsf = sm.getFieldXMLDef((sv.sacscurbal).getFieldName());										
										formatValue = smartHF.getPicFormatted(qpsf,sv.sacscurbal,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
										if(!sv.sacscurbal.getFormData().toString().trim().equalsIgnoreCase("")) {								 		
											formatValue = formatValue( formatValue );
										}																				
				    				%>
								<td align="right"><b><%=formatValue%></b></td>
								<% longValue = null;
									formatValue = null;%>
							</tr>
							<%
								count = count + 1;
								Sd5e3screensfl.setNextScreenRow(sfl, appVars, sv);
								}
								%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<%if ((new Byte((sv.survalind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
		&& (new Byte((sv.loanvalind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
		&& (new Byte((sv.bonusvalind).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0
) {%>
<div class="row">
	<div class="col-md-4">
		<input name='survalind' id='survalind' type='hidden'  value="<%=sv.survalind.getFormData()%>">
		<%
			qpsf = fw.getFieldXMLDef((sv.totalSurVal).getFieldName());
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalSurVal, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			if(!formatValue.equalsIgnoreCase("")) {
		%>
		<a href="javascript:;" onClick='hyperLinkTo(document.getElementById("survalind"))' class="hyperLink">
			<%=resourceBundleHandler.gettingValueFromBundle("Total Surrender Value")%>
		</a>
		<%}else{%>
			<%=resourceBundleHandler.gettingValueFromBundle("Total Surrender Value")%>
		<%}%>

		<div class="form-group">
			<div class="input-group" style="min-width: 130px;" align="right">
				<%=smartHF.getHTMLVar(0, 0, fw, null,sv.totalSurVal, true,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<input name='loanvalind' id='loanvalind' type='hidden'  value="<%=sv.loanvalind.getFormData()%>">
		<%
			qpsf = fw.getFieldXMLDef((sv.totalLoanVal).getFieldName());
			formatValue = smartHF.getPicFormatted(qpsf,sv.totalLoanVal, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			if(!formatValue.equalsIgnoreCase("")) {
		%>
		<a href="javascript:;" onClick='hyperLinkTo(document.getElementById("loanvalind"))' class="hyperLink">
			<%=resourceBundleHandler.gettingValueFromBundle("Total Loan Value")%>
		</a>
		<%}else{%>
			<%=resourceBundleHandler.gettingValueFromBundle("Total Loan Value")%>
		<%}%>
		<div class="form-group">
			<div class="input-group" style="min-width: 130px;" align="right">
				<%=smartHF.getHTMLVar(0, 0, fw, null,sv.totalLoanVal, true,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<input name='bonusvalind' id='bonusvalind' type='hidden'  value="<%=sv.bonusvalind.getFormData()%>">
		<%
			qpsf = fw.getFieldXMLDef((sv.totBonusVal).getFieldName());
			formatValue = smartHF.getPicFormatted(qpsf,sv.totBonusVal, COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			if(!formatValue.equalsIgnoreCase("")) {
		%>

		<a href="javascript:;" onClick='hyperLinkTo(document.getElementById("bonusvalind"))' class="hyperLink">
			<%=resourceBundleHandler.gettingValueFromBundle("Total Bonus Value")%>
		</a>
		<%}else{%>
			<%=resourceBundleHandler.gettingValueFromBundle("Total Bonus Value")%>
		<%}%>
		<div class="form-group">
			<div class="input-group" style="min-width: 130px;" align="right">
				<%=smartHF.getHTMLVar(0, 0, fw, null,sv.totBonusVal, true,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS)%>
			</div>
		</div>
	</div>
</div>
<%} %>

<script>
$(document).ready(function() {
	$('#dataTables-sd5e3').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '301px',
        scrollCollapse: true,
        paging:   false,		
        info:     false,       
        orderable: false
  	});
})
</script>

<%@ include file="/POLACommon2NEW.jsp"%>

