<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%String screenName = "Sd5e1";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%Sd5e1ScreenVars sv = (Sd5e1ScreenVars) fw.getVariables();%>
<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"A - Financial SnapShot-Contract");%>
<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"B - Financial SnapShot-Client");%>
<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Action               ");%>
<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract No          ");%>
<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Client No            ");%>


<%{
		if (appVars.ind01.isOn()) {
			sv.action.setReverse(BaseScreenData.REVERSED);
			sv.action.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.action.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind02.isOn()) {
			sv.chdrsel.setReverse(BaseScreenData.REVERSED);
			sv.chdrsel.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind02.isOn()) {
			sv.chdrsel.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.clttwo.setReverse(BaseScreenData.REVERSED);
			sv.clttwo.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind03.isOn()) {
			sv.clttwo.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

<div class="panel panel-default">
	<div class="panel-heading">Input</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<%} %>
					<%
                                         if ((new Byte((sv.chdrsel).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.chdrsel)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.chdrsel)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('chdrsel')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
					
				</div>
			</div>

			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-group">
					<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<label><%=resourceBundleHandler.gettingValueFromBundle("Client No")%></label>
					<%} %>
					<%
                                         if ((new Byte((sv.clttwo).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
                                                       || fw.getVariables().isScreenProtected()) {
                                  %>
                           
                                  <div class="input-group">
                                                <%=smartHF.getHTMLVarExt(fw, sv.clttwo)%>
                                                       
                                         
                                  </div>
                                  <%
                                         } else {
                                  %>
                                  <div class="input-group">
                                         <%=smartHF.getRichTextInputFieldLookup(fw, sv.clttwo)%>
                                         <span class="input-group-btn">
                                                <button class="btn btn-info"
                                                       style="font-size: 13px; border-bottom-width: 0px !important; right: -3px !important;"
                                                       type="button"
                                                       onClick="doFocus(document.getElementById('clttwo')); doAction('PFKEY04')">
                                                       <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </button>
                                         </span>
                                  </div>
                                  <%
                                         }
                                  %>
				
				</div>
			</div>
		</div>

		<hr>

	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Actions</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<label class="radio-inline" style="white-space: nowrap;"><b> <%=smartHF.buildRadioOption(sv.action, "action", "A")%><%=resourceBundleHandler.gettingValueFromBundle("Financial SnapShot-Contract")%></b>
				</label>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<label class="radio-inline" style="white-space: nowrap;"> <b> <%=smartHF.buildRadioOption(sv.action, "action", "B")%><%=resourceBundleHandler.gettingValueFromBundle("Financial SnapShot-Client")%></b>
				</label>
			</div>
			


		</div>
	</div>
</div>



<%@ include file="/POLACommon2NEW.jsp"%>
