<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String screenName = "S6685";
%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*"%>
<%
	S6685ScreenVars sv = (S6685ScreenVars) fw.getVariables();
%>
<%
	StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class, " ");
%>




<%
	StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Contract No   ");
%>
<%
	StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class, "1 - Select");
%>
<%
	StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Act Life Cov Rider  Component Details");
%>
<%
	appVars.rollup(new int[]{93});
%>

<!-- <style>
.panel{
height: 500px !important;
}
</style> -->

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract No")%></label>
					<table>
						<tr>
							<td>
								<%
									if (!((sv.chdrsel.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrsel.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.chdrsel.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.cnttype.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="margin-left: 1px;max-width: 200px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
							<td>
								<%
									if (!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									} else {

										if (longValue == null || longValue.equalsIgnoreCase("")) {
											formatValue = formatValue((sv.ctypedes.getFormData()).toString());
										} else {
											formatValue = formatValue(longValue);
										}

									}
								%>
								<div style="margin-left: 1px;"
									class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
									<%=XSSFilter.escapeHtml(formatValue)%>
								</div> <%
 	longValue = null;
 	formatValue = null;
 %>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id="dataTables-s6685" width="100%">
							<thead>
								<tr class='info'>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Life")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Component")%></th>
									<th style="text-align: center;"><%=resourceBundleHandler.gettingValueFromBundle("Details")%></th>
								</tr>
							</thead>
							<tbody>
								<%
									GeneralTable sfl = fw.getTable("s6685screensfl");
									S6685screensfl.set1stScreenRow(sfl, appVars, sv);
									int count = 1;
									while (S6685screensfl.hasMoreScreenRows(sfl)) {
										{
											if (appVars.ind02.isOn()) {
												sv.action.setReverse(BaseScreenData.REVERSED);
											}
											if (appVars.ind04.isOn()) {
												sv.action.setEnabled(BaseScreenData.DISABLED);
											}
											if (appVars.ind03.isOn()) {
												sv.action.setInvisibility(BaseScreenData.INVISIBLE);
											}
											if (appVars.ind02.isOn()) {
												sv.action.setColor(BaseScreenData.RED);
											}
											if (!appVars.ind02.isOn()) {
												sv.action.setHighLight(BaseScreenData.BOLD);
											}
											if (appVars.ind05.isOn()) {
												sv.life.setInvisibility(BaseScreenData.INVISIBLE);
											}
											if (appVars.ind06.isOn()) {
												sv.coverage.setInvisibility(BaseScreenData.INVISIBLE);
											}
											if (appVars.ind07.isOn()) {
												sv.rider.setInvisibility(BaseScreenData.INVISIBLE);
											}
										}
								%>
								<tr id='<%="tablerow" + count%>'>
									<div style='display: none; visiblity: hidden;'>
										<input type='text' 
											maxLength='<%=sv.action.getLength()%>'
											 value='<%= sv.action.getFormData() %>' 
											 size='<%=sv.action.getLength()%>'
											 onFocus='doFocus(this)' onHelp='return fieldHelp(s6685screensfl.action)' onKeyUp='return checkMaxLength(this)' 
											 name='<%="s6685screensfl" + "." +
											 "action" + "_R" + count %>'
											  id='<%="s6685screensfl" + "." +
										 		"action" + "_R" + count %>'
											 class = "input_cell"
											  style = "width: <%=sv.action.getLength()*12%> px;"
										  
						 								 >
									</div>
									<td>
									<%
									if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
										<%=sv.life.getFormData()%>
									<%} %>
									</td>
									<td>
									<%
									if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
										<%=sv.coverage.getFormData()%>
									<%} %>
									</td>
									<td>
									<%
									if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {%>
										<%=sv.rider.getFormData()%>
									<%} %>
									
									</td>
									<td>
										<%
										if(sv.hrgpynum.getFormData().trim().length()==0){
										%>	
											<%=sv.shortdesc.getFormData()%>
				
										<%}else{%>					
												<a href="javascript:;" class = 'tableLink' onClick='document.getElementById("<%="s6685screensfl" + "." +
									      "action" + "_R" + count %>").value="1"; doAction("PFKEY0");'><span><%=sv.shortdesc.getFormData()%></span></a>							 						 		
										<%}%>
									</td>
									<td><%=sv.elemdesc.getFormData()%></td>
								</tr>
								<%
									count = count + 1;
										S6685screensfl.setNextScreenRow(sfl, appVars, sv);
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$('#dataTables-s6685').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        paging: false,
        info: false
  	});
})
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("#ctypedes").removeClass("input-group-addon").addClass("form-control");
 }
);

</script>
<%@ include file="/POLACommon2NEW.jsp"%>

