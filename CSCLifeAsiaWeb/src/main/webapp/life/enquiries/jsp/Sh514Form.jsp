<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SH514";%>
<%@ include file="/POLACommon1NEW.jsp"%> 
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sh514ScreenVars sv = (Sh514ScreenVars) fw.getVariables();%>

<%StringData generatedText1 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Number of Policies in Plan ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Policy Number ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Assured    ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint Life      ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life Number     ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage No ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider No ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Lien Code   ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Stat. Fund  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Section  ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum Assured         ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sub-section ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single Premium      ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Cessation Date   ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk Cessation Date      ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Total Premium       ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage RCD    ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann Proc Date   ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Last Bonus Date          ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ind    ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate Date     ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate from Date         ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bonus Appl.Method ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dividend Option ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"MOP ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Pymt Curr ");%>
	<%StringData generatedText34 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Payor           ");%>
	<%StringData generatedText35 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Bank/Branch     ");%>
	<%StringData generatedText36 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Account ");%>
	<%StringData generatedText40 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"/");%>
	<%StringData generatedText39 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Dividend Details ");%>
	<%StringData generatedText38 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"PUA Details ");%>
	<%StringData generatedText37 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Special Terms Ind ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Breakdown ");%>
	<%StringData generatedText41 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Age Last Birth");%>

<%
	StringData generatedText42 = resourceBundleHandler.gettingValueFromBundle(StringData.class,
			"Total Premium w/ Tax");
%>

<%{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			sv.sumins.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			generatedText24.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.unitStatementDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			generatedText25.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind34.isOn()) {
			sv.bonusInd.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind80.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		
		if (appVars.ind39.isOn()) {
			sv.taxamt.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind01.isOn()) {
			sv.optextind.setReverse(BaseScreenData.REVERSED);
			sv.optextind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.optextind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind36.isOn()) {
			generatedText29.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind35.isOn()) {
			sv.pbind.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind36.isOn()) {
			sv.pbind.setInvisibility(BaseScreenData.INVISIBLE);
			sv.pbind.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind35.isOn()) {
			sv.pbind.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind35.isOn()) {
			sv.pbind.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind32.isOn()) {
			sv.puaInd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind37.isOn()) {
			sv.puaInd.setInvisibility(BaseScreenData.INVISIBLE);
			sv.puaInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind32.isOn()) {
			sv.puaInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind32.isOn()) {
			sv.puaInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind37.isOn()) {
			generatedText38.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind30.isOn()) {
			sv.divdInd.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind31.isOn()) {
			sv.divdInd.setInvisibility(BaseScreenData.INVISIBLE);
			sv.divdInd.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.divdInd.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.divdInd.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind31.isOn()) {
			generatedText39.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 START */
		if (appVars.ind32.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind32.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		/*BRD-306 END */
		//ILIFE-3399-STARTS
		if (appVars.ind53.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind52.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind54.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-ENDS
         	if (appVars.ind55.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
         	//BRD-NBP-011 starts
    		if (appVars.ind72.isOn()) {
    			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
    			sv.dialdownoption.setColor(BaseScreenData.RED);
    		}
    		if (!appVars.ind72.isOn()) {
    			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
    		}
    		if (appVars.ind70.isOn()) {
    			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
    		}
    		if (appVars.ind71.isOn()) {
    			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
    		}
    		//BRD-NBP-011 ends
    		if (appVars.ind69.isOn()) {
    			sv.aepaydet.setHighLight(BaseScreenData.BOLD);	
			sv.aepaydet.setInvisibility(BaseScreenData.INVISIBLE);
			sv.aepaydet.setEnabled(BaseScreenData.DISABLED);
		    }
    		//ILJ-45 - START
    		if (appVars.ind77.isOn()) {
    			sv.crrcdDisp.setInvisibility(BaseScreenData.INVISIBLE);
    		}
    		/* if (appVars.ind78.isOn()) {
    			sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
    		} */
    		if (appVars.ind79.isOn()) {
    			sv.riskCessDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
    		}
    		//ILJ-45 - END

	}

	%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">

					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>

					<table>
					<tr>
					<td>

						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.chdrnum.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>
						</td>
						<td>
						<%
							if (!((sv.ctypedes.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypedes.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="max-width: 300px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Contract Status")%></label>


					<%
						if (!((sv.chdrstatus.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.chdrstatus.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 110px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Premium Status")%></label>

					<%
						if (!((sv.premstatus.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.premstatus.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 110px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

					<%
						longValue = null;
						formatValue = null;
					%>


					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "cntcurr" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("cntcurr");
						longValue = (String) mappedItems.get((sv.cntcurr.getFormData())
								.toString().trim());
					%>

					<%
						if (!((sv.mortcls.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.cntcurr.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.cntcurr.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 140px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>

					<%
						fieldItem = appVars.loadF4FieldsLong(new String[] { "register" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("register");
						optionValue = makeDropDownList(mappedItems,
								sv.register.getFormData(), 1, resourceBundleHandler);
						longValue = (String) mappedItems.get((sv.register.getFormData())
								.toString().trim());
					%>

					<%
						if (!((sv.register.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.register.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.register.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 110px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Number of Policies in Plan")%></label>

					<%
						if (!((sv.numpols.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.numpols.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.numpols.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "padding-right: 50px"
					: "width:50px;"%>'>
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Policy Number")%></label>

					<%
						qpsf = fw.getFieldXMLDef((sv.planSuffix).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.planSuffix);

						if (!((sv.planSuffix.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="padding-right: 50px;"></div>

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Number")%></label>
					<%
						if (!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase(
								"")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.life.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Coverage No")%></label>

					<%
						if (!((sv.coverage.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.coverage.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider No")%></label>

					<%
						if (!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase(
								"")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.rider.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>


				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Stat. Fund")%></label>

					<%
						fieldItem = appVars.loadF4FieldsShort(new String[] { "statFund" },
								sv, "E", baseModel);
						mappedItems = (Map) fieldItem.get("statFund");
						longValue = (String) mappedItems.get((sv.statFund.getFormData())
								.toString());
					%>


					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style="width: 100px;">
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
					<%
						if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {
					%>
					<%
						if (!((sv.statSect.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.statSect.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
					%>
					<div
						class='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "blank_cell" : "output_cell"%>'
						style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						longValue = null;
							formatValue = null;
					%>
					<%
						}
					%>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Sub-section")%></label>

					<!--
		<%fieldItem = appVars.loadF4FieldsLong(
					new String[] { "statSubsect" }, sv, "E", baseModel);
			mappedItems = (Map) fieldItem.get("statSubsect");
			longValue = (String) mappedItems.get((sv.statSubsect.getFormData())
					.toString().trim());%>
	-->
					<!-- 
    
       <div class='<%=((sv.statSubsect.getFormData() == null) || (""
					.equals((sv.statSubsect.getFormData()).trim()))) ? "blank_cell"
					: "output_cell"%>'> 
	   <%=(sv.statSubsect.getFormData()).toString()%>
	   </div>
     -->
					<div
						class='<%=((longValue == null) || ("".equals(longValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
						style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "padding-right:50px;"
					: "width:238px;"%>'>
						<%
							if (longValue != null) {
						%>

						<%=longValue%>

						<%
							}
						%>
					</div>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Life Assured")%></label>
					<table>
					<tr>
					<td>
						<%
							if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
						%>


						<%
							if (!((sv.life.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.life.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 71px">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td >
						<%
							if (!((sv.lifename.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.lifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width:71px;max-width:200px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>

					</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="form-group">
					<%
						if (!((sv.zagelit.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.zagelit.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						} else {

							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue((sv.zagelit.getFormData())
										.toString());
							} else {
								formatValue = formatValue(longValue);
							}

						}
					%>

					<div id='zagelit' class='label_txt' class='label_txt'
						onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit
					.getFormData())%></div>
					<%
						longValue = null;
						formatValue = null;
					%>

					<%
						qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
						qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						formatValue = smartHF.getPicFormatted(qpsf, sv.anbAtCcd);

						if (!((sv.anbAtCcd.getFormData()).toString()).trim()
								.equalsIgnoreCase("")) {
							if (longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue(formatValue);
							} else {
								formatValue = formatValue(longValue);
							}
						}

						if (!formatValue.trim().equalsIgnoreCase("")) {
					%>
					<div class="output_cell" style="width: 50px;">
						<%=XSSFilter.escapeHtml(formatValue)%>
					</div>
					<%
						} else {
					%>

					<div class="blank_cell" style="width: 50px;" />

					<%
						}
					%>
					<%
						longValue = null;
						formatValue = null;
					%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><%=resourceBundleHandler
					.gettingValueFromBundle("Joint life")%></label>
					<table>
					<tr>
					<td>

						<%
							if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
									BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.jlife.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "blank_cell" : "output_cell"%>'
							style="min-width: 71px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
						</td>
						<td>

						<%
							if (!((sv.jlifename.getFormData()).toString()).trim()
									.equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.jlifename.getFormData())
											.toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
							style="min-width:71px;max-width:200px;margin-left: 1px;">
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
							formatValue = null;
						%>




					</td>
					</tr>
					</table>
				</div>
			</div>
		</div>

		<!-- <br> -->
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#contact_tab" data-toggle="tab"><label><%=resourceBundleHandler
					.gettingValueFromBundle("Risk and Premium")%></label></a>
					</li>
					<li><a href="#other_tab" data-toggle="tab"><label><%=resourceBundleHandler
					.gettingValueFromBundle("Processing Details")%></label></a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="contact_tab">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Sum Assured")%></label>


									<%
										qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.sumin,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

										if (!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase(
												"")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}

										if (!formatValue.trim().equalsIgnoreCase("")) {
									%>
									<div class="output_cell"
										style="width: 140px; text-align: right;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										} else {
									%>

									<div class="blank_cell" style="width: 82px;"></div>

									<%
										}
									%>
									<%
										longValue = null;
										formatValue = null;
									%>


								</div>
							</div>




							<div class="col-md-3">
								<div class="form-group">
									<%
									//ILJ-386
										if (sv.cntEnqScreenflag.compareTo("Y") == 0) {
									%>
									<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
									<%
										} else if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>
										<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
				        			<%} else { %>
				        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage RCD"))%></label>
				        			<%} %>
				        			<!-- ILJ-45 End -->


									<%
										qpsf = fw.getFieldXMLDef((sv.crrcdDisp).getFieldName());
										qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.crrcdDisp);

										if (!((sv.crrcdDisp.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}

										if (!formatValue.trim().equalsIgnoreCase("")) {
									%>
									<div class="output_cell" style="width: 142px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										} else {
									%>

									<div class="blank_cell" style="width: 82px;" />

									<%
										}
									%>
									<%
										longValue = null;
										formatValue = null;
									%>


								</div>
							</div>




							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Mortality class")%></label>

									<%
										fieldItem = appVars.loadF4FieldsLong(new String[] { "mortcls" },
												sv, "E", baseModel);
										mappedItems = (Map) fieldItem.get("mortcls");
										longValue = (String) mappedItems.get((sv.mortcls.getFormData())
												.toString().trim());
									%>


									<%
										if (!((sv.mortcls.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.mortcls.getFormData())
														.toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.mortcls.getFormData())
														.toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
										style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
					: "width:141px;"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>

								</div>
							</div>







							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Single Premium")%></label>


									<%
										qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.singlePremium,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

										if (!((sv.singlePremium.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}

										if (!formatValue.trim().equalsIgnoreCase("")) {
									%>
									<div class="output_cell"
										style="width: 142px; text-align: right; padding-right: 4px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										} else {
									%>

									<div class="blank_cell" style="width: 82px;"></div>

									<%
										}
									%>
									<%
										longValue = null;
										formatValue = null;
									%>

								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								<!-- ILJ-45 Starts -->
				               	<% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
					        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
					        	<%} else { %>
					        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
					        	<%} %>
					        	<!-- ILJ-45 End -->
									<!-- <div class="input-group"> -->

									<!-- <div class="row"> -->
									<table>
										<tr>
										<td>
											<%
												if (((BaseScreenData) sv.riskCessAge) instanceof StringBase) {
											%>
											<%=smartHF.getRichText(0, 0, fw, sv.riskCessAge,
						(sv.riskCessAge.getLength() + 1), null).replace(
						"absolute", "relative")%>
											<%
												} else if (((BaseScreenData) sv.riskCessAge) instanceof DecimalData) {
											%>
											<%
												if (sv.riskCessAge.equals(0)) {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.riskCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'output_cell \' style=\'width: 40px; \' ")%>
											<%
												} else {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.riskCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
											<%
												}
											%>
											<%
												} else {
											%>
											<%
												}
											%>
											</td>

										<td>


											<%
												if (((BaseScreenData) sv.riskCessTerm) instanceof StringBase) {
											%>
											<%=smartHF.getRichText(0, 0, fw, sv.riskCessTerm,
						(sv.riskCessTerm.getLength() + 1), null).replace(
						"absolute", "relative")%>
											<%
												} else if (((BaseScreenData) sv.riskCessTerm) instanceof DecimalData) {
											%>
											<%
												if (sv.riskCessTerm.equals(0)) {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.riskCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'blank_cell\' style=\'width: 40px; \' ")%>
											<%
												} else {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.riskCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
											<%
												}
											%>
											<%
												} else {
											%>
											<%
												}
											%>
										</td>
										</tr>
										</table>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
								<!-- ILJ-45 Starts -->
								<% if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
									<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation date")%></label>
								<%} else { %>
									<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cessation date"))%></label>
								<%} %>
								<!-- ILJ-45 End -->
									<div class="input-group">

										<%
											if (!((sv.riskCessDateDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.riskCessDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.riskCessDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 142px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>

									</div>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>


									<%
										if (!((sv.liencd.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.liencd.getFormData())
														.toString());
											} else {
												formatValue = formatValue(longValue);
											}

										} else {

											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue((sv.liencd.getFormData())
														.toString());
											} else {
												formatValue = formatValue(longValue);
											}

										}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
										style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:82px;"
					: "width:145px;"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
										formatValue = null;
									%>
								</div>
							</div>

							<%
								if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
						.gettingValueFromBundle("Premium Basis")%></label>

									<%
										fieldItem = appVars.loadF4FieldsLong(
													new String[] { "prmbasis" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("prmbasis");
											optionValue = makeDropDownList(mappedItems,
													sv.prmbasis.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems
													.get((sv.prmbasis.getFormData()).toString().trim());
									%>

									<%
										if ((new Byte((sv.prmbasis).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>


									<%
										if (!((sv.prmbasis.getFormData()).toString()).trim()
														.equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.prmbasis
																.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.prmbasis
																.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>'
										style='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "width:82px;" : "width:141px;"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
												formatValue = null;
									%>
									<%
										}
									%>

								</div>
							</div>
							<%
								}
							%>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Prem cess Age / Term")%></label>
									<table>
									<tr>
									<td>
											<%
												if (((BaseScreenData) sv.premCessAge) instanceof StringBase) {
											%>
											<%=smartHF.getRichText(0, 0, fw, sv.premCessAge,
						(sv.premCessAge.getLength() + 1), null).replace(
						"absolute", "relative")%>
											<%
												} else if (((BaseScreenData) sv.premCessAge) instanceof DecimalData) {
											%>
											<%
												if (sv.premCessAge.equals(0)) {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'output_cell \' style=\'width: 40px; \' ")%>
											<%
												} else {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessAge,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
											<%
												}
											%>
											<%
												} else {
											%>
											<%
												}
											%>

										</td>
										<td>


											<%
												if (((BaseScreenData) sv.premCessTerm) instanceof StringBase) {
											%>
											<%=smartHF.getRichText(0, 0, fw, sv.premCessTerm,
						(sv.premCessTerm.getLength() + 1), null).replace(
						"absolute", "relative")%>
											<%
												} else if (((BaseScreenData) sv.premCessTerm) instanceof DecimalData) {
											%>
											<%
												if (sv.premCessTerm.equals(0)) {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'blank_cell\' ",
									"class=\'blank_cell\' style=\'width: 40px; \' ")%>
											<%
												} else {
											%>
											<%=smartHF
							.getHTMLVar(
									0,
									0,
									fw,
									sv.premCessTerm,
									COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS)
							.replace("class=\'output_cell \' ",
									"class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
											<%
												}
											%>
											<%
												} else {
											%>
											<%
												}
											%>
											</td>
											</tr>
										</table>

								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Premium Cessation Date")%></label>
									<div class="input-group">
										<%
											if (!((sv.premcessDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.premcessDisp.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.premcessDisp.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 142px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>


									</div>
								</div>
							</div>



							<%
								if ((new Byte((generatedText24).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
						.gettingValueFromBundle("Joint Life (J/L)")%></label>
									<%
										if (!((sv.select.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.select.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.select.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "blank_cell" : "output_cell"%>'
										style='<%=((formatValue == null) || ("".equals(formatValue
						.trim()))) ? "padding-right: 50px" : "width:50px;"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
											formatValue = null;
									%>


								</div>
							</div>
							<%
								}
							%>

							<%
								if ((new Byte((sv.dialdownoption).getInvisible()))
										.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
						.gettingValueFromBundle("Dial Down Option")%></label>

									<%
										fieldItem = appVars.loadF4FieldsLong(
													new String[] { "dialdownoption" }, sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("dialdownoption");
											optionValue = makeDropDownList(mappedItems,
													sv.dialdownoption.getFormData(), 2,
													resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.dialdownoption
													.getFormData()).toString().trim());
									%>

									<%
										if ((new Byte((sv.dialdownoption).getInvisible()))
													.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
									%>


									<%
										if (!((sv.dialdownoption.getFormData()).toString()).trim()
														.equalsIgnoreCase("")) {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.dialdownoption
																.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												} else {

													if (longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue((sv.dialdownoption
																.getFormData()).toString());
													} else {
														formatValue = formatValue(longValue);
													}

												}
									%>
									<div
										class='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "blank_cell" : "output_cell"%>'
										style='<%=((formatValue == null) || ("".equals(formatValue
							.trim()))) ? "width:82px;" : "width:141px;"%>'>
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										longValue = null;
												formatValue = null;
									%>
									<%
										}
									%>

								</div>
							</div>
							<%
								}
							%>
						</div>
						<br>
						<!-- <hr><br> -->

						<%
							if ((new Byte((sv.adjustageamt).getInvisible()))
									.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<div class="row">
							<%
								if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
							.gettingValueFromBundle("Age Adjusted Amount")%></label>
									<%
										if (((BaseScreenData) sv.adjustageamt) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw,
								sv.adjustageamt,
								(sv.adjustageamt.getLength() + 1), null)
								.replace("absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.adjustageamt) instanceof DecimalData) {
									%>
									<%
										if (sv.adjustageamt.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.adjustageamt,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>

								</div>
							</div>
							<%
								}
							%>

							<%
								if ((new Byte((sv.zlinstprem).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
							.gettingValueFromBundle("Rate Adjusted Amount")%></label>
									<%
										if (((BaseScreenData) sv.rateadj) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.rateadj,
								(sv.rateadj.getLength() + 1), null).replace(
								"absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.rateadj) instanceof DecimalData) {
									%>
									<%
										if (sv.rateadj.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.rateadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>
								</div>
							</div>
							<%
								}
							%>


							<%
								if ((new Byte((sv.zlinstprem).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
							.gettingValueFromBundle("Flat Mortality Amount")%></label>
									<%
										if (((BaseScreenData) sv.fltmort) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.fltmort,
								(sv.fltmort.getLength() + 1), null).replace(
								"absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.fltmort) instanceof DecimalData) {
									%>
									<%
										if (sv.fltmort.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.fltmort,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>
								</div>
							</div>
							<%
								}
							%>

							<%
								if ((new Byte((sv.zlinstprem).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label><%=resourceBundleHandler
							.gettingValueFromBundle("Load Amount")%></label>
									<%
										if (((BaseScreenData) sv.loadper) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.loadper,
								(sv.loadper.getLength() + 1), null).replace(
								"absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.loadper) instanceof DecimalData) {
									%>
									<%
										if (sv.loadper.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.loadper,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>
								</div>
							</div>
							<%
								}
							%>
						</div>
						<%
							}
						%>
						<div class="row">
							<%
								if ((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
							%>
							<%
								if ((new Byte((sv.zlinstprem).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler
							.gettingValueFromBundle("Premium Adjusted Amount")%></label>
									<%
										if (((BaseScreenData) sv.premadj) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.premadj,
								(sv.premadj.getLength() + 1), null).replace(
								"absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.premadj) instanceof DecimalData) {
									%>
									<%
										if (sv.premadj.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.premadj,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>


								</div>
							</div>
							<%
								}
								}
							%>
							
							<div class="col-md-3">
								<div class="form-group">
								<%
								if ((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
								%>
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
									
								<%} else { %>
									<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
								<%}%>

									<%
											qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
											formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
										//formatValue = (sv.zlinstprem.getFormData()).toString();
										if (!((sv.zlinstprem.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}

										if (!formatValue.trim().equalsIgnoreCase("")) {
									%>
									<div class="output_cell"
										style="width: 145px !important; text-align: right;" id="zlinstprem">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										} else {
									%>

									<div class="blank_cell" style="width: 145px !important;" id="zlinstprem"></div>

									<%
										}
									%>
									<%
										longValue = null;
										formatValue = null;
									%>
								</div>
							</div>


							<%
								if ((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
										BaseScreenData.INVISIBLE)) != 0) {
							%>
							<%
								if ((new Byte((sv.zlinstprem).getInvisible()))
											.compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
							<div class="col-md-3">
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler
							.gettingValueFromBundle("Basic Premium")%></label>
									<%
										if (((BaseScreenData) sv.zbinstprem) instanceof StringBase) {
									%>
									<%=smartHF.getRichText(0, 0, fw, sv.zbinstprem,
								(sv.zbinstprem.getLength() + 1), null).replace(
								"absolute", "relative")%>
									<%
										} else if (((BaseScreenData) sv.zbinstprem) instanceof DecimalData) {
									%>
									<%
										if (sv.zbinstprem.equals(0)) {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.zbinstprem,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'blank_cell\' ",
											"class=\'blank_cell\' style=\'width: 145px !important; \' ")%>
									<%
										} else {
									%>
									<%=smartHF
									.getHTMLVar(
											0,
											0,
											fw,
											sv.zbinstprem,
											COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS)
									.replace("class=\'output_cell \' ",
											"class=\'output_cell \' style=\'width: 145px !important;  text-align: right;\' ")%>
									<%
										}
									%>
									<%
										} else {
									%>
									<%
										}
									%>
								</div>
							</div>
							<%
								}
								}
							%>
							</div>
							
							
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label  style="white-space: nowrap;"><%=resourceBundleHandler
					.gettingValueFromBundle("Total Premium")%></label>

									<%
										qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
										//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
										formatValue = smartHF.getPicFormatted(qpsf, sv.instPrem,
												COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);

										if (!((sv.instPrem.getFormData()).toString()).trim()
												.equalsIgnoreCase("")) {
											if (longValue == null || longValue.equalsIgnoreCase("")) {
												formatValue = formatValue(formatValue);
											} else {
												formatValue = formatValue(longValue);
											}
										}

										if (!formatValue.trim().equalsIgnoreCase("")) {
									%>
									<div class="output_cell"
										style="width: 145px !important; text-align: right;" id="instPrem">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>
									<%
										} else {
									%>

									<div class="blank_cell" style="width: 145px !important;" id="instPrem"></div>

									<%
										}
									%>
									<%
										longValue = null;
										formatValue = null;
									%>
								</div>
							</div>
							
					 <div class="col-md-3">
								<div class="form-group">	
								<% //ILJ-387
										if (sv.cntEnqScreenflag.compareTo("N") == 0){ 
								%> 
						<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
							<div class="input-group">
							
							<%
							if ((new Byte((generatedText42).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
								qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
						%>

						<input name='taxamt' type='text'
							<%if ((sv.taxamt).getClass().getSimpleName().equals("ZonedDecimalData")) {%>
							style="text-align: right; min-width: 145px !important;" <%}%>
							value='<%=smartHF.getPicFormatted(qpsf, sv.taxamt)%>'
							<%valueThis = smartHF.getPicFormatted(qpsf, sv.taxamt);
				if (valueThis != null && valueThis.trim().length() > 0) {%>
							title='<%=smartHF.getPicFormatted(qpsf, sv.taxamt)%>' <%}%>
							size='<%=sv.taxamt.getLength()%>'
							maxLength='<%=sv.taxamt.getLength()%>' onFocus='doFocus(this)'
							onHelp='return fieldHelp(taxamt)'
							onKeyUp='return checkMaxLength(this)'
							onKeyPress="return  digitsOnly(this,<%=qpsf.getDecimals()%> ); "
							decimal='<%=qpsf.getDecimals()%>'
							onPaste='return doPasteNumber(event);'
							onBlur='return doBlurNumber(event);'
							<%if ((new Byte((sv.taxamt).getEnabled())).compareTo(new Byte(BaseScreenData.DISABLED)) == 0
						|| fw.getVariables().isScreenProtected()) {%>
							readonly="true" class="output_cell"
							<%} else if ((new Byte((sv.taxamt).getHighLight())).compareTo(new Byte(BaseScreenData.BOLD)) == 0) {%>
							class="bold_cell" <%} else {%>
							class=' <%=(sv.taxamt).getColor() == null ? "input_cell"
							: (sv.taxamt).getColor().equals("red") ? "input_cell red reverse" : "input_cell"%>'
							<%}%>>

						<%
							}
						%>
							
							
					</div>
					<%
							}
						%>
					</div></div>		 
							
							
						</div>
					</div>
					<div class="tab-pane fade" id="other_tab">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Ann Proc Date")%></label>
									<div class="input-group">

										<%
											if (!((sv.annivProcDateDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.annivProcDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.annivProcDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Rerate Date")%></label>
									<div class="input-group">
										<%
											if (!((sv.rerateDateDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rerateDateDisp.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rerateDateDisp.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Rerate from Date")%></label>
									<div class="input-group">

										<%
											if (!((sv.rerateFromDateDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rerateFromDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.rerateFromDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Last Bonus Date")%></label>
									<div class="input-group">
										<%
											if (!((sv.unitStatementDateDisp.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.unitStatementDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.unitStatementDateDisp
															.getFormData()).toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Ind")%></label>
									<div class="input-group">
										<%
											if (!((sv.bonusInd.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bonusInd.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bonusInd.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 50px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Bonus Appl.Method")%></label>
									<div class="input-group">
										<%
											if (!((sv.bappmeth.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bappmeth.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bappmeth.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:80px;"
					: "width:140px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Dividend Option")%></label>
									<div class="input-group">

										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "zdivopt" },
													sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("zdivopt");
											optionValue = makeDropDownList(mappedItems,
													sv.zdivopt.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.zdivopt.getFormData())
													.toString().trim());
										%>
										<!-- ILIFE-884 End kpalani6  -->
										<%
											if (!((sv.zdivopt.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.zdivopt.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.zdivopt.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 240px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>


									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("MOP")%></label>
									<div class="input-group">
										<!-- ILIFE-884 start kpalani6  -->
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "paymth" }, sv,
													"E", baseModel);
											mappedItems = (Map) fieldItem.get("paymth");
											optionValue = makeDropDownList(mappedItems,
													sv.paymth.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.paymth.getFormData())
													.toString().trim());
										%>
										<!-- ILIFE-884 end kpalani6  -->
										<%
											if (!((sv.paymth.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.paymth.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.paymth.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "width:80px;"
					: "width:200px;"%>'>
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Payment Currency")%></label>
									<div class="input-group">
										<!-- ILIFE-884 start kpalani6  -->
										<%
											fieldItem = appVars.loadF4FieldsLong(new String[] { "paycurr" },
													sv, "E", baseModel);
											mappedItems = (Map) fieldItem.get("paycurr");
											optionValue = makeDropDownList(mappedItems,
													sv.paycurr.getFormData(), 2, resourceBundleHandler);
											longValue = (String) mappedItems.get((sv.paycurr.getFormData())
													.toString().trim());
										%>
										<!-- ILIFE-884 end kpalani6  -->

										<%
											if (!((sv.paycurr.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.paycurr.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.paycurr.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="width: 200px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler.gettingValueFromBundle("Payor")%></label>
									<table>
									<tr>
									<td>
										<%
											if (!((sv.payclt.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.payclt.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.payclt.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="min-width: 65px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
										</td>
										<td>

										<%
											if (!((sv.payorname.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.payorname.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.payorname.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="max-width: 300px;margin-left: 1px;min-width: 100px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</td>
									</tr>
									</table>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Bank Details")%></label>
									<table>
									<tr>
									<td>
										<%
											if (!((sv.bankkey.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankkey.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankkey.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="min-width: 65px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
										</td>
										<td>
										<%
											if (!((sv.branchdesc.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.branchdesc.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.branchdesc.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="min-width: 100px;max-width: 300px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
									</td>
									</tr>
									</table>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><%=resourceBundleHandler
					.gettingValueFromBundle("Bank Account")%></label>
									<table>
									<tr>
									<td>
										<%
											if (!((sv.bankacckey.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankacckey.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankacckey.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="min-width: 65px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>
										</td>
										<td>
										<%
											if (!((sv.bankdesc.getFormData()).toString()).trim()
													.equalsIgnoreCase("")) {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankdesc.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											} else {

												if (longValue == null || longValue.equalsIgnoreCase("")) {
													formatValue = formatValue((sv.bankdesc.getFormData())
															.toString());
												} else {
													formatValue = formatValue(longValue);
												}

											}
										%>
										<div
											class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'
											style="min-width: 100px;max-width: 300px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>
										<%
											longValue = null;
											formatValue = null;
										%>


									</td>
									</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div style='visibility: hidden;'>
			<table>
				<tr style='height: 22px;'>
					<td width='188'>&nbsp; &nbsp;<br /> <%
 	if (!((sv.crtabdesc.getFormData()).toString()).trim()
 			.equalsIgnoreCase("")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.crtabdesc.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.crtabdesc.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>




					</td>

					<td width='188'>&nbsp; &nbsp;<br /> <%
 	if (!((sv.payclt.getFormData()).toString()).trim()
 			.equalsIgnoreCase("")) {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.payclt.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	} else {

 		if (longValue == null || longValue.equalsIgnoreCase("")) {
 			formatValue = formatValue((sv.payclt.getFormData())
 					.toString());
 		} else {
 			formatValue = formatValue(longValue);
 		}

 	}
 %>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell"
					: "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div> <%
 	longValue = null;
 	formatValue = null;
 %>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<BODY >
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<input name='divdInd' id='divdInd' type='hidden'  value="<%=sv.divdInd.getFormData()%>"> 
									<!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("divdInd"))' class="hyperLink"> 
										<%=resourceBundleHandler
					.gettingValueFromBundle("Dividend Details")%>
									<!-- icon -->
									<%
										if (sv.divdInd.getFormData().equals("+")) {
									%>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.divdInd.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<%if (!appVars.ind69.isOn()) { %>
									<li>
									<input name='aepaydet' id='aepaydet' type='hidden'  value="<%=sv.aepaydet.getFormData()%>">
									<!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("aepaydet"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Antcptd Endwnt PayDetails")%>
									<!-- icon -->
									<%
									 	if (sv.aepaydet.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.aepaydet.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<% }else{ %>
								
								<li>
									<input name='aepaydet' id='aepaydet' type='hidden'  value="<%=sv.aepaydet.getFormData()%>">
									<!-- text -->
									<%if (sv.aepaydet.getInvisible() != BaseScreenData.INVISIBLE || sv.aepaydet.getEnabled() != BaseScreenData.DISABLED){%>
									<a href="#" class="disabledLink" > 
										<%=resourceBundleHandler.gettingValueFromBundle("Antcptd Endwnt PayDetails")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.aepaydet.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.aepaydet.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<% } %>
								
								<li>
									<input name='optextind' id='optextind' type='hidden'  value="<%=sv.optextind.getFormData()%>">
									<!-- text -->
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optextind"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle("Special Terms")%>
									<!-- icon -->
									<%
									 	if (sv.optextind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.optextind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='taxind' id='taxind' type='hidden'  value="<%=sv.taxind.getFormData()%>">
									<!-- text -->
									<%if (sv.taxind.getInvisible() != BaseScreenData.INVISIBLE || sv.taxind.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("taxind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Tax Details")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.taxind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.taxind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='bonusInd' id='bonusInd' type='hidden'  value="<%=sv.bonusInd.getFormData()%>">
									<!-- text -->
									<%if (sv.bonusInd.getInvisible() != BaseScreenData.INVISIBLE || sv.bonusInd.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("bonusInd"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Premium Breakdown")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.bonusInd.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.bonusInd.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<li>
									<input name='puaInd' id='puaInd' type='hidden'  value="<%=sv.puaInd.getFormData()%>">
									<!-- text -->
									<%if (sv.puaInd.getInvisible() != BaseScreenData.INVISIBLE || sv.puaInd.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("puaInd"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("PUA Details")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.puaInd.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.puaInd.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<%	if(sv.exclind.getInvisible()!= BaseScreenData.INVISIBLE){%> 
								<li>
									<input name='exclind' id='exclind' type='hidden'  value="<%=sv.exclind.getFormData()%>">
									<!-- text -->
									<%if (sv.exclind.getInvisible() != BaseScreenData.INVISIBLE || sv.exclind.getEnabled() != BaseScreenData.DISABLED){%>
										<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("exclind"))' class="hyperLink"> 
											<%=resourceBundleHandler.gettingValueFromBundle("Exclusions")%>
									<%} %>
									<!-- icon -->
									<%
									 	if (sv.exclind.getFormData().equals("+")) {
									 %>
											<i class="fa fa-tasks fa-fw sidebar-icon"></i> <%
									 	}
									 	if (sv.exclind.getFormData().equals("X")) {
									 %> 	<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
									 	}
									 %> </a>
								</li>
								<% } %>
							</ul>
					</span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>


<%@ include file="/POLACommon2NEW.jsp"%>