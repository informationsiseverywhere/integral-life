

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR50O";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr50oScreenVars sv = (Sr50oScreenVars) fw.getVariables();%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract number  ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract status  ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium status     ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life assured     ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Joint life       ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Life number      ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Coverage no        ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rider no  ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Component        ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Billing freq     ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Method of payment  ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Mortality Class  ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sum assured      ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk comm. date         ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Single premium   ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Inception date          ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Instalment prem  ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium cessation date  ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Loaded premium   ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Risk cessation date     ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate date             ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Rerate from date        ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ann. processing date    ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Ttl Prem w/Tax ");%>
<%{
		if (appVars.ind10.isOn()) {
			sv.optind01.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind11.isOn()) {
			sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind12.isOn()) {
			sv.optind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind10.isOn()) {
			sv.optind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind10.isOn()) {
			sv.optind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind13.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind14.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind15.isOn()) {
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind13.isOn()) {
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind13.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind16.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind17.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind18.isOn()) {
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind16.isOn()) {
			sv.optind03.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind16.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind19.isOn()) {
			sv.optind04.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind20.isOn()) {
			sv.optind04.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind21.isOn()) {
			sv.optind04.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind19.isOn()) {
			sv.optind04.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind19.isOn()) {
			sv.optind04.setHighLight(BaseScreenData.BOLD);
		}
		
		if (appVars.ind42.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-STARTS
		if (appVars.ind44.isOn()) {
			sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind43.isOn()) {
			sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind45.isOn()) {
			sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//ILIFE-3399-ENDS
		/*BRD-306 Starts */
		if (appVars.ind22.isOn()) {
			sv.benCessAge.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind24.isOn()) {
			sv.benCessAge.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind22.isOn()) {
			sv.benCessAge.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind22.isOn()) {
			sv.benCessAge.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind23.isOn()) {
			sv.benCessTerm.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind25.isOn()) {
			sv.benCessTerm.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind23.isOn()) {
			sv.benCessTerm.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind23.isOn()) {
			sv.benCessTerm.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind28.isOn()) {
			sv.benCessDateDisp.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind30.isOn()) {
			sv.benCessDateDisp.setReverse(BaseScreenData.REVERSED);
			sv.benCessDateDisp.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind30.isOn()) {
			sv.benCessDateDisp.setHighLight(BaseScreenData.BOLD);
		}		if (appVars.ind19.isOn()) {
			sv.mortcls.setReverse(BaseScreenData.REVERSED);
		}
		if (appVars.ind06.isOn()) {
			sv.mortcls.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind26.isOn()) {
			sv.mortcls.setColor(BaseScreenData.RED);
		}
		if (appVars.ind05.isOn()) {
			sv.mortcls.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (!appVars.ind26.isOn()) {
			sv.mortcls.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind54.isOn()) {
			sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind55.isOn()) {
			sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind56.isOn()) {
			sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind57.isOn()) {
			sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-009-STARTS
		if (appVars.ind59.isOn()) {
			sv.statcode.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind58.isOn()) {
			sv.statcode.setEnabled(BaseScreenData.DISABLED);
		}
		//BRD-009-ENDS
		// ILIFE-3509 starts
		if (appVars.ind60.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		// ILIFE-3509 ends
		//BRD-NBP-011 starts
		if (appVars.ind72.isOn()) {
			sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
			sv.dialdownoption.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind72.isOn()) {
			sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind70.isOn()) {
			sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind71.isOn()) {
			sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
		}
		//BRD-NBP-011 ends
}
/*BRD-306 Ends */


	%>


<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">       
			<%if ((new Byte((generatedText5).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %> 
					<div class="col-md-6">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Contract number")%></label>
								
								 <table><tr><td>
						<%
							if ((new Byte((sv.chdrnum).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
						%>
						<%
							if (!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								} else {
									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.chdrnum.getFormData()).toString());
									} else {
										formatValue = formatValue(longValue);
									}
								}
						%>
						<div
							class='<%=((formatValue == null) || ("".equals(formatValue.trim()))) ? "blank_cell" : "output_cell"%>'>
							<%=XSSFilter.escapeHtml(formatValue)%>
						</div>
						<%
							longValue = null;
								formatValue = null;
						%>
						<%
							}
						%>
</td><td style="padding-left:1px;">
						<%
							if (!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.cnttype.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td><td style="padding-left:1px;max-width:250px;">
						<%
							if (!((sv.ctypdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
								} else {
									formatValue = formatValue(longValue);
								}

							} else {

								if (longValue == null || longValue.equalsIgnoreCase("")) {
									formatValue = formatValue((sv.ctypdesc.getFormData()).toString());
						 		} else {
									formatValue = formatValue(longValue);
								}

							}
						%>
						<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>
</td></tr></table>
					
					 </div>
					 
				</div>   <%}%>
				
				<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								
				<div class="col-md-2">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Life number")%></label>
							<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
					 	</div>
					 
				</div>  <%}%>
				<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>
							<%if ((new Byte((sv.coverage).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
					 	</div>
					 
				</div><%}%>
				<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>
								<%if ((new Byte((sv.rider).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%}%>
		</div>
		
		<div class="row">        
				<%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2">
						<div class="form-group" style="padding-right:1px"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Contract status")%></label>
											<div style="padding-right:1px"> 
								<%if ((new Byte((sv.chdrstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div></div>
					 
				</div><%}%>
				 <%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Premium status")%></label>
								<%if ((new Byte((sv.premstatus).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:90px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%}%>
				<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2" >
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Inception date")%></label>
								<%if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crrcdDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
								
						</div>
					 
				</div><%}%>
				
				<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2" >
						<div class="form-group"> 
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Billing freq")%></label>
								
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"billfreq"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("billfreq");
	optionValue = makeDropDownList( mappedItems , sv.billfreq.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.billfreq.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.billfreq).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.billfreq.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.billfreq.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="max-width:100px; ">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%}%>
				<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2" >
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Method of payment")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mop"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mop");
	optionValue = makeDropDownList( mappedItems , sv.mop.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mop.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.mop).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.mop.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mop.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%}%>
				<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-2">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Risk comm. date")%></label>
								<%if ((new Byte((sv.rcdateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rcdateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rcdateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%}%>
		</div>
		
			<div class="row">        
				
				
				
				
		</div>
					 
					 
			<div class="row">  
			<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>      
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Life assured")%></label>
							<table><tr><td>
							<%
								if (!((sv.lifenum.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifenum.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div id="lifcnum"
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="min-width: 80px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
							</td><td style="padding-left:1px;max-width:150px;">

							<%
								if (!((sv.lifedesc.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.lifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
			</td></tr></table>
								
					 </div>
				</div><%}%>
			<div class="col-md-5">
			</div>
				<div class="col-md-2">
						<div class="form-group"> 
							
								<%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				 
				<label style="white-space: nowrap;"> <%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></label>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="max-width:100px">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
								
						</div>
					 
				</div>
				<%if ((new Byte((sv.statcode).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-2">
						<div class="form-group"> 
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Occupation Class")%></label>
								<%	
	fieldItem=appVars.loadF4FieldsLong(new String[] {"statcode"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statcode");
	optionValue = makeDropDownList( mappedItems , sv.statcode.getFormData(),2,resourceBundleHandler);  
longValue = (String) mappedItems.get((sv.statcode.getFormData()).toString().trim());  
%>

<% 
	if((new Byte((sv.statcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0 ||fw.getVariables().isScreenProtected()){ 
%>  
  <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' 
							style='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"width:82px;" : "width:140px;" %>'>  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>

<%
longValue = null;
%>

	<% }else {
	%>
	
<% if("red".equals((sv.statcode).getColor())){
%>
<div style="border:1px; border-style: solid; border-color: #B55050;  width:145px;"> 
<%
} 
%>

<select name='statcode' type='list' style="width:135px;"
<% 
	if((new Byte((sv.statcode).getEnabled()))
	.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
%>  
	readonly="true"
	disabled
	class="output_cell"
<%
	}else if((new Byte((sv.statcode).getHighLight())).
		compareTo(new Byte(BaseScreenData.BOLD)) == 0){
%>	
		class="bold_cell" 
<%
	}else { 
%>
	class = 'input_cell' 
<%
	} 
%>
>
<%=optionValue%>
</select>
<% if("red".equals((sv.statcode).getColor())){
%>
</div>
<%
} 
%>

<%
}
%>
<% longValue = null;%>	
						</div>
					 
				</div><% 
			} 
		%>
		</div>
		
			<div class="row">        
			<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
								
						<table><tr><td>
							<%
								if (!((sv.jlife.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlife.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div 
								class='<%=(sv.lifenum.getFormData()).trim().length() == 0 ? "blank_cell"
					: "output_cell"%>' style="min-width: 71px;">
								<%=XSSFilter.escapeHtml(formatValue)%>
							</div>
							<%
								longValue = null;
								formatValue = null;
							%>
</td><td style="padding-left:1px;">
							<%
								if (!((sv.jlifedesc.getFormData()).toString()).trim()
										.equalsIgnoreCase("")) {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								} else {

									if (longValue == null || longValue.equalsIgnoreCase("")) {
										formatValue = formatValue((sv.jlifedesc.getFormData())
												.toString());
									} else {
										formatValue = formatValue(longValue);
									}

								}
							%>
							<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:71px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
			</td></tr></table>
													 
				</div>
				</div><% 
			} 
		%>
		</div>
		<br><br>
		<hr>
		<div class="row"> 
		<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>       
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Component")%></label>
								<table><tr><td>
								<%if ((new Byte((sv.crtable).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtable.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtable.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
</td><td style="padding-left:1px;max-width:250px;">




<%if ((new Byte((sv.crtabled).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.crtabled.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabled.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.crtabled.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  >
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	  
  <%}%>
	</td></tr></table>							
						
						</div>
					 
				</div><% 
			} 
		%>
				<div class="col-md-3">
				</div>
				<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Sum assured")%></label>
								<%if ((new Byte((sv.sumin).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.sumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.sumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.sumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:160px;text-align: right;padding-right:3px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
						</div>
					 
				</div><% 
			} 
		%>
				<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
								<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality Class")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("mortcls");
	optionValue = makeDropDownList( mappedItems , sv.mortcls.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.mortcls).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:70px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><% 
			} 
		%>
		</div>
		<div class="row"> 
		<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term")%></label>
								<table><tr><td>
								<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
<%if(sv.riskCessAge.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</td><td style="padding-left:1px;">

<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm,( sv.riskCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
<%if(sv.riskCessTerm.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
			</td></tr></table>			
					 </div>
				</div><%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Risk cessation date")%></label>
								<%if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%}%>
				<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
				<div class="form-group"> 
								<label style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Ann. processing date")%></label>
								<%if ((new Byte((sv.annivProcDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
					<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
						</div><%}%>
						<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Lien Code")%></label>
								<%					
		if(!((sv.liencd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
								
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
						</div>
					 </div>
			</div>		 
		<div class="row"> 
		<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
								<table><tr><td>
								<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
<%if(sv.premCessAge.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

</td><td style="padding-left:1px;">
<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
<%if(sv.premCessTerm.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
	</td></tr></table>
						
					 </div>
				</div><%} %>
				<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Premium cessation date")%></label>
								<%if ((new Byte((sv.premcessDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
								
						</div>
					 
				</div><%} %>
				<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate date")%></label>
								<%if ((new Byte((sv.rerateDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rerateDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%} %>
				<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Single premium")%></label>
								<%if ((new Byte((sv.singlePremium).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;padding-right:4px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;" > &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
						</div>
					 
				</div><%} %>
			</div>		 
			
		<div class="row"> 
		<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit_cess_Age_/_Term")%></label>
								<table><tr><td>
								
	<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
<%if(sv.premCessAge.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</td><td style="padding-left:1px;">

<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
<%if(sv.premCessTerm.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

					</td></tr></table>	
						</div> 
				</div><%} %>
				<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit_cessation_date")%></label>
								<%if ((new Byte((sv.premcessDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%} %>
				<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate from date")%></label>
								<%if ((new Byte((sv.rerateFromDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.rerateFromDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' 
						style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%} %>
				<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded premium")%></label>
								<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
		//	qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;padding-right:4px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
	
						</div>
					 
				</div><%} %>
			</div>		 
			
				
		<div class="row">
		<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %> 
		<div class="col-md-3">
						<div class="form-group"> 
								<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("prmbasis");
	optionValue = makeDropDownList( mappedItems , sv.prmbasis.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.prmbasis.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.prmbasis.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.prmbasis.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	
						</div>
					 
				</div><%} %>
				
				<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"waitperiod"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("waitperiod");
	optionValue = makeDropDownList( mappedItems , sv.waitperiod.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.waitperiod.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.waitperiod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.waitperiod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%} %>
				
				<%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"bentrm"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("bentrm");
	optionValue = makeDropDownList( mappedItems , sv.bentrm.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.bentrm.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bentrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bentrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%} %>
				
				<%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"poltyp"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("poltyp");
	optionValue = makeDropDownList( mappedItems , sv.poltyp.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.poltyp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.poltyp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.poltyp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
						</div>
					 
				</div><%} %>
			</div>		 
			
		<div class="row"> 
		<div class="col-md-3">
						<div class="form-group"> 
								<label></label>
								
	
						</div>
					 
				</div>
				<div class="col-md-3">
						<div class="form-group"> 
							
								
						</div>
					 
				</div>
				<div class="col-md-3">
						<div class="form-group"> 
							
								
						</div>
					 
				</div>
				<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>
								<%
	fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("dialdownoption");
	optionValue = makeDropDownList( mappedItems , sv.dialdownoption.getFormData(),2,resourceBundleHandler);  
	longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());
	%>

<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.dialdownoption.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dialdownoption.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dialdownoption.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
								
						</div>
					 
				</div><%} %>
			</div>		 
			
	<hr>
		
			<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<div class="row"> 
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
								<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
	
						</div>
					 
				</div><%}%>
				<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-3">
						<div class="form-group"> 
							
								<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>
								<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
						</div>
					 
				</div><%}%>
				<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-3">
						<div class="form-group"> 
							
							<label>	<%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
							<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
						</div>
					 
				</div><%}%>
				<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
								<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
						</div>
					 
				</div>
			</div>	<%}%>	
			
		<div class="row"> 
		<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<div class="col-md-3">
						<div class="form-group"> 
								<label> <%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
								<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
	
						</div>
					 
				</div><%}} %>
				<div class="col-md-3">
						<div class="form-group"> 
						<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
						<%} else { %>
							<label><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
						<%}%>		
								<%	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			//ILIFE 1487 STARTS
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_MINUS_AFTER_ZEROSUPPRESS);
			//ILIFE 1487 ENDS
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
						</div>
					 
				</div>
				<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
				<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
				<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>
								<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
						</div>
					 
				</div> <%}}%>
				<%if ((new Byte((sv.zstpduty01).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
					<div class="col-md-3">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
								<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
<%if(sv.zstpduty01.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 82px;\' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 140px;  text-align: right;\' ")%>	
<%} %>
<%}else {%>
<%}%>
						</div>
					 
				</div> <%}%>
		</div> 
		
		<div class="row"> 
		<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
		<div class="col-md-3">
						<div class="form-group"> 
								<label><%=resourceBundleHandler.gettingValueFromBundle("Instalment prem")%></label>
								<%if ((new Byte((sv.instPrem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.S15VS2);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"> &nbsp; </div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 <%}%>
						</div> 
		</div> <%}%>
		<%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
			<div class="col-md-3">
						<div class="form-group"> 
								<label> <%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
								<%if ((new Byte((sv.taxamt).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
		<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_MINUSAFTER_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt);
			
			if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
				<div class="blank_cell"  style="width:82px;"> &nbsp; </div>
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
 <%}%>
						</div> 
		</div> <%}%>
	</div>
</div>

</div>	

<BODY >
	<div class="sidearea">
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse" style="display: block;">
				<ul class="nav" id="mainForm_OPTS">
					<li>
						<span>
							<ul class="nav nav-second-level" aria-expanded="true">
								<li>
									<input name='optind01' id='optind01' type='hidden'  value="<%=sv.optind01
.getFormData()%>">
<%if (sv.optind01.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind01.getEnabled() != BaseScreenData.DISABLED){%>
									<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind01"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc01.getFormData())%>
 
<%} %>
									<%
if (sv.optind01
.getFormData().equals("+")) {
%>

<i class="fa fa-tasks fa-fw sidebar-icon"></i>

<%}else {
	
    if (sv.optind01.getFormData().equals("X")) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind01
.getFormData().equals("X")) {
%>
<i class="fa fa-warning fa-fw sidebar-icon"></i> 
<%
}
%></a>
								</li>
								<li>
							
									<input name='optind02' id='optind02' type='hidden'  value="<%=sv.optind02.getFormData()%>">
<%if (sv.optind02.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind02.getEnabled() != BaseScreenData.DISABLED){%>							
					        	<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind02"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc02.getFormData())%>
 
<%} %>	

									<%
if (sv.optind02.getFormData().equals("+")) {
%>


<i class="fa fa-tasks fa-fw sidebar-icon" ></i>
<%}else {
	
    if (sv.optind02.getFormData().equals("X") ) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind02.getFormData().equals("X")) {
%>
<i class="fa fa-warning fa-fw sidebar-icon"></i> <%
}
%></a>

								</li>
								<%	if(sv.optind03
.getInvisible()!= BaseScreenData.INVISIBLE){
%> 
								<li>
									<input name='optind03' id='optind03' type='hidden'  value="<%=sv.optind03
.getFormData()%>">
	<%if (sv.optind03.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind03.getEnabled() != BaseScreenData.DISABLED){%>								
					        	<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind03"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc03.getFormData())%>
<%} %>
									<%
if (sv.optind03
.getFormData().equals("+")) {
%>

<i class="fa fa-tasks fa-fw sidebar-icon"></i>

<%}else {
	
    if (sv.optind03.getFormData().equals("X")) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind03
.getFormData().equals("X")) {
%>
<i class="fa fa-warning fa-fw sidebar-icon"></i> 
<%
}
%></a>
								</li><%} %>
								
								<%if (sv.optind04.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind04.getEnabled() != BaseScreenData.DISABLED){%>
								<li>
								<input name='optind04' id='optind04' type='hidden'  value="<%=sv.optind04
.getFormData()%>">
<%if (sv.optind04.getInvisible() != BaseScreenData.INVISIBLE
					        || sv.optind04.getEnabled() != BaseScreenData.DISABLED){%>								
					        	<a href="javascript:;" onClick='hyperLinkTo(parent.frames["mainForm"].document.getElementById("optind04"))' class="hyperLink"> 
										<%=resourceBundleHandler.gettingValueFromBundle(sv.optdsc04.getFormData())%>
 
<%} %>
									<%
if (sv.optind04
.getFormData().equals("+")) {
%>

<i class="fa fa-tasks fa-fw sidebar-icon"></i>

<%}else {
	
    if (sv.optind04.getFormData().equals("X")) {%>
<div> </div>
<% } else {%>
    <div style="width: 15px"> </div>
    
<%}
}
	if (sv.optind04
.getFormData().equals("X")) {
%>
<i class="fa fa-warning fa-fw sidebar-icon"></i> 
<%
}
%></a>
								</li><%} %>
						
							</ul>
					</span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>


<%@ include file="/POLACommon2NEW.jsp"%>