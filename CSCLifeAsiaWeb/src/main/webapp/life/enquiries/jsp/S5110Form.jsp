<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "S5110";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%S5110ScreenVars sv = (S5110ScreenVars) fw.getVariables();%>
	<%StringData generatedText0 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText30 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Group Number          ");%>
	<%StringData generatedText31 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"               ");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText16 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText17 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText29 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Member Reference No   ");%>
	<%StringData generatedText32 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"             ");%>
	<%StringData generatedText18 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText19 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText20 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText21 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText22 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText28 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Master Policy Number  ");%>
	<%StringData generatedText33 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"               ");%>
	<%StringData generatedText23 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText24 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText25 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                               ");%>
	<%StringData generatedText26 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>
	<%StringData generatedText27 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"                                                   ");%>


<div class="panel panel-default">
	 <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText30).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Group Number")%>
					<%}%></label>
		       		<div class="input-group">

							<%if ((new Byte((sv.grupnum).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
							
							<%	
								
								longValue = sv.grupnum.getFormData();  
							%>
							
							<% 
								if((new Byte((sv.grupnum).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0||(((ScreenModel) fw).getVariables().isScreenProtected())){ 
							%>  
							<div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
														"blank_cell" : "output_cell" %>' style="min-width: 120px;">  
								   		<%if(longValue != null){%>
								   		
								   		<%=longValue%>
								   		
								   		<%}%>
								   </div>
							
							<%
							longValue = null;
							%>
							<% }else {%> 
							<input name='grupnum' id='grupnum'
							type='text' 
							value='<%=sv.grupnum.getFormData()%>' 
							maxLength='<%=sv.grupnum.getLength()%>' 
							size='<%=sv.grupnum.getLength()%>'
							onFocus='doFocus(this)' onHelp='return fieldHelp(grupnum)' onKeyUp='return checkMaxLength(this)'  
							
							<% 
								if((new Byte((sv.grupnum).getEnabled()))
								.compareTo(new Byte(BaseScreenData.DISABLED)) == 0){ 
							%>  
							readonly="true"
							class="output_cell"	 >
							
							<%
								}else if((new Byte((sv.grupnum).getHighLight())).
									compareTo(new Byte(BaseScreenData.BOLD)) == 0){
								
							%>	
							class="bold_cell" >
							 
							<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('grupnum')); changeF4Image(this); doAction('PFKEY04')"> 
							<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
							</a> --%>
							
							<span class="input-group-btn">
								<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('grupnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
							
							<%
								}else { 
							%>
							
							class = ' <%=(sv.grupnum).getColor()== null  ? 
							"input_cell" :  (sv.grupnum).getColor().equals("red") ? 
							"input_cell red reverse" : "input_cell" %>' >
							
							<%-- <a href="javascript:;" style="position: relative; top:1px; left:0px"  onClick="doFocus(document.getElementById('grupnum')); changeF4Image(this); doAction('PFKEY04')"> 
							<img src="/<%= AppVars.getInstance().getContextPath() %>/screenFiles/<%=imageFolder%>/search.gif" border="0" class='iconPos'>
							</a> --%>
							
							<span class="input-group-btn">
								<button class="btn btn-info" style="font-size: 19px;" type="button" onClick="doFocus(document.getElementById('grupnum')); doAction('PFKEY04')">
								<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
								</button>
							</span>
							
							<%}longValue = null;}} %>
							
							
							
							
							
							<%if ((new Byte((sv.grupname).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.grupname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.grupname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.grupname.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
	
		       		</div>
		       		</div>
		       	</div>
		       	
		       		<div class="col-md-2">
		       		</div>
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText29).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Member Reference No")%>
					<%}%></label>
		       		<div class="input-group">
						
						<%if ((new Byte((sv.membsel).getInvisible())).compareTo(new Byte(
														BaseScreenData.INVISIBLE)) != 0) {%>
							
						  		
								<%					
								if(!((sv.membsel.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
											
													if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.membsel.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
													
													
											} else  {
														
											if(longValue == null || longValue.equalsIgnoreCase("")) {
														formatValue = formatValue( (sv.membsel.getFormData()).toString()); 
													} else {
														formatValue = formatValue( longValue);
													}
											
											}
											%>			
										<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
												"blank_cell" : "output_cell" %>' style="min-width: 80px;">
										<%=XSSFilter.escapeHtml(formatValue)%>
									</div>	
								<%
								longValue = null;
								formatValue = null;
								%>
						  <%}%>
		       		</div>
		       		</div>
		       	</div>
		       	
		       	<div class="col-md-1">
		       		</div>
		       		
		       	<div class="col-md-3">
		       		<div class="form-group">
		       		<label><%if ((new Byte((generatedText28).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>
					<%=resourceBundleHandler.gettingValueFromBundle("Master Policy Number")%>
					<%}%></label>
		       		<div class="input-group">
							<%if ((new Byte((sv.mplnum).getInvisible())).compareTo(new Byte(
															BaseScreenData.INVISIBLE)) != 0) {%>
								
							  		
									<%					
									if(!((sv.mplnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
												
														if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mplnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
														
														
												} else  {
															
												if(longValue == null || longValue.equalsIgnoreCase("")) {
															formatValue = formatValue( (sv.mplnum.getFormData()).toString()); 
														} else {
															formatValue = formatValue( longValue);
														}
												
												}
												%>			
											<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
													"blank_cell" : "output_cell" %>' style="min-width: 80px;">
											<%=XSSFilter.escapeHtml(formatValue)%>
										</div>	
									<%
									longValue = null;
									formatValue = null;
									%>
							  <%}%>
		       		</div>
		       		</div>
		       	</div>
		    </div>
		    
		   
	 </div>
</div>


<%@ include file="/POLACommon2NEW.jsp"%>

 <div style='visibility:hidden;'><table>
					<tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText31).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText11).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText12).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText13).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText14).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText16).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText17).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText32).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText18).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText20).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText21).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText22).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText23).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText24).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText25).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText26).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText27).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText0).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText7).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText8).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td><td width='188'>
					<%if ((new Byte((generatedText9).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					
					<br/>&nbsp; &nbsp; &nbsp;</td></tr><tr style='height:22px;'><td width='188'>
					<%if ((new Byte((generatedText10).getInvisible())).compareTo(new Byte(
													BaseScreenData.INVISIBLE)) != 0) { %>
					<div class="label_txt">
					<%=resourceBundleHandler.gettingValueFromBundle("")%>
					</div>
					<%}%>
					
					</tr></table></div>