<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%String screenName = "SR546";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%Sr546ScreenVars sv = (Sr546ScreenVars) fw.getVariables();%>

<%if (sv.Sr546screenWritten.gt(0)) {%>
	<%Sr546screen.clearClassString(sv);%>
	<%sv.crtabdesc.setClassString("");%>
<%	sv.crtabdesc.appendClassString("string_fld");
	sv.crtabdesc.appendClassString("output_txt");
	sv.crtabdesc.appendClassString("highlight");
%>
	<%StringData generatedText1 = new StringData("Contract ");%>
	<%sv.chdrnum.setClassString("");%>
<%	sv.chdrnum.appendClassString("string_fld");
	sv.chdrnum.appendClassString("output_txt");
	sv.chdrnum.appendClassString("highlight");
%>
	<%sv.cnttype.setClassString("");%>
<%	sv.cnttype.appendClassString("string_fld");
	sv.cnttype.appendClassString("output_txt");
	sv.cnttype.appendClassString("highlight");
%>
	<%sv.ctypedes.setClassString("");%>
<%	sv.ctypedes.appendClassString("string_fld");
	sv.ctypedes.appendClassString("output_txt");
	sv.ctypedes.appendClassString("highlight");
%>
	<%StringData generatedText2 = new StringData("Currency ");%>
	<%sv.cntcurr.setClassString("");%>
<%	sv.cntcurr.appendClassString("string_fld");
	sv.cntcurr.appendClassString("output_txt");
	sv.cntcurr.appendClassString("highlight");
%>
	<%StringData generatedText3 = new StringData("Contract Status ");%>
	<%sv.chdrstatus.setClassString("");%>
<%	sv.chdrstatus.appendClassString("string_fld");
	sv.chdrstatus.appendClassString("output_txt");
	sv.chdrstatus.appendClassString("highlight");
%>
	<%StringData generatedText4 = new StringData("Premium Status ");%>
	<%sv.premstatus.setClassString("");%>
<%	sv.premstatus.appendClassString("string_fld");
	sv.premstatus.appendClassString("output_txt");
	sv.premstatus.appendClassString("highlight");
%>
	<%StringData generatedText5 = new StringData("Register ");%>
	<%sv.register.setClassString("");%>
<%	sv.register.appendClassString("string_fld");
	sv.register.appendClassString("output_txt");
	sv.register.appendClassString("highlight");
%>
	<%StringData generatedText6 = new StringData("Number of Policies in Plan ");%>
	<%sv.numpols.setClassString("");%>
<%	sv.numpols.appendClassString("num_fld");
	sv.numpols.appendClassString("output_txt");
	sv.numpols.appendClassString("highlight");
%>
	<%StringData generatedText7 = new StringData("Policy Number ");%>
	<%sv.planSuffix.setClassString("");%>
<%	sv.planSuffix.appendClassString("num_fld");
	sv.planSuffix.appendClassString("output_txt");
	sv.planSuffix.appendClassString("highlight");
%>
	<%StringData generatedText8 = new StringData("Life Assured    ");%>
	<%sv.lifenum.setClassString("");%>
<%	sv.lifenum.appendClassString("string_fld");
	sv.lifenum.appendClassString("output_txt");
	sv.lifenum.appendClassString("highlight");
%>
	<%sv.lifename.setClassString("");%>
<%	sv.lifename.appendClassString("string_fld");
	sv.lifename.appendClassString("output_txt");
	sv.lifename.appendClassString("highlight");
%>
	<%StringData generatedText9 = new StringData("Joint Life");%>
	<%sv.jlife.setClassString("");%>
<%	sv.jlife.appendClassString("string_fld");
	sv.jlife.appendClassString("output_txt");
	sv.jlife.appendClassString("highlight");
%>
	
	<%sv.jlifename.setClassString("");%>
<%	sv.jlifename.appendClassString("string_fld");
	sv.jlifename.appendClassString("output_txt");
	sv.jlifename.appendClassString("highlight");
%>
	<%StringData generatedText10 = new StringData("Life Number");%>
	<%sv.life.setClassString("");%>
<%	sv.life.appendClassString("string_fld");
	sv.life.appendClassString("output_txt");
	sv.life.appendClassString("highlight");
%>
	<%StringData generatedText11 = new StringData("Coverage no");%>
	<%sv.coverage.setClassString("");%>
<%	sv.coverage.appendClassString("string_fld");
	sv.coverage.appendClassString("output_txt");
	sv.coverage.appendClassString("highlight");
%>
	<%StringData generatedText12 = new StringData("Rider no ");%>
	<%sv.rider.setClassString("");%>
<%	sv.rider.appendClassString("string_fld");
	sv.rider.appendClassString("output_txt");
	sv.rider.appendClassString("highlight");
%>
	<%StringData generatedText13 = new StringData("Lien Code");%>
	<%sv.liencd.setClassString("");%>
<%	sv.liencd.appendClassString("string_fld");
	sv.liencd.appendClassString("output_txt");
	sv.liencd.appendClassString("highlight");
%>
	<%sv.zagelit.setClassString("");%>
	<%sv.anbAtCcd.setClassString("");%>
<%	sv.anbAtCcd.appendClassString("num_fld");
	sv.anbAtCcd.appendClassString("output_txt");
	sv.anbAtCcd.appendClassString("highlight");
%>
	<%StringData generatedText14 = new StringData("Stat. Fund");%>
	<%sv.statFund.setClassString("");%>
<%	sv.statFund.appendClassString("string_fld");
	sv.statFund.appendClassString("output_txt");
	sv.statFund.appendClassString("highlight");
%>
	<%StringData generatedText15 = new StringData("Section");%>
	<%sv.statSect.setClassString("");%>
<%	sv.statSect.appendClassString("string_fld");
	sv.statSect.appendClassString("output_txt");
	sv.statSect.appendClassString("highlight");
%>
	<%StringData generatedText16 = new StringData("Sub-Section ");%>
	<%sv.statSubsect.setClassString("");%>
<%	sv.statSubsect.appendClassString("string_fld");
	sv.statSubsect.appendClassString("output_txt");
	sv.statSubsect.appendClassString("highlight");
%>
	<%StringData generatedText17 = new StringData("Benefit Amount");%>
	<%sv.zrsumin.setClassString("");%>
<%	sv.zrsumin.appendClassString("num_fld");
	sv.zrsumin.appendClassString("output_txt");
	sv.zrsumin.appendClassString("highlight");
%>
	<%sv.frqdesc.setClassString("");%>
<%	sv.frqdesc.appendClassString("string_fld");
	sv.frqdesc.appendClassString("output_txt");
	sv.frqdesc.appendClassString("highlight");
%>
	<%StringData generatedText18 = new StringData("Single Premium");%>
	<%sv.singlePremium.setClassString("");%>
<%	sv.singlePremium.appendClassString("num_fld");
	sv.singlePremium.appendClassString("output_txt");
	sv.singlePremium.appendClassString("highlight");
%>
	<%StringData generatedText19 = new StringData("Premium Cess Date  ");%>
	<%sv.premcessDisp.setClassString("");%>
<%	sv.premcessDisp.appendClassString("string_fld");
	sv.premcessDisp.appendClassString("output_txt");
	sv.premcessDisp.appendClassString("highlight");
%>
	<%sv.zdesc.setClassString("");%>
	<%sv.zlinstprem.setClassString("");%>
<%	sv.zlinstprem.appendClassString("num_fld");
	sv.zlinstprem.appendClassString("output_txt");
	sv.zlinstprem.appendClassString("highlight");
%>
	<%StringData generatedText20 = new StringData("Total Premium");%>
	<%sv.instPrem.setClassString("");%>
<%	sv.instPrem.appendClassString("num_fld");
	sv.instPrem.appendClassString("output_txt");
	sv.instPrem.appendClassString("highlight");
%>
	<%StringData generatedText31 = new StringData("Total Premium with Tax ");%>
	<%sv.taxamt.setClassString("");%>
<%	sv.taxamt.appendClassString("num_fld");
	sv.taxamt.appendClassString("output_txt");
	sv.taxamt.appendClassString("highlight");
%>
	<%StringData generatedText21 = new StringData("Coverage RCD");%>
	<%sv.crrcdDisp.setClassString("");%>
<%	sv.crrcdDisp.appendClassString("string_fld");
	sv.crrcdDisp.appendClassString("output_txt");
	sv.crrcdDisp.appendClassString("highlight");
%>
	<%StringData generatedText22 = new StringData("Risk Cess Date");%>
	<%sv.riskCessDateDisp.setClassString("");%>
<%	sv.riskCessDateDisp.appendClassString("string_fld");
	sv.riskCessDateDisp.appendClassString("output_txt");
	sv.riskCessDateDisp.appendClassString("highlight");
%>
	<%StringData generatedText23 = new StringData("Ann Proc Date");%>
	<%sv.annivProcDateDisp.setClassString("");%>
<%	sv.annivProcDateDisp.appendClassString("string_fld");
	sv.annivProcDateDisp.appendClassString("output_txt");
	sv.annivProcDateDisp.appendClassString("highlight");
%>
	<%StringData generatedText24 = new StringData("Rerate Date");%>
	<%StringData generatedText29 = new StringData(" ");%>
	<%sv.rerateDateDisp.setClassString("");%>
<%	sv.rerateDateDisp.appendClassString("string_fld");
	sv.rerateDateDisp.appendClassString("output_txt");
	sv.rerateDateDisp.appendClassString("highlight");
%>
	<%StringData generatedText25 = new StringData("Rerate From Date");%>
	<%sv.rerateFromDateDisp.setClassString("");%>
<%	sv.rerateFromDateDisp.appendClassString("string_fld");
	sv.rerateFromDateDisp.appendClassString("output_txt");
	sv.rerateFromDateDisp.appendClassString("highlight");
%>
	<%StringData generatedText26 = new StringData("Benefit Bill Date   ");%>
	<%sv.benBillDateDisp.setClassString("");%>
<%	sv.benBillDateDisp.appendClassString("string_fld");
	sv.benBillDateDisp.appendClassString("output_txt");
	sv.benBillDateDisp.appendClassString("highlight");
%>
	<%StringData generatedText27 = new StringData("Mortality Class ");%>
	<%StringData generatedText30 = new StringData(" ");%>
	<%sv.mortcls.setClassString("");%>
<%	sv.mortcls.appendClassString("string_fld");
	sv.mortcls.appendClassString("output_txt");
	sv.mortcls.appendClassString("highlight");
%>
	<%StringData generatedText28 = new StringData("Bonus Appl. Method ");%>
	<%sv.bappmeth.setClassString("");%>
<%	sv.bappmeth.appendClassString("string_fld");
	sv.bappmeth.appendClassString("output_txt");
	sv.bappmeth.appendClassString("highlight");
%>
	<%StringData generatedText32 = new StringData("Basic Premium");%>
	<%sv.zbinstprem.setClassString("");%>
<%	sv.zbinstprem.appendClassString("string_fld");
	sv.zbinstprem.appendClassString("output_txt");
	sv.zbinstprem.appendClassString("highlight");
%>
	<%StringData generatedText33 = new StringData("Stamp Duty");%>
	<%sv.zstpduty01.setClassString("");%>
<%	sv.zstpduty01.appendClassString("string_fld");
	sv.zstpduty01.appendClassString("output_txt");
	sv.zstpduty01.appendClassString("highlight");
%>
	<%sv.optdsc01.setClassString("");%>
<%	sv.optdsc01.appendClassString("string_fld");
	sv.optdsc01.appendClassString("output_txt");
	sv.optdsc01.appendClassString("highlight");
%>
	<%sv.optind01.setClassString("");%>
	<%sv.optdsc02.setClassString("");%>
<%	sv.optdsc02.appendClassString("string_fld");
	sv.optdsc02.appendClassString("output_txt");
	sv.optdsc02.appendClassString("highlight");
%>
	<%sv.optind02.setClassString("");%>
	<%sv.optdsc03.setClassString("");%>
<%	sv.optdsc03.appendClassString("string_fld");
	sv.optdsc03.appendClassString("output_txt");
	sv.optdsc03.appendClassString("highlight");
%>
	<%sv.optind03.setClassString("");%>
	<%sv.screenRow.setClassString("");%>
	<%sv.screenColumn.setClassString("");%>

	<%
{
		if (appVars.ind33.isOn()) {
			generatedText7.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind33.isOn()) {
			sv.planSuffix.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind06.isOn()) {
			sv.zagelit.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind10.isOn()) {
			generatedText26.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind10.isOn()) {
			sv.benBillDateDisp.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.optdsc01.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind02.isOn()) {
			sv.optind01.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind01.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind05.isOn()) {
			sv.optind01.setReverse(BaseScreenData.REVERSED);
			sv.optind01.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind05.isOn()) {
			sv.optind01.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind03.isOn()) {
			sv.optdsc02.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind03.isOn()) {
			sv.optind02.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind02.setEnabled(BaseScreenData.DISABLED);
		}
		if (appVars.ind04.isOn()) {
			sv.optind02.setReverse(BaseScreenData.REVERSED);
			sv.optind02.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind04.isOn()) {
			sv.optind02.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind07.isOn()) {
			generatedText28.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind07.isOn()) {
			sv.bappmeth.setInvisibility(BaseScreenData.INVISIBLE);
		}
		if (appVars.ind01.isOn()) {
			sv.zdesc.setHighLight(BaseScreenData.BOLD);
		}
		if (appVars.ind42.isOn()) {
			generatedText31.setInvisibility(BaseScreenData.INVISIBLE);
	}
		if (appVars.ind42.isOn()) {
			sv.taxamt.setInvisibility(BaseScreenData.INVISIBLE);
					}
		if (appVars.ind43.isOn()) {
			sv.optdsc03.setInvisibility(BaseScreenData.INVISIBLE);
							}
		if (appVars.ind42.isOn()) {
			sv.optind03.setInvisibility(BaseScreenData.INVISIBLE);
			sv.optind03.setEnabled(BaseScreenData.DISABLED);
							}
		if (appVars.ind44.isOn()) {
			sv.optind03.setReverse(BaseScreenData.REVERSED);
			sv.optind03.setColor(BaseScreenData.RED);
					}
		if (!appVars.ind42.isOn()) {
			sv.optind03.setHighLight(BaseScreenData.BOLD);
					}
			
	/*BRD-306 START */
	if (appVars.ind32.isOn()) {
		sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind32.isOn()) {
		sv.loadper.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind32.isOn()) {
		sv.rateadj.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind32.isOn()) {
		sv.fltmort.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind32.isOn()) {
		sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind32.isOn()) {
		sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	/*BRD-306 END */
	if (appVars.ind54.isOn()) {
			sv.zstpduty01.setInvisibility(BaseScreenData.INVISIBLE);
			generatedText33.setInvisibility(BaseScreenData.INVISIBLE);
		}
	
	
	if (appVars.ind55.isOn()) {
		sv.waitperiod.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind56.isOn()) {
		sv.bentrm.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind57.isOn()) {
		sv.poltyp.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind58.isOn()) {
		sv.prmbasis.setInvisibility(BaseScreenData.INVISIBLE);
	}
	//ILIFE-3399-STARTS
	if (appVars.ind59.isOn()) {
		sv.adjustageamt.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind52.isOn()) {
		sv.premadj.setInvisibility(BaseScreenData.INVISIBLE);
	}
	if (appVars.ind60.isOn()) {
		sv.zbinstprem.setInvisibility(BaseScreenData.INVISIBLE);
	}
	//ILIFE-3399-ENDS
	//BRD-NBP-011 starts
	if (appVars.ind72.isOn()) {
		sv.dialdownoption.setReverse(BaseScreenData.REVERSED);
		sv.dialdownoption.setColor(BaseScreenData.RED);
	}
	if (!appVars.ind72.isOn()) {
		sv.dialdownoption.setHighLight(BaseScreenData.BOLD);
	}
	if (appVars.ind70.isOn()) {
		sv.dialdownoption.setEnabled(BaseScreenData.DISABLED);
	}
	if (appVars.ind71.isOn()) {
		sv.dialdownoption.setInvisibility(BaseScreenData.INVISIBLE);
	}
	//BRD-NBP-011 ends
	//ILJ-45 - START
	if (appVars.ind77.isOn()) {
		sv.crrcdDisp.setInvisibility(BaseScreenData.INVISIBLE);
	}
	/* if (appVars.ind78.isOn()) {
		sv.riskCessAge.setInvisibility(BaseScreenData.INVISIBLE);
	} */
	if (appVars.ind79.isOn()) {
		sv.riskCessDateDisp.setEnabled(BaseScreenData.DISABLED);
	}
	//ILJ-45 - END

}
		%>
<!-- <style>
@media \0screen\,screen\9
{
.output_cell{margin-left:1px}
.bold_cell{margin-left:1px}
.blank_cell{margin-left:1px}
}
</style> -->
<div class="panel panel-default">
<div class="panel-body"> 
   
			 <div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
    	 					
    	 					<table><tr><td>
    	 					
    	 <%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
	 		
		
  </td><td>
	





	
  		
		<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
</td><td>




	
  		
		<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="max-width:500px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 					</td></tr></table></div></div>
    	 					
    	 					</div>
    	 					
    	 					
    	 					
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
    	 					

	<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:110px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>

		<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:110px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>

<%
longValue = null;
formatValue=null;
%>


<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
	%>
  		
		<%					
		if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cntcurr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:140px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>	
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>

	<%	
				fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
				mappedItems = (Map) fieldItem.get("register");
				optionValue = makeDropDownList( mappedItems , sv.register.getFormData(),1,resourceBundleHandler);  
				longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
			%>
  		
		<%					
		if(!((sv.register.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.register.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:110px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">	
			    	     <%if ((new Byte((generatedText6).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Number of Policies in Plan")%></label>
    	 					<%} %>

  	
<%if ((new Byte((sv.numpols).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	
 <%}%>
	
	
				
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Policy Number")%></label>

		<%					
		if(!((sv.numpols.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.numpols.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'  style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
 
					
    	 				</div></div>
    	 				
    	 				</div>	
    
    	 					
    	 					
    	 					
    	 				
    	 					<div class="row">	
			    	<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Number")%></label>
    	 					

	<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	

    	 					
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Coverage no")%></label>

	<%					
		if(!((sv.coverage.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.coverage.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rider no")%></label>

	<%					
		if(!((sv.rider.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rider.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Stat. Fund")%></label>

<%	
	fieldItem=appVars.loadF4FieldsShort(new String[] {"statFund"},sv,"E",baseModel);
	mappedItems = (Map) fieldItem.get("statFund");
	longValue = (String) mappedItems.get((sv.statFund.getFormData()).toString());  
%>


	 <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
							"blank_cell" : "output_cell" %>' style="width:100px;">  
	   		<%if(longValue != null){%>
	   		
	   		<%=longValue%>
	   		
	   		<%}%>
	   </div>
   		<%
		longValue = null;
		formatValue = null;
		%>
   
  				
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">	
			    	     <%if ((new Byte((generatedText15).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) { %>	    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Section")%></label>
    	 					
    	 					<%} %>
    	 					
   
<%if ((new Byte((sv.statSect).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.statSect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:50px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
    	 					
    	 				</div></div>
    	 				
    	 				
    	 				
    	 				<div class="col-md-2"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Sub-section")%></label>


<%if ((new Byte((sv.statSubsect).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.statSubsect.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.statSubsect.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:90px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
	

    	 					
    	 				</div></div>
    	 				
    	 				</div>
    	 					 					
    	 					<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Life Assured")%></label>
    	 					<table><tr><td>

<%if ((new Byte((sv.life).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.life.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.life.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'style="min-width: 66px">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  
	</td><td>
  		
		<%					
		if(!((sv.lifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.lifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:100px;" : "width:240px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 					</td></tr></table></div></div>
    	 					
    	 					
    	 					
    	 					<div class="col-md-4"> </div>
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-4"> 
			    	     <div class="form-group">
						
    	 					
    	 <%					
		if(!((sv.zagelit.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.zagelit.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				 
				 <div id='zagelit' class='label_txt'class='label_txt'  
				onHelp='return fieldHelp("zagelit")'><%=resourceBundleHandler.gettingValueFromBundle(sv.zagelit.getFormData())%></div>
		<%
		longValue = null;
		formatValue = null;
		%>
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.anbAtCcd).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.anbAtCcd);
			
			if(!((sv.anbAtCcd.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:50px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:50px;" />
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					</div>
    	 					
    	 	
    	 		<div class="row">	
			    	<div class="col-md-4"> 
			    	     <div class="form-group">
			    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Joint life")%></label>
    	 					<table><tr><td>

		<%if ((new Byte((sv.jlife).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
	
  		
		<%					
		if(!((sv.jlife.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlife.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:65px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  <%}%>
  		
	</td><td>	
  
	
		<%					
		if(!((sv.jlifename.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.jlifename.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:100px;" : "width:240px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

    	 					
    	 					
    	 					</td></tr></table></div></div></div>
    	 				           
    	 					
    	 					  	 					

    	 					

    	 					
    	 					<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#contact_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Risk and Premium")%></label></a>
						</li>
						<li><a href="#other_tab" data-toggle="tab"><label><%=resourceBundleHandler.gettingValueFromBundle("Processing Details")%></label></a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="contact_tab">
								<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Amount")%></label>

  		
		       		 
			<table>
			<tr>
			<td>
		
  		
		<%	
			qpsf = fw.getFieldXMLDef((sv.zrsumin).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zrsumin,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.zrsumin.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:140px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		<td>
		<%					
		if(!((sv.frqdesc.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.frqdesc.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left: 1px;" >     <!-- removed "style="width:70px;" --> <!-- ILIFE-3856 -->
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
		</td>
		</tr>
		</table>
</div>
</div>
    	 				
    	 			<div class="col-md-3"> </div>
    	 			
    	 			
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     
			    	     	<%
									//ILJ-386
								if (sv.cntEnqScreenflag.compareTo("Y") == 0) {
							%>
								<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract Date"))%></label>
							<%
								} else if ((new Byte((sv.crrcdDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0) {
							%>
								<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage Start Date"))%></label>
		        			<%} else { %>
		        				<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Coverage RCD"))%></label>
		        			<%} %>
		        			<!-- ILJ-45 End -->	


			<%	
			qpsf = fw.getFieldXMLDef((sv.crrcdDisp).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.crrcdDisp);
			
			if(!((sv.crrcdDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:142px;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"/>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
    	 				
    	 			</div></div>	
    	 				
    	 				
    
    
    <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Mortality class")%></label>	

		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"mortcls"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("mortcls");
		longValue = (String) mappedItems.get((sv.mortcls.getFormData()).toString().trim());
		%>
	
  		
		<%					
		if(!((sv.mortcls.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.mortcls.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:141px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  			
    	 					</div></div>
    	 		
    	</div>
    	 					

                  <div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<!-- ILJ-45 Starts -->
			               	<%-- <% if ((new Byte((sv.riskCessAge).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
			               	<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
				        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Contract cess Age / Term"))%></label>
				        	<%} else { %>
				        		<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk cess Age / Term"))%></label>
				        	<%} %>
				        	<!-- ILJ-45 End -->
<!-- <div class="input-group"> -->

			    	<table>
			    	<tr>
			    	<td>
<%if(((BaseScreenData)sv.riskCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.riskCessAge,( sv.riskCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.riskCessAge) instanceof DecimalData){%>
<%if(sv.riskCessAge.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

</td>
<td style="padding-left: 1px;">


<%if(((BaseScreenData)sv.riskCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.riskCessTerm,( sv.riskCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.riskCessTerm) instanceof DecimalData){%>
<%if(sv.riskCessTerm.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.riskCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%> 	</td>
</tr>
</table>	</div>			
    	 					</div>
    	 					
    	 					
    	 					
    	 					
    	 					
<%if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<!-- ILJ-45 Starts -->

							<%-- <% if ((new Byte((sv.riskCessDateDisp).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %> --%>
							<% if (sv.contDtCalcScreenflag.compareTo("Y") == 0){ %>
								<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Cessation Date")%></label>

							<%} else { %>
								<label><%=smartHF.getLabel(resourceBundleHandler.gettingValueFromBundle("Risk Cess date"))%></label>
							<%} %>
							<!-- ILJ-45 End -->
<div class="input-group">

	<%
		longValue = null;
		formatValue = null;
		%>

	
  		
		<%					
		if(!((sv.riskCessDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.riskCessDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:140px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>

    	 					</div></div></div>   <%}%>
  
    	 					
    	 					
    	 					
    	 					
    	 					
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Lien code")%></label>
    	 					
	
		<%					
		if(!((sv.liencd.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.liencd.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:145px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
	
				
    	 					
    	 					</div></div>
    	 					
    	 					
    	 					
    	 					
    	 				
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Single Premium")%></label>

	<%	
			qpsf = fw.getFieldXMLDef((sv.singlePremium).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.singlePremium,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.singlePremium.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:140px;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:82px;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
	
 
    	 					
    	 					</div></div>
 					
    	 					</div>
    	 					
    	 					
    	 					
    	 				 	 					  	 					
  	 					<div class="row">	
			    	<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Prem cess Age / Term")%></label>
<table>
<tr>
<td>
<%if(((BaseScreenData)sv.premCessAge) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessAge,( sv.premCessAge.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessAge) instanceof DecimalData){%>
<%if(sv.premCessAge.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'output_cell \' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessAge, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

</td>
<td>

	<%if(((BaseScreenData)sv.premCessTerm) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premCessTerm,( sv.premCessTerm.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premCessTerm) instanceof DecimalData){%>
<%if(sv.premCessTerm.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 40px; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premCessTerm, COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width: 40px; text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</td>
</tr>
</table>
    	 				
    	 				</div></div>	
    	 				
    	 				
    	 				
    	 				
    	 					
    	 					
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Cess Date")%></label>
<div class="input-group">
	<%					
		if(!((sv.premcessDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premcessDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:142px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				</div></div></div>
    	 				
    	 				
 
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">
			    	      <%if ((new Byte((sv.prmbasis).getInvisible())).compareTo(new Byte( //ILIFE-3517
		BaseScreenData.INVISIBLE)) != 0) { %>		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Basis")%></label>
	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"prmbasis"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("prmbasis");
		longValue = (String) mappedItems.get((sv.prmbasis.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.prmbasis.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.prmbasis.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.prmbasis.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  

<%}%>
    	 				
    	 				</div></div>
    	 				
    	 			<%if ((new Byte((sv.dialdownoption).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0) { %>
    	 				<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Dial Down Option")%></label>

	
		<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"dialdownoption"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("dialdownoption");
		longValue = (String) mappedItems.get((sv.dialdownoption.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.dialdownoption.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dialdownoption.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.dialdownoption.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  
    	 				</div></div>	<%}%>	
    	 					
    	 					
    	 					</div>
    	 					
    	 					<div class="row">
    	 					
    	 					<%if ((new Byte((sv.waitperiod).getInvisible())).compareTo(new Byte(   //ILIFE-3517
		BaseScreenData.INVISIBLE)) != 0) { %>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Waiting Period")%></label>
			    	      <div class="input-group">
			    	      <%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"waitperiod"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("waitperiod");
		longValue = (String) mappedItems.get((sv.waitperiod.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.waitperiod.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.waitperiod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.waitperiod.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  
			    	      
			    	     </div></div></div><%} %>
			    	     
			    	     <%if ((new Byte((sv.bentrm).getInvisible())).compareTo(new Byte(  //ILIFE-3517
		BaseScreenData.INVISIBLE)) != 0) {%>
			    	     <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Benefit Term")%></label>
			    	      <div class="input-group">
			    	      	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"bentrm"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("bentrm");
		longValue = (String) mappedItems.get((sv.bentrm.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.bentrm.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bentrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bentrm.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  
			    	      
			    	      
			    	     </div></div></div><%} %>
			    	     
			    	     
			    	     <%if ((new Byte((sv.poltyp).getInvisible())).compareTo(new Byte(  //ILIFE-3517
		BaseScreenData.INVISIBLE)) != 0) { %>
			    	     <div class="col-md-3"> 
			    	     <div class="form-group">
			    	     <label><%=resourceBundleHandler.gettingValueFromBundle("Policy Type")%></label>
			    	      <div class="input-group">
			    	      
			    	      	<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"poltyp"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("poltyp");
		longValue = (String) mappedItems.get((sv.poltyp.getFormData()).toString().trim());  
	%>
	
  		
		<%					
		if(!((sv.poltyp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.poltyp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.poltyp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:82px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>		
		<%
		longValue = null;
		formatValue = null;
		%>
  
			    	     </div></div></div><%} %>
    	 					
    	 					</div>
    	 					
    	 					
<hr>

<%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %> 
								 <div class="row">
<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Age Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.adjustageamt) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.adjustageamt,( sv.adjustageamt.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.adjustageamt) instanceof DecimalData){%>
<%if(sv.adjustageamt.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.adjustageamt, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

                        </div></div><%}%>
                        
                      <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Rate Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.rateadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.rateadj,( sv.rateadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.rateadj) instanceof DecimalData){%>
<%if(sv.rateadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.rateadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%}%>
                        
                        
                     <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Flat Mortality Amount")%></label>
<%if(((BaseScreenData)sv.fltmort) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.fltmort,( sv.fltmort.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.fltmort) instanceof DecimalData){%>
<%if(sv.fltmort.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.fltmort, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%></div></div><%}%>
                        
                        
                    <%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
                        <div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Load Amount")%></label>
<%if(((BaseScreenData)sv.loadper) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.loadper,( sv.loadper.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.loadper) instanceof DecimalData){%>
<%if(sv.loadper.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.loadper, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
                        </div></div><%}%>
                        
                        
                        
                        </div>  <%}%> 	 					
    	 					
    	 					
    	 					
    	 					
    	 					
 	 					
    	 					 <div class="row">
    	 					<%if((new Byte((sv.premadj).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
	<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Premium Adjusted Amount")%></label>
<%if(((BaseScreenData)sv.premadj) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.premadj,( sv.premadj.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.premadj) instanceof DecimalData){%>
<%if(sv.premadj.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.premadj, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'output_cell \' ","class=\'output_cell \' style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>

    	 					
    	 					</div></div><%}}%> 
    	 					
    	 					
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		
			    	     <%if((new Byte((sv.adjustageamt).getInvisible())).compareTo(new Byte(BaseScreenData.INVISIBLE)) != 0){ %>     	     
    	 						<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Loaded Premium")%></label>
    	 				 <%} else { %>
    	 				 			<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Loaded Premium")%></label>
    	 				 <%}%>		

<%	
			qpsf = fw.getFieldXMLDef((sv.zlinstprem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.zlinstprem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.zlinstprem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:145px !important;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:145px !important;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
    	 					</div></div>
    	 					
    	 					
    	 			<%if((new Byte((sv.zbinstprem).getInvisible())).compareTo(new Byte(
		BaseScreenData.INVISIBLE)) != 0){ %>
		<%if ((new Byte((sv.zlinstprem).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Basic Premium")%></label>
<%if(((BaseScreenData)sv.zbinstprem) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zbinstprem,( sv.zbinstprem.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zbinstprem) instanceof DecimalData){%>
<%if(sv.zbinstprem.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width:145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zbinstprem, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("<div","<div style=\'width:145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
    	 					
    	 					</div></div>	<%} }%> 
    	 					
    	 			<%if ((new Byte((generatedText33).getInvisible())).compareTo(new Byte(
								BaseScreenData.INVISIBLE)) != 0) {%>
    	 			<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Stamp Duty")%></label>
    	 			<%if(((BaseScreenData)sv.zstpduty01) instanceof StringBase) {%>
<%=smartHF.getRichText(0,0,fw,sv.zstpduty01,( sv.zstpduty01.getLength()+1),null).replace("absolute","relative")%>
<%}else if (((BaseScreenData)sv.zstpduty01) instanceof DecimalData){%>
<%if(sv.zstpduty01.equals(0)) {%>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("class=\'blank_cell\' ","class=\'blank_cell\' style=\'width: 145px !important; \' ") %>
<%} else { %>
<%=smartHF.getHTMLVar(0, 0, fw, sv.zstpduty01, COBOLHTMLFormatter.COMMA_DECIMAL_MINUSAFTER_ZEROSUPPRESS).replace("<div","<div style=\'width: 145px !important;  text-align: right;\' ")%>
<%} %>
<%}else {%>
<%}%>
</div></div><%} %> </div>
    	 					<div class="row">
    	 					<div class="col-md-3"> 
			    	     <div class="form-group">		    	     
    	 					<label  style="white-space: nowrap;"><%=resourceBundleHandler.gettingValueFromBundle("Total Premium")%></label>
    	 					
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.instPrem).getFieldName());
			//qpsf.setPicinHTML(COBOLHTMLFormatter.DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.instPrem,COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			
			if(!((sv.instPrem.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:145px !important;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:145px !important;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
						</div></div>
    	 					
    	 					
    	 						<div class="col-md-3"> 
			    	     <div class="form-group">		    	
			    	     <% //ILJ-387
						if (sv.cntEnqScreenflag.compareTo("N") == 0){ 
								%>      
    	 					<label><%=resourceBundleHandler.gettingValueFromBundle("Total Premium with Tax")%></label>
    	 					
		
		<%	
			qpsf = fw.getFieldXMLDef((sv.taxamt).getFieldName());
			qpsf.setPicinHTML(COBOLHTMLFormatter.COMMA_DECIMAL_NOSIGN_ZEROSUPPRESS);
			formatValue = smartHF.getPicFormatted(qpsf,sv.taxamt);
			
			if(!((sv.taxamt.getFormData()).toString()).trim().equalsIgnoreCase("")) {
					if(longValue == null || longValue.equalsIgnoreCase("")) { 			
					formatValue = formatValue( formatValue );
					} else {
					formatValue = formatValue( longValue );
					}
			}
	
			if(!formatValue.trim().equalsIgnoreCase("")) {
		%>
				<div class="output_cell" style="width:145px !important;text-align: right;">	
					<%= XSSFilter.escapeHtml(formatValue)%>
				</div>
		<%
			} else {
		%>
		
				<div class="blank_cell" style="width:145px !important;"></div>
		
		<% 
			} 
		%>
		<%
		longValue = null;
		formatValue = null;
		%>
				<% 
			} 
		%>
						</div></div>
    	 					
    	 					
    	 					
    	 					</div>  
    	 					
    	 					
    	 					
    	 				
    	 				</div>	
    	 				
    	 				
    	 				
    	 				
								
								
							<!-- </div> -->
						
						
						
						<div class="tab-pane fade" id="other_tab" >
						
						<div class="row">	
			    	<div class="col-md-4"> 
				    		<div class="form-group">  	  
					    		<label><%=resourceBundleHandler.gettingValueFromBundle("Ann Proc Date")%></label>
					    		     <div class="input-group">
						    			
		<%					
		if(!((sv.annivProcDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.annivProcDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
				    		</div>
					</div>
				    		
				    		
				    <div class="col-md-4">
						<div class="form-group">	
							<label><%=resourceBundleHandler.gettingValueFromBundle("Rerate Date")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.rerateDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
	

				      			     </div>
						</div>
				   </div>		
			
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Rerate from Date")%></label>
							<div class="input-group">
						    				
		<%					
		if(!((sv.rerateFromDateDisp.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.rerateFromDateDisp.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="width:80px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  

				      			     </div>
										
						</div>
				   </div>	
		    </div>
			
			
			
			<div class="row">	
			    	
			    	<div class="col-md-4">
						<div class="form-group">	
						    <label><%=resourceBundleHandler.gettingValueFromBundle("Bonus Appl.Method")%></label>
							<div class="input-group">
						    		<%					
		if(!((sv.bappmeth.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bappmeth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.bappmeth.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'
					style='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"width:80px;" : "width:140px;" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  	

				      			     </div>
										
						</div>
				   </div>	
		    </div>
						
						
						
						
						
						
						
						
							
													</div></div>
												</div>
											</div>
    	 					 
    	 					
<Div id='mainForm_OPTS' style='visibility:hidden;'>

 <%=smartHF.getMenuLink(sv.optind02, resourceBundleHandler.gettingValueFromBundle("Special Terms"))%>
 <%=smartHF.getMenuLink(sv.optind03, resourceBundleHandler.gettingValueFromBundle("Tax Details"))%>
<%=smartHF.getMenuLink(sv.optind01, resourceBundleHandler.gettingValueFromBundle(sv.optdsc03.getFormData().toString())) %>
<%=smartHF.getMenuLink(sv.optind04, resourceBundleHandler.gettingValueFromBundle(sv.optdsc04.getFormData().toString())) %>


</div> 

</div></div>


	
<%}%>

<%if (sv.Sr546protectWritten.gt(0)) {%>
	<%Sr546protect.clearClassString(sv);%>
  		
		<%					
{
							}
					
					%>			


<%}%>


<%@ include file="/POLACommon2NEW.jsp"%>
