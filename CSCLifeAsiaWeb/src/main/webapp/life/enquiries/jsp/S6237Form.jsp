    <%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%String screenName = "S6237";%>
<%@ include file="/POLACommon1NEW.jsp"%>
<%@ page import="com.csc.life.enquiries.screens.*" %>
<%--=smartHF.getHTMLFormFunctionButtons(fw.getFormActions())--%>
<%S6237ScreenVars sv = (S6237ScreenVars) fw.getVariables();%>
	<%StringData generatedText15 = resourceBundleHandler.gettingValueFromBundle(StringData.class," ");%>

<%{
		if (appVars.ind01.isOn()) {
			sv.select.setReverse(BaseScreenData.REVERSED);
			sv.select.setColor(BaseScreenData.RED);
		}
		if (!appVars.ind01.isOn()) {
			sv.select.setHighLight(BaseScreenData.BOLD);
		}
	}

	%>

	<%StringData generatedText2 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract ");%>
	<%StringData generatedText3 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Currency ");%>
	<%StringData generatedText4 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Contract Status ");%>
	<%StringData generatedText5 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Premium Status ");%>
	<%StringData generatedText6 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Register ");%>
	<%StringData generatedText7 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Servicing Agent ");%>
	<%StringData generatedText8 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Branch          ");%>
	<%StringData generatedText9 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Select 1 - Initial Commission Status,  2 - Agent Details ");%>
	<%StringData generatedText12 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Sel");%>
	<%StringData generatedText13 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Agent");%>
	<%StringData generatedText14 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Name");%>
	<%StringData generatedText10 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"Element");%>
	<%StringData generatedText11 = resourceBundleHandler.gettingValueFromBundle(StringData.class,"(Cov/Rider/Plan/O-Cat)");%>
<%		appVars.rollup(new int[] {93});
%>


	<div class="panel panel-default">
    	<div class="panel-body">
			<div class="row">        
					<div class="col-md-4">
						<div class="form-group"> 
							<label><%=resourceBundleHandler.gettingValueFromBundle("Contract")%></label>
							<table><tr><td>
								<%					
		if(!((sv.chdrnum.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrnum.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
									
									</td>
									<td >															
								<%					
		if(!((sv.cnttype.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.cnttype.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:2px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
								

									</td>
									<td >
								
								<%					
		if(!((sv.ctypedes.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.ctypedes.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="margin-left:2px; min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
								
																													
							</td></tr></table>							
						</div>
					</div>
					
					<div class="col-md-4"></div>
					<div class="col-md-4">
					<div class="form-group"> 
					
							<label><%=resourceBundleHandler.gettingValueFromBundle("Register")%></label>
							<div class="input-group" style="width:100px;"> 
							<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"register"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("register");
		longValue = (String) mappedItems.get((sv.register.getFormData()).toString().trim());  
	%><!--
	
    
   <div class='<%= ((sv.register.getFormData() == null) || ("".equals((sv.register.getFormData()).trim()))) ? 
						"blank_cell" : "output_cell" %>'> 
   <%=	(sv.register.getFormData()).toString()%>
   </div>
   
   --><div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>  
   		<%if(longValue != null){%>
   		<%=longValue%>
   		<%}%>
   </div>
   
   
  		<%
		longValue = null;
		formatValue = null;
		%>		
				</div>				
					
					
					
					
					
					
					
					
					
			</div></div></div>
			 
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Contract Status")%></label>
						<div class="input-group" style="width:100px;">
						<%					
		if(!((sv.chdrstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.chdrstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>		
					</div>
				</div></div>
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Premium Status")%></label>
						<div class="input-group" style="width:100px;">
						<%					
		if(!((sv.premstatus.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.premstatus.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
		
					</div></div>
				</div>	
				<div class="col-md-4">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Currency")%></label>
						<div class="input-group" style="width:100px;">
						<%	
		fieldItem=appVars.loadF4FieldsLong(new String[] {"cntcurr"},sv,"E",baseModel);
		mappedItems = (Map) fieldItem.get("cntcurr");
		longValue = (String) mappedItems.get((sv.cntcurr.getFormData()).toString().trim());  
	%>
	
    
   <div class='<%= ((sv.cntcurr.getFormData() == null) || ("".equals((sv.cntcurr.getFormData()).trim()))) ? 
						"blank_cell" : "output_cell" %>'> 
   <%=	(sv.cntcurr.getFormData()).toString()%>
   </div><!--
   
   <div class='<%= ((longValue == null)||("".equals(longValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>  
   		<%if(longValue != null){%>
   		<%=longValue%>
   		<%}%>
   </div>
   
   
  		--><%
		longValue = null;
		formatValue = null;
		%>
	
			
				</div>										
			</div>			 </div>
			 
			<div class="row">        
				<div class="col-md-4">
					<div class="form-group">
						<label><%=resourceBundleHandler.gettingValueFromBundle("Servicing Agent")%></label>
						<table><tr><td>
								
		<%					
		if(!((sv.servagnt.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnt.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	


					  		
					  		</td><td style="min-width:1px">
					  		</td><td>
								<%					
		if(!((sv.servagnam.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servagnam.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>' style="min-width:100px;">
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  
	
							</td></tr></table>			
					</div>
				</div>
				 
				<div class="col-md-3">
					<div class="form-group" >
						<label><%=resourceBundleHandler.gettingValueFromBundle("Branch")%></label>
						<div class="input-group" style="width:100px;">
						<%					
		if(!((sv.servbr.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servbr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servbr.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<!--<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		--><%
		longValue = null;
		formatValue = null;
		%>
  
	





	
  		
		<%					
		if(!((sv.servbrname.getFormData()).toString()).trim().equalsIgnoreCase("")) { 	
					
							if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
							
							
					} else  {
								
					if(longValue == null || longValue.equalsIgnoreCase("")) {
								formatValue = formatValue( (sv.servbrname.getFormData()).toString()); 
							} else {
								formatValue = formatValue( longValue);
							}
					
					}
					%>			
				<div class='<%= ((formatValue == null)||("".equals(formatValue.trim()))) ? 
						"blank_cell" : "output_cell" %>'>
				<%=XSSFilter.escapeHtml(formatValue)%>
			</div>	
		<%
		longValue = null;
		formatValue = null;
		%>
  		
					</div>
				</div>	
								</div>		
			</div>		
			<br>
			<%
/* This block of jsp code is to calculate the variable width of the table at runtime.*/
int[] tblColumnWidth = new int[7];
int totalTblWidth = 0;
int calculatedValue =0;

														if(resourceBundleHandler.gettingValueFromBundle("Header1").length() >= (sv.select.getFormData()).length() ) {
							calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header1").length())*12;								
						} else {		
							calculatedValue = (sv.select.getFormData()).length()*19;								
						}					
														totalTblWidth += calculatedValue;
		tblColumnWidth[0]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header2").length() >= (sv.comagnt.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header2").length())*16;								
			} else {		
				calculatedValue = (sv.comagnt.getFormData()).length()*-35;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[1]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header3").length() >= (sv.agntname.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header3").length())*8;								
			} else {		
				calculatedValue = (sv.agntname.getFormData()).length()*19;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[2]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header4").length() >= (sv.coverage.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header4").length())*8;								
			} else {		
				calculatedValue = (sv.coverage.getFormData()).length()*25;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[3]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header5").length() >= (sv.rider.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header5").length())*8;								
			} else {		
				calculatedValue = (sv.rider.getFormData()).length()*25;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[4]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header6").length() >= (sv.planSuffix.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header6").length())*10;								
			} else {		
				calculatedValue = (sv.planSuffix.getFormData()).length()*30;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[5]= calculatedValue;
			
						if(resourceBundleHandler.gettingValueFromBundle("Header7").length() >= (sv.ovrdcat.getFormData()).length() ) {
				calculatedValue = (resourceBundleHandler.gettingValueFromBundle("Header7").length())*8;								
			} else {		
				calculatedValue = (sv.ovrdcat.getFormData()).length()*25;								
			}		
				totalTblWidth += calculatedValue;
		tblColumnWidth[6]= calculatedValue;
			%>
		<%
		GeneralTable sfl = fw.getTable("s6237screensfl");
		int height;
		if(sfl.count()*27 > 355) {
		height = 355;
		} else {
		height = sfl.count()*27;
		}
		
		%>
<!--  Ilife- Life Cross Browser - Sprint 2 D1 : Task 5 Start-->


<%-- <div id="subfh" onscroll="subfh.scrollLeft=this.scrollLeft;" style='position: relative;width: <%if(totalTblWidth < 730 ) {%> <%=totalTblWidth%>px;<%} else { %>730px;<%}%>  height: 355px;  border:#316494 1px solid; 
	 <%if(totalTblWidth < 730 ) {%> overflow-x:hidden; <% } else { %> overflow-x:auto; <%}%> <%if(sfl.count()*27 > 100 ) {%> overflow-y:auto; <% } else { %> overflow-y:hidden; <%}%>'>
		 <DIV id="subf" style="POSITION: relative font-weight: bold; WIDTH: <%=totalTblWidth%>px;margin-right: -4px;  HEIGHT: 355px;  ">
		<table style="border:1px;margin-top:-1px; margin-left:-2px; margin-right:-4px;;width:<%=totalTblWidth%>px;" bgcolor="#dddddd" cellspacing="1px">		
		
		<tr style="background:#316494;  height: 25px;">
		<!-- Manual works starts -->
			<td rowspan="2" style=" color:white; font-weight: bold; padding: 5px; width:<%=tblColumnWidth[0 ]%>px;z-index:8;position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;" align="center"><%=resourceBundleHandler.gettingValueFromBundle("Header1")%></td>									
			<td colspan="2" style=" color:white; font-weight: bold; padding: 5px; width:<%=tblColumnWidth[1  ]+tblColumnWidth[2  ]%>px;" align="center">Agent</td>
			<td colspan="4" style=" color:white; font-weight: bold; padding: 5px; width:<%=tblColumnWidth[3  ]+tblColumnWidth[4  ]+tblColumnWidth[5  ]+tblColumnWidth[6  ]%>px;" align="center">Element</td>
		</tr>
		<tr style="background:#316494;  height: 25px;">
			<td style=" color:white; font-weight: bold; padding: 5px; width:<%=tblColumnWidth[1  ]%>px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header2")%>-->Number</td>
			<td style=" color:white; font-weight: bold; padding: 5px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header3")%>-->Name</td>
			<td style=" color:white; font-weight: bold; padding: 5px; width:<%=tblColumnWidth[3  ]%>px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header4")%>-->Coverage</td>
			<td style=" color:white; font-weight: bold; padding: 5px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header5")%>-->Rider</td>
			<td style=" color:white; font-weight: bold; padding: 5px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header6")%>-->Plan</td>
			<td style=" color:white; font-weight: bold; padding: 5px;" align="center"><!-- <%=resourceBundleHandler.gettingValueFromBundle("Header7")%>-->Occupational Category</td>
		<!-- Manual works ends -->	
		</tr> --%>
		
		
	<!--  Ilife- Life Cross Browser - Sprint 2 D1 : Task 5 Start-->	
		
		<style type="text/css">
.fakeContainer {
	width:720px;		
	height:330px; 
	top:140px;
	left:1px;
	}
	.s6237Table tr{height:32px}
			

</style>
    <script language="javascript">
    $ (document).ready(function(){
    	new superTable("s6237Table", {
			fixedCols : 0,	
			headerRows : 2,
			colWidths : [50,100,253,70,70,70,90],
			<%if(false){%>		
				headerRows :headerRowCount,		
				addRemoveBtn:"Y",
				<%}%>
			hasHorizonScroll: "Y"
		
			
		});
    });
    </script>
    
	<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover " width="100%"
							id='dataTables-s6237'>
							<thead>
								<tr class='info'>
		<th rowspan="2" style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Select") %></th>
<th colspan="2" style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Agent") %></th>
<th colspan="4" style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Element") %></th>

		</tr>
		
		<tr class='info'>
		
		<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Number") %></th>
<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Name") %></th>
<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Coverage") %></th>
<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Rider") %></th>
<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Plan") %></th>
<th style="text-align:center;"><%=resourceBundleHandler.gettingValueFromBundle("Occupational Category") %></th>

	
		</tr></thead>
	<!--  Ilife- Life Cross Browser - Sprint 1 D6 :  ends-->	
		<%

	String backgroundcolor="#FFFFFF";
	
	S6237screensfl
	.set1stScreenRow(sfl, appVars, sv);
	int count = 1;
	while (S6237screensfl
	.hasMoreScreenRows(sfl)) {
	
%>

	<tr style="background:<%= backgroundcolor%>;">
						    									<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[0  ]%>px;z-index:8;position:relative;left:expression(this.parentElement.offsetParent.offsetParent.offsetParent.scrollLeft+1); border-right: 1px solid #dddddd;" align="center">														
																	
								<!--  Ilife- Life Cross Browser - Sprint 1 D6 :  starts-->						
					
					 					 
					 <input type="radio" title="Select client and click required linked action."  
						 value='<%= sv.select.getFormData() %>' 
						 onFocus='doFocus(this)' onHelp='return fieldHelp("s6237screensfl" + "." +
						 "select")' onKeyUp='return checkMaxLength(this)' 
						 
						 
						 id='s6237screensfl.select_R<%=count%>'
						 name='s6237screensfl.select_R<%=count%>'
						 onClick="selectedRow('s6237screensfl.select_R<%=count%>')"
						 class="radio"
					 />
					 
					 		<!--  Ilife- Life Cross Browser - Sprint 1 D6 :  ends-->				
					
											
									</td>
				    									<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[1  ]%>px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.comagnt.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:250px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.agntname.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[3  ]%>px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.coverage.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:90px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.rider.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:<%=tblColumnWidth[5  ]%>px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.planSuffix.getFormData()%>
						
														 
				
									</td>
				    									<td style="color:#434343; padding: 5px; width:90px;font-weight: bold;font-size: 12px; font-family: Arial" align="left">									
																
									
											
						<%= sv.ovrdcat.getFormData()%>
						
														 
				
									</td>
				
		
		
	</tr>


	<%
	if(backgroundcolor.equalsIgnoreCase("#FFFFFF")){
		backgroundcolor="#ededed";
		}else{
		backgroundcolor="#FFFFFF";
		}
	count = count + 1;
	S6237screensfl
	.setNextScreenRow(sfl, appVars, sv);
	}
	%>
		</table>
		</div></div></div></div>
		<!-- </DIV> -->
		<!-- Manual works starts -->
	<!-- <div style="position:absolute; top:80px; height:35px; left:15px; width:50px; border:#35759b 2px solid; border:0px;"> -->
	
	
	
	<div class="row">
			<div class="col-md-4">
		<table><tr><!-- </br></br></br></br></br></br> -->
		<td>
		    <div class="sectionbutton">
			<p style="font-size: 12px; margin-right:0px; font-weight: bold;"><a href="#" onClick="JavaScript:perFormOperation(1)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Initial Commission Status")%></a></p>
			</div>
		</td>
		<td>
			<div class="sectionbutton">
			<p style="font-size: 12px; font-weight: bold;"><a href="#" onClick="JavaScript:perFormOperation(2)" class="btn btn-info"><%=resourceBundleHandler.gettingValueFromBundle("Agent Details")%></a></p>
			</div>
		</td>
		</tr></table>
		
	</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!-- Manual works ends -->
<br/><div style='visibility:hidden;'><table></table></div><br/>
</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTables-s6237').DataTable({
    	ordering: false,
    	searching:false,
    	scrollX: true,
    	scrollY: '300px',
        scrollCollapse: true,
        info : false,
        paging:false
        
        
  	});
})
</script>
<%@ include file="/POLACommon2NEW.jsp"%>
